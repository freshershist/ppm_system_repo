$(document).ready(function() {

	if (typeof $.fn.slick !== 'undefined') {
		$('#banner1.rotator').slick({
			accessibility: false,
			autoplay: true,
			autoplaySpeed: 5000,
			slidesToShow: 1,
			dots: true,
			arrows: true,
			responsive: [{
				breakpoint: 767,
				settings: {
					arrows: false,
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}]

		});

		$('#circles.rotator').slick({
			accessibility: false,
			autoplay: true,
			autoplaySpeed: 5000,
			arrows: true,
			adaptiveHeight: true,
			slidesToShow: 4,
			slidesToScroll: 4,
			dots: false,
			responsive: [{
				breakpoint: 767,
				settings: {
					arrows: false,
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}, {
				breakpoint: 992,
				settings: {
					arrows: false,
					slidesToShow: 2,
					slidesToScroll: 2
				}
			}]
		});
	}

	$('#menu').on('click', function() {
		$('#menubar').toggleClass('open');
	});

	$('#menu-close').on('click', function() {
		$('#menubar').removeClass('open');
	});

	$('#scroll-top-btn').on('click', function(e) {
		$(this).blur();
		e.preventDefault();
		document.querySelector('header').scrollIntoView({
			behavior: 'smooth'
		});
	});

});