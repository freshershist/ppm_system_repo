CKEDITOR.plugins.add( 'gallery', {
	icons: 'gallery',
	init: function( editor ) {
		editor.addCommand('gallery', {
			exec: function (editor) {
				$(CKEDITOR.currentInstance).trigger('enableFormSubmit')
				var _name = CKEDITOR.currentInstance.name;

				$("#ppm-browse-image").trigger('click');

				ckfunc = function(imgUrl){

					img = new Image()
					
					img.onload = function(){

						CKEDITOR.instances[_name].setReadOnly(false)
						url = imgUrl
						maxWidth = Math.min(this.width, 600)
						maxHeight = Math.min(this.height, 600)

						if ((maxWidth/maxHeight) > (this.width/this.height)){
							width = (maxWidth * this.width)/this.height
							height = maxHeight
						} else if ((maxWidth/maxHeight) < (this.width/this.height)){
							width = maxWidth
							height = (maxHeight * this.height)/this.width
						} else{
							width = maxWidth
							height = maxHeight
						}

						newLine = CKEDITOR.dom.element.createFromHtml('<p><br></p>')
						
						imgElem = '<img src="' + url + '" height="' + height + '" width="' + width + '">'

						imgDomElem = CKEDITOR.dom.element.createFromHtml(imgElem)
						editor.insertElement(newLine)
						editor.insertElement(imgDomElem)
						editor.insertElement(newLine)
						$(CKEDITOR.instances[_name]).trigger('enableFormSubmit')
					}

					img.src = imgUrl
				}

				PPMGallery.AddImage = ckfunc;
			}
		});

		editor.ui.addButton( 'Gallery', {
			label: 'Custom Gallery',
			command: 'gallery',
			toolbar: 'insert'
		});
	}
});
