<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *	@Class Name: usersmodel
 *  @description: all related transaction of users in the database
 *
 */

if(!class_exists('CI_Model')) { class CI_Model extends Model {} }

class OnlineTrainingmodel extends CI_Model
{
	public function __construct()
    {
        parent::__construct();

    }

    public function login($username = NULL, $password = NULL)
    {
        if(!empty($username) && !empty($password)) {
            $sql = "SELECT id,membershipExpires 
                    from members 
                    where username = '{$username}' 
                    and password = '{$password}' 
                    and memberShipType = 5 
                    and memberStatus = 1
                    order by id desc
                    limit 1;";

            return $this->db->query($sql)->result_array();
        }

        return array();
    }

    public function is_logged_in() 
    {
        if(isset($_SESSION['onlinetraining_logged_in']) && $_SESSION['onlinetraining_logged_in']) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    public function get_enews()
    {
        $sql = "SELECT id,name,d1,d2,mydate 
                from eNews 
                where category = 1  
                and myDate < NOW() 
                order by mydate desc;";

        return $this->db->query($sql)->result_array();     
    }

    public function get_category($id = NULL)
    {
        $sql = "SELECT name from categories where area = {$id}";

        $resuts = $this->db->query($sql)->result_array();

        $arr = array();

        foreach ($resuts as $key => $value) {
            $arr[$value['name']] = $value['name'];
        }

        return $arr;
    }

    public function get_trainings($topic = NULL, $keywords = NULL)
    {
        //get media file flv to mp4
        //select REPLACE(mediaFile, '.flv', '.mp4') from onlineTraining where mediaFile like '%.flv%'

        $sql = "SELECT myType,topic,title,shortDesc,speaker,duration,id 
                from onlineTraining 
                where draft = 0";

        if(!empty($keywords)) {
            $sql .= " and topic like '%{$keywords}%' or title like '%{$keywords}%' ";
        }

        if(!empty($topic)) {
            $sql .= " and topic like '%{$topic}%' ";
        }

        $sql .= ' order by orderby';       

        return $this->db->query($sql)->result_array();
    }

    public function get_training($id = NULL)
    {
        $sql = "SELECT id,topic,title,shortDesc,speaker,duration,body,activity,d1,d2,d3,d4,d5,c1,c2,c3,c4,c5,myType,hits,products,articles,downloads,orderby,draft,guestAccess,
            REPLACE(REPLACE(mediaFile, '.flv', '.mp4'), ' ', '%20') as mediaFile 
                from onlineTraining 
                where draft = 0 and id = {$id} limit 1";

        return $this->db->query($sql)->result_array();
    }

    public function update_hits($id = NULL, $memberId = NULL)
    {
        $sql = "update onlineTraining set hits = hits + 1  where id = {$id};";

        $this->db->query($sql);

        //memberId,trainingId,myDateTime

        $data = array(
                'memberId'      => $memberId,
                'trainingId'    => $id,
                'myDateTime'    => date('Y-m-d H:i:s', now(PPM_TIMEZONE))
        );

        $this->db->insert('trainingLog', $data);        
    }

    public function check_training_member($id = NULL)
    {
        //check to see if this member has a linked newsletter subscription
        $sql = "SELECT id from members where linkedmemberid = {$id} and membershiptype = 5 and memberStatus = 1 limit 1;";

        $result = $this->db->query($sql);

        if($result->num_rows() !== 0) {
            $id = intval($result->result_array()[0]['id']);

            $sql = "SELECT id,membershipExpires 
                    from members 
                    where id = {$id}
                    and memberShipType = 5 
                    and memberStatus = 1
                    order by id desc
                    limit 1;";

            $result = $this->db->query($sql)->result_array();

            $data = array(
                        'ot_id'                     => intval($result[0]['id']),
                        'ot_membershipExpires'      => $result[0]['membershipExpires'],
                        'onlinetraining_logged_in'  => TRUE,
                        'trainingMember'            => TRUE
                    );

            $this->session->set_userdata($data);            

        }
    } 

    public function confirm_member($memberId = NULL, $password = NULL)
    {
        $this->db->select('password');
        $this->db->where(array('id'=>$memberId));
        $result = $this->db->get('members');

        $row = $result->row_array();

        $confirmed = FALSE;

        if (isset($row)){
            $confirmed = (md5($row['password'] . SECKEY) === $password) ? TRUE : FALSE;
        }

        return $confirmed;
    }

    public function get_member_info($memberId = NULL)
    {
        $this->db->select('id,company,contactName,contactNumber,address,address2,suburb,postcode,state,emailAddress,emailAddress,username,password,linkedmemberid');
        $this->db->where(array('id'=>$memberId));
        $result = $this->db->get('members');

        return $result->row_array();
    }

    public function add_update($data = NULL, $memberId = NULL)
    {
        if(empty($memberId)) {
            $this->db->insert('members', $data);
            $memberId = $this->db->insert_id();
        }
        else {
            $this->db->where('id', $memberId);
            $this->db->update('members', $data);
            $memberId = $this->db->affected_rows();
        }

        return $memberId;
    }    

    public function add_member_staff($data = NULL)
    {
        $this->db->insert('memberStaff', $data);
    }

    public function add_transaction($data = NULL)
    {
        $this->db->insert('transactions', $data);
        return $this->db->insert_id();
    }

    public function update_transaction($orderId = NULL, $data = NULL)
    {
        if($this->db->limit(1)->get_where('transactions', array('orderId'=>$orderId))->num_rows() === 0) {
            return FALSE;
        }
        else {
            $this->db->where('orderId', $orderId);
            $this->db->update('transactions', $data);            
            return TRUE;
        }
    }

    public function lock_by_user($memberId = NULL){
        $lockExpires = date('Y-m-d h:i:s', strtotime('+3 hour' , now(PPM_TIMEZONE)));
        $sql = "update members set lockExpires = '{$lockExpires}', lockedByUser = 0 where id = {$memberId}";

        $this->db->query($sql);
    }

    public function get_membershipType($linkedMemberId = NULL)
    {
        $this->db->select('membershiptype');
        $this->db->where(array('id'=>$linkedMemberId));
        $result = $this->db->get('members');

        $row = $result->row_array();

        $confirmed = FALSE;

        if ($result->num_rows() > 0){
            return $row['membershiptype'];
        }

        return -1;
    }   
}
