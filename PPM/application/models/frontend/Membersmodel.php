<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *	@Class Name: usersmodel
 *  @description: all related transaction of users in the database
 *
 */

if(!class_exists('CI_Model')) { class CI_Model extends Model {} }

class Membersmodel extends CI_Model
{
	public function __construct()
    {
        parent::__construct();

        $this->data_collect = array();
    }

    public function login($username = NULL, $password = NULL)
    {
        if(!empty($username) && !empty($password)) {
            $this->db->select('id, membershipType, company, memberStatus, membershipExpires');
            $this->db->where(array('username'=>$username, 'password'=>$password));
            $this->db->where('memberStatus !=', '');
            $where_in = array(0,1,2,3,5);

            $this->db->where_in('membershipType', $where_in);
            $result = $this->db->limit(1)->get('members');

            return $result->result_array();
        }

        return array();
    }

    public function is_logged_in() 
    {
        if(isset($_SESSION['member_logged_in']) && $_SESSION['member_logged_in']) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    public function get_banner()
    {
        if($this->is_logged_in()) {

        }
    }

    public function get_news_articles($limit = NULL, $filter = NULL)
    {
        if(!empty($limit) && !empty($filter)) {
            $sql = "select news.id,name,myDate,shortdesc,d1 
                from news,category 
                where category.categoryid in ({$filter}) 
                and category.id = news.id 
                and category.area = 'n' 
                and myDate < NOW() order by myDate desc,news.id limit {$limit};";

            $result = $this->db->query($sql);

            return $result->result_array();
        }
    }

    public function get_galleries()
    {
        $sql = "SELECT g.name,g.body,g.id,
                (SELECT d1 from galleryitems where id = g.id order by orderby desc limit 1) as d1,
                (SELECT caption from galleryitems where id = g.id order by orderby desc limit 1) as caption
                from galleries as g 
                where g.category = 947  
                and g.myDate < NOW() 
                order by g.myDate desc;";

        $result = $this->db->query($sql);

        return $result->result_array();
    }

    public function get_articles($confineTo = NULL, $categoryTopic = 0, $state = NULL, $page = NULL, $category = NULL, $keywords = NULL)
    {
        $_sql   = '';
        $limit  = (!empty($page)) ? ' limit '.$page.', 10 ':' limit 0, 10 ';
        $arr = array();

        if(is_numeric($state)) $_sql .= " and (state = " . intval($state) . " or state is Null)";

        if(is_numeric($category) && $category > 0) {
            
            $sql = "SELECT news.name,news.id,news.d1,news.d2,shortdesc,mydate
                    from news,category 
                    where news.id = category.id 
                    and category.categoryid = {$category} 
                    and category.area = 'n' 
                    and news.Id 
                    in (select id 
                    from category 
                    where categoryid 
                    in ({$confineTo}) 
                    and area = 'n') 
                    and news.MyDate < NOW() 
                    " . $_sql . "
                    order by mydate desc
                    ".$limit;       

            $result = $this->db->query($sql);

            $arr['result'] = $result->result_array();

            $this->db->select('name as catname');
            $this->db->where(array('id'=>$category, 'area'=>6));
            $result = $this->db->get('categories');

            $catname = '';
            $cat_arr = $result->result_array();
            
            if(!empty($cat_arr)) {
                
                $catname = $cat_arr[0]['catname'];
            }
            
            $arr['catname'] = $catname;
            
            $sql = "SELECT news.id 
                    from news,category 
                    where news.id = category.id 
                    and category.categoryid = {$category} 
                    and category.area = 'n' 
                    and news.Id 
                    in (select id 
                    from category 
                    where categoryid 
                    in ({$confineTo}) 
                    and area = 'n') 
                    and news.MyDate < NOW() 
                    " . $_sql . "
                    order by mydate desc;";

            $result = $this->db->query($sql);

            $arr['total'] = $result->num_rows();

            return $arr;
        }
        elseif(is_numeric($categoryTopic) && $categoryTopic > 0) {
            
            $_sql .= " and category = " . intval($categoryTopic);

            $sql = "SELECT news.name,news.id,news.d1,shortdesc,mydate              
                    from news 
                    where news.Id 
                    in (select id 
                    from category 
                    where categoryid 
                    in ({$confineTo}) 
                    and area = 'n') 
                    and news.MyDate < NOW()  
                    " . $_sql . " 
                    order by mydate desc
                    ".$limit;

            $result = $this->db->query($sql);

            $arr['result'] = $result->result_array();

            $sql = "SELECT id 
                    from news 
                    where news.Id 
                    in (select id 
                    from category 
                    where categoryid 
                    in ({$confineTo}) 
                    and area = 'n') 
                    and news.MyDate < NOW()  
                    " . $_sql . " 
                    order by mydate desc";
            $result = $this->db->query($sql);
            $arr['total'] = $result->num_rows();

            return $arr;            
        }
        elseif(!empty($keywords)) {
            $sql = "SELECT news.name,news.id,news.d1,news.d2,shortdesc,mydate 
                    from news 
                    where (name like '%{$keywords}%' or source like '%{$keywords}%')  
                    and news.Id 
                    in (select id 
                    from category 
                    where categoryid 
                    in ({$confineTo}) and area = 'n') 
                    and news.MyDate < NOW()
                    " . $_sql . " 
                    order by mydate desc
                    ".$limit;

            $result = $this->db->query($sql);

            $arr['result'] = $result->result_array();

            $sql = "SELECT id 
                    from news 
                    where (name like '%{$keywords}%' or source like '%{$keywords}%')  
                    and news.Id 
                    in (select id 
                    from category 
                    where categoryid 
                    in ({$confineTo}) and area = 'n') 
                    and news.MyDate < NOW()
                    " . $_sql . " 
                    order by mydate desc;";

            $result = $this->db->query($sql);
            $arr['total'] = $result->num_rows();

            return $arr;
        }
        else {

            $sql = "SELECT news.name,news.id,news.d1,news.d2,shortdesc,mydate
                    from news 
                    where news.Id in 
                    (select id 
                    from category 
                    where categoryid in ({$confineTo}) 
                    and area = 'n') 
                    and news.MyDate < NOW() 
                    " . $_sql . " 
                    order by mydate desc
                    ".$limit;

            $result = $this->db->query($sql);

            $arr['result'] = $result->result_array();

            $sql = "SELECT id 
                    from news 
                    where news.Id in 
                    (select id 
                    from category 
                    where categoryid in ({$confineTo}) 
                    and area = 'n') 
                    and news.MyDate < NOW() 
                    " . $_sql . " 
                    order by mydate desc";
            $result = $this->db->query($sql);
            $arr['total'] = $result->num_rows();

            return $arr;
        }

        return array();      
    }

    public function get_article($id = NULL)
    {
        $this->db->select('name,mydate,body,d1,d2,d3,d4,d5,c1,c2,c3,c4,c5,code,relatedproducts,location,source,relatedDownloads,shortdesc');
        $this->db->where(array('id'=>$id));
        $result = $this->db->get('news');

        return $result->result_array();
    }

    public function get_related_downloads_for_news($relatedDownloads = NULL)
    {
        if(!empty($relatedDownloads)) {

            switch (intval($_SESSION['membershipType'])) {
                case 0:
                    $accessField = "Platnium";
                    break;
                case 1:
                    $accessField = "Gold";
                    break;
                case 2:
                    $accessField = "Silver";
                    break;
                case 3:
                    $accessField = "Bronze";
                    break;
                default:
                    break;
            }

            $sql = "SELECT id,dateposted,name,code,shortdesc from downloads where " . $accessField . " = 1 and id in (" . $relatedDownloads . ")  order by dateposted;";

            $result = $this->db->query($sql);

            return $result->result_array();
        }
        else {
            return array();
        }
    }

    public function update_download_log($id)
    {
        $now = date('Y-m-d H:i:s', time());
        $memberId = $_SESSION['id'];

        $sql = "INSERT into downloadlog (memberId,downloadId,mydate) values ({$memberId},{$id},'{$now}');";

        $this->db->query($sql);
    }

    public function get_downloads($id  = NULL, $section = NULL)
    {
        $sql = "SELECT * from downloads where {$section} = 1 and id = {$id};";

        $result = $this->db->query($sql);

        return $result->result_array();
    }

    public function get_categories_by_area($area = NULL)
    {
        $sql = "SELECT id,name from categories where area = {$area} order by name";

        $result = $this->db->query($sql);

        return $result->result_array();
    }

    public function get_categories_id_by_area($area = NULL)
    {
        $sql = "SELECT id from categories where area = {$area}";

        $result = $this->db->query($sql);

        return $result->result_array();
    }

    public function get_discounts()
    {
        $sql = "SELECT links.id,name,categoryid,links.d1,shortdesc 
                from links,category 
                where memberDiscounts != '' 
                and links.id = category.id 
                and area = 'l' 
                and links.isActive = 1 
                and links.displaymember = 1 ;";

        $result = $this->db->query($sql);

        return $result->result_array();

    }

    public function get_links($subcatid = NULL, $linkid = NULL) 
    {
        $sql = "SELECT links.name,pcontact,links.body,address,phone,fax,email,website,links.d1,d2,categories.name as catname,memberDiscounts 
            from links,categories 
            where links.isActive = 1 
            and links.id = {$linkid} 
            and categories.id = {$subcatid};";

        $result = $this->db->query($sql);

        return $result->result_array();

    }

    public function get_categories_by_parent($id = NULL)
    {
        $sql = "SELECT name,id,d1 from categories where parent = {$id} order by name;";

        $result = $this->db->query($sql);

        return $result->result_array();
    }

    public function get_list_links($catid = NULL, $subcatid = NULL, $postcode = NULL)
    {
        $sql = '';

        if($catid === 35) {
            $sql = "SELECT name,links.id,nationalSubCat,shortDesc 
                    from links,category 
                    where links.id = category.id 
                    and links.isActive = 1 
                    and links.displaylinkspage = 1 
                    and (category.categoryId = {$subcatid} or category.categoryId = 89) 
                    and category.area = 'l' 
                    order by nationalSubCat,name;";
        }
        elseif($catid === 145) {
            $sql = "SELECT company as name,id 
                    from members 
                    where state = {$subcatid} 
                    and membershipType <= 3 
                    and memberStatus = 1 
                    order by company;";
        }
        else {
            $sql = "SELECT name,links.id,shortDesc 
                    from links,category 
                    where links.id = category.id 
                    and links.isActive = 1 
                    and links.displaylinkspage = 1 
                    and category.categoryId = {$subcatid} 
                    and category.area = 'l' 
                    order by name;";
        }

        $result = $this->db->query($sql);

        return $result->result_array();
    }

    public function get_category_name($catid = NULL) 
    {
        $sql = "SELECT name from categories where id = {$catid};";

        $result = $this->db->query($sql);

        $row = $result->row_array();

        if (isset($row)){
            return $row['name'];
        }

        return '';
    }

    public function get_downloads_by_section($section = NULL, $state = NULL)
    {
        if(!empty($section)) {

            switch (intval($_SESSION['membershipType'])) {
                case 0:
                    $accessField = "Platnium";
                    break;
                case 1:
                    $accessField = "Gold";
                    break;
                case 2:
                    $accessField = "Silver";
                    break;
                case 3:
                    $accessField = "Bronze";
                    break;
                default:
                    break;
            }

            $sql = "SELECT id,dateposted,name,code,shortdesc,documenttype,documentlocation,max(mydate) as lastdownload 
                    from downloads 
                    left join downloadLog 
                    on ( downloadLog.downloadId = downloads.id 
                    and downloadLog.memberId = 1 )  
                    where {$accessField} = 1 
                    and mysection = {$section} 
                    and downloads.id 
                    in (select id 
                    from category 
                    where categoryId = {$state}
                    and area = 'd') 
                    group by id,dateposted,name,code,shortdesc,documenttype,documentlocation 
                    order by dateposted desc;";

            $result = $this->db->query($sql);

            return $result->result_array();
        }
        else {
            return array();
        }
    }    

    public function get_enews()
    {
        $sql = "SELECT id,name,d1,d2,mydate 
                from eNews 
                where category = 1  
                and myDate < NOW() 
                order by mydate desc;";

        return $this->db->query($sql)->result_array();     
    }

    public function get_enews_file($id = NULL, $type = NULL)
    {

        $sql = "SELECT {$type} from eNews where id = {$id};";

        $result = $this->db->query($sql);

        $row = $result->row_array();

        if (isset($row)){
            return $row[$type];
        }

        return ''; 
    }

    public function get_enews_download_count($id = NULL, $memberId = NULL, $type = NULL)
    {
        $sql = "SELECT count(*) as count from downloadLogENews 
                where dType = '{$type}' 
                and eNewsID = {$id} 
                and memberId = {$memberId};";

        $result = $this->db->query($sql);

        $row = $result->row_array();

        if (isset($row)){
            return intval($row['count']);
        }

        return 0;                
    }

    public function update_enews_download_log($data = NULL)
    {
        if(!empty($data)) {
            $this->db->insert('downloadLogENews', $data);
        }
    }

    public function get_surveys()
    {
        $sql = "SELECT pollName,id,endDate 
                from polls 
                where endDate <= NOW() 
                and member = 1 
                order by endDate desc;";


        return $this->db->query($sql)->result_array();           
    }

    public function get_galleries2($page = NULL, $is_count = FALSE)
    {
        if($is_count) {
            $sql = "SELECT count(*) as count from galleries where mydate< NOW() and category in (947) order by mydate desc;";
            $result = $this->db->query($sql);

            $row = $result->row_array();

            if (isset($row)){
                return $row['count'];
            }

            return 0; 
        }
        else {
            $limit  = (!empty($page)) ? ' limit '.$page.', 12 ':' limit 0, 12 ';

            $sql = "SELECT name,shortdesc,mydate,id,(select d1 from galleryitems where galleryitems.id = galleries.id order by orderBy desc limit 1) as d1 from galleries where mydate< NOW() and category in (947) order by mydate desc " . $limit;

            return $this->db->query($sql)->result_array();
        }
    }

    public function get_album($id = NULL, $is_contents = FALSE)
    {
        if($is_contents) {
            $sql = "SELECT d1,caption,body 
                    from galleryitems 
                    where id = {$id} 
                    order by orderBy desc;";
        }
        else {
            $sql = "SELECT name,body,mydate 
                from galleries 
                where id = {$id} 
                and mydate < NOW()";
        }
        return $this->db->query($sql)->result_array();
    }

/*** Signup / Renewal ***/    

    public function confirm_member($memberId = NULL, $password = NULL)
    {
        $this->db->select('password');
        $this->db->where(array('id'=>$memberId));
        $result = $this->db->get('members');

        $row = $result->row_array();

        $confirmed = FALSE;

        if (isset($row)){
            $confirmed = (md5($row['password'] . SECKEY) === $password) ? TRUE : FALSE;
        }

        return $confirmed;
    }

    public function get_member_info($memberId = NULL)
    {
        $this->db->select('id,company,contactName,contactNumber,address,address2,suburb,postcode,state,emailAddress,emailAddress,username,password,linkedmemberid');
        $this->db->where(array('id'=>$memberId));
        $result = $this->db->get('members');

        return $result->row_array();
    }

    public function add_update($data = NULL, $memberId = NULL)
    {
        if(empty($memberId)) {
            $this->db->insert('members', $data);
            $memberId = $this->db->insert_id();
        }
        else {
            $this->db->where('id', $memberId);
            $this->db->update('members', $data);
            $memberId = $this->db->affected_rows();
        }

        return $memberId;
    }    

    public function add_member_staff($data = NULL)
    {
        $this->db->insert('memberStaff', $data);
    }

    public function add_transaction($data = NULL)
    {
        $this->db->insert('transactions', $data);
        return $this->db->insert_id();
    }

    public function update_transaction($orderId = NULL, $data = NULL)
    {
        if($this->db->limit(1)->get_where('transactions', array('orderId'=>$orderId))->num_rows() === 0) {
            return FALSE;
        }
        else {
            $this->db->where('orderId', $orderId);
            $this->db->update('transactions', $data);            
            return TRUE;
        }
    }

    public function lock_by_user($memberId = NULL){
        $lockExpires = date('Y-m-d h:i:s', strtotime('+3 hour' , now(PPM_TIMEZONE)));
        $sql = "update members set lockExpires = '{$lockExpires}', lockedByUser = 0 where id = {$memberId}";

        $this->db->query($sql);
    }

/*** Signup / Renewal ***/

}
