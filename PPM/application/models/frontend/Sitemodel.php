<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *	@Class Name: usersmodel
 *  @description: all related transaction of users in the database
 *
 */

if(!class_exists('CI_Model')) { class CI_Model extends Model {} }

class Sitemodel extends CI_Model
{
	public function __construct()
    {
        parent::__construct();

    }

    public function get_news_articles($categoryID = NULL)
    {

        $sql = "SELECT news.name,news.id,shortdesc,mydate,REPLACE(d1, ' ', '%20') as d1 
                from news,category 
                where news.id = category.id 
                and category.categoryID in ({$categoryID}) 
                and category.area = 'n' 
                and news.MyDate < NOW() 
                and draft = 0 
                order by mydate desc
                limit 3;";

        return $this->db->query($sql)->result_array();
    }    

    public function get_news_articles_by_type($categoryID = NULL, $is_count = FALSE, $page = NULL)
    {

        if($is_count) {
            $sql = "SELECT count(*) as count from news where news.Id in (select id from category where categoryid in ({$categoryID}) and area = 'n') and news.MyDate < NOW() and draft = 0   order by mydate desc";
            $result = $this->db->query($sql);

            $row = $result->row_array();

            if (isset($row)){
                return $row['count'];
            }

            return 0;
        }
        else {   

            $limit  = (!empty($page)) ? ' limit '.$page.', 12 ':' limit 0, 12 ';

            $sql = "SELECT news.name,news.id,shortdesc,mydate,REPLACE(d1, ' ', '%20') as d1 
                    from news 
                    where news.Id 
                    in (select id 
                    from category 
                    where categoryid 
                    in ({$categoryID}) 
                    and area = 'n') 
                    and news.MyDate < NOW() 
                    and draft = 0   
                    order by mydate desc" . $limit;

            return $this->db->query($sql)->result_array();
        }
    }

    public function get_galleries($page = NULL, $is_count = FALSE)
    {
        if($is_count) {
            $sql = "SELECT count(*) as count from galleries where mydate < NOW() and category not in (947) order by mydate desc;";
            $result = $this->db->query($sql);

            $row = $result->row_array();

            if (isset($row)){
                return $row['count'];
            }

            return 0; 
        }
        else {
            $limit  = (!empty($page)) ? ' limit '.$page.', 12 ':' limit 0, 12 ';

            $sql = "SELECT name,shortdesc,mydate,id,(select d1 from galleryitems where galleryitems.id = galleries.id order by orderBy desc limit 1) as d1 from galleries where mydate< NOW() and category not in (947) order by mydate desc " . $limit;

            return $this->db->query($sql)->result_array();
        }
    }

    public function get_galleries_by_category($categoryid = NULL)
    {
        if(!empty($categoryid)) {
            $sql = "SELECT *,(select d1 from galleryitems where galleryitems.id = galleries.id order by orderBy asc limit 1) as d1 from galleries where category = {$categoryid} order by mydate desc;";

            $arr = array();

            $arr['items'] = $this->db->query($sql)->result_array();

            $sql = "SELECT name from categories where id = {$categoryid} limit 1;";

            $result = $this->db->query($sql);

            $row = $result->row_array();

            $arr['name'] = '';

            if (isset($row)){
                $arr['name'] = $row['name'];
            }

            return $arr;
        }

        return array();
    }

    public function get_album($id = NULL, $is_contents = FALSE)
    {
        if($is_contents) {
            $sql = "SELECT d1,caption,body 
                    from galleryitems 
                    where id = {$id} 
                    order by orderBy desc;";
        }
        else {
            $sql = "SELECT name,body,mydate,category 
                from galleries 
                where id = {$id} 
                and myDate < NOW()";
        }
        return $this->db->query($sql)->result_array();
    }

    public function get_events()
    {   
        //where startDate >= NOW() 
        $sql = "SELECT name,REPLACE(d2, ' ', '%20') as d2,REPLACE(d1, ' ', '%20') as d1,events.id,shortdesc,startDate,location2,endDate 
                from events
                where startDate >= NOW()
                order by startDate asc
                limit 3";

        return $this->db->query($sql)->result_array();        
    }

    public function get_products($category = NULL, $isTop3 = FALSE, $page = NULL, $isCount = FALSE)
    {
        //"SELECT top 3 name,products.id,shortdesc,code,compability,d1 from products where productOfTheMonth = '" & returnTheDate("1/" & month(now()) & "/" & year(now())) & "'   order by code,name"
        if($isTop3) {
            $year = date('Y', now());
            $month = date('m', now());
            $date = $year .'-'. $month .'-01 00:00:00';

            $sql = "SELECT name,products.id,shortdesc,code,compability,REPLACE(d1, ' ', '%20') as d1 
                from products
                where productOfTheMonth = '".$date."' 
                order by code,name
                limit 3";
        }
        else {
            if($isCount) {

                if(is_numeric($category) && $category === 0) {
                    $sql = "SELECT count(*) as count from products";
                }
                else {
                    $sql = "SELECT count(*) as count from products,category where products.id = category.id and category.categoryID = {$category} and area = 'p' order by code,name ";
                }

                $result = $this->db->query($sql);

                $row = $result->row_array();

                if (isset($row)){
                    return $row['count'];
                }

                return 0;
            }
            else {
                $limit  = (!empty($page)) ? ' limit '.$page.', 12 ':' limit 0, 12 ';

                if(is_numeric($category) && $category === 0) {
                    $sql = "SELECT name,products.id,shortdesc,code,compability,d1 from products order by code,name" . $limit;
                }
                else {
                    $sql = "SELECT name,products.id,shortdesc,code,compability,d1 from products,category where products.id = category.id and category.categoryID = {$category} and area = 'p' order by code,name " . $limit;
                }
            }

        }

        return $this->db->query($sql)->result_array();        
    }

    public function get_product_category($category = NULL)
    {
        $sql = "select name from categories where id = {$category} and area = 3;";

        $result = $this->db->query($sql);

        $row = $result->row_array();

        if (isset($row)){
            return $row['name'];
        }

        return '';       
    }

    public function get_product_details($id = NULL)
    {
        $sql = "SELECT name,compability,body,shortDesc,relatedEvents,d1,d2,d3,categoryId,d4,d5,d6,d7,d8,d9,d10,c1,c2,c3,c4,c5,c6,c7,c8,c9,c10 from products,category where products.id = {$id} and products.id = category.id and area = 'p';";

        return $this->db->query($sql)->result_array();
    }

    public function get_product_costs($id = NULL)
    {
        $sql = "SELECT name,cost,postage from productCosts where productid = {$id};";

        return $this->db->query($sql)->result_array();
    } 

    public function get_rent_roll($state = NULL)
    {
        if(!empty($state)) {
            $sql = "SELECT name,shortdesc,news.id,code,state,stateSubRegionQLD,stateSubRegionOther,location,(Select count(*) from enquiryDetails where newsId = news.id) as numberOfEnquiries,
                CASE name WHEN 'For Sale' THEN 1 WHEN 'Wanted' THEN 2 WHEN 'Sold' THEN 3 END as orderColumn 
                from news,category 
                where news.id = category.id 
                and category.categoryId = 5 
                and category.area = 'n' 
                and news.state = {$state} and draft = 0 
                order by orderColumn,name,code;";

            return $this->db->query($sql)->result_array();
        }

        return array();
    }

    public function get_events_name()
    {
        $sql = "SELECT distinct name from events;";

        return $this->db->query($sql)->result_array();
    }

    public function get_rent_roll_status($id = NULL)
    {
        $sql = "Select name,email,contactName from news where id = {$id};";

        $result = $this->db->query($sql);

        $row = $result->row_array();

        return $row;
    }

    public function get_rent_roll_details($id = NULL)
    {
        $sql = "SELECT name,shortdesc,news.id,code,state,location,(Select count(*) from enquiryDetails where newsId = {$id}) as numberOfEnquiries from news,category where news.id = category.id and category.categoryId = 5 and category.area = 'n' and news.id = {$id} and draft = 0 order by name,code;";

        return $this->db->query($sql)->result_array();
    }

    public function add_rent_roll_enquiry($data = NULL)
    {
        $this->db->insert('enquiryDetails', $data);
    }

    public function get_topics($area = NULL)
    {
        $sql = "SELECT id,name from categories where area = {$area} order by name";

        return $this->db->query($sql)->result_array();
    }
    
    public function get_ppmtv_articles($topic = NULL, $is_count = FALSE, $page = NULL)
    {   
        $with_topic = '';

        if($is_count) {
            if(!empty($topic) && is_numeric($topic)) {
                $with_topic .= " and news.id in (select id from category where categoryid = {$topic} and area = 'n')";
            }
            
            $sql = "SELECT count(*) as count
                    from news,category 
                    where news.id = category.id 
                    and category.categoryid = 889 
                    and category.area = 'n' 
                    and news.mydate < CONCAT(NOW(), ' 11:59pm') and draft = 0";
            

            $sql .= $with_topic . ' order by mydate desc;';

            $result = $this->db->query($sql);

            $row = $result->row_array();

            if (isset($row)){
                return $row['count'];
            }

            return 0;
        }
        else {   

            $limit  = (!empty($page)) ? ' limit '.$page.', 12 ':' limit 0, 12 ';

            if(!empty($topic) && is_numeric($topic)) {
                $with_topic .= " and news.id in (select id from category where categoryid = {$topic} and area = 'n')";
            }
            
            $sql = "SELECT news.name,news.id,shortdesc,mydate,d1,youtubelink 
                    from news,category 
                    where news.id = category.id 
                    and category.categoryid = 889 
                    and category.area = 'n' 
                    and news.mydate < CONCAT(NOW(), ' 11:59pm') and draft = 0";
            

            $sql .= $with_topic . ' order by mydate desc ' . $limit;

            return $this->db->query($sql)->result_array();
        }
    }

    public function get_awards($topic = NULL, $myyear = NULL, $mytype = NULL, $is_count = FALSE, $page = NULL)
    {
        $with_topic = (!empty($topic) && is_numeric($topic)) ? ' category = ' . $topic : '';
        $with_topic .= (!empty($topic) && !empty($myyear) && is_numeric($myyear)) ? ' and ' : '';
        $with_myyear = (!empty($myyear) && is_numeric($myyear)) ? ' myyear =  ' . $myyear : '';
        $with_topic .= (!empty($myyear) && !empty($mytype) && is_numeric($mytype)) ? ' and ' : '';
        $with_mytype = (!empty($mytype) && is_numeric($mytype)) ? ' mytype =  ' . $mytype : '';

        if($is_count) {
            $sql = "SELECT count(*) as count from awards";

            if(!empty($with_topic) || !empty($with_myyear) || !empty($with_mytype)) {
                $sql .= ' where ' . $with_topic . $with_myyear . $with_mytype;
            }

            $sql .= ' order by myyear desc;';

            $result = $this->db->query($sql);

            $row = $result->row_array();

            if (isset($row)){
                return $row['count'];
            }

            return 0;
        }
        else {

            $limit  = (!empty($page)) ? ' limit '.$page.', 12 ':' limit 0, 12 ';

            $sql = "SELECT * from awards";

            if(!empty($with_topic) || !empty($with_myyear) || !empty($with_mytype)) {
                $sql .= ' where ' . $with_topic . $with_myyear . $with_mytype;
            }

            $sql .= ' order by myyear desc ' . $limit;
        }

        return $this->db->query($sql)->result_array();
    }

    public function get_ppm_conference_details($id = NULL)
    {
        $arr = array();

        $sql = "SELECT * from events where id =  {$id}";

        $arr['results'] = $this->db->query($sql)->result_array();

        $sql = "select categories.name as name from events,categories,category where events.id = category.id and category.categoryId = categories.id and categories.area = 7 and category.area = 'e' and events.id = {$id}";

        $result = $this->db->query($sql);

        $row = $result->row_array();

        $arr['location'] = '';

        if (isset($row)){
            $arr['location'] = $row['name'];
        }

        $sql = "SELECT count(*) as count from category where id = {$id} and categoryid=30;";

        $result = $this->db->query($sql);

        $arr['isConference'] = ($result->num_rows() > 0) ? TRUE : FALSE;

        return $arr;
    }

    public function get_ppm_conference_event_costs($id = NULL)
    {
        $sql = "SELECT eventCosts.name,eventCosts.cost,e.onlineEvent,eventCosts.limitToMemberType from eventCosts inner join events e on eventCosts.eventId = e.Id where eventCosts.eventId = {$id} order by eventCosts.name;";

        return $this->db->query($sql)->result_array();
    }

    public function get_ppm_conference_speakers($id = NULL)
    {
        $sql = "SELECT * from eventSpeakers where eventId = {$id} order by orderBy;";

        return $this->db->query($sql)->result_array();
    }

    public function get_ppm_conference_sponsors($id = NULL){
        $sql = "SELECT * from eventSponsors where eventId = {$id} order by orderBy;";

        return $this->db->query($sql)->result_array();        
    }


    public function get_name_by_category($category = NULL)
    {
        $sql = "select name from categories where id = {$category}";

        $result = $this->db->query($sql);

        $row = $result->row_array();

        if (isset($row)){
            return $row['name'];
        }

        return '';       
    }

    public function get_testimonials_list_categories()
    {
        $sql = "SELECT name,id from categories where area = 5 and not id in(122,123)";

        return $this->db->query($sql)->result_array();
    } 


    public function get_testimonials($categoryID = NULL, $is_count = FALSE, $page = NULL)
    {

        if($is_count) {
            $sql = "SELECT count(*) as count from testimonials,category where category.categoryID = {$categoryID} and category.id = testimonials.id order by testimonials.Id desc";
            $result = $this->db->query($sql);

            $row = $result->row_array();

            if (isset($row)){
                return $row['count'];
            }

            return 0;
        }
        else {   

            $limit  = (!empty($page)) ? ' limit '.$page.', 10 ':' limit 0, 10 ';

            $sql = "SELECT name,testimonials.id,name,company,body from testimonials,category where category.categoryID = {$categoryID} and category.id = testimonials.id order by testimonials.Id desc" . $limit;

            return $this->db->query($sql)->result_array();
        }
    }   

    public function get_ppm_conference_testimonials()
    {
        $sql = "SELECT name,testimonials.id,name,company,body from testimonials,category where category.categoryID = 88 and category.id = testimonials.id order by testimonials.Id desc;";

        return $this->db->query($sql)->result_array();
    }

    public function get_ppm_conference_sponsors_testimonials()
    {
        $sql = "SELECT name,testimonials.id,name,company,body,d1 from testimonials,category where category.categoryID = 891 and category.id = testimonials.id order by testimonials.id desc;";

        return $this->db->query($sql)->result_array();
    }

    public function get_ppm_conference_register_online($id = NULL)
    {
        $sql = "SELECT EC.name,EC.cost,E.onlineEvent, EC.limitToMemberType from eventcosts EC inner join events E on E.id = EC.eventID where EC.eventId = {$id} order by EC.name;";

        return $this->db->query($sql)->result_array();
    }

    public function get_homepage($id = NULL)
    {

        if(!empty($id)) {
            $sql = "SELECT * from homepage where id = {$id};";

            return $this->db->query($sql)->result_array();
        }
        else {
            $sql = "SELECT * from homepage where isActive = 1 limit 1";

            return $this->db->query($sql)->result_array();            
        }
    }

    public function get_homepage_top4_testimonials()
    {
        $sql = "SELECT body,name,company from testimonials where displayonhome = 1 limit 4;";

        return $this->db->query($sql)->result_array();
    }

    public function get_homepage_top2_news_articles()
    {
        $sql = "SELECT news.name,news.id,shortdesc,mydate,d1 from news,category where news.id = category.id and category.categoryID = 1 and category.area = 'n' and news.MyDate < now() and draft = 0 order by mydate desc limit 2;";

        return $this->db->query($sql)->result_array();
    }

    public function get_homepage_top1_links()
    {
        $sql = "SELECT news.name,news.id,shortdesc,mydate,d1 from news,category where news.id = category.id and category.categoryID = 4 and category.area = 'n' and news.MyDate < now() and draft = 0 order by mydate desc limit 1;";

        return $this->db->query($sql)->result_array();
    }

    public function get_homepage_top1_ppmtv()
    {
        $sql = "SELECT news.name,news.id,shortdesc,mydate,d1,youtubelink from news,category where news.id = category.id and category.categoryID = 889 and category.area = 'n' and news.MyDate < now() and draft = 0 order by mydate desc limit 1;";

        return $this->db->query($sql)->result_array();
    }

    public function get_event_names()
    {
        $sql = "SELECT distinct name from events where startDate >= '2016-01-01'";

        return $this->db->query($sql)->result_array();

    }

    public function get_training_events($category = NULL, $keywords = NULL, $startDate = NULL, $endDate = NULL)
    {
        $now = "'2017-01-01'";//" NOW() ";

        if(!empty($category) && is_numeric($category)) {
            $category = intval($category);

            if($category >= 19 && $category <= 27) {
                $sql = "SELECT name,REPLACE(d2, ' ', '%20') as d2,REPLACE(d1, ' ', '%20') as d1,events.id,shortdesc,startDate,location2,endDate from events,category where events.id = category.id and (category.categoryID = {$category} or category.categoryId = 443) and category.area = 'e' and startDate >= {$now} order by startDate asc;";
            }
            else {
                $sql = "SELECT name,REPLACE(d2, ' ', '%20') as d2,REPLACE(d1, ' ', '%20') as d1,events.id,shortdesc,startDate,location2,endDate from events,category where events.id = category.id and category.categoryID = {$category} and category.area = 'e' and startDate >= {$now} order by startDate asc;";
            }

            return $this->db->query($sql)->result_array();
        }
        elseif(!empty($startDate) && !empty($endDate)) {
            $sql = "SELECT name,d2,d1,events.id,shortdesc,startDate,location2,endDate from events where events.startDate between '{$startDate}' and '{$endDate}' and startDate >= {$now} order by startDate asc;";

            return $this->db->query($sql)->result_array();
        }
        elseif(!empty($keywords)) {
            $sql = "SELECT name,REPLACE(d2, ' ', '%20') as d2,REPLACE(d1, ' ', '%20') as d1,events.id,shortdesc,startDate,location2,endDate from events where name like '%{$keywords}%'  and startDate >= {$now} order by startDate asc; ";

            return $this->db->query($sql)->result_array();
        }
        elseif(is_numeric($category) && $category === 0) {
            $sql = "SELECT name,d2,d1,events.id,shortdesc,startDate,location2,endDate from events where startDate >= {$now} order by startDate asc;";

            return $this->db->query($sql)->result_array();
        }

        return NULL;
    }
}
