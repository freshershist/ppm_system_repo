<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *	@Class Name: usersmodel
 *  @description: all related transaction of users in the database
 *
 */

if(!class_exists('CI_Model')) { class CI_Model extends Model {} }

class Newslettersmodel extends CI_Model
{
	public function __construct()
    {
        parent::__construct();

    }

    public function login($username = NULL, $password = NULL)
    {
        if(!empty($username) && !empty($password)) {
            $this->db->select('membershipExpires,id,password');
            $this->db->where(array('username'=>$username, 'password'=>$password, 'memberShipType'=> 4, 'memberStatus'=> 1));
            $result = $this->db->get('members');

            return $result->result_array();
        }

        return array();
    }

    public function is_logged_in() 
    {
        if(isset($_SESSION['newsletter_logged_in']) && $_SESSION['newsletter_logged_in']) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    public function get_enews()
    {
        $sql = "SELECT id,name,d1,d2,mydate 
                from eNews 
                where category = 1  
                and myDate < NOW() 
                order by mydate desc;";

        return $this->db->query($sql)->result_array();     
    }

    public function lock_by_user($memberId = NULL){
        $lockExpires = date('Y-m-d h:i:s', strtotime('+3 hour' , now(PPM_TIMEZONE)));
        $sql = "update members set lockExpires = '{$lockExpires}', lockedByUser = 0 where id = {$memberId}";

        $this->db->query($sql);
    }

    public function confirm_member_renewal($memberId = NULL, $password = NULL)
    {
        $this->db->select('password');
        $this->db->where(array('id'=>$memberId));
        $result = $this->db->get('members');

        $row = $result->row_array();

        $confirmed = FALSE;

        if (isset($row)){
            $confirmed = (md5($row['password'] . SECKEY) === $password) ? TRUE : FALSE;
        }

        return $confirmed;
    }

    public function get_member_renewal($memberId = NULL)
    {
        $this->db->select('company,contactName,contactNumber,address,address2,suburb,postcode,state,emailAddress,emailAddress,username,password,landlordCount');
        $this->db->where(array('id'=>$memberId));
        $result = $this->db->get('members');

        return $result->row_array();
    }

    public function add_update($data = NULL, $memberId = NULL)
    {
        if(empty($memberId)) {
            $this->db->insert('members', $data);
            $memberId = $this->db->insert_id();
        }
        else {
            $this->db->where('id', $memberId);
            $this->db->update('members', $data);
            $memberId = $this->db->affected_rows();
        }

        return $memberId;
    }

    public function add_member_staff($data = NULL)
    {
        $this->db->insert('memberStaff', $data);
    }

    public function add_transaction($data = NULL)
    {
        $this->db->insert('transactions', $data);
        return $this->db->insert_id();
    }

    public function update_transaction($orderId = NULL, $data = NULL)
    {
        if($this->db->limit(1)->get_where('transactions', array('orderId'=>$orderId))->num_rows() === 0) {
            return FALSE;
        }
        else {
            $this->db->where('orderId', $orderId);
            $this->db->update('transactions', $data);            
            return TRUE;
        }
    }

}
