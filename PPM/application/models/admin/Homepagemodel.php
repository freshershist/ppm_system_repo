<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *	@Class Name: usersmodel
 *  @description: all related transaction of users in the database
 *
 */

if(!class_exists('CI_Model')) { class CI_Model extends Model {} }

class Homepagemodel extends CI_Model
{
	public function __construct()
    {
        parent::__construct();
    }

    public function get_list()
    {
        $sql = "SELECT * from homepage order by id asc;";

        return $this->db->query($sql)->result_array();
    
    }

    public function get_page($id = NULL)
    {
        $sql = "SELECT * from homepage where id = {$id};";

        return $this->db->query($sql)->result_array();
    }

    public function add_update_page($id = NULL, $data = NULL)
    {
        if(!empty($data)) {
            if(intval($data['isActive']) === 1) {
                $this->db->where('isActive', 1);
                $this->db->update('homepage', array('isActive'=>0));
            }
        }

        if(!empty($id)) {
            $this->db->where('id', $id);
            $this->db->update('homepage', $data);
        }
        else {
            $this->db->insert('homepage', $data);
            $id = $this->db->insert_id();
        }

        return $id;
    }

    public function delete_page($id = NULL)
    {
        $sql = "DELETE from homepage where id = {$id};";

        $this->db->query($sql);      

    }
}
