<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *	@Class Name: usersmodel
 *  @description: all related transaction of users in the database
 *
 */

if(!class_exists('CI_Model')) { class CI_Model extends Model {} }

class Awardsmodel extends CI_Model
{
	public function __construct()
    {
        parent::__construct();
    }

    public function get_categories() 
    {
        $sql = "SELECT id,name,area from categories where area in (26) order by area,sort;";

        return $this->db->query($sql)->result_array();
    }

    public function get_awards_by_category($categoryid = NULL)
    {
        if(!empty($categoryid)) {
            $sql = "SELECT a.*,c.name as categoryname from awards as a, categories as c where a.category = c.id and c.id = {$categoryid} order by myyear;";
        }
        else {
            $sql = "SELECT a.*,c.name as categoryname from awards as a, categories as c where a.category = c.id order by myyear;";
        }

        return $this->db->query($sql)->result_array();
    
    }

    public function get_award_category($id = NULL)
    {
        $sql = "SELECT * FROM  awards  where id = {$id};";

        return $this->db->query($sql)->result_array();
    }

    public function add_award($data = NULL)
    {
        $this->db->insert('awards', $data);
        return $this->db->insert_id();
    }

    public function update_award($id = NULL, $data = NULL)
    {
        $this->db->where('id', $id);
        $this->db->update('awards', $data);
    }

    public function delete_award($id = NULL)
    {

        $sql = "DELETE from awards where id = {$id};";

        $this->db->query($sql);      

    }
}
