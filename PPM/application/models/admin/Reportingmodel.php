<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *	@Class Name: usersmodel
 *  @description: all related transaction of users in the database
 *
 */

if(!class_exists('CI_Model')) { class CI_Model extends Model {} }

class Reportingmodel extends CI_Model
{
	public function __construct()
    {
        parent::__construct();
    }

    public function get_report1($startDate = NULL, $endDate = NULL)
    {
        $arr = array();
        //Total General Clients (Individual)
        $sql = "SELECT count(id) as count from members where membershipType = 6;";

        $row = $this->db->query($sql)->row_array();

        $arr['general_clients']['total_general_clients_individual'] = intval($row['count']);

        //Total General Clients (Staff email addresses)
        $sql = "SELECT count(members.id) as count from members,memberStaff where memberStaff.memberid = members.id and membershipType = 6 and email != '';";

        $row = $this->db->query($sql)->row_array();

        $arr['general_clients']['total_general_clients_staff'] = intval($row['count']);

        $sql = "SELECT id,name,overrideDropdownValue from categories where area in(21) order by name;";

        $result = $this->db->query($sql)->result_array();

        $states = array();

        foreach ($result as $key => $value) {
            $states[(!empty($value['overrideDropdownValue'])) ? $value['overrideDropdownValue'] : $value['id']] = $value['name'];
        }

        //States
        $sql = "SELECT count(id) as count,state,stateSubRegion from members where membershipType = 6 group by state,stateSubRegion order by state,stateSubRegion";

        $result = $this->db->query($sql)->result_array();

        foreach ($states as $key => $value) {
            foreach ($result as $k => $v) {
                if(intval($v['state']) === intval($key)) {
                    if(!isset($arr['general_clients']['states_data'][$key]['individual']) && intval($key) !== 1) {
                        $arr['general_clients']['states_data'][$key]['individual'][] = array('count'=>$v['count'], 'state'=>$value);
                    }    
                    else {
                        if(intval($key) === 1){
                            $arr['general_clients']['states_data'][$key]['individual'][] = array('count'=>$v['count'], 'subregion'=>$this->dashboard->data_collect['state_subregion'][intval($v['stateSubRegion']) - 1],'state'=>$value);                           
                        }
                    }
                }
            }
        }

        $sql = "SELECT count(memberStaff.id) as count,state,stateSubRegion from members,memberStaff where memberStaff.memberid = members.id and membershipType = 6 and email != '' group by state,stateSubRegion  order by state,stateSubRegion";

        $result = $this->db->query($sql)->result_array();

        foreach ($states as $key => $value) {
            foreach ($result as $k => $v) {
                if(intval($v['state']) === intval($key)) {
                    if(!isset($arr['general_clients']['states_data'][$key]['staff']) && intval($key) !== 1) { 
                        $arr['general_clients']['states_data'][$key]['staff'][] = array('count'=>$v['count'], 'state'=>$value);
                    }
                    else {
                        if(intval($key) === 1){
                            $arr['general_clients']['states_data'][$key]['staff'][] = array('count'=>$v['count'], 'subregion'=>$this->dashboard->data_collect['state_subregion'][intval($v['stateSubRegion']) - 1],'state'=>$value);    
                        }
                    }
                }
            }
        }

        $sql = "SELECT id,name,overrideDropdownValue from categories where area in(20) order by name;";

        $result = $this->db->query($sql)->result_array();

        $membershipTypeDictionary = array();

        foreach ($result as $key => $value) {
            $membershipTypeDictionary[(!empty($value['overrideDropdownValue'])) ? $value['overrideDropdownValue'] : $value['id']] = $value['name'];
        }

        $sql = "SELECT membershipType, count(id) as count from members where membershipType in (8,9,10,11,12) group by membershipType;";

        $result = $this->db->query($sql)->result_array();

        foreach ($membershipTypeDictionary as $key => $value) {
            foreach ($result as $k => $v) {
                if(intval($v['membershipType']) === intval($key)) {
                    $arr['general_clients']['membershiptype_data'][$key]['individual'][] = array('count'=>$v['count'], 'name'=>$value);
                }
            }
        }

        $sql = "SELECT membershipType,count(memberStaff.id) as count from members,memberStaff where memberStaff.memberid = members.id  and membershipType in (8,9,10,11,12) and email != '' group by membershipType";

        $result = $this->db->query($sql)->result_array();

        foreach ($membershipTypeDictionary as $key => $value) {
            foreach ($result as $k => $v) {
                if(intval($v['membershipType']) === intval($key)) {
                    $arr['general_clients']['membershiptype_data'][$key]['staff'][] = array('count'=>$v['count'], 'name'=>$value);
                }
            }
        }

        //Total All Clients (Individual)
        $sql = "SELECT count(id) as count from members where membershipType in (6,8,9,10,11,12);";

        $row = $this->db->query($sql)->row_array();

        $arr['general_clients']['total_all_clients_individual'] = intval($row['count']);

        //Total All Clients (Staff email addresses)
        $sql = "SELECT count(members.id) as count from members,memberStaff where memberStaff.memberid = members.id and membershipType in (6,8,9,10,11,12) and email != '';";               

        $row = $this->db->query($sql)->row_array();

        $arr['general_clients']['total_all_clients_staff'] = intval($row['count']);

        //Total Members (Individual)
        $sql = "select count(id) as count from members WHERE ( membershipType in (0,1,2,3) );";
        $row = $this->db->query($sql)->row_array();

        $arr['members']['total_all_individual'] = intval($row['count']);

        //Total Members (Staff email addresses)
        $sql = "select count(members.id) as count from members,memberStaff where memberStaff.memberid = members.id  and ( membershipType in (0,1,2,3) )  and email != '';";
        $row = $this->db->query($sql)->row_array();

        $arr['members']['total_all_staff'] = intval($row['count']);

        //Current Members (Individual)
        $sql = "select count(id) as count from members WHERE ( memberStatus = 1 and membershipType in (0,1,2,3) );";   
        $row = $this->db->query($sql)->row_array();

        $arr['members']['current_members_individual'] = intval($row['count']);


        //Current Members (Staff email addresses)     
        $sql = "select count(members.id) as count from members,memberStaff where memberStaff.memberid = members.id  and ( memberStatus = 1 and membershipType in (0,1,2,3) )  and email != '';";
        $row = $this->db->query($sql)->row_array();

        $arr['members']['current_members_staff'] = intval($row['count']);

        $sql = "SELECT membershipType,count(id) as count from members where membershipType < 4 group by membershipType;";

        $result = $this->db->query($sql)->result_array();

        foreach ($membershipTypeDictionary as $key => $value) {
            foreach ($result as $k => $v) {
                if(intval($v['membershipType']) === intval($key) OR (intval($v['membershipType']) === 0 && intval($key) === 900)) {
                    $arr['members']['membershiptype_data'][$key]['individual'][] = array('count'=>$v['count'], 'name'=>$value);
                }
            }
        }

        $sql = "SELECT membershipType,count(members.id) as count from members,memberStaff where memberStaff.memberid = members.id  and (membershipType < 4) and email != '' group by membershipType";

        $result = $this->db->query($sql)->result_array();

        foreach ($membershipTypeDictionary as $key => $value) {
            foreach ($result as $k => $v) {
                if(intval($v['membershipType']) === intval($key) OR (intval($v['membershipType']) === 0 && intval($key) === 900)) {
                    $arr['members']['membershiptype_data'][$key]['staff'][] = array('count'=>$v['count'], 'name'=>$value);
                }
            }
        }

        //Total Cancelled Members (Individual)
        $sql = "select count(id) as count from members where membershipType < 4 and memberStatus = 3;";
        $row = $this->db->query($sql)->row_array();

        $arr['members']['total_cancelled_individual'] = intval($row['count']);


        //Total Cancelled Members (Staff email addresses)
        $sql = "select count(members.id) as count from members,memberStaff where memberStaff.memberid = members.id  and membershipType < 4 and email != ''  and memberStatus = 3 ;";
        $row = $this->db->query($sql)->row_array();

        $arr['members']['total_cancelled_staff'] = intval($row['count']);


        //Suspended Members (Individual)
        $sql = "select count(id) as count from members where membershipType < 4 and memberStatus = 2;";
        $row = $this->db->query($sql)->row_array();

        $arr['members']['total_suspended_individual'] = intval($row['count']);

        //Suspended Members (Staff email addresses)
        $sql = "select count(members.id) as count from members,memberStaff where memberStaff.memberid = members.id  and membershipType < 4 and email != ''  and memberStatus = 2 ;";

        $row = $this->db->query($sql)->row_array();

        $arr['members']['total_suspended_staff'] = intval($row['count']);

        $sd_str = (!empty($startDate)) ? ' and membershipCommencementDate >= "' . date('Y-m-d h:i:s', strtotime($startDate)) . '"': '';

        $ed_str = (!empty($endDate)) ? ' and membershipCommencementDate <= "' . date('Y-m-d h:i:s', strtotime($endDate)) . '"' : '';

        //New Member
        $sql = "Select count(id) as count from members where membershipType < 3 and membershipCommencementDate is not null" . $sd_str;

        $row = $this->db->query($sql)->row_array();        

        $arr['members']['total_new'] = intval($row['count']);

        $sd_str = (!empty($startDate)) ? ' and dateCancelled >= "' . date('Y-m-d h:i:s', strtotime($startDate)) . '"': '';

        $ed_str = (!empty($endDate)) ? ' and dateCancelled <= "' . date('Y-m-d h:i:s', strtotime($endDate)) . '"': '';

        //Cancelled Members
        $sql = "Select count(id) as count from members where membershipType < 3 and dateCancelled is not null " . $sd_str;

        $row = $this->db->query($sql)->row_array();        

        $arr['members']['total_cancelled'] = intval($row['count']);

        //Subscribers
        $subscribers = array('newsletter'=>'Newsletter','online'=>'On-Line Training');

        $memberType = 0;
        $paymentType = 0;

        $sdate = date('Y-m-d 00:00:00', strtotime($startDate));
        $eDate = (!empty($endDate)) ? date('Y-m-d 00:00:00', strtotime($endDate)) : date('Y-m-d 00:00:00', now());

        foreach ($subscribers as $key => $value) {
            if($key === 'newsletter') {
                $memberType = 4;
                $paymentType = 1;

                //Approx No. of Landlords to Receive
                $sql = "SELECT sum(COALESCE(landlordCount,0)) as count from members where membershipType = {$memberType} and membershipExpires >= NOW() and memberStatus = 1;";

                $row = $this->db->query($sql)->row_array();

                $arr['subscribers'][$key]['approx'] = intval($row['count']);
                $arr['subscribers'][$key]['label'] = $value;
            }
            else {
                $memberType = 5;
                $paymentType = 2;
                $arr['subscribers'][$key]['label'] = $value;
            }

            //Current
            $sql = "SELECT count(id) as count from members where membershipType = {$memberType} and membershipExpires >= NOW() and memberStatus = 1;";

            $row = $this->db->query($sql)->row_array();

            $arr['subscribers'][$key]['current'] = intval($row['count']);

            //Unsubscribed
            $sql = "SELECT count(id) as count from members where membershipType = {$memberType}  and dateUnsubscribed >= '" . date('Y-m-d h:i:s', strtotime($startDate)) . "' and dateUnsubscribed <= '" . $eDate . "';";

            $row = $this->db->query($sql)->row_array();

            $arr['subscribers'][$key]['unsubscribed_notes'] = 'Membership Unsubscribed between ' . $startDate . ' and '. $this->ppmsystemlib->check_date_time($eDate, 'd/m/Y');
            $arr['subscribers'][$key]['unsubscribed'] = intval($row['count']);

            //New
            $sql = "SELECT count(id) as count from members as m, transactions as t where m.id = t.memberId and paymentStatus = 1 and memberId in (select memberId from transactions where paymentStatus = 1 and paymentType = {$paymentType} group by memberId having count(memberId) = 1)  and myDateTime > '" . date('Y-m-d h:i:s', strtotime($startDate)) . "'  and myDateTime < '" . $eDate . "'";
            
            $row = $this->db->query($sql)->row_array();

            $arr['subscribers'][$key]['new'] = intval($row['count']);
            $arr['subscribers'][$key]['new_notes'] = 'All subscribers who have only been issued one invoice.  This invoice was issued between ' . $startDate . ' and '. $this->ppmsystemlib->check_date_time($eDate, 'd/m/Y');

            //Renewals
            $sql = "SELECT count(id) as count from members as m, transactions as t where m.id = t.memberId and paymentStatus = 1 and memberId in (select memberId from transactions where paymentStatus = 1 and paymentType = {$paymentType} group by memberId having count(memberId) > 1)  and myDateTime > '" . date('Y-m-d h:i:s', strtotime($startDate)) . "'  and myDateTime < '" . $eDate . "'";

            $row = $this->db->query($sql)->row_array();

            $arr['subscribers'][$key]['renewal'] = intval($row['count']);
            $arr['subscribers'][$key]['renewal_notes'] = 'All subscribers who have been issued more then 1 invoice.  However one invoice was issued between ' . $startDate . ' and '. $this->ppmsystemlib->check_date_time($eDate, 'd/m/Y');

            //Double Ups
            $sql = "SELECT count(id) as count from members where membershipType = {$memberType} and memberStatus = 4;";

            $row = $this->db->query($sql)->row_array();

            $arr['subscribers'][$key]['doubleups']          = intval($row['count']);
            $arr['subscribers'][$key]['doubleups_notes']    = 'Membership status set to Double Up. Note date filter does not affect this figure';

            $sql_days = '';

            for ($i=0; $i <= 3; $i++) {
                if($i === 3) $arr['subscribers'][$key]['expired'][$i]['days'] = '91+';
                elseif($i === 0) $arr['subscribers'][$key]['expired'][$i]['days'] = '0 - ' . ($i + 1)*30;
                else $arr['subscribers'][$key]['expired'][$i]['days'] = ($i * 30 + 1) . ' - ' . ($i + 1)*30;

                $date=date_create($startDate);

                if($i === 3) {
                    date_add($date,date_interval_create_from_date_string($i * -30 . " days"));

                    $sql_days = " and membershipExpires < '" . date_format($date, "Y-m-d") . "' ";
                }
                else {
                    $date2=date_create($startDate);

                    date_add($date,date_interval_create_from_date_string($i * -30 . " days"));
                    date_add($date2,date_interval_create_from_date_string(($i + 1) * -30 . " days"));

                    $sql_days = " and membershipExpires < '" . date_format($date, "Y-m-d") . "' and membershipExpires >= '" . date_format($date2, "Y-m-d") . "' ";
                }

                $sql = "SELECT id,max(amount) as max from members LEFT JOIN transactions on members.id = transactions.memberid where membershipType = {$memberType} and memberStatus = 1 " . $sql_days . " group by id;";

                $result = $this->db->query($sql)->result_array();

                $total      = 0;
                $total_max  = 0;

                foreach ($result as $k => $v) {
                    $total++;
                    $total_max += doubleval($v['max']);
                }

                $arr['subscribers'][$key]['expired'][$i]['total']   = $total;
                $arr['subscribers'][$key]['expired'][$i]['ex_gst']  = number_format(round(($total_max*10/11), 2), 2);
                $arr['subscribers'][$key]['expired'][$i]['inc_gst'] = number_format(round($total_max, 2), 2);
                $arr['subscribers'][$key]['expired'][$i]['notes']   = 'The Ex GST and Inc GST is calculated, by adding up the maximum amount these members have paid for this service. If no payment has been entered for a particular member, then the system assumes this member receives this service for free';

            }
        }

        $sql = "SELECT count(id) as count from members where membershipType = 6 and dateRemoved between '{$sdate}' and '{$eDate}';";

        $row = $this->db->query($sql)->row_array();

        $arr['unsubscribe_request']['general_clients']['count'] = intval($row['count']);
        $arr['unsubscribe_request']['general_clients']['notes']   = 'General Client removed between ' . $startDate . ' and '. $this->ppmsystemlib->check_date_time($eDate, 'd/m/Y');

        $sql = "SELECT count(id) as count from members where membershipType = 4 and dateUnsubscribed between '{$sdate}' and '{$eDate}';";

        $row = $this->db->query($sql)->row_array();

        $arr['unsubscribe_request']['newsletter']['count'] = intval($row['count']);
        $arr['unsubscribe_request']['newsletter']['notes']   = 'Newsletter Subscriber unsubscribed between ' . $startDate . ' and '. $this->ppmsystemlib->check_date_time($eDate, 'd/m/Y');

        $sql = "SELECT count(id) as count from members where membershipType = 5 and dateUnsubscribed between '{$sdate}' and '{$eDate}';";

        $row = $this->db->query($sql)->row_array();

        $arr['unsubscribe_request']['online']['count'] = intval($row['count']); 
        $arr['unsubscribe_request']['online']['notes']   = 'On-Line Training Subscribers unsubscribed between ' . $startDate . ' and '. $this->ppmsystemlib->check_date_time($eDate, 'd/m/Y');

        $sql = "SELECT us.username as username,(SELECT count(id) as count from members where lastUpdatedDate is not null and lastUpdatedUser = us.id and lastUpdatedDate between '{$sdate}' and '{$eDate}') as count FROM users as us";

        $result = $this->db->query($sql)->result_array();

        foreach ($result as $key => $value) {
            $arr['db_updates']['user'][$key]['username']    = $value['username'];
            $arr['db_updates']['user'][$key]['count']       = $value['count'];
        }

        $arr['db_updates']['notes']   = 'Contact cards marked as updated between ' . $startDate . ' and '. $this->ppmsystemlib->check_date_time($eDate, 'd/m/Y');


        return $arr;       
    }

    public function get_report2($user = NULL, $typeOfInterest = NULL, $startDate = NULL, $endDate = NULL)
    {

        $startDate = (!empty($startDate)) ? date('Y-m-d 00:00:00', strtotime($startDate)) : date('Y-m-d 00:00:00', strtotime('1/1/1900'));
        $endDate = (!empty($endDate)) ? date('Y-m-d 00:00:00', strtotime($endDate)) : date('Y-m-d 00:00:00', strtotime('1/1/2500'));

        $sql = "select id,company,contactName,contactNumber,memberStatus,systemEnquiryDate,systemEnquiryUser,systemPresentationDate,systemPresentationUser,trainingInterestDate,trainingInterestUser,productInterestDate,productInterestUser,conferenceInterestDate,conferenceInterestUser,onlineTrainingInterestDate,onlineTrainingInterestUser from members where membershipType = 6";

        if(!empty($typeOfInterest) && is_numeric($typeOfInterest)) {
            $dateField = '';
            $userField = '';

            switch ($typeOfInterest) {
                case 1:
                    $dateField = "systemEnquiryDate";
                    $userField = "systemEnquiryUser";
                    break;
                case 2:
                    $dateField = "systemPresentationDate";
                    $userField = "systemPresentationUser";
                    break;
                case 3:
                    $dateField = "trainingInterestDate";
                    $userField = "trainingInterestUser";
                    break;
                case 4:
                    $dateField = "productInterestDate";
                    $userField = "productInterestUser";
                    break;

                case 5:
                    $dateField = "conferenceInterestDate";
                    $userField = "conferenceInterestUser";
                    break;
                    
                case 6:
                    $dateField = "onlineTrainingInterestDate";
                    $userField = "onlineTrainingInterestUser";
                    break;                                
                default:
                    # code...
                    break;
            }

            $typeOfInterestDesc = $this->ppmsystemlib->get_data_arr('typeOfInterestArray')[$typeOfInterest - 1];

            $sql .= " and " . $dateField . " between '" . $startDate . "' and '" . $endDate . "'";

            if(!empty($user)) {
                $sql .= " and " . $userField . " = " . $user;
            }

            if($typeOfInterest === 1) $sql .= " and systemPresentationDate is Null";

        }
        else {
            if(!empty($user)) {
                $sql .= " and ((systemEnquiryDate between '" . $startDate . "' and '" . $endDate . "' and systemEnquiryUser = " . $user . ") or (systemPresentationDate between '" . $startDate . "' and '" . $endDate . "' and systemPresentationUser = " . $user . ") or (trainingInterestDate between '" . $startDate . "' and '" . $endDate . "' and trainingInterestUser = " . $user . ") or (productInterestDate between '" . $startDate . "' and '" . $endDate . "' and productInterestUser = " . $user . ") or (conferenceInterestDate between '" . $startDate  . "' and '" . $endDate . "' and conferenceInterestUser = " . $user . ") or (onlineTrainingInterestDate between '" . $startDate . "' and '" . $endDate . "' and onlineTrainingInterestUser = " . $user . "))";
            }
            else {
                $sql .= " and ((systemEnquiryDate between '" . $startDate . "' and '" . $endDate . "') or (systemPresentationDate between '" . $startDate . "' and '" . $endDate . "') or (trainingInterestDate between '" . $startDate . "' and '" . $endDate . "') or (productInterestDate between '" . $startDate . "' and '" . $endDate . "') or (conferenceInterestDate between '" . $startDate . "' and '" . $endDate . "') or (onlineTrainingInterestDate between '" . $startDate . "' and '" . $endDate . "'))";
            }
        }

        if(!empty($user) && empty($typeOfInterest)) {
            $sql .= " and (systemEnquiryUser = " . $user . " or systemPresentationUser  = " . $user . "  or trainingInterestUser  = " . $user . " or productInterestUser  = " . $user . " or conferenceInterestUser  = " . $user . " or onlineTrainingInterestUser  = " . $user . ")";
        }

        return $this->db->query($sql)->result_array();
    }

    public function get_report3()
    {
        $members = array('general_client'=>'General Clients','newsletter'=>'Newsletter Subscribers','online'=>'On-Line Training Subscribers');

        $arr = array();

        foreach ($members as $key => $value) {
            $membershipType = NULL;

            if($key === 'general_client') {
                $membershipType = 6;
            }
            elseif($key === 'newsletter') {
                $membershipType = 4;
            }
            elseif($key === 'online') {
                $membershipType = 5;
            }

            $sql = "SELECT howdidtheyhear,count(id) as count from members where membershipType = {$membershipType} group by howdidtheyhear order by count desc;";

            $result = $this->db->query($sql)->result_array();

            $last_key = NULL;

            $arr['howdidtheyhear'][$key]['title'] = $value;

            foreach ($result as $k => $v) {

                if(empty($v['howdidtheyhear'])) {
                    if($last_key === NULL) {
                        $last_key = $k;
                        $arr['howdidtheyhear'][$key]['data'][$k]['title']   = 'Not Applied';
                        $arr['howdidtheyhear'][$key]['data'][$k]['count']   = intval($v['count']);
                    }
                    elseif($last_key != $k) {
                        $arr['howdidtheyhear'][$key]['data'][$last_key]['count']   += intval($v['count']);
                    }
                    
                }
                else {
                    $arr['howdidtheyhear'][$key]['data'][$k]['title']   = $v['howdidtheyhear'];
                    $arr['howdidtheyhear'][$key]['data'][$k]['count']   = intval($v['count']);
                }
            }
        }

        return $arr;
    }

    public function get_report4($startDate = NULL, $endDate = NULL)
    {
        $members        = array('general_client'=>'General Clients','newsletter'=>'Newsletter Subscribers','online'=>'On-Line Training Subscribers');

        $membershipType = 0;
        $searchfield    = '';
        $reportfield    = '';

        $sdate = (!empty($startDate)) ? date('Y-m-d 00:00:00', strtotime($startDate)) : date('Y-m-d 00:00:00', strtotime('1/1/1900'));
        $eDate = (!empty($endDate)) ? date('Y-m-d 00:00:00', strtotime($endDate)) : date('Y-m-d 00:00:00', strtotime('1/1/2500'));

        $arr = array();

        foreach ($members as $key => $value) {
            $arr[$key]['title'] = $value;

            if($key === 'general_client') {
                $membershipType = 6;
                $searchfield    = "dateRemoved";
                $reportfield    = "removedReason";
                $totallabel     = "Total Removed";
            }
            elseif($key === 'newsletter') {
                $membershipType = 4;
                $searchfield    = "dateUnsubscribed";
                $reportfield    = "unsubscribeReason";
                $totallabel     = "Total Unsubscribed";
            }
            elseif($key === 'online') {
                $membershipType = 5;
                $searchfield    = "dateUnsubscribed";
                $reportfield    = "unsubscribeReason"; 
                $totallabel     = "Total Unsubscribed";
            }

            $sql = "select count(id) as count from members where membershipType = {$membershipType} and {$searchfield} between '{$sdate}' and '{$eDate}';";

            $arr[$key]['total']['total_title'] = $totallabel;
            $arr[$key]['total']['total_count'] = $this->db->query($sql)->row_array()['count'];

            $sql = "SELECT count(id) as count,state,stateSubRegion from members where membershipType = {$membershipType} and {$searchfield} between '{$sdate}' and '{$eDate}' group by state,stateSubRegion order by state,stateSubRegion";

            $arr[$key]['states'] = $this->db->query($sql)->result_array();

            $sql = "SELECT {$reportfield} as reason,count(id) as count from members where membershipType = {$membershipType} and {$searchfield}  between '{$sdate}' and '{$eDate}' group by {$reportfield} order by {$reportfield};";

            $arr[$key]['reason'] = $this->db->query($sql)->result_array();
        }

        return $arr;
    }

    public function get_report5($memberType = NULL, $memberStatus = NULL)
    {
        $sql = "select id,company,membershipType,state,
                    case(membershipType) 
                    when 0 then 'Platinum'
                    when 1 then 'Gold'
                    when 2 then 'Silver'
                    end as membershipTypeName,
                    (select name from categories CT where CT.area = 21 and (CT.overrideDropdownValue is not null and CT.overrideDropdownValue = members.state or CT.overrideDropdownValue is null and CT.id = members.state) ) as stateName,
                    case(state)
                    when 0 then (select count(id) from downloads where mysection = 2 and (members.memberShipType = 0 and downloads.Platnium = 1 or members.memberShipType = 1 and downloads.Gold = 1 or members.memberShipType = 2 and downloads.Silver = 1 or members.memberShipType = 3 and downloads.Bronze = 1 ) and downloads.id in (select id from category where categoryId = 111 and area = 'd') and exists (select * from downloadLog where downloadLog.downloadId = downloads.Id and memberId = members.id))
                    when 2 then (select count(id) from downloads where mysection = 2 and (members.memberShipType = 0 and downloads.Platnium = 1 or members.memberShipType = 1 and downloads.Gold = 1 or members.memberShipType = 2 and downloads.Silver = 1 or members.memberShipType = 3 and downloads.Bronze = 1 ) and downloads.id in (select id from category where categoryId = 106 and area = 'd') and exists (select * from downloadLog where downloadLog.downloadId = downloads.Id and memberId = members.id))
                    when 3 then (select count(id) from downloads where mysection = 2 and (members.memberShipType = 0 and downloads.Platnium = 1 or members.memberShipType = 1 and downloads.Gold = 1 or members.memberShipType = 2 and downloads.Silver = 1 or members.memberShipType = 3 and downloads.Bronze = 1 ) and downloads.id in (select id from category where categoryId = 104 and area = 'd') and exists (select * from downloadLog where downloadLog.downloadId = downloads.Id and memberId = members.id))
                    when 4 then (select count(id) from downloads where mysection = 2 and (members.memberShipType = 0 and downloads.Platnium = 1 or members.memberShipType = 1 and downloads.Gold = 1 or members.memberShipType = 2 and downloads.Silver = 1 or members.memberShipType = 3 and downloads.Bronze = 1 ) and downloads.id in (select id from category where categoryId = 103 and area = 'd') and exists (select * from downloadLog where downloadLog.downloadId = downloads.Id and memberId = members.id))
                    when 5 then (select count(id) from downloads where mysection = 2 and (members.memberShipType = 0 and downloads.Platnium = 1 or members.memberShipType = 1 and downloads.Gold = 1 or members.memberShipType = 2 and downloads.Silver = 1 or members.memberShipType = 3 and downloads.Bronze = 1 ) and downloads.id in (select id from category where categoryId = 110 and area = 'd') and exists (select * from downloadLog where downloadLog.downloadId = downloads.Id and memberId = members.id))
                    when 6 then (select count(id) from downloads where mysection = 2 and (members.memberShipType = 0 and downloads.Platnium = 1 or members.memberShipType = 1 and downloads.Gold = 1 or members.memberShipType = 2 and downloads.Silver = 1 or members.memberShipType = 3 and downloads.Bronze = 1 ) and downloads.id in (select id from category where categoryId = 109 and area = 'd') and exists (select * from downloadLog where downloadLog.downloadId = downloads.Id and memberId = members.id))
                    when 7 then (select count(id) from downloads where mysection = 2 and (members.memberShipType = 0 and downloads.Platnium = 1 or members.memberShipType = 1 and downloads.Gold = 1 or members.memberShipType = 2 and downloads.Silver = 1 or members.memberShipType = 3 and downloads.Bronze = 1 ) and downloads.id in (select id from category where categoryId = 108 and area = 'd') and exists (select * from downloadLog where downloadLog.downloadId = downloads.Id and memberId = members.id))
                    when 8 then (select count(*) from downloads where mysection = 2 and (members.memberShipType = 0 and downloads.Platnium = 1 or members.memberShipType = 1 and downloads.Gold = 1 or members.memberShipType = 2 and downloads.Silver = 1 or members.memberShipType = 3 and downloads.Bronze = 1 ) and downloads.id in (select id from category where categoryId = 107 and area = 'd') and exists (select * from downloadLog where downloadLog.downloadId = downloads.Id and memberId = members.id))
                    else        (select count(id) from downloads where mysection = 2 and (members.memberShipType = 0 and downloads.Platnium = 1 or members.memberShipType = 1 and downloads.Gold = 1 or members.memberShipType = 2 and downloads.Silver = 1 or members.memberShipType = 3 and downloads.Bronze = 1 ) and exists (select * from downloadLog where downloadLog.downloadId = downloads.Id and memberId = members.id))
                    end as downloadCount,
                    case(state) 
                    when 0 then (select count(id) from downloads where mysection = 2 and (members.memberShipType = 0 and downloads.Platnium = 1 or members.memberShipType = 1 and downloads.Gold = 1 or members.memberShipType = 2 and downloads.Silver = 1 or members.memberShipType = 3 and downloads.Bronze = 1 ) and downloads.id in (select id from category where categoryId = 111 and area = 'd'))
                    when 2 then (select count(id) from downloads where mysection = 2 and (members.memberShipType = 0 and downloads.Platnium = 1 or members.memberShipType = 1 and downloads.Gold = 1 or members.memberShipType = 2 and downloads.Silver = 1 or members.memberShipType = 3 and downloads.Bronze = 1 ) and downloads.id in (select id from category where categoryId = 106 and area = 'd'))
                    when 3 then (select count(id) from downloads where mysection = 2 and (members.memberShipType = 0 and downloads.Platnium = 1 or members.memberShipType = 1 and downloads.Gold = 1 or members.memberShipType = 2 and downloads.Silver = 1 or members.memberShipType = 3 and downloads.Bronze = 1 ) and downloads.id in (select id from category where categoryId = 104 and area = 'd'))
                    when 4 then (select count(id) from downloads where mysection = 2 and (members.memberShipType = 0 and downloads.Platnium = 1 or members.memberShipType = 1 and downloads.Gold = 1 or members.memberShipType = 2 and downloads.Silver = 1 or members.memberShipType = 3 and downloads.Bronze = 1 ) and downloads.id in (select id from category where categoryId = 103 and area = 'd'))
                    when 5 then (select count(id) from downloads where mysection = 2 and (members.memberShipType = 0 and downloads.Platnium = 1 or members.memberShipType = 1 and downloads.Gold = 1 or members.memberShipType = 2 and downloads.Silver = 1 or members.memberShipType = 3 and downloads.Bronze = 1 ) and downloads.id in (select id from category where categoryId = 110 and area = 'd'))
                    when 6 then (select count(id) from downloads where mysection = 2 and (members.memberShipType = 0 and downloads.Platnium = 1 or members.memberShipType = 1 and downloads.Gold = 1 or members.memberShipType = 2 and downloads.Silver = 1 or members.memberShipType = 3 and downloads.Bronze = 1 ) and downloads.id in (select id from category where categoryId = 109 and area = 'd'))
                    when 7 then (select count(id) from downloads where mysection = 2 and (members.memberShipType = 0 and downloads.Platnium = 1 or members.memberShipType = 1 and downloads.Gold = 1 or members.memberShipType = 2 and downloads.Silver = 1 or members.memberShipType = 3 and downloads.Bronze = 1 ) and downloads.id in (select id from category where categoryId = 108 and area = 'd'))
                    when 8 then (select count(id) from downloads where mysection = 2 and (members.memberShipType = 0 and downloads.Platnium = 1 or members.memberShipType = 1 and downloads.Gold = 1 or members.memberShipType = 2 and downloads.Silver = 1 or members.memberShipType = 3 and downloads.Bronze = 1 ) and downloads.id in (select id from category where categoryId = 107 and area = 'd'))
                    else        (select count(id) from downloads where mysection = 2 and (members.memberShipType = 0 and downloads.Platnium = 1 or members.memberShipType = 1 and downloads.Gold = 1 or members.memberShipType = 2 and downloads.Silver = 1 or members.memberShipType = 3 and downloads.Bronze = 1 ))
                    end as totalDownloadCount,
                    (SELECT max(myDateTime) from log where description = concat('Member ' ,members.id, ' logged in ok')) as lastLogin,(SELECT count(id) from log where description = concat('Member ' ,members.id, ' logged in ok')) as totalLogins from members where";

        switch ($memberType) {
            case 'All':
                $sql .= ' membershipType < 3';
                break;

            case 'Platinum':
                $sql .= ' membershipType = 0';
                break;
                
            case 'Gold':
                $sql .= ' membershipType in (1)';
                break;

            case 'Silver':
                $sql .= ' membershipType in (2)';
                break;               
            
            default:
                # code...
                break;
        }

        $sql .= ' and memberStatus = ' . $memberStatus;

        return $this->db->query($sql)->result_array();    
    }

    public function get_report6($startDate = NULL, $endDate = NULL, $reportunsubscribereason = NULL, $emailqueueid = NULL)
    {
        $sql = '';

        if(is_numeric($reportunsubscribereason) && intval($reportunsubscribereason) === -1) {
            $sql = "select unsubscribereason,count(id) as count from unsubscribers";

            $conj = ' where ';

            if(!empty($startDate)) {
                $sql .= $conj . " and mydate >= '" . date('Y-m-d 00:00:00', strtotime($startDate)) . "' ";
                $conj = ' and ';
            }

            if(!empty($endDate)) {
                $sql .= $conj . " and mydate <= '" . date('Y-m-d 00:00:00', strtotime($endDate)) . "' ";
                $conj = ' and ';
            }

            $sql .= " group by unsubscribereason order by count desc";

        }
        else {
            $sql = "select email,u.memberid,mydate,u.unsubscribereason,comment,m.company,u.name from unsubscribers as u, members as m where u.memberid = m.id";

            $sql .= (!empty($startDate)) ? " and mydate >= '" . date('Y-m-d 00:00:00', strtotime($startDate)) . "' " : '';
            $sql .= (!empty($endDate)) ? " and mydate <= '" . date('Y-m-d 00:00:00', strtotime($endDate)) . "' " : '';
            //$sql .= (!empty($emailqueueid) && is_numeric($emailqueueid)) ? " and emailqueueid = " . $emailqueueid : '';
            $sql .= (!empty($reportunsubscribereason) && is_numeric($reportunsubscribereason)) ? " and u.unsubscribereason = " . $reportunsubscribereason : '';
        }

        return $this->db->query($sql)->result_array();
    }   
}