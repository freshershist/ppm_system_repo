<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *	@Class Name: usersmodel
 *  @description: all related transaction of users in the database
 *
 */

if(!class_exists('CI_Model')) { class CI_Model extends Model {} }

class Promocodesmodel extends CI_Model
{
	public function __construct()
    {
        parent::__construct();
    }

    public function get_promos()
    {
        $sql = "SELECT * from promocodes order by code;";

        return $this->db->query($sql)->result_array();
    
    }

    public function get_promo($id = NULL)
    {
        $sql = "SELECT * from promocodes where id = {$id};";

        return $this->db->query($sql)->result_array();
    }

    public function add_update_promo($id = NULL, $data = NULL)
    {
        if(!empty($id)) {
            $this->db->where('id', $id);
            $this->db->update('promocodes', $data);
        }
        else {
            $this->db->insert('promocodes', $data);
            $id = $this->db->insert_id();
        }

        return $id;
    }

    public function delete_promo($id = NULL)
    {
        $sql = "DELETE from promocodes where id = {$id};";

        $this->db->query($sql);      

    }
}
