<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *	@Class Name: usersmodel
 *  @description: all related transaction of users in the database
 *
 */

if(!class_exists('CI_Model')) { class CI_Model extends Model {} }

class Eventsmodel extends CI_Model
{
	public function __construct()
    {
        parent::__construct();

        $this->data_collect = array();
    }

    public function get_pages ()
    {
		$query = "select * from `ppms_pages`";
    // $query = "select * from `users`";

		//echo json_encode(array($query));die();

		//return array('query'=>$query, 'result'=> $this->db->query($query)->result_array());

		return array('result'=> $this->db->query($query)->result_array());
    }

    public function get_page($page_id){
        $query = "select * from `ppms_pages` where `pid` = '".$page_id."'";
        // print_r($this->db->query($query)->result_array());
        return array('result'=> $this->db->query($query)->result_array());
    }

    public function update_menu(){
        // SELECT * FROM `ppms_pages` WHERE `pid`, `title`, `content`, `parent_id`, `menu`, `page_order`, `user_id`, `banner_top`, `banner_bottom`, `status`, `date_added`

        $query = "select * from `ppms_pages`";

        //echo json_encode(array($query));die();

        //return array('query'=>$query, 'result'=> $this->db->query($query)->result_array());

        return array('result'=> $this->db->query($query)->result_array());
    }

    // public function save_page($page_id, $txtPageTitle, $editor_1, $pageStatus, $txtOrder, $selParentPage, $banner_top, $banner_bottom){
    //     // return $page_id;
    //     if(!empty($page_id)){
    //         //update
    //         $query = "update `ppms_pages` set 
    //             `title` = '".$txtPageTitle."', 
    //             `content` = '".$editor_1."', 
    //             `parent_id` = '".$selParentPage."', 
    //             `menu` = '', 
    //             `page_order` = '".$txtOrder."', 
    //             `user_id` = '', 
    //             `banner_top` = '".$banner_top."', 
    //             `banner_bottom` = '".$banner_bottom."',  
    //             `status` = '".$pageStatus."'
    //             where 
    //             `pid` = '".$page_id."'
    //         "; 
    //         $this->db->query($query);
    //         $status = 'updated';
    //     }else{
    //         //insert
    //         $query = "insert into `ppms_pages` set 
    //             `title` = '".$txtPageTitle."', 
    //             `content` = '".$editor_1."', 
    //             `parent_id` = '".$selParentPage."', 
    //             `menu` = '', 
    //             `page_order` = '".$txtOrder."', 
    //             `user_id` = '', 
    //             `banner_top` = '".$banner_top."', 
    //             `banner_bottom` = '".$banner_bottom."', 
    //             `status` = '".$pageStatus."', 
    //             `date_added` = '".date('Y-m-d H:i:s')."'
    //         "; 
    //         $this->db->query($query);
    //         $status = 'inserted';
    //     }

    //     return $status;
    //     // return $page_id.' : '.$txtPageTitle.' : '.$editor_1.' : '.$pageStatus.' : '.$txtOrder;
    // }

    public function save_event(){
        // events
        // `id`, `startDate`, `endDate`, `myTime`, `location2`, `venue`, `code`, `name`, `shortdesc`, `body`, `numOfPlaces`, `price1`, `price2`, `price3`, `price4`, `price5`, `whoShouldAttend`, `included`, `relatedProducts`, `relatedGalleries`, `d1`, `d2`, `d3`, `status`, `earlyBirdPrice`, `earlyBirdCutOff`, `d4`, `d5`, `d6`, `d7`, `d8`, `d9`, `d10`, `d11`, `d12`, `d13`, `venueBody`, `travelBody`, `socialBody`, `d14`, `d15`, `d16`, `d17`, `d18`, `sponsorshipOpBody`, `d19`, `d20`, `d21`, `d22`, `d23`, `email`, `onlineEvent`, `cocktailPartyCost`, `trxTime`, `trxNotes`, `d24`, `d25`, `d26`, `d27`, `c4`, `c5`, `c6`, `c7`, `c8`, `c9`, `c10`, `c11`, `c12`, `c13`, `c14`, `c15`, `c16`, `c17`, `c18`, `c19`, `c20`, `c21`, `c22`, `c23`, `c24`, `c25`, `c26`, `c27`

        // eventcosts
        //`eventId`, `cost`, `name`, `limitToMemberType`

        //`eventbreakoutsessions`
        //`breakoutId`, `eventId`, `name`, `session1`, `session2`, `orderby`

        // `eventspeakers` 
        // `name`, `description`, `orderBy`, `photo`, `id`, `eventId`

        // `eventsponsors` 
        // `name`, `description`, `orderBy`, `photo`, `id`, `eventId`      
        $inpCat = $this->input->post('inpCat');
        $inpEventCode = $this->input->post('inpEventCode');
        $inpEventLoc = $this->input->post('inpEventLoc');
        $startDate = $this->input->post('startDate').' 00:00:00';
        $inpTime = $this->input->post('inpTime');
        $placesLeft = $this->input->post('placesLeft');
        $inpEventName = $this->input->post('inpEventName');
        $inpRegion = $this->input->post('inpRegion');
        $endDate = $this->input->post('endDate');
        if(empty($endDate)){
            $endDate = $startDate;
        }else{
            $endDate = $this->input->post('endDate').' 00:00:00';
        }
        $txtVenue = $this->input->post('txtVenue');
        $txtShortDesc = $this->input->post('txtShortDesc');
        $txtAttendees = $this->input->post('txtAttendees');
        $txtRelatedProd = $this->input->post('txtRelatedProd');
        if(!empty($txtRelatedProd)){
            $txtRelatedProd = implode(',', $txtRelatedProd);    
        }

        $txtDesc = $this->input->post('txtDesc');
        $txtIncluded = $this->input->post('txtIncluded');
        $txtRelatedGall = $this->input->post('txtRelatedGall');
        if(!empty($txtRelatedGall)){
            $txtRelatedGall = implode(',', $txtRelatedGall);    
        }
        
        $inpIsOnline = $this->input->post('inpIsOnline');
        if(empty($inpIsOnline)){
            $inpIsOnline = '0';
        }else{
            $inpIsOnline = '1';
        }

        $inpCocktailCost = $this->input->post('inpCocktailCost');

        $event_pricing = $this->input->post('event_pricing'); // ARRAY
        $speakers = $this->input->post('speakers'); // ARRAY
        // echo '<pre>';
        // print_r($speakers);
        // echo '</pre>';
        $breakout = $this->input->post('breakout'); // ARRAY
        $confSponsors = $this->input->post('confSponsors'); // ARRAY
        $txtConferenceSponsorShip = $this->input->post('txtConferenceSponsorShip');
        $genEvents = $this->input->post('genEvents'); // ARRAY


        $confVenue = $this->input->post('confVenue'); // ARRAY
        $confTravel = $this->input->post('confTravel'); // ARRAY
        $confSocial = $this->input->post('confSocial'); // ARRAY
        $confSponsorship = $this->input->post('confSocial'); // ARRAY

        $post = $this->input->post();
        // print_r($post);
        $sql1 = "insert into `events` set 
                 `startDate` = '".$startDate."', 
                 `endDate` = '".$endDate."', 
                 `myTime` = '".$inpTime."', 
                 `location2` = '".$inpEventLoc."', 
                 `venue` = '".$txtVenue."', 
                 `code` = '".$inpEventCode."', 
                 `name` = '".$inpEventName."', 
                 `shortdesc`  = '".$txtShortDesc."', 
                 `body`  = '".$txtDesc."', 
                 `numOfPlaces` = '".$placesLeft."', 
                 `price1` = '', 
                 `price2` = '', 
                 `price3` = '', 
                 `price4` = '', 
                 `price5` = '', 
                 `whoShouldAttend` = '".$txtAttendees."', 
                 `included` = '".$txtIncluded."', 
                 `relatedProducts`  = '".$txtRelatedProd."', 
                 `relatedGalleries` = '".$txtRelatedGall."', 
                 `d1` = '".$genEvents[0]."', 
                 `d2` = '".$genEvents[1]."', 
                 `d3` = '".$genEvents[2]."', 
                 `status` = '', 
                 `earlyBirdPrice` = '', 
                 `earlyBirdCutOff` = '', 
                 `d4` = '".$confVenue[1]."', 
                 `d5` = '".$confVenue[3]."',  
                 `d6` = '".$confVenue[5]."', 
                 `d7` = '".$confVenue[7]."', 
                 `d8` = '".$confVenue[9]."', 
                 `d9` = '".$confVenue[11]."', 
                 `d10` = '".$confTravel[1]."', 
                 `d11` = '".$confTravel[3]."', 
                 `d12` = '".$confTravel[5]."', 
                 `d13` = '".$confTravel[7]."', 
                 `venueBody` = '".$txtVenue."',  
                 `travelBody` = '', 
                 `socialBody` = '', 
                 `d14` = '".$confTravel[9]."', 
                 `d15` = '".$confTravel[11]."', 
                 `d16` = '".$confSocial[1]."', 
                 `d17` = '".$confSocial[3]."', 
                 `d18` = '".$confSocial[5]."', 
                 `sponsorshipOpBody` = '".$txtConferenceSponsorShip."', 
                 `d19` = '".$confSocial[7]."', 
                 `d20` = '".$confSocial[9]."', 
                 `d21` = '".$confSocial[11]."', 
                 `d22` = '".$confSponsorship[1]."', 
                 `d23` = '".$confSponsorship[3]."', 
                 `email` = '', 
                 `onlineEvent` = '".$inpIsOnline."',  
                 `cocktailPartyCost` = '".$inpCocktailCost."',  
                 `trxTime` = '', 
                 `trxNotes` = '', 
                 `d24` = '".$confSponsorship[5]."', 
                 `d25` = '".$confSponsorship[7]."', 
                 `d26` = '".$confSponsorship[9]."', 
                 `d27` = '".$confSponsorship[11]."', 
                 `c4` = '".$confVenue[0]."',  
                 `c5` = '".$confVenue[2]."', 
                 `c6` = '".$confVenue[4]."', 
                 `c7` = '".$confVenue[6]."',  
                 `c8` = '".$confVenue[8]."', 
                 `c9` = '".$confVenue[10]."', 
                 `c10` = '".$confTravel[0]."', 
                 `c11` = '".$confTravel[2]."', 
                 `c12` = '".$confTravel[4]."', 
                 `c13` = '".$confTravel[6]."', 
                 `c14` = '".$confTravel[8]."', 
                 `c15` = '".$confTravel[10]."', 
                 `c16` = '".$confSocial[0]."', 
                 `c17` = '".$confSocial[2]."', 
                 `c18` = '".$confSocial[4]."', 
                 `c19` = '".$confSocial[6]."', 
                 `c20` = '".$confSocial[8]."', 
                 `c21` = '".$confSocial[10]."', 
                 `c22` = '".$confSponsorship[0]."', 
                 `c23` = '".$confSponsorship[2]."', 
                 `c24` = '".$confSponsorship[4]."', 
                 `c25` = '".$confSponsorship[6]."', 
                 `c26` = '".$confSponsorship[8]."', 
                 `c27` = '".$confSponsorship[10]."'
        ";

// $arr = array_chunk($eventpricing, 3);
        $this->db->query($sql1);
        $insertId = $this->db->insert_id();

        $this->db->query("delete from `category` where `id`='".$insertId."' and `area` = 'e' ");
        $sql2 = "insert into `category` set 
                `categoryid` = '".$inpCat."',
                `id` = '".$insertId."',
                `area` = 'e'
        ";
        $this->db->query($sql2);



        if(!empty($speakers)){
            // print_r($speakers);
            $this->db->query("DELETE from `eventSpeakers` where `eventId` = '".$insertId."'");
            $speakers_arr = array_chunk($speakers, 4);
            // echo count($speakers_arr).'<br>';
            // echo '<pre>';
            // print_r($speakers_arr);
            // echo '</pre>';
            // $insert_eventSpkr = "";
            $insert_eventSpkr = array();
            for($i = 0; $i < count($speakers_arr); $i++){
                // if()
                $speaker_name = $speakers_arr[$i][0];
                $speaker_desc = $speakers_arr[$i][1];
                $speaker_ordr = $speakers_arr[$i][2];
                $speaker_phto = $speakers_arr[$i][3];
                if(!empty($speaker_name)){
                    $t = array(
                            'name' => $speaker_name,
                            'description' => $speaker_desc,
                            'orderBy' => $speaker_ordr,
                            'photo' => $speaker_phto,
                            'eventId' => $insertId,
                        );
                    array_push($insert_eventSpkr, $t);
                }
            }

            if(!empty($t)){
                $this->db->insert_batch('eventspeakers', $insert_eventSpkr);    
            }
            
            // `eventspeakers` 
            // `name`, `description`, `orderBy`, `photo`, `id`, `eventId`            
        }


        if(!empty($breakout)){
            $breakout_arr = array_chunk($breakout, 4);
            $this->db->query("DELETE from `eventbreakoutsessions` where `eventId` = '".$insertId."'");
            $insert_breakOut = array();
            for($i = 0; $i < count($breakout_arr); $i++){
                // if()
                $breakout_name = $breakout_arr[$i][0];
                $breakout_ses1 = $breakout_arr[$i][1];
                $breakout_ses2 = $breakout_arr[$i][2];
                $breakout_ordr = $breakout_arr[$i][3];

                if(!empty($breakout_name)){
                    $t = array(
                            'name' => $breakout_name,
                            'session1' => $breakout_ses1,
                            'session2' => $breakout_ses2,
                            'orderby' => $breakout_ordr,
                            'eventId' => $insertId
                        );
                    array_push($insert_breakOut, $t);
                }
                // echo $insert_eventSpkr;
            }

            // $this->db->query($insert_breakOut);
            if(!empty($t)){
                $this->db->insert_batch('eventbreakoutsessions', $insert_breakOut);
            }
            //`eventbreakoutsessions`
            //`breakoutId`, `eventId`, `name`, `session1`, `session2`, `orderby`
        }


        if(!empty($confSponsors)){
            $sponsors_arr = array_chunk($confSponsors, 4);
            $this->db->query("DELETE from `eventSponsors` where `eventId` = '".$insertId."'");

            $insert_sponsors = array();
            for($i = 0; $i < count($sponsors_arr); $i++){
                // if()
                $sponsors_name = $speakers_arr[$i][0];
                $sponsors_desc = $speakers_arr[$i][1];
                $sponsors_ordr = $speakers_arr[$i][2];
                $sponsors_phto = $speakers_arr[$i][3];

                if(!empty($sponsors_name)){
                    $t = array(
                            'name' => $sponsors_name,
                            'description' => $sponsors_desc,
                            'orderBy' => $sponsors_ordr,
                            'photo' => $sponsors_phto,
                            'eventId' => $insertId
                        );
                    array_push($insert_sponsors, $t);
                }
                // echo $insert_eventSpkr;
            }

            if(!empty($t)){
                $this->db->insert_batch('eventsponsors', $insert_sponsors);
            }
            // `eventsponsors` 
            // `name`, `description`, `orderBy`, `photo`, `id`, `eventId`              
        }



        if(!empty($event_pricing)){
            $eventprice_arr = array_chunk($event_pricing, 3);
            $this->db->query("DELETE from `eventCosts` where `eventId` = '".$insertId."'");
            $insert_eventprice = array();
            for($i = 0; $i < count($eventprice_arr); $i++){
                // if()
                $eventprice_name = $eventprice_arr[$i][0];
                $eventprice_cost = $eventprice_arr[$i][1];
                $eventprice_lmit = $eventprice_arr[$i][2];

                if(!empty($eventprice_name)){
                    $t = array(
                            'name' => $eventprice_name,
                            'cost' => $eventprice_cost,
                            'limitToMemberType' => $eventprice_lmit,
                            'eventId' => $insertId
                        );
                    array_push($insert_eventprice, $t);
                }
                // echo $insert_eventSpkr;
            }
            if(!empty($t)){
                $this->db->insert_batch('eventcosts', $insert_eventprice);
            }
            // start code here - Mark
            // eventcosts
            //`eventId`, `cost`, `name`, `limitToMemberType`            
        }

        return $insertId;
    }

    public function get_events($eventId){
        $query = "select * from `events` where `id` = '".$eventId."'";
        return array('result'=> $this->db->query($query)->result_array());
    }

    public function delete_page($page_id){
        $query = "delete from `ppms_pages` where `pid` = '".$page_id."'"; 
        $this->db->query($query);
        $status = 'deleted';
        return $status;
    }

    public function is_parent_page($page_id){
        $query = "select * from `ppms_pages` where `parent_id` = '".$page_id."'";
        return array('result'=> $this->db->query($query)->result_array());
    }

    public function banner_list(){
        $query = "select * from `banners` where `activate` = '1'";
        return array('result'=> $this->db->query($query)->result_array());
    }

    public function banner_data($banner_id){
        $query = "select * from `banners` where `id` = '".$banner_id."' ";
        return array('result'=>$this->db->query($query)->result_array());
    }

    public function get_categories(){
        $query = "SELECT id,name,overrideDropdownValue from categories where area in(2) order by name;";
        return array('result'=>$this->db->query($query)->result_array());
    }

    public function get_locations(){
        $query = "SELECT id,name,overrideDropdownValue from categories where area in(7) order by area,sort";
        return array('result'=> $this->db->query($query)->result_array());
    }

    public function get_galleries(){
        $query = "SELECT id,name from galleries order by name";
        return array('result'=> $this->db->query($query)->result_array());
    }

    public function get_products(){
        $query = "SELECT id,name from products order by name";
        return array('result'=> $this->db->query($query)->result_array());
    }
}
