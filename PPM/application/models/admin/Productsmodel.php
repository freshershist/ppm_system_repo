<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *	@Class Name: usersmodel
 *  @description: all related transaction of users in the database
 *
 */

if(!class_exists('CI_Model')) { class CI_Model extends Model {} }

class Productsmodel extends CI_Model
{
	public function __construct()
    {
        parent::__construct();

        $this->data_collect = array();
    }

    public function get_products ()
    {
		$query = "select * from `products`";

		//echo json_encode(array($query));die();

		//return array('query'=>$query, 'result'=> $this->db->query($query)->result_array());

		return array('result'=> $this->db->query($query)->result_array());
    }

    public function getProdCat(){
        $query = "SELECT id,name,area from categories where area in (3) order by area,sort;";
        return array('result'=> $this->db->query($query)->result_array());
    }

    public function getProdByCat($cat_id){
        if($cat_id !== 'all'){
            $query = "SELECT `products`.`id`,`name`, `shortdesc`,`code`, productOfTheMonth from `products`,`category` where `products`.`id` = `category`.`id` and `category`.`categoryid` = " . $cat_id . " order by name;";
        }else{
            $query = "select * from `products`";
        }
        
        return array('result'=> $this->db->query($query)->result_array());
    }

    public function get_relEvents(){
        $query = "SELECT id,name from events order by name";
        return array('result'=> $this->db->query($query)->result_array());
    }

    public function get_categories(){
        $query = "SELECT id,name,overrideDropdownValue from categories where area in(3) order by name;";
        return array('result'=>$this->db->query($query)->result_array());
    }

    public function save_product(){
        //`id`, `name`, `compability`, `body`, `relatedEvents`, `d1`, `d2`, `d3`, `shortdesc`, `code`, `d4`, `d5`, `d6`, `d7`, `d8`, `d9`, `d10`, `c1`, `c2`, `c3`, `c4`, `c5`, `c6`, `c7`, `c8`, `c9`, `c10`, `relatedProducts`, `productOfTheMonth`, `trxNotes`
        $prodName = $this->input->post('prodName');
        $selProdMonth = $this->input->post('selProdMonth');
        $selProdYear = $this->input->post('selProdYear');
        $prodCode = $this->input->post('prodCode');
        $inpCat = $this->input->post('inpCat');
        $prodAuthor = $this->input->post('prodAuthor');
        $prodShortD = $this->input->post('prodShortD');
        $prodDesc = $this->input->post('prodDesc');

        $txtRelatedEvents = $this->input->post('txtRelatedEvents');
        if(!empty($txtRelatedEvents)){
            $txtRelatedEvents = implode(',', $txtRelatedEvents);    
        }


        $txtRelatedProd = $this->input->post('txtRelatedProd');
        if(!empty($txtRelatedProd)){
            $txtRelatedProd = implode(',', $txtRelatedProd);    
        }

        $costs = $this->input->post('costs');
        $attachment = $this->input->post('attachment');
        echo '<pre>';
        print_r($costs);
        echo '</pre>';
        /*
prodName
selProdMonth
selProdYear
prodCode
inpCat
prodAuthor
prodShortD
prodDesc
txtRelatedEvents
txtRelatedProd

    [0] => sdfsdf
    [1] => 
    [2] => sdfsdf
    [3] => 
    [4] => sdfsdf
    [5] => 
    [6] => sdfsdf
    [7] => 
    [8] => sdfsdf
    [9] => 
    [10] => sdfsdf
    [11] => 
    [12] => sdfsdf
    [13] => 
    [14] => sdfsdf
    [15] => 
    [16] => sdfsdf
    [17] => 
    [18] => sdfsdf
    [19] => 
        */

        $query = "insert into `products` set 
        `name` = '".$prodName."', 
        `compability` = '', 
        `body` = '".$prodDesc."', 
        `relatedEvents` = '".$txtRelatedEvents."', 
        `shortdesc` = '".$prodShortD."', 
        `code` = '".$prodCode."',
        `d1` = '".$attachment[1]."',
        `d2` = '".$attachment[3]."',
        `d3` = '".$attachment[5]."', 
        `d4` = '".$attachment[7]."', 
        `d5` = '".$attachment[9]."', 
        `d6` = '".$attachment[11]."', 
        `d7` = '".$attachment[13]."', 
        `d8` = '".$attachment[15]."', 
        `d9` = '".$attachment[17]."', 
        `d10` = '".$attachment[19]."', 
        `c1` = '".$attachment[0]."', 
        `c2` = '".$attachment[2]."', 
        `c3` = '".$attachment[4]."', 
        `c4` = '".$attachment[6]."', 
        `c5` = '".$attachment[8]."', 
        `c6` = '".$attachment[10]."', 
        `c7` = '".$attachment[12]."', 
        `c8` = '".$attachment[14]."', 
        `c9` = '".$attachment[16]."', 
        `c10` = '".$attachment[18]."', 
        `relatedProducts` = '".$txtRelatedProd."', 
        `productOfTheMonth` = '".$selProdYear."-".$selProdMonth."-01 00:00:00', 
        `trxNotes` = ''";

        $this->db->query($query);
        $insertID = $this->db->insert_id();

        // $attachment_arr = array_chunk($attachment, 2);
        // print_r($attachment_arr);
    
        $query_del = "DELETE from category where id = '".$insertID."' and area = 'p' ";
        $this->db->query($query_del);
        $this->db->query("insert into `category` set `id` = '".$insertID."', `categoryid` = '".$inpCat."', `area`= 'p'");

        if(!empty($costs)){
            // print_r($speakers);
            $this->db->query("DELETE from `productcosts` where `productid` = '".$insertID."'");
            $costs_arr = array_chunk($costs, 3);
            // echo count($speakers_arr).'<br>';
            // echo '<pre>';
            // print_r($speakers_arr);
            // echo '</pre>';
            // $insert_eventSpkr = "";
            $insert_costs = array();
            for($i = 0; $i < count($costs_arr); $i++){
                // if()
                $costs_name = $costs_arr[$i][0];
                $costs_cost = $costs_arr[$i][1];
                $costs_post = $costs_arr[$i][2];
                if(!empty($costs_name)){
                    $t = array(
                            'name' => $costs_name,
                            'cost' => $costs_cost,
                            'postage' => $costs_post,
                            'productId' => $insertID,
                        );
                    array_push($insert_costs, $t);
                }
            }

            if(!empty($t)){
                $this->db->insert_batch('productcosts', $insert_costs);    
            }
                       
        }
        // SELECT * FROM `productcosts` WHERE 1 `productId`, `cost`, `name`, `postage`
    }
}
