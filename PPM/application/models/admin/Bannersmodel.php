<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *	@Class Name: usersmodel
 *  @description: all related transaction of users in the database
 *
 */

if(!class_exists('CI_Model')) { class CI_Model extends Model {} }

class Bannersmodel extends CI_Model
{
	public function __construct()
    {
        parent::__construct();

        $this->data_collect = array();
    }

    public function get_banners ()
    {
		$query = "select * from `banners` where `parent_id` is null";
    // $query = "select * from `users`";

		//echo json_encode(array($query));die();

		//return array('query'=>$query, 'result'=> $this->db->query($query)->result_array());

		return array('result'=> $this->db->query($query)->result_array());
    }

    // public function get_page($page_id){
    //     $query = "select * from `ppms_pages` where `pid` = '".$page_id."'";
    //     // print_r($this->db->query($query)->result_array());
    //     return array('result'=> $this->db->query($query)->result_array());
    // }

    // public function update_menu(){
    //     // SELECT * FROM `ppms_pages` WHERE `pid`, `title`, `content`, `parent_id`, `menu`, `page_order`, `user_id`, `banner_top`, `banner_bottom`, `status`, `date_added`

    //     $query = "select * from `ppms_pages`";

    //     //echo json_encode(array($query));die();

    //     //return array('query'=>$query, 'result'=> $this->db->query($query)->result_array());

    //     return array('result'=> $this->db->query($query)->result_array());
    // }

    public function save_banner($bid, $txtLink, $placement, $selectedFilePath, $isActive){
        // return $page_id;
        $selectedFilePath = str_replace( base_url().'/assets/uploads/files/', '', $selectedFilePath);
        if(!empty($bid)){
            //update
            $query = "update `banners` set 
                `image` = '".$selectedFilePath."', 
                `activate` = '".$isActive."',
                `placement` = '".$placement."', 
                `displayOn` = '', 
                `myText` = '', 
                `myLink` = '".$txtLink."', 
                `orderBy` = '', 
                `startDate` = '', 
                `endDate` = '', 
                `contentType` = '1'
                where 
                `id` = '".$bid."'
            "; 
            $this->db->query($query);
            $status = 'updated';
        }else{
            //insert
            $query = "insert into `banners` set 
                `image` = '".$selectedFilePath."', 
                `activate` = '".$isActive."',
                `placement` = '".$placement."', 
                `displayOn` = '', 
                `myText` = '', 
                `myLink` = '".$txtLink."', 
                `orderBy` = '', 
                `startDate` = '', 
                `endDate` = '', 
                `contentType` = '1'
            "; 
            $this->db->query($query);
            $status = 'inserted';
        }

        return $status;
        // return $page_id.' : '.$txtPageTitle.' : '.$editor_1.' : '.$pageStatus.' : '.$txtOrder;
    }

    public function delete_banner($banner_id){

        $sql = "select * from `banners` where `id` = '".$banner_id."'";
        $image_url = $this->db->query($sql)->result_array()[0]['image'];

        $new_image_url = str_replace(base_url(), '', $image_url);
        unlink($new_image_url);

        $query = "delete from `banners` where `id` = '".$banner_id."'"; 
        $this->db->query($query);
        $status = 'deleted';
        return $status;
    }

    // public function is_parent_page($page_id){
    //     $query = "select * from `ppms_pages` where `parent_id` = '".$page_id."'";
    //     return array('result'=> $this->db->query($query)->result_array());
    // }

    public function save_carousel($banner_id, $count, $array_img, $isActive){
        if(empty($banner_id)){
            $array_img[0] = str_replace( base_url().'/assets/uploads/files/', '', $array_img[0]);
            $query = "insert into `banners` set 
                `image` = '".$array_img[0]."', 
                `activate` = '".$isActive."',
                `placement` = '', 
                `displayOn` = '', 
                `myText` = '', 
                `myLink` = '', 
                `orderBy` = '', 
                `startDate` = '', 
                `endDate` = '', 
                `contentType` = '3'
            "; 
            $this->db->query($query);

            $id = $this->db->insert_id();
            if($count > 0){
                for($x = 1; $x < ($count+1); $x++){
                    $array_img[$x] = str_replace( base_url().'/assets/uploads/files/', '', $array_img[$x]);
                    $query = "insert into `banners` set 
                        `image` = '".$array_img[$x]."', 
                        `activate` = '".$isActive."',
                        `placement` = '', 
                        `displayOn` = '', 
                        `myText` = '', 
                        `myLink` = '', 
                        `orderBy` = '', 
                        `startDate` = '', 
                        `endDate` = '', 
                        `contentType` = '3',
                        `parent_id` = '".$id."'
                    "; 
                    $this->db->query($query);
                }                
            }


            $status = 'save'.$count;
        }else{
            $array_img[0] = str_replace( base_url().'/assets/uploads/files/', '', $array_img[0]);
            $query = "update `banners` set 
                `image` = '".$array_img[0]."', 
                `activate` = '".$isActive."',
                `placement` = '', 
                `displayOn` = '', 
                `myText` = '', 
                `myLink` = '', 
                `orderBy` = '', 
                `startDate` = '', 
                `endDate` = '', 
                `contentType` = '3'
                where 
                `id` = '".$banner_id."'
            "; 
            $this->db->query($query);

            // $id = $this->db->insert_id();
            $this->db->query("delete from `banners` where `parent_id` = '".$banner_id."'");

            for($x = 1; $x <= $count; $x++){
                $array_img[$x] = str_replace( base_url().'/assets/uploads/files/', '', $array_img[$x]);
                $query = "insert into `banners` set 
                    `image` = '".$array_img[$x]."', 
                    `activate` = '".$isActive."',
                    `placement` = '', 
                    `displayOn` = '', 
                    `myText` = '', 
                    `myLink` = '', 
                    `orderBy` = '', 
                    `startDate` = '', 
                    `endDate` = '', 
                    `contentType` = '3',
                    `parent_id` = '".$banner_id."'
                "; 
                $this->db->query($query);
            }

            $status = 'updated';
        }

        return $status;
    }

    public function get_banner_info($banner_id){
        $query = "select * from `banners` where `id` = '".$banner_id."'";
        return array('result'=> $this->db->query($query)->result_array());
    }

    public function get_banner_child($banner_id){
        $query = "select * from `banners` where `parent_id` = '".$banner_id."'";
        return array('result'=> $this->db->query($query)->result_array());
    }
}
