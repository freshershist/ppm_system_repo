<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *	@Class Name: usersmodel
 *  @description: all related transaction of users in the database
 *
 */

if(!class_exists('CI_Model')) { class CI_Model extends Model {} }

class Dashboardmodel extends CI_Model
{
	public function __construct()
    {
        parent::__construct();

        $this->data_collect = array();
        $this->set_data_collect();
    }

    public function is_logged_in() 
    {
        if(isset($_SESSION['admin_logged_in']) && $_SESSION['admin_logged_in']) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    public function login_user ($username = NULL, $password = NULL)
    {  
        $this->db->select('id, access, userType');
        $this->db->where(array('username'=>$username, 'password'=>$password));
        $result = $this->db->get('users');

        return $result->result_array();

    }

    public function get_categories_options()
    {
        $exclude = array(676,677,616,609,673,674,615,824);
    	$sql = "SELECT id,name,overrideDropdownValue from categories where area = 20 and id not in (" . implode(',', $exclude) . ") order by name;";

    	return $this->db->query($sql)->result_array();
    }

    public function get_members_categories() 
    {
        $exclude = array(298,286,309,221,877,303,339,306,378,311,222,341,375,305,382,307,302,304);//States
    	$sql = "SELECT name,id from categories where area = 10 and id not in (" . implode(',', $exclude) . ") order by name";

    	return $this->db->query($sql)->result_array();
    }

    public function get_state($area = NULL)
    {
    	if(isset($area)) {
    		$sql = "SELECT id,name,overrideDropdownValue from categories where area in(".$area.") order by area,sort";

    		return $this->db->query($sql)->result_array();
    	}

    	return array();
    }

    public function get_rei_master_modules()
    {
    	$sql = "SELECT name,id from categories where area = 11";

    	return $this->db->query($sql)->result_array();
    }

    public function populate_category_dictionary($area = NULL, $forceAlpha = FALSE)
    {
        if($area === 6) {
            $sql = "SELECT id,name,overrideDropdownValue from categories where area in( " . $area . ") order by name;";
        }
        else {
            if($forceAlpha) {
                $sql = "SELECT id,name,overrideDropdownValue from categories where area in( " . $area . ") order by name;";
            }
            else {
                $sql = "SELECT id,name,overrideDropdownValue from categories where area in( " . $area . ") order by area,sort;";
            }
        }
        

        $result = $this->db->query($sql)->result_array();

        $return_arr = array();

        foreach ($result as $value) {

            if(!empty($value['overrideDropdownValue']) || is_numeric($value['overrideDropdownValue'])) {
                $return_arr[$value['overrideDropdownValue']] = $value['name'];
            }
            else {
                $return_arr[$value['id']] = $value['name'];
            }
        }

        return $return_arr;
    }

	public function set_data_collect() {
		$this->data_collect = array(
			'member_status'			=>array("Active","Suspended","Removed/Cancelled/Unsubscribed","Double Ups"),
			'membership_payments'	=>array("Monthly Ezi Debit","Monthly Direct","Monthly Cheque","Upfront","Cash","Cheque","Direct Deposit","Other"),
			'billing_cycle'			=>array("Annual","Monthly"),
			'trust_account_program'	=>array("1"=>"Rockend","2"=>"Sherlock","3"=>"Rest","4"=>"Real Oz", "5"=>"Realtor Trust Manager", "6"=>"GeeDee", "7"=>"Tenacity", "8"=>"Macpro", "9"=>"Console", "10"=>"Multi-array", "11"=>"Other","12"=>"REI Master"),
			'frequency_of_audits'	=>array("2 weeks","1 month","2 months","3 months","4 months","6 months","12 months"),
			'system_type'			=>array("PPMsystem","PPM Powered by REI Master"),
			'interest_type'			=>array("System Enquiry","System Presentation","Training","Product","Conference","Online Training"),
			'state_subregion'		=>array("Brisbane","Brisbane NW","Gold Coast","Sunshine Coast","Nth Qld"),
            'status'                =>array("Completed","Open","Critical","Left Message","Waiting Response","Call Not Returned"),
            'section_array'         =>array("Download","System Upgrade","Conference Download"),
            'reason_cancelled'      => array("Business Closed/Receivership", "Changed to Competitor System", "Not of Value to the Office", "Not Utilising Member Benefits", "Sold Rent Roll", "Staff Not Using the System", "Too Expensive/Cutting Costs", "Other - Text"),
            'howdidtheyhear'        =>array("Current Members","ENews","PPM Group Event","Franchise Head Office","Referral","PM Journal","REI Journal","Breakfast Club","Tele-Call","Fax Promo","Email Promo","Unknown","Other"),
            'cat_sections' =>array(
                'newsTopic'                 => array('area'=>6, 'parent'=>0, 'title'=> 'News Topic'),
                'pmJournal'                 => array('area'=>8, 'parent'=>0, 'title'=> 'PM Journal'),
                'events'                    => array('area'=>2, 'parent'=>0, 'title'=> 'Events'),
                'linksISP'                  => array('area'=>4, 'parent'=>37, 'title'=> 'Links: Industry Service Provider'),
                'members'                   => array('area'=>10, 'parent'=>0, 'title'=> 'General Client'),
                'reiMasterModules'          => array('area'=>11, 'parent'=>0, 'title'=> 'PPMsystem Powered by REI Master Modules'),
                'trainingTopics'            => array('area'=>12, 'parent'=>0, 'title'=> 'Training Topics'),
                'emailQueue'                => array('area'=>13, 'parent'=>0, 'title'=> 'Email Queue'),
                'pollcategories'            => array('area'=>16, 'parent'=>0, 'title'=> 'Poll Types'),
                'clienttypecategories'      => array('area'=>20, 'parent'=>0, 'title'=> 'Client Types'),
                'memberstatecategories'     => array('area'=>21, 'parent'=>0, 'title'=> 'Member States'),
                'positiontypecategories'    => array('area'=>22, 'parent'=>0, 'title'=> 'Position Types'),
                'howhearaboutuscategories'  => array('area'=>24, 'parent'=>0, 'title'=> 'How Did They Hear About Us'),
                'pollfinishmsgs'            => array('area'=>25, 'parent'=>0, 'title'=> 'Poll Finish Messages'),
                'pmawards'                  => array('area'=>26, 'parent'=>0, 'title'=> 'Property Management Awards'),
                'gallery'                   => array('area'=>27, 'parent'=>0, 'title'=> 'Gallery'),
                //'forum'                     => array('area'=>, 'parent'=>, 'title'=> 'Forum Folder'),
                //'forumcategories'           => array('area'=>, 'parent'=>, 'title'=> 'Forum Topic Categories'),
                //'tasktypecategories'        => array('area'=>, 'parent'=>, 'title'=> 'Task Types'),
                //'taskleadtypecategories'    => array('area'=>, 'parent'=>, 'title'=> 'Task Lead Types'),
                //'taskobjectiontypecategories'=> array('area'=>, 'parent'=>, 'title'=> 'Task Objection Types'),
            ),
            'user_type' => array(1=>'Admin', 2=>'Normal', 3=>'Partner'),
            'access' => array(
                1=>'List/Add Clients | Members',
                2=>'List/Add Events',
                3=>'List/Add Training',
                4=>'List/Add Contents',
                5=>'List/Add Products',
                6=>'List/Add Menus',
                7=>'Lis/Add Page',
                8=>'List/Add Banners',
                9=>'List/Add Links | Discounts',
                10=>'List/Add Testimonials | Staffs',
                11=>'List/Add eNews | Newsletter',
                12=>'List/Add Awards',
                13=>'List/Add Galleries',
                14=>'List/Add Category',
                15=>'List/Add Download | System Upgrade',
                16=>'List/Add Promo Codes',
                17=>'List/Add Polls',
                18=>'Member Awards Stats',
                19=>'Reporting',
                20=>'Export DB',
                21=>'Home Page',
                22=>'Partners Page'
            )   
			);
	}

    public function convert_au_time_to_standard($date_str = '')
    {
        if(!empty($date_str)) {
            $str_arr = explode('/', $date_str);

            if(is_array($str_arr) && count($str_arr) === 3) {
                return $str_arr[1] . '/' . $str_arr[0] . '/' . $str_arr[2];
            }
        }
    }

    public function get_moderators($forumId) 
    {
        if($forumId === 'all') {
            $sql = "SELECT moderators from forums where moderators !=''";
        }
        else {
            $sql = "SELECT moderators from forums where forumid = " . $forumId . " and moderators != ''";
        }

        $result = $this->db->query($sql)->result_array();

        $return_arr = array();

        foreach ($result as $value) {
            $moderators = explode(',', $value['moderators']);

            if(is_array($moderators) && count($moderators)>0) {
                foreach ($moderators as $id) {
                    if(!in_array($id, $return_arr)) {
                        $return_arr[] = $id;
                    }
                }
            }    
        }

        return (count($return_arr)>0) ? implode(',', $return_arr) : 0;        
    }

    public function get_users()
    {
        $sql = "SELECT username,id from users;";

        $result = $this->db->query($sql)->result_array();

        $return_arr = array();

        foreach ($result as $value) {
            $return_arr[$value['id']] = $value['username'];
        }

        return $return_arr;       
    }

    public function list_categories($area = NULL, $parent = 0)
    {
        if(!empty($area)) {
            $sql = "SELECT id,name,area,sort from categories where area = {$area} and parent = {$parent} order by name;";
        }
        else {
            $sql = "SELECT id,name,area,sort from categories where area in (2,3,5,6,8,10,11,12,13,14,16,17,18,19,20,21,22) or parent = 37 order by area,name;";
        }

        return $this->db->query($sql)->result_array();
    }

    public function get_category_details($id = NULL)
    {
        $sql = "SELECT name,area,sort,body,forumid,d1 FROM categories where ID = {$id} limit 1";

        return $this->db->query($sql)->result_array();
    }

    public function add_category($data = NULL)
    {
        $this->db->insert('categories', $data);
        return $this->db->insert_id();
    }

    public function update_category($id = NULL, $data = NULL)
    {
        $this->db->where('id', $id);
        $this->db->update('categories', $data);
    }

    public function delete_category($id = NULL) 
    {
        $sql = "DELETE from categories where id = {$id}";

        $this->db->query($sql);
    }

    public function get_gallery_by_category($catId = NULL, $from = NULL, $to = NULL)
    {
        $with_from  = (!empty($from)) ? ' and mydate >= "' . $from . '" ' : '';
        $with_to    = (!empty($to)) ? ' and mydate <= "' . $to . '" ' : '';

        $sql = "SELECT *, (Select name from categories where id = {$catId} limit 1) as categoryname from galleries where category = {$catId} " . $with_from . $with_to . " order by mydate desc,name;";

        return $this->db->query($sql)->result_array();
    }

    public function get_gallery_details($id = NULL)
    {
        $sql = "SELECT * FROM  galleries  where id = {$id}";

        $details = $this->db->query($sql)->result_array();

        $sql = "SELECT d1,caption,body,orderBy from galleryitems where id = {$id} order by orderBy desc;";

        $items = $this->db->query($sql)->result_array();

        return array('details'=>$details, 'items'=>$items);
    }

    public function add_update_gallery($id = NULL, $data = NULL)
    {
        $affected_row = NULL;

        if(empty($id)) {
            $this->db->insert('galleries', $data);
            $affected_row = $this->db->insert_id();
        }
        else {
            $this->db->where('id', $id);
            $this->db->update('galleries', $data);

            $affected_row = $this->db->affected_rows();
        }

        return $affected_row; 
    }

    public function add_gallery_items($id = NULL, $data = NULL)
    {
        if(!empty($data) && count($data)>0) {
            $this->db->delete('galleryitems', array('id'=>$id));

            foreach ($data as $value) {
                if(is_array($value)) {
                    $arr = array(
                            'orderBy'   => $value[0],
                            'd1'        => $value[1],
                            'caption'   => $value[2],
                            'body'      => $value[3],
                            'id'        => $id
                            
                        );

                    $this->db->insert('galleryitems', $arr);
                }
            }
        }
        else {
            $this->db->delete('galleryitems', array('id'=>$id));
        }
    }

    public function get_users_by_type($userType = NULL) 
    {
        if(!empty($userType)) {
            $sql = "SELECT * from users where userType = {$userType} order by username;";

            return $this->db->query($sql)->result_array();
        }
    }

    public function get_user_details($id = NULL)
    {
        $sql = "SELECT * from users where id = {$id}";

        return $this->db->query($sql)->result_array();
    }

    public function add_update_user($id = NULL, $data = NULL)
    {
        if(empty($id)) {
            $this->db->insert('users', $data);
            $id = $this->db->insert_id();
        }
        else {
            $this->db->where('id', $id);
            $this->db->update('users', $data);
        }

        return $id; 
    }

    public function delete_user($id = NULL)
    {
        $sql = "DELETE from users where id = {$id};";

        $this->db->query($sql);
    }

    public function check_user_access($accessId = NULL)
    {
        if(isset($_SESSION['admin_access']) && !empty($_SESSION['admin_access'])) {

            $access = json_decode($_SESSION['admin_access']);

            if (json_last_error() === JSON_ERROR_NONE) {
                $allow = FALSE;

                foreach ($access->data as $key => $value) {
                    if($accessId === intval($value)) {
                        $allow = TRUE;
                        break;
                    }
                }

                return $allow;
            }
            else {
                return FALSE;
            }
        }
        else {
            return FALSE;
        }
    }

    /** Monthly Award Stats **/
    public function get_annual_report($fiscalYear = NULL)
    {

        $to = $fiscalYear + 1;

        $sql = "SELECT DISTINCT id,company,statId,forMonth 
            from monthlyAwardStats as mas,members as m 
            where forMonth between '{$fiscalYear}-07-01' and '{$to}-06-30' and mas.memberId = m.id order by id,forMonth asc;";

        return $this->db->query($sql)->result_array();
    }

    public function get_quarter_report($id = NULL, $col = NULL, $startPeriod = NULL, $endPeriod = NULL)
    {
        $item = '';

        if($col === 'net') {
            $item = 'SUM(numberOfPropertiesGained - numberOfPropertiesLost) as net';
        }
        elseif($col === 'dollarIncrease'){
            $item = 'SUM(propertiesGainedDollar) - SUM(propertieslostDollar) as dollarIncrease';
        }
        else {
            $item = 'SUM('.$col.') as '.$col;
        }

        $sql = "SELECT " . $item . " from monthlyAwardStats
            where forMonth >= '{$startPeriod}' and forMonth < '{$endPeriod}' and memberId = {$id};";

        $row = $this->db->query($sql)->row_array();

        $arr = array();

        if (isset($row)){
            return doubleval($row[$col]);
        }

        return 0;    
    }

    public function get_individual_achievement($startDate = NULL, $endDate = NULL)
    {
        $sql = "SELECT company,individualAchievement,forMonth from monthlyAwardStats as mas,members as m where forMonth between '{$startDate}' and '{$endDate}' and m.id = mas.memberId and individualAchievement != '' order by forMonth desc;";

        return $this->db->query($sql)->result_array();
    }

    public function get_monthly_stats($startDate = NULL, $endDate = NULL)
    {
        $sql = "SELECT id,company 
            from monthlyAwardStats as mas,members as m 
            where forMonth between '{$startDate}' and '{$endDate}' and mas.memberId = m.id GROUP BY id,company order by id asc;";

        return $this->db->query($sql)->result_array();
    }    

    public function get_month_stats($id = NULL, $col = NULL, $month = NULL, $year = NULL)
    {
        $item = '';

        if($col === 'net') {
            $item = 'SUM(numberOfPropertiesGained - numberOfPropertiesLost) as net';
        }
        elseif($col === 'dollarIncrease'){
            $item = 'SUM(propertiesGainedDollar) - SUM(propertieslostDollar) as dollarIncrease';
        }
        else {
            $item = 'SUM('.$col.') as '.$col;
        }

        $sql = "SELECT " . $item . " from monthlyAwardStats
            where MONTH(forMonth) = {$month} and YEAR(forMonth) = {$year} and memberId = {$id};";

        $row = $this->db->query($sql)->row_array();

        $arr = array();

        if (isset($row)){
            return doubleval($row[$col]);
        }

        return 0;
    }

    /** Monthly Award Stats **/
}
