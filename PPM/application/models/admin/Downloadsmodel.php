<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *	@Class Name: usersmodel
 *  @description: all related transaction of users in the database
 *
 */

if(!class_exists('CI_Model')) { class CI_Model extends Model {} }

class Downloadsmodel extends CI_Model
{
	public function __construct()
    {
        parent::__construct();
    }

    public function get_downloads($category = NULL)
    {
        if(!empty($category)) {
            $sql = "SELECT id,datePosted,name,code,shortdesc,platnium,gold,silver,bronze from downloads where mysection = {$category} order by datePosted,id desc;";
        }
        else {
            $sql = "SELECT id,datePosted,name,code,shortdesc,platnium,gold,silver,bronze from downloads order by datePosted,id desc;";
        }

        return $this->db->query($sql)->result_array();
    
    }

    public function get_download($id = NULL)
    {
        $arr = array();

        $sql = "SELECT * from downloads where id = {$id};";

        $arr['details'] = $this->db->query($sql)->result_array();

        $sql = "SELECT categoryId from category where id = {$id} and area = 'd';";

        $arr['state'] = $this->db->query($sql)->result_array();

        return $arr;
    }

    public function add_update_download($id = NULL, $data = NULL, $state = NULL)
    {
        if(!empty($id)) {
            $this->db->where('id', $id);
            $this->db->update('downloads', $data);
        }
        else {
            $this->db->insert('downloads', $data);
            $id = $this->db->insert_id();
        }

        $this->db->delete('category', array('id'=>$id, 'area'=>'d'));

        if(!empty($state)) {
            $batch = array();

            foreach ($state as $key => $value) {
                $batch[] = array('id'=>$id, 'categoryid'=>intval($value), 'area'=>'d');
            }

            $this->db->insert_batch('category', $batch);
        }

        return $id;
    }

    public function delete_download($id = NULL)
    {
        //DELETE from downloadLogENews where eNewsID = 
        //DELETE from eNews where id =

        $sql = "DELETE from downloads where id = {$id};";

        $this->db->query($sql);         

        $sql = "DELETE from category where id = {$id} and area = 'd';";

        $this->db->query($sql);      

    }
}
