<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *	@Class Name: usersmodel
 *  @description: all related transaction of users in the database
 *
 */

if(!class_exists('CI_Model')) { class CI_Model extends Model {} }

class Testimonialsmodel extends CI_Model
{
	public function __construct()
    {
        parent::__construct();

        $this->data_collect = array();
    }

    public function get_testimonials ()
    {
		$query = "select * from `testimonials`";
    // $query = "select * from `users`";

		//echo json_encode(array($query));die();

		//return array('query'=>$query, 'result'=> $this->db->query($query)->result_array());

		return array('result'=> $this->db->query($query)->result_array());
    }

    public function get_testimonialsByCat($cat_id){
        if(isset($cat_id)){
            if($cat_id == 'All'){
                // $query = "select `id`, `name`, `body`, `company` from `testimonials` order by `name`";
                $query = "select * from `testimonials` order by `name`";
            }else{
                // $query = "select `testimonials`.`id`, `name`, `body`, `company` from `testimonials`,`category` where `testimonials`.`id` = `category`.`id` and `category`.`categoryid` = '".$cat_id."' order by trim(`name`)";
                $query = "select * from `testimonials`,`category` where `testimonials`.`id` = `category`.`id` and `category`.`categoryid` = '".$cat_id."' order by trim(`name`)";
            }
        }
        return array('result'=> $this->db->query($query)->result_array());
    }


    public function get_Staff(){
        $query = "select `id` from `categories` where `area` in (5) and `name` = 'Staff' order by `sort`";
        $res = $this->db->query($query)->result_array();
        if(count($res) > 0){
            $id =  $res[0]['id'];    
            $query1 = "select * from `testimonials`,`category` where `testimonials`.`id` = `category`.`id` and `category`.`categoryid` = '".$id."' order by trim(`name`)";
            return array('result'=> $this->db->query($query1)->result_array());
        }else{
            return false;
        }
        // print
        // return array('result'=> $this->db->query($query)->result_array());
    }
    // public function get_page($page_id){
    //     $query = "select * from `ppms_pages` where `pid` = '".$page_id."'";
    //     // print_r($this->db->query($query)->result_array());
    //     return array('result'=> $this->db->query($query)->result_array());
    // }

    // public function update_menu(){
    //     // SELECT * FROM `ppms_pages` WHERE `pid`, `title`, `content`, `parent_id`, `menu`, `page_order`, `user_id`, `banner_top`, `banner_bottom`, `status`, `date_added`

    //     $query = "select * from `ppms_pages`";

    //     //echo json_encode(array($query));die();

    //     //return array('query'=>$query, 'result'=> $this->db->query($query)->result_array());

    //     return array('result'=> $this->db->query($query)->result_array());
    // }

    public function save_banner($bid, $txtLink, $placement, $selectedFilePath, $isActive){
        // return $page_id;
        if(!empty($bid)){
            //update
            $query = "update `banners` set 
                `image` = '".$selectedFilePath."', 
                `activate` = '".$isActive."',
                `placement` = '".$placement."', 
                `displayOn` = '', 
                `myText` = '', 
                `myLink` = '".$txtLink."', 
                `orderBy` = '', 
                `startDate` = '', 
                `endDate` = '', 
                `contentType` = '1'
                where 
                `id` = '".$bid."'
            "; 
            $this->db->query($query);
            $status = 'updated';
        }else{
            //insert
            $query = "insert into `banners` set 
                `image` = '".$selectedFilePath."', 
                `activate` = '".$isActive."',
                `placement` = '".$placement."', 
                `displayOn` = '', 
                `myText` = '', 
                `myLink` = '".$txtLink."', 
                `orderBy` = '', 
                `startDate` = '', 
                `endDate` = '', 
                `contentType` = '1'
            "; 
            $this->db->query($query);
            $status = 'inserted';
        }

        return $status;
        // return $page_id.' : '.$txtPageTitle.' : '.$editor_1.' : '.$pageStatus.' : '.$txtOrder;
    }

    public function get_TestiCategory(){
        $query = "select `id`,`name`,`area` from `categories` where `area` in (5) order by `sort`";

        return array('result'=> $this->db->query($query)->result_array());
    }

    public function getTestiByCatId($cat_id){
        $query = "SELECT testimonials.id,name, body, company from testimonials,category where testimonials.id = category.id and category.categoryid = '".$cat_id."' order by name;";
        return array('result'=> $this->db->query($query)->result_array());
    }
    

    public function saveData($post){
        // print_r($post);
        // [selCategory] => All
        // [txtID] => 
        // [txtName] => 
        // [txtDescript] => 
        // [txtCompany] => 
        $selCategory = $post['selCategory'];
        $ID = $post['txtID'];
        $txtDescript = $post['txtDescript'];
        $txtCompany = $post['txtCompany'];
        $txtName = $post['txtName'];
        $txtTitle = $post['txtTitle'];
        $imgPath = $post['imgPath'];

        if(!isset($post['isHome'])){
            $selIsHome = 0;    
        }else{
            $selIsHome = 1;
        }
        // echo $selIsHome;

        if(empty($ID)){
            #ADD
            //`name`, `body`, `title`, `company`, `d1`, `displayonhome`
            $insert_testimonials = "insert into `testimonials` set 
                                    `name` = '".$txtName."',
                                    `body` = '".$txtDescript."',
                                    `title` = '".$txtTitle."',
                                    `company` = '".$txtCompany."',
                                    `d1` = '".$imgPath."',
                                    `displayonhome` = '".$selIsHome."'
                                    ";

            $this->db->query($insert_testimonials);
            $i_id = $this->db->insert_id();

            $this->db->query("DELETE from `category` where `id` = '" .$i_id. "' and `area` = 't';");
            $this->db->query("INSERT INTO `CATEGORY` (`id`,`categoryid`,`area`) values ('" .$i_id. "','" .$selCategory. "','t');");
            echo 'added';
        }else{
            #UPDATE
            $update_testimonials = "update `testimonials` set 
                                    `name` = '".$txtName."',
                                    `body` = '".$txtDescript."',
                                    `title` = '".$txtTitle."',
                                    `company` = '".$txtCompany."',
                                    `d1` = '".$imgPath."',
                                    `displayonhome` = '".$selIsHome."'
                                    where 
                                    `id` = '".$ID."'
                                    ";

            $this->db->query($update_testimonials);

            $this->db->query("DELETE from `category` where `id` = '" .$ID. "' and `area` = 't';");
            $this->db->query("INSERT INTO `CATEGORY` (`id`,`categoryid`,`area`) values ('" .$ID. "','" .$selCategory. "','t');");
            echo 'updated';
        }
    }

    public function getTestiDetails($id){
        $query = "select * from `testimonials` where `id` = '".$id."'";
        return array('result'=> $this->db->query($query)->result_array());
    }

    public function getTestCatergoryByTID($id){
        $query = "select `categoryid` from `category` where `id` = '".$id."' and `area` = 't' ";
        return array('result'=> $this->db->query($query)->result_array());
    }

    // public function delete_page($page_id){
    //     $query = "delete from `ppms_pages` where `pid` = '".$page_id."'"; 
    //     $this->db->query($query);
    //     $status = 'deleted';
    //     return $status;
    // }

    // public function is_parent_page($page_id){
    //     $query = "select * from `ppms_pages` where `parent_id` = '".$page_id."'";
    //     return array('result'=> $this->db->query($query)->result_array());
    // }
}
