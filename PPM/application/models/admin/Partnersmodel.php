<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *	@Class Name: usersmodel
 *  @description: all related transaction of users in the database
 *
 */

if(!class_exists('CI_Model')) { class CI_Model extends Model {} }

class Partnersmodel extends CI_Model
{
	public function __construct()
    {
        parent::__construct();
    }

    public function get_list()
    {
        $sql = "SELECT * from partnerspage order by id asc;";

        return $this->db->query($sql)->result_array();
    
    }

    public function get_page($id = NULL)
    {
        $sql = "SELECT * from partnerspage where id = {$id};";

        return $this->db->query($sql)->result_array();
    }

    public function add_update_page($id = NULL, $data = NULL)
    {
        if(!empty($id)) {
            $this->db->where('id', $id);
            $this->db->update('partnerspage', $data);
        }
        else {
            $this->db->insert('partnerspage', $data);
            $id = $this->db->insert_id();
        }

        return $id;
    }

    public function delete_page($id = NULL)
    {
        $sql = "DELETE from partnerspage where id = {$id};";

        $this->db->query($sql);      

    }

    public function get_pages(){
        $sql = "Select pid, slug, title from ppms_pages order by slug asc;";

        return $this->db->query($sql)->result_array();
    }
}
