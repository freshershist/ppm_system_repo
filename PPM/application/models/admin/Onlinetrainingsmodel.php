<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *	@Class Name: usersmodel
 *  @description: all related transaction of users in the database
 *
 */

if(!class_exists('CI_Model')) { class CI_Model extends Model {} }

class Onlinetrainingsmodel extends CI_Model
{
	public function __construct()
    {
        parent::__construct();

        $this->data_collect = array();
    }

    public function get_onlinetraining ()
    {
		$query = "select * from `onlineTraining` order by orderby";
		return array('result'=> $this->db->query($query)->result_array());
    }

    public function getTrainingTopics(){
        $query = "SELECT `name` from `categories` where `area` = '12'";
        return array('result'=> $this->db->query($query)->result_array());
    }

    public function save_trainings($p){
        if(!isset($p['isGuest'])){
            $isGuest = '';
        }else{
            $isGuest = $p['isGuest'];
        }

        if($p['inpIsDraft'] == '0'){
            $p['inpIsDraft'] = '';
        }

        if(!empty($p['inpOID'])){
            // echo 'update';
            $sql = "update `onlinetraining` set 
                    `topic` = '".$p['inpTopic']."',
                    `title` = '".$p['inpTitle']."',
                    `shortDesc` = '".$p['inpDescription']."',
                    `speaker` = '".$p['inpSpeaker']."',
                    `duration` = '".$p['inpDuration']."',
                    `body` = '".$p['sub_editor_1']."',
                    `activity` = '".$p['sub_editor_2']."',
                    `c1` = '".$p['inpC1']."',
                    `c2` = '".$p['inpC2']."',
                    `c3` = '".$p['inpC3']."',
                    `c4` = '".$p['inpC4']."',
                    `c5` = '".$p['inpC5']."',
                    `d1` = '".$p['inpD1']."',
                    `d2` = '".$p['inpD2']."',
                    `d3` = '".$p['inpD3']."',
                    `d4` = '".$p['inpD4']."',
                    `d5` = '".$p['inpD5']."',
                    `mediaFile` = '".$p['inpMedia']."',
                    `myType` = '".$p['inpType']."',
                    `products` = '',
                    `articles` = '',
                    `downloads` = '',
                    `orderby` = '".$p['inpOrder']."',
                    `draft` = '".$p['inpIsDraft']."',
                    `guestAccess` = '".$isGuest."' where `onlinetraining`.`id` = ".$p['inpOID']."
            ";
            $res = 'updated';
        }else{
            // echo 'Add';
            $sql = "insert into `onlinetraining` set 
                    `topic` = '".$p['inpTopic']."',
                    `title` = '".$p['inpTitle']."',
                    `shortDesc` = '".$p['inpDescription']."',
                    `speaker` = '".$p['inpSpeaker']."',
                    `duration` = '".$p['inpDuration']."',
                    `body` = '".$p['sub_editor_1']."',
                    `activity` = '".$p['sub_editor_2']."',
                    `c1` = '".$p['inpC1']."',
                    `c2` = '".$p['inpC2']."',
                    `c3` = '".$p['inpC3']."',
                    `c4` = '".$p['inpC4']."',
                    `c5` = '".$p['inpC5']."',
                    `d1` = '".$p['inpD1']."',
                    `d2` = '".$p['inpD2']."',
                    `d3` = '".$p['inpD3']."',
                    `d4` = '".$p['inpD4']."',
                    `d5` = '".$p['inpD5']."',
                    `mediaFile` = '".$p['inpMedia']."',
                    `myType` = '".$p['inpType']."',
                    `products` = '',
                    `articles` = '',
                    `downloads` = '',
                    `orderby` = '".$p['inpOrder']."',
                    `draft` = '".$p['inpIsDraft']."',
                    `guestAccess` = '".$isGuest."' ,
                    `hits` = '0'
            ";
            $res = 'added';
        }

        $this->db->query($sql);
        return $res;
    }

    public function getTrainingByID($id){
        $sql = "select * from `onlinetraining` where `id` = '".$id."'";

        return array('result'=> $this->db->query($sql)->result_array());   
    }

    public function delete_training($id){
        $sql = "delete from `onlinetraining` where `id`='".$id."'";
        $this->db->query($sql);
        $status = 'deleted';
        return $status;
    }
}