<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *	@Class Name: usersmodel
 *  @description: all related transaction of users in the database
 *
 */

if(!class_exists('CI_Model')) { class CI_Model extends Model {} }

class Contentsmodel extends CI_Model
{
	public function __construct()
    {
        parent::__construct();
    }

    public function get_categories() 
    {
        $sql = "SELECT id,name,area from categories where area in (1,6,23) order by area,sort";

        return $this->db->query($sql)->result_array();
    }

    public function get_news_by_category($categoryid = NULL, $state = NULL, $rentRollType = NULL)
    {
        if(!empty($categoryid)) {
            $sql = "SELECT news.id,name, shortdesc, clickherepromotiontext, mydate, enddate, pollid, drawdate, drawperson,code,comments,draft,location,state,(Select count(*) from enquiryDetails where newsId = news.id) as numberOfEnquiries,code,contactName,companyName,email,contactNumber from news,category where news.id = category.id and category.categoryid = {$categoryid} order by mydate desc;";
        }
        else {
            $sql = "SELECT id,name, shortdesc, clickherepromotiontext, mydate, enddate, pollid, drawdate, drawperson,code,comments,draft,location,state,(Select count(*) from enquiryDetails where newsId = news.id) as numberOfEnquiries,code,contactName,companyName,email,contactNumber from news order by mydate desc;";
        }

        return $this->db->query($sql)->result_array();
    
    }

    public function get_related_products()
    {
        $sql = "SELECT id,name from products order by name;";

        return $this->db->query($sql)->result_array();
    }

    public function get_related_downloads() {
        $sql = "SELECT id,name,
                (Select categoryid category from category where category.id = downloads.id and area = 'd' limit 1) as stateid 
                from downloads order by name;";

        return $this->db->query($sql)->result_array();
    }

    public function get_article($id = NULL) 
    {
        $sql = "SELECT * FROM  news  where ID = {$id}";

        return $this->db->query($sql)->result_array();
    }

    public function get_news_category($newsId = NULL)
    {
        $sql = "SELECT categoryId from category where id = {$newsId} and area = 'n';";

        return $this->db->query($sql)->result_array();
    }

    public function add_news($data = NULL)
    {
        $this->db->insert('news', $data);
        return $this->db->insert_id();
    }

    public function update_news($id = NULL, $data = NULL)
    {
        $this->db->where('id', $id);
        $this->db->update('news', $data);
    }

    public function update_news_categories($id = NULL, $categories = NULL, $topics = NULL)
    {
        $sql = "Delete from category where id = {$id} and area = 'n';";

        $this->db->query($sql);

        $cat_arr = array();

        if(is_array($categories) && !empty($categories)) {
            if(is_array($topics) && !empty($topics)) {
                $cat_arr = array_merge($categories, $topics);
            }
            else {
                $cat_arr = $categories;
            }

            $categories = array();

            foreach ($cat_arr as $value) {
                $categories[] = array('id' => $id,'categoryid' => $value,'area' => 'n');
            }

            $this->db->insert_batch('category', $categories);                  
        }
    }

    public function get_enquiry_details($news_id = NULL, $is_count = FALSE)
    {
        if($is_count) {

            $sql = "SELECT count(*) as count FROM enquiryDetails WHERE newsId = {$news_id}";

            $result = $this->db->query($sql);

            $row = $result->row_array();

            if (isset($row)){
                return (intval($row['count']) > 0) ? $row['count'] : '';
            }

            return ''; 
        }
        else {
            $sql = "SELECT * FROM enquiryDetails WHERE newsId = {$news_id} ORDER BY enquiryId";

            return $this->db->query($sql)->result_array();
        }
    }

    public function get_polls()
    {
        $sql = "SELECT id,CONCAT(pollName,' [',DAY(startDate), '-',MONTHNAME(startDate), '-',YEAR(startDate), '] to [', DAY(endDate), '-',MONTHNAME(endDate), '-',YEAR(endDate),']') as pollname from polls order by enddate desc;";

        return $this->db->query($sql)->result_array();
    }

    public function delete_news($id = NULL)
    {
        /*
        executeQuery("DELETE from enquiryDetails where newsid = " & id & ";")
        executeQuery("DELETE from news where id = " & id & ";")
        executeQuery("DELETE from category where id = " & id & " and area = 'n';")
        */
        $sql = "DELETE from enquiryDetails where newsid = {$id};";

        $this->db->query($sql);

        $sql = "DELETE from news where id = {$id};";

        $this->db->query($sql);

        $sql = "DELETE from category where id = {$id} and area = 'n';";

        $this->db->query($sql);        

    }

    public function get_rent_roll_code()
    {
        $sql = "SELECT max(code) + 1 as code from news,category where news.id = category.id and category.categoryid = 5;";

        $result = $this->db->query($sql);

        $row = $result->row_array();

        if (isset($row)){
            return (intval($row['code']) > 0) ? $row['code'] : '';
        }

        return '';
    }
}
