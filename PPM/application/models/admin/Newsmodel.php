<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *	@Class Name: usersmodel
 *  @description: all related transaction of users in the database
 *
 */

if(!class_exists('CI_Model')) { class CI_Model extends Model {} }

class Newsmodel extends CI_Model
{
	public function __construct()
    {
        parent::__construct();
    }

    public function get_news($categoryid = NULL)
    {
        if(!empty($categoryid)) {
            $sql = "SELECT id,name,category,mydate from eNews where category = {$categoryid} order by mydate desc;";
        }
        else {
            $sql = "SELECT id,name,category,mydate from eNews order by  mydate desc;";
        }

        return $this->db->query($sql)->result_array();
    
    }

    public function get_enews($id = NULL)
    {
        $sql = "SELECT name,category,d1,d2,mydate,body FROM eNews  where id = {$id};";

        return $this->db->query($sql)->result_array();
    }

    public function add_update_enews($id = NULL, $data = NULL)
    {
        if(!empty($id)) {
            $this->db->where('id', $id);
            $this->db->update('eNews', $data);
        }
        else {
            $this->db->insert('eNews', $data);
            $id = $this->db->insert_id();
        }

        return $id;
    }

    public function delete_enews($id = NULL)
    {
        //DELETE from downloadLogENews where eNewsID = 
        //DELETE from eNews where id =

        $sql = "DELETE from downloadLogENews where eNewsID = {$id};";

        $this->db->query($sql);         

        $sql = "DELETE from eNews where id = {$id};";

        $this->db->query($sql);      

    }
}
