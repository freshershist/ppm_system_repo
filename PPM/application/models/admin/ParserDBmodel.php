<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *	@Class Name: usersmodel
 *  @description: all related transaction of users in the database
 *
 */

if(!class_exists('CI_Model')) { class CI_Model extends Model {} }

class ParserDBmodel extends CI_Model
{
	public function __construct()
    {
        parent::__construct();
    }

    public function check_members () 
    {
    	$query = $this->db->query("Select username from members");
    	echo '<pre>';
    	//print_r();

    	foreach ($query->result_array() as $key => $value) {
    		print_r($value);
    	}
    }

    public function users () 
    {
    	$query = $this->db->query("SELECT * FROM users_orig");

    	foreach ($query->result_array() as $value) {
			if(is_array($value)) {
				$this->set_users($value);
			}
		}
    }

    public function set_users ($arr = NULL) 
    {
    	$users_arr = array();
    	$users_meta_arr = array();
    	$username = '';
    	$uid = 0;

    	foreach ($arr as $key => $value) {
    		if($key === 'access' OR $key === 'userType') {
    			if($key === 'access') {
    				$users_meta_arr = array(
    					'uid' =>  $users_arr['uid'],
    					'meta' => 'access',
    					'value' => json_encode(explode(', ', $value)),
    					'extra' => ''
    					);

    				$this->db->insert('users_meta', $users_meta_arr);
    			}
    			else {
    				$users_meta_arr = array(
    					'uid' =>  $users_arr['uid'],
    					'meta' => 'userType',
    					'value' => $value,
    					'extra' => ''
    					);

    				$this->db->insert('users_meta', $users_meta_arr);
    			}
    		}
    		else {
    			//$data[$key] = 
    			//$this->db->insert('users', $data);

    			if($key === 'id') {
    				$users_arr['uid'] = $value;
    			}
    			elseif($key === 'username') {
    				$username = $value;
    				$users_arr[$key] = $value;
    			}
    			elseif($key === 'password') {
    				$salt = sha1($username . SECKEY);
    				$users_arr[$key] = md5($salt . $value);
    			}
    		}
    	}

    	//echo '<pre>';
    	//print_r($users_meta_arr);

    	$this->db->insert('users', $users_arr);
    }

    public function set_users_meta ($uid = NULL, $meta = NULL, $value = NULL, $extra = NULL)
    {

    }

    public function members ()
    {
    	$query = $this->db->query("SELECT * FROM members");

		//echo  '<pre>';


		
		
		//print_r($query->result_array());
		foreach ($query->result_array() as $value) {
			if(is_array($value)) {

				$users_arr = array();
				$members_arr = array();
				$members_posts_arr = array();
				$username = '';

				foreach ($value as $k => $v) {
					
					if($k === 'username') {
						if(!empty($v)){
							$username = $v;
							$users_arr[$k] = $v;
						}
					}
					elseif($k === 'password') {
						if(!empty($v)){
							$salt = sha1($username . SECKEY);
    						$users_arr[$k] = md5($salt . $v);
    					}
					}
					elseif($k !== 'comments' && $k !== 'generalAccountsComments') {

						$members_arr[] = array(
    						'uid' =>  NULL,
	    					'meta' => $k,
	    					'value' => $v,
	    					'extra' => ''
    					);

    					if($k === 'id') {

    						$this->db->where(array('memberId'=>$v));
    						$res = $this->db->get('membersCategories');

    						foreach ($res->result_array() as $k1 => $v1) {
    							if(is_array($v1)) {
								    $members_posts_arr['membersCategories']['posts']['mc_'.$k1][] = array(
										'title'=>'',
										'content'=>'',
										'uid'=> NULL,
										'type'=>'membersCategories'
									);
    								
    								foreach ($v1 as $k2=> $v2) {
    									if($k2 !== 'memberId') {
	    									$members_posts_arr['membersCategories']['meta']['mc_'.$k1][] = array(
					    						'mpid' =>  NULL,
						    					'meta' => $k2,
						    					'value' => $v2,
						    					'extra' => ''
					    					);

    									}
    								}
    							}
    							
    						}

    						$this->db->where(array('memberId'=>$v));
    						$res = $this->db->get('memberStaff');

    						foreach ($res->result_array() as $k1 => $v1) {
    							if(is_array($v1)) {
									$members_posts_arr['memberStaff']['posts']['mc_'.$k1][] = array(
										'title'=>'',
										'content'=>'',
										'uid'=> NULL,
										'type'=>'memberStaff'
									);

    								foreach ($v1 as $k2=> $v2) {
    									if($k2 !== 'memberId') {
											$members_posts_arr['memberStaff']['meta']['mc_'.$k1][] = array(
					    						'mpid' =>  NULL,
						    					'meta' => $k2,
						    					'value' => $v2,
						    					'extra' => ''
					    					);
    									}
    								}
    							}
    							
    						}    						
    					}
					}
				}

				if(!empty($username)) {
					$this->db->insert('users', $users_arr);
					$uid = $this->db->insert_id();
					//echo '<pre>';

					//print_r($members_posts_arr);

					//foreach ($members_posts_arr['posts'] as $mk => $mv) {
					//	print_r($members_posts_arr['meta'][$mk]);
					//}


					//die();

					foreach ($members_arr as $mv) {
						if(is_array($mv)) {
							$mv['uid'] = $uid;
							$this->db->insert('users_meta', $mv);
						}
					}

					$users_meta_arr = array(
    					'uid' =>  $uid,
    					'meta' => 'userType',
    					'value' => '2',
    					'extra' => ''
    					);

    				$this->db->insert('users_meta', $users_meta_arr);

    				if(isset($members_posts_arr['membersCategories']['posts'])) {
	    				foreach ($members_posts_arr['membersCategories']['posts'] as $index => $mv) {
							if(is_array($mv[0])) {
								$mv[0]['uid'] = $uid;

								$this->db->insert('members_posts', $mv[0]);
								$mpid = $this->db->insert_id();

								foreach ($members_posts_arr['membersCategories']['meta'][$index] as $mcmv) {
									$mcmv['mpid'] = $mpid;

									$this->db->insert('members_posts_meta', $mcmv);
								}
							}
						}
					}

					if(isset($members_posts_arr['memberStaff']['posts'])) {
	    				foreach ($members_posts_arr['memberStaff']['posts'] as $index => $mv) {
							if(is_array($mv[0])) {
								$mv[0]['uid'] = $uid;

								$this->db->insert('members_posts', $mv[0]);
								$mpid = $this->db->insert_id();

								foreach ($members_posts_arr['memberStaff']['meta'][$index] as $msmv) {
									$msmv['mpid'] = $mpid;

									$this->db->insert('members_posts_meta', $msmv);
								}
							}
						}
					}
				}
			}
		}
    }

    public function login($username, $pass) 
    {
    	$this->db->where(array('username'=>$username, 'password'=>$pass));
    	$res = $this->db->get('users');

    	echo '<pre>';

    	print_r($res->result_array());
    }

    public function set_values ($arr) 
    {
    	foreach ($arr as $table) {
    		$query = $this->db->get($table);
    		//echo '<br>';
    		//echo $query->num_rows() . ' rows';

    		if($query->num_rows()>0) {

				foreach ($query->result_array() as $value) {
					if(is_array($value)) {

						$posts_arr = array(
							'title'=>'',
							'content'=>'',
							'uid'=>1,
							'type'=>$table
						);

						$this->db->insert('posts', $posts_arr);
						$pid = $this->db->insert_id();

						foreach ($value as $k => $v) {
							$posts_meta_arr = array(
								'pid' =>  $pid,
								'meta' => $k,
								'value' => $v,
								'extra' => ''
							);

		    				$this->db->insert('posts_meta', $posts_meta_arr);
						}
					}
				}
    		}
    	}
    }

    public function banners($uid) 
    {
    	$query = $this->db->query("SELECT * FROM banners");

		//echo  '<pre>';

		//print_r($query->result_array());



		foreach ($query->result_array() as $value) {
			if(is_array($value)) {

				$posts_arr = array(
					'title'=>'',
					'content'=>'',
					'uid'=>1,
					'type'=>'banners'
				);

				$this->db->insert('posts', $posts_arr);
				$pid = $this->db->insert_id();

				foreach ($value as $k => $v) {
					$posts_meta_arr = array(
						'pid' =>  $pid,
						'meta' => $k,
						'value' => $v,
						'extra' => ''
					);

    				$this->db->insert('posts_meta', $posts_meta_arr);
				}
			}
		}
    }

    public function delegate_type () {
    	$query = $this->db->query("SELECT * FROM delegateType");

		//echo  '<pre>';

		//print_r($query->result_array());

		$posts_arr = array(
			'title'=>'Delegate Type',
			'content'=>'',
			'uid'=>1,
			'type'=>'delegateType'
		);

		$this->db->insert('posts', $posts_arr);
		$pid = $this->db->insert_id();

		foreach ($query->result_array() as $value) {
			if(is_array($value)) {

				foreach ($value as $k => $v) {
					$posts_meta_arr = array(
						'pid' =>  $pid,
						'meta' => $k,
						'value' => $v,
						'extra' => ''
					);

    				$this->db->insert('posts_meta', $posts_meta_arr);
				}
			}
		}
    }

    public function download_log () 
    {
    	
    	$query = $this->db->query("SELECT * FROM downloadLog");

		//echo  '<pre>';

		//print_r($query->result_array());

		$posts_arr = array(
			'title'=>'Download Log',
			'content'=>'',
			'uid'=>1,
			'type'=>'downloadLog'
		);

		$this->db->insert('posts', $posts_arr);
		$pid = $this->db->insert_id();

		foreach ($query->result_array() as $value) {
			if(is_array($value)) {

				foreach ($value as $k => $v) {
					$posts_meta_arr = array(
						'pid' =>  $pid,
						'meta' => $k,
						'value' => $v,
						'extra' => ''
					);

    				$this->db->insert('posts_meta', $posts_meta_arr);
				}
			}
		}    	
    }

}
