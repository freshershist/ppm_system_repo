<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *	@Class Name: usersmodel
 *  @description: all related transaction of users in the database
 *
 */

if(!class_exists('CI_Model')) { class CI_Model extends Model {} }

class Linksmodel extends CI_Model
{
	public function __construct()
    {
        parent::__construct();
    }

    public function get_categories() 
    {
        $sql = "SELECT id,name,parent from categories where area = 4 order by sort";

        return $this->db->query($sql)->result_array();
    }

    public function get_links_by_category($categoryid = NULL)
    {
        if(!empty($categoryid)) {
            $sql = "SELECT links.id,links.name,website,cost,expiry,displaymember,displaylinkspage,isactive from links,category where links.id = category.id and category.categoryid = {$categoryid} order by name;";
        }
        else {
            $sql = "SELECT id,name,website,cost,expiry,displaymember,displaylinkspage,isactive from links order by name;";
        }

        return $this->db->query($sql)->result_array();
    
    }

    public function get_links_category($linksId = NULL)
    {
        $sql = "SELECT name,isActive,pcontact,body,address,phone,fax,email,website,d1,d2,categoryID,nationalSubCat,postcode,expiry,cost,memberDiscounts,shortDesc,displaymember,displaylinkspage FROM  links,category  where links.ID = {$linksId} and links.id = category.id and category.area = 'l';";

        return $this->db->query($sql)->result_array();
    }

    public function add_update_link($id = NULL, $subCategory = NULL, $data = NULL)
    {
        if(!empty($subCategory)) {
            if(!empty($id)) {
                $this->db->where('id', $id);
                $this->db->update('links', $data);

                $sql = "DELETE from category where id = {$id} and area = 'l';";

                $this->db->query($sql);
            }
            else {
                $this->db->insert('links', $data);
                $id = $this->db->insert_id();
            }

            if(!empty($id)) {
                $arr = array('id'=>intval($id), 'categoryid'=>intval($subCategory), 'area'=>'l');

                $this->db->insert('category', $arr);
            }
            
            return $id;
        }
    }

    public function add_news($data = NULL)
    {
        $this->db->insert('news', $data);
        return $this->db->insert_id();
    }

    public function update_news($id = NULL, $data = NULL)
    {
        $this->db->where('id', $id);
        $this->db->update('news', $data);
    }

    public function update_news_categories($id = NULL, $categories = NULL, $topics = NULL)
    {
        $sql = "Delete from category where id = {$id} and area = 'n';";

        $this->db->query($sql);

        $cat_arr = array();

        if(is_array($categories) && !empty($categories)) {
            if(is_array($topics) && !empty($topics)) {
                $cat_arr = array_merge($categories, $topics);
            }
            else {
                $cat_arr = $categories;
            }

            $categories = array();

            foreach ($cat_arr as $value) {
                $categories[] = array('id' => $id,'categoryid' => $value,'area' => 'n');
            }

            $this->db->insert_batch('category', $categories);                  
        }
    }

    public function delete_link($id = NULL)
    {
        //DELETE from links where id = " & id & ";
        //DELETE from category where id = " & id & " and area = 'l';
        $sql = "DELETE from links where id = {$id};";

        $this->db->query($sql);

        $sql = "DELETE from category where id = {$id} and area = 'l';";

        $this->db->query($sql);        

    }
}
