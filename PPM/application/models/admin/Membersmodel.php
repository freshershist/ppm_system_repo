<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *	@Class Name: usersmodel
 *  @description: all related transaction of users in the database
 *
 */

if(!class_exists('CI_Model')) { class CI_Model extends Model {} }

class Membersmodel extends CI_Model
{
	public function __construct()
    {
        parent::__construct();

        $this->data_collect = array();
    }

    public function get_members_by_filter () 
    {
        $category_arr           = $this->input->post('category[]');
        $startDate              = $this->input->post('startDate');
        $endDate                = $this->input->post('endDate');
        $startDateR             = $this->input->post('startDateR');
        $endDateR               = $this->input->post('endDateR');
        $orderBy                = $this->input->post('orderBy');
        $keywords               = $this->input->post('keywords');
        $memberCategory_arr     = $this->input->post('memberCategory[]');
        $systemType             = $this->input->post('systemType');
        $reiMasterModules       = $this->input->post('reiMasterModules');
        $outstandingActionPlans = $this->input->post('outstandingActionPlans');
        $state_arr              = $this->input->post('state[]');
        $trustAccountProgram    = $this->input->post('trustAccountProgram');
        $reverseMatch           = $this->input->post('reverseMatch');
        $partnerId              = $this->input->post('partnerId');
        $memberStatus           = $this->input->post('memberStatus');
        $interestType           = $this->input->post('interestType');
        $staffPosition          = $this->input->post('staffPosition');
        $stateSubRegion_arr     = $this->input->post('stateSubRegion[]');
        $forum                  = $this->input->post('forum');
        $startDateU             = $this->input->post('startDateU');
        $endDateU               = $this->input->post('endDateU');
        $startDateC             = $this->input->post('startDateC');
        $endDateC               = $this->input->post('endDateC');
        $postcodeFrom           = $this->input->post('postcodeFrom');
        $postcodeTo             = $this->input->post('postcodeTo');
        $excludeSystemPresentation = $this->input->post('excludeSystemPresentation');
        

        $sql = ' WHERE 1=1 ';
        $conj = ' AND ';

        if(is_array($category_arr)) {
            $category = (count($category_arr)>0) ? '' : 'all';

            foreach ($category_arr as $cat) {
                if(is_numeric($cat)) {
                    if(intval($cat) < 6) {
                        $sql .= $conj . 'membershipType = ' . $cat . ' and memberStatus = 1';
                        $conj = ' OR ';
                    }
                    else {
                        if (intval($cat) === 7){
                        //searchingforum = true
                        }

                        $sql .= $conj . 'membershipType = ' . $cat;
                        $conj = ' OR ';
                    }
                }
                else {
                    if(strpos($cat, 'dbcat')>-1) {
                        $sql .= $conj . 'membershipType = ' . str_replace('dbcat','', $cat);
                        $conj = ' OR ';
                    }
                    else {
                        switch ($cat) {
                            case 'all':
                                $sql .= $conj . ' 1 = 1';
                                $conj = ' OR ';
                                break;

                            case 'ma':
                                $sql .= $conj . ' membershipType in (0,1,2,3) ';
                                $conj = ' OR ';
                                break;

                            case 'mc':
                                $sql .= $conj . ' memberStatus = 1 and membershipType in (0,1,2,3) ';
                                $conj = ' OR ';
                                break;

                            case 'ms':
                                $sql .= $conj . ' memberStatus = 2 and membershipType in (0,1,2,3) ';
                                $conj = ' OR ';
                                break;

                            case 'mCancel':
                                $sql .= $conj . ' memberStatus = 3 and membershipType in (0,1,2,3) ';
                                $conj = ' OR ';
                                break;

                            case '4a':
                                $sql .= $conj . '(membershipType = 4 and membershipExpires >= NOW() and memberStatus=1)';
                                $conj = ' OR ';
                                break;

                            case '4b':
                                $sql .= $conj . '(membershipType = 4 and memberStatus = 1 and membershipExpires < NOW())';
                                $conj = ' OR ';
                                break;

                            case '4c':
                                $sql .= $conj . '(membershipType = 4 and id in (select memberId from transactions where paymentType = 1 and paymentStatus = 1 group by memberId having count(1) > 1 ))';
                                $conj = ' OR ';
                                break;

                            case '4d':
                                $sql .= $conj . '(membershipType = 4 and id in (select memberId from transactions where paymentType = 1 and paymentStatus = 1 group by memberId having count(1) = 1))';
                                $conj = ' OR ';
                                break;

                            case '4e':
                                $sql .= $conj . 'membershipType = 4 and not dateUnsubscribed is Null';
                                $conj = ' OR ';
                                break;

                            case '4f':
                                $sql .= $conj . 'membershipType = 4 and memberStatus = 4';
                                $conj = ' OR ';
                                break;

                            case '5a':
                                $sql .= $conj . '(membershipType = 5 and membershipExpires >= NOW())';
                                $conj = ' OR ';
                                break;

                            case '5b':
                                $sql .= $conj . '(membershipType = 5 and memberStatus = 1 and membershipExpires < NOW())';
                                $conj = ' OR ';
                                break;

                            case '5c':
                            case '5d':
                                $sql .= $conj . '(membershipType = 5 and members.id in (select memberId from transactions where paymentType = 2 and paymentStatus = 1 group by memberId having count(1) > 1 ))';
                                $conj = ' OR ';
                                break;                                                                      

                            case '5e':
                                $sql .= $conj . 'membershipType = 5 and not dateUnsubscribed is Null';
                                $conj = ' OR ';
                                break;

                            case '5f':
                                $sql .= $conj . 'membershipType = 5 and memberStatus = 4';
                                $conj = ' OR ';
                                break;

                            case 'mNew':
                                $first_day_now = date('Y-m-01');

                                $sql .= $conj . 'membershipType in (0,1,2,3) and membershipCommencementDate > "' . $first_day_now . '"';
                                $conj = ' OR ';
                                break;

                            default:
                                break;
                        }
                    }
                }
            }

            if(strlen($sql)>0) {
                //$sql .= ')';
                $conj = ' AND ';
            }

            if(strlen($startDate)>0 && strlen($endDate)>0) {

                $stime = strtotime($this->ppmsystemlib->convert_au_time_to_standard($startDate));
                $etime = strtotime($this->ppmsystemlib->convert_au_time_to_standard($endDate));

                if(checkdate(date('m',$stime),date('d',$stime),date('Y',$stime)) && checkdate(date('m',$etime),date('d',$etime),date('Y',$etime))) {
                    $startDate = date('Y/m/d',$stime);
                    $endDate = date('Y/m/d',$etime);

                    $sql .= $conj . ' membershipExpires between "' . $startDate . '" and "' . $endDate . '"';
                    $conj = ' AND '; 
                }
            }

            if(strlen($startDateR)>0 && strlen($endDateR)>0) {
                $stime = strtotime($this->ppmsystemlib->convert_au_time_to_standard($startDateR));
                $etime = strtotime($this->ppmsystemlib->convert_au_time_to_standard($endDateR));

                if(checkdate(date('m',$stime),date('d',$stime),date('Y',$stime)) && checkdate(date('m',$etime),date('d',$etime),date('Y',$etime))) {
                    $startDateR = date('Y/m/d',$stime);
                    $endDateR = date('Y/m/d',$etime);

                    $sql .= $conj . ' members.id in (select memberId from transactions where myDateTime between "' . $startDateR . '" and "' . $endDateR . '")';
                    $conj = ' AND '; 
                }    
            }

            if(strlen($startDateU)>0 && strlen($endDateU)>0) {
                $stime = strtotime($this->ppmsystemlib->convert_au_time_to_standard($startDateU));
                $etime = strtotime($this->ppmsystemlib->convert_au_time_to_standard($endDateU));

                if(checkdate(date('m',$stime),date('d',$stime),date('Y',$stime)) && checkdate(date('m',$etime),date('d',$etime),date('Y',$etime))) {
                    $startDateU = date('Y/m/d',$stime);
                    $endDateU = date('Y/m/d',$etime);

                    $sql .= $conj . ' dateUnsubscribed between "' . $startDateU . '" and "' . $endDateU . '"';
                    $conj = ' AND '; 
                }    
            }

            if(strlen($startDateC)>0 && strlen($endDateC)>0) {
                $stime = strtotime($this->ppmsystemlib->convert_au_time_to_standard($startDateC));
                $etime = strtotime($this->ppmsystemlib->convert_au_time_to_standard($endDateC));

                if(checkdate(date('m',$stime),date('d',$stime),date('Y',$stime)) && checkdate(date('m',$etime),date('d',$etime),date('Y',$etime))) {
                    $startDateC = date('Y/m/d',$stime);
                    $endDateC = date('Y/m/d',$etime);

                    $sql .= $conj . ' dateCancelled between "' . $startDateC . '" and "' . $endDateC . '"';
                    $conj = ' AND '; 
                }
            }

            if(is_numeric($postcodeFrom) && is_numeric($postcodeTo)) {
                $sql .= $conj . ' IsNumeric(postcode) and CAST(postcode as signed) >= ' . intval($postcodeFrom) . ' and CAST(postcode as signed) <= ' . intval($postcodeTo);
                $conj = ' AND ';
            }

            if(!empty($keywords) && strlen($keywords)>0) {
                $keywords_arr = explode(' ', trim($keywords));

                $sql .= $conj . '((';

                foreach ($keywords_arr as $value) {
                    $sql .= '  company like "%' . $value . '%"';
                }

                //$sql .= ') or ( contactName like "%' . 

                echo json_encode(array($keywords_arr));die();
            }

            if(is_array($memberCategory_arr) && count($memberCategory_arr)>0) {
                if($reverseMatch === 'on') {
                    $sql .= $conj . " not members.id in (select memberId from membersCategories where categoryId In (" . implode(",", $memberCategory_arr) . ") and area = 10)";
                }
                else {
                    $sql .= $conj . " members.id in (select memberId from membersCategories where categoryId In (" . implode(",", $memberCategory_arr) . ") and area = 10)";
                }
            }

            if(!empty($excludeSystemPresentation) && $excludeSystemPresentation === 'on') {
                $sql .= $conj . ' systemPresentationDate is NULL';
                $conj = ' AND ';
            }

            if(!empty($systemType)) {
                $sql .= $conj . ' systemType = ' . $systemType;
            }

            if(!empty($outstandingActionPlans)) {
                if(intval($outstandingActionPlans) === 99) {
                    $sql .= $conj . ' (aPlan1 is Null or aPlan2 is Null or aPlan3 is Null or aPlan4 is Null or aPlan5 is Null or aPlan6 is Null or aPlan7 is Null or aPlan8 is Null)';
                    $conj = ' AND ';
                }
                else {
                    $sql .= $conj . ' aPlan' . $outstandingActionPlans . ' is Null';
                    $conj = ' AND ';
                }
            }

            if(is_array($state_arr) && count($state_arr) > 0) {
                $sql .= $conj . ' state in (' . implode(',', $state_arr) .')';;
                $conj = ' AND ';
            }

            if(is_array($stateSubRegion_arr) && count($stateSubRegion_arr)>0) {
                $sql .= $conj . ' stateSubRegion in (' . implode(',', $stateSubRegion_arr) . ')';
            }

            if(!empty($reiMasterModules)) {
                $sql .= $conj . ' members.id in (select memberId from membersCategories where categoryId = ' . $reiMasterModules . ' and area = 11)';
            }

            if(!empty($trustAccountProgram)) {
                $sql .= $conj . ' trustAccountProgram = ' . $trustAccountProgram;
            }

            if(!empty($memberStatus)) {
                $sql .= $conj . ' memberStatus = ' . $memberStatus;
            }

            if(!empty($interestType)) {
                switch (intval($interestType)) {
                    case 1:
                        $sql .= $conj . ' systemEnquiryDate is not null ';
                        break;

                    case 2:
                        $sql .= $conj . ' systemPresentationDate is not null ';
                        break;

                    case 3:
                        $sql .= $conj . ' trainingInterestDate is not null ';
                        break;

                    case 4:
                        $sql .= $conj . ' productInterestDate is not null ';
                        break;

                    case 5:
                        $sql .= $conj . ' conferenceInterestDate is not null ';
                        break;

                    case 6:
                        $sql .= $conj . ' onlineTrainingInterestDate is not null ';
                        break;
           
                    default:
                        break;
                }
            }

            if(!empty($forum)) {
                if(intval($forum) === 1) {
                    $sql .= $conj . ' id in (' . $this->dashboard->get_moderators('all') . ')';
                }
                elseif(intval($forum) === 2) {
                    $sql .= $conj . ' superStar = 1';
                }
            }

            $conj = " AND ";

            $sql .= " order by company";

            $sql = str_replace(' and memberStatus = 1', '', $sql);

        }

        $query = "SELECT id,company,memberShipCommencementDate,membershipExpires,nextAuditDue,membershipType,membershipPayments,billingCycle,membershipInvestment,memberStatus,totalConsultancyHoursPurchased - consultancyHoursUsed as consultingHoursRemaining,lockedByUser,lockExpires,state,contactName  from members " . $sql;

        //echo json_encode(array($query));die();

        //echo json_encode( array('query'=>$query, 'result'=> $this->db->query($query)->result_array()));

        return $this->db->query($query)->result_array();
    }

    public function get_entry($memberId = NULL, $isCopy = FALSE, $isCopyOLTraining = FALSE)
    {
        if(isset($memberId) && !empty($memberId)) {
            $sql = "SELECT * FROM members where id = " . $memberId . " limit 1";

            $arr['member'] = $this->db->query($sql)->result_array();

            $sql = "SELECT name,lastName,email,mobile,direct,myPosition from memberStaff where memberId = " . $memberId;

            $arr['staff'] = $this->db->query($sql)->result_array();

            if(!$isCopy) {

                $sql = "SELECT categoryId from membersCategories where memberId = " . $memberId . " and area = 10";

                $res = array();

                foreach ($this->db->query($sql)->result_array() as $value) {
                    $res[] = $value['categoryId'];
                }

                $arr['memberCategory'] = $res;

                $sql = "SELECT categoryId from membersCategories where memberId = " . $memberId . " and area = 11";

                $arr['reiMasterModules'] = $this->db->query($sql)->result_array();

                $sql = "SELECT name,dateDue,status,tasks.id ,username,myType,regenerate from tasks left join users on tasks.userID = users.id where memberId = " . $memberId . " order by dateDue desc";

                $arr['tasks'] = $this->db->query($sql)->result_array();

                $sql = "SELECT orderId,myDateTime,trxnReference,authCode,amount,paymentStatus from transactions where memberId = " . $memberId . " and paymentType in (1,2)";

                $arr['paymentHistories'] = $this->db->query($sql)->result_array();

                $sql = "SELECT tr.orderId,tr.myDateTime,tr.trxnReference,tr.authCode,tr.amount,tr.paymentStatus,p.name,p.code from transactions tr inner join transactionItems ti on ti.transactionId = tr.orderId inner join products p on p.id = ti.cartItemId where tr.memberId = " . $memberId . " and tr.paymentType in (4) and ti.itemType = 1";

                $arr['productsPurchased'] = $this->db->query($sql)->result_array();

                $sql = "SELECT tr.orderId,tr.myDateTime,tr.trxnReference,tr.authCode,tr.amount,tr.paymentStatus,e.name,e.id from transactions tr inner join transactionItems ti on ti.transactionId = tr.orderId inner join events e on e.id = ti.cartItemId where tr.memberId = " . $memberId . " and tr.paymentType in (4) and ti.itemType = 2 and ti.qty > 0 and not exists (select * from category ca where ca.area = 'e' and ca.id = e.id and categoryid in (30))";

                $arr['trainingAttended'] = $this->db->query($sql)->result_array();

                $sql = "SELECT tr.orderId,tr.myDateTime,tr.trxnReference,tr.authCode,tr.amount,tr.paymentStatus,e.name,e.id from transactions tr inner join transactionItems ti on ti.transactionId = tr.orderId inner join events e on e.id = ti.cartItemId where tr.memberId = " . $memberId . " and tr.paymentType in (4) and ti.itemType = 2 and ti.qty > 0 and exists (select * from category ca where ca.area = 'e' and ca.id = e.id and categoryid in (30))";

                $arr['conferenceDelegates'] = $this->db->query($sql)->result_array();   

                $sql = "SELECT forMonth,statId from monthlyAwardStats where memberId = " . $memberId . " order by forMonth";

                $arr['monthlyAwardStats'] = $this->db->query($sql)->result_array();

                $sql = "SELECT memberId from downloadLogENews where memberId = " . $memberId . ' limit 1';     

                $arr['isResetNewsletter'] = $this->db->query($sql)->num_rows();

                if(intval($arr['isResetNewsletter']) === 1) {
                    $sql = "SELECT id, name from eNews where category = 1 order by myDate desc";

                    $arr['newsletter'] = $this->db->query($sql)->result_array();
                }
            }
            else {
                $arr['memberCategory']      = array();
                $arr['reiMasterModules']    = array();
                $arr['tasks']               = array();
                $arr['paymentHistories']    = array();
                $arr['productsPurchased']   = array();
                $arr['trainingAttended']    = array();
                $arr['conferenceDelegates'] = array();
                $arr['isResetNewsletter']   = 0;
            }

            if($isCopy && !empty($arr['member'])) {
                $member = $arr['member'][0];

                if(!empty($member) && is_array($member)) {
                    if(array_key_exists('id',$member)) {
                        unset($member['id']);
                    }
                    
                    $memberId = $this->add_update(NULL, $member, NULL, $arr['staff'], $isCopy);

                    if(is_numeric($memberId) && $memberId > 0) {
                        $member['id'] = $memberId;

                        if($isCopyOLTraining) {
                            $member['membershipCommencementDate']   = date('Y-m-d H:i:s', now(PPM_TIMEZONE));
                            $member['membershipType']               = 5;
                            $member['membershipExpires']            = '2100-01-01 00:00:00';
                            $member['dateUnsubscribed']             = NULL;
                            $member['unsubscribeReason']            = NULL;
                            $member['membershipPayments']           = 8;
                            $member['memberStatus']                 = 1;
                            $member['membershipInvestment']         = NULL;

                            $data = array(
                                    'membershipCommencementDate'    => $member['membershipCommencementDate'],
                                    'membershipType'                => 5,
                                    'membershipExpires'             => '2100-01-01 00:00:00',
                                    'dateUnsubscribed'              => NULL,
                                    'unsubscribeReason'             => NULL,
                                    'membershipPayments'            => 8,
                                    'memberStatus'                  => 1,
                                    'membershipInvestment'          => NULL
                                );

                            $this->db->where('id', $memberId);
                            $this->db->update('members', $data);
                        }

                        $arr['member'][0] = $member;
                        
                        return $arr;
                    }
                    else {
                        return array();
                    }
                }

                return array();
            }

            return $arr;
        }

        return array();
    }

    public function get_logs($memberId = NULL, $logType = NULL, $emailType = NULL)
    {
        if(!empty($memberId) && !empty($logType)){
            $sql = '';

            if($logType === 'download-log') {

                $sql = "Select membershipType,state from members where id=" . $memberId;

                $result = $this->db->query($sql)->result_array(); 

                $membershipType = '';
                $state = '';

                foreach ($result as $value) {
                    $membershipType .= $value['membershipType'];
                    $state .= $value['state'];
                }

                switch ($membershipType) {
                    case '0':
                        $accessField = "Platnium";
                        break;
                    case '1':
                        $accessField = "Gold";
                        break;
                    case '2':
                        $accessField = "Silver";
                        break;
                    case '3':
                        $accessField = "Bronze";
                        break;
     
                    default:
                        $accessField = "Bronze";
                        break;
                }

                switch ($state) {
                    case '0':
                        $stateCategorySQL = " and downloads.id in (select id from category where categoryId = 111 and area = 'd') ";
                        break;
                    case '2':
                        $stateCategorySQL = " and downloads.id in (select id from category where categoryId = 106 and area = 'd') ";
                        break;
                    case '3':
                        $stateCategorySQL = " and downloads.id in (select id from category where categoryId = 104 and area = 'd') ";
                        break;
                    case '4':
                        $stateCategorySQL = " and downloads.id in (select id from category where categoryId = 103 and area = 'd') ";
                        break;
                    case '5':
                        $stateCategorySQL = " and downloads.id in (select id from category where categoryId = 110 and area = 'd') ";
                        break;
                    case '6':
                        $stateCategorySQL = " and downloads.id in (select id from category where categoryId = 109 and area = 'd') ";
                        break;
                    case '7':
                        $stateCategorySQL = " and downloads.id in (select id from category where categoryId = 108 and area = 'd') ";
                        break;
                    case '8':
                        $stateCategorySQL = " and downloads.id in (select id from category where categoryId = 107 and area = 'd') ";
                        break;
                    default:
                        $stateCategorySQL = "";
                        break;
                }

                $sql = "SELECT count(*) as count from downloads where " . $accessField . "=1 and mysection = 2 " . $stateCategorySQL;

                $query = $this->db->query($sql);

                $row = $query->row();

                $totalDownloads = 0;

                if (isset($row)) {
                    $totalDownloads = $row->count;
                }  

                //echo $totalDownloads;

                //die();

                $sql = "SELECT name,mydate,mysection from downloads,downloadLog where downloadLog.downloadId = downloads.id and downloadLog.memberId = " . $memberId . " order by myDate desc;";

                return $this->db->query($sql)->result_array(); 
            }
            elseif($logType === 'login-log'){
                $sql = "SELECT myDateTime,ip from log where description = 'Member " . $memberId . " logged in ok' order by myDateTime Desc;";

                return $this->db->query($sql)->result_array(); 
            }
            elseif($logType === 'email-log') {

                $sql = "SELECT subject,myDateTime,logId,emailAddress,fromAddress,sent,openDateTime from emailLog where memberId = " . $memberId;

                if(!empty($emailType) && $emailType != 'all') {
                    if($emailType === 'campaign') {
                        $sql .= " and (not emailQueueID is Null or myDateTime < '4-Jan-2010')";
                    }
                    else {
                        $sql .= " and emailQueueID is Null and myDateTime > '4-Jan-2010'";
                    }
                }

                $sql .= " order by myDateTime desc;";

                return $this->db->query($sql)->result_array(); 
            }
        }
    }

    public function add_update($id = NULL, $data = NULL, $memberCategory = NULL, $staff = NULL, $isCopy = FALSE)
    {

        $id = (is_numeric($id)) ? intval($id) : NULL;

        $affected_row = 0;

        $new_member = FALSE;

        if(!empty($id) && !empty($data)) {//UPDATE
            $this->db->update('members', $data, array('id'=>$id));

            $affected_row = $this->db->affected_rows();
            
        }
        elseif (!empty($data)) {//ADD
            $this->db->insert('members', $data);
            $id = $this->db->insert_id();
            $new_member = TRUE;
            $affected_row = $id;
        }

        if(is_array($memberCategory) && count($memberCategory)>0) {

            $this->db->delete('membersCategories', array('memberId'=>$id));

            foreach ($memberCategory as $value) {
                $arr = array(
                    'memberId'  => $id,
                    'categoryId'=> intval($value),
                    'area'      => 10
                    );

                $this->db->insert('membersCategories', $arr);
            }
        }
        else {
            $this->db->delete('membersCategories', array('memberId'=>$id));
        }

        if(!empty($staff) && count($staff)>0) {
            $this->db->delete('memberstaff', array('memberId'=>$id));

            if($isCopy) {
                foreach ($staff as $value) {
                    $value['memberId'] = $id;
                    $this->db->insert('memberstaff', $value);
                }
            }
            else {
                foreach ($staff as $value) {
                    if(is_array($value)) {
                        $arr = array(
                                'name'          => $value[0],
                                'lastName'      => $value[1],
                                'email'         => $value[2],
                                'mobile'        => $value[3],
                                'direct'        => $value[4],
                                'myposition'    => intval($value[5]),
                                'memberId'      => $id
                            );

                        $this->db->insert('memberstaff', $arr);
                    }
                }
            }
        }
        else {
            $this->db->delete('memberstaff', array('memberId'=>$id));
        }

/*        if(is_numeric($data['membershipType']) && $new_member && $data['membershipType'] === 4) {

            $arr = array(
                    'memberId'      =>$id,
                    'myDateTime'    =>date('Y-m-d H:i:s', now(PPM_TIMEZONE)),
                    'paymentStatus' =>1,
                    'trxnreference' =>'Manual Payment: ',
                    'amount'        =>154,
                    'paymentType'   =>1
                );

            $this->db->insert('transactions', )
        }*/

        return $affected_row;
    }

    public function reset_newsletter_downloads ($memberId = NULL, $eNewsID = NULL)
    {
        if(!empty($memberId) && !empty($eNewsID)) {
            $data = array('eNewsID'=>$eNewsID, 'memberId'=>$memberId);

            return $this->db->delete('downloadLogENews', $data);
        }

        return FALSE;
    }

    public function get_monthly_stats_details($statid = NULL)
    {
        $sql = "SELECT *, (Select company from members where id = mas.memberId limit 1) as memberName from monthlyAwardStats as mas where statId = {$statid};";

        return $this->db->query($sql)->result_array();
    }

    public function add_update_monthly_stats($id = NULL, $data = NULL)
    {
        if(empty($id)) {
            $this->db->insert('monthlyAwardStats', $data);
            $id = $this->db->insert_id();
        }
        else {
            $this->db->where('statId', $id);
            $this->db->update('monthlyAwardStats', $data);
        }

        return $id; 
    }

    public function delete_monthly_stats($statid = NULL, $memberId = NULL)
    {
        //DELETE from monthlyAwardStats where statId = " & Cint(statId) & " and memberId = "
        $sql = "DELETE from monthlyAwardStats where statId = {$statid} and memberId = {$memberId};";

        $this->db->query($sql);
    }  
}
