<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *	@Class Name: usersmodel
 *  @description: all related transaction of users in the database
 *
 */

if(!class_exists('CI_Model')) { class CI_Model extends Model {} }

class Pagesmodel extends CI_Model
{
	public function __construct()
    {
        parent::__construct();

        $this->data_collect = array();
    }

    public function get_pages ()
    {
		$query = "select * from `ppms_pages`";
    // $query = "select * from `users`";

		//echo json_encode(array($query));die();

		//return array('query'=>$query, 'result'=> $this->db->query($query)->result_array());

		return array('result'=> $this->db->query($query)->result_array());
    }

    public function get_page($page_id){
        $query = "select * from `ppms_pages` where `pid` = '".$page_id."'";
        // print_r($this->db->query($query)->result_array());
        return array('result'=> $this->db->query($query)->result_array());
    }

    public function update_menu(){
        // SELECT * FROM `ppms_pages` WHERE `pid`, `title`, `content`, `parent_id`, `menu`, `page_order`, `user_id`, `banner_top`, `banner_bottom`, `status`, `date_added`

        $query = "select * from `ppms_pages`";

        //echo json_encode(array($query));die();

        //return array('query'=>$query, 'result'=> $this->db->query($query)->result_array());

        return array('result'=> $this->db->query($query)->result_array());
    }

    public function save_page($page_id, $txtPageTitle, $editor_1, $pageStatus, $txtOrder, $selParentPage, $banner_top, $banner_bottom, $selMenu, $slug){
        // return $page_id;
        if(!empty($page_id)){
            //update
            $query = "update `ppms_pages` set 
                `title` = '".$txtPageTitle."', 
                `content` = '".$editor_1."', 
                `parent_id` = '".$selParentPage."', 
                `menu` = '".$selMenu."', 
                `page_order` = '".$txtOrder."', 
                `user_id` = '', 
                `banner_top` = '".$banner_top."', 
                `banner_bottom` = '".$banner_bottom."',  
                `status` = '".$pageStatus."',
                `slug` = '".$slug."'
                where 
                `pid` = '".$page_id."'
            "; 
            $this->db->query($query);
            $nid = $page_id;
            // $status = 'updated';
        }else{
            //insert
            $query = "insert into `ppms_pages` set 
                `title` = '".$txtPageTitle."', 
                `content` = '".$editor_1."', 
                `parent_id` = '".$selParentPage."', 
                `menu` = '".$selMenu."', 
                `page_order` = '".$txtOrder."', 
                `user_id` = '', 
                `banner_top` = '".$banner_top."', 
                `banner_bottom` = '".$banner_bottom."', 
                `status` = '".$pageStatus."',
                `slug` = '".$slug."',
                `date_added` = '".date('Y-m-d H:i:s')."'
            "; 
            $this->db->query($query);
            // $status = 'inserted';
            $nid = $this->db->insert_id();
        }

        return $nid;
        // return $page_id.' : '.$txtPageTitle.' : '.$editor_1.' : '.$pageStatus.' : '.$txtOrder;
    }

    public function delete_page($page_id){
        $query = "delete from `ppms_pages` where `pid` = '".$page_id."'"; 
        $this->db->query($query);
        $status = 'deleted';
        return $status;
    }

    public function is_parent_page($page_id){
        $query = "select * from `ppms_pages` where `parent_id` = '".$page_id."'";
        return array('result'=> $this->db->query($query)->result_array());
    }

    public function banner_list(){
        $query = "select * from `banners` where `activate` = '1'";
        return array('result'=> $this->db->query($query)->result_array());
    }

    public function banner_data($banner_id){
        $query = "select * from `banners` where `id` = '".$banner_id."' ";
        return array('result'=>$this->db->query($query)->result_array());
    }
}
