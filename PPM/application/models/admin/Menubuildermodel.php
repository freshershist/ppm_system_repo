<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *	@Class Name: usersmodel
 *  @description: all related transaction of users in the database
 *
 */

if(!class_exists('CI_Model')) { class CI_Model extends Model {} }

class Menubuildermodel extends CI_Model
{
	public function __construct()
    {
        parent::__construct();

        $this->data_collect = array();
    }

    public function get_list ()
    {
		$query = "select * from `menus`";
    // $query = "select * from `users`";

		//echo json_encode(array($query));die();

		//return array('query'=>$query, 'result'=> $this->db->query($query)->result_array());

		return array('result'=> $this->db->query($query)->result_array());
    }

    // public function get_page($page_id){
    //     $query = "select * from `ppms_pages` where `pid` = '".$page_id."'";
    //     // print_r($this->db->query($query)->result_array());
    //     return array('result'=> $this->db->query($query)->result_array());
    // }

    // public function update_menu(){
    //     // SELECT * FROM `ppms_pages` WHERE `pid`, `title`, `content`, `parent_id`, `menu`, `page_order`, `user_id`, `banner_top`, `banner_bottom`, `status`, `date_added`

    //     $query = "select * from `ppms_pages`";

    //     //echo json_encode(array($query));die();

    //     //return array('query'=>$query, 'result'=> $this->db->query($query)->result_array());

    //     return array('result'=> $this->db->query($query)->result_array());
    // }

    public function save_menuName($menu_id, $menuName){
        // return $page_id;
        if(!isset($menu_id)){ $menu_id=''; }
        if(!isset($menuName)){ $menuName=''; }

        $sql = "select * from `menus` where `name` = '".$menuName."'";
        $rs = $this->db->query($sql);
        $row = $rs->row();
        if (isset($row)){
            $status = 'exists';
        }else{
            if(!empty($bid)){
                //update
                $query = "update `menus` set 
                    `name` = '".$menuName."', 
                    where 
                    `menu_id` = '".$menu_id."'
                "; 
                $this->db->query($query);
                $status = 'updated';
            }else{
                //insert
                $query = "insert into `menus` set 
                    `name` = '".$menuName."', 
                    `menu_list` = '', 
                    `menu_data` = '',
                    `added_by` = '',
                    `date_added` = '".date('Y-m-d H:i:s')."'
                "; 
                $this->db->query($query);
                $status = $this->db->insert_id();
            }
        }


        return $status;
        // return $page_id.' : '.$txtPageTitle.' : '.$editor_1.' : '.$pageStatus.' : '.$txtOrder;
    }

    public function save_menuItems($menu_id, $menu_list, $menu_data){
        $query = "update `menus` set 
            `menu_list` = '".$menu_list."', 
            `menu_data` = '".json_encode($menu_data)."'
            where 
            `menu_id` = '".$menu_id."'
        "; 

        $this->db->query($query);

        return 'updated';
    }

    public function get_menu_details($menu_id){

        $query = "select * from `menus` where `menu_id` = '".$menu_id."'";

        return array('result'=> $this->db->query($query)->result_array());
    }

    public function delete_menu($menu_id){
        $query = "delete from `menus` where `menu_id` = '".$menu_id."'"; 
        $this->db->query($query);
        $status = 'deleted';
        return $status;
    }

    // public function is_parent_page($page_id){
    //     $query = "select * from `ppms_pages` where `parent_id` = '".$page_id."'";
    //     return array('result'=> $this->db->query($query)->result_array());
    // }
}
