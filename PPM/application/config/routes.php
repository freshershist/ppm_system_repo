<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

/**** ADMIN ****/

$route['admin'] 						= 'admin/Dashboard';
$route['admin/home'] 					= 'admin/Dashboard';
$route['admin/members/add']				= 'admin/Members/show_list';
$route['admin/members/list']			= 'admin/Members/show_list';
$route['admin/members/action']			= 'admin/Members/process_action';
$route['admin/members/logs']			= 'admin/Members/get_logs';
$route['admin/members/monthlystats']	= 'admin/Members/get_month_stats_details';
$route['admin/members/monthlystats/add_update'] = 'admin/Members/mas_add_update';
$route['admin/members/edit/(:num)']		= 'admin/Members/show_list/$1';
$route['admin/logout']					= 'admin/Login/logout';

$route['admin/gallery/list'] 			= 'admin/Gallery';
$route['admin/gallery/add'] 			= 'admin/Gallery';
$route['admin/gallery_delete'] 			= 'admin/Uploads/delete_uploaded_file';
$route['admin/gallery/get']				= 'admin/Gallery/get_gallery';
$route['admin/gallery/get_files']		= 'admin/Gallery/get_files_from_folder';
$route['admin/gallery/new_folder']		= 'admin/Gallery/create_new_folder';
$route['admin/gallery/get_list']		= 'admin/Gallery/get_list';
$route['admin/gallery/add_update']		= 'admin/Gallery/add_update';
$route['admin/gallery/action'] 			= 'admin/Gallery/process_action';

$route['admin/contents/get_list']		= 'admin/Contents/get_list';
$route['admin/contents/action']			= 'admin/Contents/process_action';
$route['admin/contents/add_update']		= 'admin/Contents/add_update';
$route['admin/contents/(:any)']			= 'admin/Contents';

$route['admin/products']        		= 'admin/Products/show_list'; 
$route['admin/products/add']        	= 'admin/Products/add_list'; 
$route['admin/pages']          			= 'admin/Pages/show_list'; 
$route['admin/pages/add']        		= 'admin/Pages/add_list'; 
$route['admin/pages/edit']        		= 'admin/Pages/add_list'; 
$route['admin/pages/edit/(:num)']    	= 'admin/Pages/edit_list/$1'; 
$route['admin/pages/delete']        	= 'admin/Pages/delete_list'; 

$route['admin/banners']					= 'admin/Banners/show_list';
$route['admin/banners/add']				= 'admin/Banners/add_list';
$route['admin/banners/edit']          	= 'admin/Banners/add_list';  
$route['admin/banners/edit/(:num)']     = 'admin/Banners/edit_list/$1'; 
$route['admin/banners/saveBanner']		= 'admin/Banners/saveBanner';
$route['admin/banners/saveCarousel']	= 'admin/Banners/saveCarousel';
$route['admin/banners/delete']			= 'admin/Banners/deleteBanner';

$route['admin/menus']					= 'admin/Menubuilder/show_list';
$route['admin/menubuilder']				= 'admin/Menubuilder/menubuilder';
$route['admin/menubuilder/edit/(:num)']	= 'admin/Menubuilder/menubuilder_edit/$1';
$route['admin/menubuilder/delete']		= 'admin/Menubuilder/menubuilder_delete';

$route['admin/testimonials']			= 'admin/Testimonials/show_list';
$route['admin/testimonials/add']		= 'admin/Testimonials/add_list';
$route['admin/testimonials/edit/(:num)']= 'admin/Testimonials/edit_list/$1';

$route['admin/events']          		= 'admin/Events/show_list'; 
$route['admin/events/list']        		= 'admin/Events/show_list'; 
$route['admin/events/add']        		= 'admin/Events/add_list';

$route['admin/links/get_list']			= 'admin/Links/get_list';
$route['admin/links/action']			= 'admin/Links/process_action';
$route['admin/links/add_update']		= 'admin/Links/add_update';
$route['admin/links/(:any)']			= 'admin/Links';

$route['admin/get_categories']			= 'admin/Dashboard/list_categories';
$route['admin/category/action']			= 'admin/Dashboard/process_action';
$route['admin/category/add_update']		= 'admin/Dashboard/add_update';

$route['admin/onlinetrainings']			= 'admin/OnlineTrainings/show_list';
$route['admin/onlinetrainings/list']	= 'admin/OnlineTrainings/show_list';
$route['admin/onlinetrainings/add']		= 'admin/OnlineTrainings/add_list';

$route['admin/awards'] 					= 'admin/Awards';
$route['admin/awards/list'] 			= 'admin/Awards';
$route['admin/awards/add'] 				= 'admin/Awards';
$route['admin/awards/get_list']			= 'admin/Awards/get_list';
$route['admin/awards/add_update']		= 'admin/Awards/add_update';
$route['admin/awards/action'] 			= 'admin/Awards/process_action';

$route['admin/users'] 					= 'admin/Users';
$route['admin/users/list'] 				= 'admin/Users';
$route['admin/users/add'] 				= 'admin/Users';
$route['admin/users/action']			= 'admin/Users/process_action';
$route['admin/users/add_update']		= 'admin/Users/add_update';

$route['admin/news']					= 'admin/News';
$route['admin/news/list'] 				= 'admin/News';
$route['admin/news/add'] 				= 'admin/News';
$route['admin/news/action']				= 'admin/News/process_action';
$route['admin/news/add_update']			= 'admin/News/add_update';

$route['admin/downloads']				= 'admin/Downloads';
$route['admin/downloads/list'] 			= 'admin/Downloads';
$route['admin/downloads/add'] 			= 'admin/Downloads';
$route['admin/downloads/action']		= 'admin/Downloads/process_action';
$route['admin/downloads/add_update']	= 'admin/Downloads/add_update';

$route['admin/promocodes']				= 'admin/Promocodes';
$route['admin/promocodes/list'] 		= 'admin/Promocodes';
$route['admin/promocodes/add'] 			= 'admin/Promocodes';
$route['admin/promocodes/action']		= 'admin/Promocodes/process_action';
$route['admin/promocodes/add_update']	= 'admin/Promocodes/add_update';

$route['admin/monthlyawardstats']		= 'admin/MonthlyAwardStats';

$route['admin/reporting']				= 'admin/Reporting';

$route['admin/homepage']				= 'admin/Homepage';
$route['admin/homepage/action']			= 'admin/Homepage/process_action';
$route['admin/homepage/add_update']		= 'admin/Homepage/add_update';

$route['admin/partners']				= 'admin/Partners';
$route['admin/partners/action']			= 'admin/Partners/process_action';
$route['admin/partners/add_update']		= 'admin/Partners/add_update';

/**** ADMIN ****/

/**** WEBSITE ****/

$route['ppmsystem-login']				= 'Members/login';
$route['ppmsystem-check']				= 'Members/check';
$route['ppmsystem-logout']				= 'Members/logout';

$route['newsletter-login']				= 'Newsletter/login';
$route['newsletter-check']				= 'Newsletter/check';
$route['newsletter-logout']				= 'Newsletter/logout';

$route['members']						= 'Members';
$route['members/(:any)']				= 'Members';
$route['members/(:any)/(:num)']			= 'Members';
$route['members/(:any)/(:num)/(:num)']	= 'Members';
$route['members/(:any)/(:any)']			= 'Members';
$route['members/(:any)/(:any)/(:num)']	= 'Members';

$route['enews/(:any)/(:any)/(:num)'] 	= 'Members/get_enews_downloads';
$route['newsletter']					= 'Newsletter';
$route['newsletter/(:any)']				= 'Newsletter';

$route['gallery']						= 'Gallery';
$route['gallery/(:any)']				= 'Gallery';
$route['gallery/(:any)/(:any)']			= 'Gallery';
$route['gallery/(:any)/(:any)/(:num)']	= 'Gallery';

$route['onlinetraining-login']			= 'OnlineTraining/login';
$route['onlinetraining-check']			= 'OnlineTraining/check';
$route['onlinetraining-logout']			= 'OnlineTraining/logout';
$route['onlinetraining']				= 'OnlineTraining';
$route['onlinetraining/(:any)']			= 'OnlineTraining';
$route['onlinetraining/(:any)/(:num)']	= 'OnlineTraining';

$route['enews']							= 'Pages/enews';
$route['articles']						= 'Pages/news_articles';
$route['articles/(:any)']				= 'Pages/news_articles';
$route['articles/(:any)/(:num)']		= 'Pages/news_articles';
$route['articles/(:any)/(:any)']		= 'Pages/news_articles';
$route['articles/(:any)/(:any)/(:num)']	= 'Pages/news_articles';

$route['ppm-tv']						= 'Pages/ppm_tv';
$route['ppm-tv/(:num)']					= 'Pages/ppm_tv';

$route['partners']						= 'Pages/industry_partners';
$route['partners/(:any)/(:num)']		= 'Pages/industry_partners';
$route['partners/(:any)/(:num)/(:num)']	= 'Pages/industry_partners';
$route['partners/(:any)/(:num)/(:num)/(:num)']	= 'Pages/industry_partners';
$route['partners/(:any)']				= 'Pages/industry_partners';

$route['contact-us']					= 'ContactUs';
$route['contact-us/(:num)']				= 'ContactUs/enquiry';
$route['contact-us/(:num)/(:num)']		= 'ContactUs/enquiry';
$route['contact-us/show/(:num)']		= 'ContactUs/enquiry';
$route['contact-us/(:num)/(:any)']		= 'ContactUs/enquiry';

$route['rent-roll']						= 'Pages/rent_roll';
$route['rent-roll/(:num)']				= 'Pages/rent_roll';

$route['property-management-awards-winners'] = 'Pages/award_finalists_winners';
$route['property-management-awards-winners/(:num)'] = 'Pages/award_finalists_winners';

$route['(:any)/gallery/(:num)']			= 'Pages/gallery';
$route['ppm-conference/(:any)']			= 'Pages/ppm_conference';

$route['events']						= 'Pages/events';
$route['events/(:any)']					= 'Pages/events';
$route['events/list/(:num)']			= 'Pages/events';
$route['events/show/(:num)/(:num)']		= 'Pages/events';

$route['products/(:any)']				= 'Pages/products';
$route['products/list/(:num)']			= 'Pages/products';
$route['products/list/(:num)/(:num)']	= 'Pages/products';
$route['products/show/(:num)/(:num)']	= 'Pages/products';

$route['ppmsystem/ppm/newsletter/(:any)'] = 'ppmsystem/ppm/Newsletter';
$route['ppmsystem/ppm/newsletter/(:any)/(:num)/(:any)'] = 'ppmsystem/ppm/Newsletter';

$route['ppmsystem/ppm/onlinetraining/(:any)'] = 'ppmsystem/ppm/OnlineTraining';
$route['ppmsystem/ppm/onlinetraining/(:any)/(:num)/(:any)'] = 'ppmsystem/ppm/OnlineTraining';

$route['ppmsystem/ppm/member/(:any)']	= 'ppmsystem/ppm/Members';
$route['ppmsystem/ppm/member/(:any)/(:num)/(:any)'] = 'ppmsystem/ppm/Members';

$route['testimonials']					= 'Testimonials';
$route['testimonials/list/(:num)']			= 'Testimonials';
$route['testimonials/list/(:num)/(:num)']	= 'Testimonials';
$route['testimonials/show/(:num)/(:num)']	= 'Testimonials';

// $route['ppmsystem/ppm/ordering']		= 'ppmsystem/ppm/Ordering';
$route['ppmsystem/ppm/ordering/delete_item']		= 'ppmsystem/ppm/Ordering/delete_item';
$route['ppmsystem/ppm/ordering/update_item']		= 'ppmsystem/ppm/Ordering/update_item';
$route['ppmsystem/ppm/ordering/reset_cart']		= 'ppmsystem/ppm/Ordering/reset_cart';
$route['ppmsystem/ppm/ordering/upgradecart']		= 'ppmsystem/ppm/Ordering/upgradecart';
$route['ppmsystem/ppm/ordering/checkout']		= 'ppmsystem/ppm/Ordering/checkout';
$route['ppmsystem/ppm/ordering/collect_attendees']	= 'ppmsystem/ppm/Ordering/collect_attendees';
$route['ppmsystem/ppm/ordering/details']	= 'ppmsystem/ppm/Ordering/details';
$route['ppmsystem/ppm/ordering/(:any)']		= 'ppmsystem/ppm/Ordering';

$route['home']							= 'Home';
$route['(:any)']						= 'Pages/random_pages';

/**** WEBSITE ****/

$route['default_controller'] 			= 'home';
$route['404_override'] 					= '';
$route['translate_uri_dashes'] 			= FALSE;
