<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class PPMSystemLib {
	protected $CI;
	
	public function __construct() 
	{		
        $this->CI = & get_instance();
        $this->CI->load->helper('date');
		$this->CI->load->helper("file");
		$this->CI->load->helper('directory');
		$this->CI->load->library("pagination");
		//$this->CI->load->library('unzip');
		$this->CI->load->library('image_lib');
		$this->CI->load->library('user_agent');

		$this->data_arr = array();
		$this->set_data_arr();

		//require_once APPPATH.'third_party/lib/eWay/RapidAPI.php';

	}

	public function createDropdown($name, $arr, $value, $isPlus = FALSE, $isEdit = FALSE, $isTextAsValue = FALSE, $optGroupLabel = '', $id = '', $defaultText = '')
	{

		if(!empty($optGroupLabel)) {
			if(!empty($id)) {
				$strSelect = '<select id="'.$id.'" name="'.$name.'" class="form-control"><optgroup label="'.$optGroupLabel.'"><option value="">[Select]</option>';
			}
			else {
				$strSelect = '<select id="'.$name.'" name="'.$name.'" class="form-control"><optgroup label="'.$optGroupLabel.'"><option value="">[Select]</option>';
			}
		}
		else {
			if(!empty($id)) {
				$strSelect = '<select id="'.$id.'" name="'.$name.'" class="form-control"><option value="">[Select]</option>';
			}
			else {
				$strSelect = '<select id="'.$name.'" name="'.$name.'" class="form-control"><option value="">[Select]</option>';
			}

			if(!empty($defaultText)) {
				$strSelect = '<select id="'.$name.'" name="'.$name.'" class="form-control"><option value="">'.$defaultText.'</option>';
			}
		}
		
		$selected = '';

		foreach ($arr as $key => $title) {
			$val = NULL;

			if(is_numeric($key) && !$isTextAsValue) {
				if($isPlus) {
					$val = (intval($key) + 1);
				}
				else {
					$val = intval($key);
				}

				if($isEdit && $val === intval($value)){
					$selected = 'selected';
				}
				else{
					$selected = '';
				}

			}
			else {
				if($isEdit && $key === $value){
					$selected = 'selected';
				}
				else{
					$selected = '';
				}

				$val = ($isTextAsValue)?$title:$key;
			}

			$strSelect .= '<option ' . $selected . ' value="' . $val .'">' . $title. '</option>';
		}

		if(!empty($optGroupLabel)) $strSelect .= '</optgroup>';

		$strSelect .= '</select>';

		return $strSelect;
	}

	public function compress_output($buffer = NULL)
	{
		ini_set("pcre.recursion_limit", "16777");

		$re = '%# Collapse whitespace everywhere but in blacklisted elements.
	        (?>             # Match all whitespans other than single space.
	          [^\S ]\s*     # Either one [\t\r\n\f\v] and zero or more ws,
	        | \s{2,}        # or two or more consecutive-any-whitespace.
	        ) # Note: The remaining regex consumes no text at all...
	        (?=             # Ensure we are not in a blacklist tag.
	          [^<]*+        # Either zero or more non-"<" {normal*}
	          (?:           # Begin {(special normal*)*} construct
	            <           # or a < starting a non-blacklist tag.
	            (?!/?(?:textarea|pre|script)\b)
	            [^<]*+      # more non-"<" {normal*}
	          )*+           # Finish "unrolling-the-loop"
	          (?:           # Begin alternation group.
	            <           # Either a blacklist start tag.
	            (?>textarea|pre|script)\b
	          | \z          # or end of file.
	          )             # End alternation group.
	        )  # If we made it here, we are not in a blacklist tag.
	        %Six';

	    $new_buffer = preg_replace($re, " ", $buffer);

	    // We are going to check if processing has working
		if ($new_buffer === NULL){
			$new_buffer = $buffer;
		}

		//$this->CI->output->set_output($new_buffer);
		$this->CI->output->_display($new_buffer);
	}

	public function create_folder($path = NULL, $folderName = NULL) 
	{
		if(!empty($folderName)) {
			if(empty($path)) {
				$pathToUpload = UPLOADFOLDER . '/files/' . $folderName;
			}
			else {
				$path = ($path === 'files/') ? '': $path;

				$pathToUpload = UPLOADFOLDER . '/files/' . $path . $folderName;
				$folderPath = $path;
			}

			$indexFile = UPLOADFOLDER . "/files/index.html";

			if ( ! file_exists($pathToUpload) ) {
				$created = mkdir($pathToUpload, DIR_WRITE_MODE, TRUE);

				if($created) {
					if ( ! write_file($pathToUpload . "/index.html", file_get_contents($indexFile))){}
					else {
						if(!empty($folderPath)) {
							$folderArr = explode('/', $folderPath);

							for($i=0;$i<count($folderArr);$i++){
								$str = '';
								for($k=0;$k<=$i;$k++){
									$str .= '/' . $folderArr[$k]; 
								}

								$pathToUploadIndex = UPLOADFOLDER . '/files' . $str;
								
								if ( ! file_exists($pathToUploadIndex . "/index.html") ) {
									write_file($pathToUploadIndex . "/index.html", file_get_contents($indexFile));	
								}
								
							}
						}
					}
				}

				return $created;
			}
			else {
				return false;
			}
		}

		return false;
	}

	public function get_files ($path = '', $fileType = 'image') //image|application|audio|video
	{
		$arr_files = array();

		$path = ($path === 'files/') ? '': $path;

		$map = directory_map(UPLOADFOLDER . '/files/' . $path, 1);

		foreach ($map as $key => $value) {

			if(is_string($value) && $value!=='index.html' && !is_numeric(strpos($value, '/'))) {
				if(file_exists(UPLOADFOLDER.'/files/'.$path.$value)){
					$mime_type = get_mime_by_extension(UPLOADFOLDER.'/files/'.$path.$value);

					if(!empty($mime_type) && is_numeric(strpos($mime_type, $fileType))) {
						$arr_info = get_file_info(UPLOADFOLDER.'/files/'.$path.$value);
						if(array_key_exists('server_path',$arr_info)) unset($arr_info['server_path']);
						$arr_files[] = $arr_info;
					}
				}
			}
		}

		$gallery_data['file_info'] = json_encode($arr_files);

		return $gallery_data;
	}

    public function convert_au_time_to_standard($date_str = '')
    {
        if(!empty($date_str)) {
            $str_arr = explode('/', $date_str);

            if(is_array($str_arr) && count($str_arr) === 3) {
                return $str_arr[1] . '/' . $str_arr[0] . '/' . $str_arr[2];
            }
        }

        return '';
    }		

	public function create_users_folder($email) 
	{
		$folder = $this->convert_letters_to_number($email);
		//update folder name
		$this->CI->usersmodel->update_folder_name($email, $folder);
		
		$pathToUpload = UPLOADFOLDER . '/' . $folder;
		
		if (!file_exists($pathToUpload)){
			$create = mkdir($pathToUpload, 0775, TRUE);
			$create2 = mkdir($pathToUpload . '/epub', 0775, TRUE);
		}
        else {
        	echo "folder exists";
        	die();
        }    

	}
	
	public function create_users_folder_exist ($folder) 
	{

		$pathToUpload = UPLOADFOLDER . '/' . $folder;

		if ( ! file_exists($pathToUpload) )
		{
			$create = mkdir($pathToUpload, 0775, TRUE);
			$create2 = mkdir($pathToUpload . '/epub', 0775, TRUE);

		}
	}	
	
	function convert_letters_to_number($string) 
	{
		
		$num = "";
		$x = 0;
		$newword = $this->randomize_number($string);
		
		while($x != strlen($string)) {
			$num .= abs(ord(strtolower($newword[$x])) - 96);
			$x++;	
		}
		
		return $num;
	}
	
	function randomize_number($num) 
	{
		$arr = array();
		$word = str_split($num);
		
		$x = 0;
		while( $x != strlen($num) ) {
			$arr[] = $word[$x];
			$x++;
		}
		shuffle( $arr );
		return $arr;
	}

    public function image_resize ($filepath) 
    {

    	if(file_exists($filepath)) {

			$config['image_library'] 	= 'gd2';
			$config['source_image'] 	= $filepath;
			$config['create_thumb'] 	= FALSE;
			$config['maintain_ratio'] 	= TRUE;
			$config['width']         	= 385;

			$this->CI->image_lib->initialize($config);

			$this->CI->image_lib->resize();
		}
    }

    public function isValidEmail($email) 
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL) !== false;
    }

    public function isValid_base64($str = "") 
    {

    	if(preg_match("/jpeg;base64/i", $str) || preg_match("/png;base64/i", $str) || preg_match("/gif;base64/i", $str)) {
    		return true;
    	}
    	else {
    		return false;
    	}
    }

	public function archive_user_files ($filename, $folder, $files) 
	{
		$this->CI->load->library('zip');

		foreach ($files as $key => $value) {
			if(file_exists(UPLOADFOLDER . "/" . $folder . "/" . $value['file_url'])) {
				$this->CI->zip->read_file(UPLOADFOLDER . "/" . $folder . "/" . $value['file_url']);
			}
		}
		
		$this->CI->zip->archive(UPLOADFOLDER . "/" . $folder . '/' . $filename);
	}

	public function get_accepted_mimes ()
	{
		return array(
			'image' => 'image/gif,image/jpeg,image/pjpeg,image/png,image/x-png',
			'audio' => 'audio/mpeg,audio/mpg,audio/mpeg3,audio/mp3',
			'video' => 'video/quicktime,video/x-msvideo,video/msvideo,video/avi,application/x-troff-msvideo,video/mp4,video/x-flv,video/x-ms-wmv,video/x-ms-asf',
			'application' => 'application/pdf,application/force-download,application/x-download,binary/octet-stream,application/vnd.ms-excel,application/msexcel,application/x-msexcel,application/x-ms-excel,application/x-excel,application/x-dos_ms_excel,application/xls,application/x-xls,application/excel,application/download,application/vnd.ms-office,application/msword,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/zip,application/vnd.ms-excel,application/msword,application/x-zip,application/msword,application/octet-stream,application/vnd.ms-office,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/zip,application/x-zip'
		);
	}

	public function get_user_agent() 
	{
		return $this->CI->agent->platform();
	}

	public function is_windows()
	{
		$agent = $this->CI->agent->platform();

		return (is_numeric(strpos($agent, 'Windows'))) ? TRUE : FALSE;
	}

	public function is_mac() 
	{
		$agent = $this->CI->agent->platform();

		return (is_numeric(strpos($agent, 'Mac'))) ? TRUE : FALSE;
	}

	public function check_date_time($dateTimeStr = NULL, $format = NULL)
	{
		if(!empty($dateTimeStr)){
			$dateTime = explode(' ', $dateTimeStr);

			if(count($dateTime) > 1) {
				$formatStr = (empty($format)) ? 'd/m/Y' : $format;

				if($dateTime[1] != '00:00:00') {
					return date($formatStr . ' h:i:s A', strtotime($dateTimeStr));
				}
				else {
					return date($formatStr, strtotime($dateTimeStr));
				}
			}
		}
		else {
			return '';
		}
	}

	public function set_final_date_time($date_str = NULL) 
	{
		if(!empty($date_str)) {
			$time = $this->convert_au_time_to_standard($date_str);

			return date('Y-m-d H:i:s', strtotime($time));
		}

		return '';
	}

	public function set_data_arr()
	{
		$this->data_arr = array(
			'nationalSubCatArray' 		=> array("Head Offices","Real Estate Institute","Business Assistance"),
			'paymentStatusArray' 		=> array("Not Processed","Successful","Cancelled/Refunded"),
			'yesNoUnsureArray' 			=> array("Yes","No","Unsure"),
			'attendeePositionArray' 	=> array("Assistant Property Manager", "Business Development", "Dept. Manager", "Inspection Officer", "Leasing Officer", "Maintenance Officer", "Principal", "Property Manager", "Receptionist", "Support/Admin", "Other"),
			'transactionHowHearArray' 	=> array("Franchise/Head Office","PPM Client Telephone Call","PPM Fax","PPM Mailout","PPM Marketing Email","PPM Training Event","Property Management Journal","REI Journal","Referral (Word of Month)","Other"),
			'stateArray' 				=> array("Queensland","New South Wales","Victoria","South Australia","Western Australia","Tasmania","Northern Territory","ACT","New Zealand"),
			'stateArrayOther' 			=> array("Queensland","New South Wales","Victoria","South Australia","Western Australia","Tasmania","Northern Territory","ACT","New Zealand","Other"),
			'stateArray1' 				=> array("Queensland","New South Wales","Victoria","South Australia","Western Australia","Tasmania","Northern Territory","ACT","New Zealand","All"),
			'stateArray2' 				=> array("Queensland","New South Wales","Victoria","South Australia","Western Australia","Tasmania","Northern Territory","ACT"),
			'stateSubRegionArray' 		=> array("Brisbane","Brisbane NW","Gold Coast","Sunshine Coast","Nth Qld"),
			'stateSubRegionOtherArray' 	=> array("CBD","Suburbs","Regional"),
			'rentRollAdType' 			=> array("All","For Sale","Wanted","Sold"),
			'recruitmentCategories' 	=> array("Super Star Candidates","Career Opportunities"),
			'stateArrayPolls' 			=> array("Queensland","New South Wales","Victoria","South Australia","Western Australia","Tasmania","Northern Territory","ACT","New Zealand","USA","United Kingdom","Asia","Other"),
			'typeOfInterestArray' 		=> array("System Enquiry","System Presentation","Training","Product","Conference","Online Training"),
			'unsubscribeReasonArray'	=> array("We do not have a property management dept","I don't have time to read the information","The email is being sent to the sales dept.","I am receiving duplicate emails","I don't remember subscribing to your Newsletter/eNews","I would like to write my own comment (complete the box provided)"),
			'awardTypeArray' 			=> array("Winner","Finalist"),
			'categoryArray' 			=> array("General News","Legislation News"),
			'sectionArray'				=> array("Download","System Upgrade","Conference Download")
		);
	}

	public function get_data_arr($arrName = NULL)
	{
		if(!empty($arrName) && array_key_exists($arrName, $this->data_arr)) {
			return $this->data_arr[$arrName];
		}
		else{
			return array();
		}
	}

	public function get_file_type_by_ext($filenameStr = NULL)
	{
		$ext = strtolower(pathinfo($filenameStr, PATHINFO_EXTENSION));

		if($ext === 'jpg' OR $ext === 'jpeg' OR $ext === 'gif' OR $ext === 'png') {
			$pathToUpload = UPLOADFOLDER . '/files/' . $filenameStr;
			
			if ( file_exists($pathToUpload) ){
				return 'img';
			}
			else {
				return 'removed';
			}
		}
		elseif($ext === 'mp4') {
			return 'mp4';
		}
		elseif($ext === 'mp3') {
			return 'mp3';
		}
		elseif($ext === 'swf'){
			return 'swf';
		}
		elseif($ext === 'zip') {
			return 'zip';
		}
		else{
			return 'doc';
		}
	}

	public function create_pagination($base_url = NULL, $total_rows = NULL, $per_page = NULL, $uri_segment = NULL, $num_links = NULL)
	{
        $this->CI->load->library('pagination');

        $config['base_url']         = $base_url;
        $config['total_rows']       = intval($total_rows);
        $config['per_page']         = $per_page;
        $config['uri_segment']      = $uri_segment;
        //$config['num_links']        = $num_links;
        $config['full_tag_open']    = '<ul class="pagination">';
        $config['full_tag_close']   = '</ul>';
        $config['num_tag_open']     = '<li>';
        $config['num_tag_close']    = '</li>';
        $config['first_link']       = 'First';
        $config['cur_tag_open']     = '<li class="active"><a href="javascript:;">';
        $config['cur_tag_close']    = '</a></li>';
        $config['next_tag_open']    = '<li>';
        $config['next_tag_close']   = '</li>';
        $config['prev_tag_open']    = '<li>';
        $config['prev_tag_close']   = '</li>';
        $config['first_tag_open']   = '<li>';
        $config['first_tag_close']  = '</li>';
        $config['last_tag_open']    = '<li>';
        $config['last_tag_close']   = '</li>';
        $config['reuse_query_string'] = TRUE;

        $this->CI->pagination->initialize($config);

        return $this->CI->pagination->create_links();
	}

	public function get_unix_from_date($dateStr = NULL)
	{
		if(!empty($dateStr)) {
			$date = mdate($dateStr);
			return human_to_unix($date);
		}
		else {
			return 0;
		}
	}

	public function get_days_left($startDate = NULL, $endDate = NULL)
	{
		/*
		1 hour					3600 seconds
		1 day					86400 seconds
		1 week					604800 seconds
		1 month (30.44 days) 	2629743 seconds
		1 year (365.24 days) 	31556926 seconds
		*/

		$startDate 	= $this->get_unix_from_date($startDate);
		$endDate 	= $this->get_unix_from_date($endDate);

		if(is_numeric($startDate) && is_numeric($endDate)) {
			$days = intval($endDate) - intval($startDate);
			$days = ($days > 0) ? $days / 86400 : 0;

			return ($days > 0) ? ceil($days) : 0;
		}
		else {
			return 0;
		}
	}

    public function send_email($message, $email, $subject) {

    	return TRUE;//temporary

    	date_default_timezone_set(PPM_TIMEZONE);
		
		$this->CI->load->library('email');
		
		$config['protocol']     = "sendmail";
        $config['charset']      = "/usr/sbin/sendmail";
        $config['mailtype']     = "html";
        $config['wordwrap'] 	= TRUE;
        $config['charset']      = "utf-8";
        $config['newline'] 		= "\r\n";

        $this->CI->email->initialize($config);
        
        $this->CI->email->from("webmail@ppmsystem.com", 'PPM System Contact');
        $this->CI->email->to($email);
        $this->CI->email->subject($subject);

        $this->CI->email->message($message);

        return $this->CI->email->send();	
		
	}

	public function is_unique($table = NULL, $data = NULL, $where_in = NULL, $where_in_col = NULL)
	{
		if(empty($where_in)) {
	    	return is_object($this->CI->db) ? ($this->CI->db->limit(1)->get_where($table, $data)->num_rows() === 0) : FALSE;
	    }
	    else {
	    	if(is_object($this->CI->db)) {
	    		$this->CI->db->where_in($where_in_col, $where_in);
	    		return ($this->CI->db->limit(1)->get_where($table, $data)->num_rows() === 0);
	    	}
	    	else {
	    		return FALSE;
	    	}
	    }

	    return FALSE;
	}

	public function money_format($format, $number)
	{
		$amount = doubleval($number);
		setlocale(LC_MONETARY,$format);
		return money_format("%n", $amount);
	}

	public function check_file_exists($file = NULL){

		if(!empty($file)) {
			return file_exists(UPLOADFOLDER . '/files/' . $file);
		}
		else {
			return FALSE;
		}
	}

	public function is_json($string,$return_data = false) {
		$data = json_decode($string);
		return (json_last_error() == JSON_ERROR_NONE) ? ($return_data ? $data : TRUE) : FALSE;
	}	
}
