<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('getPageTitle')){
	function getPageTitle($page_id){
	    $ci = & get_instance();
	    $ci->load->database(); 

	    $query = "select `title` from `ppms_pages` where `pid` = '".$page_id."'";
	    // return array('result'=> $ci->db->query($query)->result_array());
	    $result = $ci->db->query($query)->result_array();
	    if(count($result) > 0){
	    	return $result[0]['title'];	
	    }else{
	    	return '-';
	    }
	    
	}
}


if (! function_exists('display_menus')){
	function display_menus($menu_name){
	    $ci = & get_instance();
	    $ci->load->database(); 


	    function returnMenuTitle($item_id, $item_data){
	    	foreach ($item_data as $item_d) {
	    		# code...
	    		if($item_d->label_id == $item_id){
	    			return $item_d->label_name;
	    		}
	    	}
	    }

	    function returnMenuLink($item_id, $item_data){
	    	foreach ($item_data as $item_d) {
	    		# code...
	    		if($item_d->label_id == $item_id){
	    			return $item_d->label_link;
	    		}
	    	}
	    }

	    function buildItem($items, $item_data){
	        $html = "<li class='menu-item' data-id='" . $items->id . "'>";
	        $html .= "<a href='".base_url().returnMenuLink($items->id, $item_data)."'>" . returnMenuTitle($items->id, $item_data) . "</a>";
	        if (isset($items->children)) {
	            $html .= "<ul class='menu-list'>";
	            foreach ($items->children as $child ) {
	            	# code...
	            	$html .= buildItem($child, $item_data);
	            }
	            $html .= "</ul>";
	        }
	        $html .= "</li>";
	    	return $html;
	    }

	    // function buildItem($items, $item_data){
	    //     $html = "<li class='dd-item' data-id='" . $items->id . "'><div class=\"dd-handle dd3-handle\">Drag</div>";
	    //     $html .= "<div class='dd3-content'>" . returnMenuTitle($items->id, $item_data) . "</div>";
	    //     if (isset($items->children)) {
	    //         $html .= "<ol class='dd-list'>";
	    //         foreach ($items->children as $child ) {
	    //         	# code...
	    //         	$html .= buildItem($child, $item_data);
	    //         }
	    //         $html .= "</ol>";
	    //     }
	    //     $html .= "</li>";
	    // 	return $html;
	    // }

	    $query = "select * from `menus` where `name` = '".$menu_name."'";
	    $result = $ci->db->query($query)->result_array();
	    if(count($result) > 0){
	    	$menu_list_item =  json_decode($result[0]['menu_list']);
	    	$menu_data = json_decode($result[0]['menu_data']);
	    	$new_html ='';
		    foreach($menu_list_item as $list){
		    	$new_html .= buildItem($list, $menu_data);
		    }
		    echo $new_html;
	    }	    


	}
}

if(! function_exists('update_banner_table')){
	function update_banner_table(){
	 //    $ci = & get_instance();
	 //    $ci->load->database(); 
		// // $query = "ALTER TABLE `banners` ADD `parent_id` TEXT NULL AFTER `contentType`";
		// $query = "
		// 		IF NOT EXISTS( SELECT NULL
		// 		            FROM INFORMATION_SCHEMA.COLUMNS
		// 		           WHERE table_name = 'banners'
		// 		             AND table_schema = '".$db['default']['database']."'
		// 		             AND column_name = 'parent_id')  THEN
		// 		  ALTER TABLE `banners` ADD `parent_id` TEXT NULL AFTER `contentType`;
		// 		END IF;
		// ";
		// // $query = "ALTER TABLE `banners` ADD `parent_id` TEXT NULL AFTER `contentType`;";
		// $ci->db->query($query);
		// if(!empty($ci->db->_error_message())){
		// 	return false;
		// }
		return true;

		//ALTER TABLE `ppms_pages` ADD `slug` LONGTEXT NULL AFTER `status`;
	}
}

if(! function_exists('get_title_by_slug')){
	function get_title_by_slug($slug){
	    $ci = & get_instance();
	    $ci->load->database(); 

    	$sql = "select * from `ppms_pages` where `slug` = '".$slug."' and `status` = 'publish'";
    	$t = $ci->db->query($sql)->result_array();
    	if(empty($t)){
    		//do nothing
    		$a = str_replace('-', ' ', $slug);
    	}else{
		    foreach($t as $r){
		    	$a = $r['title'];
		    }
    	}

		return $a;
	}
}


if(! function_exists('render_page_content')){
	function render_page_content($alias_or_id){
	    $ci = & get_instance();
	    $ci->load->database(); 

	    if(is_numeric($alias_or_id)){
	    	//by ID
	    	$sql = "select * from `ppms_pages` where `pid` = '".$alias_or_id."' and `status` = 'publish'";
	    }else{
	    	//by name
	    	// $alias_or_id = urldecode($alias_or_id);
	    	$sql = "select * from `ppms_pages` where `slug` = '".$alias_or_id."' and `status` = 'publish'";
	    	$t = $ci->db->query($sql)->result_array();
	    	// print_r($t);
	    	if(empty($t)){
	    		$alias_or_id = str_replace('-', " ", $alias_or_id);
	    		$sql = "select * from `ppms_pages` where `title` like '".$alias_or_id."' and `status` = 'publish'";
	    	}
	    }

	    // echo $sql;
	    $result = $ci->db->query($sql)->result_array();

	    foreach($result as $r){
	    	$a = $r['content'];
	    }

	    if(empty($a)){
	    	$a = 'Page Not Found!';
	    }

	    return $a;
	}
}

if(! function_exists('render_menu')){
	function render_menu($alias_or_id){
	    $ci = & get_instance();
	    $ci->load->database(); 

	    if(is_numeric($alias_or_id)){
	    	//by ID
	    	$sql = "select * from `ppms_pages` where `pid` = '".$alias_or_id."' and `status` = 'publish'";
	    }else{
	    	//by name
	    	$sql = "select * from `ppms_pages` where `slug` = '".$alias_or_id."' and `status` = 'publish'";
	    	$t = $ci->db->query($sql)->result_array();
	    	if(empty($t)){
	    		$alias_or_id = str_replace('-', " ", $alias_or_id);
	    		$sql = "select * from `ppms_pages` where `title` like '".$alias_or_id."' and `status` = 'publish'";
	    	}	    	
	    }


	    $result = $ci->db->query($sql)->result_array();

	    foreach($result as $r){
	    	$a = $r['menu'];
	    }

	    if(!empty($a)){
		    $sql1 = "select * from `menus` where `menu_id` = '".$a."'";
			$result1 = $ci->db->query($sql1)->result_array();
		    
		    foreach($result1 as $r1){
		    	$a1 = $r1['name'];
		    }

			echo display_menus($a1);	    	
	    }else{
	    	return false;
	    }

	    // return $result1;
	}
}


if(! function_exists('render_featured_img')){
	function render_featured_img($alias_or_id, $pos = 'top'){
	    $ci = & get_instance();
	    $ci->load->database();
	    $ci->load->helper('string');

	    if(is_numeric($alias_or_id)){
	    	//by ID
	    	$sql = "select * from `ppms_pages` where `pid` = '".$alias_or_id."' and `status` = 'publish'";
	    }else{
	    	//by name
	    	$sql = "select * from `ppms_pages` where `slug` = '".$alias_or_id."' and `status` = 'publish'";
	    	$t = $ci->db->query($sql)->result_array();
	    	if(empty($t)){
	    		$alias_or_id = str_replace('-', " ", $alias_or_id);
	    		$sql = "select * from `ppms_pages` where `title` like '".$alias_or_id."' and `status` = 'publish'";
	    	}
	    }


	    $result = $ci->db->query($sql)->result_array();

	    foreach($result as $r){
	    	$a1 = $r['banner_top'];
	    	$a2 = $r['banner_bottom'];
	    }

	    if(!empty($a1) || !empty($a2)){
	    	if($pos == 'top'){
	    		$img = $a1;
	    	}else{
	    		$img = $a2;
	    	}
			echo '<div class="rotator" id="banner1"><div style="background-image:url(\'' . reduce_double_slashes($img) . '\');"><img src="'.reduce_double_slashes($img).'" class="visible-xs-block"></div></div>';
	    }else{
	    	return false;
	    }
	}
}

if(! function_exists('render_partners')) {
	function render_partners($alias = NULL){
	    $ci = & get_instance();
	    $ci->load->database();
	    $ci->load->helper('string');

	    $sql = 'Select photos from partnerspage where pages like "%' . $alias . '%" limit 1';

	    $result1 = $ci->db->query($sql)->result_array();
		
	    return $result1;
	}
}


if(! function_exists('menu_listing')){
	function menu_listing(){
	    $ci = & get_instance();
	    $ci->load->database(); 

	    $sql1 = "select * from `menus`";
		$result1 = $ci->db->query($sql1)->result_array();
		
	    return $result1;
	}
}


if(! function_exists('get_menu_name')){
	function get_menu_name($menu_id){
	    $ci = & get_instance();
	    $ci->load->database(); 

	    $sql1 = "select * from `menus` where `menu_id` = '".$menu_id."'";
		$result1 = $ci->db->query($sql1)->result_array();

	    foreach($result1 as $r1){
	    	$a = $r1['name'];
	    }
	    return $a;
	}
}


if(!function_exists('clean_string_dash')){
	function clean_string_dash($string) {
	   $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

	   return preg_replace('/[^A-Za-z0-9\-\.]/', '', $string); // Removes special chars.
	}
}


if (!function_exists('get_list_events')){
	function get_list_events(){
		$ci = & get_instance();
		$ci->load->database();
		
		$sql = "select * from `events`";
		$result = $ci->db->query($sql)->result_array();

		return $result;
	}
}

if(!function_exists('get_event')){
	function get_event($event_id){
		$ci = & get_instance();
		$ci->load->database();

		$sql = "select * from `events` where `id` = '{$event_id}' ";
		$result = $ci->db->query($sql)->result_array();

		return $result;
	}
}