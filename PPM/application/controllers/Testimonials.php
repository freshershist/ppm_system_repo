<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testimonials extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('text');
		$this->load->helper('ppmsystem');
		$this->load->library('PPMSystemLib');
		$this->load->model('frontend/Sitemodel', 'site');
	}

	public function index()
	{

		$data['title'] 	= 'Testimonials';

		$page 			= $this->uri->segment(2, '');

		$data['page'] 	= (empty($page)) ? 'home' : $page;

		switch ($page) {
			case '':
				$data['results'] = $this->site->get_testimonials_list_categories();
				break;

			case 'list':
				$category = $this->uri->segment(3, '0');

				$category = (is_numeric($category)) ? intval($category) : 0;

				$testimonials_page 						= $this->uri->segment(4, 0);

				$data['title']						= 'Testimonials - ' . $this->site->get_name_by_category($category);
				$data['category'] 					= $category;
				$data['testimonials_total'] 		= $this->site->get_testimonials($category, TRUE, $testimonials_page);
				$data['results'] 					= $this->site->get_testimonials($category, FALSE, $testimonials_page);
				$data['testimonials_start_row'] 	= (is_numeric($testimonials_page)) ? intval($testimonials_page)+1:1;
				$data['testimonials_end_row'] 		= (is_numeric($testimonials_page)) ? intval($testimonials_page)+count($data['results']) : 12;
				$data['testimonials_pagination']	=  $this->ppmsystemlib->create_pagination(base_url() . 'testimonials/list/'.$category.'/', $data['testimonials_total'], 10, 4, 9);
				break;
			
			default:
				# code...
				break;
		}

		$this->load->view('frontend/common/header', $data);

		$data['slug'] = 'about-testimonials';

		$this->load->view('frontend/testimonials', $data);
		$this->load->view('frontend/common/footer');
	}
}
