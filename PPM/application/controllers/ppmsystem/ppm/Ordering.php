<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ordering extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');

		$this->load->library('PPMSystemLib');
		$this->load->library('encryption');

		$this->load->model('frontend/Membersmodel', 'members');
		$this->load->model('frontend/Newslettersmodel', 'newsletters');
		$this->load->model('frontend/OnlineTrainingmodel', 'onlinetraining');

		$this->is_onlinetraining 	= $this->onlinetraining->is_logged_in();
		$this->is_newsletter 		= (isset($_SESSION['newsletter_logged_in']) && $_SESSION['newsletter_logged_in']) ? TRUE : FALSE;
		$this->is_ppmsystem 		= (isset($_SESSION['member_logged_in']) && $_SESSION['member_logged_in']) ? TRUE : FALSE;
		$this->linkedMemberId 		= '';
		$this->password 			= '';
		//$this->encryption->initialize(array('driver' => 'mcrypt'));
	}

	public function index()
	{

		$this->page_action = $this->uri->segment(4, 'addtocart');
		// session_destroy();
		// die();
		// $f = $this->session->cart_session;

		// unset($f[77]);
		// $this->session->set_userdata('cart_session', $f);
		// echo '<pre>';
		// print_r($this->session->cart_session);
		// die();

		if($this->page_action == 'addtocart'){
			$data['title'] = 'Ordering';
			$this->load->view('frontend/common/header', $data);

			//add to cart session here
			if(isset($_GET['isproduct'])){
				$id = $_GET['id'];
				$name = $_GET['name'];
				
				if(!isset($_GET['comp'])){
					$comp = '';
				}else{
					$comp = $_GET['comp'];
				}

				if(!isset($_GET['mytype'])){
					$mytype = '';
				}else{
					$mytype = $_GET['mytype'];
				}

				$postage = $_GET['postage'];

				if(!empty($postage)){
					$postage =  $this->encryption->decrypt($postage);
				}

				$price = $_GET['price'];
				if(!empty($price)){
					$price =  $this->encryption->decrypt($price);
				}

				/*==========================*/
				
				if(!isset($_GET['isconference'])){
					$isconference = '';
				}else{
					$isconference = $_GET['isconference'];
				}

				
				if(!isset($_GET['shortdesc'])){
					$shortdesc = '';
				}else{
					$shortdesc = $_GET['shortdesc'];
				}

				
				if(!isset($_GET['online'])){
					$online = '';
				}else{
					$online = $_GET['online'];
				}


			}else{
				if(isset($_GET['id'])){
					$id = $_GET['id'];
					$name = $_GET['name'];
					$mytype = $_GET['mytype'];

					if(!isset($_GET['online'])){
						$online = '';
					}else{
						$online = $_GET['online'];
					}

					$isconference = $_GET['isconference'];
					$shortdesc = $_GET['shortdesc'];
					$price = $_GET['price'];
					if(!empty($price)){
						$price =  $this->encryption->decrypt($price);
					}

					/*==========================*/
					
					if(!isset($_GET['comp'])){
						$comp = '';
					}else{
						$comp = $_GET['comp'];
					}

					if(!isset($_GET['mytype'])){
						$mytype = '';
					}else{
						$mytype = $_GET['mytype'];
					}

					
					if(isset($_GET['postage'])){
						$postage =  $this->encryption->decrypt($postage);
					}else{
						$postage = '';
					}					
				}else{
					$id = '';
					$name = '';
					$mytype = '';
					$online = '';
					$isconference = '';
					$shortdesc = '';
					$price = '';
				}

			}

			$the_cart = $this->session->cart_session;
			if(empty($the_cart)){
				$the_cart = array();
			}
			// echo '<pre>';
			// print_r($the_cart);

			// if(array_key_exists('id', $the_cart[])){
			// 	echo 'hello';
			// }else{
			// 	echo 'world';
			// }
			$len_cart = count($the_cart);
			$search_id = false;
			if($len_cart > 0){
				// for($i=0; $i<$len_cart; $i++){
				foreach($the_cart as $tc){
					if(array_key_exists('id', $tc) && $tc['id'] == $id){
						$search_id = true;
					}						
				}
			}
			// echo 'count'. count($the_cart);
			if(!$search_id && !empty($id)){
				$to_cart = array(
					'id'=>$id, 
					'shortdesc'=> $shortdesc, 
					'price'=> $price,
					'name' => $name,
					'postage' => $postage,
					'mytype' => $mytype,
					'isconference' => $isconference,
					'online' => $online,
					'comp' => $comp,
					'qty' => 1
					);
				// array_push($the_cart, $to_cart);
				$the_cart[$id] = $to_cart;
				$this->session->set_userdata('cart_session', $the_cart);				
			}

			$data['cart'] = $this->session->cart_session;

			$this->load->view('frontend/ppmsystem/ppm/ordering', $data);
			$this->load->view('frontend/common/footer');
		}else{
			redirect(base_url());
		}

	}

	public function delete_item(){
		$id = $this->input->post('id');
		// unset($this->session->cart_session[$id]);
		// print_r($this->session->cart_session);
		$f = $this->session->cart_session;

		unset($f[$id]);
		$this->session->set_userdata('cart_session', $f);
		echo '<pre>';
		print_r($this->session->cart_session);
		// die();
	}

	public function update_item(){
		$post = $this->input->post('cart_data');
		$cart_d = array_chunk($post, 2);

		$f = $this->session->cart_session;
		for($i = 0; $i < count($cart_d); $i++){
			$new_qty = $cart_d[$i][0];
			$id = $cart_d[$i][1];
			$f[$id]['qty'] = $new_qty;
		}
		$this->session->set_userdata('cart_session', $f);
		// print_r($cart_d);

		echo '<pre>';
		print_r($this->session->cart_session);
	}

	public function reset_cart(){
		$f = $this->session->cart_session;
		unset($f);
		$this->session->set_userdata('cart_session', $f);

		echo '<pre>';
		print_r($this->session->cart_session);
	}

	public function upgradecart(){
		$this->page_action = $this->uri->segment(4);
		if($this->page_action == 'upgradecart'){
			$data['title'] = 'Upgrade Cart';
			// echo '<pre>';
			// print_r($this->session->cart_session);
			$this->load->view('frontend/common/header', $data);
			$this->load->view('frontend/ppmsystem/ppm/upgrades', $data);
			$this->load->view('frontend/common/footer');
		}else{
			redirect(base_url().'ppmsystem/ppm/ordering/');
		}
	}

	public function checkout(){
		$this->page_action = $this->uri->segment(4);
		if($this->page_action == 'checkout'){
			$data['title'] = 'Checkout';
			// echo '<pre>';
			// print_r($this->session->cart_session);
			$data['cart'] = $this->session->cart_session;
			$data['cart_attendees'] = $this->session->cart_attendees;
			$data['page'] = 'checkout';
			$this->load->view('frontend/common/header', $data);
			$this->load->view('frontend/ppmsystem/ppm/checkout', $data);
			$this->load->view('frontend/common/footer');
		}else{
			redirect(base_url().'ppmsystem/ppm/ordering/');
		}
	}

	public function collect_attendees(){
		$ids = $this->input->post('ids');
		$id = explode(',', $ids);
		$is_empty = false;
		$the_attendees = array();
		foreach($id as $key => $val){
			// echo $val;
			$post = 'attendee_'.$val;
			$v = $this->input->post($post);
			$attendees = array_chunk($v, 2);
			for($i = 0; $i < count($attendees); $i++){
				$name = $attendees[$i][0];
				$position = $attendees[$i][1];
				$to_attendees = array(
					'id'=> $val, 
					'name'=> $name, 
					'position'=> $position
					);
				// array_push($the_cart, $to_cart);
				$the_attendees[$val] = $to_attendees;

				if(empty($name)){
					$is_empty = true;
				}

				if(empty($position) || $position == '-'){
					$is_empty = true;
				}



			}
		}

		if($is_empty){
			echo 'has_empty';
		}else{
			//move
			$this->session->set_userdata('cart_attendees', $the_attendees);	
		}
	}

	public function details(){
		$this->page_action = $this->uri->segment(4);
		if($this->page_action == 'details'){
			$data['title'] = 'Checkout Details';
			// echo '<pre>';
			// print_r($this->session->cart_session);
			$data['page'] = 'details';
			$data['cart'] = $this->session->cart_session;
			$data['cart_attendees'] = $this->session->cart_attendees;
			$this->load->view('frontend/common/header', $data);
			$this->load->view('frontend/ppmsystem/ppm/checkout', $data);
			$this->load->view('frontend/common/footer');
		}else{
			redirect(base_url().'ppmsystem/ppm/ordering/');
		}
	}
}
