<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Newsletter extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');

		$this->load->library('PPMSystemLib');

		$this->load->model('frontend/Membersmodel', 'members');
		$this->load->model('frontend/Newslettersmodel', 'newsletters');

		$this->is_newsletter 		= $this->newsletters->is_logged_in();
		$this->is_ppmsystem 		= (isset($_SESSION['member_logged_in']) && $_SESSION['member_logged_in']) ? TRUE : FALSE;
		$this->is_onlinetraining 	= (isset($_SESSION['onlinetraining_logged_in']) && $_SESSION['onlinetraining_logged_in']) ? TRUE : FALSE;		
	}

	public function index()
	{
		//echo $_SESSION['newsletterorderId']; die();
		//echo $this->ppmsystemlib->money_format("en_AU.UTF-8", NEWSLETTER_AMOUNT); die();

		$stage 				= $this->uri->segment(4, '');

		if(!empty($stage)) {
			$data['stage']	= $stage;
			$data['title'] 	= 'BECOME A NEWSLETTER SUBSCRIBER';

			$stateArrayOther = $this->ppmsystemlib->get_data_arr('stateArrayOther');

			$ctr = 1;
			$arr = array(''=>'[ Select ]');

			foreach ($stateArrayOther as $key => $value) {
				$arr[$ctr] = $value;
				$ctr++;
			}

			$post = $this->input->post();

			switch ($stage) {
				case 'step1':

					$memberId 			= $this->uri->segment(5, '');//(!empty($post)) ? $this->input->post('memberId') : 
					$memberIdConfirm	= $this->uri->segment(6, '');//(!empty($post)) ? $this->input->post('memberIdConfirm') : 
					$isRenewal 			= FALSE;

					if(!empty($memberId) && is_numeric($memberId)) {
						if($this->newsletters->is_logged_in()) {
							$isRenewal = $this->newsletters->confirm_member_renewal(intval($memberId), $memberIdConfirm);
						}
					}

					$data['stateArrayOther'] = $arr;

					if($isRenewal) {
						$results = $this->newsletters->get_member_renewal(intval($memberId));

						if(count($results) > 0) {
							$results['confirmemail'] 	= $results['emailAddress'];
							$results['memberId'] 		= $memberId;
							$results['memberIdConfirm'] = $memberIdConfirm;
						}

						$data['post'] = $results;
						$data['isRenewal'] = $isRenewal;

						$this->session->set_flashdata('isRenewal', TRUE);
					}
					else {
						if(!empty($post)) {
							if(isset($_SESSION['isRenewal']) && $_SESSION['isRenewal']) {
								$results = $this->validate_fields($stage, intval($this->input->post('memberId')), $this->input->post('memberIdConfirm'));
							}
							else {
								$results = $this->validate_fields($stage);
							}
							
							$valid = FALSE;
							$error = array();

							if($results['valid']) {
								$valid = TRUE;
							}
							else {
								$data['post'] 	= $post;
								$data['error'] 	= json_encode($results['error']);
							}

							if($valid) {
								$this->session->set_flashdata('step1_done', TRUE);
								$this->session->set_flashdata('isRenewal', $_SESSION['isRenewal']);
								$this->session->set_flashdata('form_post', $post);
								redirect(base_url() . 'ppmsystem/ppm/newsletter/step2');
							}
						}
					}					

					break;	

				case 'step2':

					if(isset($_SESSION['step1_done']) && $_SESSION['step1_done']) {

						if(isset($_SESSION['isRenewal']) && $_SESSION['isRenewal']) {
							$this->session->set_flashdata('isRenewal', TRUE);
							$data['isRenewal'] = TRUE;

							if(!isset($_SESSION['lockExpires'])) {
								$_SESSION['lockExpires'] = TRUE;
								$this->newsletters->lock_by_user($_SESSION['m_nl_id']);
							}
						}

						if(!empty($post)) {
							$company 			= $this->input->post('company');
							$contactName 		= $this->input->post('contactName');
							$contactNumber 		= $this->input->post('contactNumber');
							$address 			= $this->input->post('address');
							$address2 			= $this->input->post('address2');
							$suburb 			= $this->input->post('suburb');
							$postcode 			= $this->input->post('postcode');
							$state 				= $this->input->post('state');
							$emailAddress 		= $this->input->post('emailAddress');
							$username 			= $this->input->post('username');
							$password 			= $this->input->post('password');
							$landlordCount 		= $this->input->post('landlordCount');
							$memberId 			= $this->input->post('memberId');
							$memberIdConfirm 	= $this->input->post('memberIdConfirm');

							$results = $this->validate_fields($stage);
							
							$valid = FALSE;
							$error = array();

							if($results['valid']) {
								$valid = TRUE;
							}
							else {
								$this->session->set_flashdata('step1_done', TRUE);
								$data['post'] 	= $post;
								$data['error'] 	= json_encode($results['error']);
							}

							/*
							Visa - 4444333322221111
							Mastercard - 5105105105105100 / 2223000048400011 / 2223520043560014
							Amex - 378282246310005
							Diners - 38520000023237 or 30569309025904
							*/

							if(TRUE){//$valid) {
								if(TRUE){ //Process eWay here
									$data = array(
										'company' 		=> $company,
										'contactName' 	=> $contactName,
										'contactNumber' => $contactNumber,
										'address' 		=> $address,
										'address2' 		=> $address2,
										'suburb' 		=> $suburb,
										'postcode' 		=> $postcode,
										'state' 		=> intval($state),
										'emailAddress' 	=> $emailAddress,
										'username' 		=> $username,
										'password' 		=> $password,
										'landlordCount' => $landlordCount
									);

									$orderId = (isset($_SESSION['newsletterorderId'])) ? $_SESSION['newsletterorderId'] : '';

									if(empty($orderId)) {
										$arr = array(
											'paymentStatus'	=> 0,
											'memberId'		=> (!empty($memberId) && is_numeric($memberId)) ? intval($memberId) : NULL,
											'myDateTime'	=> date('Y-m-d h:i:s', now(PPM_TIMEZONE)),
											'amount'		=> doubleval(NEWSLETTER_AMOUNT),
											'paymentType'	=> 1
										);
										
										$orderId = $this->newsletters->add_transaction($arr);

										$_SESSION['newsletterorderId'] = $orderId;
									}

									if(!empty($memberId) && $_SESSION['isRenewal']) { //Renewal

										$membershipExpires 		= $_SESSION['m_nl_membershipExpires'];
										$membershipNextExpiry 	= strtotime('+1 year' , strtotime($membershipExpires));
										$unix_date 				= (is_numeric($membershipNextExpiry)) ? $membershipNextExpiry : $this->ppmsystemlib->get_unix_from_date($membershipNextExpiry);
										$membershipExpires 		= ($membershipNextExpiry < strtotime('+1 year' , now())) ? date('Y-m-d 00:00:00',strtotime('+1 year' , now())) : date('Y-m-d 00:00:00', $unix_date);

										$data['memberShipExpires']	= $membershipExpires;
										$data['lockedByUser'] 		= NULL;
										$data['lockExpires'] 		= NULL;

										$this->add_update($data, $memberId, $memberIdConfirm);
									}
									else { //NEW ENTRY

										$membershipExpires = (now(PPM_TIMEZONE) < intval(strtotime('2007-12-05'))) ? strtotime('+1 year' , strtotime('2007-12-05')) : strtotime('+1 year' , now(PPM_TIMEZONE));

										$data['memberShipExpires'] 			= date('Y-m-d 00:00:00', $membershipExpires);
										$data['memberShipType'] 			= 4;
										$data['membershipCommencementDate'] = date('Y-m-d h:i:s', now(PPM_TIMEZONE));
										$data['memberStatus'] 				= 1;
										$data['billingCycle'] 				= 1;
										$data['membershipPayments'] 		= 8;
										$data['howdidtheyhear'] 			= '';

										$this->add_update($data);
									}
								}
								else {
									$data['error'] = array('msg'=>'The credit card details you entered below are incorrect, or you have insufficient funds.');
								}
							}
						}
						else {
							$step1_done = $this->session->flashdata('step1_done');
							$form_post 	= $this->session->flashdata('form_post');
							if($step1_done) $this->session->set_flashdata('step1_done', $step1_done);

							$data['post'] 	= $form_post;
						}

						$data['stage'] 						= 'step2';
						$data['cardTypeArray'] 				= array(''=>'[ Select ]', 1=>"Visa", 2=>"Mastercard");

					}
					else {
						redirect(base_url() . 'ppmsystem/ppm/newsletter/step1');
					}

					break;

				case 'step3':

					if($this->session->flashdata('step2_done')) {
						$this->session->set_flashdata('step2_done', TRUE);
						$contactName = $this->session->flashdata('contactName');
						$this->session->set_flashdata('contactName', $contactName);
						$data['title'] = 'Newsletter Subscription Processed';
						$data['contactName'] = $contactName;

						$transaction_error = $this->session->flashdata('transaction_error');
						
						if(!empty($transaction_error)) {
							$data['transaction_error'] = $transaction_error;
						}
						
						$email_error = $this->session->flashdata('email_error');

						if(!empty($email_error)) {
							$arr = $this->session->flashdata('email_error');
							$msg = $this->load->view('frontend/ppmsystem/ppm/email_invoice', $arr, TRUE);

							if($this->ppmsystemlib->send_email($msg, $arr['emailAddress'], 'Tax Invoice: ' . $arr['orderId'])){
								if(!$this->ppmsystemlib->send_email($msg, EMAIL_INFO, 'New Subscriber: ' . $arr['orderId'])){
									$this->session->set_flashdata('email_error', $arr);
								}
							}
							else {
								$this->session->set_flashdata('email_error', $arr);
							}
						}
					}
					else {
						redirect(base_url() . 'ppmsystem/ppm/newsletter/step1');
					}

					break;
				
				default:
					redirect(base_url() . 'ppmsystem/ppm/newsletter/step1');
					break;
			}
			
			$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.css" rel="stylesheet">';
			$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">';

			$this->load->view('frontend/common/header', $data);

			$this->load->view('frontend/ppmsystem/ppm/newsletter', $data);
			
			$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.js"></script>';
			$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.buttons.js"></script>';
			
			$this->load->view('frontend/common/footer', $data);
		}
		else {
			redirect(base_url() . 'newsletter/signup');
		}
	}

	private function validate_fields($stage = NULL, $memberId = NULL)
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('company', '"Company "', 'required');
		$this->form_validation->set_rules('contactName', '"Central Contact Name "', 'required');
		$this->form_validation->set_rules('contactNumber', '"Contact Number "', 'required|is_numeric');
		$this->form_validation->set_rules('address', '"Postal Address (Line 1) "', 'required');
		$this->form_validation->set_rules('suburb', '"Suburb "', 'required');
		$this->form_validation->set_rules('postcode', '"Postcode "', 'required');
		$this->form_validation->set_rules('state', '"State/Country "', 'required');
		$this->form_validation->set_rules('emailAddress', '"Email Address "', 'required|valid_email');
		$this->form_validation->set_rules('confirmemail', '"Confirm Email Address "', 'required|valid_email|matches[emailAddress]');
		
		if(!empty($memberId) && is_numeric($memberId)) {
			$this->form_validation->set_rules('username', '"Username "', 'required|max_length[50]|alpha_numeric');
		}
		else {
			$this->form_validation->set_rules('username', '"Username "', 'required|max_length[50]|alpha_numeric|is_unique[members.username]');
			$this->form_validation->set_rules('company', '"Company "', 'required|callback_company_check');
			$this->form_validation->set_rules('emailAddress', '"Email Address "', 'required|valid_email|callback_email_check');
		}
		
		
		$this->form_validation->set_rules('password', '"Password "', 'required');
		$this->form_validation->set_rules('landlordCount', '"Approx. how many landlords will receive the newletter"', 'required|is_numeric');

		if($stage === 'step2') {
			$this->form_validation->set_rules('cardType', '"Card Type "', 'required|is_numeric');
			$this->form_validation->set_rules('creditcard', '"Credit Card Number "', 'required');
			$this->form_validation->set_rules('nameoncard', '"Name on card "', 'required|max_length[30]');
			$this->form_validation->set_rules('expiry', '"Expiry date "', 'required|max_length[4]|is_numeric');
			$this->form_validation->set_rules('cvv', '"CCV "', 'required');
		}

		if ($this->form_validation->run() == FALSE){
            return array("valid"=>FALSE, "error"=>$this->form_validation->error_array());
        }
        else {
        	return array("valid"=>TRUE);
        }
	}

	public function company_check($companyName)
	{
		$data = array(
			'company'=>$companyName,
			'membershipType'=>4
		);

		if(!$this->ppmsystemlib->is_unique('members', $data)) {
			$this->form_validation->set_message('company_check', 'The Company Name entered below appears to be attached to an existing Membership. Please login via this account to renew');
			return FALSE;
		}

		return TRUE;
	}

	public function email_check($emailAddress)
	{
		$data = array(
			'emailAddress'=>$emailAddress,
			'membershipType'=>4
		);

		if(!$this->ppmsystemlib->is_unique('members', $data)) {
			$this->form_validation->set_message('email_check', 'The Email Address entered below appears to be attached to an existing Membership. Please login via this email address to renew');
			return FALSE;
		}

		return TRUE;
	}	

	private function add_update($data = NULL, $memberId = NULL, $memberIdConfirm = NULL) {
		$isRenewal = FALSE;

		if(!empty($memberId) && is_numeric($memberId)) {
			if($this->newsletters->is_logged_in()) {
				//Security check - make sure that the user has provided the OLD password before updating the existing information
				if($this->newsletters->confirm_member_renewal(intval($memberId), $memberIdConfirm)){
					$isRenewal = TRUE;
				}
			}
		}

		$success = FALSE;

		$membershipType = '';

		if($isRenewal) {
			if($this->newsletters->add_update($data, intval($memberId)) > 0) {
				$_SESSION['m_nl_membershipExpires'] 	= $data['memberShipExpires'];
				$membershipType 				= 'Renewal';
				$success 						= TRUE;
			}
		}
		else {
			$memberId = $this->newsletters->add_update($data);

			if($memberId > 0) {
				$fullName 		= explode(' ', $data['contactName']);
				$firstName 		= '';
				$lastName 		= '';
				$membershipType = 'New';

				if(count($fullName) > 1) {
					$firstName 	= $fullName[0];
					$lastName 	= $fullName[1];
				}
				else {
					$firstName 	= $data['contactName'];
				}

				$arr = array(
					'memberId'	=> $memberId,
					'name'		=> $firstName,
					'lastName'	=> $lastName,
					'email'		=> $data['emailAddress']
				);

				$this->newsletters->add_member_staff($arr);

				$success = TRUE;
			}
		}

		if($success) {

			$arr = array(
				'paymentStatus' => 1,
				'trxnReference' => 'test',
				'authCode' => 'test',
				'memberId' => intval($memberId),
				'paymentMethod' => 'creditcard-eway'
			);

			if(!$this->newsletters->update_transaction(intval($_SESSION['newsletterorderId']), $arr)) {
				$this->session->set_flashdata('transaction_error', 'Cannot find order - please contact this store quoting orderId: ' . $_SESSION['newsletterorderId']);
			}

			$orderId = $_SESSION['newsletterorderId'];
			
			unset($_SESSION['newsletterorderId']);

			$amount = $this->ppmsystemlib->money_format("en_AU.UTF-8", NEWSLETTER_AMOUNT);

			$arr = array(
				'amount'			=> $amount,
				'orderId'			=> $orderId,
				'membershipType'	=> $membershipType,
				'company'			=> $data['company'],
				'contactName'		=> $data['contactName'],
				'contactNumber'		=> $data['contactNumber'],
				'address'			=> $data['address'],
				'emailAddress'		=> $data['emailAddress'],
				'landlordCount'		=> $data['landlordCount'],
				'memberShipExpires' => $data['memberShipExpires']
			);

			$msg = $this->load->view('frontend/ppmsystem/ppm/email_invoice', $arr, TRUE);

			if($this->ppmsystemlib->send_email($msg, $arr['emailAddress'], 'Tax Invoice: ' . $orderId)){
				if(!$this->ppmsystemlib->send_email($msg, EMAIL_INFO, 'New Subscriber: ' . $orderId)){
					$this->session->set_flashdata('email_error', $arr);
				}
			}
			else {
				$this->session->set_flashdata('email_error', $arr);
			}

			$this->session->set_flashdata('step2_done', TRUE);
			$this->session->set_flashdata('contactName', $data['contactName']);
			redirect(base_url() . 'ppmsystem/ppm/newsletter/step3');
		}
	}
}
