<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class OnlineTraining extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');

		$this->load->library('PPMSystemLib');
		$this->load->library('encryption');

		$this->load->model('frontend/Membersmodel', 'members');
		$this->load->model('frontend/Newslettersmodel', 'newsletters');
		$this->load->model('frontend/OnlineTrainingmodel', 'onlinetraining');

		$this->is_onlinetraining 	= $this->onlinetraining->is_logged_in();
		$this->is_newsletter 		= (isset($_SESSION['newsletter_logged_in']) && $_SESSION['newsletter_logged_in']) ? TRUE : FALSE;
		$this->is_ppmsystem 		= (isset($_SESSION['member_logged_in']) && $_SESSION['member_logged_in']) ? TRUE : FALSE;
		$this->linkedMemberId 		= '';
		$this->password 			= '';
		//$this->encryption->initialize(array('driver' => 'mcrypt'));
	}

	public function index()
	{

		/*
		$this->load->library('encryption');
		$this->encryption->initialize(array('driver' => 'mcrypt'));
		
		$plain_text = '99';
		$ciphertext = $this->encryption->encrypt($plain_text);
		
		echo $ciphertext;
		echo $this->encryption->decrypt($ciphertext);
		
		die();
		*/

		//echo $_SESSION['newsletterorderId']; die();
		//echo $this->ppmsystemlib->money_format("en_AU.UTF-8", NEWSLETTER_AMOUNT); die();

		$stage 				= $this->uri->segment(4, '');

		if(!empty($stage)) {
			$data['stage']	= $stage;
			$data['title'] 	= 'Become an Online Training Subscriber';

			$stateArrayOther = $this->ppmsystemlib->get_data_arr('stateArrayOther');

			$ctr = 1;
			$arr = array(''=>'[ Select ]');

			foreach ($stateArrayOther as $key => $value) {
				$arr[$ctr] = $value;
				$ctr++;
			}

			$post = $this->input->post();

			switch ($stage) {
				case 'step1':

					$data['annual_investment'] = '';

					$memberId 			= $this->uri->segment(5, '');
					$memberIdConfirm	= $this->uri->segment(6, '');
					$isRenewal 			= FALSE;


					if(!empty($memberId) && is_numeric($memberId) && !empty($memberIdConfirm)) {
						if($this->is_onlinetraining) {
							$isRenewal = $this->onlinetraining->confirm_member(intval($memberId), $memberIdConfirm);
						}
						elseif($this->is_ppmsystem) {
							if($this->onlinetraining->confirm_member(intval($memberId), $memberIdConfirm)) {
								$data['post'] 						= $this->onlinetraining->get_member_info(intval($memberId));
								$data['post']['memberId'] 			= '';
								$data['post']['linkedMemberId'] 	= $memberId;
								$data['post']['memberIdConfirm'] 	= $memberIdConfirm;
								$data['post']['confirmemail']		= $data['post']['emailAddress'];

								$data['annual_investment'] = $this->get_amount_label();
							}
						}
					}
					else {
						if($this->is_ppmsystem) {
							$data['annual_investment'] = $this->get_amount_label(); 
						}
					}				

					$data['stateArrayOther'] = $arr;

					if($isRenewal) {
						$results = $this->onlinetraining->get_member_info(intval($memberId));

						if(count($results) > 0) {
							$results['confirmemail'] 	= $results['emailAddress'];
							$results['memberId'] 		= $memberId;
							$results['memberIdConfirm'] = $memberIdConfirm;
							//$results['linkedMemberId']	= $results['linkedmemberid'];

							$data['annual_investment'] = $this->get_amount_label();					
						}

						$data['post'] = $results;
						$data['isRenewal'] = $isRenewal;

						$this->session->set_flashdata('isRenewal', TRUE);

					}
					else {
						if(!empty($post)) {

							$this->linkedMemberId = $this->input->post('linkedMemberId');

							$id = ($this->is_ppmsystem) ? $this->linkedMemberId : $this->input->post('memberId');

							$amount = $this->get_amount($id);

							$this->session->set_flashdata('amount', $amount);							

							$results = $this->validate_fields($stage);
							
							$valid = FALSE;
							$error = array();

							if($results['valid']) {
								$valid = TRUE;
							}
							else {
								$data['post'] 	= $post;
								$data['error'] 	= json_encode($results['error']);
							}

							if($valid) {
								$this->session->set_flashdata('step1_done', TRUE);
								$this->session->set_flashdata('isRenewal', $_SESSION['isRenewal']);
								$this->session->set_flashdata('form_post', $post);
								redirect(base_url() . 'ppmsystem/ppm/onlinetraining/step2');
							}
						}
					}

					break;	

				case 'step2':

					$memberId 			= (!empty($post)) ? $this->input->post('memberId') : $this->uri->segment(5, '');
					$memberIdConfirm	= (!empty($post)) ? $this->input->post('memberIdConfirm') : $this->uri->segment(6, '');
					$isRenewal 			= FALSE;
					$amount 			= ONLINETRAINING_GENERAL_AMOUNT;

					if(!empty($memberId) && is_numeric($memberId) && !empty($memberIdConfirm)) {
						if($this->is_onlinetraining) {
							$isRenewal = $this->onlinetraining->confirm_member(intval($memberId), $memberIdConfirm);
							$this->session->set_flashdata('isRenewal', $isRenewal);
							$this->session->set_flashdata('step1_done', $isRenewal);

							$result = ($isRenewal) ? $this->onlinetraining->get_member_info(intval($memberId)) : NULL;

							if(!empty($result)) {
								$arr = array();	

								$arr['company'] 			= $result['company'];
								$arr['contactName'] 		= $result['contactName'];
								$arr['contactNumber'] 		= preg_replace('/\D/', '', $result['contactNumber']);//strip other characters except numbers
								$arr['address'] 			= $result['address'];
								$arr['address2'] 			= $result['address2'];
								$arr['suburb'] 				= $result['suburb'];
								$arr['postcode'] 			= $result['postcode'];
								$arr['state'] 				= $result['state'];
								$arr['username'] 			= $result['username'];
								$arr['password']			= $result['password'];
								$arr['memberId'] 			= $memberId;
								$arr['linkedMemberId'] 		= $result['linkedmemberid'];
								$arr['memberIdConfirm'] 	= $memberIdConfirm;
								$arr['emailAddress']		= $result['emailAddress'];
								$arr['confirmemail']		= $result['emailAddress'];

								$this->linkedMemberId 		= $result['linkedmemberid'];

								$this->session->set_flashdata('form_post', $arr);

								$id = (!empty($result['linkedmemberid'])) ? $result['linkedmemberid'] : $result['id'];

								$amount = $this->get_amount($id);

								$this->session->set_flashdata('amount', $amount);
								$data['amount'] = $amount;						
							}
						}			
					}
					else {
						$amount = $this->session->amount;

						$this->session->set_flashdata('amount', $amount);
						$data['amount'] = $amount;
					}

					if(isset($_SESSION['step1_done']) && $_SESSION['step1_done']) {

						if(isset($_SESSION['isRenewal']) && $_SESSION['isRenewal']) {

							$data['title'] 	= 'Online Training Subscriber Renewal';

							$this->session->set_flashdata('isRenewal', TRUE);
							$data['isRenewal'] = TRUE;

							$amount = $this->session->amount;

							$this->session->set_flashdata('amount', $amount);
							$data['amount'] = $amount;						

							if(!isset($_SESSION['lockExpires'])) {
								$_SESSION['lockExpires'] = TRUE;
								$this->onlinetraining->lock_by_user($_SESSION['ot_id']);
							}
						}

						if(!empty($post)) {
							$company 			= ($isRenewal) ? $post['company'] 			: $this->input->post('company');
							$contactName 		= ($isRenewal) ? $post['contactName'] 		: $this->input->post('contactName');
							$contactNumber 		= ($isRenewal) ? $post['contactNumber'] 	: $this->input->post('contactNumber');
							$address 			= ($isRenewal) ? $post['address'] 			: $this->input->post('address');
							$address2 			= ($isRenewal) ? $post['address2'] 			: $this->input->post('address2');
							$suburb 			= ($isRenewal) ? $post['suburb'] 			: $this->input->post('suburb');
							$postcode 			= ($isRenewal) ? $post['postcode'] 			: $this->input->post('postcode');
							$state 				= ($isRenewal) ? $post['state'] 			: $this->input->post('state');
							$emailAddress 		= ($isRenewal) ? $post['emailAddress'] 		: $this->input->post('emailAddress');
							$username 			= ($isRenewal) ? $post['username'] 			: $this->input->post('username');
							$password 			= ($isRenewal) ? $post['password'] 			: $this->input->post('password');
							$memberId 			= ($isRenewal) ? $memberId 					: $this->input->post('memberId');
							$memberIdConfirm 	= ($isRenewal) ? $memberIdConfirm 			: $this->input->post('memberIdConfirm');
							$linkedMemberId 	= ($isRenewal) ? $this->linkedMemberId 		: $this->input->post('linkedMemberId');

							if($isRenewal) {
								$results = $this->validate_fields($stage, intval($memberId), $memberIdConfirm);
							}
							else {
								if($this->is_ppmsystem) {
									$results = $this->validate_fields($stage, intval($linkedMemberId), $memberIdConfirm);
								}
								else {
									$results = $this->validate_fields($stage);
								}
							}
							
							$valid = FALSE;
							$error = array();

							if($results['valid']) {
								$valid = TRUE;
							}
							else {
								$this->session->set_flashdata('step1_done', TRUE);
								$data['post'] 	= $post;
								$data['error'] 	= json_encode($results['error']);
							}

							/*
							Visa - 4444333322221111
							Mastercard - 5105105105105100 / 2223000048400011 / 2223520043560014
							Amex - 378282246310005
							Diners - 38520000023237 or 30569309025904
							*/

							if(TRUE){//$valid) {
								if(TRUE){ //Process eWay here
									$data = array(
										'company' 		=> $company,
										'contactName' 	=> $contactName,
										'contactNumber' => $contactNumber,
										'address' 		=> $address,
										'address2' 		=> $address2,
										'suburb' 		=> $suburb,
										'postcode' 		=> $postcode,
										'state' 		=> intval($state),
										'emailAddress' 	=> $emailAddress,
										'username' 		=> $username,
										'password' 		=> $password
									);

									$orderId = (isset($_SESSION['onlinetrainingorderId'])) ? $_SESSION['onlinetrainingorderId'] : '';

									if(empty($orderId)) {
										$arr = array(
											'paymentStatus'	=> 0,
											'memberId'		=> (!empty($memberId) && is_numeric($memberId)) ? intval($memberId) : NULL,
											'myDateTime'	=> date('Y-m-d h:i:s', now(PPM_TIMEZONE)),
											'amount'		=> doubleval($amount),
											'paymentType'	=> 1
										);
										
										$orderId = $this->onlinetraining->add_transaction($arr);

										$_SESSION['onlinetrainingorderId'] = $orderId;
									}

									if(!empty($memberId) && $_SESSION['isRenewal']) { //Renewal

										$membershipExpires 		= $_SESSION['ot_membershipExpires'];
										$membershipNextExpiry 	= strtotime('+1 year' , strtotime($membershipExpires));
										$unix_date 				= (is_numeric($membershipNextExpiry)) ? $membershipNextExpiry : $this->ppmsystemlib->get_unix_from_date($membershipNextExpiry);
										$membershipExpires 		= ($membershipNextExpiry < strtotime('+1 year' , now())) ? date('Y-m-d 00:00:00',strtotime('+1 year' , now())) : date('Y-m-d 00:00:00', $unix_date);

										$data['memberShipExpires']	= $membershipExpires;
										$data['lockedByUser'] 		= NULL;
										$data['lockExpires'] 		= NULL;

										$this->add_update($data, $memberId, $memberIdConfirm);
									}
									else { //NEW ENTRY

										$membershipExpires = (now(PPM_TIMEZONE) < intval(strtotime('2007-12-05'))) ? strtotime('+1 year' , strtotime('2007-12-05')) : strtotime('+1 year' , now(PPM_TIMEZONE));

										$data['memberShipExpires'] 			= date('Y-m-d 00:00:00', $membershipExpires);
										$data['memberShipType'] 			= 5;
										$data['membershipCommencementDate'] = date('Y-m-d h:i:s', now(PPM_TIMEZONE));
										$data['memberStatus'] 				= 1;
										$data['billingCycle'] 				= 1;
										$data['membershipPayments'] 		= 8;
										$data['howdidtheyhear'] 			= '';

										if(!empty($linkedMemberId) && is_numeric($linkedMemberId)) {
											$data['linkedmemberid'] 			= intval($linkedMemberId);
											$this->add_update($data, NULL, $memberIdConfirm);
										}
										else {
											$this->add_update($data);
										}
									}
								}
								else {
									$data['error'] = array('msg'=>'The credit card details you entered below are incorrect, or you have insufficient funds.');
								}
							}
						}
						else {
							$step1_done = $this->session->flashdata('step1_done');
							$form_post 	= $this->session->flashdata('form_post');
							if($step1_done) $this->session->set_flashdata('step1_done', $step1_done);

							$data['post'] 	= $form_post;
						}

						$data['stage'] 						= 'step2';
						$data['cardTypeArray'] 				= array(''=>'[ Select ]', 1=>"Visa", 2=>"Mastercard");

					}
					else {
						redirect(base_url() . 'ppmsystem/ppm/onlinetraining/step1');
					}

					break;

				case 'step3':

					if($this->session->flashdata('step2_done')) {
						$this->session->set_flashdata('step2_done', TRUE);
						$contactName = $this->session->flashdata('contactName');
						$this->session->set_flashdata('contactName', $contactName);
						$data['title'] = 'Online Training Subscription Processed';
						$data['contactName'] = $contactName;

						$transaction_error = $this->session->flashdata('transaction_error');
						
						if(!empty($transaction_error)) {
							$data['transaction_error'] = $transaction_error;
						}
						
						$email_error = $this->session->flashdata('email_error');

						if(!empty($email_error)) {
							$arr = $this->session->flashdata('email_error');
							$msg = $this->load->view('frontend/ppmsystem/ppm/email_invoice', $arr, TRUE);

							if($this->ppmsystemlib->send_email($msg, $arr['emailAddress'], 'Tax Invoice: ' . $arr['orderId'])){
								if(!$this->ppmsystemlib->send_email($msg, EMAIL_INFO, 'New Subscriber: ' . $arr['orderId'])){
									$this->session->set_flashdata('email_error', $arr);
								}
							}
							else {
								$this->session->set_flashdata('email_error', $arr);
							}
						}
					}
					else {
						redirect(base_url() . 'ppmsystem/ppm/onlinetraining/step1');
					}

					break;
				
				default:
					redirect(base_url() . 'ppmsystem/ppm/onlinetraining/step1');
					break;
			}
			
			$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.css" rel="stylesheet">';
			$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">';

			$this->load->view('frontend/common/header', $data);

			$this->load->view('frontend/ppmsystem/ppm/onlinetraining', $data);
			
			$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.js"></script>';
			$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.buttons.js"></script>';
			
			$this->load->view('frontend/common/footer', $data);
		}
		else {
			redirect(base_url() . 'onlinetraining/signup');
		}
	}

	private function validate_fields($stage = NULL, $memberId = NULL)
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('company', '"Company "', 'required');
		$this->form_validation->set_rules('contactName', '"Central Contact Name "', 'required');
		$this->form_validation->set_rules('contactNumber', '"Contact Number "', 'required|is_numeric');
		$this->form_validation->set_rules('address', '"Postal Address (Line 1) "', 'required');
		$this->form_validation->set_rules('suburb', '"Suburb "', 'required');
		$this->form_validation->set_rules('postcode', '"Postcode "', 'required');
		$this->form_validation->set_rules('state', '"State/Country "', 'required');
		$this->form_validation->set_rules('emailAddress', '"Email Address "', 'required|valid_email');
		$this->form_validation->set_rules('confirmemail', '"Confirm Email Address "', 'required|valid_email|matches[emailAddress]');
		
		if(!empty($memberId) && is_numeric($memberId)) {
			$this->form_validation->set_rules('username', '"Username "', 'required|alpha_numeric|max_length[50]');
		}
		else {
			$this->form_validation->set_rules('username', '"Username "', 'required|max_length[50]|alpha_numeric|callback_username_check');
			$this->form_validation->set_rules('company', '"Company "', 'required|callback_company_check');
			$this->form_validation->set_rules('emailAddress', '"Email Address "', 'required|valid_email|callback_email_check');
		}
		
		$this->form_validation->set_rules('password', '"Password "', 'required');

		if($stage === 'step2') {
			$this->form_validation->set_rules('cardType', '"Card Type "', 'required|is_numeric');
			$this->form_validation->set_rules('creditcard', '"Credit Card Number "', 'required');
			$this->form_validation->set_rules('nameoncard', '"Name on card "', 'required|max_length[30]');
			$this->form_validation->set_rules('expiry', '"Expiry date "', 'required|max_length[4]|is_numeric');
			$this->form_validation->set_rules('cvv', '"CCV "', 'required');
		}

		if ($this->form_validation->run() == FALSE){
            return array("valid"=>FALSE, "error"=>$this->form_validation->error_array());
        }
        else {
        	return array("valid"=>TRUE);
        }
	}

	public function username_check($username)
	{
		$data = array(
			'username'=>$username
		);

		if(!$this->ppmsystemlib->is_unique('members', $data)) {

			if(empty($this->linkedMemberId)) {
				$this->form_validation->set_message('username_check', 'That username has already been taken');
				return FALSE;
			}
			
		}

		return TRUE;
	}	

	public function company_check($companyName)
	{
		$data = array(
			'company'=>$companyName,
			'membershipType'=>5
		);

		if(!$this->ppmsystemlib->is_unique('members', $data)) {
			$this->form_validation->set_message('company_check', 'The Company Name entered below appears to be attached to an existing Membership. Please login via this account to renew.');
			return FALSE;
		}

		return TRUE;
	}

	public function email_check($emailAddress)
	{
		$data = array(
			'emailAddress'=>$emailAddress,
			'membershipType'=>5
		);

		if(!$this->ppmsystemlib->is_unique('members', $data)) {
			$this->form_validation->set_message('email_check', 'The Email Address entered below appears to be attached to an existing Membership. Please login via this email address to renew');
			return FALSE;
		}

		return TRUE;
	}	

	private function add_update($data = NULL, $memberId = NULL, $memberIdConfirm = NULL) {
		$isRenewal = FALSE;

		if(!empty($memberId) && is_numeric($memberId)) {
			if($this->onlinetraining->is_logged_in()) {
				//Security check - make sure that the user has provided the OLD password before updating the existing information
				if($this->onlinetraining->confirm_member(intval($memberId), $memberIdConfirm)){
					$isRenewal = TRUE;
				}
			}
		}

		$success = FALSE;

		$membershipType = '';

		if($isRenewal) {
			if($this->onlinetraining->add_update($data, intval($memberId)) > 0) {
				$_SESSION['ot_membershipExpires'] 	= $data['memberShipExpires'];
				$membershipType 				= 'Renewal';
				$success 						= TRUE;
			}
		}
		else {

			if(!empty($data['linkedmemberid'])) {
				if($this->onlinetraining->confirm_member($data['linkedmemberid'], $memberIdConfirm)){
					$memberId = $this->onlinetraining->add_update($data);
				}
			}
			else {
				$memberId = $this->onlinetraining->add_update($data);
			}

			if($memberId > 0) {
				$fullName 		= explode(' ', $data['contactName']);
				$firstName 		= '';
				$lastName 		= '';
				$membershipType = 'New';

				if(count($fullName) > 1) {
					$firstName 	= $fullName[0];
					$lastName 	= $fullName[1];
				}
				else {
					$firstName 	= $data['contactName'];
				}

				$arr = array(
					'memberId'	=> $memberId,
					'name'		=> $firstName,
					'lastName'	=> $lastName,
					'email'		=> $data['emailAddress']
				);

				$this->onlinetraining->add_member_staff($arr);

				$success = TRUE;
			}
		}

		if($success) {

			$arr = array(
				'paymentStatus' => 1,
				'trxnReference' => 'test',
				'authCode' 		=> 'test',
				'memberId' 		=> intval($memberId),
				'paymentMethod' => 'creditcard-eway'
			);

			if(!$this->onlinetraining->update_transaction(intval($_SESSION['onlinetrainingorderId']), $arr)) {
				$this->session->set_flashdata('transaction_error', 'Cannot find order - please contact this store quoting orderId: ' . $_SESSION['onlinetrainingorderId']);
			}

			$orderId = $_SESSION['onlinetrainingorderId'];
			
			unset($_SESSION['onlinetrainingorderId']);

			$amount = $this->ppmsystemlib->money_format("en_AU.UTF-8", NEWSLETTER_AMOUNT);

			$arr = array(
				'amount'			=> $amount,
				'orderId'			=> $orderId,
				'membershipType'	=> $membershipType,
				'company'			=> $data['company'],
				'contactName'		=> $data['contactName'],
				'contactNumber'		=> $data['contactNumber'],
				'address'			=> $data['address'],
				'emailAddress'		=> $data['emailAddress'],
				'memberShipExpires' => $data['memberShipExpires']
			);

			$msg = $this->load->view('frontend/ppmsystem/ppm/email_invoice', $arr, TRUE);

			if($this->ppmsystemlib->send_email($msg, $arr['emailAddress'], 'Tax Invoice: ' . $orderId)){
				if(!$this->ppmsystemlib->send_email($msg, EMAIL_INFO, 'Online Training Subscriber: ' . $orderId)){
					$this->session->set_flashdata('email_error', $arr);
				}
			}
			else {
				$this->session->set_flashdata('email_error', $arr);
			}

			$this->session->set_flashdata('step2_done', TRUE);
			$this->session->set_flashdata('contactName', $data['contactName']);
			redirect(base_url() . 'ppmsystem/ppm/onlinetraining/step3');
		}
	}

	private function get_amount($id = NULL) 
	{

		$membershipType = $this->onlinetraining->get_membershipType(intval($id));

		switch ($membershipType) {
			case 0:
				$amount = ONLINETRAINING_PLATINUM_AMOUNT;
				break;
			case 1:
				$amount = ONLINETRAINING_GOLD_AMOUNT;
				break;
			case 2:
				$amount = ONLINETRAINING_SILVER_AMOUNT;
				break;																			
			default:
				$amount = ONLINETRAINING_GENERAL_AMOUNT;
				break;
		}

		return $amount;			
	}

	private function get_amount_label() 
	{
		$label = '';

		switch (intval($this->session->membershipType)) {
			case 0:
				$label = ONLINETRAINING_PLATINUM_AMOUNT . ' Platinum Members';
				break;
			case 1:
				$label = ONLINETRAINING_GOLD_AMOUNT . ' Gold Members';
				break;
			case 2:
				$label = ONLINETRAINING_SILVER_AMOUNT . ' Silver Members';
				break;																			
			default:
				$label = ONLINETRAINING_GENERAL_AMOUNT . ' General Public';
				break;
		}

		return $label;		
	}
}
