<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Members extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('ppmsystem_helper');
		$this->load->library('PPMSystemLib');
		$this->load->library('encryption');

		$this->load->model('frontend/Membersmodel', 'members');
		$this->load->model('frontend/OnlineTrainingmodel', 'onlinetraining');

		$this->is_ppmsystem = $this->members->is_logged_in();
		$this->is_newsletter = (isset($_SESSION['newsletter_logged_in']) && $_SESSION['newsletter_logged_in']) ? TRUE : FALSE;
		$this->is_onlinetraining = (isset($_SESSION['onlinetraining_logged_in']) && $_SESSION['onlinetraining_logged_in']) ? TRUE : FALSE;
	
		$this->encryption->initialize(array('driver' => 'mcrypt'));
	}

	public function index()
	{

		if($this->is_ppmsystem) {

			$this->action = $this->uri->segment(2, '');

			$membershipType 	= intval($this->session->membershipType);

			$days_left = intval($this->ppmsystemlib->get_days_left(date('Y-m-d h:i:s', now(PPM_TIMEZONE)), $this->session->membershipExpires));

			if($days_left === 0) {
				$this->action = 'expired';
			}

			//echo $days_left;
			//die();

			$data['page'] = $this->action;

			switch ($this->action) {
				case '':
				case 'home':
					$data['title'] 				= 'Members';
					$data['page']				= 'home';

					$top3_news_articles 		= array();
					$top3_news_articles[] 		= array('data'=>$this->members->get_news_articles(3, 77),'number'=>3,'filter'=>77);
					$top3_news_articles[] 		= array('data'=>$this->members->get_news_articles(3, 2),'number'=>3,'filter'=>2);

					$data['galleries']			= $this->members->get_galleries();

					$data['top3_news_articles'] = $top3_news_articles;

					if($this->session->flashdata('error')) {
						$data['error'] = $this->session->flashdata('error');
					}

					break;

				case 'listnews':

					$page 				= $this->uri->segment(3, '');
					$category 			= $this->input->post('category');
					$state 				= $this->input->post('state');

					$page      			= (!empty($page)) ? $page : 0;

					$articles_result 	= (!empty($category) && is_numeric($category)) ? $this->members->get_articles(77, $category, $state, $page) : $this->members->get_articles(77, 0, $state, $page);
					$articles 			= $articles_result['result'];

					$data['title'] 		= 'News';
					$data['news'] 		= $articles;
					$data['state_arr']	= $this->ppmsystemlib->get_data_arr('stateArray');
					$data['category']	= intval($category);
					$data['state']		= intval($state);
					$data['start_row']	= (!empty($page) && is_numeric($page)) ? intval($page)+1:1;
					$data['end_row']	= intval($page)+count($articles);
					$data['total'] 		= intval($articles_result['total']);
			        $data['pagination']	=  $this->ppmsystemlib->create_pagination(base_url() . 'members/listnews/', intval($articles_result['total']), 10, 3, 9);

					break;

				case 'showarticle':
					$id 				= $this->uri->segment(3, '');
					//$categoryid 		= $this->uri->segment(4, '');
					$data['article'] 	= $this->members->get_article($id);
					$data['title'] 		= (!empty($data['article'])) ? $data['article'][0]['name'] : 'Article';
					$related_downloads 	= array();

					if(!empty($data['article'])) {
						$related_downloads = $this->members->get_related_downloads_for_news($data['article'][0]['relatedDownloads']);
					}
					$data['related_downloads'] = $related_downloads;
					break;

				case 'showdownload':
					
					$id 		= $this->uri->segment(3, '');
					$section 	= '';

					if(!empty($id) && is_numeric($id)) {
						$this->members->update_download_log($id);

						switch ($membershipType) {
							case 0:
								$section = "Platnium";
								break;
							case 1:
								$section = "Gold";
								break;
							case 2:
								$section = "Silver";
								break;
							case 3:
								$section = "Bronze";
								break;				
							default:
								redirect(base_url() . 'members');
								break;
						}

						$downloads = $this->members->get_downloads($id, $section);

						if(!empty($downloads)) {
							$data['title'] 		= $downloads[0]['name'];
							$data['downloads'] 	= $downloads;
						}

					}
					else {
						redirect(base_url() . 'members');
					}

					break;

				case 'articles':
					$data['title'] = 'Article Archive';
					$data['categories'] = $this->members->get_categories_by_area(6);
					break;

				case 'listarticles':
					$catid 				= $this->uri->segment(3, '');
					$page 				= $this->uri->segment(4, '');
					$data['title'] 		= 'Article Archive';

					if(is_numeric($catid) && intval($catid) > 0) {
						$articles_result 	= $this->members->get_articles(2, 0, NULL, $page, intval($catid), NULL);
						$articles 			= $articles_result['result'];
						$data['articles'] 	= $articles;
						$data['title'] 		= 'Article Archive: ' . $articles_result['catname'];
						$data['start_row']	= (!empty($page) && is_numeric($page)) ? intval($page)+1:1;
						$data['end_row']	= (!empty($page) && is_numeric($page)) ? intval($page)+count($articles):10;
						$data['total'] 		= intval($articles_result['total']);
						//$data['categoryid'] = $catid;
						$data['pagination']	=  $this->ppmsystemlib->create_pagination(base_url() . 'members/listarticles/'.$catid.'/', $data['total'], 10, 4, 9);
					}
					break;

				case 'searcharticles':
					$page 				= $this->uri->segment(3, '');
					$page      			= (!empty($page)) ? $page : 0;

					$keywords 			= $this->input->get('keywords');
					$categories 		= $this->members->get_categories_id_by_area(8);
					$categoryid_arr 	= array();

					foreach ($categories as $key => $value) {
						$categoryid_arr[] = $value['id'];
					}

					$articles_result 	= $this->members->get_articles(implode(',',$categoryid_arr).',2', 0, NULL, $page, NULL, $keywords);
					$articles 			= $articles_result['result'];
					$data['articles'] 	= $articles;
					$data['title'] 		= 'SEARCH ARTICLE ARCHIVE';
					$data['start_row']	= (!empty($page) && is_numeric($page)) ? intval($page)+1:1;
					$data['end_row']	= intval($page)+count($articles);
					$data['total'] 		= intval($articles_result['total']);
					//$data['categoryid'] = $catid;
					$data['pagination']	=  $this->ppmsystemlib->create_pagination(base_url() . 'members/searcharticles/', $data['total'], 10, 3, 9);
					break;

				case 'discounts':
					$data['title'] 		= 'discounts';
					$data['discounts'] 	= $this->members->get_discounts();
					break;

				case 'systemupgrades':
				case 'listupgrades':
					$state = $this->uri->segment(3, '');

					if(is_numeric($state) && !empty($state) && strlen($state) <= 3 && $membershipType < 2 ) {
						$stateName 			= $this->members->get_category_name($state);
						$data['downloads'] 	= $this->members->get_downloads_by_section(2, $state);

						$data['total']		= (!empty($data['downloads'])) ? count($data['downloads']) : 0;
						$data['title'] 		= 'System Upgrades : ' . $stateName;
					}
					else {
						$data['title'] = 'System Upgrades';
					}
					break;

				case 'monthlystats':
					$data['title'] = 'PPM ACHIEVEMENT AWARDS';
					break;

				case 'newsletters':
					$data['title'] = 'Landlord Newsletter';
					$data['enews'] = $this->members->get_enews();
					$data['total'] = (!empty($data['enews'])) ? count($data['enews']) : 0;

					if($this->session->flashdata('error')) {
						$data['error'] = $this->session->flashdata('error');
					}

					if($this->session->flashdata('file_download')) {
						$data['file_download'] = $this->session->flashdata('file_download');
					}

					break;

				case 'surveyresults':
					$data['title'] = 'Survey Result Archives';
					$data['polls'] = $this->members->get_surveys();
					break;	
				
				case 'ppmtutorials':
					$data['title'] 	= 'PPM Tutorials';
					break;

				case 'gallery':
					$data['is_member']		= TRUE;
					$data['page'] 			= $this->uri->segment(3, 'list');
					$data['title'] 			= 'GALLERY';


			        if($data['page'] === 'album') {
						$id 					= $this->uri->segment(4, 0);

				    	$data['album']			= $this->members->get_album($id);
				    	$data['body']			= (!empty($data['album'])) ? $data['album'][0]['body'] : '';
		    			$data['myDate']			= (!empty($data['album'])) ? $this->ppmsystemlib->check_date_time($data['album'][0]['mydate']) : '';
				    	$data['title'] 			= (!empty($data['album'])) ? $data['album'][0]['name'] : 'GALLERY';
				    	$data['pics']			= $this->members->get_album($id, TRUE);
				    	$data['other_styles'][] = '<link href="'. base_url() . 'assets/photoswipe/dist/photoswipe.css"; rel="stylesheet">';
				    	$data['other_styles'][] = '<link href="'. base_url() . 'assets/photoswipe/dist/default-skin/default-skin.css"; rel="stylesheet">';
				    	$data['other_scripts'][] = '<script src="'. base_url() . 'assets/photoswipe/dist/photoswipe.min.js"></script>';
				    	$data['other_scripts'][] = '<script src="'. base_url() . 'assets/photoswipe/dist/photoswipe-ui-default.min.js"></script>';			        	
			        }
			        else {
			        	$pagenumber 			= $this->uri->segment(4, 0);
						$data['galleries'] 		= $this->members->get_galleries2($pagenumber);
						$data['start_row']		= (is_numeric($pagenumber)) ? intval($pagenumber)+1:1;
						$data['end_row']		= intval($pagenumber)+count($data['galleries']);
						$data['total'] 			= $this->members->get_galleries2(NULL, TRUE);
				        $data['pagination']		=  $this->ppmsystemlib->create_pagination(base_url() . 'members/gallery/list/', $data['total'], 12, 4, 9);
			        }

					break;

				case 'expired':
					$data['title'] 	= 'Members';
					$data['membershipExpires'] = date('d/m/Y', strtotime($this->session->membershipExpires));
					break;


				case 'editdetails':
					$data['title'] 	= 'Edit your details';
					break;

				case 'autoredirect':

					$get = $this->input->get();

					if(!empty($get)) {
						$this->check_membertype_redirect();
					}

					break;					

				default:
					redirect(base_url() . 'members');
					break;
			}

			$this->load->view('frontend/common/header', $data);

			$menu_data['page'] 				= $data['page'];
			$menu_data['membershipType'] 	= $membershipType;

			$data['sidemenu'] 				= $this->load->view('frontend/common/sidemenu', $menu_data, TRUE);

			$data['slug'] = 'gallery';

			$data['membershipType'] = $membershipType;

			if($this->action === 'gallery') {
				$this->load->view('frontend/gallery', $data);
			}
			else {
				$this->load->view('frontend/members', $data);
			}	
			$this->load->view('frontend/common/footer');
		}
		else {
			$get = $this->input->get();

			if(!empty($get)) {

				$redirect = $this->input->get('redirect');
				$checkMemberType = $this->input->get('checkMemberType');

				$this->session->set_flashdata('redirect', $redirect);
				if(!empty($checkMemberType)) $this->session->set_flashdata('checkMemberType', $checkMemberType);
			}
			
			redirect(base_url() . 'ppmsystem-login');
		}
	}

	private function check_membertype_redirect()
	{
		$checkMemberType = (isset($this->session->checkMemberType)) ? $this->session->checkMemberType : $this->input->get('checkMemberType');

		if(!empty($checkMemberType)) {

			$checkMemberType = $this->encryption->decrypt($checkMemberType);

			if(is_numeric($checkMemberType) && intval($this->session->membershipType) === intval($checkMemberType)) {
				redirect($this->input->get('redirect'));
			}
			else {
                if(isset($this->session->redirect)) {
                	$this->session->set_flashdata('redirect', $this->session->redirect);
                }

                $membershipTypeDictionary = array(0=>'PLATINUM', 1=>'GOLD', 2=>'SILVER');

                $membershipType = intval($this->session->membershipType);  

				$this->session->set_flashdata('error', 'The option you have chosen is specifically for ' . $membershipTypeDictionary[intval($checkMemberType)] . ' members. You are currently a ' . $membershipTypeDictionary[intval($this->session->membershipType)] . ' member.');

				redirect(base_url() . 'members');
			}
		}
		else {
			redirect($this->input->get('redirect'));
		}
	}

	public function login()
	{
		if(!$this->is_ppmsystem) {

			if(isset($this->session->redirect)) {
				$this->session->set_flashdata('redirect', $this->session->redirect);
			}

			if(isset($this->session->checkMemberType)) {
				$this->session->set_flashdata('checkMemberType', $this->session->checkMemberType);
			}
			$data['slug'] 			= 'ppmsystem-member';
			$data['title'] 			= 'Members';
			$data['page'] 			= 'members';
			$data['error'] 			= $this->session->flashdata('error');
			$menu_data['is_member'] = FALSE;
			$data['sidemenu'] 		= $this->load->view('frontend/common/sidemenu', $menu_data, TRUE); 
			$this->load->view('frontend/common/header', $data);
			$this->load->view('frontend/login', $data);

			$this->load->view('frontend/common/footer');
		}
		else {
			redirect(base_url() . 'members');
		}
	}

	public function check()
	{

		$post = $this->input->post();

		if(!empty($post)) {
            $username = $this->input->post('username');
            $password = $this->input->post('password');

            $result = $this->members->login($username, $password);

            if(count($result)>0) {

				$memberStatus 	= intval($result[0]['memberStatus']);

				$statusActive = TRUE;

				if($memberStatus !== 1) {
					$statusActive = FALSE;
					$this->session->set_flashdata('error', 'This is a courtesy message to let you know that your membership has been suspended.<br><br class=""space"">Please email <a href=""mailto:accounts@ppmsystem.com.au?subject=Suspended Account"">accounts@ppmsystem.com.au</a> to enable access to the members area or <br>contact our Head Office on 07 5562 0037<br>');
				}

				if(!$statusActive) {
					redirect(base_url() . 'ppmsystem-login');
				}
				else {          	

	            	$this->onlinetraining->check_training_member(intval($result[0]['id']));

	                $data = array(
	                        'username'        	=> $username,
	                        'id'              	=> $result[0]['id'],
	                        'company'			=> $result[0]['company'],
	                        'membershipType'  	=> $result[0]['membershipType'],
	                        'memberStatus'		=> $result[0]['memberStatus'],
	                        'membershipExpires' => $result[0]['membershipExpires'],
	                        'member_logged_in'	=> TRUE,
	                        'memberIdConfirm'	=> md5($password . SECKEY)
	                );

	                $this->session->set_userdata($data);

	                if(isset($this->session->redirect)) {
	                	$this->check_membertype_redirect();
						//redirect($this->session->redirect);
	                }
	                else {
	                	redirect(base_url() . 'members');
	            	}
            	}
            }
			else {

                if(isset($this->session->redirect)) {
                	$this->session->set_flashdata('redirect', $this->session->redirect);
                }

				if(isset($this->session->checkMemberType)) {
					$this->session->set_flashdata('checkMemberType', $this->session->checkMemberType);
				}                

				$this->session->set_flashdata('error', 'Sorry that username/password combination does not exist. Please try again.');
				redirect(base_url() . 'ppmsystem-login');
			}

		}
		else {
			redirect(base_url() . 'ppmsystem-login');
		}
	}

	public function logout()
    {
        if(isset($_SESSION['member_logged_in']) && $_SESSION['member_logged_in']) {
            $data = array(
                    'username',
                    'id',
                    'company',
                    'membershipType',
                    'memberStatus',
                    'membershipExpires',
                    'member_logged_in',
                    'memberIdConfirm'
            );

		    $this->session->unset_userdata($data);

	        if(isset($_SESSION['onlinetraining_logged_in']) && $_SESSION['onlinetraining_logged_in']) {
	            $data = array(
	                        'ot_id',
	                        'ot_membershipExpires',
	                        'onlinetraining_logged_in',
	                        'ot_memberIdConfirm'
	                    );

	            $this->session->unset_userdata($data);
	        }

	        if(isset($_SESSION['newsletter_logged_in']) && $_SESSION['newsletter_logged_in']) {
	        	$data = array(
	        			'm_nl_id',
						'm_nl_membershipExpires',
						'newsletter_logged_in',
						'm_nl_memberIdConfirm'
						);

	        	$this->session->unset_userdata($data);
	        }
        }

        redirect(base_url() . 'home');
    }

	public function get_enews_downloads()
	{

		if(!$this->is_ppmsystem) {
			if(!$this->is_newsletter){
				redirect(base_url() . 'ppmsystem-login');
			}
		}

		$memberID = 0;

		if($this->is_ppmsystem) {
			$memberID = $_SESSION['id'];
		}
		elseif ($this->is_newsletter) {
			$memberID = $_SESSION['m_nl_id'];
		}

		$src 		= $this->uri->segment(2, '');
		$dataType 	= $this->uri->segment(3, '');
		$id 		= $this->uri->segment(4, '');

		if(!empty($src) && !empty($dataType) && !empty($id) && is_numeric($id)) {
			if(($dataType === 'd1' OR $dataType === 'd2') && ($src === 'm' OR $src === 'nl')) {
				$downloadCount = $this->members->get_enews_download_count($id, $memberID, $dataType);

				$redirect = ($src === 'm') ? 'members/newsletters' : 'newsletter';

				if($downloadCount > 0) {
					$this->session->set_flashdata('error', 'You have already downloaded this newsletter. To download it again please contact PPM.');

					redirect(base_url() . $redirect);

				}
				else {
					$filename = $this->members->get_enews_file($id, $dataType);

					if(!empty($filename)) {
						$data = array(
							'memberId' 	=> $memberID,
							'myDate' 	=> date('Y-m-d h:i:s', now(PPM_TIMEZONE)),
							'eNewsID' 	=> $id,
							'dType' 	=> $dataType);

						$this->members->update_enews_download_log($data);

						$this->session->set_flashdata('file_download', $filename);

						redirect(base_url() . $redirect);
					}
					else{
						$this->session->set_flashdata('error', 'File not found.');

						redirect(base_url() . $redirect);
					}
				}
			}
			else {
				$this->session->set_flashdata('error', 'Invalid parameters');

				redirect(base_url() . $redirect);
			}
		}
		else {
			$this->session->set_flashdata('error', 'Invalid parameters');

			redirect(base_url() . $redirect);
		}		
	}
}
