<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('text');
		$this->load->library('PPMSystemLib');
		$this->load->model('frontend/Sitemodel', 'site');
	}

	public function index()
	{

		$data['title'] 	= 'Home';
		$data['page'] 	= 'home';

		$result = $this->site->get_homepage();

		foreach ($result as $key => $value) {
			$data['id'] = $value['id'];
			$data['banner'] = $value['banner'];
			$data['banner_text'] = $value['banner_text'];
			$data['body'] = $value['body'];
			$data['featured'] = (!empty($value['featured'])) ? $this->is_json($value['featured'], TRUE) : '';
			$data['partners'] = (!empty($value['partners'])) ? $this->is_json($value['partners'], TRUE) : '';
		}

		$data['what_client_say'] 	= $this->site->get_homepage_top4_testimonials();
		$data['news_articles'] 		= $this->site->get_homepage_top2_news_articles();
		$data['links']				= $this->site->get_homepage_top1_links();
		$data['ppmtv']				= $this->site->get_homepage_top1_ppmtv();

		$this->load->view('frontend/common/header', $data);

		$this->load->view('frontend/common/banner', $data);

		$this->load->view('frontend/home', $data);
		$this->load->view('frontend/common/footer');
	}

	private function is_json($string,$return_data = false) {
		$data = json_decode($string);
		return (json_last_error() == JSON_ERROR_NONE) ? ($return_data ? $data : TRUE) : FALSE;
	}
}
