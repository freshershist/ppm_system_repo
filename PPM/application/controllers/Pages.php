<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('ppmsystem');
		$this->load->helper('text');
		$this->load->library('PPMSystemLib');
		$this->load->model('frontend/Membersmodel', 'members');
		$this->load->model('frontend/Sitemodel', 'site');
	}

	public function index()
	{
		redirect('/home');
	}

	public function news_articles() 
	{
		$data['title'] = 'News & Articles';
		$data['slug'] = 'articles'; 
		
		$this->action = $this->uri->segment(2, '');

		$data['page'] = $this->action;

		switch ($this->action) {
			case '':
				$news_page 					= 0;
				$articles_page 				= 0;

				$data['page'] 				= 'home';
				$data['is_pagination']		= FALSE;

				$data['news_total'] 		= $this->site->get_news_articles_by_type(4, TRUE);
				$data['news'] 				= $this->site->get_news_articles_by_type(4, FALSE, $news_page);
				$data['news_start_row'] 	= (is_numeric($news_page)) ? intval($news_page)+1:1;
				$data['news_end_row'] 		= (is_numeric($news_page)) ? intval($news_page)+count($data['news']) : 12;
				$data['news_pagination']	=  $this->ppmsystemlib->create_pagination(base_url() . 'articles/list/n/', $data['news_total'], 12, 4, 9);

				$data['articles_total'] 	= $this->site->get_news_articles_by_type(1, TRUE);
				$data['articles'] 			= $this->site->get_news_articles_by_type(1, FALSE, $articles_page);
				$data['articles_start_row'] = (is_numeric($articles_page)) ? intval($articles_page)+1:1;
				$data['articles_end_row'] 	= (is_numeric($articles_page)) ? intval($articles_page)+count($data['articles']) : 12;
				$data['articles_pagination']=  $this->ppmsystemlib->create_pagination(base_url() . 'articles/list/a/', $data['articles_total'], 12, 4, 9);

			break;

			case 'list':
				if (!$this->input->is_ajax_request()) {
				   redirect(base_url() . 'articles');
				}

				$type 						= $this->uri->segment(3, '');

				$page 						= $this->uri->segment(4, 0);
				$catId 						= ($type === 'n') ? 4 : 1;

				$data['page'] 				= ($type === 'n') ? 'news' : 'articles';
				$data['is_pagination']		= TRUE;

				$data['total'] 				= $this->site->get_news_articles_by_type($catId, TRUE);
				$data['articles'] 			= $this->site->get_news_articles_by_type($catId, FALSE, $page);
				$data['start_row'] 			= (is_numeric($page)) ? intval($page)+1:1;
				$data['end_row'] 			= (is_numeric($page)) ? intval($page)+count($data['articles']) : 12;
				$data['pagination']			=  $this->ppmsystemlib->create_pagination(base_url() . 'articles/list/'.$type.'/', $data['total'], 12, 4, 9);

				$this->ppmsystemlib->compress_output($this->load->view('frontend/articles', $data, TRUE));

				return FALSE;
			break;			

			case 'showarticle':
				$id 				= $this->uri->segment(3, '');
				$data['article'] 	= $this->members->get_article($id);
				$data['page_title'] = (!empty($data['article'])) ? $data['article'][0]['name'] : 'Article';
				$related_downloads 	= array();
				$data['is_pagination']		= FALSE;

				if(!empty($data['article'])) {

					if(isset($_SESSION['membershipType'])){
						$related_downloads = $this->members->get_related_downloads_for_news($data['article'][0]['relatedDownloads']);
					}
					else {
						if(!isset($_SESSION['admin_logged_in']) && !empty($data['article'][0]['relatedDownloads'])) redirect(base_url() . 'ppmsystem-login');
					}

				}

				$data['related_downloads'] = $related_downloads;
			break;

			case 'showdownload':
				if(isset($_SESSION['membershipType'])) {
					$news_id = $this->uri->segment(3, '');

					if(is_numeric($news_id)) {
						redirect(base_url() . 'members/showdownload/' . $news_id);
					}
				}
				else {
					redirect(base_url() . 'ppmsystem-login');
				}
			break;

			default:
				redirect(base_url() . 'articles');
			break;
		}

		$this->load->view('frontend/common/header', $data);

		$this->load->view('frontend/articles', $data);

		$this->load->view('frontend/common/partners', array('slug'=>$data['slug']));

		$this->load->view('frontend/common/footer');		
	}

	public function ppm_tv() 
	{
		$this->load->helper('form');
		
		$data['title'] 	= 'PPM TV';
		$data['slug']	= 'ppm-tv';

		$this->action = $this->uri->segment(2, '');

		$articles_page 				= $this->uri->segment(2, 0);

		$post 						= $this->input->post();
		$data['topic'] 				= '';

		if(!empty($post)) {
			$data['topic'] = $this->input->post('topic');
		}

		$result = $this->site->get_topics(6);
		$data['topics'] = array(''=>'[ Select ]');

		foreach ($result as $key => $value) {
			$data['topics'][$value['id']] = $value['name'];
		}

		$data['articles_total'] 	= $this->site->get_ppmtv_articles($data['topic'], TRUE);
		$data['articles'] 			= $this->site->get_ppmtv_articles($data['topic'], FALSE, $articles_page);
		$data['articles_start_row'] = (is_numeric($articles_page)) ? intval($articles_page)+1:1;
		$data['articles_end_row'] 	= (is_numeric($articles_page)) ? intval($articles_page)+count($data['articles']) : 12;
		$data['articles_pagination']=  $this->ppmsystemlib->create_pagination(base_url() . 'ppm-tv', $data['articles_total'], 12, 2, 9);

		$this->load->view('frontend/common/header', $data);

		$this->load->view('frontend/ppm_tv', $data);
		$this->load->view('frontend/common/partners', array('slug'=>$data['slug']));
		$this->load->view('frontend/common/footer');
	}

	public function industry_partners() 
	{	

		$title 			= 'Industry Partners';
		$action 		= $this->uri->segment(2, '');
		$data['slug'] 	= '';

		if(!empty($action)) {
			$data['page'] = $action;

			switch ($action) {
				case 'listlinks':
					$subCatId 	= $this->uri->segment(3, '');
					$catId 		= $this->uri->segment(4, '');
					$postCode 	= $this->uri->segment(5, '');

					$catId 		= (!is_numeric($catId)) ? 0: intval($catId);

					if(is_numeric($subCatId) && intval($subCatId) > 0) {

						$listlinks 			= $this->members->get_list_links($catId, intval($subCatId));

						$data['links']		= $listlinks;
						$data['title'] 		= ($catId === 145) ? 'Member Offices': $this->members->get_category_name($subCatId);
						$data['categoryid'] = $catId;
						$data['subcatid'] 	= intval($subCatId);
					}
					else {
						redirect(base_url() . 'partners');
					}					

					break;

				case 'show':

					$catId 		= $this->uri->segment(5, '');
					$subCatId 	= $this->uri->segment(4, '');
					$id 		= $this->uri->segment(3, '');
					$discount 	= $this->input->get('discount');

					if(is_numeric($id) && is_numeric($subCatId)) {
						$data['links'] = $this->members->get_links(intval($subCatId), intval($id));
						$data['title'] = (!empty($data['links'])) ? $data['links'][0]['catname']:'';
						$data['isLogin'] = $this->members->is_logged_in();
						$data['discount'] = $discount;
					}
					else {
						redirect(base_url() . 'partners');
					}
					
					break;		
				
				default:
					redirect(base_url() . 'partners');
					break;
			}
		}
		else {
			$data['page'] 		= 'partners';
			$data['title'] 		= 'Industry Partners';

			$data['categories'] = $this->members->get_categories_by_parent(37);
		}

		$this->load->view('frontend/common/header', $data);

		$banner_data['slug'] = 'partners'; 

		$this->load->view('frontend/common/banner', $banner_data);

		$this->load->view('frontend/industry_partners');
		$this->load->view('frontend/common/partners', array('slug'=>$data['slug']));
		$this->load->view('frontend/common/footer');
	}

	public function rent_roll()
	{
		$data['page'] 	= 'rentroll';
		$data['title'] 	= 'Rent Roll Sales';
		$state 			= $this->uri->segment(2, '');
		
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/iCheck/skins/flat/green.css" rel="stylesheet">';

		$this->load->view('frontend/common/header', $data);

		$result 				= $this->site->get_rent_roll($state);
		$rent_roll_arr 			= array();
		$state_sub_region_arr 	= array();

		$data['state']	= (!empty($state) && is_numeric($state)) ? $this->ppmsystemlib->get_data_arr('stateArray')[intval($state) - 1] : '';

		$data['total_rent_roll'] = count($result);

		foreach ($result as $key => $value) {
			if(!empty($value['name'])) {

				if(is_numeric($value['state'])) {
					$state = $this->ppmsystemlib->get_data_arr('stateArray')[intval($value['state']) - 1];
					$name = '';


					if(intval($value['state']) === 1) {
						if(is_numeric($value['stateSubRegionQLD'])) {
							
							$name = $this->ppmsystemlib->get_data_arr('stateSubRegionArray')[intval($value['stateSubRegionQLD']) - 1];

							if(!array_key_exists($name, $state_sub_region_arr)) {
								$state_sub_region_arr[$name] = array('value'=>$value['stateSubRegionQLD'], 'name'=>$name, 'total'=> 1);
								$value['stateSubRegion'] = $value['stateSubRegionQLD'];
							}
							else {
								$state_sub_region_arr[$name]['total'] += 1;
								$value['stateSubRegion'] = $value['stateSubRegionQLD'];
							}

							if(empty($value['code'])) $value['code'] = 'N/A';
						}
					}
					else {
						if(is_numeric($value['stateSubRegionOther'])) {
							$name = $this->ppmsystemlib->get_data_arr('stateSubRegionOtherArray')[intval($value['stateSubRegionOther']) - 1];

							if(!array_key_exists($name, $state_sub_region_arr)) {
								$state_sub_region_arr[$name] = array('value'=>$value['stateSubRegionOther'], 'name'=>$name, 'total'=>1);
								$value['stateSubRegion'] = $value['stateSubRegionOther'];
							}
							else {
								$state_sub_region_arr[$name]['total'] += 1;
								$value['stateSubRegion'] = $value['stateSubRegionOther'];
							}

							if(empty($value['code'])) $value['code'] = 'N/A';						
						}
					}

					$value['location'] = $state . ' - ' . ((!empty($value['location'])) ? $name . ' - ' . $value['location'] : '');

					$rent_roll_arr[$value['name']][] = $value;
				}
			}
		}

		$data['rent_roll'] 		= $rent_roll_arr;
		$data['for_sale_total'] = (isset($rent_roll_arr['FOR SALE'])) ? count($rent_roll_arr['FOR SALE']) : 0;
		$data['wanted_total'] 	= (isset($rent_roll_arr['WANTED'])) ? count($rent_roll_arr['WANTED']) : 0;
		$data['sold_total'] 	= (isset($rent_roll_arr['SOLD'])) ? count($rent_roll_arr['SOLD']) : 0;
		$data['sub_region'] 	= $state_sub_region_arr;

		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/iCheck/icheck.min.js"></script>';

		$data['slug'] 	= 'rent-roll';
		$this->load->view('frontend/rent_roll', $data);
		$this->load->view('frontend/common/partners', array('slug'=>$data['slug']));
		$this->load->view('frontend/common/footer');		
	}

	public function enews() 
	{
		$data['title'] 				= 'eNews';
		$data['page']				= 'home';

		$top3_news_articles 		= array();
		$top3_news_articles[] 		= array('data'=>$this->site->get_news_articles(4),'filter'=>4);
		$top3_news_articles[] 		= array('data'=>$this->site->get_news_articles(1),'filter'=>1);

		$data['top3_events'] 		= $this->site->get_events();
		$data['top3_products'] 		= $this->site->get_products(NULL, TRUE);		

		$data['top3_news_articles'] = $top3_news_articles;

		$this->load->view('frontend/common/header', $data);

		$data['slug'] = 'enews';

		$sub_page_content = render_page_content('enews');

		$data['sub_page_content'] = (strlen($sub_page_content) > 100) ? $sub_page_content : '';	

		$this->load->view('frontend/enews', $data);
		$this->load->view('frontend/common/partners', array('slug'=>$data['slug']));
		$this->load->view('frontend/common/footer');
	}

	public function award_finalists_winners()
	{
		$this->load->helper('form');
		
		$data['title'] 	= 'Award Finalists & Winners';

		$this->action 	= $this->uri->segment(2, '');
		$articles_page 	= $this->uri->segment(2, 0);

		$data['slug'] 	= 'property-management-awards';//$this->uri->segment(1, '');

		$get 			= $this->input->get();
		
		$data['topic'] 	= '';
		$data['mytype'] = '';
		$data['myyear'] = '';

		if(!empty($get)) {
			$data['topic'] 	= $this->input->get('topic');
			$data['mytype'] = $this->input->get('mytype');
			$data['myyear'] = $this->input->get('myyear');
		}

		$result = $this->site->get_topics(26);
		$data['topics'] = array(''=>'[ Select Category ]');

		foreach ($result as $key => $value) {
			$data['topics'][$value['id']] = $value['name'];
		}

		$data['types'] = array(''=>'[ Select Type ]','1'=>'Winner', '2'=>'Finalist');
		$data['years'] = array(''=>'[ Select Year ]');
		for ($i=2014; $i <= 2018 ; $i++) { 
			$data['years'][$i] = $i; 
		}

		$data['articles_total'] 	= $this->site->get_awards($data['topic'],$data['myyear'],$data['mytype'], TRUE);
		$data['articles'] 			= $this->site->get_awards($data['topic'],$data['myyear'],$data['mytype'], FALSE, $articles_page);
		$data['articles_start_row'] = (is_numeric($articles_page)) ? intval($articles_page)+1:1;
		$data['articles_end_row'] 	= (is_numeric($articles_page)) ? intval($articles_page)+count($data['articles']) : 12;
		$data['articles_pagination']=  $this->ppmsystemlib->create_pagination(base_url() . 'property-management-awards-winners', $data['articles_total'], 12, 2, 9);

		$this->load->view('frontend/common/header', $data);

		$this->load->view('frontend/award_finalists_winners');
		$this->load->view('frontend/common/partners', array('slug'=>$data['slug']));
		$this->load->view('frontend/common/footer');		
	}

	public function ppm_conference()
	{
		$this->load->library('encryption');
		$this->encryption->initialize(array('driver' => 'mcrypt'));

		$this->title 	= $this->uri->segment(2, '');

		if(!empty($this->title)) {
			$data['page']	= $this->title;

			$eventId 	= 1229;
			$pageNumber = NULL;

			$arr = $this->site->get_ppm_conference_details($eventId);

			$data['results'] = $arr['results'];

			foreach ($arr['results'] as $key => $value) {
				for ($i=1; $i <= 27; $i++) { 
					$data['files1'][$i] = $value['d' . $i];

					if($i > 3) {
						$data['files2'][$i] = $value['c' . $i];
					}
				}
			}

			$data['arr'] 	= array('location'=>$arr['location'], 'isConference'=>$arr['isConference']);
			$data['slug'] 	= 'ppm-conference';

			switch ($this->title) {
				case 'conference-details':

					$data['location'] = $arr['location'];
					$data['title'] 	= 'PPM CONFERENCE - CONFERENCE DETAILS';
					$data['eventCosts'] = $this->site->get_ppm_conference_event_costs($eventId);

					break;

				case 'speakers-program':
					$data['title'] = 'PPM CONFERENCE - SPEAKERS & PROGRAM';
					$data['results'] = $this->site->get_ppm_conference_speakers($eventId);
					break;

				case 'delegate-testimonials':
					$data['title'] = 'PPM CONFERENCE - DELEGATE TESTIMONIALS';
					$data['results'] = $this->site->get_ppm_conference_testimonials();
					break;

				case 'social-events':
					
					foreach ($arr['results'] as $key => $value) {
						$data['content'] = $value['socialBody'];
					}

					$data['file_arr_index'] = 16;
					$data['title'] 	= 'PPM CONFERENCE - SOCIAL EVENTS';

					break;

				case 'venue-accomodation':
					foreach ($arr['results'] as $key => $value) {
						$data['content'] = $value['venueBody'];
					}

					$data['file_arr_index'] = 4;
					$data['title'] 	= 'PPM CONFERENCE - VENUE & ACCOMMODATION';				
					break;

				case 'travel':
					foreach ($arr['results'] as $key => $value) {
						$data['content'] = $value['travelBody'];
					}

					$data['file_arr_index'] = 10;
					$data['title'] 	= 'PPM CONFERENCE - TRAVEL';				
					break;

				case 'sponsorship-opportunities':		
					foreach ($arr['results'] as $key => $value) {
						$data['content'] = $value['sponsorshipOpBody'];
					}

					$data['file_arr_index'] = 22;
					$data['title'] 	= 'PPM CONFERENCE - SPONSORSHIP OPPORTUNITIES';
					$sub_page_content = render_page_content('ppm-conference-sponsorship-opportunities');

					$data['sub_page_content'] = (strlen($sub_page_content) > 100) ? $sub_page_content : '';		

					break;

				case 'sponsors':
					$data['title'] = 'PPM CONFERENCE - SPONSORS';
					$data['results'] = $this->site->get_ppm_conference_sponsors($eventId);

					$sub_page_content = render_page_content('ppm-conference-sponsor');

					$data['sub_page_content'] = (strlen($sub_page_content) > 100) ? $sub_page_content : '';		
					break;

				case 'sponsor-testimonials':
					$data['results'] = $this->site->get_ppm_conference_sponsors_testimonials();
					$data['title'] 	= 'PPM CONFERENCE - SPONSORS TESTIMONIALS';						
					break;

				case 'register-online':
					$data['results'] = $this->site->get_ppm_conference_register_online($eventId);
					$data['title'] 	= 'PPM CONFERENCE - REGISTER ONLINE';
					break;	

				default:
					redirect(base_url() . 'ppm-conference');
					break;
			}

			$this->load->view('frontend/common/header', $data);

			$this->load->view('frontend/ppm_conference', $data);
			$this->load->view('frontend/common/partners', array('slug'=>$data['slug']));
			$this->load->view('frontend/common/footer');
		}
		else {
			redirect(base_url() . 'ppm-conference');
		}

	}

	public function events()
	{
		$this->load->library('encryption');
		$this->encryption->initialize(array('driver' => 'mcrypt'));

		$this->load->helper('form');

		$this->title 	= $this->uri->segment(2, '');

		if(!empty($this->title)) {
			$data['page']	= $this->title;
			$data['slug'] 	= 'training-events';
			$data['title']	= 'Training & Events';

			switch ($this->title) {
				case 'home':
				case 'list': 

					$post = $this->input->post();
					$get = $this->input->get();

					$category = $this->uri->segment(3, '0');

					$startDate	= '';
					$endDate	= '';

					if(!empty($get)) {
						$startDate = $this->input->get('startDate');
						$endDate = $this->input->get('endDate');
					}
					elseif(is_numeric($category) && !empty($category)) {
						$data['category'] = intval($category);
						$data['title'] .= ' - ' . $this->site->get_name_by_category($category);
					}

					$results = $this->site->get_event_names();

					$list = array(''=>'[ Select ]');

					//print_r($results); die();

					foreach ($results as $key => $value) {
						$list[$value['name']] = $value['name'];
					}

					$data['list'] = $list;
					$data['event_title'] = '';

					if(!empty($post) OR is_numeric($category)) {
						if(!empty($startDate) && !empty($endDate)) {
							$data['title'] 		.= ' - ' . date('F', strtotime($this->ppmsystemlib->set_final_date_time($startDate)));
							$data['category']	= '';
							$results = $this->site->get_training_events(NULL, NULL, $this->ppmsystemlib->set_final_date_time($startDate), $this->ppmsystemlib->set_final_date_time($endDate));
						}
						else {
							$keywords = $this->input->post('keywords');

							$data['event_title'] 	= $keywords;
							$data['category']		= $category;

							$results = $this->site->get_training_events(intval($category), $keywords);
						}

						$data['results'] = $results;
					}

					break;

				case 'show':
					
					/*$request = new eWAY\CreateDirectPaymentRequest();

					var_dump($request);
					die();*/

					/*$plain_text = '99';
					$ciphertext = $this->encryption->encrypt($plain_text);
					
					echo $ciphertext;
					echo $this->encryption->decrypt($ciphertext);
					die();*/

					$eventId = $this->uri->segment(4, '0');

					$arr = $this->site->get_ppm_conference_details(intval($eventId));

					$data['results'] = $arr['results'];

					foreach ($arr['results'] as $key => $value) {
						for ($i=1; $i <= 27; $i++) { 
							$data['files1'][$i] = $value['d' . $i];

							if($i > 3) {
								$data['files2'][$i] = $value['c' . $i];
							}
						}

					}

					$data['arr'] 			= array('location'=>$arr['location'], 'isConference'=>$arr['isConference']);
					$data['title'] 			.= ' - DETAILS';
					$data['eventCosts'] 	= $this->site->get_ppm_conference_event_costs($eventId);

					// echo '<pre>';
					// print_r($data['eventCosts']);
					// die();

					break;

				case 'print':
					$data['print']	= TRUE;
					$data['category'] = 0;
					$data['results'] = $this->site->get_training_events(0, NULL);
					$data['content'] = $this->load->view('frontend/events', $data, TRUE);

					$this->load->view('frontend/common/print', $data);

					return;
					break;	

				default:
					redirect(base_url() . 'events/home');
					break;
			}

			$this->load->view('frontend/common/header', $data);

			$this->load->view('frontend/events', $data);

			$this->load->view('frontend/common/partners', array('slug'=>$data['slug']));

			$this->load->view('frontend/common/footer');
		}
		else {
			redirect(base_url() . 'events/home');
		}

	}

	public function products()
	{
		$this->load->library('encryption');
		$this->encryption->initialize(array('driver' => 'mcrypt'));

		$this->load->helper('form');

		$this->title 	= $this->uri->segment(2, '');

		if(!empty($this->title)) {
			$data['page']	= $this->title;
			$data['slug'] 	= 'products';
			$data['title']	= 'Products';

			switch ($this->title) {
				case 'list':

					$category = $this->uri->segment(3, '0');

					$category = (is_numeric($category)) ? intval($category) : 0;

					$results = $this->site->get_products($category);

					$products_page 					= $this->uri->segment(4, 0);

					$data['title']					= 'Products <span class="grey">' . $this->site->get_product_category($category) . '</span>';
					$data['category'] 				= $category;
					$data['products_total'] 		= $this->site->get_products($category, FALSE, NULL, TRUE);
					$data['products'] 				= $this->site->get_products($category, FALSE, $products_page);
					$data['products_start_row'] 	= (is_numeric($products_page)) ? intval($products_page)+1:1;
					$data['products_end_row'] 		= (is_numeric($products_page)) ? intval($products_page)+count($data['products']) : 12;
					$data['products_pagination']	=  $this->ppmsystemlib->create_pagination(base_url() . 'products/list/'.$category.'/', $data['products_total'], 12, 4, 9);

					break;

				case 'show':
					$category 			= $this->uri->segment(3, '0');
					$prodId 			= $this->uri->segment(4, '0');
					$category 			= (is_numeric($category)) ? intval($category) : 0;
					$prodId 			= (is_numeric($prodId)) ? intval($prodId) : 0;
					$results 			= $this->site->get_product_details($prodId);

					// echo '<pre>';
					// print_r($results);
					// die();

					$data['title']		= (!empty($results)) ? $results[0]['name'] : '';
					$data['products']	= $results;
					$data['prodId']		= $prodId;
					$data['productCosts']= $this->site->get_product_costs($prodId);
					$data['category']	= $category;

					break;	

				default:
					break;	

			}

			$this->load->view('frontend/common/header', $data);

			$this->load->view('frontend/products', $data);

			$this->load->view('frontend/common/partners', array('slug'=>$data['slug']));

			$this->load->view('frontend/common/footer');			
		}
	}

	public function gallery()
	{
		$this->title 	= $this->uri->segment(2, '');
		$this->category = $this->uri->segment(3, 0);

		if(!empty($this->category)) {
			$data['page']		= $this->title;
			$data['results'] 	= $this->site->get_galleries_by_category($this->category);
			$data['title'] 		= 'PPM CONFERENCE - GALLERY';
			$data['slug'] 		= 'ppm-conference';

			$this->load->view('frontend/common/header', $data);

			$this->load->view('frontend/ppm_conference', $data);
			$this->load->view('frontend/common/footer');

		}
	}

	public function random_pages () //Handles admin generated pages (gets page ID by page alias name)
	{
		$page_name = $this->uri->segment(1,'');

		if(!empty($page_name)) {
			$data['page'] = ''; 
			$data['title'] = $page_name;
			$data['slug'] = $page_name;
			$this->load->view('frontend/common/header', $data); 
			$this->load->view('frontend/dyn_pages', $data);
			$this->load->view('frontend/common/partners', array('slug'=>$data['slug']));
			$this->load->view('frontend/common/footer'); 
		}
	}
}
