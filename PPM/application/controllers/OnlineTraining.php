<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class OnlineTraining extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');

		$this->load->library('PPMSystemLib');
		$this->load->library('encryption');

		$this->load->model('frontend/OnlineTrainingmodel', 'onlinetraining');

		$this->is_onlinetraining = $this->onlinetraining->is_logged_in();
		$this->is_newsletter = (isset($_SESSION['newsletter_logged_in']) && $_SESSION['newsletter_logged_in']) ? TRUE : FALSE;
		$this->is_ppmsystem = (isset($_SESSION['member_logged_in']) && $_SESSION['member_logged_in']) ? TRUE : FALSE;
	}

	public function index()
	{
		//training member
		//username: rre2174
		//pass: sys184x

		$this->action = $this->uri->segment(2, '');

		$data['page'] = (!empty($this->action)) ? $this->action : 'home';		

		if($this->is_onlinetraining) {

			switch ($this->action) {
				case '':
					$topic 				= $this->input->post('topic');
					$keywords 			= $this->input->post('keywords');

					$data['title'] 		= 'Online Training';
					$data['topics'] 	= $this->onlinetraining->get_category(12);
					$data['topic']		= (!empty($topic)) ? $topic : '';
					$data['keywords']	= (!empty($keywords)) ? $keywords : '';
					$data['training']	= $this->onlinetraining->get_trainings($topic, $keywords);

					if($_SESSION['ot_id'] === -1) {
						$subscribe = '';
						$days_left = 0;
					}
					else {

						$days_left = intval($this->ppmsystemlib->get_days_left(date('Y-m-d h:i:s', now(PPM_TIMEZONE)), $_SESSION['ot_membershipExpires']));

						if($days_left > 0) {
							$memberIdConfirm = ($this->is_ppmsystem) ? $this->session->memberIdConfirm : $this->session->ot_memberIdConfirm;

							$data['subscribe_link'] = base_url() . 'ppmsystem/ppm/onlinetraining/step2/' . $this->session->ot_id . '/' . $memberIdConfirm;
							$data['days_left'] = $days_left;
						}
						else {
							$data['page'] = 'expired';
							$data['membershipExpires'] = date('d/m/Y', strtotime($_SESSION['ot_membershipExpires']));
						}
					}

					if($this->session->flashdata('error')) {
						$data['error'] = $this->session->flashdata('error');
					}

					break;

				case 'signup':
					$data['title'] = 'Online Training SIGN UP';
					break;

				case 'sessions':
					$data['title']	= 'Online Training Sessions';
					break;

				case 'renew':
					$data['title'] = 'Landlord Newsletter';
					break;

				case 'show':
					$id = $this->uri->segment(3, 0);

					if(!empty($id)) {
						$data['training'] = $this->onlinetraining->get_training(intval($id));

						if(!empty($data['training'])) {
							$this->onlinetraining->update_hits($id, $_SESSION['ot_id']);
						}

						$data['title'] = (!empty($data['training'])) ? $data['training'][0]['topic']: 'Online Training';
						$data['other_styles'][] 	= '<link href="'. base_url() . 'assets/js/videojs/video-js.min.css"; rel="stylesheet">';
						$data['other_scripts'][] 	= '<script src="'. base_url() . 'assets/js/videojs/video.min.js"></script>';
					}
					else {
						redirect(base_url() . 'onlinetraining');
					}
					break;

				case 'autoredirect':

					$get = $this->input->get('redirect');

					if(!empty($get)) {

						$redirect = $this->input->get('redirect');

						if(!empty($redirect)) {
							redirect($redirect);
						}
					}
					break;						
				
				default:
					break;
			}		
		}
		else {
			switch ($this->action) {
				case '':
				case 'signup':
					$data['page'] 	= 'signup';	
					$data['title'] 	= 'Online Training SIGN UP';
					break;

				case 'sessions':
					$data['title']	= 'Online Training Sessions';
					break;

				case 'renew':
					$data['title'] = 'Online Training';
					break;

				case 'autoredirect':

					$get = $this->input->get('redirect');

					if(!empty($get)) {

						$redirect = $this->input->get('redirect');

						$this->session->set_flashdata('redirect', $redirect);

						redirect(base_url() . 'onlinetraining-login');
					}
					break;		
				
				default:
					redirect(base_url() . 'onlinetraining');
					break;
			}			
		}

		$menu_data['is_member']		= FALSE;
		$data['sidemenu'] 			= $this->load->view('frontend/common/sidemenu', $menu_data, TRUE);

		$this->load->view('frontend/common/header', $data); 

		//$banner_data['placement'] = 1; 

		//$this->load->view('frontend/common/banner', $banner_data);

		$this->load->view('frontend/onlinetraining');
		$this->load->view('frontend/common/footer');	

	}

	public function login()
	{
		if(!$this->is_onlinetraining) {
			$data['title'] 	= 'Online Training';
			$data['page'] 	= 'onlinetraining';
			$data['error'] 	= $this->session->flashdata('error');

			if(isset($this->session->redirect)) {
				$this->session->set_flashdata('redirect', $this->session->redirect);
			}

			$this->load->view('frontend/common/header', $data);
			$menu_data['is_member']	= FALSE;
			$data['sidemenu'] 	= $this->load->view('frontend/common/sidemenu', $menu_data, TRUE); 
			$this->load->view('frontend/login', $data);
			$this->load->view('frontend/common/footer');
		}
		else {
			redirect(base_url() . 'onlinetraining');
		}
	}

	public function check()
	{
		$post = $this->input->post();

		if(!empty($post)) {
            $username = $this->input->post('username');
            $password = $this->input->post('password');

            $result = $this->onlinetraining->login($username, $password);

            if(count($result)>0) {
                $data = array(
                        'username'        			=> $username,
                        'ot_id'         			=> intval($result[0]['id']),
                        'ot_membershipExpires'  	=> $result[0]['membershipExpires'],
                        'onlinetraining_logged_in'	=> TRUE,
                        'ot_memberIdConfirm'		=> md5($password . SECKEY)
                );

                $this->session->set_userdata($data);

                if($this->is_newsletter) {
		            $data = array(
		                    'm_nl_id',
		                    'm_nl_membershipExpires',
		                    'newsletter_logged_in',
		                    'm_nl_memberIdConfirm'
		            );

				    $this->session->unset_userdata($data);
                }

                if(isset($this->session->redirect)) {
					redirect($this->session->redirect);
                }
                else {
                	redirect(base_url() . 'onlinetraining');
            	}
            }
			else {

                if(isset($this->session->redirect)) {
                	$this->session->set_flashdata('redirect', $this->session->redirect);
                }				

				$this->session->set_flashdata('error', 'Sorry that username/password combination does not exist. Please try again.');
				redirect(base_url() . 'onlinetraining-login');
			}

		}
		else {
			redirect(base_url() . 'onlinetraining-login');
		}
	}

	public function logout()
    {
        if(isset($_SESSION['onlinetraining_logged_in']) && $_SESSION['onlinetraining_logged_in']) {
            $data = array(
                        'ot_id',
                        'ot_membershipExpires',
                        'onlinetraining_logged_in',
                        'ot_memberIdConfirm'
                    );

            $this->session->unset_userdata($data);
        }

        redirect(base_url() . 'home');
    }
}
