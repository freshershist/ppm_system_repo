<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ContactUs extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('ppmsystem');
		$this->load->helper('form');
		$this->load->library('PPMSystemLib');
		$this->load->model('frontend/Membersmodel', 'members');
		$this->load->model('frontend/Sitemodel', 'site');
	}

	public function index()
	{

		$data['title'] = 'Contact Us';

		$data['enquiry'] = '';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/iCheck/skins/flat/green.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">';

		$this->load->view('frontend/common/header', $data);

		$data['isMain'] = FALSE;

		$this->load->view('frontend/contact_us', $data);

		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/moment/min/moment.min.js"></script>';
		//$data['other_scripts'][] = '<script src="'. base_url() . 'assets/js/moment/moment.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/js/moment/moment.timezone_2012_2022.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.buttons.js"></script>';

		$this->load->view('frontend/common/footer', $data);
	}

	public function enquiry()
	{

		$enquiry_id = $this->uri->segment(2,'');
		$rent_roll_id = $this->uri->segment(3,'');
		$commentsText = '';
		$isForSale = FALSE;

		$commentArr = array(
			'speaker'			=>'Please send me Debbie\'s speaker introduction booklet',
			'healthcheck'		=>'PPM Health Check Request',
			'careers'			=>'I am interested in a career with the PPM Group',
			'freeconsult'		=>'I would like to book a free consultation',
			'consultppm'		=>'I would like to book a free consultation to discuss the PPMsystem',
			'rentrollinfobook'	=>'I would like to request a free copy of your buying and selling rent rolls information booklet',
			'advertiseopp'		=>'I would like to request a copy of your advertising, marketing and sponsorship opportunities form to be emailed direct to me.',
			'memberdiscount'	=>'I would like the PPM Group to negotiate a special member rate with ...',
			'rentrollclass'		=>'I would like to request more information on your rent roll classified service',
			'privateconf'		=>'I would like to request a private and confidential discussion',
			'rentrollbuyandsell'=>'I would like to request more information on what I need to know about buying and selling a rent roll'
		);

		if(!empty($rent_roll_id) && is_numeric($rent_roll_id)) {
			$result = $this->site->get_rent_roll_status($rent_roll_id);

			if (!empty($result)) {
            	$isForSale = ($result['name'] === 'FOR SALE' && !empty($result['email']) && !empty($result['contactName'])) ? TRUE : FALSE;

            	if($isForSale) {
            		$clientEmail = $result['email'];
            		$clientContactName = $result['contactName'];
            	}
        	}
		}
		else {
			if(!empty($rent_roll_id)) {
				if(array_key_exists($rent_roll_id, $commentArr)) {
					$commentsText = $commentArr[$rent_roll_id];
				}
			}
		}

		$data['title'] = 'Contact Us';
		$data['enquiry'] = $enquiry_id;
		$data['rent_roll_id'] = '';

		$data['howdidyouheararray'] = array("Current Member", "ENews", "PPM Group Event", "Franchise Head Office", "Conference Event", "Referral (word of mouth)",  "REI Journal", "Breakfast Club", "Client Service Call", "Other");
		$data['titleArray'] = array("Dr","Mr","Mrs","Ms","Miss");
		$data['stateArray'] = array(""=>"[Select]","1"=>"ACT","2"=>"NSW","3"=>"NT","4"=>"QLD","5"=>"SA","6"=>"TAS","7"=>"WA","8"=>"VIC","9"=>"NZ","10"=>"USA","11"=>"JAPAN","12"=>"OTHER");
		$data['interestedInArray'] = array("Marketing concepts, strategies & ideas", "strategies and ideas", "How to work smarter not harder", "Outlook ");

		$post = $this->input->post();

		$companyName 		= $this->input->post('companyName');
		$firstname 			= $this->input->post('firstname');
		$surname 			= $this->input->post('surname');
		$email 				= $this->input->post('email');
		$phone 				= $this->input->post('phone');
		$street 			= $this->input->post('street');
		$suburb 			= $this->input->post('suburb');
		$state 				= $this->input->post('state');
		$postcode 			= $this->input->post('postcode');
		$other 				= $this->input->post('other');
		$mobile 			= $this->input->post('mobile');
		$comments 			= (!empty($commentsText)) ? $commentsText : $this->input->post('comments');
		$unsubscribeReason 	= $this->input->post('unsubscribeReason');
		$oldcompanyName 	= $this->input->post('oldcompanyName');
		$oldEmail 			= $this->input->post('oldEmail');
		$eventName 			= $this->input->post('eventName');
		$othertrain 		= $this->input->post('othertrain');
		$prefDate 			= $this->input->post('prefDate');
		$myTime	 			= $this->input->post('myTime');

		$subject = '';
		$response = '';
		$sendemail = EMAIL_INFO;//'jubikbok10@gmail.com';//'info@ppmsystem.com';

		switch (intval($enquiry_id)) {
			case 1:
				$data['title'] = 'Unsubscribe from database';
				$subject = 'Request to unsubscribe from database';
				$response = 'Thank you, your details have been successfully sent to the PPM Group. Your contact details will be removed from our website within 2 working days.';

				$arr = $this->ppmsystemlib->get_data_arr('unsubscribeReasonArray');
				$data['unsubscribeReasonArray'] = array(""=>"[Select]");
				$ctr = 1;
				foreach ($arr as $key => $value) {
					$data['unsubscribeReasonArray'][$ctr] = $value;
					$ctr++;
				}

				break;

			case 2:
				$data['title'] = 'Update your contact details';
				$subject = 'Request to update contact details';
				$response = 'Thank you, your details have been successfully sent to the PPM Group. Your contact details will be updated within two working days.';
				break;

			case 3:
				$data['title'] = 'System Presentation Request';
				$subject = 'Request for system presentation';
				$response = 'Thank you, your details have been successfully sent to the PPM Group. A member of our team will contact you shortly (within two working days) to discuss the PPMsystem and how it can benefit your organisation.';
				$sendemail = 'sales@ppmsystem.com';
				break;					

			case 4:
				$data['title'] = 'Request to advertise in the Rent Roll Classifieds';
				$subject = 'Request to advertise in the Rent Roll Classifieds';
				$response = 'Thank you, your details have been successfully sent to the PPM Group. A member of our team will contact you shortly (within two working days) to discuss advertising in the Rent Roll Classifieds.';
				break;

			case 5:
				$data['title'] = 'Enquiry into a Rent Roll Classified';
				$subject = 'Enquiry into a listed rent roll classified';
				$response = 'Thank you, your details have been successfully sent to the PPM Group.

				Special Note:

				All enquiry contact details on a "Rent Roll for SALE" are forwarded directly to the vendor/client. 

				It is at the discretion of the vendor to contact potential purchasers.

				The PPM Group advertise Rent Roll\'s for Sale & Wanted, they do not take part in the negotiation or sale process.';

				if(!empty($rent_roll_id) && is_numeric($rent_roll_id)) {
					$result = $this->site->get_rent_roll_details($rent_roll_id);

					$data['rent_roll'] = (!empty($result)) ? $result[0] : array();
					$data['rent_roll_id'] = '/' . $rent_roll_id;
				}

				break;

			case 6:
				$data['title'] = 'Enquiry into a career with a PPMsystem office';
				$subject = 'Enquiry into job advertised';
				$response = 'Thank you, your details have been successfully sent to the PPM Group.  Within two business days you will receive an email from the PPM Group, outlining our recruitment requirements to be eligible for consideration.';
				break;

			case 7:
				$data['title'] = 'Enquiry into careers at the PPM Group';
				$subject = 'Enquiry into careers at the PPM Group';
				$response = 'Thank you, your details have been successfully sent to the PPM Group.  Within two business days you will receive an email from the PPM Group, outlining our internal recruitment requirements to be eligible for consideration.';
				break;

			case 8:
				$data['title'] = 'General Feedback';
				$subject = 'General Feedback';
				$response = 'Thank you, your details have been successfully sent to the PPM Group.';
				break;

			case 9:
				$data['title'] = 'Contact Us';
				$subject = 'Contact Us';
				$response = 'Thank you, your details have been successfully sent to the PPM Group.';
				break;

			case 10:
				$data['title'] = 'Free Newsletter';
				$subject = 'Free Newsletter subscriber';
				$response = 'Your User ID is: ' . $surname . '
				Your Password is: m12nq8';
				break;

			case 11:
				$data['title'] = 'Training Interest';
				$subject = 'Training Interest';
				$response = 'Thank you, your details have been successfully sent to the PPM Group.  A member of our team will contact you shortly (within two working days) to discuss your training interest.';
				$arr = $this->site->get_events_name();
				$eventNameArr = array(""=>"[Select]");

				foreach ($arr as $key => $value) {
					$eventNameArr[$value['name']] = $value['name'];
				}

				$data['eventNameArr'] = $eventNameArr;

				break;

			case 12:
				$data['title'] = 'Consultancy Request';
				$subject = 'Consultancy request';
				$response = 'Thank you, your details have been successfully sent to the PPM Group.  A member of our team will contact you shortly (within two working days) to discuss your consultancy needs.';
				break;

			case 13:
				$data['title'] = 'Forgot your password';
				$subject = 'Free Newsletter subscriber - forgot password';
				$response = 'Your User ID is: ' . $surname . '
				Your Password is: m12nq8';
				break;	

			case 14:
				$data['title'] = 'PPM ENews Sign Up';
				$subject = 'eNews Subscription';
				$response = 'Thank you for becoming an eNews subscriber.  Your details have been successfully sent to the PPM Group. You will now receive a copy of the eNews on or around the 25th of each month.';
				break;

			case 15:
				$data['title'] = 'Book an Audit Online';
				$subject = 'Book an Audit Online';
				$response = 'Thank you' . $firstname . ', your details have been successfully sent to the PPM Group.  A member of our team will contact you shortly (within two working days).';
				$sendemail = 'support@ppmsystem.com';
				break;

			case 16:
				$data['title'] = 'System Improvement Request';
				$subject = 'System Improvement Request';
				$response = 'Thank you' . $firstname . ', your details have been successfully sent to the PPM Group.  A member of our team will contact you shortly (within two working days).';
				$sendemail = 'support@ppmsystem.com';
				break;

			case 17:
				$data['title'] = 'Recruitment Enquiry';
				$subject = 'Recruitment Enquiry';
				$response = 'Thank you' . $firstname . ', your details have been successfully sent to the PPM Group.';
				$sendemail = 'recruitment@ppmsystem.com';
				break;
			
			default:
				redirect(base_url() . 'contact-us');
				break;
		}

		$isMain = $this->input->post('is_main');

		$data['companyName'] 		= $companyName;
		$data['firstname'] 			= $firstname;
		$data['surname'] 			= $surname;
		$data['email'] 				= $email;
		$data['phone'] 				= $phone;
		$data['street'] 			= $street;
		$data['suburb'] 			= $suburb;
		$data['state'] 				= $state;
		$data['postcode'] 			= $postcode;
		$data['other'] 				= $other;
		$data['mobile'] 			= $mobile;
		$data['comments'] 			= $comments;
		$data['unsubscribeReason'] 	= $unsubscribeReason;
		$data['oldcompanyName'] 	= $oldcompanyName;
		$data['oldEmail'] 			= $oldEmail;
		$data['eventName'] 			= $eventName;
		$data['othertrain'] 		= $othertrain;
		$data['prefDate'] 			= $prefDate;
		$data['myTime'] 			= $myTime;	

		$data['isMain'] = FALSE;

		if(!empty($post)) {

			$results = $this->validate_fields(intval($enquiry_id));

			$valid = FALSE;
			$error = array();

			if(empty($isMain)) {

				if($results['valid']) {
					$valid = TRUE;
				}
				else {
					$data['error'] = json_encode(array('error'=>$results['error']));
				}
			}
			else {

				if($results['valid']) {
					$valid = TRUE;
				}
				else {
					echo json_encode(array('error'=>$results['error']));
					return;
				}
			}

			if($valid) {
				$msg = '';

				$msg .= (!empty($oldEmail)) ? 'Old Email: ' . $oldEmail . '<br>' : '';
				$msg .= (!empty($oldCompanyName)) ? 'Old Company Name: ' . $oldCompanyName . '<br>' : '';
				$msg .= (!empty($eventName)) ? 'Event Name: ' . $eventName . '<br>' : '';
				$msg .= (!empty($otherTrain)) ? 'Other Requested Training: ' . $otherTrain . '<br>' : '';
				$msg .= (!empty($unsubscribeReason)) ? 'Unsubscribe Reason: ' . $data['unsubscribeReasonArray'][intval($unsubscribeReason)] . '<br>' : '';
				$msg .= (!empty($companyname)) ? 'Company Name: ' . $companyname . '<br>' : '';
				$msg .= (!empty($email)) ? 'Email: ' . $email . '<br>' : '';
				$msg .= (!empty($firstname)) ? 'First Name: ' . $firstname . '<br>' : '';
				$msg .= (!empty($surname)) ? 'Surname: ' . $surname . '<br>' : '';
				$msg .= (!empty($phone)) ? 'Phone: ' . $phone . '<br>' : '';
				$msg .= (!empty($prefDate)) ? 'Preferred Date: ' . $prefDate . '<br>' : '';
				$msg .= (!empty($myTime)) ? 'Time: ' . $myTime . '<br>' : '';
				$msg .= (!empty($mobile)) ? 'Mobile: ' . $mobile . '<br>' : '';
				$msg .= (!empty($street)) ? 'Street: ' . $street . '<br>' : '';
				$msg .= (!empty($suburb)) ? 'Suburb: ' . $suburb . '<br>' : '';
				$msg .= (!empty($state)) ? 'State/Country: ' . $data['stateArray'][$state] . '<br>' : '';
				$msg .= (!empty($other)) ? 'Other: ' . $other . '<br>' : '';
				$msg .= (!empty($postcode)) ? 'Postcode: ' . $postcode . '<br>' : '';
				$msg .= (!empty($comments)) ? 'Comments: ' . $comments . '<br>' : '';

				if($isForSale) {

					$customerEmailBody = '';

					$customerEmailBody .= 'Dear ' . $clientContactName . ',<br/><br/>';
					$customerEmailBody .= 'Please find following the contact details of an interested purchaser for your rent roll/business.  It is at your discretion if you choose to make contact with the purchaser. We wish you good luck and prosperity with the sale process.<br/><br/>';
					$customerEmailBody .= $msg;
					$customerEmailBody .= '<br/><br/>If you have any further questions, please feel welcome to contact our office.<br/><br/><br/>';
					$customerEmailBody .= '<p>PPM Group<br/>PO Box 5019 Robina Town Centre QLD 4230<br/>';
					$customerEmailBody .= 'T: 07-5562 0037<br/>F: 07-5580 9844<br/>E: <a href="mailto:info@ppmsystem.com">info@ppmsystem.com</a><br/>W: <a href="' . base_url() . '">' . base_url() . '</a><br/></p>';

					if(!$this->ppmsystemlib->send_email($customerEmailBody, $clientEmail, $subject)) {
						if(empty($isMain)) {
							$data['failed'] = json_encode(array('response'=>'Unable to send message. Please try again later.','msg'=>$customerEmailBody));
						}
						else {
							echo json_encode(array('failed'=>array('response'=>'Unable to send message. Please try again later.')));
							return;
						}
					}

					if(!array_key_exists('failed', $data)) {
						$arr = array();

						$arr['newsId'] = $rent_roll_id;
						$arr['CompanyName'] = $companyName;
						$arr['Title'] = ' ';
						$arr['FirstName'] = $firstname;
						$arr['Surname'] = $surname;
						$arr['Email'] = $email;
						$arr['Phone'] = $phone;
						$arr['Mobile'] = $mobile;
						$arr['Street'] = $street;
						$arr['Suburb'] = $suburb;
						$arr['Location'] = $data['stateArray'][$state];
						$arr['Other'] = $other;
						$arr['PostCode'] = $postcode;

						$this->site->add_rent_roll_enquiry($arr);
					}					
				}

				if(!array_key_exists('failed', $data)) {

					if($this->ppmsystemlib->send_email($msg, $sendemail, $subject)) {
						if(empty($isMain)) {

							if($results['valid']) {
								$arr = array('companyName','firstname','surname','email','phone','street','suburb','state','postcode','other','mobile','comments','unsubscribeReason','oldcompanyName','oldEmail','eventName','othertrain','prefDate','myTime');
								foreach ($arr as $value) {
									if(array_key_exists($value, $data)) {
										$data[$value] = '';
									}
								}

								$data['success'] = json_encode(array('response'=>$response));
							}
							else {
								$data['error'] = json_encode(array('error'=>$results['error']));
							}
						}
						else {

							if($results['valid']) {
								echo json_encode(array('msg'=>$msg,'subject'=>$subject,'sendemail'=>$sendemail,'response'=>$response));
							}
							else {
								echo json_encode(array('error'=>$results['error']));
							}

							return;
						}
					}
					else {
						if(empty($isMain)) {
							$data['failed'] = json_encode(array('response'=>'Unable to send message. Please try again later.'));
						}
						else {
							echo json_encode(array('failed'=>array('response'=>'Unable to send message. Please try again later.')));
							return;
						}
					}
				}
			}
		}

		$get = ($data['isMain']) ? 1 : $this->input->get();

		if(!empty($get)) {
			$data['isMain'] = TRUE;
		}

		if(!$data['isMain']) {
			$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">';
			$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/iCheck/skins/flat/green.css" rel="stylesheet">';
			$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.css" rel="stylesheet">';
			$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">';

			$this->load->view('frontend/common/header', $data);
		}

		$this->load->view('frontend/contact_us', $data);

		if(!$data['isMain']) {
			$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/moment/min/moment.min.js"></script>';
			//$data['other_scripts'][] = '<script src="'. base_url() . 'assets/js/moment/moment.min.js"></script>';
			$data['other_scripts'][] = '<script src="'. base_url() . 'assets/js/moment/moment.timezone_2012_2022.min.js"></script>';
			$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>';
			$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.js"></script>';
			$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.buttons.js"></script>';

			$this->load->view('frontend/common/footer', $data);
		}
	}

	function validate_fields($enquiry_id = NULL)
	{
		$this->load->library('form_validation');

		if(!empty($enquiry_id)) {
			switch ($enquiry_id) {
				case 1:
					$this->form_validation->set_rules('state', '"State/Country"', 'required');
					$this->form_validation->set_rules('unsubscribeReason', '"Reason"', 'required');
					break;

				case 2:
					$this->form_validation->set_rules('oldcompanyName', '"Old Company Name"', 'required');
					$this->form_validation->set_rules('oldEmail', '"Old Email"', 'required|valid_email');
					break;
				
				case 6:
				case 7:
				case 10:
				case 14:
					$this->form_validation->set_rules('street', '"Street"', 'required');
					$this->form_validation->set_rules('suburb', '"Suburb"', 'required');
					$this->form_validation->set_rules('state', '"State"', 'required');
					$this->form_validation->set_rules('postcode', '"Postcode"', 'required');
					$this->form_validation->set_rules('mobile', '"Mobile"', 'required');
					break;

				case 11:
					$this->form_validation->set_rules('state', '"State"', 'required');
					break;	

				default:
					break;
			}

			$arr = array(6,7,8,9);

			if(!in_array($enquiry_id, $arr)) {
				$this->form_validation->set_rules('companyName', '"Company Name"', 'required');
			}

			$this->form_validation->set_rules('firstname', '"First Name"', 'required');
			$this->form_validation->set_rules('surname', '"Surname"', 'required');
			$this->form_validation->set_rules('email', '"Email"', 'required|valid_email');
			
			if($enquiry_id != 13) $this->form_validation->set_rules('phone', '"Phone"', 'required');			

			if ($this->form_validation->run() == FALSE){
	            return array("valid"=>FALSE, "error"=>$this->form_validation->error_array());
	        }
	        else {
	        	return array("valid"=>TRUE);
	        }			
		}
	}
}
