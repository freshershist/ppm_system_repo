<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contents extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('file');
		$this->load->helper('directory');
		$this->load->helper('form');
		$this->load->helper('date');
		$this->load->helper('email');
		$this->load->helper('text');

		$this->load->library('form_validation');
		$this->load->library('PPMSystemLib');

		$this->load->model('admin/dashboardmodel', 'dashboard');
		$this->load->model('admin/contentsmodel', 'contents');

		$this->action = '';
		$this->memberId = 0;

		if(!$this->dashboard->is_logged_in()) {
			redirect(base_url() . 'admin/login');
		}
		else {
			if(!$this->dashboard->check_user_access(4)) {
				redirect(base_url() . 'admin/home');
			}
		}
	}

	public function index()
	{

		$this->action = $this->uri->segment(3, 'list');

		$data['active_page'] 	= "Contents";

		$data['action']	= $this->action;

		$filters['categories'] = $this->contents->get_categories();		

		$state_downloads = array();

		foreach ($this->dashboard->get_state(9) as $key => $value) {
			$state_downloads[$value['id']] = $value['name'];
		}

		$categories_arr = array(
			'rent-roll' => array('5'=>'Rent Roll'),
			'ppm-tv' 	=> array('889'=>'PPM TV')
		);

		$news_article_arr 		= array(1,2,4,77);
		$rent_roll_ppmtv_arr 	= array(5,889);

		foreach ($filters['categories'] as $key => $value) {
			
			if(intval($value['area']) === 1) {
				if(!array_key_exists('news-articles', $categories_arr)) {
					$categories_arr['news-articles'] = array();
				}

				if(!array_key_exists('others', $categories_arr)) {
					$categories_arr['others'] = array();
				}						

				if(in_array(intval($value['id']), $news_article_arr)) {
					$categories_arr['news-articles'][$value['id']] = $value['name']; 
				}
				elseif(!in_array(intval($value['id']), $rent_roll_ppmtv_arr)) {
					$categories_arr['others'][$value['id']] = $value['name']; 
				}
			}
			elseif(intval($value['area']) === 6) {
				if(!array_key_exists('topics', $categories_arr)) {
					$categories_arr['topics'] = array();
				}
				
				$categories_arr['topics'][$value['id']] = $value['name'];
			}
			elseif(intval($value['area']) === 23) {
				if(!array_key_exists('promotions', $categories_arr)) {
					$categories_arr['promotions'] = array();
				}
				
				$categories_arr['promotions'][$value['id']] = $value['name'];
			}				
		}

		$filters['categories_arr'] = $categories_arr;

		$gallery_data = $this->ppmsystemlib->get_files();

		$gallery_data['directory'] 	= directory_map(UPLOADFOLDER . '/files');
		$gallery_data['acceptedFiles'] = $this->ppmsystemlib->get_accepted_mimes();

		$data['upload'] = $this->load->view('admin/common/dz_modal', $gallery_data, TRUE);
		$editor['editor_id'] = 'data_body';
		//$editor['text']	= "test";

		$add_edit['editor'] = $this->load->view('admin/common/editor', $editor, TRUE);
		$add_edit['tab']	= $this->action;
		$add_edit['categories_arr'] = $categories_arr;

		$result = $this->contents->get_related_products();
		$related_products = array();

		foreach ($result as $key => $value) {
			$related_products[$value['id']] = $value['name'];
		}

		$add_edit['related_products'] = $related_products;

		$result = $this->contents->get_related_downloads();
		$related_downloads = array();

		foreach ($result as $key => $value) {
			$related_downloads[$value['id']] = $value['name'] . ' [ ' . $state_downloads[$value['stateid']] . ' ]';
		}

		$add_edit['related_downloads'] = $related_downloads;

		$stateArray = $this->ppmsystemlib->get_data_arr('stateArray');

		$stateArr = array();

		foreach ($stateArray as $key => $value) {
			$stateArr[$value] = $value;
		}

		$data['stateFilterArray'] = $stateArr;

		$stateArr = array();

		$stateArr[''] = '[ Select ]'; 

		$ctr = 1;
		foreach ($stateArray as $key => $value) {
			$stateArr[$ctr] = $value;
			$ctr++;
		}

		$add_edit['stateArray'] = $stateArr;

		$stateArray = $this->ppmsystemlib->get_data_arr('stateSubRegionArray');

		$stateArr = array();

		$stateArr[''] = '[ Select ]'; 

		$ctr = 1;
		foreach ($stateArray as $key => $value) {
			$stateArr[$ctr] = $value;
			$ctr++;
		}

		$add_edit['stateSubRegionArray'] = $stateArr;

		$categoryNewsArray = $this->ppmsystemlib->get_data_arr('categoryArray');

		$newsArr = array();

		$newsArr[''] = '[ Select ]'; 

		$ctr = 1;
		foreach ($categoryNewsArray as $key => $value) {
			$newsArr[$ctr] = $value;
			$ctr++;
		}

		$add_edit['categoryNewsArray'] = $newsArr;

		if($this->action === 'promotions') {//Associated Poll
			$result = $this->contents->get_polls();
			$associatedPoll = array(''=>'[ Select ]');

			foreach ($result as $key => $value) {
				$associatedPoll[$value['id']] = $value['pollname'];
			}

			$add_edit['associatedPoll'] = $associatedPoll;
		}


		$data['add_edit'] = $this->load->view('admin/contents_add_edit', $add_edit, TRUE);

		switch ($this->action) {
			case 'news-articles':
				$data['page'] 		= 'news-articles';
				$data['page_title'] = "News & Articles";
				break;

			case 'rent-roll':
				$data['page'] 		= 'rent-roll';
				$data['page_title'] = "Rent Roll";
				break;
				
			case 'ppm-tv':
				$data['page'] 		= 'ppm-tv';
				$data['page_title'] = "PPM TV";
				break;

			case 'promotions':
				$data['page'] 			= 'promotions';
				$data['page_title'] 	= "Promotions";
				break;
				
			case 'others':
				$data['page'] 		= 'others';
				$data['page_title'] = "Others";
				break;		
			
			default:
				redirect(base_url() . 'admin/contents/news-articles');
				break;
		}

		$filters['page'] = $data['page'];
		$filters['page_title'] = $data['page_title'];

		$data['filters'] 	= $this->load->view('admin/contents_filters', $filters, TRUE);

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/switchery/dist/switchery.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/iCheck/skins/flat/green.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/dropzone/dist/min/dropzone.min.css" rel="stylesheet">';

		$this->load->view('admin/common/header', $data);
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/common/topnav');

		$this->load->helper('form');

		$this->load->view('admin/contents', $data);

		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/switchery/dist/switchery.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/moment/min/moment.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/js/moment/moment.timezone_2012_2022.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net/js/jquery.dataTables.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/iCheck/icheck.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.buttons.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/dropzone/dist/min/dropzone.min.js"></script>';

		$data['show_categories'] = $this->load->view('admin/show_categories', array('section_name'=> array('newsTopic')), TRUE);

		$this->load->view('admin/common/footer', $data);			

	}

	public function get_list() 
	{
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$post = $this->input->post();

		if(count($post)>0){

			$categoryId = $this->input->post('category');
			$result = array();

			if(is_numeric($categoryId)) {
				$result = $this->contents->get_news_by_category($categoryId);
			}
			else {
				$result = $this->contents->get_news_by_category();
			}


			/*
			clickherepromotiontext
			drawdate
			drawperson
			enddate
			pollid

			id
			name
			location
			state
			numberOfEnquiries
			code
			contactName
			companyName
			email
			contactNumber
			shortdesc
			mydate
			comments
			draft

			*/

			$result_arr = array();

			foreach ($result as $key => $value) {
				$arr = array();

				$enquiry = '';

				if(intval($categoryId) === 5) {	//Rent Roll	
					$arr['code'] 				= $value['code'];
					$arr['location'] 			= $value['location'];
					$arr['state'] 				= (is_numeric($value['state'])) ? $this->ppmsystemlib->get_data_arr('stateArray')[intval($value['state']) - 1] : '';
					$arr['contactName'] 		= $value['contactName'];
					$arr['companyName'] 		= (strlen($value['companyName']) <= 30) ? $value['companyName'] : '<span class="text-word-wrap">'.$value['companyName'].'</span>';
					$arr['email'] 				= $value['email'];
					$arr['contactNumber'] 		= $value['contactNumber'];
					$arr['comments'] 			= (strlen($value['comments']) <= 30) ? strip_tags($value['comments']) : '<span class="truncate" title="' . strip_tags($value['comments']) . '">' . ellipsize(strip_tags($value['comments']), 50, 1) . '</span>';
					$arr['numberOfEnquiries'] 	= $this->contents->get_enquiry_details($value['id'], TRUE);//$value['numberOfEnquiries'];
					$arr['name'] 				= $value['name'];
					
					if(is_numeric($arr['numberOfEnquiries']) && intval($arr['numberOfEnquiries']) > 0) {
						$enquiry = '<li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="enquiry">Enquiry Details</a></li>';
					}
				}
				else {
					$arr['name'] 		= (strlen($value['name']) <= 30) ? $value['name'] : '<span class="text-word-wrap">'.$value['name'].'</span>';
				}
				
				$arr['id'] 			= $value['id'];
				
				$arr['shortdesc'] 	= (strlen(strip_tags($value['shortdesc'])) <= 30) ? strip_tags($value['shortdesc']) : '<span class="truncate" title="' . strip_tags($value['shortdesc']) . '">' . ellipsize(strip_tags($value['shortdesc']), 50, 1) . '</span>';
				$arr['mydate'] 		= '<font class="hide">'. $this->ppmsystemlib->get_unix_from_date($value['mydate']) . '</font> ' . $this->ppmsystemlib->check_date_time($value['mydate']);
				$arr['draft'] 		= (intval($value['draft']) === 1) ? '<font class="text-danger">DRAFT</font>' : '<font class="text-success">LIVE</font>' ;
				

				$arr['action']	= '<ul class="nav nav-pills" role="tablist"> <li role="presentation" class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">Action<span class="caret"></span></a><ul class="dropdown-menu animated fadeInDown" role="menu" aria-labelledby="drop6" style="left:-108%!important;"><li role="presentation"><a tabindex="-1" href="javascript:;">ID: <label class="action-id"></label></a></li><li role="presentation" class="divider"></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="edit">Edit</a></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="copy">Copy</a></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="delete">Delete</a></li>' . $enquiry . '</ul></li></ul>';

				if(intval($categoryId) === 4) {//News General
					$state = (is_numeric($value['state'])) ? $this->ppmsystemlib->get_data_arr('stateArray')[intval($value['state']) - 1] : '';
					if(!empty($state)) {
						$stateStr = '<font class="hide">' . $state . '</font>' . $arr['action'];
						$arr['action'] = $stateStr;
					}
				}

				$result_arr[] = $arr;
			}		

			echo json_encode($result_arr);
		}
		else {
			echo json_encode(array());
		}
	}

	public function process_action()
	{
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$post = $this->input->post();

		if(!empty($post)) {
			$id = $this->input->post('id');
			$action = $this->input->post('action');

			if($action === 'edit' OR $action === 'copy') {

				if(is_numeric($id)) {
					$result = $this->contents->get_article($id);
					$categoryId = $this->contents->get_news_category($id);

					$cat_arr = array();

					foreach ($categoryId as $key => $value) {
						$cat_arr[] = intval($value['categoryId']);
					}				

					$data = array();

					$data['categories'] = $cat_arr;

					//$data['result'] = $result;

					foreach ($result as $key => $value) {
						$data['id']					= intval($value['id']);
						$data['draft'] 				= intval($value['draft']);
						$data['name'] 				= $value['name'];
						$data['mydate'] 			= $this->ppmsystemlib->check_date_time($value['mydate']);
						$data['source'] 			= $value['source'];
						$data['shortdesc'] 			= $value['shortdesc'];
						$data['body'] 				= $value['body'];
						$data['d1'] 				= $value['d1'];
						$data['d2'] 				= $value['d2'];
						$data['d3'] 				= $value['d3'];
						$data['d4'] 				= $value['d4'];
						$data['d5'] 				= $value['d5'];
						$data['c1'] 				= $value['c1'];
						$data['c2'] 				= $value['c2'];
						$data['c3'] 				= $value['c3'];
						$data['c4'] 				= $value['c4'];
						$data['c5'] 				= $value['c5'];
						$data['w1'] 				= $value['w1'];
						$data['w2'] 				= $value['w2'];
						$data['w3'] 				= $value['w3'];
						$data['w4'] 				= $value['w4'];
						$data['w5'] 				= $value['w5'];						
						$data['relatedDownloads'] 	= explode(',',str_replace(' ','',$value['relatedDownloads']));
						$data['relatedproducts'] 	= explode(',',str_replace(' ','',$value['relatedproducts']));
						$data['location'] 			= $value['location'];
						$data['state'] 				= $value['state'];
						$data['stateSubRegionQLD'] 	= $value['stateSubRegionQLD'];
						$data['category']			= $value['category'];
						$data['youtubelink']		= $value['youtubelink'];

						$data['comments']			= $value['comments'];
						$data['email']				= $value['email'];
						$data['contactName']		= $value['contactName'];
						$data['companyName']		= $value['companyName'];
						$data['contactNumber']		= $value['contactNumber'];

						$data['enddate'] 			= $this->ppmsystemlib->check_date_time($value['enddate']);
						$data['associatedPoll'] 	= (is_numeric($value['pollid'])) ? intval($value['pollid']) : '';
						$data['drawdate'] 			= $this->ppmsystemlib->check_date_time($value['drawdate']);
						$data['drawperson'] 		= $value['drawperson'];
						$data['donotdisplaywinnerdrawn'] = intval($value['donotdisplaywinnerdrawn']);
						$data['clickherepromotiontext'] = $value['clickherepromotiontext'];

						//$data['name'] = $value['name'];

					}

					echo json_encode($data);
				}
			}
			elseif($action === 'enquiry'){
				/*
	            [enquiryId] => 280
	            [newsId] => 1742
	            [CompanyName] => Chapple holdings pty ltd
	            [Title] => Ms
	            [FirstName] => Aimee
	            [Surname] => Foley
	            [Email] => aimee@cairnsplatinum.com.au
	            [Phone] => 0740574444
	            [Street] => 5/116 reed road 
	            [Suburb] => Trinity Park
	            [Location] => QLD
	            [PostCode] => 
	            [Other] => 
	            [Mobile] => 0447570510
	            [Fax] => 
	            [CDTS] => 2016-12-07 19:27:23

				*/

            	if(is_numeric($id)) {
					$result = $this->contents->get_enquiry_details(intval($id));

					$details = array();

            		foreach ($result as $key => $value) {
						$details[$key]['id'] 			= $value['enquiryId'];
						$details[$key]['CompanyName'] 	= $value['CompanyName'];
						$details[$key]['Title'] 		= $value['Title'];
						$details[$key]['FirstName'] 	= $value['FirstName'];
						$details[$key]['Surname'] 		= $value['Surname'];
						$details[$key]['Email'] 		= $value['Email'];
						$details[$key]['Phone'] 		= $value['Phone'];
						$details[$key]['Street'] 		= $value['Street'];
						$details[$key]['Suburb'] 		= $value['Suburb'];
						$details[$key]['Location'] 		= $value['Location'];
						$details[$key]['PostCode'] 		= $value['PostCode'];
						$details[$key]['Other'] 		= $value['Other'];
						$details[$key]['Mobile'] 		= $value['Mobile'];
						$details[$key]['Fax'] 			= $value['Fax'];
						$details[$key]['date'] 			= '<font class="hide">'. $this->ppmsystemlib->get_unix_from_date($this->ppmsystemlib->set_final_date_time($this->ppmsystemlib->check_date_time($value['CDTS']))) . '</font> ' . $this->ppmsystemlib->check_date_time($value['CDTS']);
					}

					echo json_encode($details);
            	}
			}
			elseif($action === 'delete') {
				if(is_numeric($id)) {
					$this->contents->delete_news($id);

					echo json_encode(array('success'=>1));
				}
				else {
					echo json_encode(array('error'=>1));
				}
			}
		}
		else {
			exit('No direct script access allowed');
		}
	}	

	public function add_update()
	{
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$post = $this->input->post();

		if(!empty($post)) {

			$id 				= $this->input->post('id'); 
			$categories 		= $this->input->post('category1[]');
			$topics 			= $this->input->post('category2[]');
			$draft 				= $this->input->post('draft');
			$name 				= $this->input->post('title');
			$mydate 			= $this->input->post('mydate');
			$source 			= $this->input->post('source');
			$shortdesc 			= $this->input->post('shortdesc');
			$body 				= $this->input->post('data_body');
			$d1 				= $this->input->post('d1');
			$d2 				= $this->input->post('d2');
			$d3 				= $this->input->post('d3');
			$d4 				= $this->input->post('d4');
			$d5 				= $this->input->post('d5');
			$c1 				= $this->input->post('c1');
			$c2 				= $this->input->post('c2');
			$c3 				= $this->input->post('c3');
			$c4 				= $this->input->post('c4');
			$c5 				= $this->input->post('c5');
			$w1 				= $this->input->post('w1');
			$w2 				= $this->input->post('w2');
			$w3 				= $this->input->post('w3');
			$w4 				= $this->input->post('w4');
			$w5 				= $this->input->post('w5');			
			$relatedDownloads 	= $this->input->post('relatedDownloads[]');
			$relatedproducts 	= $this->input->post('relatedProducts[]');
			$location 			= $this->input->post('location');
			$state 				= $this->input->post('state');
			$stateSubRegionQLD 	= $this->input->post('stateSubRegionQLD');
			$newsCategory 		= $this->input->post('category');
			$youtubelink		= $this->input->post('youtubelink');
			$comments			= $this->input->post('comments');
			$email				= $this->input->post('email');
			$contactName		= $this->input->post('contactName');
			$companyName		= $this->input->post('companyName');
			$contactNumber		= $this->input->post('contactNumber');
			$enddate 			= $this->input->post('enddate');
			$associatedPoll 	= $this->input->post('associatedPoll');
			$drawdate 			= $this->input->post('drawdate');
			$drawperson 		= $this->input->post('drawperson');

			$donotdisplaywinnerdrawn 	= $this->input->post('donotdisplaywinnerdrawn');
			$clickherepromotiontext 	= $this->input->post('clickherepromotiontext');					

			$valid = TRUE;
			$error = NULL;

			if(empty($categories)) {
				echo json_encode(array("error"=>array('category'=>'Web Location is required.')));

				return FALSE;
			}

			foreach ($categories as $key => $value) {
				$result = $this->validate_fields(intval($value));
				if(!$result['valid']) {
					$valid = $result['valid'];
					$error = $result['error'];
					break;
				}
			}

			if($valid) {

				$data = array();

				$data['draft'] 				= ($draft === 'on') ? 1 : 0;
				$data['name'] 				= $name;
				$data['mydate'] 			= $this->ppmsystemlib->set_final_date_time($mydate);
				$data['source'] 			= $source;
				$data['shortdesc'] 			= $shortdesc;
				$data['body'] 				= $body;
				$data['d1'] 				= $d1;
				$data['d2'] 				= $d2;
				$data['d3'] 				= $d3;
				$data['d4'] 				= $d4;
				$data['d5'] 				= $d5;
				$data['c1'] 				= $c1;
				$data['c2'] 				= $c2;
				$data['c3'] 				= $c3;
				$data['c4'] 				= $c4;
				$data['c5'] 				= $c5;
				$data['w1'] 				= $w1;
				$data['w2'] 				= $w2;
				$data['w3'] 				= $w3;
				$data['w4'] 				= $w4;
				$data['w5'] 				= $w5;				
				$data['relatedDownloads'] 	= (is_array($relatedDownloads) && !empty($relatedDownloads)) ? implode(',', $relatedDownloads) : '';
				$data['relatedproducts'] 	= (is_array($relatedproducts) && !empty($relatedproducts)) ? implode(',', $relatedproducts) : '';
				$data['location'] 			= $location;
				$data['state'] 				= (empty($state)) ? NULL : $state;
				$data['stateSubRegionQLD'] 	= $stateSubRegionQLD;
				$data['category']			= $newsCategory;
				$data['youtubelink']		= $youtubelink;

				$data['comments']			= $comments;
				$data['email']				= $email;
				$data['contactName']		= $contactName;
				$data['companyName']		= $companyName;
				$data['contactNumber']		= $contactNumber;

				$data['enddate'] 			= $this->ppmsystemlib->set_final_date_time($enddate);
				$data['pollid'] 			= (is_numeric($associatedPoll)) ? intval($associatedPoll) : '';
				$data['drawdate'] 			= $this->ppmsystemlib->set_final_date_time($drawdate);
				$data['drawperson'] 		= $drawperson;
				$data['donotdisplaywinnerdrawn'] 	= ($donotdisplaywinnerdrawn === 'on') ? 1 : 0;
				$data['clickherepromotiontext'] 	= $clickherepromotiontext;

				$arr = array();

				if(in_array("5", $categories)) {
					$arr = array(
							'name'				=>$name,
							'location'			=>$location,
							'state'				=>(is_numeric($state)) ? $this->ppmsystemlib->get_data_arr('stateArray')[intval($state) - 1] : '',
							'contactName'		=>$contactName,
							'companyName'		=>(strlen($companyName) <= 30) ? $companyName : '<span class="text-word-wrap">'.$companyName.'</span>',
							'email'				=>$email,
							'contactNumber'		=>$contactNumber,
							'comments'			=>(strlen(strip_tags($comments)) <= 30) ? strip_tags($comments) : '<span class="truncate" title="' . strip_tags($comments) . '">' . ellipsize(strip_tags($comments), 50, 1) . '</span>',
							'shortdesc'			=>(strlen(strip_tags($shortdesc)) <= 30) ? strip_tags($shortdesc) : '<span class="truncate" title="' . strip_tags($shortdesc) . '">' . ellipsize(strip_tags($shortdesc), 50, 1) . '</span>',
							'mydate'			=>'<font class="hide">'. $this->ppmsystemlib->get_unix_from_date($this->ppmsystemlib->set_final_date_time($mydate)) . '</font> ' . $mydate,
							'draft'				=>($draft === 'on') ? '<font class="text-danger">DRAFT</font>' : '<font class="text-success">LIVE</font>'
					);
				}
				else {
					$arr = array(
							
							'name'				=>$name,
							'mydate'			=>'<font class="hide">'. $this->ppmsystemlib->get_unix_from_date($this->ppmsystemlib->set_final_date_time($mydate)) . '</font> ' . $mydate,
							'shortdesc'			=>(strlen(strip_tags($shortdesc)) <= 30) ? strip_tags($shortdesc) : '<span class="truncate" title="' . strip_tags($shortdesc) . '">' . ellipsize(strip_tags($shortdesc), 50, 1) . '</span>',
							'draft'				=>($draft === 'on') ? '<font class="text-danger">DRAFT</font>' : '<font class="text-success">LIVE</font>'
					);						
				}				

				if(is_numeric($id)) { //Edit
					$arr['id'] = $id;
					$arr['numberOfEnquiries'] = $this->contents->get_enquiry_details($id, TRUE);

					$this->contents->update_news($id, $data);
					$this->contents->update_news_categories($id, $categories, $topics);
				}
				else { //Add
					$arr['numberOfEnquiries'] = '';
					$arr['action']	= '<ul class="nav nav-pills" role="tablist"> <li role="presentation" class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">Action<span class="caret"></span></a><ul class="dropdown-menu animated fadeInDown" role="menu" aria-labelledby="drop6" style="left:-108%!important;"><li role="presentation"><a tabindex="-1" href="javascript:;">ID: <label class="action-id"></label></a></li><li role="presentation" class="divider"></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="edit">Edit</a></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="copy">Copy</a></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="delete">Delete</a></li></ul></li></ul>';
					$data['code'] = (in_array("5", $categories)) ? $this->contents->get_rent_roll_code() : '';

					$id = $this->contents->add_news($data);

					if(!empty($id)) {
						$arr['id'] = $id;
						$arr['code'] = $data['code'];
						$this->contents->update_news_categories($id, $categories, $topics);
					}
				}

				echo json_encode($arr);
			}
			else {
				echo json_encode(array("error"=>$error));
				return FALSE;
			}
		}
	}

	function validate_fields($categoryId = NULL)
	{
		if(!empty($categoryId)) {
			switch ($categoryId) {
				case 5:
					$this->form_validation->set_rules('title', '"Title"', 'required');
					$this->form_validation->set_rules('mydate', '"Date Posted"', 'required|callback_is_date', array('is_date'=>'Invalid Date.'));
					$this->form_validation->set_rules('state', '"State"', 'required');
					$this->form_validation->set_rules('email', '"Email"', 'required|valid_email');
					$this->form_validation->set_rules('contactName', '"Contact Name"', 'required');
					$this->form_validation->set_rules('companyName', '"Company Name"', 'required');
					$this->form_validation->set_rules('contactNumber', '"Contact Number"', 'required');

					break;
				
				default:
					if($categoryId !== 445 && $categoryId !== 289) $this->form_validation->set_rules('title', '"Title"', 'required');
					$this->form_validation->set_rules('mydate', '"Date Posted"', 'required|callback_is_date', array('is_date'=>'Invalid Date.'));
					if($categoryId === 4) $this->form_validation->set_rules('state', '"State"', 'required');

					if($categoryId === 671) {
						$this->form_validation->set_rules('enddate', '"End Date"', 'required|callback_is_date', array('is_date'=>'Invalid Date.'));
						$this->form_validation->set_rules('drawdate', '"Draw Date"', 'required|callback_is_date', array('is_date'=>'Invalid Date.'));
					}

					break;
			}

			if ($this->form_validation->run() == FALSE){
	            return array("valid"=>FALSE, "error"=>$this->form_validation->error_array());
	        }
	        else {
	        	return array("valid"=>TRUE);
	        }			
		}
	}

	function is_date ($dateTimeStr) 
	{
		if(empty($dateTimeStr)) return TRUE;

		$dateTime = explode(' ', $dateTimeStr);

		$au_date = explode('/', $dateTime[0]);

		if(count($au_date) === 3) {
			$day 	= $au_date[0];
			$month 	= $au_date[1];
			$year 	= $au_date[2];

			if(is_numeric($day) && is_numeric($month) && is_numeric($year)) {
				return checkdate(intval($month), intval($day), intval($year));
			}
			else {
				return FALSE;
			}
		}
		else {
			return FALSE;
		}
	}
}


