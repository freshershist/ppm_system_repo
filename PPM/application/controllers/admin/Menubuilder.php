<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menubuilder extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('file');
		$this->load->helper('directory');
		$this->load->helper('form');

		// $this->load->library('ppmsystemlib');
		$this->load->helper('PPMSystem');
		$this->load->library('PPMSystemLib');
		

		$this->load->model('admin/dashboardmodel', 'dashboard');
		$this->load->model('admin/menubuildermodel', 'menubuilder');
		$this->load->model('admin/pagesmodel', 'pages');

		$this->action = '';
		$this->memberId = 0;
		$this->page_id = 0;

	}
	public function index(){


		// redirect(base_url() . 'admin/pages/list');
		echo '<pre>';

		print_r($this->ppmsystemlib->get_accepted_mimes());

		die();
	}

	public function show_list(){
		$this->action = $this->uri->segment(3, 'list');
		//$this->dashboard->get_moderators('all');
		//echo $this->action;
		//echo '<pre>';
		//print_r();
		//die();

		// $filters = $this->get_filters();

		// if($this->action === 'add'){
		// 	if(!empty($this->uri->segment(4, 0))) redirect(base_url() . 'admin/members/add');
		// }
		// elseif($this->action === 'view'){
		//
		// 	$this->memberId = $this->uri->segment(4, 0);
		//
		// 	if(!empty($this->memberId) && is_numeric($this->memberId)){
		// 		$filters['member_data'] = $this->members->get_entry($this->memberId);//12651,877,16076
		// 	}
		// 	else{
		// 		redirect(base_url() . 'admin/members');
		// 	}
		//
		// }

		$data['active_page'] 	= "Banners";

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/switchery/dist/switchery.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/iCheck/skins/flat/green.css" rel="stylesheet">';

		$this->load->view('admin/common/header', $data);
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/common/topnav');

		$this->load->helper('form');

		// $data['filters'] 	= $this->load->view('admin/members_filters', $filters, TRUE);
		// $data['add_edit'] 	= $this->load->view('admin/members_add_edit', $filters, TRUE);
		$data['action'] 	= $this->action;
		$data['list_array'] = $this->menubuilder->get_list();


		$this->load->view('admin/menulist', $data);

		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/switchery/dist/switchery.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/moment/min/moment.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net/js/jquery.dataTables.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/iCheck/icheck.min.js"></script>';



		$this->load->view('admin/common/footer', $data);
	}

	public function menubuilder(){
		$this->action = $this->uri->segment(3, 'add');
		

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/switchery/dist/switchery.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/iCheck/skins/flat/green.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/dropzone/dist/min/dropzone.min.css" rel="stylesheet">';

		$this->load->view('admin/common/header', $data);
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/common/topnav');

		$this->load->helper('form');

		$data['action'] 	= $this->action;


		$this->load->view('admin/menubuilder', $data);

		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/switchery/dist/switchery.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/moment/min/moment.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net/js/jquery.dataTables.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/iCheck/icheck.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/dropzone/dist/min/dropzone.min.js"></script>';

		$this->load->view('admin/common/footer', $data);
	}

	public function menubuilder_edit(){
		$this->action = $this->uri->segment(3, 'edit');

		$this->menu_id = $this->uri->segment(4);

		$data['menu_details'] = $this->menubuilder->get_menu_details($this->menu_id);

		if(empty($this->menu_id)){
			// $data['active_page'] 	= "Menu Builder";	
			redirect(base_url() . 'admin/menubuilder');
		}else{
			$result = count($data['menu_details']['result']);
			if($result == 0){
				redirect(base_url() . 'admin/menus');	
			}

			$data['active_page'] 	= "Edit Menu Builder";
		}
		

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/switchery/dist/switchery.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/iCheck/skins/flat/green.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/dropzone/dist/min/dropzone.min.css" rel="stylesheet">';

		$this->load->view('admin/common/header', $data);
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/common/topnav');

		$this->load->helper('form');

		$data['action'] 	= $this->action;
		$data['menu_id']    = $this->menu_id;



		$this->load->view('admin/menubuilder', $data);

		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/switchery/dist/switchery.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/moment/min/moment.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net/js/jquery.dataTables.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/iCheck/icheck.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/dropzone/dist/min/dropzone.min.js"></script>';

		$this->load->view('admin/common/footer', $data);
	}

	public function saveMenu(){
		$menu_id = $_POST['menuID'];
		$menuName = $_POST['menuName'];

		if(empty($menu_id)|| !isset($menu_id)){
			$menu_id = '';	
		}
		if(!isset($menuName)){
			$menuName = '';
		}

		$rs = $this->menubuilder->save_menuName($menu_id, $menuName);
		echo $rs;

		// $rs = $this->banners->save_banner($bid, $txtLink, $placement, $selectedFilePath, $isActive);
		// echo json_encode($rs);
		// print_r($_POST);
		// echo json_encode($_POST);
	}

	public function saveMenuItem(){

		$menu_data = $this->input->post('nest_data');
		$menu_list = $this->input->post('nest_list');
		$menu_id   = $this->input->post('menuID');

		$rs = $this->menubuilder->save_menuItems($menu_id, $menu_list, $menu_data);
		echo $rs;
	}

	public function menubuilder_delete(){
		$menu_id   = $this->input->post('menuID');
		$rs = $this->menubuilder->delete_menu($menu_id);
	}

}
