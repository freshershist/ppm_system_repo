<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('file');
		$this->load->helper('directory');
		$this->load->helper('form');
		$this->load->helper('text');
		$this->load->library('PPMSystemLib');

		$this->load->model('admin/dashboardmodel', 'dashboard');
		$this->load->model('admin/awardsmodel', 'awards');
		
		if(!$this->dashboard->is_logged_in()) {
			redirect(base_url() . 'admin/login');
		}
		else {
			if(!$this->dashboard->check_user_access(13)) {
				redirect(base_url() . 'admin/home');
			}
		}
	}

	public function index()
	{

		$this->action = $this->uri->segment(3, 'list');

		$data['active_page'] 	= "Gallery";
		$data['page_title'] 	= "Gallery";

		$data['action']	= $this->action;

		$categories = $this->dashboard->list_categories(27, 0);

		$categories_arr = array();
		$categories_add_edit_arr = array(''=>'[ Select ]');

		foreach ($categories as $key => $value) {
			$categories_arr[$value['id']] = $value['name'];
			$categories_add_edit_arr[$value['id']] = $value['name'];
		}

		$filters['categories_arr'] = $categories_arr;

		$data['filters'] 	= $this->load->view('admin/gallery_filters', $filters, TRUE);

		$gallery_data = $this->ppmsystemlib->get_files();

		$gallery_data['directory'] 	= directory_map(UPLOADFOLDER . '/files');
		$gallery_data['acceptedFiles'] = $this->ppmsystemlib->get_accepted_mimes();

		$data['upload'] = $this->load->view('admin/common/dz_modal', $gallery_data, TRUE);

		$add_edit['categories_arr'] = $categories_add_edit_arr;

		$data['add_edit'] = $this->load->view('admin/gallery_add_edit', $add_edit, TRUE);

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/switchery/dist/switchery.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/iCheck/skins/flat/green.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/dropzone/dist/min/dropzone.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/js/fancybox-master/dist/jquery.fancybox.min.css" rel="stylesheet" media="screen">';

		$this->load->view('admin/common/header', $data);
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/common/topnav');

		$this->load->helper('form');

		$this->load->view('admin/gallery', $data);

		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/switchery/dist/switchery.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/moment/min/moment.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/js/moment/moment.timezone_2012_2022.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net/js/jquery.dataTables.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/iCheck/icheck.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.buttons.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/dropzone/dist/min/dropzone.min.js"></script>';

		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/js/html5sortable/jquery.sortable.min.js"></script>';

		$data['show_categories'] = $this->load->view('admin/show_categories', array('section_name'=> array('gallery')), TRUE);

		$this->load->view('admin/common/footer', $data);
	}

	public function get_files_from_folder()
	{
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$pathFolder = $this->input->post('path');
		$fileType = $this->input->post('file_type');

		$fileType = (empty($fileType)) ? 'application' : $fileType;

		if(! empty($pathFolder)) {
			echo $this->ppmsystemlib->get_files($pathFolder, $fileType)['file_info'];
		}
	}

	private function get_files ($path = '') {
		$arr_img = array();

		$path = ($path === 'files/') ? '': $path;

		$map = directory_map(UPLOADFOLDER . '/files/' . $path, 1);

		// echo '<pre>';

		// print_r($map);

		// die();

		foreach ($map as $key => $value) {

			if(is_string($value) && $value!=='index.html' && !is_numeric(strpos($value, '/'))) {
				if(file_exists(UPLOADFOLDER.'/files/'.$path.$value)){
					$arr_info = get_file_info(UPLOADFOLDER.'/files/'.$path.$value);
					if(array_key_exists('server_path',$arr_info)) unset($arr_info['server_path']);
					$arr_img[] = $arr_info;
				}
			}
		}

		$gallery_data['file_info'] = json_encode($arr_img);

		return $gallery_data;
	}

	function get_gallery () {

		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$get = $this->input->get();

		if(isset($get) && ! empty($get)) {
			$gallery_data = $this->get_files();

			echo json_encode($gallery_data['file_info']);
		}
		else {
			echo "failed";
		}
	}

	public function create_new_folder ()
	{
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$post = $this->input->post();

		if(! empty($post)) {
			$path 	= $this->input->post('path');
			$folder = $this->input->post('folder');

			if(! $this->ppmsystemlib->create_folder($path, $folder)) {
				echo json_encode(array('result'=>'failed'));
			}
			else {
				$map = directory_map(UPLOADFOLDER . '/files');

				if(is_array($map)) {
					$li = '<li><label id="path-root" data-path="files/">files/</label><ul><li>';
					
        			foreach ($map as $key => $value) {
        				if(is_array($value)) {
        					$li .= $this->check_subdirectory(str_replace('\\', '/', $key), $value);
        				}
        			}
        			$li .= '</li></ul></li>';
        		}

				echo json_encode(array('result'=>'success', 'dir'=>$li));
			}

		}
	}

	private function check_subdirectory($key, $subDir){
		$li = '<li><label data-path="' . $key . '"">'.$key.'</label><ul>';
		
		foreach ($subDir as $k1 => $v1) {
			if(is_array($v1)) {
				$li .= $this->check_subdirectory(str_replace('\\', '/', $k1), $v1);
			}
		}
		$li .= '</ul></li>';

		return $li;
	}

	public function get_list() 
	{
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$post = $this->input->post();

		if(count($post)>0){

			$categoryId = $this->input->post('category');
			$from = $this->input->post('datePostedFrom');
			$to = $this->input->post('datePostedTo');

			$result = array();

			if(is_numeric($categoryId)) {
				$result = $this->dashboard->get_gallery_by_category($categoryId, $this->ppmsystemlib->set_final_date_time($from), $this->ppmsystemlib->set_final_date_time($to));
			}

			/*
			[0] => Array
		        (
		            [id] => 17
		            [name] => Wide Bay Prestige Properties
		            [category] => 934
		            [myyear] => 2009
		            [d1] => NEW Website Graphics/PPM Award Winners/2010 to 2014 PPMsystem/Wide Bay Prestige Properties - 300x180.jpg
		            [body] => PPM Group Property Management Excellence Award  agency of the Year (Member) 
		            [orderby] => 0
		            [mytype] => 1
		            [categoryname] => PPM Group Property Management Excellence Award Agency of the Year (Member)
		        )
			*/

			$result_arr = array();

			foreach ($result as $key => $value) {
				$arr = array();
				
				$arr['id'] 				= $value['id'];
				$arr['name'] 			= '<span class="text-word-wrap">'.$value['name'].'</span>';
				$arr['category']		= (strlen($value['categoryname']) <= 30) ? $value['categoryname'] : '<span class="text-word-wrap">'.$value['categoryname'].'</span>';
				$arr['shortdesc'] 		= (empty($value['shortdesc'])) ? '' : '<span class="text-word-wrap">'.$value['shortdesc'].'</span>';
				$arr['body'] 			= (strlen(strip_tags($value['body'])) <= 30) ? strip_tags($value['body']) : '<span class="truncate" title="' . strip_tags($value['body']) . '">' . ellipsize(strip_tags($value['body']), 50, 1) . '</span>';
				$arr['mydate'] 			= '<font class="hide">'. $this->ppmsystemlib->get_unix_from_date($value['mydate']) . '</font> ' . $this->ppmsystemlib->check_date_time($value['mydate']);

				$arr['action']			= '<ul class="nav nav-pills" role="tablist"> <li role="presentation" class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">Action<span class="caret"></span></a><ul class="dropdown-menu animated fadeInDown" role="menu" aria-labelledby="drop6" style="left:-108%!important;"><li role="presentation"><a tabindex="-1" href="javascript:;">ID: <label class="action-id"></label></a></li><li role="presentation" class="divider"></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="edit">Edit</a></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="delete">Delete</a></li></ul></li></ul>';

				$result_arr[] = $arr;
			}		

			echo json_encode($result_arr);
		}
	}

	public function process_action()
	{
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$post = $this->input->post();

		if(!empty($post)) {
			$id = $this->input->post('id');
			$action = $this->input->post('action');

			if($action === 'edit') {

				if(is_numeric($id)) {
					$result = $this->dashboard->get_gallery_details($id);

					$details 	= array();
					$items		= array();

					if(is_array($result['details'])) {
						foreach ($result['details'] as $v) {
							$details['body'] 			= $v['body'];
							$details['category'] 		= $v['category'];
							$details['id'] 				= $v['id'];
							$details['mydate'] 			= $this->ppmsystemlib->check_date_time($v['mydate']);
							$details['name'] 			= $v['name'];
							$details['shortdesc'] 		= $v['shortdesc'];
						}
					}

					if(is_array($result['items'])) {
						$items = $result['items'];
					}

					echo json_encode(array('details'=>$details, 'items'=>$items));
				}
			}
			elseif($action === 'delete') {
				if(is_numeric($id)) {
					//$this->awards->delete_award($id);

					echo json_encode(array('success'=>1));
				}
				else {
					echo json_encode(array('error'=>1));
				}
			}
		}
		else {
			exit('No direct script access allowed');
		}
	}

	public function add_update()
	{
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$post = $this->input->post();

		if(!empty($post)) {

			$id 				= $this->input->post('id'); 
			$name 				= $this->input->post('name');
			$category 			= $this->input->post('category');
			$mydate 			= $this->input->post('mydate');
			$shortdesc 			= $this->input->post('shortdesc');
			$body 				= $this->input->post('body');
			$galleryitem 		= $this->input->post('galleryitem[]');

			if(is_array($galleryitem) && count($galleryitem)>0) {
	            $galleryitem = array_chunk($galleryitem,4);

	            $ctr = 0;
	            foreach ($galleryitem as $value) {
	            	if(empty($value[0]) && empty($value[1]) && empty($value[2]) && empty($value[3])) {
		            	unset($galleryitem[$ctr]);
		            }

		            $ctr++;
	            }
	        }

			$valid = TRUE;
			$error = NULL;

			$result = $this->validate_fields();

			if(!$result['valid']) {
				$valid = $result['valid'];
				$error = $result['error'];
			}

			if($valid) {

				$data = array();

				$data['body'] 		= $body;
				$data['category'] 	= $category;
				$data['mydate'] 	= $this->ppmsystemlib->set_final_date_time($mydate);
				$data['shortdesc'] 	= $shortdesc;
				$data['name'] 		= $name;

				$arr = array();

				if(is_numeric($id)) { //Edit
					$arr['id'] = $id;

					$this->dashboard->add_update_gallery($id, $data);
				}
				else { //Add

					$id = $this->dashboard->add_update_gallery(NULL, $data);
					$arr['id'] = $id;
				}

				$arr['name'] 			= '<span class="text-word-wrap">'.$name.'</span>';
				$arr['category']		= $category;
				$arr['shortdesc'] 		= (empty($shortdesc)) ? '' : '<span class="text-word-wrap">'.$shortdesc.'</span>';
				$arr['body'] 			= (strlen(strip_tags($body)) <= 30) ? strip_tags($body) : '<span class="truncate" title="' . strip_tags($body) . '">' . ellipsize(strip_tags($body), 50, 1) . '</span>';
				$arr['mydate'] 			= '<font class="hide">'. $this->ppmsystemlib->get_unix_from_date($data['mydate']) . '</font> ' . $mydate;

				$arr['action']			= '<ul class="nav nav-pills" role="tablist"> <li role="presentation" class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">Action<span class="caret"></span></a><ul class="dropdown-menu animated fadeInDown" role="menu" aria-labelledby="drop6" style="left:-108%!important;"><li role="presentation"><a tabindex="-1" href="javascript:;">ID: <label class="action-id"></label></a></li><li role="presentation" class="divider"></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="edit">Edit</a></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="delete">Delete</a></li></ul></li></ul>';

				if(!empty($id)) {
					if(!empty($galleryitem)) {
						$this->dashboard->add_gallery_items($id, $galleryitem);
					}
				}

				echo json_encode($arr);
			}
			else {
				echo json_encode(array("error"=>$error));
				return FALSE;
			}
		}
	}

	function validate_fields()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('name', '"Name"', 'required');
		$this->form_validation->set_rules('mydate', '"Date Posted"', 'required|callback_is_date', array('is_date'=>'Invalid Date.'));
		$this->form_validation->set_rules('category', '"Category"', 'required');

		if ($this->form_validation->run() == FALSE){
            return array("valid"=>FALSE, "error"=>$this->form_validation->error_array());
        }
        else {
        	return array("valid"=>TRUE);
        }
	}

	function is_date ($dateTimeStr) 
	{
		if(empty($dateTimeStr)) return TRUE;

		$dateTime = explode(' ', $dateTimeStr);

		$au_date = explode('/', $dateTime[0]);

		if(count($au_date) === 3) {
			$day 	= $au_date[0];
			$month 	= $au_date[1];
			$year 	= $au_date[2];

			if(is_numeric($day) && is_numeric($month) && is_numeric($year)) {
				return checkdate(intval($month), intval($day), intval($year));
			}
			else {
				return FALSE;
			}
		}
		else {
			return FALSE;
		}
	}	
}
