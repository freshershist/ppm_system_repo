<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ParseOldDB extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('admin/parserdbmodel', 'parserdb');

		ini_set('memory_limit', '1024M');
		ini_set('max_execution_time', 0);
		$this->output->enable_profiler(TRUE);
	}

	public function index()
	{
		 $tables_arr = array(
			'awards', 
			'banners',
			'delegateType',
			'downloadLog',
			'downloadLogENews',
			'downloads',
			'emaillinks',
			'emaillinkslog',
			'emailQueue',
			'emailTemplates',
			'eNews',
			'enquiryDetails',
			'eventBreakoutSessions',
			'eventCosts',
			'events',
			'eventSpeakers',
			'eventSponsors',
			'forumMessages',
			'forums',
			'galleries',
			'galleryitems',
			'links',
			'monthlyAwardStats',
			'news',
			'onlineTraining',
			'pages',
			'partners',
			'polls',
			'promocodes',
			'questionResponses',
			'questions',
			'recruitment',
			'respondants',
			'responses',
			'sysc',
			'tasks',
			'temp61211',
			'testimonials',
			'trainingLog',
			'transactionDetails',
			'transactionItemAttendeeConferenceBreakoutSessions',
			'transactionItemAttendeeConferenceDetails',
			'transactionItemAttendees',
			'transactionItems',
			'transactions',
			'unsubscribers'
		);

		//$this->parserdb->set_values($tables_arr);

		 $this->parserdb->check_members();

	}

	public function get_users () 
	{
		$this->parserdb->users();
	}

	public function get_members () 
	{
		$this->parserdb->members();
	}

	public function login_test () 
	{
		$username = "";

		$salt = sha1($username . SECKEY);
    	$pass = md5($salt . '');

    	$this->parserdb->login($username, $pass);
	}

	public function get_banners ()
	{
		$this->parserdb->banners(1);
	}

	public function delegate_type () {
		$this->parserdb->delegate_type();
	}

	public function download_log()
	{
		$this->parserdb->download_log();
	}
}
