<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promocodes extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('PPMSystemLib');

		$this->load->model('admin/dashboardmodel', 'dashboard');
		$this->load->model('admin/promocodesmodel', 'promo');

		$this->action = '';

		if(!$this->dashboard->is_logged_in()) {
			redirect(base_url() . 'admin/login');
		}
		else {
			if(!$this->dashboard->check_user_access(11)) {
				redirect(base_url() . 'admin/home');
			}
		}
	}

	public function index()
	{

		$this->action = $this->uri->segment(3, 'list');

		$data['active_page'] 	= "Promo Codes";
		$data['page_title'] 	= "Promo Codes";
		$data['action']			= $this->action;

		$data['filters'] 		= $this->load->view('admin/promocodes_filters', NULL, TRUE);

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">';

		$this->load->view('admin/common/header', $data);
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/common/topnav');

		$this->load->helper('form');

		$data['add_edit'] = $this->load->view('admin/promocodes_add_edit', NULL, TRUE);

		$this->load->view('admin/promocodes', $data);

		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/moment/min/moment.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>';

		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net/js/jquery.dataTables.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.buttons.js"></script>';

		$this->load->view('admin/common/footer', $data);	
	}

	public function get_list() 
	{
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$get = $this->input->get();

		if(count($get)>0){

			$result = $this->promo->get_promos();

			$result_arr = array();

			/*
			id
			name
			code
			startdate
			enddate
			*/

			foreach ($result as $key => $value) {
				$arr = array();
				
				$arr['id'] 			= $value['id'];
				$arr['name'] 		= (strlen($value['name']) <= 30) ? $value['name'] : '<span class="text-word-wrap">'.$value['name'].'</span>';
				$arr['code']		= $value['code'];
				$arr['startdate']	= '<font class="hide">'. $this->ppmsystemlib->get_unix_from_date($value['startdate']) . '</font> ' . $this->ppmsystemlib->check_date_time($value['startdate']);
				$arr['enddate']		= '<font class="hide">'. $this->ppmsystemlib->get_unix_from_date($value['enddate']) . '</font> ' . $this->ppmsystemlib->check_date_time($value['enddate']);
				
				$arr['action']		= '<ul class="nav nav-pills" role="tablist"> <li role="presentation" class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">Action<span class="caret"></span></a><ul class="dropdown-menu animated fadeInDown" role="menu" aria-labelledby="drop6" style="left:-108%!important;"><li role="presentation"><a tabindex="-1" href="javascript:;">ID: <label class="action-id"></label></a></li><li role="presentation" class="divider"></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="edit">Edit</a></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="delete">Delete</a></li></ul></li></ul>';

				$result_arr[] = $arr;
			}		

			echo json_encode($result_arr);
		}
		else {
			echo json_encode(array());
		}		
	}

	public function process_action()
	{
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$post = $this->input->post();

		if(!empty($post)) {
			$id = $this->input->post('id');
			$action = $this->input->post('action');

			if($action === 'edit') {

				if(is_numeric($id)) {
					$result = $this->promo->get_promo($id);

					//echo json_encode($result); die();
					/*
					id
					name
					code
					startdate
					enddate
					*/			

					$data = array();

					foreach ($result as $key => $value) {
						$data['name'] 		= $value['name'];
						$data['code'] 		= $value['code'];
						$data['startdate'] 	= $this->ppmsystemlib->check_date_time($value['startdate']);
						$data['enddate'] 	= $this->ppmsystemlib->check_date_time($value['enddate']);
					}

					echo json_encode($data);
				}
			}
			elseif($action === 'delete') {
				if(is_numeric($id)) {
					$this->promo->delete_promo($id);

					echo json_encode(array('success'=>1));
				}
				else {
					echo json_encode(array('error'=>1));
				}
			}
		}
		else {
			exit('No direct script access allowed');
		}
	}	

	public function add_update()
	{
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$post = $this->input->post();

		/*
		id
		name
		code
		startdate
		enddate
		*/

		if(!empty($post)) {
			$id 		= $this->input->post('id');
			$name 		= $this->input->post('name');
			$code 		= $this->input->post('code'); 
			$startdate 	= $this->input->post('startdate');
			$enddate 	= $this->input->post('enddate');

			$valid = TRUE;
			$error = NULL;

			$result = $this->validate_fields();

			if(!$result['valid']) {
				$valid = $result['valid'];
				$error = $result['error'];
			}

			if($valid) {

				$data = array();

				$data['name'] 		= $name;
				$data['code'] 		= $code;
				$data['startdate'] 	= $this->ppmsystemlib->set_final_date_time($startdate);

				$data['enddate'] 	= $this->ppmsystemlib->set_final_date_time($enddate);

				$arr = array();

				$arr['name'] 		= $name;
				$arr['code']		= $code;
				$arr['startdate'] 	= '<font class="hide">'. $this->ppmsystemlib->get_unix_from_date($this->ppmsystemlib->set_final_date_time($startdate)) . '</font> ' . $startdate;
				$arr['enddate'] 	= '<font class="hide">'. $this->ppmsystemlib->get_unix_from_date($this->ppmsystemlib->set_final_date_time($enddate)) . '</font> ' . $enddate;

				if(is_numeric($id)) { //Edit
					$arr['id'] = $this->promo->add_update_promo($id, $data);
				}
				else { //Add

					$id = $this->promo->add_update_promo(NULL, $data);

					$arr['id'] = $id;
					$arr['action'] = '<ul class="nav nav-pills" role="tablist"> <li role="presentation" class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">Action<span class="caret"></span></a><ul class="dropdown-menu animated fadeInDown" role="menu" aria-labelledby="drop6" style="left:-108%!important;"><li role="presentation"><a tabindex="-1" href="javascript:;">ID: <label class="action-id"></label></a></li><li role="presentation" class="divider"></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="edit">Edit</a></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="delete">Delete</a></li></ul></li></ul>';
				}

				echo json_encode($arr);
			}
			else {
				echo json_encode(array("error"=>$error));
				return FALSE;
			}
		}
	}

	function validate_fields($id = NULL)
	{
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('name', '"Name"', 'required');
		$this->form_validation->set_rules('code', '"Code"', 'required');
		$this->form_validation->set_rules('startdate', '"Start Date"', 'required');
		$this->form_validation->set_rules('enddate', '"End Date"', 'required');

		if ($this->form_validation->run() == FALSE){
            return array("valid"=>FALSE, "error"=>$this->form_validation->error_array());
        }
        else {
        	return array("valid"=>TRUE);
        }
	}	
}
