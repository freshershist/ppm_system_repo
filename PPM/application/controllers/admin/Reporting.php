<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reporting extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('file');
		$this->load->helper('directory');
		$this->load->helper('form');
		$this->load->library('PPMSystemLib');

		$this->load->model('admin/dashboardmodel', 'dashboard');
		$this->load->model('admin/reportingmodel', 'report');

		if(!$this->dashboard->is_logged_in()) {
			redirect(base_url() . 'admin/login');
		}
		else {
			if(!$this->dashboard->check_user_access(19)) {
				redirect(base_url() . 'admin/home');
			}
		}
	}

	public function index()
	{


		/*echo '<pre>';
		print_r($this->report->get_report5());
		die();*/

		$data['active_page'] 	= "reporting";
		$data['page_title']		= "Reporting";

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">';

		$this->load->view('admin/common/header', $data);
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/common/topnav');


		$usersArray 			= $this->dashboard->get_users();
		$typeOfInterestArray 	= $this->ppmsystemlib->get_data_arr('typeOfInterestArray');
		$unsubscribereasonArray = $this->ppmsystemlib->get_data_arr('unsubscribeReasonArray');

		$usersArray[0]			= 'All';
		ksort($usersArray);
		
		$data['usersArray'] 	= $usersArray;
		$data['howDidYouHearArr'] = array();

		for ($i=0; $i < count($typeOfInterestArray); $i++) {
			if($i===0) $data['typeOfInterestArray'][$i] = '[ Select ]';
			$data['typeOfInterestArray'][$i+1] = $typeOfInterestArray[$i];
		}

		for ($i=0; $i < count($unsubscribereasonArray); $i++) {
			if($i===0) $data['unsubscribereasonArray'][''.$i] = '[ Select ]';
			$data['unsubscribereasonArray'][''.$i+1] = $unsubscribereasonArray[$i];
		}

		$data['unsubscribereasonArray']["-1"] = 'All';

		$data['membershipTypeArray'] 	= array(""=>"[ Select ]","All"=>"All","Platinum"=>"Platinum","Gold"=>"Gold","Silver"=>"Silver");
		$data['memberStatusArray'] 	= array(""=>"[ Select ]",1=>"Active",2=>"Suspended",3=>"Removed/Cancelled/Unsubscribed",4=>"Double Ups");


		$this->load->view('admin/reporting', $data);

		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/moment/min/moment.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net/js/jquery.dataTables.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>';				
		$this->load->view('admin/common/footer', $data);
	}

	public function get_list()
	{
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$post = $this->input->post();

		if(!empty($post)) {

			$reportType = $this->input->post('reportType');
			$startDate 	= $this->input->post('startDate');
			$endDate 	= $this->input->post('endDate');

			$data['reportType'] = intval($reportType);

			switch (intval($reportType)) {
				case 1:
					$result 					= $this->report->get_report1($this->ppmsystemlib->convert_au_time_to_standard($startDate), $this->ppmsystemlib->convert_au_time_to_standard($endDate));

					$data['generalClients'] 	= $result['general_clients'];
					$data['states']				= $result['general_clients']['states_data'];
					$data['membershipType_gc']	= $result['general_clients']['membershiptype_data'];
					$data['members']			= $result['members'];
					$data['membershipType_m']	= $result['members']['membershiptype_data'];
					$data['subscribers']		= $result['subscribers'];
					$data['startDate']			= $startDate;
					$data['endDate']			= $endDate;
					$data['unsubscribe_request']= $result['unsubscribe_request'];
					$data['db_updates'] 		= $result['db_updates'];

					break;

				case 2:
					$user 						= $this->input->post('user');
					$typeOfInterest 			= $this->input->post('typeOfInterest');

					$data['result'] 			= $this->report->get_report2(intval($user),intval($typeOfInterest), $startDate, $endDate);

					$typeOfInterest 			= (empty($typeOfInterest)) ? -1 : intval($typeOfInterest);

					$data['typeOfInterest'] 	= $typeOfInterest;
					break;

				case 3:
					$result 					= $this->report->get_report3();
					$data['howdidtheyhear'] 	= $result['howdidtheyhear'];
					break;
				case 4:
					$result 		= $this->report->get_report4($startDate, $endDate);
					$stateResult 	= $this->dashboard->get_state(21);

					foreach ($stateResult as $key => $value) {
						$index = (!empty($value['overrideDropdownValue'])) ? $value['overrideDropdownValue'] : $value['id'];
						$data['stateDictionaryMember'][$index] = $value['name'];
					}
					$data['removed_unsubscribed'] 	= $result;
					break;

				case 5:
					$membershipType = $this->input->post('membershipType');
					$memberStatus = $this->input->post('memberStatus');

					$result = array();

					if(!empty($membershipType) && !empty($memberStatus) && is_numeric($memberStatus)) {
						$result = $this->report->get_report5($membershipType, intval($memberStatus));
					}

					
					$data['result'] = $result;
									
					break;

				case 6:
					$reportunsubscribereason 	= $this->input->post('reportunsubscribereason');

					if(intval($reportunsubscribereason) === -1) {
						$data['result']	= $this->report->get_report6($this->ppmsystemlib->convert_au_time_to_standard($startDate), $this->ppmsystemlib->convert_au_time_to_standard($endDate), $reportunsubscribereason);
						$data['isAll'] = TRUE;

					}
					else {
						$data['result']	= $this->report->get_report6($this->ppmsystemlib->convert_au_time_to_standard($startDate), $this->ppmsystemlib->convert_au_time_to_standard($endDate), $reportunsubscribereason);
						$data['isAll'] = FALSE;
					}
					
					break;			
				default:
					# code...
					break;
			}

			$this->ppmsystemlib->compress_output($this->load->view('admin/reporting_results', $data, TRUE));
		}
	}
}
