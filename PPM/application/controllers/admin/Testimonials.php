<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testimonials extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('file');
		$this->load->helper('directory');
		$this->load->helper('form');

		// $this->load->library('ppmsystemlib');
		$this->load->library('PPMSystemlib');
		

		$this->load->model('admin/dashboardmodel', 'dashboard');
		$this->load->model('admin/testimonialsmodel', 'testimonials');
		$this->load->model('admin/pagesmodel', 'pages');

		$this->action = '';
		$this->memberId = 0;
		$this->page_id = 0;

	}
	public function index(){


		// redirect(base_url() . 'admin/pages/list');
		echo '<pre>';

		print_r($this->ppmsystemlib->get_accepted_mimes());

		die();
	}

	public function show_list(){
		$this->action = $this->uri->segment(3, 'list');
		//$this->dashboard->get_moderators('all');
		//echo $this->action;
		//echo '<pre>';
		//print_r();
		//die();

		// $filters = $this->get_filters();

		// if($this->action === 'add'){
		// 	if(!empty($this->uri->segment(4, 0))) redirect(base_url() . 'admin/members/add');
		// }
		// elseif($this->action === 'view'){
		//
		// 	$this->memberId = $this->uri->segment(4, 0);
		//
		// 	if(!empty($this->memberId) && is_numeric($this->memberId)){
		// 		$filters['member_data'] = $this->members->get_entry($this->memberId);//12651,877,16076
		// 	}
		// 	else{
		// 		redirect(base_url() . 'admin/members');
		// 	}
		//
		// }

		$data['active_page'] 	= "Testimonials";

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/switchery/dist/switchery.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/iCheck/skins/flat/green.css" rel="stylesheet">';

		$this->load->view('admin/common/header', $data);
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/common/topnav');

		$this->load->helper('form');

		// $data['filters'] 	= $this->load->view('admin/members_filters', $filters, TRUE);
		// $data['add_edit'] 	= $this->load->view('admin/members_add_edit', $filters, TRUE);
		$data['action'] 	= $this->action;
		$data['list_array'] = $this->testimonials->get_testimonials();
		$data['cat_array']  = $this->testimonials->get_TestiCategory();
		$data['staffs']   = $this->testimonials->get_Staff();



		$this->load->view('admin/testimonials', $data);

		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/switchery/dist/switchery.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/moment/min/moment.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net/js/jquery.dataTables.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/iCheck/icheck.min.js"></script>';



		$this->load->view('admin/common/footer', $data);
	}

	public function add_list(){
		$this->action = $this->uri->segment(3, 'add');

		$data['active_page'] 	= "Add New Banner";

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/switchery/dist/switchery.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/iCheck/skins/flat/green.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/dropzone/dist/min/dropzone.min.css" rel="stylesheet">';

		$this->load->view('admin/common/header', $data);
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/common/topnav');

		$this->load->helper('form');

		$data['action'] 	= $this->action;
		// $data['list_array'] = $this->banners->get_banners();

		$gallery_data = $this->ppmsystemlib->get_files();

		$gallery_data['directory'] = directory_map(UPLOADFOLDER . '/files');
		$gallery_data['acceptedFiles'] = $this->ppmsystemlib->get_accepted_mimes();
		$data['upload'] = $this->load->view('admin/common/dz_modal', $gallery_data, TRUE);
		$d['editor_id'] = 'editor_1';

		$data['editor'] = $this->load->view('admin/common/editor', $d, TRUE);

		$data['pages'] = $this->pages->get_pages();

		$data['cat_array']  = $this->testimonials->get_TestiCategory();

		$this->load->view('admin/testimonials_add_edit', $data);

		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/switchery/dist/switchery.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/moment/min/moment.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net/js/jquery.dataTables.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/iCheck/icheck.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/dropzone/dist/min/dropzone.min.js"></script>';

		$this->load->view('admin/common/footer', $data);

	}

	public function edit_list(){
		$this->action = $this->uri->segment(3, 'add');
		$this->testimonial_id = $this->uri->segment(4);

		if(empty($this->testimonial_id)){
			redirect(base_url() . 'admin/testimonials/list');
			die();
		}

		$data['active_page'] 	= "Edit Banner";

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/switchery/dist/switchery.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/iCheck/skins/flat/green.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/dropzone/dist/min/dropzone.min.css" rel="stylesheet">';

		$this->load->view('admin/common/header', $data);
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/common/topnav');

		$this->load->helper('form');

		$data['action'] 	= $this->action;
		// $data['list_array'] = $this->banners->get_banners();

		$gallery_data = $this->ppmsystemlib->get_files();

		$gallery_data['directory'] = directory_map(UPLOADFOLDER . '/files');
		$gallery_data['acceptedFiles'] = $this->ppmsystemlib->get_accepted_mimes();
		$data['upload'] = $this->load->view('admin/common/dz_modal', $gallery_data, TRUE);
		$d['editor_id'] = 'editor_1';

		$data['editor'] = $this->load->view('admin/common/editor', $d, TRUE);

		$data['pages'] = $this->pages->get_pages();

		$data['page_id'] = $this->testimonial_id;
		$data['testi_details'] = $this->testimonials->getTestiDetails($this->testimonial_id);
		$data['category_id'] = $this->testimonials->getTestCatergoryByTID($this->testimonial_id);
		$data['cat_array']  = $this->testimonials->get_TestiCategory();

		$this->load->view('admin/testimonials_add_edit', $data);

		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/switchery/dist/switchery.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/moment/min/moment.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net/js/jquery.dataTables.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/iCheck/icheck.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/dropzone/dist/min/dropzone.min.js"></script>';

		$this->load->view('admin/common/footer', $data);

	}

	// public function saveBanner(){
	// 	$bid = $_POST['bid'];
	// 	$txtLink = $_POST['txtLink'];
	// 	$placement = $_POST['placement'];
	// 	$selectedFilePath = $_POST['selectedFilePath'];
	// 	$isActive = $_POST['isActive'];

	// 	if(empty($bid)){
	// 		$bid = '';	
	// 	}

	// 	$rs = $this->banners->save_banner($bid, $txtLink, $placement, $selectedFilePath, $isActive);
	// 	// echo json_encode($rs);
	// 	// print_r($_POST);
	// 	echo json_encode($_POST);
	// }

	public function category(){
		$cat_id   = $this->input->post('cat_id');
		$rs = $this->testimonials->get_testimonialsByCat($cat_id);
		echo json_encode($rs);
	}

	public function load_testi_by_catid(){
		$cat_id = $this->input->post('cat_id');
		$rs = $this->testimonials->getTestiByCatId($cat_id);
		echo json_encode($rs);
	}

	public function save_data(){
		$post = $this->input->post();
		$rs = $this->testimonials->saveData($post);
		print_r($rs);
	}

}
