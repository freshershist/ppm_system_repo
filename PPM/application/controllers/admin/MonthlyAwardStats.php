<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MonthlyAwardStats extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('PPMSystemLib');

		$this->load->model('admin/dashboardmodel', 'dashboard');

		$this->action = '';

		if(!$this->dashboard->is_logged_in()) {
			redirect(base_url() . 'admin/login');
		}
		else {
			if(!$this->dashboard->check_user_access(18)) {
				redirect(base_url() . 'admin/home');
			}
		}
	}

	public function index()
	{	

		$data['active_page'] 	= "Monthly Award Statistics";
		$data['page_title'] 	= "Monthly Award Statistics";
		$data['action']			= 'list';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">';

		$this->load->view('admin/common/header', $data);
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/common/topnav');

		$data['categoryArr']	= array(''=>'[ Select ]', '1'=>'Monthly Statistics', '2'=>'Annual Overview', '3'=>'Individual Achievement');
		$data['reportYear']		= array(''=>'[ Select ]');

		$now = intval(date("Y", Now()));

		for ($i=2007; $i <= $now; $i++) { 
			$data['reportYear'][$i] = $i . '/' . ($i + 1);
		}


		$this->load->view('admin/monthlyawardstats', $data);

		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/moment/min/moment.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>';

		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net/js/jquery.dataTables.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>';

		$this->load->view('admin/common/footer', $data);	
	}

	public function get_list() 
	{
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$post = $this->input->post();

		if(count($post)>0){

			$category 	= $this->input->post('category');
			$fiscalYear = $this->input->post('fiscalYear');

			$masterList = array();

			switch (intval($category)) {
				case 1://Monthly Statstics
					
					$startDate 	= $this->input->post('startDate');
					$endDate 	= $this->input->post('endDate');
					$period 	= array();

					if(!empty($startDate) && !empty($endDate)) {

						$startPeriod = date('Y', strtotime($startDate)) . '-' . date('m', strtotime($startDate)) . '-' . 1;

						$endYear 	= date('Y', strtotime($endDate));
						$endMonth 	= date('m', strtotime($endDate));

						$endPeriod 	= date('Y', strtotime($endDate)) . '-' . date('m', strtotime($endDate)) . '-' . cal_days_in_month(CAL_GREGORIAN,$endMonth, $endYear);

						$col = array('vacancyRate','rentArrears','comissionIncrease','numberOfPropertiesGained','numberOfPropertiesLost','net','propertiesGainedDollar','propertieslostDollar','dollarIncrease');

						$result = $this->dashboard->get_monthly_stats($startPeriod, $endPeriod);

						$start    = (new DateTime($startPeriod))->modify('first day of this month');
						$end      = (new DateTime($endPeriod))->modify('first day of next month');
						$interval = DateInterval::createFromDateString('1 month');
						$period   = new DatePeriod($start, $interval, $end);

						$periodArr = array();

						foreach ($period as $dt) {
							$periodArr[] = $dt->format("M");
						}

						$totalAvgArr = array();
						$totalMonths = count($periodArr);
						$lastId = 0;
						$totalRecords = count($result);

						$totalPeriodArr = array();

						foreach ($result as $key => $value) {

							$startPeriod 	= '';
							$endPeriod 		= '';
							$totalAvg 		= 0.0;

							for ($i=0; $i <= 8 ; $i++) {
								$avg 			= 0;
								$avgTotal 		= 0;

								foreach ($period as $dt) {
								    //echo $dt->format("M") . "<br>\n" . $value['id'] . ' - ' . $col[$i];

								    $amount = $this->dashboard->get_month_stats($value['id'], $col[$i], intval($dt->format("m")), intval($dt->format("Y")));

									$avg = round($amount, 2);
									$neg = ($avg < 0) ? '-' : '';
									$avg = (($i === 2 OR $i === 6 OR $i === 7 OR $i === 8) && $avg < 0) ? number_format(abs($avg), 2) : number_format($avg, 2);
									$color = (empty($neg)) ? '<span>' : '<span class="text-danger">';

									$masterList[$value['id']][$col[$i]][$dt->format("M")] =  $color . (($i === 2 OR $i === 6 OR $i === 7 OR $i === 8) ? $neg . '$'. $avg : round($amount, 2) . (($i < 2) ? '%' : ''))  . '</span>';

									if(!in_array($i, array(3,4,6,7))) {
										if(!isset($totalPeriodArr[$col[$i]]['total'])) {
											$totalPeriodArr[$col[$i]]['total'] = round($amount, 2);
										}
										else {
											$totalPeriodArr[$col[$i]]['total'] += round($amount, 2);
										}
									}
									else {
										//29.5 140
									}

									$avgTotal += round($amount, 2);
								}

								//i = 6 or i = 7 or i = 4 or i = 3
								if(!in_array($i, array(3,4,6,7))) {
									$avg = round($avgTotal / $totalMonths, 2);

									$neg = ($avgTotal < 0) ? '-' : '';
									$avg = (($i === 2 OR $i === 4 OR $i === 6 OR $i === 7 OR $i === 8) && $avg < 0) ? number_format(abs($avg), 2) : number_format($avg, 2);

									$color = (empty($neg)) ? '<span class="text-success">' : '<span class="text-danger">';

									$masterList[$value['id']][$col[$i]]['avg'] = $color . (($i === 2 OR $i === 4 OR $i === 6 OR $i === 7 OR $i === 8) ? $neg . '$' . $avg : round($avgTotal / $totalMonths, 2) . (($i < 2) ? '%' : '')) . '</span>';
							
								}
							}

							$masterList[$value['id']]['id'] 		= '<span>' . $value['id'] . '</span>';
							$masterList[$value['id']]['company'] 	= '<span>' . $value['company'] . '</span>';

							$lastId = $value['id'];
						}

						for ($i=0; $i <= 8 ; $i++) {
													
						}

						//echo array_sum($totalPeriodArr);

						//print_r($totalPeriodArr);die();





						$lastId = intval($lastId) + 1;

						//Total Avg
						for ($i=0; $i <= 8 ; $i++) {

							foreach ($period as $dt) {
								$masterList[$lastId][$col[$i]][$dt->format("M")] =  '';
							}
							
							if(!in_array($i, array(3,4,6,7))) {
								$avg = round($totalPeriodArr[$col[$i]]['total'] / $totalRecords, 2);
								$neg = ($avg < 0) ? '-' : '';
								$avg = (($i === 2 OR $i === 4 OR $i === 6 OR $i === 7 OR $i === 8) && $avg < 0) ? number_format(abs($avg), 2) : number_format($avg, 2);

								$color = (empty($neg)) ? '<span class="text-success">' : '<span class="text-danger">';

								$masterList[$lastId][$col[$i]]['avg'] = $color . (($i === 2 OR $i === 4 OR $i === 6 OR $i === 7 OR $i === 8) ? $neg . '$' . $avg : round($totalPeriodArr[$col[$i]]['total'] / $totalRecords, 2) . (($i < 2) ? '%' : '')) . '</span>';
							}

						}

						$masterList[$lastId]['id'] 			= '<span class="hide">' . $lastId . '</span>';
						$masterList[$lastId]['company'] 	= '';

						//Total Avg


						$data['colArr']		= $col;
						$data['periodArr'] 	= $periodArr;
						$data['masterList'] = json_encode($masterList);

						echo $this->load->view('admin/monthlyawardstats_table', $data, TRUE);

						return FALSE;

					}

					break;
				
				case 2://Annual Overview
					$fiscalYear = intval($fiscalYear);

					$result = $this->dashboard->get_annual_report($fiscalYear);

					$list = array();

					$col = array('vacancyRate','rentArrears','comissionIncrease','net','dollarIncrease');

					foreach ($result as $key => $value) {

						$startPeriod 	= '';
						$endPeriod 		= '';

						for ($i=0; $i < 5 ; $i++) {
							$avg = 0;
							$avgTotal = 0;

							for ($k=0; $k < 4 ; $k++) {
								switch ($k) {
									case 0:
										$startPeriod 	= $fiscalYear . '-' . 7 . '-' . 1;
										$endPeriod 		= $fiscalYear . '-' . 10 . '-' . cal_days_in_month(CAL_GREGORIAN,10,$fiscalYear);
										break;

									case 1:
										$startPeriod 	= $fiscalYear . '-' . 10 . '-' . 1;
										$endPeriod 		= $fiscalYear . '-' . 12 . '-' . cal_days_in_month(CAL_GREGORIAN,12,$fiscalYear);
										break;
									case 2:
										$startPeriod 	= $fiscalYear + 1 . '-' . 1 . '-' . 1;
										$endPeriod 		= $fiscalYear + 1 . '-' . 3 . '-' . cal_days_in_month(CAL_GREGORIAN,3,$fiscalYear + 1);
										break;
									case 3:
										$startPeriod 	= $fiscalYear + 1 . '-' . 4 . '-' . 1;
										$endPeriod 		= $fiscalYear + 1 . '-' . 6 . '-' . cal_days_in_month(CAL_GREGORIAN,6,$fiscalYear + 1);
										break;				
									
									default:
										# code...
										break;
								}

								$amount = $this->dashboard->get_quarter_report($value['id'], $col[$i], $startPeriod, $endPeriod);

								$avg = round($amount / 3, 2);
								$neg = ($avg < 0) ? '-' : '';
								$avg = (($i === 2 OR $i === 4) && $avg < 0) ? number_format(abs($avg), 2) : number_format($avg, 2);
								$color = (empty($neg)) ? '<span>' : '<span class="text-danger">';

								$masterList[$value['id']][$col[$i]]['q'.($k+1)] =  $color . (($i === 2 OR $i === 4) ? $neg . '$'. $avg : round($amount / 3, 2) . '%')  . '</span>';

								$avgTotal += round($amount / 3, 2);
							}

							$avg = round($avgTotal / 4, 2);
							$neg = ($avgTotal < 0) ? '-' : '';
							$avg = (($i === 2 OR $i === 4) && $avg < 0) ? number_format(abs($avg), 2) : number_format($avg, 2);

							$color = (empty($neg)) ? '<span class="text-success">' : '<span class="text-danger">';

							$masterList[$value['id']][$col[$i]]['avg'] = $color . (($i === 2 OR $i === 4) ? $neg . '$' . $avg : round($avgTotal / 4, 2) . '%') . '</span>';
						}

						$masterList[$value['id']]['id'] 		= '<span>' . $value['id'] . '</span>';
						$masterList[$value['id']]['company'] 	= '<span>' . $value['company'] . '</span>';
					}

					echo json_encode($masterList);

					break;

				case 3://Individual Achievement
					
					$startDate 	= $this->input->post('startDate');
					$endDate 	= $this->input->post('endDate');

					if(!empty($startDate) && !empty($endDate)) {

						$startPeriod = date('Y', strtotime($startDate)) . '-' . date('m', strtotime($startDate)) . '-' . 1;

						$endYear 	= date('Y', strtotime($endDate));
						$endMonth 	= date('m', strtotime($endDate));

						$endPeriod 	= date('Y', strtotime($endDate)) . '-' . date('m', strtotime($endDate)) . '-' . cal_days_in_month(CAL_GREGORIAN,$endMonth, $endYear);

						$result = $this->dashboard->get_individual_achievement($startPeriod, $endPeriod);

						$returnArr = array();

						foreach ($result as $key => $value) {
							$arr = array();

							$arr['company']		= $value['company'];
							$arr['forMonth'] 	= '<font class="hide">'. $this->ppmsystemlib->get_unix_from_date($value['forMonth']) . '</font> ' . $this->ppmsystemlib->check_date_time($value['forMonth'], 'd/m/Y');
							$arr['individualAchievement'] = (strlen($value['individualAchievement']) <= 30) ? $value['individualAchievement'] : '<span class="text-word-wrap">'.$value['individualAchievement'].'</span>';

							$returnArr[] = $arr;
						}

						echo json_encode($returnArr);

					}

					break;	

				default:
					# code...
					break;
			}
		}
		else {
			echo json_encode(array());
		}		
	}
}
