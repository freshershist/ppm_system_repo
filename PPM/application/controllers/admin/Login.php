<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('form_validation');

		$this->load->model('admin/dashboardmodel', 'dashboard');
	}

	public function index()
	{
		if($this->dashboard->is_logged_in()) {
			redirect(base_url() . 'admin/home');
		}

		$data['title'] 	= APP_TITLE . " Admin Login";
		$this->load->view('admin/login', $data);
	}

    public function check_user()
    {
        $post = $this->input->post();

        if(!empty($post)) {
            $username = $this->input->post('username');
            $password = $this->input->post('password');

            $result = $this->dashboard->login_user($username, $password);

            if(count($result)>0) {
                $data = array(
                        'admin_username'    => $username,
                        'admin_id'          => $result[0]['id'],
                        'admin_access'      => $result[0]['access'],
                        'admin_user_type'   => $this->dashboard->data_collect['user_type'][$result[0]['userType']],
                        'admin_logged_in'   => TRUE,
                        'admin_status'      => (intval($result[0]['userType']) === 1) ? TRUE : FALSE
                );

                $this->session->set_userdata($data);

                redirect(base_url() . 'admin/home');
            }
            else {
                redirect(base_url() . 'admin/login');
            }
        }
        else {
            redirect(base_url() . 'admin/login');
        }
    }

    public function logout()
    {
        if(isset($_SESSION['admin_logged_in']) && $_SESSION['admin_logged_in']) {
            session_unset();
            session_destroy();
        }

        redirect(base_url() . 'admin/login');
    }
}
