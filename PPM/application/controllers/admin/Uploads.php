<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Uploads extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('file');
		$this->load->library('upload');

		$this->load->model('admin/dashboardmodel', 'dashboard');

		if(!$this->dashboard->is_logged_in()) {
			redirect(base_url() . 'admin/login');
		}		
	}

	public function index()
	{

		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$post = $this->input->post();

		if(!empty($post)) {
			$path 		= $this->input->post('path');
			$file_type 	= $this->input->post('file_type');
			
			$path = ($path === 'files/') ? '': $path;
			$allowed_types = '';

			switch ($file_type) {
				case 'image':
					$allowed_types = 'gif|jpg|png';
					break;
				
				case 'application':
					$allowed_types = 'doc|docx|word|pdf|xls|xlsx|ppt|pptx';
					break;

				case 'audio':
					$allowed_types = 'mp3';
					break;

				case 'video':
					$allowed_types = 'wmv|flv|mov|mp4|avi';
					break;

				default:
					$allowed_types = 'gif|jpg|png|doc|docx|word|pdf|xls|xlsx|mp3|wmv|flv|mov|mp4';
					break;
			}

			$config['upload_path'] 		= UPLOADFOLDER . '/files/' . $path;
			$config['allowed_types'] 	= $allowed_types;
			$config['max_size']     	= '0';
			$config['max_width'] 		= '0';
			$config['max_height'] 		= '0';
			$config['encrypt_name']		= FALSE;
			$config['file_ext_tolower'] = TRUE;

			$data = array();

			$this->upload->initialize($config);

	        if (!$this->upload->do_upload('file')) {
	            $error 	= array('error' => $this->upload->display_errors());

	            $url 	= array('url'=>'','error'=>$error);

				echo json_encode($url);
	        }
	        else {
	            $data 	= array('upload_data' => $this->upload->data());
	            //is_image , file_ext
	            $url 	= array('url'=>base_url(). 'assets/uploads/files/'. $path . $data['upload_data']['file_name'], 'filename'=>$data['upload_data']['file_name'], 'is_image'=>$data['upload_data']['is_image'], 'file_ext'=>$data['upload_data']['file_ext']);

				echo json_encode($url);
	        }
    	}
	}

	public function delete_uploaded_file () 
	{
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}		

		$filename 	= $this->input->post('filename');
		$path 		= $this->input->post('path');

		if(isset($filename) && ! empty($filename) && ! empty($path)) {
			$path = ($path === 'files/') ? '': $path;

			if(file_exists(UPLOADFOLDER . '/files/' . $path. $filename)){
				unlink(UPLOADFOLDER . '/files/' . $path. $filename);
				echo json_encode(array('result'=>'success', 'msg'=>'File Deleted.'));
			}
			else {
				echo json_encode(array('result'=>'failed', 'msg'=>'File doesn\'t exist.'));
			}
		}
		else {
			echo json_encode(array('result'=>'error', 'msg'=>'Invalid file.'));
		}
	}
}
