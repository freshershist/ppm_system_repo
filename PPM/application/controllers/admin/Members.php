<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Members extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('file');
		$this->load->helper('directory');
		$this->load->helper('form');
		$this->load->helper('date');
		$this->load->helper('email');

		$this->load->library('form_validation');
		$this->load->library('PPMSystemLib');

		$this->load->model('admin/dashboardmodel', 'dashboard');
		$this->load->model('admin/membersmodel', 'members');

		$this->action = '';
		$this->memberId = 0;

		if(!$this->dashboard->is_logged_in()) {
			redirect(base_url() . 'admin/login');
		}
		else {
			if(!$this->dashboard->check_user_access(1)) {
				redirect(base_url() . 'admin/home');
			}
		}
	}

	public function index()
	{
		redirect(base_url() . 'admin/members/list');
	}

	public function show_list()
	{
		$this->action = $this->uri->segment(3, 'list');
		//$this->dashboard->get_moderators('all');
		//echo $this->action;
		//echo '<pre>';
		//print_r();
		//die();

		$filters = $this->get_filters();

		if($this->action === 'add'){
			$seg = $this->uri->segment(4, 0);
			if(!empty($seg)) redirect(base_url() . 'admin/members/add');
		}
		elseif($this->action === 'edit'){
			$this->action = 'view';
			
			$this->memberId = $this->uri->segment(4, 0);

			if(!empty($this->memberId) && is_numeric($this->memberId)){
				$filters['member_data'] = $this->members->get_entry($this->memberId);//12651,877,16076
			}
			else{
				redirect(base_url() . 'admin/members');
			}
			
		}	

		$data['active_page'] 	= "Members";

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/switchery/dist/switchery.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/iCheck/skins/flat/green.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">';

		$this->load->view('admin/common/header', $data);
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/common/topnav');

		$this->load->helper('form');

		$data['filters'] 	= $this->load->view('admin/members_filters', $filters, TRUE);
		$data['add_edit'] 	= $this->load->view('admin/members_add_edit', $filters, TRUE);
		$data['action'] 	= $this->action;


		$this->load->view('admin/members', $data);

		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/switchery/dist/switchery.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/moment/min/moment.min.js"></script>';
		//$data['other_scripts'][] = '<script src="'. base_url() . 'assets/js/moment/moment.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/js/moment/moment.timezone_2012_2022.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net/js/jquery.dataTables.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/iCheck/icheck.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.buttons.js"></script>';

		$data['show_categories'] = $this->load->view('admin/show_categories', array('section_name'=> array('members','clienttypecategories','memberstatecategories','positiontypecategories')), TRUE);

		$this->load->view('admin/common/footer', $data);
	}

	public function edit_entry()
	{
		$get = $this->input->get();

		if(!empty($get)) {

			$memberId = $this->input->get('memberId');

			$result = $this->members->get_entry($memberId);

			echo json_encode($result);
		}
		else {
			redirect(base_url() . 'admin/members');
		}
	}

	public function process_action()
	{
		$post = $this->input->post();

		if(empty($post)){
			$this->list();
		}
		else{
			$this->memberId = $this->input->post('id');
			$action 		= $this->input->post('action');
			$filters 		= $this->get_filters(TRUE);

			if($action === 'copy' || $action === 'copy-ol-train') {
				$filters['member_data'] = $this->members->get_entry($this->memberId, TRUE, $action === 'copy-ol-train');
			}
			elseif($action === 'edit') {
				$filters['member_data'] = $this->members->get_entry($this->memberId);//12651,877,16076
			}

			$this->ppmsystemlib->compress_output($this->load->view('admin/members_add_edit', $filters, TRUE));
		}
	}

	public function edit_from_report()
	{
		$id = $this->action = $this->uri->segment(4, '');

		if(!empty($id) && is_numeric($id)) {
			
		}
		else {
			redirect(base_url() . 'admin/reporting');
		}
	}

	private function get_filters($isEdit = FALSE)
	{
		if(!$isEdit) {
			$filters['categories'] 				= $this->dashboard->get_categories_options();
			$filters['interest_type'] 			= $this->dashboard->data_collect['interest_type'];
			$filters['stateDictionaryMember']	= $this->dashboard->populate_category_dictionary(21, FALSE);
		}
		
		$filters['member_status'] 			= $this->dashboard->data_collect['member_status'];
		$trust_account_program 				= $this->dashboard->data_collect['trust_account_program'];
		asort($trust_account_program);
		$filters['trust_account_program'] 	= $trust_account_program;
		$filters['members_categories'] 		= $this->dashboard->get_members_categories();
		$filters['state'] 					= $this->dashboard->get_state(21);
		$filters['state_subregion'] 		= $this->dashboard->data_collect['state_subregion'];
		$filters['system_type'] 			= $this->dashboard->data_collect['system_type'];
		$filters['rei_master_modules'] 		= $this->dashboard->get_rei_master_modules();
		$filters['membershipTypeDictionary']= $this->dashboard->populate_category_dictionary(20, FALSE);
		$filters['membershipPayments']		= $this->dashboard->data_collect['membership_payments'];
		$filters['billingCycle'] 			= $this->dashboard->data_collect['billing_cycle'];
		$filters['users']					= $this->dashboard->get_users();
		$filters['frequency_of_audits']		= $this->dashboard->data_collect['frequency_of_audits'];
		$filters['status_arr']				= $this->dashboard->data_collect['status'];
		$filters['staff_positions']			= $this->dashboard->populate_category_dictionary(22, FALSE);
		$filters['action']					= $this->action;
		$filters['reason_cancelled']		= $this->dashboard->data_collect['reason_cancelled'];
		$filters['howdidtheyhear']			= $this->dashboard->data_collect['howdidtheyhear'];

		return $filters;
	}

	/*** Form Submit ***/

	public function get_list()
	{
		$post = $this->input->post();

		if(count($post)>0){

			$result 					= $this->members->get_members_by_filter();
			$membershipTypeDictionary 	= $this->dashboard->populate_category_dictionary(20, FALSE);
			$stateDictionaryMember		= $this->dashboard->populate_category_dictionary(21, FALSE);
			$membershipPayments			= $this->dashboard->data_collect['membership_payments'];
			$billingCycleArr 			= $this->dashboard->data_collect['billing_cycle'];

			$result_arr = array();

			foreach ($result as $key=>$value) {
				$status = intval($value['memberStatus']);
				$membershipType = intval($value['membershipType']);
				$state = intval($value['state']);
				$payment = intval($value['membershipPayments']);
				$billingCycle = intval($value['billingCycle']);

				$statusText = '';

				if($status === 1) {
					if($membershipType === 4 OR $membershipType === 5) {
						$statusText = 'Current';
					}
					else {
						$statusText = 'Active';
					}
				}
				elseif($status === 2) {
					$statusText = 'Suspended';
				}
				elseif($status === 3){
					if($membershipType === 4 OR $membershipType === 5) {
						$statusText = 'Unsubscribed';
					}
					else {
						$statusText = 'Cancelled';
					}
				}

				$value['memberStatus'] 		= $statusText;
				$value['membershipType'] 	= (array_key_exists($membershipType, $membershipTypeDictionary)) ? $membershipTypeDictionary[$membershipType] : '';
				$value['state'] 			= (array_key_exists($state, $stateDictionaryMember)) ? $stateDictionaryMember[$state] : '';
				$value['membershipPayments'] = (array_key_exists($payment-1, $membershipPayments)) ? $membershipPayments[$payment-1] : '';
				$value['billingCycle'] = (array_key_exists($billingCycle-1, $billingCycleArr)) ? $billingCycleArr[$billingCycle-1] : '';
				$value['membershipInvestment'] = round($value['membershipInvestment'],2);
				$value['memberShipCommencementDate'] = $this->ppmsystemlib->check_date_time($value['memberShipCommencementDate']);
				$value['membershipExpires'] = $this->ppmsystemlib->check_date_time($value['membershipExpires']);
				$value['nextAuditDue'] = $this->ppmsystemlib->check_date_time($value['nextAuditDue']);
				$value['consultingHoursRemaining'] = round($value['consultingHoursRemaining'],2);

				if(array_key_exists('lockExpires',$value)) unset($value['lockExpires']);
				if(array_key_exists('lockedByUser',$value)) unset($value['lockedByUser']);

				$value['action']	= '<ul class="nav nav-pills" role="tablist"> <li role="presentation" class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">Action<span class="caret"></span></a><ul class="dropdown-menu animated fadeInDown" role="menu" aria-labelledby="drop6" style="left:-135%!important;"><li role="presentation"><a tabindex="-1" href="javascript:;">ID: <label class="action-id"></label></a></li><li role="presentation" class="divider"></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="edit">Edit</a></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="copy">Copy</a></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="copy-ol-train">Copy OL Train</a></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="delete">Delete</a></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="download-log">Download Log</a></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="email-log">Email Log</a></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="login-log">Login Log</a></li></ul></li></ul>';
				$result_arr[] = $value;
			}

			echo json_encode($result_arr);
		}
		else {
			redirect(base_url() . 'admin/members');
		}
	}

	public function get_logs()
	{
		$post = $this->input->post();

		if(!empty($post)){
			$action = $this->input->post('action');
			$memberId = $this->input->post('id');

			if(!empty($action) && !empty($memberId)) {
				if($action === 'download-log'){
					$sectionArr = $this->dashboard->data_collect['section_array'];

					$results = $this->members->get_logs($memberId,$action);

					$arr = array();

					foreach ($results as $key => $value) {
						if(is_array($value)) {

							$mysection = intval($value['mysection']);

							$value['mydate'] = $this->ppmsystemlib->check_date_time($value['mydate']);
							$value['mysection'] = (array_key_exists($mysection-1, $sectionArr)) ? $sectionArr[$mysection-1] : '';
							$arr[] = $value;
						}
					}

					echo json_encode($arr);
				}
				elseif($action === 'email-log'){
					$action = $this->input->post('action');
					$emailType = $this->input->post('emailtype');
					$results = $this->members->get_logs($memberId,$action,$emailType);

					$arr = array();
					
					foreach ($results as $key => $value) {
						if(is_array($value)) {
							$sent = intval($value['sent']);
							$value['sent'] = ($sent===1) ? '<label class="text-success">Sent</label>' : '<label class="text-warning">In Progress</label>';
							$value['openDateTime'] = ($value['openDateTime'] != '0000-00-00 00:00:00') ? '<label class="text-success">Yes</label>': '<label class="text-danger">No</label>';
							$value['myDateTime'] = $this->ppmsystemlib->check_date_time($value['myDateTime']);
							$value['logId'] = '<a href="javascript:;" class="view-email-log" data-logid="'.$value['logId'].'"><i class="fa fa-eye"></i></a>';
							$arr[] = $value;
						}
					}					

					echo json_encode($arr);
				}
				elseif($action === 'login-log'){
					$results = $this->members->get_logs($memberId,$action);

					$arr = array();

					foreach ($results as $key => $value) {
						if(is_array($value)) {
							$value['myDateTime'] = $this->ppmsystemlib->check_date_time($value['myDateTime']);
							$arr[] = $value;
						}
					}					

					echo json_encode($arr);
				}
			}
		}
	}

	public function update_member () 
	{
		$validate = $this->validate_required();

		if(is_array($validate)) {
			echo json_encode($validate);
			return FALSE;
		}

		//echo json_encode($this->input->post());

		// $time = '01/04/2011 12:00 AM';

		// $time = $this->ppmsystemlib->convert_au_time_to_standard($time);

		// $date = date('Y-m-d h:i:s A', strtotime($time));

		// echo $date;
		$id 								= $this->input->post('id');
        $AdditionalConsultinghoursbilled    = $this->input->post('AdditionalConsultinghoursbilled');
        $company                            = $this->input->post('company');
        $aPlan1                             = $this->input->post('aPlan1');
        $aPlan2                             = $this->input->post('aPlan2');
        $aPlan3                             = $this->input->post('aPlan3');
        $aPlan3a                            = $this->input->post('aPlan3a');
        $aPlan4                             = $this->input->post('aPlan4');
        $aPlan5                             = $this->input->post('aPlan5');
        $aPlan6                             = $this->input->post('aPlan6');
        $aPlan7                             = $this->input->post('aPlan7');
        $aPlan8                             = $this->input->post('aPlan8');
        $address                            = $this->input->post('address');
        $address2                           = $this->input->post('address2');
        $asAt                               = $this->input->post('asAt');
        $billingCycle                       = $this->input->post('billingCycle');
        $businessAddress1                   = $this->input->post('businessAddress1');
        $businessAddress2                   = $this->input->post('businessAddress2');
        $businessPostcode                   = $this->input->post('businessPostcode');
        $businessState                      = $this->input->post('businessState');
        $businessSuburb                     = $this->input->post('businessSuburb');
        $comments                           = $this->input->post('comments');
        $conferenceInterestDate             = $this->input->post('conferenceInterestDate');
        $conferenceInterestUser             = $this->input->post('conferenceInterestUser');
        $consultancyHoursUsed               = $this->input->post('consultancyHoursUsed');
        $consultingRate                     = $this->input->post('consultingRate');
        $contactName                        = $this->input->post('contactName');
        $contactNumber                     	= $this->input->post('contactNumber');
        $contactNumber2                     = $this->input->post('contactNumber2');
        $contactNumber3                     = $this->input->post('contactNumber3');
        $dateCancelled                      = $this->input->post('dateCancelled');
        $dateRemoved                        = $this->input->post('dateRemoved');
        $dateUnsubscribed                   = $this->input->post('dateUnsubscribed');
        $emailAddress                       = $this->input->post('emailAddress');
        $fax                                = $this->input->post('fax');
        $frequencyOfAudits                  = $this->input->post('frequencyOfAudits');
        $howdidtheyhear                     = $this->input->post('howdidtheyhear');
        $howdidtheyhearother				= $this->input->post('howdidtheyhearother');
        $lastAuditDate                      = $this->input->post('lastAuditDate');
        $lastUpdatedDate                    = $this->input->post('lastUpdatedDate');
        $lastUpdatedUser                    = $this->input->post('lastUpdatedUser');
        $linkedmemberid                     = $this->input->post('linkedmemberid');
        $memberCategory                     = $this->input->post('memberCategory[]');
        $memberStatus                       = $this->input->post('memberStatus');
        $membershipCommencementDate         = $this->input->post('membershipCommencementDate');
        $membershipExpires                  = $this->input->post('membershipExpires');
        $membershipInvestment               = $this->input->post('membershipInvestment');
        $membershipType                     = $this->input->post('membershipType');
        $mobile                             = $this->input->post('mobile');
        $nextAuditDue                       = $this->input->post('nextAuditDue');
        $numberOfProperties                 = $this->input->post('numberOfProperties');
        $numberOfPropertiesCommercial       = $this->input->post('numberOfPropertiesCommercial');
        $numberOfPropertiesHoliday          = $this->input->post('numberOfPropertiesHoliday');
        $numberOfRegisteredSystemLicences   = $this->input->post('numberOfRegisteredSystemLicences');
        $onlineTrainingInterestDate         = $this->input->post('onlineTrainingInterestDate');
        $onlineTrainingInterestUser         = $this->input->post('onlineTrainingInterestUser');
        $password                           = $this->input->post('password');
        $postcode                           = $this->input->post('postcode');
        $ppmSystemInvestment                = $this->input->post('ppmSystemInvestment');
        $productInterestDate                = $this->input->post('productInterestDate');
        $productInterestUser                = $this->input->post('productInterestUser');
        $rei1                               = $this->input->post('rei1');
        $rei2                               = $this->input->post('rei2');
        $rei3                               = $this->input->post('rei3');
        $rei4                               = $this->input->post('rei4');
        $rei5                               = $this->input->post('rei5');
        $rei6                               = $this->input->post('rei6');
        $rei7                               = $this->input->post('rei7');
        $rei8                               = $this->input->post('rei8');
        $rei9                               = $this->input->post('rei9');
        $rei10                              = $this->input->post('rei10');
        $reasonCancelled 					= $this->input->post('reasonCancelled');
        $reasonCancelledOther				= $this->input->post('reasonCancelledOther');
        $removedReason                      = $this->input->post('removedReason');
        $removedReasonOther 				= $this->input->post('removedReasonOther');
        $serverDriver                       = $this->input->post('serverDriver');
        $staff                              = $this->input->post('staff[]');
        $state                              = $this->input->post('state');
        $stateSubRegion                     = $this->input->post('stateSubRegion');
        $suburb                             = $this->input->post('suburb');
        $systemCommencementDate             = $this->input->post('systemCommencementDate');
        $systemEnquiryDate                  = $this->input->post('systemEnquiryDate');
        $systemEnquiryUser                  = $this->input->post('systemEnquiryUser');
        $systemPresentationDate             = $this->input->post('systemPresentationDate');
        $systemPresentationUser             = $this->input->post('systemPresentationUser');
        $totalBilledHoursToDate             = $this->input->post('totalBilledHoursToDate');
        $totalConsultancyHoursPurchased     = $this->input->post('totalConsultancyHoursPurchased');
        $trainingInterestDate               = $this->input->post('trainingInterestDate');
        $trainingInterestUser               = $this->input->post('trainingInterestUser');
        $trustAccountProgram                = $this->input->post('trustAccountProgram');
        $trustAccountProgramOther           = $this->input->post('trustAccountProgramOther');
        $unsubscribeReason                  = $this->input->post('unsubscribeReason');
        $unsubscribeReasonOther             = $this->input->post('unsubscribeReasonOther');
        $username                           = $this->input->post('username');
        $website                            = $this->input->post('website');

        if(!empty($howdidtheyhearother)) {
        	$howdidtheyhear = $howdidtheyhearother;
        }

        if(!empty($reasonCancelledOther)) {
        	$reasonCancelled = $reasonCancelledOther;
        }

        if(!empty($removedReasonOther)) {
        	$removedReason = $removedReasonOther;
        }

        if(!empty($unsubscribeReasonOther)) {
        	$unsubscribeReason = $unsubscribeReasonOther;
        }

        $data = array(
            'AdditionalConsultinghoursbilled' 	=> floatval($AdditionalConsultinghoursbilled),
            'company' 							=> $company,
            'aPlan1' 							=> $this->ppmsystemlib->set_final_date_time($aPlan1),
            'aPlan2' 							=> $this->ppmsystemlib->set_final_date_time($aPlan2),
            'aPlan3' 							=> $this->ppmsystemlib->set_final_date_time($aPlan3),
            'aPlan3a' 							=> $this->ppmsystemlib->set_final_date_time($aPlan3a),
            'aPlan4' 							=> $this->ppmsystemlib->set_final_date_time($aPlan4),
            'aPlan5' 							=> $this->ppmsystemlib->set_final_date_time($aPlan5),
            'aPlan6' 							=> $this->ppmsystemlib->set_final_date_time($aPlan6),
            'aPlan7' 							=> $this->ppmsystemlib->set_final_date_time($aPlan7),
            'aPlan8' 							=> $this->ppmsystemlib->set_final_date_time($aPlan8),
            'address' 							=> $address,
            'address2' 							=> $address2,
            'asAt' 								=> $this->ppmsystemlib->set_final_date_time($asAt),
            'billingCycle' 						=> intval($billingCycle),
            'businessAddress1' 					=> $businessAddress1,
            'businessAddress2' 					=> $businessAddress2,
            'businessPostcode' 					=> $businessPostcode,
            'businessState'						=> intval($businessState),
            'businessSuburb' 					=> $businessSuburb,
            'comments' 							=> $comments,
            'conferenceInterestDate' 			=> $this->ppmsystemlib->set_final_date_time($conferenceInterestDate),
            'conferenceInterestUser' 			=> intval($conferenceInterestUser),
            'consultancyHoursUsed' 				=> floatval($consultancyHoursUsed),
            'consultingRate' 					=> $consultingRate,
            'contactName' 						=> $contactName,
            'contactNumber' 					=> $contactNumber,
            'contactNumber2' 					=> $contactNumber2,
            'contactNumber3' 					=> $contactNumber3,
            'dateCancelled' 					=> $this->ppmsystemlib->set_final_date_time($dateCancelled),
            'dateRemoved' 						=> $this->ppmsystemlib->set_final_date_time($dateRemoved),
            'dateUnsubscribed' 					=> $this->ppmsystemlib->set_final_date_time($dateUnsubscribed),
            'emailAddress' 						=> $emailAddress,
            'fax' 								=> $fax,
            'frequencyOfAudits' 				=> intval($frequencyOfAudits),
            'howdidtheyhear' 					=> $howdidtheyhear,
            'lastAuditDate' 					=> $this->ppmsystemlib->set_final_date_time($lastAuditDate),
            'lastUpdatedDate' 					=> $this->ppmsystemlib->set_final_date_time($lastUpdatedDate),
            'lastUpdatedUser' 					=> intval($lastUpdatedUser),
            'linkedmemberid' 					=> intval($linkedmemberid),
            'memberStatus' 						=> intval($memberStatus),
            'membershipCommencementDate' 		=> $this->ppmsystemlib->set_final_date_time($membershipCommencementDate),
            'membershipExpires' 				=> $this->ppmsystemlib->set_final_date_time($membershipExpires),
            'membershipInvestment' 				=> $membershipInvestment,
            'membershipType' 					=> intval($membershipType),
            'mobile' 							=> $mobile,
            'nextAuditDue' 						=> $this->ppmsystemlib->set_final_date_time($nextAuditDue),
            'numberOfProperties' 				=> intval($numberOfProperties),
            'numberOfPropertiesCommercial' 		=> intval($numberOfPropertiesCommercial),
            'numberOfPropertiesHoliday' 		=> intval($numberOfPropertiesHoliday),
            'numberOfRegisteredSystemLicences' 	=> intval($numberOfRegisteredSystemLicences),
            'onlineTrainingInterestDate' 		=> $this->ppmsystemlib->set_final_date_time($onlineTrainingInterestDate),
            'onlineTrainingInterestUser' 		=> intval($onlineTrainingInterestUser),
            'password' 							=> $password,
            'postcode' 							=> $postcode,
            'ppmSystemInvestment' 				=> $ppmSystemInvestment,
            'productInterestDate' 				=> $this->ppmsystemlib->set_final_date_time($productInterestDate),
            'productInterestUser' 				=> intval($productInterestUser),
            'rei1' 								=> $this->ppmsystemlib->set_final_date_time($rei1),
            'rei2' 								=> $this->ppmsystemlib->set_final_date_time($rei2),
            'rei3' 								=> $this->ppmsystemlib->set_final_date_time($rei3),
            'rei4' 								=> $this->ppmsystemlib->set_final_date_time($rei4),
            'rei5' 								=> $this->ppmsystemlib->set_final_date_time($rei5),
            'rei6' 								=> $this->ppmsystemlib->set_final_date_time($rei6),
            'rei7' 								=> $this->ppmsystemlib->set_final_date_time($rei7),
            'rei8' 								=> $this->ppmsystemlib->set_final_date_time($rei8),
            'rei9' 								=> $this->ppmsystemlib->set_final_date_time($rei9),
            'rei10' 							=> $this->ppmsystemlib->set_final_date_time($rei10),
            'reasonCancelled'					=> $reasonCancelled,
            'removedReason' 					=> $removedReason,
            'serverDriver' 						=> $serverDriver,
            'state'								=> intval($state),
            'stateSubRegion'					=> intval($stateSubRegion),
            'suburb' 							=> $suburb,
            'systemCommencementDate' 			=> $this->ppmsystemlib->set_final_date_time($systemCommencementDate),
            'systemEnquiryDate' 				=> $this->ppmsystemlib->set_final_date_time($systemEnquiryDate),
            'systemEnquiryUser' 				=> intval($systemEnquiryUser),
            'systemPresentationDate' 			=> $this->ppmsystemlib->set_final_date_time($systemPresentationDate),
            'systemPresentationUser' 			=> intval($systemPresentationUser),
            'totalBilledHoursToDate' 			=> floatval($totalBilledHoursToDate),
            'totalConsultancyHoursPurchased' 	=> floatval($totalConsultancyHoursPurchased),
            'trainingInterestDate' 				=> $this->ppmsystemlib->set_final_date_time($trainingInterestDate),
            'trainingInterestUser' 				=> intval($trainingInterestUser),
            'trustAccountProgram' 				=> intval($trustAccountProgram),
            'trustAccountProgramOther' 			=> $trustAccountProgramOther,
            'unsubscribeReason' 				=> $unsubscribeReason,
            'username' 							=> $username,
            'website' 							=> $website
        );

		foreach ($data as $key => $value) {
			if(empty($value)) {
				if(!is_numeric($value))$data[$key] = NULL;
			}
		}

		if(is_array($staff) && count($staff)>0) {
            $staff = array_chunk($staff,6);

            $ctr = 0;
            foreach ($staff as $value) {
            	if(empty($value[0]) && empty($value[1]) && empty($value[2]) && empty($value[3]) && empty($value[4]) && empty($value[5])) {
	            	unset($staff[$ctr]);
	            }

	            $ctr++;
            }
        }

		$id = $this->members->add_update($id, $data, $memberCategory, $staff);

		echo json_encode(array('error'=>array(),'id'=>$id));

	}

	public function reset_newsletter_download()
	{
		$post = $this->input->post();

		if(!empty($post)) {
			$memberId 	= $this->input->post('id');
			$eNewsId 	= $this->input->post('eNewsId');

			if($this->members->reset_newsletter_downloads($memberId, $eNewsId)) {
				echo json_encode(array('success'=>TRUE));
			}
			else {
				echo json_encode(array('success'=>FALSE));
			}
		}
	}

	public function validate_required () {
		$post = $this->input->post();

		if(!empty($post)) {
			/*
			[always required]

			membershipType
			company
			contactNumber
			state
			howdidtheyhear 		if value 'Other' require howdidtheyhearother
			removedReason    	if value 'Other' require removedReasonOther
			unsubscribeReason 	if value 'Other' require unsubscribeReasonOther
			reasonCancelled 	if value 'Other - Text' require reasonCancelledOther
			state 				if value 'Queensland' require stateSubRegion
			
			[required by type]

			Member - (membershipType:0,1,2)
			Newsletter Subscriber - (membershipType:4)
			On-Line Training Subscriber - (membershipType:5)

			contactName  - except 4,5
			emailAddress - except 4,5
			username
			password
			memberStatus
			membershipCommencementDate
			membershipExpires - only 4,5
			*/

			$this->memberId 			= $this->input->post('id');

			$membershipType 			= intval($this->input->post('membershipType'));
			$state 						= intval($this->input->post('state'));
			$howdidtheyhear 			= $this->input->post('howdidtheyhear');
			$removedReason 				= $this->input->post('removedReason');
			$unsubscribeReason 			= $this->input->post('unsubscribeReason');
			$reasonCancelled 			= $this->input->post('reasonCancelled');

			$this->form_validation->set_rules('membershipType', '"Client Type"', 'required');
			$this->form_validation->set_rules('company', '"Company"', 'required');
			$this->form_validation->set_rules('contactNumber', '"Contact Number"', 'required|is_numeric');
			$this->form_validation->set_rules('state', '"State"', 'required');
			if(intval($state) === 1) $this->form_validation->set_rules('stateSubRegion', '"State SubRegion"', 'required', array('required'=>'You must choose a sub-region for Queensland.'));
			if($howdidtheyhear === 'Other') $this->form_validation->set_rules('howdidtheyhearother', '"How Did They Hear About Us - Other Reason"', 'required');
			if($removedReason === 'Other') $this->form_validation->set_rules('removedReasonOther', '"Removed - Other Reason"', 'required');
			if($unsubscribeReason === 'Other') $this->form_validation->set_rules('unsubscribeReasonOther', '"Unsubscribed - Other Reason"', 'required');
			if($reasonCancelled === 'Other - Text') $this->form_validation->set_rules('reasonCancelledOther', '"Reason Cancelled - Other Reason"', 'required');

			if(is_numeric($membershipType)) {
				$membershipType = intval($membershipType);
				switch ($membershipType) {
					case 0:
					case 1:
					case 2:
					case 4:
					case 5:
						if($membershipType !== 4 && $membershipType !== 5) {
							$this->form_validation->set_rules('contactName', '"General Accounts Contact"', 'required');
							$this->form_validation->set_rules('emailAddress', '"General Accounts Email"', 'required|valid_email');
							$this->form_validation->set_rules('membershipExpires', '"Expires"', 'required');
						}
						else {
							$this->form_validation->set_rules('membershipExpires', '"Expires"', 'required');
						}
						
						if(!empty($this->memberId)) {
							$this->form_validation->set_rules('username', '"Username"', 'required|alpha_numeric');
						}
						else {
							$this->form_validation->set_rules('username', '"Username"', 'required|alpha_numeric|is_unique[members.username]');
						}
						
						$this->form_validation->set_rules('password', '"Password"', 'required');
						$this->form_validation->set_rules('memberStatus', '"Client Status"', 'required');
						$this->form_validation->set_rules('membershipCommencementDate', '"Commencement Date"', 'required');
						break;
					
					default:
						break;
				}
			}

            if ($this->form_validation->run() == FALSE){
            	//return error;
            	return array("error"=>$this->form_validation->error_array());
            }
            else {
            	$result = $this->validate_fields();

            	if(is_array($result)) {
            		return $result;
            	}
            	else {
            		return TRUE;
            	}
            }
		}
	}

	public function validate_fields()
	{
		$post = $this->input->post();

		if(!empty($post)) {

			/*
			[exempted fields]

			membershipType
			company
			contactNumber
			state
			howdidtheyhear
			removedReason
			unsubscribeReason
			reasonCancelled
			contactName
			emailAddress
			username
			password
			memberStatus
			membershipCommencementDate
			membershipExpires
			*/

			$membershipType 	= intval($this->input->post('membershipType'));
			$memberStatus   	= intval($this->input->post('memberStatus'));
			$dateUnsubscribed 	= $this->input->post('dateUnsubscribed');

	        $this->form_validation->set_rules('AdditionalConsultinghoursbilled', '"Additional Consulting Hours Billed"', 'is_numeric');
	        $this->form_validation->set_rules('aPlan1', '"Action Plan 1: Pre-system Requirements"', 'callback_is_date', array('is_date'=>'Invalid Date.'));
	        $this->form_validation->set_rules('aPlan2', '"Action Plan 2: Principal Discussions"', 'callback_is_date', array('is_date'=>'Invalid Date.'));
	        $this->form_validation->set_rules('aPlan3', '"Action Plan 3: Install"', 'callback_is_date', array('is_date'=>'Invalid Date.'));
	        $this->form_validation->set_rules('aPlan3a', '"Action Plan 3a: Customised Forms & Office Layout"', 'callback_is_date', array('is_date'=>'Invalid Date.'));
	        $this->form_validation->set_rules('aPlan4', '"Action Plan 4: Customise Checklists & Training"', 'callback_is_date', array('is_date'=>'Invalid Date.'));
	        $this->form_validation->set_rules('aPlan5', '"Action Plan 5: Department Audit"', 'callback_is_date', array('is_date'=>'Invalid Date.'));
	        $this->form_validation->set_rules('aPlan6', '"Action Plan 6: Human Resource Management"', 'callback_is_date', array('is_date'=>'Invalid Date.'));
	        $this->form_validation->set_rules('aPlan7', '"Action Plan 7: Marketing & Customer Service"', 'callback_is_date', array('is_date'=>'Invalid Date.'));
	        $this->form_validation->set_rules('aPlan8', '"Action Plan 8: KPI Report & Bottom-Line Report"', 'callback_is_date', array('is_date'=>'Invalid Date.'));
	        $this->form_validation->set_rules('asAt', '"As At"', 'callback_is_date', array('is_date'=>'Invalid Date.'));
			$this->form_validation->set_rules('conferenceInterestDate', '"Conference Date"', 'callback_is_date', array('is_date'=>'Invalid Date.'));
			$this->form_validation->set_rules('consultancyHoursUsed', '"Consultancy Hours Used"', 'is_numeric');
	        $this->form_validation->set_rules('consultingRate', '"Consulting Rate"', 'is_numeric');
	        $this->form_validation->set_rules('dateCancelled', '"Cancelled Date"', 'callback_is_date', array('is_date'=>'Invalid Date.'));
	        $this->form_validation->set_rules('dateRemoved', '"Date Removed"', 'callback_is_date', array('is_date'=>'Invalid Date.'));
	        $this->form_validation->set_rules('dateUnsubscribed', '"Date Unsubscribed"', 'callback_is_date', array('is_date'=>'Invalid Date.'));
	        $this->form_validation->set_rules('emailAddress', '"General Accounts Email"', 'valid_email');
	        $this->form_validation->set_rules('lastAuditDate', '"Last Audit Date"', 'callback_is_date', array('is_date'=>'Invalid Date.'));
	        $this->form_validation->set_rules('lastUpdatedDate', '"Last Date Contact Details Updated"', 'callback_is_date', array('is_date'=>'Invalid Date.'));
	        $this->form_validation->set_rules('linkedmemberid', '"Linked Member ID"', 'is_numeric');
	        $this->form_validation->set_rules('membershipExpires', '"Expires"', 'callback_is_date', array('is_date'=>'Invalid Date.'));
	        $this->form_validation->set_rules('membershipInvestment', '"Membership Investment"', 'is_numeric');
	        $this->form_validation->set_rules('numberOfProperties', '"Number of Residential Properties"', 'is_numeric');
	        $this->form_validation->set_rules('numberOfPropertiesCommercial', '"Number of Commercial Properties"', 'is_numeric');
	        $this->form_validation->set_rules('numberOfPropertiesHoliday', '"Number of Holiday Properties"', 'is_numeric');
	        $this->form_validation->set_rules('numberOfRegisteredSystemLicences', '"Num of Registered System Licences"', 'is_numeric');
	        $this->form_validation->set_rules('onlineTrainingInterestDate', '"Online Training"', 'callback_is_date', array('is_date'=>'Invalid Date.'));
	        $this->form_validation->set_rules('ppmSystemInvestment', '"PPMsystem Investment"', 'is_numeric');
	        $this->form_validation->set_rules('productInterestDate', '"Product Interest Date"', 'callback_is_date', array('is_date'=>'Invalid Date.'));
			$this->form_validation->set_rules('rei1', '"REI Checklist : Installation"', 'callback_is_date', array('is_date'=>'Invalid Date.'));
			$this->form_validation->set_rules('rei2', '"REI Lesson Plan 1: Data Entry"', 'callback_is_date', array('is_date'=>'Invalid Date.'));
			$this->form_validation->set_rules('rei3', '"REI Lesson Plan 1: Data Conversion"', 'callback_is_date', array('is_date'=>'Invalid Date.'));
			$this->form_validation->set_rules('rei4', '"REI Lesson Plan 2: Daily Applications"', 'callback_is_date', array('is_date'=>'Invalid Date.'));
			$this->form_validation->set_rules('rei5', '"REI Lesson Plan 3: Interim Disbursements"', 'callback_is_date', array('is_date'=>'Invalid Date.'));
			$this->form_validation->set_rules('rei6', '"REI Lesson Plan 4: Advanced Features"', 'callback_is_date', array('is_date'=>'Invalid Date.'));
			$this->form_validation->set_rules('rei7', '"REI Lesson Plan 5: Pre End of Month"', 'callback_is_date', array('is_date'=>'Invalid Date.'));
			$this->form_validation->set_rules('rei8', '"REI Lesson Plan 6: End of Month"', 'callback_is_date', array('is_date'=>'Invalid Date.'));
			$this->form_validation->set_rules('rei9', '"REI Lesson Plan 7: Sales Trust"', 'callback_is_date', array('is_date'=>'Invalid Date.'));
			$this->form_validation->set_rules('rei10', '"REI Lesson Plan 8: Commercial"', 'callback_is_date', array('is_date'=>'Invalid Date.'));

			$this->form_validation->set_rules('systemCommencementDate', '"System Implementation Date"', 'callback_is_date', array('is_date'=>'Invalid Date.'));
	        $this->form_validation->set_rules('systemEnquiryDate', '"System Enquiry"', 'callback_is_date', array('is_date'=>'Invalid Date.'));
			$this->form_validation->set_rules('systemPresentationDate', '"System Presentation"', 'callback_is_date', array('is_date'=>'Invalid Date.'));
			$this->form_validation->set_rules('totalBilledHoursToDate', '"Total Billed Hours to Date"', 'is_numeric');
			$this->form_validation->set_rules('totalConsultancyHoursPurchased', '"Total Consultancy Hours Purchased"', 'is_numeric');
			$this->form_validation->set_rules('trainingInterestDate', '"Training Interest Date"', 'callback_is_date', array('is_date'=>'Invalid Date.'));

			if(is_numeric($membershipType)) {
				if(($membershipType === 4 OR $membershipType === 5) && !empty($dateUnsubscribed)) {
					$this->form_validation->set_rules('memberStatus', 'Member Status', 'callback_check_status_for_ol');
				}
			}

			$staff = $this->input->post('staff[]');

			if( ! empty($staff) && is_array($staff)) {
				
	            $staff = array_chunk($staff,6);

	            foreach ($staff as $value) {
	            	// memberStatus = 1 and membershipType = 5
	            	if(empty($value[0]) && $memberStatus === 1 && $membershipType === 5) {
	            		$this->form_validation->set_rules('staff[]', 'Staff', 'callback_check_staff', array('check_staff'=>'Staff First Name is required.'));
	            	}
	            	elseif(empty($value[1]) && $memberStatus === 1 && $membershipType === 5) {
	            		$this->form_validation->set_rules('staff[]', 'Staff', 'callback_check_staff', array('check_staff'=>'Staff Last Name is required.'));
	            	}
	            	elseif($memberStatus === 1 && $membershipType === 5) {

	            		$msg = (empty($value[2])) ? 'Staff Email Address is required.' : '';
	            		$msg = (empty($msg) && !valid_email($value[2])) ? 'Staff Email Address is invalid.' : '';

	            		if(!empty($msg)) {
	            			$this->form_validation->set_rules('staff[]', 'Staff', 'callback_check_staff', array('check_staff'=>$msg));	
	            		}
	            		else {
	            			if(empty($value[2])) {
								$this->form_validation->set_rules('staff[]', 'Staff', 'callback_check_staff', array('check_staff'=>'Staff Email Address is required.'));
							}
	            		}
	            		
	            	}
	            	if(!is_numeric($value[5]) && (!empty($value[0]) OR !empty($value[1]) OR !empty($value[2]) OR !empty($value[5]))) {
	            		$this->form_validation->set_rules('staff[]', 'Staff', 'callback_check_staff', array('check_staff'=>'Staff Position is required.'));
	            	}
	            }
	        }

			if ($this->form_validation->run() == FALSE){
	            	//return error;
	            return array("error"=>$this->form_validation->error_array());
				//echo json_encode(array("error"=>$this->form_validation->error_array()));
	        }
	        else {
	        	return TRUE;
	        }
        }
	}

	function check_staff($arr)
	{
		return FALSE;
	}

	function check_status_for_ol($status) 
	{
		if (intval($status) !== 3){
	        $this->form_validation->set_message('check_status_for_ol', 'You have entered a date for Date Unsubscribed.  You must change the Status to Cancelled/Unsubscribed');
	        return FALSE;
		}
		else{
			return TRUE;
		}
	}

	function is_date ($dateTimeStr) 
	{
		if(empty($dateTimeStr)) return TRUE;

		$dateTime = explode(' ', $dateTimeStr);

		$au_date = explode('/', $dateTime[0]);

		if(count($au_date) === 3) {
			$day 	= $au_date[0];
			$month 	= $au_date[1];
			$year 	= $au_date[2];

			if(is_numeric($day) && is_numeric($month) && is_numeric($year)) {
				return checkdate(intval($month), intval($day), intval($year));
			}
			else {
				return FALSE;
			}
		}
		else {
			return FALSE;
		}
	}
	
	/*** Form Submit ***/	

	/** Monthly Award Stats **/

	function get_month_stats_details()
	{
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$post = $this->input->post();

		if(!empty($post)) {

			$statid 	= $this->input->post('id');
			$memberId 	= $this->input->post('memberId');
			$action 	= $this->input->post('action');

			/*
			comissionIncrease
			dateSubmitted
			forMonth
			individualAchievement
			memberId
			numberOfPropertiesGained
			numberOfPropertiesLost
			propertiesGainedDollar
			propertiesLostDollar
			rentArrears
			statId
			vacancyRate
			*/

			if($action === 'edit') {
				$result = $this->members->get_monthly_stats_details($statid);

				$arr = array();

				foreach ($result as $key => $value) {
					$arr['comissionIncrease'] 			= round(doubleval($value['comissionIncrease']), 2);
					$arr['forMonth'] 					= $this->ppmsystemlib->check_date_time($value['forMonth'], 'F Y');
					$arr['individualAchievement'] 		= $value['individualAchievement'];
					$arr['memberId'] 					= intval($value['memberId']);
					$arr['statId']						= intval($value['statId']);
					$arr['memberName']					= $value['memberName'];
					$arr['numberOfPropertiesGained'] 	= round(doubleval($value['numberOfPropertiesGained']), 2);
					$arr['numberOfPropertiesLost'] 		= round(doubleval($value['numberOfPropertiesLost']), 2);
					$arr['propertiesGainedDollar'] 		= round(doubleval($value['propertiesGainedDollar']), 2);
					$arr['propertiesLostDollar'] 		= round(doubleval($value['propertiesLostDollar']), 2);
					$arr['rentArrears'] 				= round(doubleval($value['rentArrears']), 2);
					$arr['vacancyRate'] 				= round(doubleval($value['vacancyRate']), 2);			
				}

				echo json_encode($arr);
			}
			elseif($action === 'delete') {
				if(is_numeric($statid) && is_numeric($memberId)) {
					$this->members->delete_monthly_stats($statid, $memberId);

					echo json_encode(array('success'=>1));
				}
				else {
					echo json_encode(array('error'=>1));
				}
			}
		}
	}

	public function mas_add_update()
	{
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$post = $this->input->post();

		/*
		comissionIncrease
		dateSubmitted
		forMonth
		individualAchievement
		memberId
		numberOfPropertiesGained
		numberOfPropertiesLost
		propertiesGainedDollar
		propertiesLostDollar
		rentArrears
		statId
		vacancyRate
		*/

		if(!empty($post)) {
			$comissionIncrease 			= $this->input->post('comissionIncrease');
			$forMonth 					= $this->input->post('forMonth');
			$individualAchievement 		= $this->input->post('individualAchievement');
			$memberId 					= $this->input->post('memberId');
			$numberOfPropertiesGained 	= $this->input->post('numberOfPropertiesGained');
			$numberOfPropertiesLost 	= $this->input->post('numberOfPropertiesLost');
			$propertiesGainedDollar 	= $this->input->post('propertiesGainedDollar');
			$propertiesLostDollar 		= $this->input->post('propertiesLostDollar');
			$rentArrears 				= $this->input->post('rentArrears');
			$statId						= $this->input->post('statId');
			$vacancyRate 				= $this->input->post('vacancyRate');

			$valid = TRUE;
			$error = NULL;
			
			$this->form_validation->set_rules('memberId', '"Member ID"', 'required|is_numeric');
			$this->form_validation->set_rules('vacancyRate', '"Vacancy Rate"', 'required|is_numeric');
			$this->form_validation->set_rules('rentArrears', '"Rent Arrears"', 'required|is_numeric');
			$this->form_validation->set_rules('comissionIncrease', '"Commision Increase"', 'required|is_numeric');
			$this->form_validation->set_rules('propertiesGainedDollar', '"Dollar value for properties gain."', 'required|is_numeric');
			$this->form_validation->set_rules('propertiesLostDollar', '"Dollar value for properties lost."', 'required|is_numeric');
			$this->form_validation->set_rules('numberOfPropertiesGained', '"Number of properties gained."', 'required|is_numeric');
			$this->form_validation->set_rules('numberOfPropertiesLost', '"Number of properties lost."', 'required|is_numeric');

			if ($this->form_validation->run() == FALSE){
	            $result = array("valid"=>FALSE, "error"=>$this->form_validation->error_array());
	        }
	        else {
	        	$result = array("valid"=>TRUE);
	        }

			if(!$result['valid']) {
				$valid = $result['valid'];
				$error = $result['error'];
			}

			if($valid) {
				//date('Y-m-d H:i:s', now());
				$data = array();

				$data['comissionIncrease'] 			= $comissionIncrease;
				$data['dateSubmitted'] 				= date('Y-m-d H:i:s', now());
				$data['forMonth'] 					= date('Y-m-d H:i:s', strtotime($forMonth));
				$data['individualAchievement'] 		= $individualAchievement;
				$data['memberId'] 					= $memberId;
				$data['numberOfPropertiesGained'] 	= $numberOfPropertiesGained;
				$data['numberOfPropertiesLost'] 	= $numberOfPropertiesLost;
				$data['propertiesGainedDollar'] 	= $propertiesGainedDollar;
				$data['propertiesLostDollar'] 		= $propertiesLostDollar;
				$data['rentArrears'] 				= $rentArrears;
				$data['vacancyRate'] 				= $vacancyRate;

				$arr 				= array();

				$arr['forMonth']	= $forMonth;

				if(is_numeric($statId)) { //Edit
					$arr['statId'] 	= $this->members->add_update_monthly_stats($statId, $data);
				}
				else { //Add

					$id = $this->members->add_update_monthly_stats(NULL, $data);

					if(!empty($id)) {
						$arr['statId'] 	= $id;
						$arr['edit']	= '<button type="button" data-id="' . $id . '" data-action="edit" class="action-btn btn btn-xs btn-warning"><i class="fa fa-pencil"></i></button>';
						$arr['delete']  = '<button type="button" data-id="' . $id . '" data-action="delete" class="action-btn btn btn-xs btn-danger"><i class="fa fa-close"></i></button>';
					}
				}

				echo json_encode($arr);
			}
			else {
				echo json_encode(array("error"=>$error));
				return FALSE;
			}
		}
	}

	/** Monthly Award Stats **/

}


