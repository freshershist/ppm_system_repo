<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('PPMSystemLib');

		$this->load->model('admin/dashboardmodel', 'dashboard');
		$this->load->model('admin/newsmodel', 'news');

		$this->action = '';

		if(!$this->dashboard->is_logged_in()) {
			redirect(base_url() . 'admin/login');
		}
		else {
			if(!$this->dashboard->check_user_access(11)) {
				redirect(base_url() . 'admin/home');
			}
		}
	}

	public function index()
	{

		$this->action = $this->uri->segment(3, 'list');

		$data['active_page'] 	= "eNews | Newsletter";
		$data['page_title'] 	= "eNews | Newsletter";
		$data['action']			= $this->action;

		$eNewsCategoryArray	= array('all'=>'All','1'=>"Newsletter",'2'=>"eNews");

		$filters['eNewsCategoryArray'] = $eNewsCategoryArray;
		$data['filters'] 	= $this->load->view('admin/news_filters', $filters, TRUE);

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/dropzone/dist/min/dropzone.min.css" rel="stylesheet">';

		$this->load->view('admin/common/header', $data);
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/common/topnav');

		$this->load->helper('form');

		$add_edit['eNewsCategoryArray'] = array(''=>'[ Select ]','1'=>"Newsletter",'2'=>"eNews");

		$data['add_edit'] = $this->load->view('admin/news_add_edit', $add_edit, TRUE);

		$gallery_data = $this->ppmsystemlib->get_files();

		$gallery_data['directory'] 	= directory_map(UPLOADFOLDER . '/files');
		$gallery_data['acceptedFiles'] = $this->ppmsystemlib->get_accepted_mimes();
		$data['upload'] = $this->load->view('admin/common/dz_modal', $gallery_data, TRUE);

		$this->load->view('admin/news', $data);

		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/moment/min/moment.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>';

		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net/js/jquery.dataTables.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.buttons.js"></script>';		
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/dropzone/dist/min/dropzone.min.js"></script>';

		$this->load->view('admin/common/footer', $data);	
	}

	public function get_list() 
	{
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$post = $this->input->post();

		if(count($post)>0){

			$category = $this->input->post('category');
			$result = array();

			if(is_numeric($category)) {
				$result = $this->news->get_news($category);
			}
			else {
				$result = $this->news->get_news();
			}

			$result_arr = array();

			/*
			id
			name
			category
			mydate
			*/

			foreach ($result as $key => $value) {
				$arr = array();
				
				$arr['id'] 			= $value['id'];
				$arr['name'] 		= (strlen($value['name']) <= 30) ? $value['name'] : '<span class="text-word-wrap">'.$value['name'].'</span>';
				$arr['category']	= (intval($value['category']) == 1) ? 'Newsletter' : 'eNews';
				$arr['mydate']		= '<font class="hide">'. $this->ppmsystemlib->get_unix_from_date($value['mydate']) . '</font> ' . $this->ppmsystemlib->check_date_time($value['mydate']);
				
				$arr['action']		= '<ul class="nav nav-pills" role="tablist"> <li role="presentation" class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">Action<span class="caret"></span></a><ul class="dropdown-menu animated fadeInDown" role="menu" aria-labelledby="drop6" style="left:-108%!important;"><li role="presentation"><a tabindex="-1" href="javascript:;">ID: <label class="action-id"></label></a></li><li role="presentation" class="divider"></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="edit">Edit</a></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="delete">Delete</a></li></ul></li></ul>';

				$result_arr[] = $arr;
			}		

			echo json_encode($result_arr);
		}
		else {
			echo json_encode(array());
		}		
	}

	public function process_action()
	{
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$post = $this->input->post();

		if(!empty($post)) {
			$id = $this->input->post('id');
			$action = $this->input->post('action');

			if($action === 'edit') {

				if(is_numeric($id)) {
					$result = $this->news->get_enews($id);

					//echo json_encode($result); die();
					/*
					id
					name
					category
					mydate
					body
					d1
					d2
					*/			

					$data = array();

					foreach ($result as $key => $value) {
						$data['name'] 		= $value['name'];
						$data['category'] 	= $value['category'];
						$data['mydate'] 	= $this->ppmsystemlib->check_date_time($value['mydate']);
						$data['body'] 		= $value['body'];
						$data['d1'] 		= $value['d1'];
						$data['d2']			= $value['d2'];
					}

					echo json_encode($data);
				}
			}
			elseif($action === 'delete') {
				if(is_numeric($id)) {
					$this->news->delete_enews($id);

					echo json_encode(array('success'=>1));
				}
				else {
					echo json_encode(array('error'=>1));
				}
			}
		}
		else {
			exit('No direct script access allowed');
		}
	}	

	public function add_update()
	{
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$post = $this->input->post();

		/*
		id
		name
		category
		mydate
		body
		d1
		d2
		*/

		if(!empty($post)) {
			$id 		= $this->input->post('id');
			$name 		= $this->input->post('name');
			$category 	= $this->input->post('category'); 
			$mydate 	= $this->input->post('mydate');
			$body 		= $this->input->post('body');
			$d1 		= $this->input->post('d1');
			$d2 		= $this->input->post('d2');

			$valid = TRUE;
			$error = NULL;


			$result = $this->validate_fields($id);

			if(!$result['valid']) {
				$valid = $result['valid'];
				$error = $result['error'];
			}

			if($valid) {

				$data = array();

				$data['name'] 			= $name;
				$data['category'] 		= $category;
				$data['mydate'] 		= $this->ppmsystemlib->set_final_date_time($mydate);

				$data['body'] 			= $body;

				$data['d1'] 			= $d1;
				$data['d2'] 			= $d2;

				$arr = array();

				$arr['name'] 		= $name;
				$arr['category']	= (intval($category) === 1) ? 'Newsletter' : 'eNews';
				$arr['mydate'] 		= '<font class="hide">'. $this->ppmsystemlib->get_unix_from_date($this->ppmsystemlib->set_final_date_time($mydate)) . '</font> ' . $mydate;

				if(is_numeric($id)) { //Edit
					$arr['id'] = $this->news->add_update_enews($id, $data);
				}
				else { //Add

					$id = $this->news->add_update_enews(NULL, $data);

					$arr['id'] = $id;
					$arr['action'] = '<ul class="nav nav-pills" role="tablist"> <li role="presentation" class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">Action<span class="caret"></span></a><ul class="dropdown-menu animated fadeInDown" role="menu" aria-labelledby="drop6" style="left:-108%!important;"><li role="presentation"><a tabindex="-1" href="javascript:;">ID: <label class="action-id"></label></a></li><li role="presentation" class="divider"></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="edit">Edit</a></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="delete">Delete</a></li></ul></li></ul>';
				}

				echo json_encode($arr);
			}
			else {
				echo json_encode(array("error"=>$error));
				return FALSE;
			}
		}
	}

	function validate_fields($id = NULL)
	{
		$this->load->library('form_validation');
		
		if(empty($id)) {
			$this->form_validation->set_rules('name', '"Year/Month"', 'required|is_unique[eNews.name]');
		}
		else {
			$this->form_validation->set_rules('name', '"Year/Month"', 'required');
		}
		
		$this->form_validation->set_rules('category', '"Category"', 'required');
		$this->form_validation->set_rules('mydate', '"Date Posted"', 'required');

		if ($this->form_validation->run() == FALSE){
            return array("valid"=>FALSE, "error"=>$this->form_validation->error_array());
        }
        else {
        	return array("valid"=>TRUE);
        }
	}	
}
