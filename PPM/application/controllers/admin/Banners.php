<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banners extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('file');
		$this->load->helper('directory');
		$this->load->helper('form');

		$this->load->helper('ppmsystem');
		$this->load->library('PPMSystemLib');
		

		$this->load->model('admin/dashboardmodel', 'dashboard');
		$this->load->model('admin/bannersmodel', 'banners');
		$this->load->model('admin/pagesmodel', 'pages');

		$this->action = '';
		$this->memberId = 0;
		$this->page_id = 0;

		if(!$this->dashboard->is_logged_in()) {
			redirect(base_url() . 'admin/login');
		}
		else {
			if(!$this->dashboard->check_user_access(8)) {
				redirect(base_url() . 'admin/home');
			}
		}

	}
	public function index(){


		// redirect(base_url() . 'admin/pages/list');
		echo '<pre>';

		print_r($this->ppmsystemlib->get_accepted_mimes());

		die();
	}

	public function show_list(){
		$this->action = $this->uri->segment(3, 'list');
		//$this->dashboard->get_moderators('all');
		//echo $this->action;
		//echo '<pre>';
		//print_r();
		//die();

		// $filters = $this->get_filters();

		// if($this->action === 'add'){
		// 	if(!empty($this->uri->segment(4, 0))) redirect(base_url() . 'admin/members/add');
		// }
		// elseif($this->action === 'view'){
		//
		// 	$this->memberId = $this->uri->segment(4, 0);
		//
		// 	if(!empty($this->memberId) && is_numeric($this->memberId)){
		// 		$filters['member_data'] = $this->members->get_entry($this->memberId);//12651,877,16076
		// 	}
		// 	else{
		// 		redirect(base_url() . 'admin/members');
		// 	}
		//
		// }

		$data['active_page'] 	= "Banners";

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/switchery/dist/switchery.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/iCheck/skins/flat/green.css" rel="stylesheet">';

		$this->load->view('admin/common/header', $data);
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/common/topnav');

		$this->load->helper('form');

		// $data['filters'] 	= $this->load->view('admin/members_filters', $filters, TRUE);
		// $data['add_edit'] 	= $this->load->view('admin/members_add_edit', $filters, TRUE);
		$data['action'] 	= $this->action;
		$data['list_array'] = $this->banners->get_banners();


		$this->load->view('admin/banners', $data);

		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/switchery/dist/switchery.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/moment/min/moment.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net/js/jquery.dataTables.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/iCheck/icheck.min.js"></script>';



		$this->load->view('admin/common/footer', $data);
	}

	public function add_list(){
		$this->action = $this->uri->segment(3, 'add');

		$data['active_page'] 	= "Add New Banner";

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/switchery/dist/switchery.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/iCheck/skins/flat/green.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/dropzone/dist/min/dropzone.min.css" rel="stylesheet">';

		$this->load->view('admin/common/header', $data);
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/common/topnav');

		$this->load->helper('form');

		$data['action'] 	= $this->action;
		// $data['list_array'] = $this->banners->get_banners();

		$gallery_data = $this->ppmsystemlib->get_files();

		$gallery_data['directory'] = directory_map(UPLOADFOLDER . '/files');
		$gallery_data['acceptedFiles'] = $this->ppmsystemlib->get_accepted_mimes();
		$data['upload'] = $this->load->view('admin/common/dz_modal', $gallery_data, TRUE);
		$d['editor_id'] = 'editor_1';

		$data['editor'] = $this->load->view('admin/common/editor', $d, TRUE);

		$data['pages'] = $this->pages->get_pages();

		$this->load->view('admin/banners_add_edit', $data);

		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/switchery/dist/switchery.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/moment/min/moment.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net/js/jquery.dataTables.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/iCheck/icheck.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/dropzone/dist/min/dropzone.min.js"></script>';

		$this->load->view('admin/common/footer', $data);

	}

	public function edit_list(){
		$this->action = $this->uri->segment(3, 'edit');
		$this->page_id = $this->uri->segment(4); // Banner ID

		$data['active_page'] 	= "Edit Banner";

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/switchery/dist/switchery.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/iCheck/skins/flat/green.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/dropzone/dist/min/dropzone.min.css" rel="stylesheet">';

		$this->load->view('admin/common/header', $data);
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/common/topnav');

		$this->load->helper('form');

		$data['action'] 	= $this->action;
		$data['page_id'] = $this->page_id;

		$gallery_data = $this->ppmsystemlib->get_files();

		$gallery_data['directory'] = directory_map(UPLOADFOLDER . '/files');
		$gallery_data['acceptedFiles'] = $this->ppmsystemlib->get_accepted_mimes();
		$data['upload'] = $this->load->view('admin/common/dz_modal', $gallery_data, TRUE);
		$d['editor_id'] = 'editor_1';

		$data['editor'] = $this->load->view('admin/common/editor', $d, TRUE);

		$data['pages'] = $this->pages->get_pages();

		$data['banner_details'] = $this->banners->get_banner_info($this->page_id);
		$data['banner_child'] = $this->banners->get_banner_child($this->page_id);

		$this->load->view('admin/banners_add_edit', $data);

		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/switchery/dist/switchery.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/moment/min/moment.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net/js/jquery.dataTables.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/iCheck/icheck.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/dropzone/dist/min/dropzone.min.js"></script>';

		$this->load->view('admin/common/footer', $data);

	}

	public function saveBanner(){
		$bid = $_POST['bid'];
		$txtLink = $_POST['txtLink'];
		$placement = $_POST['placement'];
		$selectedFilePath = $_POST['selectedFilePath'];
		$isActive = $_POST['isActive'];

		if(empty($bid)){
			$bid = '';	
		}

		$rs = $this->banners->save_banner($bid, $txtLink, $placement, $selectedFilePath, $isActive);
		// echo json_encode($rs);
		// print_r($_POST);
		echo json_encode($_POST);
	}

	public function deleteBanner(){
		$post = $this->input->post();
		if(!empty($post)){
			$bid = $this->input->post('bid');
			// echo $bid;
			$rs = $this->banners->delete_banner($bid);
		}
	}

	public function saveCarousel(){
		$post = $this->input->post();
		if(!empty($post)){
			// $count = $this->input->post('count');
			// print_r($post);
			$bid = $this->input->post('bid');
			// $imgPathC_arr = $this->input->post('imgPathC_arr');
			$imgPathC_arr = $_POST['imgPathC_arr'];
			$count = $this->input->post('count');
			$isActive = $this->input->post('isActive');
			$res = $this->banners->save_carousel($bid, $count, $imgPathC_arr, $isActive);
			echo $res;
		}
	}

}
