<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Downloads extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('PPMSystemLib');

		$this->load->model('admin/dashboardmodel', 'dashboard');
		$this->load->model('admin/downloadsmodel', 'downloads');

		$this->action = '';

		if(!$this->dashboard->is_logged_in()) {
			redirect(base_url() . 'admin/login');
		}
		else {
			if(!$this->dashboard->check_user_access(11)) {
				redirect(base_url() . 'admin/home');
			}
		}
	}

	public function index()
	{

		$this->action = $this->uri->segment(3, 'list');

		$data['active_page'] 	= "Downloads";
		$data['page_title'] 	= "Downloads | System Upgrade";
		$data['action']			= $this->action;

		$filters['section'] 	= array('all'=>'All','1'=>"Download",'2'=>"System Upgrade",'3'=>'Conference Download');
		
		$data['filters'] 		= $this->load->view('admin/downloads_filters', $filters, TRUE);
		
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/iCheck/skins/flat/green.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/dropzone/dist/min/dropzone.min.css" rel="stylesheet">';

		$this->load->view('admin/common/header', $data);
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/common/topnav');

		$this->load->helper('form');

		$add_edit['categoryArray'] = array(''=>'[ Select ]','1'=>"Download",'2'=>"System Upgrade",'3'=>'Conference Download');
		$add_edit['downloadTypeArray'] = array(''=>'[ Select ]',"Checklist"=>"Checklist","Letter"=>"Letter","Form"=>"Form","Report"=>"Report","Newsletter"=>"Newsletter","Marketing Material"=>"Marketing Material","Procedure"=>"Procedure","PowerPoint"=>"PowerPoint","General"=>"General");

		$state = array();

		foreach ($this->dashboard->get_state(9) as $key => $value) {
			$state[$value['id']] = $value['name'];
		}

		$add_edit['state'] = $state;

		$data['add_edit'] = $this->load->view('admin/downloads_add_edit', $add_edit, TRUE);

		$gallery_data = $this->ppmsystemlib->get_files();

		$gallery_data['directory'] 	= directory_map(UPLOADFOLDER . '/files');
		$gallery_data['acceptedFiles'] = $this->ppmsystemlib->get_accepted_mimes();
		$data['upload'] = $this->load->view('admin/common/dz_modal', $gallery_data, TRUE);

		$this->load->view('admin/downloads', $data);

		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/iCheck/icheck.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/moment/min/moment.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>';

		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net/js/jquery.dataTables.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.buttons.js"></script>';		
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/dropzone/dist/min/dropzone.min.js"></script>';

		$this->load->view('admin/common/footer', $data);	
	}

	public function get_list() 
	{
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$post = $this->input->post();

		if(count($post)>0){

			$category = $this->input->post('category');
			$result = array();

			if(is_numeric($category)) {
				$result = $this->downloads->get_downloads($category);
			}
			else {
				$result = $this->downloads->get_downloads();
			}

			$result_arr = array();

			/*
			id
			datePosted
			name
			code
			shortdesc
			platnium
			gold
			silver
			bronze
			*/

			foreach ($result as $key => $value) {
				$arr 				= array();

				$arr['id'] 			= $value['id'];
				$arr['datePosted'] 	= '<font class="hide">'. $this->ppmsystemlib->get_unix_from_date($value['datePosted']) . '</font> ' . $this->ppmsystemlib->check_date_time($value['datePosted'], 'd-M-Y');
				$arr['name'] 		= (strlen($value['name']) <= 30) ? $value['name'] : '<span class="text-word-wrap">'.$value['name'].'</span>';
				$arr['code'] 		= $value['code'];
				$arr['shortdesc'] 	= (strlen($value['shortdesc']) <= 30) ? $value['shortdesc'] : '<span class="text-word-wrap">'.$value['shortdesc'].'</span>';
				$arr['platnium'] 	= (intval($value['platnium']) === 1) ? '<i class="fa fa-check text-success"></i>' : '';
				$arr['gold'] 		= (intval($value['gold']) === 1) ? '<i class="fa fa-check text-success"></i>' : '';
				$arr['silver'] 		= (intval($value['silver']) === 1) ? '<i class="fa fa-check text-success"></i>' : '';
				$arr['bronze'] 		= (intval($value['bronze']) === 1) ? '<i class="fa fa-check text-success"></i>' : '';
				
				$arr['action']		= '<ul class="nav nav-pills" role="tablist"> <li role="presentation" class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">Action<span class="caret"></span></a><ul class="dropdown-menu animated fadeInDown" role="menu" aria-labelledby="drop6" style="left:-108%!important;"><li role="presentation"><a tabindex="-1" href="javascript:;">ID: <label class="action-id"></label></a></li><li role="presentation" class="divider"></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="edit">Edit</a></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="delete">Delete</a></li></ul></li></ul>';

				$result_arr[] = $arr;
			}		

			echo json_encode($result_arr);
		}
		else {
			echo json_encode(array());
		}		
	}

	public function process_action()
	{
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$post = $this->input->post();

		if(!empty($post)) {
			$id = $this->input->post('id');
			$action = $this->input->post('action');

			if($action === 'edit') {

				if(is_numeric($id)) {
					$result = $this->downloads->get_download($id);

					//echo json_encode($result); die();
					/*
					bronze
					code
					compatibility
					d1
					d2
					d3
					datePosted
					description
					documentlocation
					documenttype
					gold
					id
					mysection
					name
					platnium
					shortdesc
					silver
					*/			

					$data = array();

					foreach ($result['details'] as $key => $value) {
						$data['bronze']				= (intval($value['bronze']) === 1) ? TRUE : FALSE;
						$data['code']				= $value['code'];
						$data['compatibility']		= $value['compatibility'];
						$data['d1']					= $value['d1'];
						$data['d2']					= $value['d2'];
						$data['d3']					= $value['d3'];
						$data['datePosted']			= $this->ppmsystemlib->check_date_time($value['datePosted'], 'd-M-Y');
						$data['description']		= $value['description'];
						$data['documentlocation']	= $value['documentlocation'];
						$data['documenttype']		= $value['documenttype'];
						$data['gold']				= (intval($value['gold']) === 1) ? TRUE : FALSE;
						$data['mysection']			= $value['mysection'];
						$data['name']				= $value['name'];
						$data['platnium']			= (intval($value['platnium']) === 1) ? TRUE : FALSE;
						$data['shortdesc']			= $value['shortdesc'];
						$data['silver']				= (intval($value['silver']) === 1) ? TRUE : FALSE;
					}

					$state = array();

					foreach ($result['state'] as $key => $value) {
						$state[] = intval($value['categoryId']);
					}

					$data['state'] = $state;

					echo json_encode($data);
				}
			}
			elseif($action === 'delete') {
				if(is_numeric($id)) {
					$this->downloads->delete_download($id);

					echo json_encode(array('success'=>1));
				}
				else {
					echo json_encode(array('error'=>1));
				}
			}
		}
		else {
			exit('No direct script access allowed');
		}
	}	

	public function add_update()
	{
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$post = $this->input->post();

		/*
		bronze
		code
		compatibility
		d1
		d2
		d3
		datePosted
		description
		documentlocation
		documenttype
		gold
		id
		mysection
		name
		platnium
		shortdesc
		silver
		*/

		if(!empty($post)) {
			$bronze 			= $this->input->post('bronze');
			$code 				= $this->input->post('code');
			$compatibility 		= $this->input->post('compatibility');
			$d1 				= $this->input->post('d1');
			$d2 				= $this->input->post('d2');
			$d3 				= $this->input->post('d3');
			$datePosted 		= $this->input->post('datePosted');
			$description 		= $this->input->post('description');
			$documentlocation 	= $this->input->post('documentlocation');
			$documenttype 		= $this->input->post('documenttype');
			$gold				= $this->input->post('gold');
			$id 				= $this->input->post('id');
			$mysection 			= $this->input->post('mysection');
			$name 				= $this->input->post('name');
			$platnium 			= $this->input->post('platnium');
			$shortdesc 			= $this->input->post('shortdesc');
			$silver 			= $this->input->post('silver');
			$state 				= $this->input->post('state');

			$valid = TRUE;
			$error = NULL;


			$result = $this->validate_fields($id);

			if(!$result['valid']) {
				$valid = $result['valid'];
				$error = $result['error'];
			}

			if($valid) {

				$data = array();

				$data['bronze'] 			= ($bronze === 'on') ? 1 : 0;
				$data['code'] 				= $code;
				$data['compatibility'] 		= $compatibility;
				$data['d1'] 				= $d1;
				$data['d2'] 				= $d2;
				$data['d3'] 				= $d3;
				$data['datePosted'] 		= date('Y-m-d H:i:s', strtotime($datePosted));
				$data['description'] 		= $description;
				$data['documentlocation'] 	= $documentlocation;
				$data['documenttype'] 		= $documenttype;
				$data['gold'] 				= ($gold === 'on') ? 1 : 0;
				$data['id'] 				= $id;
				$data['mysection'] 			= intval($mysection);
				$data['name'] 				= $name;
				$data['platnium'] 			= ($platnium === 'on') ? 1 : 0;
				$data['shortdesc'] 			= $shortdesc;
				$data['silver'] 			= ($silver === 'on') ? 1 : 0;

				$arr 				= array();

				$arr['datePosted'] 	= '<font class="hide">'. $this->ppmsystemlib->get_unix_from_date(date('Y-m-d H:i:s', strtotime($datePosted))) . '</font> ' . $datePosted;
				$arr['name'] 		= (strlen($name) <= 30) ? $name : '<span class="text-word-wrap">'.$name.'</span>';
				$arr['code'] 		= $code;
				$arr['shortdesc'] 	= (strlen($shortdesc) <= 30) ? $shortdesc : '<span class="text-word-wrap">'.$shortdesc.'</span>';
				$arr['platnium'] 	= ($platnium === 'on') ? '<i class="fa fa-check text-success"></i>' : '';
				$arr['gold'] 		= ($gold === 'on') ? '<i class="fa fa-check text-success"></i>' : '';
				$arr['silver'] 		= ($silver === 'on') ? '<i class="fa fa-check text-success"></i>' : '';
				$arr['bronze'] 		= ($bronze === 'on') ? '<i class="fa fa-check text-success"></i>' : '';
				$arr['state']		= $state;

				if(is_numeric($id)) { //Edit
					$arr['id'] = $this->downloads->add_update_download($id, $data, $state);
				}
				else { //Add

					$id = $this->downloads->add_update_download(NULL, $data, $state);

					$arr['id'] = $id;
					$arr['action'] = '<ul class="nav nav-pills" role="tablist"> <li role="presentation" class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">Action<span class="caret"></span></a><ul class="dropdown-menu animated fadeInDown" role="menu" aria-labelledby="drop6" style="left:-108%!important;"><li role="presentation"><a tabindex="-1" href="javascript:;">ID: <label class="action-id"></label></a></li><li role="presentation" class="divider"></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="edit">Edit</a></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="delete">Delete</a></li></ul></li></ul>';
				}

				echo json_encode($arr);
			}
			else {
				echo json_encode(array("error"=>$error));
				return FALSE;
			}
		}
	}

	function validate_fields()
	{
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('datePosted', '"Date Posted"', 'required');
		$this->form_validation->set_rules('mysection', '"Section"', 'required');
		$this->form_validation->set_rules('name', '"Download Name"', 'required');
		$this->form_validation->set_rules('access', '"Select at least 1 access privilege."', 'required');
		

		if ($this->form_validation->run() == FALSE){
            return array("valid"=>FALSE, "error"=>$this->form_validation->error_array());
        }
        else {
        	return array("valid"=>TRUE);
        }
	}	
}
