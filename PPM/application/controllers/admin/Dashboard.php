<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('file');
		$this->load->helper('directory');
		$this->load->helper('form');
		$this->load->library('PPMSystemLib');

		$this->load->model('admin/dashboardmodel', 'dashboard');

		if(!$this->dashboard->is_logged_in()) {
			redirect(base_url() . 'admin/login');
		}
	}

	public function index()
	{

		$data['active_page'] 	= "home";
		$data['page_title']		= "Dashboard";

		$this->load->view('admin/common/header', $data);
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/common/topnav');

		$this->load->view('admin/home', $data);

		$this->load->view('admin/common/footer', $data);
	}

	public function get_captcha () {
		$map = directory_map(UPLOADFOLDER);

		echo '<pre>';
		//print_r($map);

		$this->load->helper('captcha');

		$vals = array(
        'word'          => '',
        'img_path'      => UPLOADFOLDER. '/captcha/',
        'img_url'       => base_url() . 'assets/uploads/captcha/',
        'font_path'     => UPLOADFOLDER. 'fonts/MyriadPro-Thin.ttf',
        'img_width'     => 150,
        'img_height'    => 30,
        'expiration'    => 5,//7200 => 2hrs
        'word_length'   => 8,
        'font_size'     => 20,
        'img_id'        => 'Imageid',
        'pool'          => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',

        // White background and border, black text and red grid
        'colors'        => array(
                'background' => array(255, 255, 255),
                'border' => array(255, 255, 255),
                'text' => array(0, 0, 0),
                'grid' => array(255, 40, 40)
        )
		);

		$cap = create_captcha($vals);

		echo $cap['image'];

		die();
	}

	private function get_files ()
	{
		$map = directory_map(UPLOADFOLDER);

		$arr_img = array();

		foreach ($map as $key => $value) {
			if(is_string($value) && $value!=='index.html') {
				if(file_exists(UPLOADFOLDER.'/'.$value)){
					$arr_info = get_file_info(UPLOADFOLDER.'/'.$value);
					if(array_key_exists('server_path',$arr_info)) unset($arr_info['server_path']);
					$arr_img[] = $arr_info;
				}
			}
		}

		$gallery_data['file_info'] = json_encode($arr_img);

		return $gallery_data;
	}

	public function list_categories()
	{
		if(!$this->dashboard->check_user_access(14)) {
			exit('No direct script access allowed');
		}

		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$post = $this->input->post();

		if(!empty($post)) {
			//array('section_name'=> array('newsTopic'))
			$section_name = $this->input->post('section[]');

		    $categories = array();

		    foreach ($section_name as $key => $value) {
		        $area   = $this->dashboard->data_collect['cat_sections'][$value]['area'];
		        $parent = $this->dashboard->data_collect['cat_sections'][$value]['parent'];
		        $title  = $this->dashboard->data_collect['cat_sections'][$value]['title'];

		        $categories[$value]['list'] = $this->dashboard->list_categories($area, $parent);

		        foreach ($categories[$value]['list'] as $k => $v) {
		            $categories[$value]['list'][$k]['area'] = $title;
		            $categories[$value]['list'][$k]['action'] = '<ul class="nav nav-pills" role="tablist"> <li role="presentation" class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">Action<span class="caret"></span></a><ul class="dropdown-menu animated fadeInDown" role="menu" aria-labelledby="drop6" style="left:-108%!important;"><li role="presentation"><a tabindex="-1" href="javascript:;">ID: <label class="action-id"></label></a></li><li role="presentation" class="divider"></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="edit" data-section="'.$value.'">Edit</a></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="delete">Delete</a></li></ul></li></ul>';
		        }
		    }

		    $list = array();

		    if(!empty($categories)) {
		    	foreach ($categories as $key => $value) {
		    		if(is_array($value['list'])) {
		    			foreach ($value['list'] as $k => $v) {
		    				$v['name'] = '<span class="text-word-wrap">'.$v['name'].'</span>';
		    				$list[] = $v;
		    			}
		    		}
		    	}
		    }

		    echo json_encode($list);
		}
	}

	public function process_action()
	{

		if(!$this->dashboard->check_user_access(14)) {
			exit('No direct script access allowed');
		}

		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$post = $this->input->post();

		$arr = array();

		if(!empty($post)) {
			$id = $this->input->post('id');
			$section = $this->input->post('section');
			$action = $this->input->post('action');

			if($action === 'edit') {			

				$result = $this->dashboard->get_category_details($id);

				foreach ($result as $key => $value) {
					if(is_array($value)) {
						foreach ($value as $k => $v) {
							$arr[$k] = $v;
						}
						$arr['section'] = $section;
					}
				}

				echo json_encode($arr);
			}
			elseif($action === 'delete') {
				if(is_numeric($id)) {
					$this->dashboard->delete_category($id);

					echo json_encode(array('success'=>1));
				}
				else {
					echo json_encode(array('error'=>1));
				}				
			}
		}
	}

	public function add_update()
	{

		if(!$this->dashboard->check_user_access(14)) {
			exit('No direct script access allowed');
		}

		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$post = $this->input->post();

		if(!empty($post)) {

			$id 		= $this->input->post('id');
			$section 	= $this->input->post('section');
			$name 		= $this->input->post('name');
			$order 		= $this->input->post('order');
			$body 		= $this->input->post('body');
			$d1 		= $this->input->post('d1');

			$valid = TRUE;
			$error = NULL;

			$result = $this->validate_fields();

			if(!$result['valid']) {
				$valid = $result['valid'];
				$error = $result['error'];
			}

			if($valid){

				$data = array();
				$arr = array();

				if(array_key_exists($section, $this->dashboard->data_collect['cat_sections'])) {
					if(empty($id)) {
						$data['name'] 	= $name;
					}

					$data['body'] 	= $body;
					$data['sort']	= $order;
					$data['d1']		= $d1;
					$data['area'] 	= $this->dashboard->data_collect['cat_sections'][$section]['area'];
		        	$data['parent'] = $this->dashboard->data_collect['cat_sections'][$section]['parent'];

		        	if(is_numeric($id)) {//EDIT
		        		$this->dashboard->update_category($id, $data);
		        	}
		        	else {
		        		$id = $this->dashboard->add_category($data);
		        	}

		        	if(!empty($id) && is_numeric($id)) {

			        	$arr = array(
			        		'id' 		=> $id,
			        		'name' 		=> $name,
			        		'area' 		=> $this->dashboard->data_collect['cat_sections'][$section]['title'],
			        		'sort'		=> $order,
			        		'action'	=> '<ul class="nav nav-pills" role="tablist"> <li role="presentation" class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">Action<span class="caret"></span></a><ul class="dropdown-menu animated fadeInDown" role="menu" aria-labelledby="drop6" style="left:-108%!important;"><li role="presentation"><a tabindex="-1" href="javascript:;">ID: <label class="action-id"></label></a></li><li role="presentation" class="divider"></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="edit" data-section="'.$section.'">Edit</a></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="delete">Delete</a></li></ul></li></ul>'
			        	);
		        	}

				}

				echo json_encode($arr);

			}
			else {
				echo json_encode(array("error"=>$error));
				return FALSE;
			}			
		}	
	}

	private function validate_fields()
	{

		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$this->load->library('form_validation');

		$this->form_validation->set_rules('section', '"Section"', 'required');
		$this->form_validation->set_rules('name', '"Name"', 'required');
		$this->form_validation->set_rules('order', '"Order"', 'required|is_numeric');
		$this->form_validation->set_rules('body', '"Body"', 'max_length[1000]');		

		if ($this->form_validation->run() == FALSE){
            return array("valid"=>FALSE, "error"=>$this->form_validation->error_array());
        }
        else {
        	return array("valid"=>TRUE);
        }			
	}
}
