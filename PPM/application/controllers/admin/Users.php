<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('PPMSystemLib');

		$this->load->model('admin/dashboardmodel', 'dashboard');

		$this->action = '';

		if(!$this->dashboard->is_logged_in()) {
			redirect(base_url() . 'admin/login');
		}
		else {
			if(isset($_SESSION['admin_status']) && !$_SESSION['admin_status']) {
				redirect(base_url() . 'admin/home');
			}
		}
	}

	public function index()
	{

		$this->action = $this->uri->segment(3, 'list');

		$data['active_page'] 	= "Users";
		$data['page_title'] 	= "Users";
		$data['action']			= $this->action;

		$userType = $_SESSION['admin_user_type'];

		//$users = $this->dashboard->get_users_by_type($userType);

		//echo '<pre>';
		//print_r($users);

		$usertypeArr = $this->dashboard->data_collect['user_type'];

		$filters['usertypeArr'] = $usertypeArr;

		$data['filters'] 	= $this->load->view('admin/users_filters', $filters, TRUE);

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">';

		$this->load->view('admin/common/header', $data);
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/common/topnav');

		$this->load->helper('form');

		$usertypeArr[0] 			= '[ Select ]';
		ksort($usertypeArr);
		$add_edit['usertypeArr'] 	= $usertypeArr;

		$add_edit['accessArr'] 		= $this->dashboard->data_collect['access'];

		$data['add_edit'] = $this->load->view('admin/users_add_edit', $add_edit, TRUE);

		$this->load->view('admin/users', $data);

		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net/js/jquery.dataTables.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.buttons.js"></script>';		

		$this->load->view('admin/common/footer', $data);	
	}

	public function get_list() 
	{
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$post = $this->input->post();

		if(count($post)>0){

			$userType = $this->input->post('usertype');
			$result = array();

			if(is_numeric($userType)) {
				$result = $this->dashboard->get_users_by_type($userType);
			}

			$result_arr = array();

			/*
			id
			username
			access
			userType
			*/

			foreach ($result as $key => $value) {
				$arr = array();
				
				$arr['id'] 			= $value['id'];
				$arr['username'] 	= (strlen($value['username']) <= 30) ? $value['username'] : '<span class="text-word-wrap">'.$value['username'].'</span>';
				$arr['usertype']	= $this->dashboard->data_collect['user_type'][intval($value['userType'])];

				$access = json_decode($value['access']);

				if (json_last_error() === JSON_ERROR_NONE) {
					$accessArr = $this->dashboard->data_collect['access'];

					$accessStr = array();

					foreach ($access->data as $key => $value) {
						$accessStr[] = $accessArr[$value];
					}

					$arr['access'] = '<span class="text-word-wrap">' . implode(' , ', $accessStr) . '</span>';
				}
				else {
					$arr['access'] = 'None';
				}
				
				$arr['action']	= '<ul class="nav nav-pills" role="tablist"> <li role="presentation" class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">Action<span class="caret"></span></a><ul class="dropdown-menu animated fadeInDown" role="menu" aria-labelledby="drop6" style="left:-108%!important;"><li role="presentation"><a tabindex="-1" href="javascript:;">ID: <label class="action-id"></label></a></li><li role="presentation" class="divider"></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="edit">Edit</a></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="delete">Delete</a></li></ul></li></ul>';

				$result_arr[] = $arr;
			}		

			echo json_encode($result_arr);
		}
		else {
			echo json_encode(array());
		}		
	}

	public function process_action()
	{
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$post = $this->input->post();

		if(!empty($post)) {
			$id = $this->input->post('id');
			$action = $this->input->post('action');

			if($action === 'edit') {

				if(is_numeric($id)) {
					$result = $this->dashboard->get_user_details($id);

					//echo json_encode($result); die();
					/*
					id
					username
					password
					access
					userType
					*/			

					$data = array();

					foreach ($result as $key => $value) {
						$data['username'] 			= $value['username'];
						$data['password'] 			= $value['password'];
						$data['confirmPassword'] 	= $value['password'];
						$data['access'] 			= $value['access'];
						$data['userType'] 			= $value['userType'];
					}

					echo json_encode($data);
				}
			}
			elseif($action === 'delete') {
				if(is_numeric($id)) {
					$this->dashboard->delete_user($id);

					echo json_encode(array('success'=>1));
				}
				else {
					echo json_encode(array('error'=>1));
				}
			}
		}
		else {
			exit('No direct script access allowed');
		}
	}	

	public function add_update()
	{
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$post = $this->input->post();

		/*
		access
		confirmPassword
		id
		password
		userType
		username
		*/

		if(!empty($post)) {
			$id 		= $this->input->post('id');
			$userType 	= $this->input->post('userType');
			$username 	= $this->input->post('username'); 
			$password 	= $this->input->post('password');
			$access 	= $this->input->post('access[]');

			$valid = TRUE;
			$error = NULL;


			$result = $this->validate_fields($id);

			if(!$result['valid']) {
				$valid = $result['valid'];
				$error = $result['error'];
			}

			if($valid) {

				$data = array();

				$data['userType'] 			= $userType;
				$data['username'] 			= $username;
				$data['password'] 			= $password;

				$access 					= (!empty($access)) ? $access : array();

				$data['access'] 			= json_encode(array('data'=>$access));

				$arr = array();

				$arr['username'] 	= $username;
				$arr['access']		= $access;
				$arr['userType'] 	= $userType;

				if(is_numeric($id)) { //Edit
					$arr['id'] = $this->dashboard->add_update_user($id, $data);
				}
				else { //Add

					$id = $this->dashboard->add_update_user(NULL, $data);

					$arr['id'] = $id;
					$arr['action'] = '<ul class="nav nav-pills" role="tablist"> <li role="presentation" class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">Action<span class="caret"></span></a><ul class="dropdown-menu animated fadeInDown" role="menu" aria-labelledby="drop6" style="left:-108%!important;"><li role="presentation"><a tabindex="-1" href="javascript:;">ID: <label class="action-id"></label></a></li><li role="presentation" class="divider"></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="edit">Edit</a></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="delete">Delete</a></li></ul></li></ul>';
				}

				echo json_encode($arr);
			}
			else {
				echo json_encode(array("error"=>$error));
				return FALSE;
			}
		}
	}

	function validate_fields($id = NULL)
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('userType', '"User Type"', 'required');
		
		if(empty($id)) {
			$this->form_validation->set_rules('username', '"Username"', 'required|is_unique[users.username]');
		}
		else {
			$this->form_validation->set_rules('username', '"Username"', 'required');
		}
		
		$this->form_validation->set_rules('password', '"Password"', 'required');
		$this->form_validation->set_rules('confirmPassword', 'Password Confirmation', 'required|matches[password]');

		if ($this->form_validation->run() == FALSE){
            return array("valid"=>FALSE, "error"=>$this->form_validation->error_array());
        }
        else {
        	return array("valid"=>TRUE);
        }
	}	
}
