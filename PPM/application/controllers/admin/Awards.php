<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Awards extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('file');
		$this->load->helper('directory');
		$this->load->helper('form');
		$this->load->helper('date');
		$this->load->helper('email');
		$this->load->helper('text');

		$this->load->library('form_validation');
		$this->load->library('PPMSystemLib');

		$this->load->model('admin/dashboardmodel', 'dashboard');
		$this->load->model('admin/awardsmodel', 'awards');

		$this->action = '';
		$this->memberId = 0;

		if(!$this->dashboard->is_logged_in()) {
			redirect(base_url() . 'admin/login');
		}
		else {
			if(!$this->dashboard->check_user_access(12)) {
				redirect(base_url() . 'admin/home');
			}
		}
	}

	public function index()
	{

		$this->action = $this->uri->segment(3, 'list');

		$data['active_page'] 	= "Awards";
		$data['page_title'] 	= "Awards";

		$data['action']	= $this->action;

		$categories = $this->awards->get_categories();

		$categories_arr = array(''=>'All');

		foreach ($categories as $key => $value) {
			$categories_arr[$value['id']] = $value['name'];
		}

		$filters['categories_arr'] = $categories_arr;

		$data['filters'] 	= $this->load->view('admin/awards_filters', $filters, TRUE);

		$gallery_data = $this->ppmsystemlib->get_files();

		$gallery_data['directory'] 	= directory_map(UPLOADFOLDER . '/files');
		$gallery_data['acceptedFiles'] = $this->ppmsystemlib->get_accepted_mimes();

		$data['upload'] = $this->load->view('admin/common/dz_modal', $gallery_data, TRUE);

		$year_arr = array(''=>'[ Select ]');

		for ($i=2009; $i <= intval(date('Y')) ; $i++) { 
			$year_arr[$i] = $i;
		}

		$add_edit['year_arr'] 		= $year_arr;
		$add_edit['type_arr'] 		= array(''=>'[ Select ]', '1'=>'Winner', '2'=>'Finalist');
		$categories_arr['']			= '[ Select ]';

		$add_edit['categories_arr'] = $categories_arr;

		$data['add_edit'] = $this->load->view('admin/awards_add_edit', $add_edit, TRUE);

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/switchery/dist/switchery.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/iCheck/skins/flat/green.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/dropzone/dist/min/dropzone.min.css" rel="stylesheet">';

		$this->load->view('admin/common/header', $data);
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/common/topnav');

		$this->load->helper('form');

		$this->load->view('admin/awards', $data);

		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/switchery/dist/switchery.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/moment/min/moment.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/js/moment/moment.timezone_2012_2022.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net/js/jquery.dataTables.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/iCheck/icheck.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.buttons.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/dropzone/dist/min/dropzone.min.js"></script>';

		$data['show_categories'] = $this->load->view('admin/show_categories', array('section_name'=> array('pmawards')), TRUE);

		$this->load->view('admin/common/footer', $data);			

	}

	public function get_list() 
	{
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$post = $this->input->post();

		if(count($post)>0){

			$categoryId = $this->input->post('category');
			$result = array();

			if(is_numeric($categoryId)) {
				$result = $this->awards->get_awards_by_category($categoryId);
			}
			else {
				$result = $this->awards->get_awards_by_category();
			}

			/*
			[0] => Array
		        (
		            [id] => 17
		            [name] => Wide Bay Prestige Properties
		            [category] => 934
		            [myyear] => 2009
		            [d1] => NEW Website Graphics/PPM Award Winners/2010 to 2014 PPMsystem/Wide Bay Prestige Properties - 300x180.jpg
		            [body] => PPM Group Property Management Excellence Award  agency of the Year (Member) 
		            [orderby] => 0
		            [mytype] => 1
		            [categoryname] => PPM Group Property Management Excellence Award Agency of the Year (Member)
		        )
			*/

			$result_arr = array();

			foreach ($result as $key => $value) {
				$arr = array();
				
				$arr['id'] 				= $value['id'];
				$arr['name'] 			= '<span class="text-word-wrap">'.$value['name'].'</span>';
				$arr['categoryname']	= '<span class="text-word-wrap">'.$value['categoryname'].'</span>';
				$arr['myyear'] 			= $value['myyear'];
				$arr['mytype'] 			= (intval($value['mytype']) === 1) ? 'Winner' : 'Finalist' ;

				$arr['action']			= '<ul class="nav nav-pills" role="tablist"> <li role="presentation" class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">Action<span class="caret"></span></a><ul class="dropdown-menu animated fadeInDown" role="menu" aria-labelledby="drop6" style="left:-108%!important;"><li role="presentation"><a tabindex="-1" href="javascript:;">ID: <label class="action-id"></label></a></li><li role="presentation" class="divider"></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="edit">Edit</a></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="copy">Copy</a></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="delete">Delete</a></li></ul></li></ul>';

				$result_arr[] = $arr;
			}		

			echo json_encode($result_arr);
		}
	}

	public function process_action()
	{
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$post = $this->input->post();

		if(!empty($post)) {
			$id = $this->input->post('id');
			$action = $this->input->post('action');

			if($action === 'edit' OR $action === 'copy') {

				if(is_numeric($id)) {
					$result = $this->awards->get_award_category($id);

					foreach ($result as $key => $value) {
						$data['body'] 				= $value['body'];
						$data['category'] 			= $value['category'];
						$data['d1'] 				= $value['d1'];
						$data['mytype'] 			= $value['mytype'];
						$data['myyear'] 			= $value['myyear'];
						$data['name'] 				= $value['name'];
					}

					echo json_encode($data);
				}
			}
			elseif($action === 'delete') {
				if(is_numeric($id)) {
					$this->awards->delete_award($id);

					echo json_encode(array('success'=>1));
				}
				else {
					echo json_encode(array('error'=>1));
				}
			}
		}
		else {
			exit('No direct script access allowed');
		}
	}	

	public function add_update()
	{
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$post = $this->input->post();

		if(!empty($post)) {

			$id 				= $this->input->post('id'); 
			$name 				= $this->input->post('name');
			$category 			= $this->input->post('category');
			$mytype 			= $this->input->post('mytype');
			$myyear 			= $this->input->post('myyear');
			$body 				= $this->input->post('body');
			$d1 				= $this->input->post('d1');		

			$valid = TRUE;
			$error = NULL;

			$result = $this->validate_fields();

			if(!$result['valid']) {
				$valid = $result['valid'];
				$error = $result['error'];
			}

			if($valid) {

				$data = array();

				$data['body'] 		= $body;
				$data['category'] 	= $category;
				$data['d1'] 		= $d1;
				$data['mytype'] 	= $mytype;
				$data['myyear'] 	= $myyear;
				$data['name'] 		= $name;

				$arr = array();

				$arr['name'] 			= '<span class="text-word-wrap">'.$name.'</span>';
				$arr['category']		= $category;
				$arr['myyear'] 			= $myyear;
				$arr['mytype'] 			= (intval($mytype) === 1) ? 'Winner' : 'Finalist' ;

				$arr['action']			= '<ul class="nav nav-pills" role="tablist"> <li role="presentation" class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">Action<span class="caret"></span></a><ul class="dropdown-menu animated fadeInDown" role="menu" aria-labelledby="drop6" style="left:-108%!important;"><li role="presentation"><a tabindex="-1" href="javascript:;">ID: <label class="action-id"></label></a></li><li role="presentation" class="divider"></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="edit">Edit</a></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="copy">Copy</a></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="delete">Delete</a></li></ul></li></ul>';					

				if(is_numeric($id)) { //Edit
					$arr['id'] = $id;

					$this->awards->update_award($id, $data);
				}
				else { //Add

					$id = $this->awards->add_award($data);
					$arr['id'] = $id;
				}

				echo json_encode($arr);
			}
			else {
				echo json_encode(array("error"=>$error));
				return FALSE;
			}
		}
	}

	function validate_fields()
	{
		$this->form_validation->set_rules('name', '"Name"', 'required');
		$this->form_validation->set_rules('myyear', '"Year"', 'required');
		$this->form_validation->set_rules('mytype', '"Type"', 'required');
		$this->form_validation->set_rules('category', '"Category"', 'required');

		if ($this->form_validation->run() == FALSE){
            return array("valid"=>FALSE, "error"=>$this->form_validation->error_array());
        }
        else {
        	return array("valid"=>TRUE);
        }
	}
}


