<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Events extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('file');
		$this->load->helper('directory');
		$this->load->helper('form');
		$this->load->helper('PPMSystem');

		$this->load->library('PPMSystemLib');
		
		$this->load->model('admin/dashboardmodel', 'dashboard');
		$this->load->model('admin/eventsmodel', 'events');

		$this->action = '';
		$this->memberId = 0;
		$this->page_id = 0;

	}
	public function index(){


		redirect(base_url() . 'admin/events/list');
		// echo '<pre>';

		// print_r($this->ppmsystemlib->get_accepted_mimes());

		die();
	}

	public function show_list(){
		$this->action = $this->uri->segment(3, 'list');
		//$this->dashboard->get_moderators('all');
		//echo $this->action;
		//echo '<pre>';
		//print_r();
		//die();

		// $filters = $this->get_filters();

		// if($this->action === 'add'){
		// 	if(!empty($this->uri->segment(4, 0))) redirect(base_url() . 'admin/members/add');
		// }
		// elseif($this->action === 'view'){
		//
		// 	$this->memberId = $this->uri->segment(4, 0);
		//
		// 	if(!empty($this->memberId) && is_numeric($this->memberId)){
		// 		$filters['member_data'] = $this->members->get_entry($this->memberId);//12651,877,16076
		// 	}
		// 	else{
		// 		redirect(base_url() . 'admin/members');
		// 	}
		//
		// }

		$data['active_page'] 	= "Pages";

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/switchery/dist/switchery.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/iCheck/skins/flat/green.css" rel="stylesheet">';

		$this->load->view('admin/common/header', $data);
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/common/topnav');

		$this->load->helper('form');

		// $data['filters'] 	= $this->load->view('admin/members_filters', $filters, TRUE);
		// $data['add_edit'] 	= $this->load->view('admin/members_add_edit', $filters, TRUE);
		$data['action'] 	= $this->action;
		$data['list_array'] = $this->events->get_pages();


		$this->load->view('admin/events', $data);

		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/switchery/dist/switchery.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/moment/min/moment.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net/js/jquery.dataTables.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/iCheck/icheck.min.js"></script>';



		$this->load->view('admin/common/footer', $data);
	}

	public function add_list(){

		$this->action = $this->uri->segment(3, 'add');
		//$this->dashboard->get_moderators('all');
		//echo $this->action;
		//echo '<pre>';
		//print_r();
		//die();

		// $filters = $this->get_filters();

		// if($this->action === 'add'){
		// 	if(!empty($this->uri->segment(4, 0))) redirect(base_url() . 'admin/members/add');
		// }
		// elseif($this->action === 'view'){
		//
		// 	$this->memberId = $this->uri->segment(4, 0);
		//
		// 	if(!empty($this->memberId) && is_numeric($this->memberId)){
		// 		$filters['member_data'] = $this->members->get_entry($this->memberId);//12651,877,16076
		// 	}
		// 	else{
		// 		redirect(base_url() . 'admin/members');
		// 	}
		//
		// }

		$data['active_page'] 	= "Add New Event";

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/switchery/dist/switchery.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/iCheck/skins/flat/green.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/dropzone/dist/min/dropzone.min.css" rel="stylesheet">';

		$this->load->view('admin/common/header', $data);
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/common/topnav');

		$this->load->helper('form');

		// $data['filters'] 	= $this->load->view('admin/members_filters', $filters, TRUE);
		// $data['add_edit'] 	= $this->load->view('admin/members_add_edit', $filters, TRUE);
		$data['action'] 	= $this->action;
		// $data['list_array'] = $this->pages->get_pages();

		$gallery_data = $this->ppmsystemlib->get_files();

		$data['category_list'] = $this->events->get_categories();
		$data['location_list'] = $this->events->get_locations();
		$data['gallery_list']  = $this->events->get_galleries();
		$data['product_list']  = $this->events->get_products();
		
		$gallery_data['directory'] = directory_map(UPLOADFOLDER . '/files');
		$gallery_data['acceptedFiles'] = $this->ppmsystemlib->get_accepted_mimes();
		$data['upload'] = $this->load->view('admin/common/dz_modal', $gallery_data, TRUE);
		// $d['editor_id'] = 'editor_1';

		// $data['editor'] = $this->load->view('admin/common/editor', $d, TRUE);

		$this->load->view('admin/events_add_edit', $data);

		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/switchery/dist/switchery.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/moment/min/moment.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net/js/jquery.dataTables.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/iCheck/icheck.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/dropzone/dist/min/dropzone.min.js"></script>';

		$this->load->view('admin/common/footer', $data);
	}

	public function edit_list(){
		$this->action = $this->uri->segment(3, 'edit');
		$this->page_id = $this->uri->segment(4);
		//$this->dashboard->get_moderators('all');
		//echo $this->action;
		//echo '<pre>';
		//print_r();
		//die();

		// $filters = $this->get_filters();

		// if($this->action === 'add'){
		// 	if(!empty($this->uri->segment(4, 0))) redirect(base_url() . 'admin/members/add');
		// }
		// elseif($this->action === 'view'){
		//
		// 	$this->memberId = $this->uri->segment(4, 0);
		//
		// 	if(!empty($this->memberId) && is_numeric($this->memberId)){
		// 		$filters['member_data'] = $this->members->get_entry($this->memberId);//12651,877,16076
		// 	}
		// 	else{
		// 		redirect(base_url() . 'admin/members');
		// 	}
		//
		// }

		$data['active_page'] 	= "Edit Page";

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/switchery/dist/switchery.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/iCheck/skins/flat/green.css" rel="stylesheet">';
		
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/dropzone/dist/min/dropzone.min.css" rel="stylesheet">';

		$this->load->view('admin/common/header', $data);
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/common/topnav');

		$this->load->helper('form');

		// $data['filters'] 	= $this->load->view('admin/members_filters', $filters, TRUE);
		// $data['add_edit'] 	= $this->load->view('admin/members_add_edit', $filters, TRUE);
		$data['action'] 	= $this->action;
		$data['list_array'] = $this->pages->get_pages();

		$gallery_data = $this->ppmsystemlib->get_files();
		$gallery_data['directory'] = directory_map(UPLOADFOLDER . '/files');
		$gallery_data['acceptedFiles'] = $this->ppmsystemlib->get_accepted_mimes();
		$data['upload'] = $this->load->view('admin/common/dz_modal', $gallery_data, TRUE);
		$d['editor_id'] = 'editor_1';


		$data['editor'] = $this->load->view('admin/common/editor', $d, TRUE);



		$data['page_id'] = $this->page_id;
		$data['is_parent_page'] = $this->pages->is_parent_page($this->page_id);
		$data['page_info'] = $this->pages->get_page($this->page_id);
		$this->load->view('admin/pages_add_edit', $data);

		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/switchery/dist/switchery.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/moment/min/moment.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net/js/jquery.dataTables.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/iCheck/icheck.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/dropzone/dist/min/dropzone.min.js"></script>';

		$this->load->view('admin/common/footer', $data);
	}


	private function get_files () 
	{
		$map = directory_map(UPLOADFOLDER);

		$arr_img = array();

		foreach ($map as $key => $value) {
			if(is_string($value) && $value!=='index.html') {
				if(file_exists(UPLOADFOLDER.'/'.$value)){
					$arr_info = get_file_info(UPLOADFOLDER.'/'.$value);
					if(array_key_exists('server_path',$arr_info)) unset($arr_info['server_path']);
					$arr_img[] = $arr_info;
				}
			}
		}

		$gallery_data['img_info'] = json_encode($arr_img);

		return $gallery_data;
	}	

	public function save_events(){
		// $post = $this->input->post();
		// if(!empty($post)){
		// 	$bid = $this->input->post('bid');
		// 	$res = $this->pages->banner_data($bid);
		// 	echo json_encode($res);
		// }
		// $post = $this->input->post();
		// print_r($post);
		// $eventpricing = $this->input->post('event_pricing[]');
		$res = $this->events->save_event();
		echo $res;
		// foreach ($arr as $value) {
		//     $option_name = $value[0];
		//     $cost = $value[1];
		//     $limit = $value[2];
		// }
	}

}
