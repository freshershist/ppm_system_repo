<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Partners extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('PPMSystemLib');

		$this->load->model('admin/dashboardmodel', 'dashboard');
		$this->load->model('admin/partnersmodel', 'partners');

		$this->action = '';

		if(!$this->dashboard->is_logged_in()) {
			redirect(base_url() . 'admin/login');
		}
		else {
			if(!$this->dashboard->check_user_access(22)) {
				redirect(base_url() . 'admin/home');
			}
		}
	}

	public function index()
	{

		$this->action = $this->uri->segment(3, 'list');

		$data['active_page'] 	= "partners";
		$data['page_title'] 	= "Partners";
		$data['action']			= $this->action;

		$data['filters'] 		= $this->load->view('admin/partners_filters', NULL, TRUE);

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/dropzone/dist/min/dropzone.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/js/fancybox-master/dist/jquery.fancybox.min.css" 		rel="stylesheet" media="screen">';

		$this->load->view('admin/common/header', $data);
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/common/topnav');

		$this->load->helper('form');

		$gallery_data = $this->ppmsystemlib->get_files();

		$gallery_data['directory'] 	= directory_map(UPLOADFOLDER . '/files');
		$gallery_data['acceptedFiles'] = $this->ppmsystemlib->get_accepted_mimes();

		$data['upload'] = $this->load->view('admin/common/dz_modal', $gallery_data, TRUE);

		$add_edit = array();

		$add_edit['pagesArr'] = $pages = $this->partners->get_pages();

		$data['add_edit'] = $this->load->view('admin/partners_add_edit', $add_edit, TRUE);

		$this->load->view('admin/partners', $data);

		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net/js/jquery.dataTables.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.buttons.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/dropzone/dist/min/dropzone.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/js/html5sortable/jquery.sortable.min.js"></script>';

		$this->load->view('admin/common/footer', $data);	
	}

	public function get_list() 
	{
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$get = $this->input->get();

		if(count($get)>0){

			$result = $this->partners->get_list();

			$result_arr = array();

			foreach ($result as $key => $value) {
				$arr = array();
				
				$arr['id'] 			= $value['id'];
				$arr['name'] 		= (strlen($value['name']) <= 30) ? $value['name'] : '<span class="text-word-wrap">'.$value['name'].'</span>';
				$arr['date_added']		= '<font class="hide">'. $this->ppmsystemlib->get_unix_from_date($value['date_added']) . '</font> ' . $this->ppmsystemlib->check_date_time($value['date_added']);
				
				$arr['action']		= '<ul class="nav nav-pills" role="tablist"> <li role="presentation" class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">Action<span class="caret"></span></a><ul class="dropdown-menu animated fadeInDown" role="menu" aria-labelledby="drop6" style="left:-108%!important;"><li role="presentation"><a tabindex="-1" href="javascript:;">ID: <label class="action-id"></label></a></li><li role="presentation" class="divider"></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="edit">Edit</a></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="delete">Delete</a></li></ul></li></ul>';

				$result_arr[] = $arr;
			}		

			echo json_encode($result_arr);
		}
		else {
			echo json_encode(array());
		}		
	}

	public function process_action()
	{
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$post = $this->input->post();

		if(!empty($post)) {
			$id = $this->input->post('id');
			$action = $this->input->post('action');

			if($action === 'edit') {

				if(is_numeric($id)) {
					$result = $this->partners->get_page($id);		

					$data = array();

					foreach ($result as $key => $value) {
						$data['name'] 		= $value['name'];
						$data['pages'] 		= json_decode($value['pages']);
						$data['partners'] 	= json_decode($value['photos']);
					}

					echo json_encode($data);
				}
			}
			elseif($action === 'delete') {
				if(is_numeric($id)) {
					$this->partners->delete_page($id);

					echo json_encode(array('success'=>1));
				}
				else {
					echo json_encode(array('error'=>1));
				}
			}
		}
		else {
			exit('No direct script access allowed');
		}
	}	

	public function add_update()
	{
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$post = $this->input->post();

		/*
		id
		name
		pages
		photos
		*/

		if(!empty($post)) {

			$id 		= $this->input->post('id');
			$name 		= $this->input->post('name');
			$pages 		= $this->input->post('pages[]');
			$photos 	= $this->input->post('partnersItems[]');

			$valid = TRUE;
			$error = NULL;

			$result = $this->validate_fields($id);

			if(!$result['valid']) {
				$valid = $result['valid'];
				$error = $result['error'];
			}

			if($valid) {

				$data = array();

				$data['name'] 		= $name;
				$data['pages']		= (!empty($pages)) ? json_encode($pages) : json_encode(array());

				$data['photos'] = json_encode(array());

				if(is_array($photos) && count($photos)>0) {
		            $photos = array_chunk($photos,3);
		            $arr = array();

		            $ctr = 0;
		            foreach ($photos as $value) {
		            	if(empty($value[1]) && empty($value[2])) {
			            	unset($photos[$ctr]);
			            }
			            else {
			            	$arr[] = array('order'=>$value[0], 'photo'=>$value[1], 'link'=>$value[2]);
			            }

			            $ctr++;
		            }

		            $data['photos'] = json_encode($arr);
		        }

				$arr = array();

				$arr['name'] 		= $name;

				if(is_numeric($id)) { //Edit
					$arr['id'] = $this->partners->add_update_page($id, $data);
				}
				else { //Add

					$id = $this->partners->add_update_page(NULL, $data);

					$arr['id'] = $id;
					$arr['date_added'] = '<font class="hide">'. now() . '</font> ' . date('d/m/Y h:i:s A', now());
					$arr['action'] = '<ul class="nav nav-pills" role="tablist"> <li role="presentation" class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">Action<span class="caret"></span></a><ul class="dropdown-menu animated fadeInDown" role="menu" aria-labelledby="drop6" style="left:-108%!important;"><li role="presentation"><a tabindex="-1" href="javascript:;">ID: <label class="action-id"></label></a></li><li role="presentation" class="divider"></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="edit">Edit</a></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="delete">Delete</a></li></ul></li></ul>';
				}

				echo json_encode($arr);
			}
			else {
				echo json_encode(array("error"=>$error));
				return FALSE;
			}
		}
	}

	function validate_fields($id = NULL)
	{
		$this->load->library('form_validation');
		
		if(empty($id)) {
			$this->form_validation->set_rules('name', '"Name"', 'required|is_unique[partnerspage.name]');
		} else {
			$this->form_validation->set_rules('name', '"Name"', 'required');
		}

		if ($this->form_validation->run() == FALSE){
            return array("valid"=>FALSE, "error"=>$this->form_validation->error_array());
        }
        else {
        	return array("valid"=>TRUE);
        }
	}	
}
