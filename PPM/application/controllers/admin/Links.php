<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Links extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('file');
		$this->load->helper('directory');
		$this->load->helper('form');
		$this->load->helper('date');
		$this->load->helper('email');
		$this->load->helper('text');

		$this->load->library('form_validation');
		$this->load->library('PPMSystemLib');

		$this->load->model('admin/dashboardmodel', 'dashboard');
		$this->load->model('admin/linksmodel', 'links');

		$this->action = '';
		$this->memberId = 0;

		if(!$this->dashboard->is_logged_in()) {
			redirect(base_url() . 'admin/login');
		}
		else {
			if(!$this->dashboard->check_user_access(9)) {
				redirect(base_url() . 'admin/home');
			}
		}
	}

	public function index()
	{

		$this->action = $this->uri->segment(3, 'list');

		$data['active_page'] 	= "Links";

		$data['action']	= $this->action;

		$filters['categories'] = $this->links->get_categories();

		$categories_arr = array();

		foreach ($filters['categories'] as $key => $value) {
			
			if(intval($value['parent']) === 35) {
				if(!array_key_exists('national', $categories_arr)) {
					$categories_arr['national'] = array();
				}

				$categories_arr['national'][$value['id']] = $value['name']; 

			}
			elseif(intval($value['parent']) === 37) {
				if(!array_key_exists('industry', $categories_arr)) {
					$categories_arr['industry'] = array();
				}
				
				$categories_arr['industry'][$value['id']] = $value['name'];
			}
			elseif(intval($value['parent']) === 145) {
				if(!array_key_exists('system', $categories_arr)) {
					$categories_arr['system'] = array();
				}
				
				$categories_arr['system'][$value['id']] = $value['name'];
			}				
		}

		$filters['categories_arr'] = $categories_arr;

		$gallery_data = $this->ppmsystemlib->get_files();

		$gallery_data['directory'] 	= directory_map(UPLOADFOLDER . '/files');
		$gallery_data['acceptedFiles'] = $this->ppmsystemlib->get_accepted_mimes();

		$data['upload'] = $this->load->view('admin/common/dz_modal', $gallery_data, TRUE);
		$editor['editor_id'] = 'data_body';
		//$editor['text']	= "test";

		$add_edit['editor'] = $this->load->view('admin/common/editor', $editor, TRUE);		

		switch ($this->action) {
			case 'national-links':
				$data['page'] 						= 'national';
				$data['page_title'] 				= "National Links";
				$data['parent_category'] 			= 35;
				$add_edit['sub_categories2_arr'] 	= $this->ppmsystemlib->get_data_arr('nationalSubCatArray');
				break;

			case 'industry-service-providers':
				$data['page'] 		= 'industry';
				$data['page_title'] = "Industry Service Providers";
				$data['parent_category'] = 37;
				break;
				
			case 'system-member-offices':
				$data['page'] 		= 'system';
				$data['page_title'] = "System Member Offices";
				$data['parent_category'] = 145;

				break;	
			
			default:
				redirect(base_url() . 'admin/links/national-links');
				break;
		}

		$add_edit['tab']				= $data['page'];
		$add_edit['sub_categories_arr'] = $categories_arr;
		$add_edit['parent_category'] 	= $data['parent_category'];

		$data['add_edit'] 				= $this->load->view('admin/links_add_edit', $add_edit, TRUE);

		$filters['page'] 				= $data['page'];
		$filters['page_title'] 			= $data['page_title'];
		$filters['parent_category'] 	= $data['parent_category'];

		$data['filters'] 	= $this->load->view('admin/links_filters', $filters, TRUE);

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/switchery/dist/switchery.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/iCheck/skins/flat/green.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/dropzone/dist/min/dropzone.min.css" rel="stylesheet">';

		$this->load->view('admin/common/header', $data);
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/common/topnav');

		$this->load->helper('form');

		$this->load->view('admin/links', $data);

		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/switchery/dist/switchery.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/moment/min/moment.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/js/moment/moment.timezone_2012_2022.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net/js/jquery.dataTables.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/iCheck/icheck.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.buttons.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/dropzone/dist/min/dropzone.min.js"></script>';

		$data['show_categories'] = $this->load->view('admin/show_categories', array('section_name'=> array('linksISP')), TRUE);

		$this->load->view('admin/common/footer', $data);			

	}

	public function get_list() 
	{
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$post = $this->input->post();

		if(count($post)>0){

			$categoryId = $this->input->post('category');
			$parentCategory = $this->input->post('parent-category');
			$result = array();

			if(is_numeric($categoryId)) {
				$result = $this->links->get_links_by_category($categoryId);
			}
			else {
				$result = $this->links->get_links_by_category();
			}

			$result_arr = array();

			/*
			cost
			displaylinkspage
			displaymember
			expiry
			id
			isactive
			name
			website
			*/

			foreach ($result as $key => $value) {
				$arr = array();

				if(intval($parentCategory) === 37) {	//Industry Service Providers
					$arr['cost'] 				= (!empty($value['cost'])) ? '<i class="fa fa-dollar"></i> ' . $value['cost'] : '';
					$arr['displaymember'] 		= (intval($value['displaymember']) === 1) ? '<font class="text-success">YES</font>' : '' ;
					$arr['expiry'] 				= '<font class="hide">'. $this->ppmsystemlib->get_unix_from_date($value['expiry']) . '</font> ' . $this->ppmsystemlib->check_date_time($value['expiry']);
					
				}
				
				$arr['website']		= '<span class="text-word-wrap">'.$value['website'].'</span>';
				$arr['name'] 		= (strlen($value['name']) <= 30) ? $value['name'] : '<span class="text-word-wrap">'.$value['name'].'</span>';
				$arr['id'] 			= $value['id'];
				$arr['isactive'] 		= (intval($value['isactive']) === 1) ? '<font class="text-success">YES</font>' : '' ;
				$arr['displaylinkspage'] 		= (intval($value['displaylinkspage']) === 1) ? '<font class="text-success">YES</font>' : '' ;
				$arr['action']	= '<ul class="nav nav-pills" role="tablist"> <li role="presentation" class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">Action<span class="caret"></span></a><ul class="dropdown-menu animated fadeInDown" role="menu" aria-labelledby="drop6" style="left:-108%!important;"><li role="presentation"><a tabindex="-1" href="javascript:;">ID: <label class="action-id"></label></a></li><li role="presentation" class="divider"></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="edit">Edit</a></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="delete">Delete</a></li></ul></li></ul>';

				$result_arr[] = $arr;
			}		

			echo json_encode($result_arr);
		}
	}

	public function process_action()
	{
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$post = $this->input->post();

		if(!empty($post)) {
			$id = $this->input->post('id');
			$action = $this->input->post('action');

			if($action === 'edit') {

				if(is_numeric($id)) {
					$result = $this->links->get_links_category($id);

					//echo json_encode($result); die();
					/*
					address
					body
					categoryID
					cost
					d1
					d2
					displaylinkspage
					displaymember
					email
					expiry
					fax
					isActive
					memberDiscounts
					name
					nationalSubCat
					pcontact
					phone
					postcode
					shortDesc
					website
					*/			

					$data = array();

					foreach ($result as $key => $value) {
						$data['address'] 			= $value['address'];
						$data['body'] 				= $value['body'];
						$data['categoryID'] 		= $value['categoryID'];
						$data['cost'] 				= $value['cost'];
						$data['d1'] 				= $value['d1'];
						$data['d2'] 				= $value['d2'];
						$data['displaylinkspage'] 	= $value['displaylinkspage'];
						$data['displaymember'] 		= $value['displaymember'];
						$data['email'] 				= $value['email'];
						$data['expiry'] 			= $this->ppmsystemlib->check_date_time($value['expiry']);
						$data['fax'] 				= $value['fax'];
						$data['isActive'] 			= $value['isActive'];
						$data['memberDiscounts'] 	= $value['memberDiscounts'];
						$data['name'] 				= $value['name'];
						$data['nationalSubCat'] 	= $value['nationalSubCat'];
						$data['pcontact'] 			= $value['pcontact'];
						$data['phone'] 				= $value['phone'];
						$data['postcode'] 			= $value['postcode'];
						$data['shortDesc'] 			= $value['shortDesc'];
						$data['website'] 			= $value['website'];

					}

					echo json_encode($data);
				}
			}
			elseif($action === 'delete') {
				if(is_numeric($id)) {
					$this->links->delete_link($id);

					echo json_encode(array('success'=>1));
				}
				else {
					echo json_encode(array('error'=>1));
				}
			}
		}
		else {
			exit('No direct script access allowed');
		}
	}	

	public function add_update()
	{
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$post = $this->input->post();

		if(!empty($post)) {
			$address 			= $this->input->post('address');
			$category 			= $this->input->post('category'); 
			$cost 				= $this->input->post('cost');
			$d1 				= $this->input->post('d1');
			$d2 				= $this->input->post('d2');
			$body 				= $this->input->post('data_body');
			$displaymember 		= $this->input->post('displaymember');
			$displaylinkspage	= $this->input->post('displaylinkspage');
			$email 				= $this->input->post('email');
			$expiry 			= $this->input->post('expiry');
			$fax 				= $this->input->post('fax');
			$id 				= $this->input->post('id');
			$isActive 			= $this->input->post('isActive');
			$memberDiscounts 	= $this->input->post('memberDiscounts');
			$name 				= $this->input->post('name');
			$nationalSubCat 	= $this->input->post('nationalSubCat');
			$pcontact 			= $this->input->post('pcontact');
			$phone 				= $this->input->post('phone');
			$postcode 			= $this->input->post('postcode');
			$shortDesc 			= $this->input->post('shortDesc');
			$subCategory 		= $this->input->post('subCategory');
			$website 			= $this->input->post('website');

			$valid = TRUE;
			$error = NULL;

			if(empty($category)) {
				echo json_encode(array("error"=>array('category'=>'Category is required.')));

				return FALSE;
			}

			$result = $this->validate_fields(intval($category));

			if(!$result['valid']) {
				$valid = $result['valid'];
				$error = $result['error'];
			}

			if($valid) {

				$data = array();

				$data['address'] 			= $address;
				$data['cost'] 				= $cost;
				$data['d1'] 				= $d1;
				$data['d2'] 				= $d2;
				$data['body'] 				= $body;
				$data['displaymember'] 		= ($displaymember === 'on') ? 1 : 0;
				$data['displaylinkspage']	= ($displaylinkspage === 'on') ? 1 : 0;
				$data['email'] 				= $email;
				$data['expiry'] 			= $this->ppmsystemlib->set_final_date_time($expiry);
				$data['fax'] 				= $fax;
				$data['isActive'] 			= ($isActive === 'on') ? 1 : 0;
				$data['memberDiscounts'] 	= $memberDiscounts;
				$data['name'] 				= $name;
				$data['nationalSubCat'] 	= $nationalSubCat;
				$data['pcontact'] 			= $pcontact;
				$data['phone'] 				= $phone;
				$data['postcode'] 			= $postcode;
				$data['shortDesc'] 			= $shortDesc;
				$data['website'] 			= $website;	

				$arr = array();



				if(intval($category) === 37) {
					$arr['amount'] = (!empty($cost)) ? '<i class="fa fa-dollar"></i> ' . $cost : '';
					$arr['expiry'] = '<font class="hide">'. $this->ppmsystemlib->get_unix_from_date($this->ppmsystemlib->set_final_date_time($expiry)) . '</font> ' . $expiry;
					$arr['displaymember'] = ($displaymember === 'on') ? '<font class="text-success">YES</font>' : '';
				}

				$arr['name'] = (strlen($name) <= 30) ? $name : '<span class="text-word-wrap">'.$name.'</span>';
				$arr['website'] = $website;
				$arr['active'] = ($isActive === 'on') ? '<font class="text-success">YES</font>' : '';
				$arr['displaylinkspage'] = ($displaylinkspage === 'on') ? '<font class="text-success">YES</font>' : '';
				

				if(is_numeric($id)) { //Edit
					$arr['id'] = $this->links->add_update_link($id, $subCategory, $data);
				}
				else { //Add

					$id = $this->links->add_update_link(NULL, $subCategory, $data);

					$arr['id'] = $id;
					$arr['action'] = '<ul class="nav nav-pills" role="tablist"> <li role="presentation" class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">Action<span class="caret"></span></a><ul class="dropdown-menu animated fadeInDown" role="menu" aria-labelledby="drop6" style="left:-108%!important;"><li role="presentation"><a tabindex="-1" href="javascript:;">ID: <label class="action-id"></label></a></li><li role="presentation" class="divider"></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="edit">Edit</a></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="delete">Delete</a></li></ul></li></ul>';
				}

				echo json_encode($arr);
			}
			else {
				echo json_encode(array("error"=>$error));
				return FALSE;
			}
		}
	}

	function validate_fields($categoryId = NULL)
	{
		if(!empty($categoryId)) {

			$this->form_validation->set_rules('name', '"Company Name"', 'required');
			$this->form_validation->set_rules('pcontact', '"Primary Contact"', 'required');
			$this->form_validation->set_rules('subCategory', '"Sub Category"', 'required');

			if($categoryId === 35) {
				$this->form_validation->set_rules('nationalSubCat', '"Sub Category 2"', 'required');
			}
			
			$this->form_validation->set_rules('website', '"Website"', 'required');
			$this->form_validation->set_rules('postcode', '"Postcode"', 'required|is_numeric');

			if($categoryId === 37) {
				$this->form_validation->set_rules('cost', '"Amount"', 'is_numeric');
				$this->form_validation->set_rules('expiry', '"Expiry Date"', 'callback_is_date', array('is_date'=>'Invalid Date.'));
				$this->form_validation->set_rules('memberDiscounts', '"Member Discounts"', 'max_length[500]');
			}

			if ($this->form_validation->run() == FALSE){
	            return array("valid"=>FALSE, "error"=>$this->form_validation->error_array());
	        }
	        else {
	        	return array("valid"=>TRUE);
	        }
		}
	}

	function is_date ($dateTimeStr) 
	{
		if(empty($dateTimeStr)) return TRUE;

		$dateTime = explode(' ', $dateTimeStr);

		$au_date = explode('/', $dateTime[0]);

		if(count($au_date) === 3) {
			$day 	= $au_date[0];
			$month 	= $au_date[1];
			$year 	= $au_date[2];

			if(is_numeric($day) && is_numeric($month) && is_numeric($year)) {
				return checkdate(intval($month), intval($day), intval($year));
			}
			else {
				return FALSE;
			}
		}
		else {
			return FALSE;
		}
	}
}


