<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Homepage extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('PPMSystemLib');

		$this->load->model('admin/dashboardmodel', 'dashboard');
		$this->load->model('admin/homepagemodel', 'homepage');

		$this->action = '';

		if(!$this->dashboard->is_logged_in()) {
			redirect(base_url() . 'admin/login');
		}
		else {
			if(!$this->dashboard->check_user_access(21)) {
				redirect(base_url() . 'admin/home');
			}
		}
	}

	public function index()
	{

		$this->action = $this->uri->segment(3, 'list');

		$data['active_page'] 	= "homepage";
		$data['page_title'] 	= "Home Page";
		$data['action']			= $this->action;

		$data['filters'] 		= $this->load->view('admin/homepage_filters', NULL, TRUE);

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">';

		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/switchery/dist/switchery.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/gentelella/vendors/dropzone/dist/min/dropzone.min.css" rel="stylesheet">';
		$data['other_styles'][] = '<link href="'. base_url() . 'assets/js/fancybox-master/dist/jquery.fancybox.min.css" 		rel="stylesheet" media="screen">';

		$this->load->view('admin/common/header', $data);
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/common/topnav');

		$this->load->helper('form');

		$gallery_data = $this->ppmsystemlib->get_files();

		$gallery_data['directory'] 	= directory_map(UPLOADFOLDER . '/files');
		$gallery_data['acceptedFiles'] = $this->ppmsystemlib->get_accepted_mimes();
		$gallery_data['is_homepage'] = TRUE;

		$data['upload'] = $this->load->view('admin/common/dz_modal', $gallery_data, TRUE);
		$editor['editor_id'] = 'data_body';

		$add_edit['editor'] = $this->load->view('admin/common/editor', $editor, TRUE);		

		$data['add_edit'] = $this->load->view('admin/homepage_add_edit', $add_edit, TRUE);

		$this->load->view('admin/homepage', $data);

		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/moment/min/moment.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>';

		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net/js/jquery.dataTables.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/pnotify/dist/pnotify.buttons.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/switchery/dist/switchery.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/gentelella/vendors/dropzone/dist/min/dropzone.min.js"></script>';
		$data['other_scripts'][] = '<script src="'. base_url() . 'assets/js/html5sortable/jquery.sortable.min.js"></script>';

		$this->load->view('admin/common/footer', $data);	
	}

	public function get_list() 
	{
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$get = $this->input->get();

		if(count($get)>0){

			$result = $this->homepage->get_list();

			$result_arr = array();

			foreach ($result as $key => $value) {
				$arr = array();
				
				$arr['id'] 			= $value['id'];
				$arr['name'] 		= (strlen($value['name']) <= 30) ? $value['name'] : '<span class="text-word-wrap">'.$value['name'].'</span>';
				$active 			= intval($value['isActive']);
				$arr['isActive']	= (empty($active)) ? '' : '<span class="text-success is-page-active"><i class="fa fa-check"></i></span>';
				$arr['date_added']		= '<font class="hide">'. $this->ppmsystemlib->get_unix_from_date($value['date_added']) . '</font> ' . $this->ppmsystemlib->check_date_time($value['date_added']);
				
				$arr['action']		= '<ul class="nav nav-pills" role="tablist"> <li role="presentation" class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">Action<span class="caret"></span></a><ul class="dropdown-menu animated fadeInDown" role="menu" aria-labelledby="drop6" style="left:-108%!important;"><li role="presentation"><a tabindex="-1" href="javascript:;">ID: <label class="action-id"></label></a></li><li role="presentation" class="divider"></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="edit">Edit</a></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="delete">Delete</a></li></ul></li></ul>';

				$result_arr[] = $arr;
			}		

			echo json_encode($result_arr);
		}
		else {
			echo json_encode(array());
		}		
	}

	public function process_action()
	{
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$post = $this->input->post();

		if(!empty($post)) {
			$id = $this->input->post('id');
			$action = $this->input->post('action');

			if($action === 'edit') {

				if(is_numeric($id)) {
					$result = $this->homepage->get_page($id);

					//echo json_encode($result); die();
					/*
					id
					name
					banner
					banner_text
					body
					featured
					partners
					isActive
					date_added
					*/			

					$data = array();

					foreach ($result as $key => $value) {
						$data['name'] 		= $value['name'];
						$data['banner'] 	= $value['banner'];
						$data['banner_text']= $value['banner_text'];
						$data['body'] 		= $value['body'];
						$data['featured'] 	= $value['featured'];
						$data['partners'] 	= $value['partners'];
						$data['isActive'] 	= $value['isActive'];
					}

					echo json_encode($data);
				}
			}
			elseif($action === 'delete') {
				if(is_numeric($id)) {
					$this->homepage->delete_page($id);

					echo json_encode(array('success'=>1));
				}
				else {
					echo json_encode(array('error'=>1));
				}
			}
		}
		else {
			exit('No direct script access allowed');
		}
	}	

	public function add_update()
	{
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$post = $this->input->post();

		/*
		id
		name
		banner
		banner_text
		body
		featured
		partners
		isActive
		date_added
		*/

		if(!empty($post)) {

			$id 			= $this->input->post('id');
			$name 			= $this->input->post('name');
			$banner 		= $this->input->post('banner');
			$banner_text 	= $this->input->post('banner_text');
			$body 			= $this->input->post('data_body');
			$featured 		= $this->input->post('featuredItems[]');
			$partners 		= $this->input->post('partnersItems[]');
			$isActive 		= $this->input->post('isActive');

			$valid = TRUE;
			$error = NULL;

			$result = $this->validate_fields($id);

			if(!$result['valid']) {
				$valid = $result['valid'];
				$error = $result['error'];
			}

			if($valid) {

				$data = array();

				$data['isActive']	= ($isActive === 'on') ? 1 : 0;
				$data['name'] 		= $name;
				$data['banner']		= $banner;
				$data['banner_text']= $banner_text;
				$data['body']		= $body;

				if(is_array($featured) && count($featured)>0) {
		            $featured = array_chunk($featured,5);
		            $arr = array();

		            $ctr = 0;
		            foreach ($featured as $value) {
		            	if(empty($value[1]) && empty($value[2]) && empty($value[3]) && empty($value[4])) {
			            	unset($featured[$ctr]);
			            }
			            else {
			            	$arr[] = array('order'=>$value[0], 'photo'=>$value[1], 'icon'=>$value[2], 'title'=>$value[3], 'link'=>$value[4]);
			            }

			            $ctr++;
		            }

		            $data['featured'] = json_encode($arr);
		        }

				if(is_array($partners) && count($partners)>0) {
		            $partners = array_chunk($partners,3);
		            $arr = array();

		            $ctr = 0;
		            foreach ($partners as $value) {
		            	if(empty($value[1]) && empty($value[2])) {
			            	unset($partners[$ctr]);
			            }
			            else {
			            	$arr[] = array('order'=>$value[0], 'photo'=>$value[1], 'link'=>$value[2]);
			            }

			            $ctr++;
		            }

		            $data['partners'] = json_encode($arr);
		        }

				$arr = array();

				$arr['name'] 		= $name;
				$arr['isActive']	= ($isActive === 'on') ? '<span class="text-success is-page-active"><i class="fa fa-check"></i></span>' : '';

				if(is_numeric($id)) { //Edit
					$arr['id'] = $this->homepage->add_update_page($id, $data);
				}
				else { //Add

					$id = $this->homepage->add_update_page(NULL, $data);

					$arr['id'] = $id;
					$arr['date_added'] = '<font class="hide">'. now() . '</font> ' . date('d/m/Y h:i:s A', now());
					$arr['action'] = '<ul class="nav nav-pills" role="tablist"> <li role="presentation" class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">Action<span class="caret"></span></a><ul class="dropdown-menu animated fadeInDown" role="menu" aria-labelledby="drop6" style="left:-108%!important;"><li role="presentation"><a tabindex="-1" href="javascript:;">ID: <label class="action-id"></label></a></li><li role="presentation" class="divider"></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="edit">Edit</a></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="delete">Delete</a></li></ul></li></ul>';
				}

				echo json_encode($arr);
			}
			else {
				echo json_encode(array("error"=>$error));
				return FALSE;
			}
		}
	}

	function validate_fields($id = NULL)
	{
		$this->load->library('form_validation');
		
		if(empty($id)) {
			$this->form_validation->set_rules('name', '"Name"', 'required|is_unique[homepage.name]');
		} else {
			$this->form_validation->set_rules('name', '"Name"', 'required');
		}

		if ($this->form_validation->run() == FALSE){
            return array("valid"=>FALSE, "error"=>$this->form_validation->error_array());
        }
        else {
        	return array("valid"=>TRUE);
        }
	}	
}
