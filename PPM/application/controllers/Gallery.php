<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('ppmsystem');
		$this->load->library('PPMSystemLib');
		$this->load->model('frontend/Membersmodel', 'members');
		$this->load->model('frontend/Sitemodel', 'site');

		$this->is_ppmsystem = $this->members->is_logged_in();
	}

	public function index()
	{
		$data['membershipType'] = ($this->is_ppmsystem) ? intval($_SESSION['membershipType']) : -1;
		$action 				= $this->uri->segment(2, '');
		$data['page']			= (empty($action)) ? 'home' : $action;

		switch ($action) {
			case '':
			case 'list':
				$pagenumber 			= $this->uri->segment(3, 0);
				$data['title'] 			= 'GALLERY';
				$data['galleries'] 		= $this->site->get_galleries($pagenumber);
				$data['start_row']		= (is_numeric($pagenumber)) ? intval($pagenumber)+1:1;
				$data['end_row']		= intval($pagenumber)+count($data['galleries']);
				$data['total'] 			= $this->site->get_galleries(NULL, TRUE);
		        $data['pagination']		=  $this->ppmsystemlib->create_pagination(base_url() . 'gallery/list/', $data['total'], 12, 3, 9);
		        break;
		    case 'album':
		    	$id 					= $this->uri->segment(3, 0);

		    	if(empty($id) OR !is_numeric($id)) {
		    		redirect(base_url() . 'gallery');
		    	}

		    	$data['album']			= $this->site->get_album($id);

		    	if(!empty($data['album'])) {
		    		if(!$this->is_ppmsystem && $data['album'][0]['category'] === '947') {//For members only
		    			redirect(base_url() . 'ppmsystem-login');
		    		}
		    	}

		    	$data['title'] 			= (!empty($data['album'])) ? $data['album'][0]['name'] : 'GALLERY';
		    	$data['body']			= (!empty($data['album'])) ? $data['album'][0]['body'] : '';
		    	$data['myDate']			= (!empty($data['album'])) ? $this->ppmsystemlib->check_date_time($data['album'][0]['mydate']) : '';
		    	$data['pics']			= $this->site->get_album($id, TRUE);
		    	$data['other_styles'][] = '<link href="'. base_url() . 'assets/photoswipe/dist/photoswipe.css"; rel="stylesheet">';
		    	$data['other_styles'][] = '<link href="'. base_url() . 'assets/photoswipe/dist/default-skin/default-skin.css"; rel="stylesheet">';
		    	$data['other_scripts'][] = '<script src="'. base_url() . 'assets/photoswipe/dist/photoswipe.min.js"></script>';
		    	$data['other_scripts'][] = '<script src="'. base_url() . 'assets/photoswipe/dist/photoswipe-ui-default.min.js"></script>';
				break;

			case 'display':
				$categoryid = $this->uri->segment(3, 0);

				if(is_numeric($categoryid)) {
					$data['galleries'] 	= $this->site->get_galleries_by_category($categoryid);
					$data['title'] 		= (!empty($data['galleries'])) ? $data['galleries']['name'] : '';
				}
				else {
					redirect(base_url() . 'gallery');
				}
				break;
			
			default:
				redirect(base_url() . 'gallery');
				break;
		}

		$this->load->view('frontend/common/header', $data);


		$menu_data['is_member']		= FALSE;
		$data['sidemenu'] 			= $this->load->view('frontend/common/sidemenu', $menu_data, TRUE);  

		//$this->load->view('frontend/common/banner', $banner_data);
		$data['slug'] = 'gallery';
		$this->load->view('frontend/gallery', $data);
		$this->load->view('frontend/common/footer');
	}
}
