<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Newsletter extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');

		$this->load->library('PPMSystemLib');

		$this->load->model('frontend/Newslettersmodel', 'newsletter');

		$this->is_newsletter 		= $this->newsletter->is_logged_in();
		$this->is_ppmsystem 		= (isset($_SESSION['member_logged_in']) && $_SESSION['member_logged_in']) ? TRUE : FALSE;
		$this->is_onlinetraining 	= (isset($_SESSION['onlinetraining_logged_in']) && $_SESSION['onlinetraining_logged_in']) ? TRUE : FALSE;
	}

	public function index()
	{

		//username: robinavista@bigpond.com
		//pass: sioban72

		//rachelle:patrick

		$this->action = $this->uri->segment(2, '');

		$data['page'] = (!empty($this->action)) ? $this->action : 'home';		

		if($this->is_newsletter) {

			switch ($this->action) {
				case '':

	                if($this->ppmsystemlib->get_days_left(date('Y-m-d h:i:s', time(PPM_TIMEZONE)), $_SESSION['m_nl_membershipExpires']) === 0) {
	                	redirect(base_url() . 'newsletter/renew');
	                }

					$data['title'] = 'Landlord Newsletter';
					$data['enews'] = $this->newsletter->get_enews();
					$data['total'] = (!empty($data['enews'])) ? count($data['enews']) : 0;

					if($_SESSION['m_nl_id'] === -1) {
						$subscribe = '';
						$days_left = 0;
					}
					else {

						$data['subscribe_link'] = base_url() . 'ppmsystem/ppm/newsletter/step1/' . $this->session->m_nl_id . '/' . $this->session->m_nl_memberIdConfirm;
						$data['days_left'] = $this->ppmsystemlib->get_days_left(date('Y-m-d h:i:s', time(PPM_TIMEZONE)), $_SESSION['m_nl_membershipExpires']);
					}


					if($this->session->flashdata('error')) {
						$data['error'] = $this->session->flashdata('error');
					}

					if($this->session->flashdata('file_download')) {
						$data['file_download'] = $this->session->flashdata('file_download');
					}
					break;

				case 'signup':
					$data['title'] = 'LANDLORD NEWSLETTER SIGN UP';
					break;

				case 'renew':
					$data['title'] = 'Landlord Newsletter';
					break;					
				
				default:
					break;
			}		
		}
		else {
			switch ($this->action) {
				case '':
				case 'signup':
					$data['page'] 	= 'signup';	
					$data['title'] 	= 'LANDLORD NEWSLETTER SIGN UP';
					break;

				case 'renew':
					$data['title'] = 'Landlord Newsletter';
					break;					
				
				default:
					break;
			}			
		}

		$this->load->view('frontend/common/header', $data);

		$banner_data['placement'] = 1; 

		$this->load->view('frontend/common/banner', $banner_data);

		$this->load->view('frontend/newsletter', $data);
		$this->load->view('frontend/common/footer');	

	}

	public function login()
	{
		if(!$this->is_newsletter && !$this->is_ppmsystem) {
			$data['title'] 	= 'Newsletter';
			$data['page'] 	= 'newsletter';
			$data['error'] 	= $this->session->flashdata('error');

			$this->load->view('frontend/common/header', $data);
			$menu_data['is_member']	= FALSE;
			$data['sidemenu'] 	= $this->load->view('frontend/common/sidemenu', $menu_data, TRUE); 
			$this->load->view('frontend/login', $data);
			$this->load->view('frontend/common/footer');
		}
		else {
			if($this->is_ppmsystem) {
				redirect(base_url() . 'members/newsletters');
			}
			else {
				redirect(base_url() . 'newsletter');
			}
		}
	}

	public function check()
	{
		$post = $this->input->post();

		if(!empty($post)) {
            $username = $this->input->post('username');
            $password = $this->input->post('password');

            $result = $this->newsletter->login($username, $password);

            if(count($result)>0) {
                $data = array(
                        'username'        			=> $username,
                        'm_nl_id'         			=> intval($result[0]['id']),
                        'm_nl_membershipExpires'  	=> $result[0]['membershipExpires'],
                        'newsletter_logged_in'		=> TRUE,
                        'm_nl_memberIdConfirm'		=> md5($password . SECKEY)
                );

                $this->session->set_userdata($data);

                if($this->is_onlinetraining) {
		            $data = array(
		                        'ot_id',
		                        'ot_membershipExpires',
		                        'onlinetraining_logged_in',
		                        'ot_memberIdConfirm'
		                    );

		            $this->session->unset_userdata($data);                	
                }

                if($this->ppmsystemlib->get_days_left(date('Y-m-d h:i:s', time(PPM_TIMEZONE)), $_SESSION['m_nl_membershipExpires']) === 0) {
                	redirect(base_url() . 'newsletter/renew');
                }
                else {
                	redirect(base_url() . 'newsletter');
                }
            }
			else {
				$this->session->set_flashdata('error', 'Sorry that username/password combination does not exist. Please try again.');
				redirect(base_url() . 'newsletter-login');
			}

		}
		else {
			redirect(base_url() . 'newsletter-login');
		}
	}

	public function logout()
    {
        if(isset($_SESSION['newsletter_logged_in']) && $_SESSION['newsletter_logged_in']) {
            $data = array(
                    'username',
                    'm_nl_id',
                    'm_nl_membershipExpires',
                    'newsletter_logged_in',
                    'm_nl_memberIdConfirm'
            );

		    $this->session->unset_userdata($data);
            //session_destroy();
        }

        redirect(base_url() . 'home');
    }
}
