<?php 
    if(!$is_pagination) { 
        render_featured_img($slug);
?>

    <style>
    .menu-active {
        font-weight: bolder;
        color: #A6A8AC !important;
    }
    </style>

    <div class="container">
        <div class="row">
            <div class="col-md-3 left-column">
            </div>
            <div id="print-page" class="col-md-9 right-column">
                <h1><?php echo $title; ?></h1>
                <?php if(isset($page_title)) { ?><h2><?php echo $page_title; ?></h2><?php } ?>

                <?php if($page === 'showarticle') { ?>
                    <?php if(!empty($article)) { ?>
                        
                                
                        <?php foreach ($article as $key => $value) {
                            
                                $imgPath = '';
                                $content = '';

                                if(!empty($value['d1']) && $this->ppmsystemlib->get_file_type_by_ext($value['d1']) === 'img'){
                                    $imgPath = $value['d1'];
                                    $content = $value['c1'];
                                }

                                if(!empty($value['d2']) && $this->ppmsystemlib->get_file_type_by_ext($value['d2']) === 'img'){
                                    $imgPath = $value['d2'];
                                    $content = $value['c2'];
                                }

                                if(!empty($value['d3']) && $this->ppmsystemlib->get_file_type_by_ext($value['d3']) === 'img'){
                                    $imgPath = $value['d3'];
                                    $content = $value['c3'];
                                }

                                if(!empty($value['d4']) && $this->ppmsystemlib->get_file_type_by_ext($value['d4']) === 'img'){
                                    $imgPath = $value['d4'];
                                    $content = $value['c4'];
                                }

                                if(!empty($value['d5']) && $this->ppmsystemlib->get_file_type_by_ext($value['d5']) === 'img'){
                                    $imgPath = $value['d5'];
                                    $content = $value['c5'];
                                }
                            ?>
                            <div class="row">
                                <?php if(!empty($imgPath)) { ?>
                                    <div class="col-md-9">
                                <?php }else{ ?>
                                    <div class="col-md-12">
                                <?php } ?>
                                        <article>
                                    <?php if(!empty($value['code'])) { ?> <b>Article Code:</b> <?php echo $value['code']; ?> <br> <?php } ?>
                                    <?php if(!empty($value['location'])) { ?> <b>Location:</b> <?php echo $value['location']; ?> <br> <?php } ?>
                                    <i><?php echo $value['shortdesc']; ?></i><br><br class="space">
                                    <?php echo $value['body']; ?><br><br>
                                    <?php if(!empty($value['source'])) { ?> <b>Author:</b> <?php echo $value['source']; ?><br><br><?php } ?>
                                    <?php if(!empty($related_downloads)) { ?>
                                        <span class="body"><b>Related Downloads</b><br><br></span>
                                        <?php foreach ($related_downloads as $k => $v) { ?>
                                        <span class="body"><?php echo $this->ppmsystemlib->check_date_time($v["dateposted"]); ?>&nbsp;<b><?php echo $v["name"]; ?></b><br><?php echo $v["shortdesc"]; ?> <a href="<?php echo base_url(); ?>articles/showdownload/<?php echo $v["id"]; ?>" >more...</a><br><br></span>    
                                        <?php } ?>
                                    <?php } ?>
                                        </article>

                                    </div>

                            <?php if(!empty($imgPath)) { ?>
                                <div class="col-md-3">
                                    <img src="<?php echo base_url(); ?>assets/uploads/files/<?php echo $imgPath; ?>" width="200"><br><br class="space"><span class="body"><?php echo $content; ?></span><br><br>
                                </div>
                            <?php } ?>
                            </div>
                        <?php } ?>
                    <?php } else { ?>
                    <p>Sorry that article has been removed from our system</p>
                    <?php } ?>
                <?php 
                }//END PAGE showarticle
                ?>

                <?php if($page === 'home') { ?>

                <p>Welcome to our property management news and articles.</p>
                <p>Keeping you up-to-date on legislation changes, cutting edge property management processes, stories and bright ideas.</p>

                <p><a href="<?php echo base_url(); ?>ppmsystem/ppm/member/pricing">Click here</a> to become a member and receive exclusive access to our member's area, which has an extensive archive library with 100's of articles.</p>

                <br>

                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#news-section">News</a></li>
                    <li><a data-toggle="tab" href="#articles-section">Articles</a></li>
                </ul>

                <div class="tab-content">
                    <div id="news-section" class="tab-pane fade in active">
                        <p>&nbsp;</p>
                        <p>Number of news returned:  <?php echo $news_start_row; ?> to <?php echo $news_end_row; ?> records of <?php echo $news_total; ?> news</p>
                        <p>&nbsp;</p>

                        <?php if(!empty($news)) { ?>
                            <div class="row">
                            <?php foreach ($news as $key => $value) { ?>
                            
                                <div class="col-md-4">
                                <?php if(!empty($value['d1'])) {?>
                                        <img src="<?php echo base_url(); ?>assets/uploads/files/<?php echo $value['d1']?>" class="img-responsive" />
                                <?php } else { ?>
                                <p><img src="<?php echo base_url(); ?>assets/images/front/broken-image.gif" class="img-responsive" /></p>
                                <?php } ?>
                                <p><?php echo $this->ppmsystemlib->check_date_time($value['mydate']);?></p>
                                <p><b><?php echo $value['name']; ?></b> <?php echo $value['shortdesc']; ?> <a href="<?php echo base_url(); ?>articles/showarticle/<?php echo $value['id']; ?>" target="_blank" >more...</a></p>
                                </div> 

                            <?php } ?>
                            </div>

                            <?php
                                if(!empty($news_pagination) && intval($news_total) > 12) {
                                    echo '<div class="text-center">'.$news_pagination.'</div>';
                                }
                            ?>

                        <?php } ?>

                    </div>
                    <div id="articles-section" class="tab-pane fade">
                        <p>&nbsp;</p>
                        <p>Number of articles returned:  <?php echo $articles_start_row; ?> to <?php echo $articles_end_row; ?> records of <?php echo $articles_total; ?> articles</p>
                        <p>&nbsp;</p>

                        <?php if(!empty($articles)) { ?>
                            <div class="row">
                            <?php foreach ($articles as $key => $value) { ?>
                            
                                <div class="col-md-4">
                                <?php if(!empty($value['d1'])) {?>
                                        <img src="<?php echo base_url(); ?>assets/uploads/files/<?php echo $value['d1']?>" class="img-responsive" />
                                <?php } else { ?>
                                <p><img src="<?php echo base_url(); ?>assets/images/front/broken-image.gif" class="img-responsive" /></p>
                                <?php } ?>
                                <p><?php echo $this->ppmsystemlib->check_date_time($value['mydate']);?></p>
                                <p><b><?php echo $value['name']; ?></b> <?php echo $value['shortdesc']; ?> <a href="<?php echo base_url(); ?>articles/showarticle/<?php echo $value['id']; ?>" target="_blank" >more...</a></p>
                                </div> 

                            <?php } ?>
                            </div>

                            <?php
                                if(!empty($articles_pagination) && intval($articles_total) > 12) {
                                    echo '<div class="text-center">'.$articles_pagination.'</div>';
                                }
                            ?>

                        <?php } ?>
                    </div>
                </div>

                <?php } ?>

            </div>
        </div>    
    </div>

    <?php if($page === 'home') { ?>

    <script type="text/javascript">
        
    $(document).ready(function(){

        var articles        = {};
        var newsHolder      = $('#news-section');
        var articlesHolder  = $('#articles-section');

        articles.init = function() {
            $.each($('ul.pagination'), function(k,v){
                articles.pagination(v);
            });
        };

        articles.pagination = function(ul){
            var linkCollect = $(ul).find('a');

            $.each(linkCollect, function(k,v){
                $(v).attr({'data-url':$(v).attr('href'), 'href':'javascript:;'});
                $(v).on('click', function(){articles.getPage($(this))});
            });
        };

        articles.getPage = function(btn){
            var url = $(btn).data('url');

            if(url != '' && url != 'javascript:;') {
                var request = $.ajax({
                    url: url,
                    method: "GET"
                });

                request.done(function( data ) {
                    if(data!=null && data!='') {
                        var div = $('<div></div>');
                        var ul = null;

                        div.html(data);

                        var typeVal = div.find('.page-type').val();

                        if(typeVal == 'news') {
                            newsHolder.html(div.html());
                            ul = newsHolder.find('ul.pagination');
                        }
                        else if(typeVal == 'articles') {
                            articlesHolder.html(div.html());
                            ul = articlesHolder.find('ul.pagination');
                        }

                        articles.pagination(ul);

                        setTimeout(function(){
                            document.querySelector('.right-column').scrollIntoView({ 
                                behavior: 'smooth' 
                            });
                        }, 50);

                        data = null;
                    }
                    
                });
            }
        }

        articles.init();
    });

    </script>

    <?php } ?>

<?php } else { ?>
    <input class="page-type" type="hidden" value="<?php echo $page; ?>" />
    <p>&nbsp;</p>
    <p>Number of <?php echo $page; ?> returned:  <?php echo $start_row; ?> to <?php echo $end_row; ?> records of <?php echo $total; ?> <?php echo $page; ?></p>
    <p>&nbsp;</p>

    <?php if(!empty($articles)) { ?>
        <div class="row">
        <?php foreach ($articles as $key => $value) { ?>
        
            <div class="col-md-4">
            <?php if(!empty($value['d1'])) {?>
                    <img src="<?php echo base_url(); ?>assets/uploads/files/<?php echo $value['d1']?>" class="img-responsive" />
            <?php } else { ?>
            <p><img src="<?php echo base_url(); ?>assets/images/front/broken-image.gif" class="img-responsive" /></p>
            <?php } ?>
            <p><?php echo $this->ppmsystemlib->check_date_time($value['mydate']);?></p>
            <p><b><?php echo $value['name']; ?></b> <?php echo $value['shortdesc']; ?> <a href="<?php echo base_url(); ?>articles/showarticle/<?php echo $value['id']; ?>" target="_blank" >more...</a></p>
            </div> 

        <?php } ?>
        </div>

        <?php
            if(!empty($pagination) && intval($total) > 12) {
                echo '<div class="text-center">'.$pagination.'</div>';
            }
        ?>

    <?php } ?>

<?php } ?>
