<style type="text/css">

.menu-active {
    font-weight: bolder;
    color: #A6A8AC !important;
}

.single-table {
  background: #f4f4f4;
  padding: 0px 0px 50px;
  border-bottom: 2px solid #414142;
  margin: 20px 0 20px;
}
.plan-header {
  background: #47c7eb;
  padding: 20px 0px;
  color: #fff;
  margin-bottom: 20px;
  border-bottom: 2px solid #414142;
}

/*--- Premium Section ---*/
.premium-ribbon {
    position: absolute;
    top: 20px;
    right: 15px;
}
.premium-btn {
  background: #f06724 !important;
}
.premium-btn:hover {
  border-color: #f06724 !important;
  color: #f06724 !important;
}
.premium-features li i {
  color: #f06724 !important;
}
/*--- End of Premium Section ---*/


.header-separator {
  width: 60%;
  height: 1px;
  background: #fff;
  margin: 0 auto;
}
.plan-price {
    font-size: 80px;
    margin-bottom: -5px;
    font-weight: 600;
}
.plan-price sup {
  font-size: 30px;
  top: -35px !important;
}
.per-month {
  color: #414142;
}
.plan-features {
  padding: 0px;
  padding-left: 20px;
  margin-bottom: 50px;
  text-align: left;
}
.plan-features li {
list-style: none;
  padding: 20px 0px 20px;
  font-size: 18px;
  border-bottom: 1px dashed #e1e1e1;
}
.plan-features li i {
  color: #47c7eb;
}
.plan-submit {
  background: #47c7eb;
  color: #fff !important;
  padding: 10px 40px;
  font-size: 21px;
  width: 200px !important;
  text-transform: uppercase;
}
.plan-submit:hover {
  background: transparent;
  text-decoration: none;
  border: 2px solid #47c7eb;
  color: #47c7eb !important;
}

.plan-submit.premium-btn:hover {
    color: #fff !important;
}

.plan-features .fa {
    font-size: 20px;
}

</style>

<div class="row">
    <div class="col-md-6 col-sm-6 col-xs-12 table-container">
        <div class="single-table text-center">
            <div class="plan-header">
                <h3><strong>PLATINUM</strong> PLAN</h3>
                <div class="header-separator"></div>
                <h4 class="plan-price"><sup><i class="fa fa-dollar"></i></sup><?php echo PLATINUM_MONTHLY_AMOUNT; ?></h4>
                <p class="per-month">PER MONTH</p>
            </div>

            <ul class="plan-features">
                <li>
                    <small>Exclusive to clients who have implemented the PPMsystem&reg;.</small>
                    <small>From $<?php echo PLATINUM_MONTHLY_AMOUNT; ?>.00 per month (minimum 12 month term)</small>
                </li>
                <li><i class="fa fa-check-circle"></i> <small>All general membership benefits</small></li>
                <li><i class="fa fa-check-circle"></i> <small>50% Discount on training, consulting & conference events</small></li>
                <li><i class="fa fa-check-circle"></i> <small>FREE PPMsystem LIVE On-Line Training tutorials</small></li>
                <li><i class="fa fa-check-circle"></i> <small>FREE PPMsystem upgrades via our members portal for easy access</small></li>
            </ul>
            <a href="<?php echo base_url(); ?>ppmsystem/ppm/member/step1?plan=platinum" class="plan-submit">Get Platinum</a>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12 table-container">
        <div class="single-table text-center">
            <div class="plan-header">
                <img class="premium-ribbon" src="<?php echo base_url(); ?>assets/images/front/premium.png">
                <h3><strong>GOLD</strong> PLAN</h3>
                <div class="header-separator"></div>
                <h4 class="plan-price"><sup><i class="fa fa-dollar"></i></sup><?php echo GOLD_MONTHLY_AMOUNT; ?></h4>
                <p class="per-month">PER MONTH</p>
            </div>

            <ul class="plan-features premium-features">
                <li>
                    <small>Exclusive to clients who have implemented the PPMsystem&reg;.</small>
                    <small>From $<?php echo GOLD_MONTHLY_AMOUNT; ?>.00 per month (minimum 12 month term)</small>
                </li>                
                <li><i class="fa fa-check-circle"></i> <small>All general membership benefits</small></li>
                <li><i class="fa fa-check-circle"></i> <small>25% Discount on training, consulting & conference events</small></li>
                <li><i class="fa fa-check-circle"></i> <small>FREE PPMsystem LIVE On-Line Training tutorials</small></li>
                <li><i class="fa fa-check-circle"></i> <small>FREE PPMsystem upgrades via our members portal for easy access</small></li>
            </ul>
            <a href="<?php echo base_url(); ?>ppmsystem/ppm/member/step1?plan=gold" class="plan-submit premium-btn">Go Gold</a>
        </div>
    </div>
</div>