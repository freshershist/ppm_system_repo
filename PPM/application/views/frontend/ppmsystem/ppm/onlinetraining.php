<?php 
    
    $hasPost = (isset($post) && !empty($post)) ? TRUE : FALSE;
    $isRenewal = (isset($isRenewal)) ? TRUE: FALSE;

?>

<div class="container">
    <div class="row">
        <div class="col-md-3 left-column">
        
            <div id="replacement-sub-nav">

                <p><a href="<?php echo base_url(); ?>onlinetraining">onlinetraining home</a></p>
            <?php if(isset($this->is_onlinetraining) && $this->is_onlinetraining) { ?>
                <p><a href="<?php echo base_url(); ?>onlinetraining-logout">onlinetraining logout</a></p>
            <?php } ?>
            
            <?php if(isset($this->is_ppmsystem) && $this->is_ppmsystem) { ?>

                <p><a href="<?php echo base_url(); ?>members">members home</a></p>
                <p><a href="<?php echo base_url(); ?>ppmsystem-logout">members logout</a></p>                

            <?php } ?>

            </div>          
        </div>
        <div class="col-md-9 right-column">

            <h1><?php echo $title; ?></h1>

            <?php if($stage === 'step1') { ?>
                <p><b>Step 1 of 3</b></p>
                <div class="alert alert-dismissible alert-info">
                <?php if($isRenewal) { ?>
                    <h2>Online Training Subscription Renewal</h2>  
                <?php } else { ?>
                    <?php if(!$this->is_ppmsystem) { ?>
                    <h2>Are you a PPMsystem Member? </h2>  
                    <p>If yes, please login via the member's area to receive your member on-line training discount.</p><p><a href="<?php echo base_url() . 'members'; ?>">Click here</a> to access the member's area</p>
                    <?php } ?>
                <?php } ?>
                    <h2>ANNUAL INVESTMENT</h2>

                    <?php 
                        if(!empty($annual_investment)) {
                            echo '<h4><i class="fa fa-dollar"></i>' . $annual_investment . '</h4>';
                        }
                        else {
                    ?>
                    <h4><i class="fa fa-dollar"></i><?php echo ONLINETRAINING_GENERAL_AMOUNT; ?> General Public</h4>
                    <h4><i class="fa fa-dollar"></i><?php echo ONLINETRAINING_PLATINUM_AMOUNT; ?> Platinum Members</h4>
                    <h4><i class="fa fa-dollar"></i><?php echo ONLINETRAINING_GOLD_AMOUNT; ?> Gold Members</h4>
                    <h4><i class="fa fa-dollar"></i><?php echo ONLINETRAINING_SILVER_AMOUNT; ?> Silver Members</h4>
                    <?php } ?>
                </div>
                             
                
                <form action="<?php echo base_url(). 'ppmsystem/ppm/onlinetraining/step1'; ?>" method="POST" class="form-horizontal form-label-left" >
                    <input type="hidden" name="linkedMemberId" value="<?php echo ($hasPost) ? $post['linkedMemberId'] : '' ; ?>" /> 
                    <input type="hidden" name="memberId" value="<?php echo ($hasPost) ? $post['memberId'] : '' ; ?>" /> 
                    <input type="hidden" name="memberIdConfirm" value="<?php echo ($hasPost) ? $post['memberIdConfirm'] : '' ; ?>" />   

                    <div class="form-group">
                        <span for="Company" class="col-sm-12 col-md-3 control-label text-right">Company: <i class="fa fa-asterisk text-danger"></i></span>
                        <div class="col-sm-12 col-md-5">
                            <input type="text" name="company" class="form-control" value="<?php echo ($hasPost) ? $post['company'] : '' ; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <span for="contactName" class="col-sm-12 col-md-3 control-label text-right">Central Contact Name: <i class="fa fa-asterisk text-danger"></i></span>
                        <div class="col-sm-12 col-md-5">
                            <input type="text" name="contactName" class="form-control" value="<?php echo ($hasPost) ? $post['contactName'] : '' ; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <span for="contactNumber" class="col-sm-12 col-md-3 control-label text-right">Contact Number: <i class="fa fa-asterisk text-danger"></i></span>
                        <div class="col-sm-12 col-md-5">
                            <input type="text" name="contactNumber" class="form-control" value="<?php echo ($hasPost) ? $post['contactNumber'] : '' ; ?>">
                        </div>
                    </div>


                    <div class="form-group">
                        <span for="address" class="col-sm-12 col-md-3 control-label text-right">Postal Address (Line 1): <i class="fa fa-asterisk text-danger"></i></span>
                        <div class="col-sm-12 col-md-5">
                            <input type="text" name="address" class="form-control" value="<?php echo ($hasPost) ? $post['address'] : '' ; ?>">
                        </div>
                    </div>


                    <div class="form-group">
                        <span for="address2" class="col-sm-12 col-md-3 control-label text-right">Postal Address (Line 2): </span>
                        <div class="col-sm-12 col-md-5">
                            <input type="text" name="address2" class="form-control" value="<?php echo ($hasPost) ? $post['address2'] : '' ; ?>">
                        </div>
                    </div>


                    <div class="form-group">
                        <span for="suburb" class="col-sm-12 col-md-3 control-label text-right">Suburb: <i class="fa fa-asterisk text-danger"></i></span>
                        <div class="col-sm-12 col-md-5">
                            <input type="text" name="suburb" class="form-control" value="<?php echo ($hasPost) ? $post['suburb'] : '' ; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <span for="postcode" class="col-sm-12 col-md-3 control-label text-right">Postcode: <i class="fa fa-asterisk text-danger"></i></span>
                        <div class="col-sm-12 col-md-5">
                            <input type="text" name="postcode" class="form-control" value="<?php echo ($hasPost) ? $post['postcode'] : '' ; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <span for="postcode" class="col-sm-12 col-md-3 control-label text-right">State/Country: <i class="fa fa-asterisk text-danger"></i></span>
                        <div class="col-sm-12 col-md-5">
                            <?php 
                                $state = ($hasPost) ? $post['state'] : '' ;
                                echo form_dropdown('state', $stateArrayOther, $state, 'id="state" class="form-control"'); 
                            ?>
                        </div>
                    </div> 

                    <div class="form-group">
                        <span for="postcode" class="col-sm-12 col-md-3 control-label text-right">Email Address: <i class="fa fa-asterisk text-danger"></i></span>
                        <div class="col-sm-12 col-md-5">
                            <input type="email" name="emailAddress" class="form-control" value="<?php echo ($hasPost) ? $post['emailAddress'] : '' ; ?>">
                        </div>
                    </div> 

                    <div class="form-group">
                        <span for="postcode" class="col-sm-12 col-md-3 control-label text-right">Confirm Email Address: <i class="fa fa-asterisk text-danger"></i></span>
                        <div class="col-sm-12 col-md-5">
                            <input type="email" name="confirmemail" class="form-control" value="<?php echo ($hasPost) ? $post['confirmemail'] : '' ; ?>">
                        </div>
                    </div> 
                    <?php if(!$this->is_ppmsystem) { ?>

                    <div class="form-group">
                        <span for="postcode" class="col-sm-12 col-md-3 control-label text-right">Username: <i class="fa fa-asterisk text-danger"></i></span>
                        <div class="col-sm-12 col-md-5">
                            <input type="text" name="username" class="form-control" value="<?php echo ($hasPost) ? $post['username'] : '' ; ?>" />
                        </div>
                    </div> 

                    <div class="form-group">
                        <span for="postcode" class="col-sm-12 col-md-3 control-label text-right">Password: <i class="fa fa-asterisk text-danger"></i></span>
                        <div class="col-sm-12 col-md-5">
                            <input type="text" name="password" class="form-control" value="<?php echo ($hasPost) ? $post['password'] : '' ; ?>">
                        </div>
                    </div>

                    <?php } else { ?>

                    <div class="form-group">
                        <input type="hidden" name="username" class="form-control" value="<?php echo ($hasPost) ? $post['username'] : '' ; ?>">
                        <input type="hidden" name="password" class="form-control" value="<?php echo ($hasPost) ? $post['password'] : '' ; ?>">
                    </div>    

                    <?php } ?>                    

                    <div class="form-group">
                        <span for="postcode" class="col-sm-12 col-md-3 control-label text-right">&nbsp;</span>
                        <div class="col-sm-12 col-md-5">
                            <input type="submit" value="next step" class="btn btn-default">
                        </div>
                    </div>

                                 
                </form>

            <?php } ?>

            <?php if($stage === 'step2') { ?>
                <p><b>Step 2 of 3</b></p>

                <h2>Your Payment Details</h2><p>We accept</span><br><img src="<?php echo base_url() . 'assets/images/visa-card.png'; ?>"></p>

                <form id="payForm" action="<?php echo base_url(). 'ppmsystem/ppm/onlinetraining/step2'; ?>" method="POST" class="form-horizontal form-label-left" >
                    <input type="hidden" name="linkedMemberId" value="<?php echo ($hasPost) ? $post['linkedMemberId'] : '' ; ?>" />
                    <input type="hidden" name="memberId" value="<?php echo ($hasPost) ? $post['memberId'] : '' ; ?>" />
                    <input type="hidden" name="memberIdConfirm" value="<?php echo ($hasPost) ? $post['memberIdConfirm'] : '' ; ?>" />  
                    <input type="hidden" name="company" value="<?php echo ($hasPost) ? $post['company'] : '' ; ?>" />
                    <input type="hidden" name="contactName" value="<?php echo ($hasPost) ? $post['contactName'] : '' ; ?>" />
                    <input type="hidden" name="contactNumber" value="<?php echo ($hasPost) ? $post['contactNumber'] : '' ; ?>" />
                    <input type="hidden" name="address" value="<?php echo ($hasPost) ? $post['address'] : '' ; ?>" />
                    <input type="hidden" name="address2" value="<?php echo ($hasPost) ? $post['address2'] : '' ; ?>" />
                    <input type="hidden" name="suburb" value="<?php echo ($hasPost) ? $post['suburb'] : '' ; ?>" />
                    <input type="hidden" name="postcode" value="<?php echo ($hasPost) ? $post['postcode'] : '' ; ?>" />
                    <input type="hidden" name="state" value="<?php echo ($hasPost) ? $post['state'] : '' ; ?>" />
                    <input type="hidden" name="emailAddress" value="<?php echo ($hasPost) ? $post['emailAddress'] : '' ; ?>" />
                    <input type="hidden" name="confirmemail" value="<?php echo ($hasPost) ? $post['confirmemail'] : '' ; ?>" />
                    <input type="hidden" name="username" value="<?php echo ($hasPost) ? $post['username'] : '' ; ?>" />
                    <input type="hidden" name="password" value="<?php echo ($hasPost) ? $post['password'] : '' ; ?>" />

                    <div class="form-group">
                        <span for="Card Type" class="col-sm-12 col-md-3 control-label text-right">Amount: </span>
                        <div class="col-sm-12 col-md-5">
                            <div class="alert alert-dismissible alert-success">
                                <h4><i class="fa fa-dollar"></i><?php echo $amount; ?><?php if(intval($amount) !== intval(ONLINETRAINING_GENERAL_AMOUNT)) { ?><strike class="text-danger pull-right"><i class="fa fa-dollar"></i><?php echo ONLINETRAINING_GENERAL_AMOUNT; ?></strike> <?php } ?></h4>

                                <?php if(isset($this->is_ppmsystem) && !$this->is_ppmsystem && empty($this->linkedMemberId)) { ?>                           
                                    <p>(To receive PPMsystem member discounts of up to 50%<br />you must subscribe via the member's logon area)</p>
                                <?php } ?>
                            </div>
                        </div>
                    </div> 

                    <div class="form-group">
                        <span for="Card Type" class="col-sm-12 col-md-3 control-label text-right">Card Type: <i class="fa fa-asterisk text-danger"></i></span>
                        <div class="col-sm-12 col-md-5">
                            <?php 
                                $cardType = ($hasPost && isset($post['cardType'])) ? $post['cardType'] : '' ;
                                echo form_dropdown('cardType', $cardTypeArray, $cardType, 'id="cardType" class="form-control"'); 
                            ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <span for="postcode" class="col-sm-12 col-md-3 control-label text-right">Credit Card Number: <i class="fa fa-asterisk text-danger"></i></span>
                        <div class="col-sm-12 col-md-5">
                            <input id="cardnumber" type="text" name="creditcard" class="form-control" value="<?php echo ($hasPost && isset($post['creditcard'])) ? $post['creditcard'] : '' ; ?>" maxlength="30">
                        </div>
                    </div>

                    <div class="form-group">
                        <span for="postcode" class="col-sm-12 col-md-3 control-label text-right">Name on card: <i class="fa fa-asterisk text-danger"></i></span>
                        <div class="col-sm-12 col-md-5">
                            <input type="text" name="nameoncard" class="form-control" value="<?php echo ($hasPost && isset($post['nameoncard'])) ? $post['nameoncard'] : '' ; ?>" maxlength="50">
                        </div>
                    </div>

                    <div class="form-group">
                        <span for="postcode" class="col-sm-12 col-md-3 control-label text-right">Expiry date: <i class="fa fa-asterisk text-danger"></i></span>
                        <div class="col-sm-12 col-md-5">
                            <input type="text" name="expiry" class="form-control" value="<?php echo ($hasPost && isset($post['expiry'])) ? $post['expiry'] : '' ; ?>" placeholder="(MMYY i.e 0610)" maxlength="4" />
                        </div>
                    </div>

                    <div class="form-group">
                        <span for="postcode" class="col-sm-12 col-md-3 control-label text-right">CCV: <i class="fa fa-asterisk text-danger"></i></span>
                        <div class="col-sm-12 col-md-5">
                            <input id="cardcvv" type="text" name="cvv" class="form-control" value="<?php echo ($hasPost && isset($post['cvv'])) ? $post['cvv'] : '' ; ?>" maxlength="4" placeholder="(This is the 3 digit code on the back of your card)" />
                        </div>
                    </div>                    

                    <div class="form-group">
                        <span for="postcode" class="col-sm-12 col-md-3 control-label text-right">&nbsp;</span>
                        <div class="col-sm-12 col-md-5">
                            <input id="payButton" type="button" value="make payment" class="btn btn-default" >
                        </div>
                    </div>                    

                </form>

                <script src="https://secure.ewaypayments.com/scripts/eCrypt.js"></script>
                <script>
                    $(document).ready(function(){
                        var payBtn      = $('#payButton');
                        var cardnumber  = $('#cardnumber');
                        var cvv         = $('#cardcvv');
                        var form        = $('#payForm');

                        payBtn.on('click', function(){
                            
                            if ((!(cardnumber.val().substring(0,9) == 'eCrypted:')) && (cardnumber.val().length > 0)) {     
                                actualcardnumber = cardnumber.val();
                                actualcardnumber = actualcardnumber.replace(/-/g,'');       
                                cardnumber.hide();  
                                cardnumber.val(eCrypt.encryptValue(actualcardnumber,'<?php echo EWAYCLIENTSIDEENCRYPTION; ?>'));
                            }
                            
                            if ((!(cvv.val().substring(0,9) == 'eCrypted:')) && (cvv.val().length > 0)) {   
                                cvv.hide();
                                cvv.val(eCrypt.encryptValue(cvv.val(),'<?php echo EWAYCLIENTSIDEENCRYPTION; ?>'));
                            }

                            set_cc_format(false);

                            form.submit();
                        });

                        function update_cc(field) {
                            var field = $('#' + field);
                            field.val('');  
                            field.next('span').remove();    
                            field.show();
                        }

                        function set_cc_format(reset) {
                            
                            if (reset) {
                                update_cc('cardcvv');
                                update_cc('cardnumber');
                            }
                                
                            if (cvv.val().substring(0,9) == 'eCrypted:') {
                                var span = $('<span>xxxx <a href="javascript:;">Change</a></span>');
                                
                                span.on('click', function(){update_cc('cardcvv');});

                                span.insertAfter(cvv);
                                cvv.hide();
                            }


                            if (cardnumber.val().substring(0,9) == 'eCrypted:') {

                                var span = $('<span>xxxx-xxxx-xxxx-xxxx <a href="javascript:;">Change</a></span>');
                                
                                span.on('click', function(){update_cc('cardnumber');});

                                span.insertAfter(cardnumber);
                                cardnumber.hide();

                                return true;
                            }
                        }

                        set_cc_format(false);
                    });                 
                </script>

                <div class="alert alert-dismissible alert-info">
                    <span class="body"><b>Please note:</b> Your credit card details are not stored in our system.  Once payment has been accepted we have no record of your card details.</span> 
                </div>

            <?php } ?>

            <?php if($stage === 'step3') { ?>
                <p>Thank you <strong><?php echo $contactName; ?></strong>,<br><br>
                Your PPM Group On-Line Training subscription has now been processed and a tax invoice has been emailed to you<br><br>
                Click here to access the <a href="<?php echo base_url() . 'onlinetraining-login'; ?>">Online Training</a></p>
            <?php } ?>
        </div>
    </div>
</div>

<?php if(isset($error) && !empty($error)) { ?>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/sweetalert/dist/sweetalert.min.js"></script>

<script>
    $(document).ready(function(){
        var error = <?php echo $error; ?>;

        if(Object.keys(error).length > 0) {
            var errMsg = '';

            $.each(error, function(k, v){
                errMsg += '* ' + v + '\n';
            });

            new PNotify({
                title: 'Invalid or Required!',
                text: errMsg,
                type: 'error',
                styling: 'bootstrap3'
            });

            swal({title:"Failed!",text:"Some fields are required (*) or have invalid values",icon:'error',button:false, timer:3000});
            
        }

    });
</script>

<?php } ?>    