<?php

$surCharge = 0.02;

?>

<div class="container">
    <div class="row">
         <div class="col-md-3 left-column">
                <div class="sub-nav">
                    <p></p>
                </div>
            </div>
            <div class="col-md-9 right-column">
			
			<!-- start main content -->
				


		

				<h1>Your Cart</h1>
				
				<h2>You have <?php echo count($cart); ?> item<?php echo (count($cart)>1)? 's':''; ?> in your cart<br><br class="space"><small>You may navigate around the website and your chosen items in the shopping cart will remain until you checkout.</small></h2>
				
			<form action="" method="post" name="dataform">
			<?php 
			// echo '<pre>';
			// print_r($cart);
			// echo '</pre>';
			?>
			<table border="0" cellspacing="5" cellpadding="5" bgcolor="#ffffff" class="table"><tbody><tr>
				<td bgcolor="#47c7ec" align="middle"><p class="white">QTY/Places</p></td>
				<td bgcolor="#47c7ec" align="middle"><p class="white">Name</p></td>
				<td bgcolor="#47c7ec" align="middle" nowrap=""><p class="white">Price <br><span class="body">(per item)</span></p></td>
				<td bgcolor="#47c7ec" align="middle"><p class="white">Subtotal</p></td>
				<td bgcolor="#47c7ec"></td>
			</tr>
				<?php
				$total_subs = 0;
				if(!empty($cart) ){
					$x = 0;
					
					foreach($cart as $c){
						if(empty($c['name'])){
							$name = '';
						}else{
							$name = $c['name'];
						}
						if(!empty($c['id'])){
							$qty = $c['qty'];

							$price = number_format($c['price'], 2);
							$subtotal = number_format($price * $qty, 2);

							$total_subs += $subtotal;
							// $serialized = serialize($cart[$c['id']]);
							// $unserialize = unserialize($serialized);
							// print_r($unserialize);
							if($c['isconference'] == '1'){
								$display_qty = '1';
								$display_qty .= '<input type="hidden" name="cart_data[]" class="inp-qty" value="1" data-price="'.$price.'" data-inc="'.$x.'" data-id="'.$c['id'].'">';
							}else{
								$display_qty = '<input type="text" name="cart_data[]" class="inp-qty" value="'.$qty.'" data-price="'.$price.'" data-inc="'.$x.'" data-id="'.$c['id'].'">';
							}
							echo '
								<!-- START -->
								<tr>
									<td bgcolor="white" valign="top">
										'.$display_qty.'
									</td>		
									<td bgcolor="white" valign="top">
										'.$name.'
									</td>		
									<td bgcolor="white" valign="top">$'.$price.'</td>		
									<td bgcolor="white" valign="top" id="td_Subtotal'.$x.'" >$'.$subtotal.'</td>			
									<td bgcolor="white" valign="top"><button type="button" class="btn btnDeleteItem" data-id="'.$c['id'].'">Delete</button>
									<input type="hidden" name="cart_data[]" value="'.$c['id'].'">
									</td>		
								</tr>
								<!-- END -->
							';
							$x++;
						}else{
							$price = '0.00';
							$subtotal = '0.00';
						}
					}
				}
				?>


<!-- 			    <tr>
			    
			    <td bgcolor="white" valign="top">
			         
			        <input type="hidden" value="1" name="number" onchange="changes=true;">
			        <span class="body">1</span>
			         
			    </td>
			    
			    <td bgcolor="white" valign="top">
			    
			    
			    <span class="body">
			    <a href="http://www.ppmsystem.com/events.asp?id=1231&amp;action=display">On-Line Live PM Training : 2. Non-Members</a><br>
			    Location: All States<br>
			    Date:  5/02/2019<br>
			    Pricing:  <br>
			    Attendees: 
			    </span>
			    
			    </td>
			    <td bgcolor="white" valign="top"><span class="body">$99.00</span></td>
			    <td bgcolor="white" valign="top"><span class="body">$99.00</span></td>
			    <td bgcolor="white" valign="top"><span class="body"><a href="ordering.asp?action=removefromcart&amp;i=0">Delete</a></span></td>
			    </tr> -->
			    
			    <?php 

			    $postage = 0;
			    ?>
			<tr>
			<td colspan="6" bgcolor="white" align="right"><span class="body"><b>Sub Total: $</b><?php echo number_format($total_subs,2); ?></span></td>
			</tr>
			<tr>
			<td colspan="6" bgcolor="white" align="right"><span class="body"><b>Postage: </b> N/A</span></td>
			</tr>
	        
			<tr>
				<td colspan="6" bgcolor="white" align="right"><span class="body"><b>Total: </b><span id="total"> $ <?php echo number_format(($total_subs + $postage),2) ?> </span>(incl. GST)</span></td>
			</tr>
			</tbody></table>
			
					<table border="0" width="100%"><tbody><tr>
					<td width="100%" align="right" nowrap="">
						<input type="button" value="Update Cart" class="btn btn-default btnUpdateCart">&nbsp;
					</td>
					<td nowrap="">
						<input type="button" value="Reset Cart" class="btn btn-default btnResetCart">&nbsp;
					</td>
					<td align="right">
						<input type="button" value="Pay Now" class="btn btn-default btnPayNow">
					</td>
					</tr></tbody></table>
	         

				<!-- end main content -->

				
	             
				 
	        </form></div>

	    </div>
    </div>
</div>

<script>
$(document).ready(function(){
	$('.inp-qty').blur(function(){
		var id = $(this).data('inc');
		var p = $(this).data('price');

		var v = $(this).val();

		var total = p * v;
		$('#td_Subtotal'+ id).html('$'+ Number(total).toFixed(2));
	});

	$('.btnDeleteItem').click(function(){
		var id = $(this).data('id');
		var x = $('form[name=dataform]').serialize();

		swal({
		  title: "Are you sure?",
		  text: "Once deleted, you will add to cart again the item!",
		  icon: "warning",
		  buttons: true,
		  dangerMode: true,
		})
		.then((willDelete) => {
			console.log(x);
			$.ajax({
				type : 'post',
				url : '<?php echo base_url(); ?>ppmsystem/ppm/ordering/delete_item',
				data : {
					'id' : id
				},
				success : function(res){
					console.log(res);
					// window.location.reload();
					window.location = '<?php echo base_url(); ?>ppmsystem/ppm/ordering/addtocart/';
				}
			});
		});
	});

	$('.btnUpdateCart').click(function(){
		var x = $('form[name=dataform]').serialize();
			$.ajax({
				type : 'post',
				url : '<?php echo base_url(); ?>ppmsystem/ppm/ordering/update_item',
				data : x,
				success : function(res){
					console.log(res);
					window.location.reload();
					// window.location = '<?php echo base_url(); ?>ppmsystem/ppm/ordering/addtocart/';
				}
			});
	});

	$('.btnResetCart').click(function(){
			$.ajax({
				type : 'post',
				url : '<?php echo base_url(); ?>ppmsystem/ppm/ordering/reset_cart',
				data : {

				},
				success : function(res){
					console.log(res);
					// window.location.reload();
					window.location = '<?php echo base_url(); ?>ppmsystem/ppm/ordering/addtocart/';
				}
			});
	});

	$('.btnPayNow').click(function(){
		window.location = '<?php echo base_url(); ?>ppmsystem/ppm/ordering/upgradecart/';
	});
});

</script>
