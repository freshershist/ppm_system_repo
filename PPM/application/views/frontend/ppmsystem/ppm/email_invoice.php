<style>
.body { color: #003596; font-size: 12px; line-height: 17px; font-family: Arial, Helvetica, Geneva, Swiss, SunSans-Regular } 
.header { color: #003596; font-size: 18px; line-height: 17px; font-family: Arial, Helvetica, Geneva, Swiss, SunSans-Regular }
</style>

<img src="<?php echo base_url() . 'assets/images/ppm.gif'; ?>"><br><span class="body"><b>Palmer Professional Management Group Pty Ltd</b><br>ABN: 16 100 733 408<br><br>

<span class="header">Tax Invoice : <?php echo $orderId; ?></span><br>
<span class="body">On-Line newsletter subscription received at : <?php echo date('Y-m-d h:i:s A', now(PPM_TIMEZONE)); ?><br>
    Subscription Type: <?php echo $membershipType; ?><br>
    Company Name: <?php echo $company; ?><br>
    Contact Name: <?php echo $contactName; ?><br>
    Contact Number: <?php echo $contactNumber; ?><br>
    Address: <?php echo $address; ?> . <br>
    Email Adddress: <?php echo $emailAddress; ?><br>
    Approx. Landlords: <?php echo $landlordCount; ?><br>
    Payment method: Credit Card<br>
    <br>Order as follows<br><br>
</span>

<table border="0" cellpadding="4" cellspacing="1" bgcolor="#8AB8DE" width="100%">

    <tr>
        <td bgcolor="#DCEAF5" valign="top" align="middle"><span class="body">QTY</span></td>
        <td bgcolor="#DCEAF5" valign="top" align="middle"><span class="body">Name</span></td>
        <td bgcolor="#DCEAF5" valign="top" align="middle" nowrap><span class="body">Price</span><br><span class="body">(per item)</span></td>
        <td bgcolor="#DCEAF5" valign="top" align="middle"><span class="body">Subtotal</span></td>
    </tr>

    <tr>
        <td bgcolor="#eeeeee" valign="top"><span class="body">1</span></td>
        <td bgcolor="#eeeeee" valign="top"><span class="body">Newsletter Subscription (Expires: <?php echo $memberShipExpires; ?>)</span></td>
        <td bgcolor="#eeeeee" valign="top"><span class="body"><?php echo $amount; ?></span></td>
        <td bgcolor="#eeeeee" valign="top"><span class="body"><?php echo $amount; ?></span></td>
    </tr>

    <tr>
        <td colspan="4" bgcolor="white" align="right"><span class="body"><b>Sub Total: </b><?php echo $amount; ?></span></td>
    </tr>

    <tr>
        <td colspan="4" bgcolor="white" align="right"><span class="body"><b>Postage: </b>$0.00</span></td>
    </tr>

    <tr>
        <td colspan="4" bgcolor="white" align="right"><span class="body"><b>Total (incl GST): </b><?php echo $amount; ?></span></td>
    </tr>    

</table>

<br>
PO Box 5019 <br>
Robina Town Centre QLD 4230 Australia<br>
Phone: 07 5562 0037<br>
Fax: 07 5580 9844<br>
Email: info@ppmsystem.com<br>