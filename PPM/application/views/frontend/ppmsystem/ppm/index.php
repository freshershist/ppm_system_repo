

    <div class="container-aqua">
        <div class="container">
                <h1>INTRODUCING
                <div class="visible-xs-inline"><br></div>
                 PPM GROUP <span class="visible-xs-inline"><br></span> <span class="hidden-xs">&nbsp;  | &nbsp;</span>  THE COMPETITIVE EDGE <sup class="hidden-xs">&reg;</sup></h1>

            <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <p>Leading the way in property management procedural systems, learning resource tools, consulting, training, due diligence and rent roll sales</p>
            </div>
            </div>
        </div>
    </div>

    <div class="container support">
        <p class="h1">How the PPM Group can support and assist you</p>
        <p class="big">Fine-tuning the way you do property management</p>

        <div class="col-md-2 module">
            <img src="media/inspire.png" alt="Inspire &amp; motivate your team">
            <p><strong>Inspire &amp; motivate your team</strong></p>
            <p class="darkgrey small">Our easy to access online training sessions and national training events will transform, inspire, motivate and energise your entire team to reach their full potential.</p>
        </div>

        <div class="col-md-2 module">
            <img src="media/increase-profits.png" alt="Increase profits &amp; productivity">
            <p><strong>Increase profits
                       &amp; productivity</strong></p>
            <p class="darkgrey small">Our passion is to train, empower and coach property management team members and business owners across the nation to assist with the overall performance of the business.</p>
        </div>


        <div class="col-md-2 module">
            <img src="media/recruit-staff.png" alt="Recruit staff with ease">
            <p><strong>Recruit staff
                       with ease</strong></p>
            <p class="darkgrey small">We have a tried and tested solution that takes the thinking out of the day-to-day processes, enabling agencies to recruit support team members with limited experience.</p>
        </div>


        <div class="col-md-2 module">
            <img src="media/less-stress.png" alt="Less stress and more control">
            <p><strong>Less stress and
                       more control</strong></p>
            <p class="darkgrey small">We provide the systems, tools, training and support to assist in reducing the stress for property management team members, allowing owners more time to work on the business.</p>
        </div>


        <div class="col-md-2 module">
            <img src="media/work-smarter.png" alt="Work smarter not harder">
            <p><strong>Work smarter
                       not harder</strong></p>
            <p class="darkgrey small">Our focus is to share the cutting-edge and up-to-date strategies, tips and solutions to enable property management agencies to gain more time and out-perform their competitors.</p>
        </div>


        <div class="col-md-2 module">
            <img src="media/fast-track.png" alt="Fast track your systems">
            <p><strong>Fast track your systems</strong></p>
            <p class="darkgrey small">The PPMsystem is our tried and tested blueprint for success. Everything can be accessed at the click of a button from any location guiding your team through each process.</p>
        </div>


    </div>

    <div class="container line"></div>

    <div class="container">
        <div id="circles" class="rotator">


            <div>
                <a class="circle" href="contact2.asp?enquiry=9&healthcheck=true">
                    <div class="content">
                        <img src="media/circles/icons/pm-health-check-request.png" alt="PM health check request icon">
                        PM health check request
                    </div>
                    <img src="media/circles/PM-Health-Check-Request.jpg" class="background" alt="PM health check request background">
                </a>
            </div>

            <div>
                <a class="circle" href="products.asp?category=34&action=list">
                    <div class="content">
                        <img src="media/circles/icons/learning-resources.png" alt=" learning resources icon">
                        learning resources
                    </div>
                    <img src="media/circles/Learning-Resources.jpg" class="background" alt="learning resources background">
                </a>
            </div>

            <div>
                <a class="circle" href="services2c.asp">
                    <div class="content">
                        <img src="media/circles/icons/due-dilligence.png" alt="due diligence icon">
                        due diligence
                    </div>
                    <img src="media/circles/Due-Diligence.jpg" class="background" alt="due diligence background">
                </a>
            </div>

            <div>
                <a class="circle" href="products.asp">
                    <div class="content">
                        <img src="media/circles/icons/products.png" alt="products icon">
                        products
                    </div>
                    <img src="media/circles/Products.jpg" class="background" alt="products background">
                </a>
            </div>

            <div>
                <a class="circle" href="services2.asp">
                    <div class="content">
                        <img src="media/circles/icons/consulting.png" alt="consulting icon">
                        consulting
                    </div>
                    <img src="media/circles/Consulting.jpg" class="background" alt="consulting background">
                </a>
            </div>

            <div>
                <a class="circle" href="property-management-awards-national.asp">
                    <div class="content">
                        <img src="media/circles/icons/national-awards.png" alt="national awards icon">
                        national awards
                    </div>
                    <img src="media/circles/National-Awards.jpg" class="background" alt="national awards background">
                </a>
            </div>

            <div>
                <a class="circle" href="services2d.asp">
                    <div class="content">
                        <img src="media/circles/icons/performance-assessments.png" alt="performance assessment icon">
                        performance assessment
                    </div>
                    <img src="media/circles/Performance-Assesment.jpg" class="background" alt="performance assessment background">
                </a>
            </div>

            <div>
                <a class="circle" href="events.asp?id=1092&category=&action=display">
                    <div class="content">
                        <img src="media/circles/icons/ppm-conference.png" alt="PPM conference icon">
                        ppm conference
                    </div>
                    <img src="media/circles/PPM-Conference.jpg" class="background" alt="ppm conference background">
                </a>
            </div>

            <div>
                <a class="circle" href="services1.asp">
                    <div class="content">
                        <img src="media/circles/icons/rent-roll.png" alt="rent roll &amp; agency sales icon">
                        rent roll &amp; agency sales
                    </div>
                    <img src="media/circles/Rent-Roll-Agency-Sales.jpg" class="background" alt="rent roll &amp; agency sales background">
                </a>
            </div>

            <div>
                <a class="circle" href="services3.asp">
                    <div class="content">
                        <img src="media/circles/icons/corporate-speaking.png" alt="corporate speaking icon">
                        corporate speaking
                    </div>
                    <img src="media/circles/Corporate-Speaking.jpg" class="background" alt="corporate speaking background">
                </a>
            </div>

            <div>
                <a class="circle" href="events.asp">
                    <div class="content">
                        <img src="media/circles/icons/online-training.png" alt="online training icon">
                        online training
                    </div>
                    <img src="media/circles/Online-Training.jpg" class="background" alt="online training background">
                </a>
            </div>

            <div>
                <a class="circle" href="newsletter.asp">
                    <div class="content">
                        <img src="media/circles/icons/landlord-newsletter.png" alt="landlord newsletter icon">
                        landlord newsletter
                    </div>
                    <img src="media/circles/Landlord-Newsletter.jpg" class="background" alt="landlord newsletter background">
                </a>
            </div>

            <div>
                <a class="circle" href="events.asp">
                    <div class="content">
                        <img src="media/circles/icons/training-events.png" alt="training &amp; events icon">
                        training &amp; events
                    </div>
                    <img src="media/circles/Training-Events.jpg" class="background" alt=" training &amp; events background">
                </a>
            </div>

            <div>
                <a class="circle" href="ppmsystem.asp">
                    <div class="content">
                        <img src="media/circles/icons/ppm-system.png" alt="PPM system icon">
                        PPM system
                    </div>
                    <img src="media/circles/PPM-System.jpg" class="background" alt="PPM system background">
                </a>
            </div>

            <div>
                <a class="circle" href="ppmtv.asp">
                    <div class="content">
                        <img src="media/circles/icons/ppm-tv.png" alt="PPM tv icon">
                        ppm tv
                    </div>
                    <img src="media/circles/PPM-TV.jpg" class="background" alt="PPM tv background">
                </a>
            </div>

            <div>
                <a class="circle" href="articles.asp">
                    <div class="content">
                        <img src="media/circles/icons/news-articles.png" alt="news &amp; articles icon">
                        news &amp; articles
                    </div>
                    <img src="media/circles/News-Articles.jpg" class="background" alt="news &amp; articles background">
                </a>
            </div>


        </div>
    </div>

    <div class="container-grey">
        <p><a href="contact2.asp?enquiry=9&healthcheck=true" class="btn btn-white">CLICK HERE</a> <span class="visible-xs-inline"><br></span>&nbsp; TO REQUEST A FREE PROPERTY MANAGEMENT HEALTH CHECK QUESTIONNAIRE</a></p>
    </div>

    <div class="container clients">
        <p class="h1">What do our clients say?</p>
        <br>
        <div class="row">
        </div>

    </div>
    
    <br>
    
    <div class="container clients">
        <p class="h1">Keeping you updated</p>
        <br>
        <div class="row">
        </div>

    </div>

