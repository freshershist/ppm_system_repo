<?php

$surCharge = 0.02;
$attendeePositionArray = array("Assistant Property Manager", "Business Development", "Dept. Manager", "Inspection Officer", "Leasing Officer", "Maintenance Officer", "Principal", "Property Manager", "Receptionist", "Support/Admin", "Other")
?>

<div class="container">
    <div class="row">
			<div class="col-md-3 left-column">
			    <div class="sub-nav">
			        <p></p>
			    </div>
			</div>
            <div class="col-md-9 right-column">

            <?php
            print_r($cart);
            $position_html = '<option value="-">Select</option>';
            foreach($attendeePositionArray as $position => $pos_value){
            	$position_html .= '<option>'.$pos_value.'</position>';
            }

            if(!empty($cart)){
            	$has_conference = false;
            	$event_info = '';
                  $event_ids = '';
            	foreach($cart as $c){
            		$isconference = $c['isconference'];
            		if($isconference == '1'){
            			$has_conference = true;
            			$event_info .= '<hr>
            			<div class="row">
            				<div class="col-md-6">
            					'.$c['name'].'
            				</div>
            				<div class="col-md-6">
            					<table class="table" id="tbl_attendee_'.$c['id'].'" >
            						<tr>
            							<td>Attendee</td>
            							<td>Position</td>
            							<td></td>
            						</tr>
            						<tr>
            							<td><input type="text" name="attendee_'.$c['id'].'[]" class="form-control"></td>
            							<td>
            								<select name="attendee_'.$c['id'].'[]">
            									'.$position_html.'
            								</select>
            							</td>
            							<td></td>
            						</tr>
            					</table>
            					<button type="button" class="btn btnAddAttendee" data-id="'.$c['id'].'" ><i class="fa fa-plus"></i> Additional Name</button>
            				</div>
            			</div>
            			<hr>
            			';
                              $event_ids .= $c['id'].',';
            		}
            	}
            }

            if(isset($has_conference) && $has_conference==true && $page!=='details'){
            	?>
                  <form id="frmEventInfo">
            	<h1>EVENT INFORMATION</h1>
            	<p>We've noticed as part of your order you would like to take part in one of our training sessions. Please enter the name of the delegates that will be participating.</p>
            	<?php
            	echo $event_info;
            	?>
            	<input type="button" data-ids="<?php echo  rtrim($event_ids, ','); ?>"  value="CONTINUE" class="btn btn-default btnCheckoutContinue" />
                  </form>
            	<?php
            }else{

                  if(!empty($cart_attendees)):  
                  echo '<pre>';
                  print_r($cart_attendees);
                  echo '</pre>';
                  endif;
                  ?>
            	<!-- START YOUR DETAILS -->
		      <h1>YOUR DETAILS</h1>
                  
                  <div class="row">                
                        <div class="col-md-12">
                              <div class="form-group">
                                    <label>Business Name *</label>
                                    <input type="text" name="inpBusinessName" class="form-control">
                              </div>

                              <div class="form-group">
                                    <label>Central Contact Name *</label>
                                    <input type="text" name="inpBusinessName" class="form-control">
                              </div>

                              <strong>Postal Details</strong>
                              
                              <div class="form-group">
                                    <label>Street *</label>
                                    <input type="text" name="inpBusinessName" class="form-control">
                              </div>

                              <div class="form-group">
                                    <label>Suburb *</label>
                                    <input type="text" name="inpBusinessName" class="form-control">
                              </div>

                              <div class="form-group">
                                    <label>State/County *</label>
                                    <input type="text" name="inpBusinessName" class="form-control">
                              </div>

                              <div class="form-group">
                                    <label>Post Code *</label>
                                    <input type="text" name="inpBusinessName" class="form-control">
                              </div>

                              <div class="form-group">
                                    <label>Country</label>
                                    <input type="text" name="inpBusinessName" class="form-control">
                              </div>

                              <div class="form-group">
                                    <label>Phone *</label>
                                    <input type="text" name="inpBusinessName" class="form-control">
                              </div>

                              <div class="form-group">
                                    <label>Email *</label>
                                    <input type="text" name="inpBusinessName" class="form-control">
                              </div>

                              <div class="form-group">
                                    <label>Confirm Email *</label>
                                    <input type="text" name="inpBusinessName" class="form-control">
                              </div>

                              <div class="form-group">
                                    <label>How did you hear about us *</label>
                                    <input type="text" name="inpBusinessName" class="form-control">
                              </div>

                              <div class="form-group">
                                    <label>Promotional Code </label>
                                    <input type="text" name="inpBusinessName" class="form-control">
                              </div>

                              <div class="form-group">
                                    <label>Order Comments </label>
                                    <input type="text" name="inpBusinessName" class="form-control">
                              </div>

                              <div class="form-group">
                                    <label>
                                    <input type="checkbox">
                                    I agree to the following terms and conditions *<br/>
                                    The PPM Products, Services and Training Material are sold and delivered subject to the condition that they shall not, by way of trade or otherwise, be lent, re-sold, hired out or otherwise circulated without the written consent of the PPM Group. All rights reserved under copyright laws. Credit Card orders will attract a 2% credit card charge. By submitting this order I agree to the above conditions of sale. </label>
                              </div>
                        </div>
                  </div>


			<!-- END YOUR DETAILS-->
		<?php 
            } 
            ?>
            </div>
	    </div>
    </div>
</div>

<script>
$(document).ready(function(){
	$('.btnProceedCheckout').click(function(){
		window.location = '<?php echo base_url(); ?>ppmsystem/ppm/ordering/checkout/';
	});

	$('.btnAddAttendee').click(function(){
		var id = $(this).data('id');
		var add_html = '\
            						<tr>\
            							<td><input type="text" name="attendee_'+ id +'[]" class="form-control"></td>\
            							<td>\
            								<select name="attendee_'+ id +'[]">\
            									<?php echo $position_html; ?>\
            								</select>\
            							</td>\
            							<td><button type="button" class="btn btnRemove">x</button></td>\
            						</tr>\
		';


		$('#tbl_attendee_'+ id + ' tbody').append(add_html);
	});

	$('table').on('click', '.btnRemove', function(){
		$(this).parent().parent().remove();
	});

      $('.btnCheckoutContinue').click(function(){
            var ids = $(this).data('ids');
            console.log(ids);
            var ser = $('#frmEventInfo').serialize();
            ser += '&ids='+ids;
            $.ajax({
                  type : 'post',
                  url  : '<?php echo base_url(); ?>ppmsystem/ppm/ordering/collect_attendees',
                  data : ser,
                  success : function(res){
                        console.log(res);
                        if(res == 'has_empty'){
                              swal({
                                title: "Warning!",
                                text: "Please enter attendees and position!",
                                icon: "warning",
                                dangerMode: true
                              })
                        }else{
                              window.location = '<?php echo base_url(); ?>ppmsystem/ppm/ordering/details';
                        }
                  }
            });
      });
});

</script>
