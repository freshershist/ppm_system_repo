<?php 

if(isset($stateArray)) $GLOBALS['stateArray'] = $stateArray;
if(isset($unsubscribeReasonArray)) $GLOBALS['unsubscribeReasonArray'] = $unsubscribeReasonArray;
if(isset($eventNameArr)) $GLOBALS['eventNameArr'] = $eventNameArr;

function get_fields($params = NULL){

    $form = '';

    foreach ($params as $arr) {
        $required = ($arr['required']) ? ' <i class="fa fa-asterisk text-danger"></i>' : '';

        switch ($arr['id']) {

            case 1:
                $label = (!empty($arr['label'])) ? $arr['label'] : 'Company Name';
                $form .= '<div class="form-group row">
                            <span for="staticEmail" class="col-sm-5 control-label text-right">'.$label.':'.$required.'</span>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" name="companyName" id="" value="' . $arr['value'] . '">
                            </div>
                        </div>';
                break;

            case 2:
                $label = (!empty($arr['label'])) ? $arr['label'] : 'First Name';
                $form .= '<div class="form-group row">
                            <span for="staticEmail" class="col-sm-5 control-label text-right">'.$label.':'.$required.'</span>
                            <div class="col-sm-7">
                                <input type="text" name="firstname" class="form-control" id="" value="' . $arr['value'] . '">
                            </div>
                        </div>';
                break;    

            case 3:
                $label = (!empty($arr['label'])) ? $arr['label'] : 'Surname';
                $form .= '<div class="form-group row">
                            <span for="staticEmail" class="col-sm-5 control-label text-right">'.$label.':'.$required.'</span>
                            <div class="col-sm-7">
                                <input type="text" name="surname" class="form-control" id="" value="' . $arr['value'] . '">
                            </div>
                        </div>';
                break; 

            case 4:
                $label = (!empty($arr['label'])) ? $arr['label'] : 'Email';
                $form .= '<div class="form-group row">
                            <span for="staticEmail" class="col-sm-5 control-label text-right">'.$label.':'.$required.'</span>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" name="email" id="" value="' . $arr['value'] . '">
                            </div>
                        </div>';
                break;

            case 5:
                $label = (!empty($arr['label'])) ? $arr['label'] : 'Phone';
                $form .= '<div class="form-group row">
                            <span for="staticEmail" class="col-sm-5 control-label text-right">'.$label.':'.$required.'</span>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" name="phone" id="" value="' . $arr['value'] . '">
                            </div>
                        </div>';
                break; 

            case 6:
                $label = (!empty($arr['label'])) ? $arr['label'] : 'Street';
                $form .= '<div class="form-group row">
                            <span for="staticEmail" class="col-sm-5 control-label text-right">'.$label.':'.$required.'</span>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" name="street" id="" value="' . $arr['value'] . '">
                            </div>
                        </div>';
                break;

            case 7:
                $label = (!empty($arr['label'])) ? $arr['label'] : 'Suburb';
                $form .= '<div class="form-group row">
                            <span for="staticEmail" class="col-sm-5 control-label text-right">'.$label.':'.$required.'</span>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" name="suburb" id="" value="' . $arr['value'] . '">
                            </div>
                        </div>';
                break; 

            case 8:
                $label = (!empty($arr['label'])) ? $arr['label'] : 'State/Country';

                $state = form_dropdown('state', $GLOBALS['stateArray'], $arr['value'], 'id="state" class="form-control"');
                $form .= '<div class="form-group row">
                            <span for="staticEmail" class="col-sm-5 control-label text-right">'.$label.':'.$required.'</span>
                            <div class="col-sm-7">'.$state.'</div>
                        </div>';
                break;

            case 9:
                $label = (!empty($arr['label'])) ? $arr['label'] : 'Postcode';
                $form .= '<div class="form-group row">
                            <span for="staticEmail" class="col-sm-5 control-label text-right">'.$label.':'.$required.'</span>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" name="postcode" id="" value="' . $arr['value'] . '">
                            </div>
                        </div>';
                break; 

            case 10:
                $label = (!empty($arr['label'])) ? $arr['label'] : 'Other';
                $form .= '<div class="form-group row">
                            <span for="staticEmail" class="col-sm-5 control-label text-right">'.$label.':'.$required.'</span>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" name="other" id="" value="' . $arr['value'] . '">
                            </div>
                        </div>';
                break;                             

            case 11:
                $label = (!empty($arr['label'])) ? $arr['label'] : 'Mobile';
                $form .= '<div class="form-group row">
                            <span for="staticEmail" class="col-sm-5 control-label text-right">'.$label.':'.$required.'</span>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" name="mobile" id="" value="' . $arr['value'] . '">
                            </div>
                        </div>';
                break; 

            case 12:
                $label = (!empty($arr['label'])) ? $arr['label'] : 'Comments';
                $form .= '<div class="form-group row">
                            <span for="staticEmail" class="col-sm-5 control-label text-right">'.$label.':'.$required.'</span>
                            <div class="col-sm-7">
                                <textarea name="comments" class="form-control" rows="6">' . $arr['value'] . '</textarea>
                            </div>
                        </div>';
                break;

            case 13:
                $label = (!empty($arr['label'])) ? $arr['label'] : 'Reason';
                $reason = form_dropdown('unsubscribeReason', $GLOBALS['unsubscribeReasonArray'], "".$arr['value'], 'id="unsubscribeReason" class="form-control"');
                $form .= '<div class="form-group row">
                            <span for="staticEmail" class="col-sm-5 control-label text-right">'.$label.':'.$required.'</span>
                            <div class="col-sm-7">'.$reason.'</div>
                        </div>';
                break; 

            case 14:
                $label = (!empty($arr['label'])) ? $arr['label'] : 'Company Name';
                $form .= '<div class="form-group row">
                            <span for="staticEmail" class="col-sm-5 control-label text-right">'.$label.':'.$required.'</span>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" name="oldcompanyName" id="" value="' . $arr['value'] . '">
                            </div>
                        </div>';
                break;

            case 15:
                $label = (!empty($arr['label'])) ? $arr['label'] : 'Email';
                $form .= '<div class="form-group row">
                            <span for="staticEmail" class="col-sm-5 control-label text-right">'.$label.':'.$required.'</span>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" name="oldEmail" id="" value="' . $arr['value'] . '">
                            </div>
                        </div>';
                break; 

            case 16:
                $label = (!empty($arr['label'])) ? $arr['label'] : 'PPM Training Sessions';
                $eventName = form_dropdown('eventName', $GLOBALS['eventNameArr'], '', 'id="eventName" class="form-control"');
                $form .= '<div class="form-group row">
                            <span for="staticEmail" class="col-sm-5 control-label text-right">'.$label.':'.$required.'</span>
                            <div class="col-sm-7">'.$eventName.'</div>
                        </div>';
                break;

            case 17:
                $label = (!empty($arr['label'])) ? $arr['label'] : 'Other Requested Training';
                $form .= '<div class="form-group row">
                            <span for="staticEmail" class="col-sm-5 control-label text-right">'.$label.':'.$required.'</span>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" name="othertrain" id="" value="' . $arr['value'] . '">
                            </div>
                        </div>';
                break; 

            case 18:
                $label = (!empty($arr['label'])) ? $arr['label'] : 'Preferred Date';
                $am = (!empty($arr['value2']) && $arr['value2'] === 'am') ? 'checked=""' : '';
                $pm = (!empty($arr['value2']) && $arr['value2'] === 'pm') ? 'checked=""' : '';

                $form .= '<div class="form-group row">
                            <span class="col-sm-5 control-label text-right">'.$label.':'.$required.'</span>
                            <div class="col-sm-7">
                                <div class="input-group" id="preferredDate">
                                    <input name="prefDate" type="text" class="form-control" placeholder="dd/mm/yyyy" value="' . $arr['value'] . '" />
                                    <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <span class="col-sm-5 control-label"></span>
                            <div class="form-check col-sm-7">
                                <label class="form-check-label">
                                  <input type="radio" class="form-check-input" name="myTime" id="" value="am" ' . $am . '> AM
                                </label>
                                <label class="form-check-label">
                                  <input type="radio" class="form-check-input" name="myTime" id="" value="pm" ' . $pm . ' > PM
                                </label>

                              </div>
                        </div>';
                break;                                                

            default:
                # code...
                break;
        }
    }

    return $form;
}

?>

<?php if(isset($isMain) && !$isMain) { ?>

<div class="container">
    <div class="row">
        <div class="col-md-3 left-column"></div>
        <div class="col-md-9 right-column">
            <h1><?php echo $title; ?></h1>

            <?php if(empty($enquiry)) { ?>
                <h2>Do you want to contact us electronically?</h2>
                <p>Please select the nature of your enquiry:</p>
                <form id="contact-form" action="<?php echo base_url(). 'contact-us/'; ?>" method="POST">

                    <div class="form-group">
                        <div class="col-md-5 col-sm-9 col-xs-12">
                            <select id="contact-form-enquiry" class="form-control" size="1">
                                <option value="' . $arr['value'] . '">[ select ]</option>
                                <option value="9">Contact Us</option>
                                <option value="1">Unsubscribe from database</option>
                                <option value="2">Update your contact details</option>
                                <option value="3">System Presentation Request</option>
                                <option value="4">Request to advertise in the Rent Roll Classifieds</option>
                                <option value="5">Enquiry into a Rent Roll classified</option>
                                <option value="11">Training Interest</option>
                                <option value="6">Enquiry for Job Advertised</option>
                                <option value="12">Consultancy Request</option>
                                <option value="7">Enquiry into careers at the PPM Group</option>
                                <option value="8">General Feedback</option>
                                <option value="14">eNews Signup</option>
                            </select>
                        </div>
                    </div>
                </form>

                <div class="clearfix"></div>

                <h2>Head Office Contact</h2>
                <p>
                    <b>Postal Address<br>
                    </b>PO Box 5019 <br>
                    Robina Town Centre QLD 4230 Australia <br>
                    <br>
                    <b>Contact<br>
                    </b>T: (07) 5562-0037<br>
                    F: (07) 5580-9844<br>
                    E: <a href="mailto:info@ppmgroup.com.au">info@ppmgroup.com.au</a><br>
                </p>

            <?php } ?>
<?php } //isMain ?>

        <?php if(!empty($enquiry)) { ?>

            <form id="enquiry-form" class="form-horizontal form-label-left" action="<?php echo base_url() . 'contact-us/' . $enquiry . $rent_roll_id; ?>" method="POST">
                <div class="row">

                <?php if($enquiry === '1') { ?>
                    <div class="col-xs-12 col-sm-12 col-md-8">
                        
                        <p><b>We are sorry to see you go ...</b><br>To understand how to service our subscribers better, please select the option which best suits the reason why you requested to be removed. We truly do appreciate your honest feedback and you'll definitely be removed from our list immediately. Thank you!</p>

                        <?php 

                            $params = array(
                                array('id'=>1, 'required'=>TRUE, 'label'=>'', 'value'=>$companyName),
                                array('id'=>2, 'required'=>TRUE, 'label'=>'', 'value'=>$firstname),
                                array('id'=>3, 'required'=>TRUE, 'label'=>'', 'value'=>$surname),
                                array('id'=>4, 'required'=>TRUE, 'label'=>'Email Address <small>(to be removed)</small>', 'value'=>$email),
                                array('id'=>5, 'required'=>TRUE, 'label'=>'Contact Number', 'value'=>$phone),
                                array('id'=>8, 'required'=>TRUE, 'label'=>'', 'value'=>$state),
                                array('id'=>10, 'required'=>FALSE, 'label'=>'', 'value'=>$other),
                                array('id'=>13, 'required'=>TRUE, 'label'=>'', 'value'=>$unsubscribeReason),
                                array('id'=>12, 'required'=>FALSE, 'label'=>'', 'value'=>$comments)         
                            );

                            echo get_fields($params); 
                        ?>
                    </div>
                <?php } elseif($enquiry === '2' OR $enquiry === '3' OR $enquiry === '4' OR $enquiry === '5' OR $enquiry === '12' OR $enquiry === '17') { ?>
                    <div class="col-xs-12 col-sm-12 col-md-8">
                        <?php if($enquiry === '2') { ?>
                        <label>Old Details</label>
                                     
                        <?php 
                            $params = array(
                                array('id'=>14, 'required'=>TRUE, 'label'=>'','value'=>$oldcompanyName),
                                array('id'=>15, 'required'=>TRUE, 'label'=>'','value'=>$oldEmail)     
                            );

                            echo get_fields($params); 
                        ?>                   
                        <hr/>

                        <label>New Details</label>

                        <?php } elseif($enquiry === '5') { ?>

                            <?php if(isset($rent_roll) && !empty($rent_roll)) { ?>
                                <p>
                                    <label>Description:</label><br />
                                    <label><?php echo $rent_roll['name']; ?> |</label><span>REF: <?php echo $rent_roll['code']; ?></span>
                                    <br/>
                                <?php if(!empty($rent_roll['numberOfEnquiries'])) { ?>
                                    <label>Current number of enquiries: </label> <span><?php echo $rent_roll['numberOfEnquiries']; ?></span>
                                    <br/>
                                <?php } ?>

                                    <label>Location: </label> <span><?php if(!empty($rent_roll['state'])) { echo $stateArray[$rent_roll['state']]; } if(!empty($rent_roll['location'])) { echo ' - ' . $rent_roll['location']; } ?></span>
                                    <br/>
                                    <span><?php echo $rent_roll['shortdesc']; ?></span>
                                </p>
                            <?php } ?>
                        <?php } ?>    

                        <?php 
                            $params = array(
                                array('id'=>1, 'required'=>TRUE, 'label'=>'','value'=>$companyName),
                                array('id'=>2, 'required'=>TRUE, 'label'=>'','value'=>$firstname),
                                array('id'=>3, 'required'=>TRUE, 'label'=>'','value'=>$surname),
                                array('id'=>4, 'required'=>TRUE, 'label'=>'','value'=>$email),
                                array('id'=>5, 'required'=>TRUE, 'label'=>'','value'=>$phone),
                                array('id'=>6, 'required'=>FALSE, 'label'=>'','value'=>$street),
                                array('id'=>7, 'required'=>FALSE, 'label'=>'','value'=>$suburb),
                                array('id'=>8, 'required'=>FALSE, 'label'=>'','value'=>$state),
                                array('id'=>9, 'required'=>FALSE, 'label'=>'','value'=>$postcode),
                                array('id'=>10, 'required'=>FALSE, 'label'=>'','value'=>$other),
                                array('id'=>11, 'required'=>FALSE, 'label'=>'','value'=>$mobile)      
                            );

                            if($enquiry === '12') {
                                $params[] = array('id'=>12, 'required'=>FALSE, 'label'=>'','value'=>'');
                            }

                            echo get_fields($params); 
                        ?>
                    </div>
                <?php } elseif($enquiry === '6' OR $enquiry === '7' OR $enquiry === '10' OR $enquiry === '14') { ?>
                    <div class="col-xs-12 col-sm-12 col-md-8">

                        <?php if($enquiry === '10') { ?>
                            <p>If you would like access to our Monthly Newsletter to send to lessors at the end of the month with their statement, please complete and submit the below form.</p>
                            
                        <?php } elseif($enquiry === '14') { ?>
                            <p>Welcome to our PPM ENews.</p>

                            <p>We invite you to join thousands of agencies and a community of property management professionals across Australia and internationally who receive a complimentary copy of our PPM ENews.</p>

                            <p>Keep up-to-date on training events, cutting edge property management processes, motivational tips, industry news, legislation changes, special offers and much more by signing up to today.</p>

                            <h2>PPM E-NEWS</h2>
                            <p>Join thousands of property management professionals today</p>
                        <?php } ?>

                        <?php 
                            if($enquiry === '10' OR $enquiry === '14') {
                                $params = array(
                                    array('id'=>1, 'required'=>TRUE, 'label'=>'','value'=>$companyName),
                                    array('id'=>2, 'required'=>TRUE, 'label'=>'','value'=>$firstname),
                                    array('id'=>3, 'required'=>TRUE, 'label'=>'','value'=>$surname),
                                    array('id'=>4, 'required'=>TRUE, 'label'=>'','value'=>$email),
                                    array('id'=>5, 'required'=>TRUE, 'label'=>'','value'=>$phone),
                                    array('id'=>6, 'required'=>TRUE, 'label'=>'','value'=>$street),
                                    array('id'=>7, 'required'=>TRUE, 'label'=>'','value'=>$suburb),
                                    array('id'=>8, 'required'=>TRUE, 'label'=>'','value'=>$state),
                                    array('id'=>9, 'required'=>TRUE, 'label'=>'','value'=>$postcode),
                                    array('id'=>10, 'required'=>FALSE, 'label'=>'','value'=>$other),
                                    array('id'=>11, 'required'=>TRUE, 'label'=>'','value'=>$mobile)  
                                );
                            }
                            else {
                                $params = array(
                                    array('id'=>2, 'required'=>TRUE, 'label'=>'','value'=>$firstname),
                                    array('id'=>3, 'required'=>TRUE, 'label'=>'','value'=>$surname),
                                    array('id'=>4, 'required'=>TRUE, 'label'=>'','value'=>$email),
                                    array('id'=>5, 'required'=>TRUE, 'label'=>'','value'=>$phone),
                                    array('id'=>6, 'required'=>TRUE, 'label'=>'','value'=>$street),
                                    array('id'=>7, 'required'=>TRUE, 'label'=>'','value'=>$suburb),
                                    array('id'=>8, 'required'=>TRUE, 'label'=>'','value'=>$state),
                                    array('id'=>9, 'required'=>TRUE, 'label'=>'','value'=>$postcode),
                                    array('id'=>10, 'required'=>FALSE, 'label'=>'','value'=>$other),
                                    array('id'=>11, 'required'=>TRUE, 'label'=>'','value'=>$mobile),
                                    array('id'=>12, 'required'=>FALSE, 'label'=>'','value'=>$comments)   
                                );
                            }

                            echo get_fields($params); 
                        ?>
                    </div>
                <?php } elseif($enquiry === '8' OR $enquiry === '9') { ?>
                    <div class="col-xs-12 col-sm-12 col-md-8">

                        <?php 
                            if($enquiry === '9') {
                                $params = array(
                                    array('id'=>1, 'required'=>FALSE, 'label'=>'', 'value'=>$companyName)   
                                );

                                echo get_fields($params); 
                            }

                            $params = array(
                                array('id'=>2, 'required'=>TRUE, 'label'=>'','value'=>$firstname),
                                array('id'=>3, 'required'=>TRUE, 'label'=>'','value'=>$surname),
                                array('id'=>4, 'required'=>TRUE, 'label'=>'','value'=>$email),
                                array('id'=>5, 'required'=>TRUE, 'label'=>'','value'=>$phone),
                                array('id'=>6, 'required'=>FALSE, 'label'=>'','value'=>$street),
                                array('id'=>7, 'required'=>FALSE, 'label'=>'','value'=>$suburb),
                                array('id'=>8, 'required'=>FALSE, 'label'=>'','value'=>$state),
                                array('id'=>9, 'required'=>FALSE, 'label'=>'','value'=>$postcode),
                                array('id'=>10, 'required'=>FALSE, 'label'=>'','value'=>$other),
                                array('id'=>11, 'required'=>FALSE, 'label'=>'','value'=>$mobile),
                                array('id'=>12, 'required'=>FALSE, 'label'=>'','value'=>$comments)   
                            );

                            echo get_fields($params); 
                        ?>
                    </div>
                <?php } elseif($enquiry === '11') { ?>
                    <div class="col-xs-12 col-sm-12 col-md-8">
                        <p>If you are interested in inviting the PPM Group to your region, company or organisation to share their property management philosophies, please let us know.</p>

                        <p>The PPM Group can deliver one of their training sessions or feel welcome to let us know what you want. We can tailor design a session to suit your needs.</p>

                        <p>All regional and interstate training is subject to achieving numbers.</p>
                        <?php

                            $params = array(
                                array('id'=>1, 'required'=>TRUE, 'label'=>'','value'=>$companyName),
                                array('id'=>16, 'required'=>FALSE, 'label'=>'','value'=>$eventName), 
                                array('id'=>17, 'required'=>FALSE, 'label'=>'','value'=>$othertrain), 
                                array('id'=>2, 'required'=>TRUE, 'label'=>'','value'=>$firstname),
                                array('id'=>3, 'required'=>TRUE, 'label'=>'','value'=>$surname),
                                array('id'=>4, 'required'=>TRUE, 'label'=>'','value'=>$email),
                                array('id'=>5, 'required'=>TRUE, 'label'=>'','value'=>$phone),
                                array('id'=>6, 'required'=>FALSE, 'label'=>'','value'=>$street),
                                array('id'=>7, 'required'=>FALSE, 'label'=>'','value'=>$suburb),
                                array('id'=>8, 'required'=>TRUE, 'label'=>'','value'=>$state),
                                array('id'=>9, 'required'=>FALSE, 'label'=>'','value'=>$postcode),
                                array('id'=>10, 'required'=>FALSE, 'label'=>'','value'=>$other),
                                array('id'=>11, 'required'=>FALSE, 'label'=>'','value'=>$mobile),
                                array('id'=>12, 'required'=>FALSE, 'label'=>'','value'=>$comments)   
                            );

                            echo get_fields($params); 
                        ?>
                    </div>
                <?php } elseif($enquiry === '13') { ?>
                    <div class="col-xs-12 col-sm-12 col-md-8">

                        <?php 
                            $params = array(
                                array('id'=>1, 'required'=>TRUE, 'label'=>'','value'=>$companyName),
                                array('id'=>2, 'required'=>TRUE, 'label'=>'','value'=>$firstname),
                                array('id'=>3, 'required'=>TRUE, 'label'=>'','value'=>$surname),
                                array('id'=>4, 'required'=>TRUE, 'label'=>'','value'=>$email)      
                            );

                            echo get_fields($params); 
                        ?>
                    </div>
                <?php } elseif($enquiry === '15' OR $enquiry === '16') { ?>
                    <div class="col-xs-12 col-sm-12 col-md-8">

                        <?php 
                            if($enquiry === '15') {
                                $params = array(
                                    array('id'=>1, 'required'=>TRUE, 'label'=>'','value'=>$companyName),
                                    array('id'=>2, 'required'=>TRUE, 'label'=>'','value'=>$firstname),
                                    array('id'=>3, 'required'=>TRUE, 'label'=>'','value'=>$surname),
                                    array('id'=>4, 'required'=>TRUE, 'label'=>'','value'=>$email),
                                    array('id'=>5, 'required'=>TRUE, 'label'=>'','value'=>$phone),
                                    array('id'=>18, 'required'=>FALSE, 'label'=>'','value'=>$prefDate,'value2'=>$myTime),
                                    array('id'=>12, 'required'=>FALSE, 'label'=>'','value'=>$comments)
                                );
                            } 
                            else {
                                $params = array(
                                    array('id'=>1, 'required'=>TRUE, 'label'=>'','value'=>$companyName),
                                    array('id'=>2, 'required'=>TRUE, 'label'=>'','value'=>$firstname),
                                    array('id'=>3, 'required'=>TRUE, 'label'=>'','value'=>$surname),
                                    array('id'=>4, 'required'=>TRUE, 'label'=>'','value'=>$email),
                                    array('id'=>5, 'required'=>TRUE, 'label'=>'','value'=>$phone),
                                    array('id'=>12, 'required'=>FALSE, 'label'=>'System Request','value'=>$comments)
                                );
                            }

                            echo get_fields($params); 
                        ?>
                    </div>
                <?php } ?>

                </div>
            <?php if(isset($isMain) && !$isMain) { ?>
                <?php if(!empty($enquiry)) { ?>
                    <div class="row">
                        <div class="form-group col-xs-12 col-sm-12 col-md-8">
                            <span class="col-sm-5 control-label">
                                <input type="submit" value="submit" class="btn btn-primary">
                            </span>
                        </div>
                    </div>
                <?php } ?>
            <?php } else { ?>
                    <div class="row">
                        <div class="form-group col-xs-12 col-sm-12 col-md-8">
                            <span class="col-sm-5 control-label">
                                <input id="submit-btn" type="button" value="submit" class="btn btn-primary">
                            </span>
                        </div>
                    </div>
            <?php } ?>

            </form>
        <?php } ?>

        </div>
    </div>
</div>

<?php if(isset($error)) { ?>

<script>
    $(document).ready(function(){
        var error = <?php echo $error; ?>;

        var errMsg = '';

        $.each(error.error, function(k, v){
            errMsg += '* ' + v + '\n';
        });

        new PNotify({
            title: 'Invalid or Required!',
            text: errMsg,
            type: 'error',
            styling: 'bootstrap3'
        });

        swal({title:"Failed!",text:"Some fields are required (*) or have invalid values",icon:'error',button:false, timer:3000});

    });
</script>

<?php } ?>

<?php if(isset($success)) { ?>

<script>
    $(document).ready(function(){
        var success = <?php echo $success; ?>;

        if(success.hasOwnProperty('response')) {
            swal({
                title: "Sent!",
                text: success.response,
                icon: "success",
                closeOnClickOutside: false
            });

            $('#enquiry-form')[0].reset();
        }

    });
</script>

<?php } ?>

<?php if(isset($failed)) { ?>

<script>
    $(document).ready(function(){
        var failed = <?php echo $failed; ?>;

        if(failed.hasOwnProperty('response')) {
            swal({
                title: "Failed!",
                text: failed.response,
                icon: "error",
                closeOnClickOutside: false
            });
        }

    });
</script>

<?php } ?>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/sweetalert/dist/sweetalert.min.js"></script>

<?php if(isset($isMain) && !$isMain) { ?>

    <?php if(empty($enquiry)) { ?>
    <div id="show-contact-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title"><font></font></h4>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">          
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function(){

            var modal       = $('#show-contact-modal');
            var enquiry     = $('#contact-form-enquiry');
            var title       = modal.find('.modal-title font');
            var body        = modal.find('.modal-body');

            var contact = {
                enquiryForm: null,
                init: function(){
                    enquiry.on('change', function(){
                        contact.getForm($(this).val(), $(this).find('option:selected').text());
                    });
                },
                getForm: function(id, form_title){
                    if($.isNumeric(id)) {
                        title.text(form_title);
                        body.html('');

                        var request = $.ajax({
                          url: "<?php echo base_url(); ?>contact-us/" + id,
                          method: "GET",
                          data:{form_id:id}
                        });
                         
                        request.done(function( data ) {

                            if(data!=null) {
                                body.html(data);

                                $('#submit-btn').on('click', function(){
                                    contact.submit(id);
                                });

                                contact.enquiryForm = $('#enquiry-form');

                                modal.modal({backdrop: "static"});

                                data = null;
                            }
                        });

                        request.error(function(_data){
                            if(_data.readyState == 4 && _data.status == 200) {}
                        });
                    }
                },
                submit: function(id){
                    if($.isNumeric(id)) {
                        var _data = contact.enquiryForm.serializeFormJSON();
                        _data.is_main = true;

                        var request = $.ajax({
                          url: "<?php echo base_url(); ?>contact-us/" + id,
                          method: "POST",
                          data: _data,
                          dataType: 'json'
                        });
                         
                        request.done(function( data ) {
                            if(data!=null) {
                                if(data.hasOwnProperty('error')) {
                                    var errMsg = '';

                                    $.each(data.error, function(k, v){
                                        errMsg += '* ' + v + '\n';
                                    });

                                    new PNotify({
                                        title: 'Invalid or Required!',
                                        text: errMsg,
                                        type: 'error',
                                        styling: 'bootstrap3'
                                    });

                                    swal({title:"Failed!",text:"Some fields are required (*) or have invalid values",icon:'error',button:false, timer:3000});
                                    
                                }
                                else if(data.hasOwnProperty('failed')){
                                    swal({
                                        title: "Failed!",
                                        text: data.failed.response,
                                        icon: "error",
                                        closeOnClickOutside: false
                                    });
                                }
                                else {
                                    //console.log(data);
                                    contact.enquiryForm[0].reset();

                                    if(data.hasOwnProperty('response')) {
                                        swal({
                                            title: "Sent!",
                                            text: data.response,
                                            icon: "success",
                                            closeOnClickOutside: false
                                        });
                                    }
                                }

                            }
                        });

                        request.error(function(_data){
                            swal({
                                title: "Request Error!",
                                text: 'Unable to send message. Please try again later',
                                icon: "error",
                                closeOnClickOutside: false
                            });
                        });
                    }
                }
            };

            contact.init();
        });

        (function ($) {
            $.fn.serializeFormJSON = function () {

                var o = {};
                var a = this.serializeArray();
                $.each(a, function () {
                    if (o[this.name]) {
                        if (!o[this.name].push) {
                            o[this.name] = [o[this.name]];
                        }
                        o[this.name].push(this.value || '');
                    } else {
                        o[this.name] = this.value || '';
                    }
                });
                return o;
            };
        })(jQuery);

    </script>

    <?php } ?>

<?php } ?>  
