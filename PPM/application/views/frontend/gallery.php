<?php

render_featured_img($slug);
    
?>

<style type="text/css">

a.img-holder {
    display: block;
    overflow: hidden;
}

a.img-holder img{
    transition: all .45s ease;
    transform: scale(1);
}
   
a.img-holder:hover img{
    transform: scale(1.1);
}

figure {
    overflow: hidden;
}

figure figcaption {
    color: #fff;
    background: rgba(1,1,1,0.8);
    width: calc(100% - 30px)!important;
    text-align: center;
    padding: 10px 5px;
    transition: all .25s ease;
    position: absolute;
    transform: translateY(100%);
}

figure:hover figcaption {
    transform: translateY(-95%);
}

</style>

<div class="container">
    <div class="row">
        <div class="col-md-3 left-column">
        <?php if(isset($sidemenu) && !empty($sidemenu)) echo $sidemenu; ?>
        </div>
        <div id="print-page" class="col-md-9 right-column">

            <?php if($page === 'album') { ?>
                <h1><?php echo $title; ?></h1>
                <p>Posted: <?php echo $myDate; ?></p>
                <p><?php echo $body; ?></p>
            <?php } else { ?>     

                

                <?php if($page !== 'display') { ?>
                    <h1><?php echo $title; ?></h1>
                    <?php if($membershipType >= 0 && $membershipType <= 3) { ?>
                    <p>Welcome to our members gallery.</p>
                    <p><a href="mailto:info@ppmsystem.com?subject=Photo Gallery Request">Click here</a> to request a photo to be added</p>
                    <?php } else { ?>
                    <p>Welcome to our property management gallery.</p>
                    <p><a href="mailto:info@ppmsystem.com?subject=Photo Gallery Request">Click here</a> to request a photo to be added to our gallery
                    (you will be required to attached a high res photo and description)</p>
                    <p>Feel welcome to navigate around our gallery, which features photos of our events, awards, social club gatherings and lots more.</p>
                    <?php } ?>

                <?php } else { ?>
                    <h1><?php echo $title; ?> <span class="grey">Gallery</span></h1>
                <?php } ?>        

            <?php } ?>

            <?php if($page === 'list' OR $page === 'home') { ?>

                <?php if(!empty($galleries)) { ?>
                
                <p>Number of galleries returned:  <?php echo $start_row; ?> to <?php echo $end_row; ?> records of <?php echo $total; ?> galleries</p>

                <div class="row">
                <?php foreach ($galleries as $key => $value) { 

                    $img = ($this->ppmsystemlib->get_file_type_by_ext($value['d1']) === 'img') ? base_url() . 'assets/uploads/files/' . $value['d1']: base_url() . 'assets/images/front/broken-image.gif';

                    $link = (isset($is_member) && $is_member) ? base_url() .  'members/gallery/album/' . $value['id'] : base_url() . 'gallery/album/' . $value['id'];
                ?>

                <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject" class="col-md-4">
                    <a href="<?php echo $link; ?>" itemprop="contentUrl" class="img-holder">
                    <img src="<?php echo $img; ?>" itemprop="thumbnail" alt="<?php echo $value['name']; ?>" width="100%" class="img-resposive"/>
                    </a>
                    <?php if(!empty($value['name'])) { ?>
                    <figcaption itemprop="caption description" class="album"><?php echo $value['name']; ?><br><?php echo $this->ppmsystemlib->check_date_time($value['mydate']); ?><br>
                        <?php echo $value['shortdesc']; ?></figcaption>
                    <?php } ?>
                </figure>                

                <?php } ?>
                </div>

                <?php if(!empty($pagination)) { ?>
                <div class="text-center"><?php echo $pagination; ?></div>
                <?php } ?>

                <?php } else { ?>
                <p>Sorry no galleries exist</p>
                <?php } ?>
            <?php } //END PAGE list?>
            
            <?php if($page === 'album') { ?>

                <?php if(!empty($pics)) { ?>
                <p>Number of photos returned: <?php echo count($pics); ?></p>

                <div class="row">

                    <div class="my-gallery" itemscope itemtype="http://schema.org/ImageGallery">

                <?php foreach ($pics as $key => $value) { 

                    $img = ($this->ppmsystemlib->get_file_type_by_ext($value['d1']) === 'img') ? base_url() . 'assets/uploads/files/' . str_replace(' ','%20',$value['d1']) : base_url() . 'assets/images/front/broken-image.gif';
                ?>
                    

                    <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject" class="col-md-4">
                        <a href="<?php echo $img; ?>" itemprop="contentUrl" class="img-holder">
                        <img src="<?php echo $img; ?>" itemprop="thumbnail" alt="<?php echo $value['caption']; ?>" width="100%" class="img-resposive"/>
                        </a>
                        <?php if(!empty($value['caption'])) { ?>
                        <figcaption itemprop="caption description"><?php echo $value['caption']; ?></figcaption>
                        <?php } ?>
                    </figure>

                    
                <?php } ?>
                    </div>

                </div>

                <?php } else { ?>
                <p>No photos are currently available for this gallery</p>
                <?php } ?>
            <?php } //END PAGE album?>

            <?php if($page === 'display') { ?>

                <?php if(!empty($galleries['items'])) { ?>

                <div class="row">
                <?php foreach ($galleries['items'] as $key => $value) { 

                    $img = ($this->ppmsystemlib->get_file_type_by_ext($value['d1']) === 'img') ? base_url() . 'assets/uploads/files/' . $value['d1']: base_url() . 'assets/images/front/broken-image.gif';

                    $link = base_url() . 'gallery/album/' . $value['id'];
                ?>

                <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject" class="col-md-4">
                    <a href="<?php echo $link; ?>" itemprop="contentUrl" class="img-holder">
                    <img src="<?php echo $img; ?>" itemprop="thumbnail" alt="<?php echo $value['name']; ?>" width="100%" class="img-resposive"/>
                    </a>
                    <?php if(!empty($value['name'])) { ?>
                    <figcaption itemprop="caption description" class="album"><?php echo $value['name']; ?><br><?php echo $this->ppmsystemlib->check_date_time($value['mydate']); ?><br>
                        <?php echo $value['shortdesc']; ?></figcaption>
                    <?php } ?>
                </figure>                

                <?php } ?>
                </div>

                <?php } else { ?>
                <p>Sorry no galleries exist</p>
                <?php } ?>

            <?php } //END PAGE display ?>       
        </div>
    </div>

</div>

</div>

<?php if($page === 'album') { ?>

<!-- Root element of PhotoSwipe. Must have class pswp. -->
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true" style="z-index:4;">

    <!-- Background of PhotoSwipe. 
         It's a separate element, as animating opacity is faster than rgba(). -->
    <div class="pswp__bg"></div>

    <!-- Slides wrapper with overflow:hidden. -->
    <div class="pswp__scroll-wrap">

        <!-- Container that holds slides. PhotoSwipe keeps only 3 slides in DOM to save memory. -->
        <div class="pswp__container">
            <!-- don't modify these 3 pswp__item elements, data is added later on -->
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>

        <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
        <div class="pswp__ui pswp__ui--hidden">

            <div class="pswp__top-bar">

                <!--  Controls are self-explanatory. Order can be changed. -->

                <div class="pswp__counter"></div>

                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                <button class="pswp__button pswp__button--share" title="Share"></button>

                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                <!-- Preloader demo https://codepen.io/dimsemenov/pen/yyBWoR -->
                <!-- element will get class pswp__preloader--active when preloader is running -->
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                      <div class="pswp__preloader__cut">
                        <div class="pswp__preloader__donut"></div>
                      </div>
                    </div>
                </div>
            </div>

            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div> 
            </div>

            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
            </button>

            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
            </button>

            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>

          </div>

        </div>
    </div>    

<script>
   $(document).ready(function(){

    var initPhotoSwipeFromDOM = function(gallerySelector) {

        // parse slide data (url, title, size ...) from DOM elements 
        // (children of gallerySelector)
        var parseThumbnailElements = function(el) {
            var thumbElements = el.childNodes,
                numNodes = thumbElements.length,
                items = [],
                figureEl,
                linkEl,
                size,
                item;

            for(var i = 0; i < numNodes; i++) {

                figureEl = thumbElements[i]; // <figure> element

                // include only element nodes 
                if(figureEl.nodeType !== 1) {
                    continue;
                }

                linkEl = figureEl.children[0]; // <a> element

                //size = linkEl.getAttribute('data-size').split('x');

                var theImage = new Image();
                theImage.src = linkEl.getAttribute('href');

                // Get accurate measurements from that.
                var imageWidth = theImage.width;
                var imageHeight = theImage.height;

                // create slide object
                item = {
                    src: linkEl.getAttribute('href'),
                    w: parseInt(imageWidth, 10),
                    h: parseInt(imageHeight, 10)
                };



                if(figureEl.children.length > 1) {
                    // <figcaption> content
                    item.title = figureEl.children[1].innerHTML; 
                }

                if(linkEl.children.length > 0) {
                    // <img> thumbnail element, retrieving thumbnail url
                    item.msrc = linkEl.children[0].getAttribute('src');
                } 

                item.el = figureEl; // save link to element for getThumbBoundsFn
                items.push(item);
            }

            return items;
        };

        // find nearest parent element
        var closest = function closest(el, fn) {
            return el && ( fn(el) ? el : closest(el.parentNode, fn) );
        };

        // triggers when user clicks on thumbnail
        var onThumbnailsClick = function(e) {
            e = e || window.event;
            e.preventDefault ? e.preventDefault() : e.returnValue = false;

            var eTarget = e.target || e.srcElement;

            // find root element of slide
            var clickedListItem = closest(eTarget, function(el) {
                return (el.tagName && el.tagName.toUpperCase() === 'FIGURE');
            });

            if(!clickedListItem) {
                return;
            }

            // find index of clicked item by looping through all child nodes
            // alternatively, you may define index via data- attribute
            var clickedGallery = clickedListItem.parentNode,
                childNodes = clickedListItem.parentNode.childNodes,
                numChildNodes = childNodes.length,
                nodeIndex = 0,
                index;

            for (var i = 0; i < numChildNodes; i++) {
                if(childNodes[i].nodeType !== 1) { 
                    continue; 
                }

                if(childNodes[i] === clickedListItem) {
                    index = nodeIndex;
                    break;
                }
                nodeIndex++;
            }



            if(index >= 0) {
                // open PhotoSwipe if valid index found
                openPhotoSwipe( index, clickedGallery );
            }
            return false;
        };

        // parse picture index and gallery index from URL (#&pid=1&gid=2)
        var photoswipeParseHash = function() {
            var hash = window.location.hash.substring(1),
            params = {};

            if(hash.length < 5) {
                return params;
            }

            var vars = hash.split('&');
            for (var i = 0; i < vars.length; i++) {
                if(!vars[i]) {
                    continue;
                }
                var pair = vars[i].split('=');  
                if(pair.length < 2) {
                    continue;
                }           
                params[pair[0]] = pair[1];
            }

            if(params.gid) {
                params.gid = parseInt(params.gid, 10);
            }

            return params;
        };

        var openPhotoSwipe = function(index, galleryElement, disableAnimation, fromURL) {
            var pswpElement = document.querySelectorAll('.pswp')[0],
                gallery,
                options,
                items;

            items = parseThumbnailElements(galleryElement);

            // define options (if needed)
            options = {

                // define gallery index (for URL)
                galleryUID: galleryElement.getAttribute('data-pswp-uid'),

                getThumbBoundsFn: function(index) {
                    // See Options -> getThumbBoundsFn section of documentation for more info
                    var thumbnail = items[index].el.getElementsByTagName('img')[0], // find thumbnail
                        pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
                        rect = thumbnail.getBoundingClientRect(); 

                    return {x:rect.left, y:rect.top + pageYScroll, w:rect.width};
                }

            };

            // PhotoSwipe opened from URL
            if(fromURL) {
                if(options.galleryPIDs) {
                    // parse real index when custom PIDs are used 
                    // http://photoswipe.com/documentation/faq.html#custom-pid-in-url
                    for(var j = 0; j < items.length; j++) {
                        if(items[j].pid == index) {
                            options.index = j;
                            break;
                        }
                    }
                } else {
                    // in URL indexes start from 1
                    options.index = parseInt(index, 10) - 1;
                }
            } else {
                options.index = parseInt(index, 10);
            }

            // exit if index not found
            if( isNaN(options.index) ) {
                return;
            }

            if(disableAnimation) {
                options.showAnimationDuration = 0;
            }

            // Pass data to PhotoSwipe and initialize it
            gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
            gallery.init();
        };

        // loop through all gallery elements and bind events
        var galleryElements = document.querySelectorAll( gallerySelector );

        for(var i = 0, l = galleryElements.length; i < l; i++) {
            galleryElements[i].setAttribute('data-pswp-uid', i+1);
            galleryElements[i].onclick = onThumbnailsClick;
        }

        // Parse URL and open gallery if it contains #&pid=3&gid=1
        var hashData = photoswipeParseHash();
        if(hashData.pid && hashData.gid) {
            openPhotoSwipe( hashData.pid ,  galleryElements[ hashData.gid - 1 ], true, true );
        }
    };

    // execute above function
    initPhotoSwipeFromDOM('.my-gallery');

   });
</script>

<?php } ?>