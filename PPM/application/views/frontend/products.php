<?php

if(!empty($slug)) render_featured_img($slug);
    
?>

<style>
.menu-active {
    font-weight: bolder;
    color: #A6A8AC !important;
}

.main-page-menu {
    list-style: none;
}

</style>

<div class="container">
    <div class="row">
        <div class="col-md-3 left-column">
            <ul class="main-page-menu">
            <?php if(!empty($slug)) render_menu($slug); ?>
            </ul>
        </div>
        <div class="col-md-9 right-column">

            <div class="row">

                <h1><?php echo $title; ?></h1>

            <?php 
                if(!empty($products)) { 
                    switch($page) {
                        case 'list':
                            _list($products, $products_start_row, $products_end_row, $products_total, $category, $this);
                            break;

                        case 'show':
                            details($products, $productCosts, $prodId, $category, $this);
                            break;

                        case 'gallery':
                            gallery($results, $this);
                            break;

                        default:
                            break;
                    }
                } 
            ?>

            <?php
                if(!empty($products_pagination) && intval($products_total) > 12) {
                    echo '<div class="text-center">'.$products_pagination.'</div>';
                }
            ?> 

            </div>
        </div>
    </div>
</div>


<?php function _list($results, $products_start_row, $products_end_row, $products_total, $category, $this_){ ?>
    <p>Number of products returned:  <?php echo $products_start_row; ?> to <?php echo $products_end_row; ?> records of <?php echo $products_total; ?> products</p>
    <p>&nbsp;</p>

    <?php if(!empty($results)) { ?>
        <div class="row">
        <?php foreach ($results as $key => $value) { ?>
        
            <div class="col-md-4">
            <?php if(!empty($value['d1'])) {?>
                    <img src="<?php echo base_url(); ?>assets/uploads/files/<?php echo $value['d1']?>" class="img-responsive" />
            <?php } else { ?>
            <p><img src="<?php echo base_url(); ?>assets/images/front/broken-image.gif" class="img-responsive" /></p>
            <?php } ?>
            <br/>

            <p><b><?php echo $value['code']; ?></b></p>

            <p><a href="<?php echo base_url(); ?>products/show/<?php echo (!empty($category)) ? $category : 0; ?>/<?php echo $value['id']; ?>" target="_blank" ><?php echo $value['name']; ?></a></p>

            <?php if(!empty($value['compability']) && $category === 90) { ?>
            <p>Author: <?php echo $value['compability']; ?></p>
            <?php }?>

            <?php if(!empty($value['shortdesc'])) { ?>
            <p><?php echo $value['shortdesc']; ?> <a href="<?php echo base_url(); ?>products/show/<?php echo (!empty($category)) ? $category : 0; ?>/<?php echo $value['id']; ?>" target="_blank" >more...</a></p>
            <?php }?>
            </div> 

        <?php } ?>
        </div>

    <?php } ?>
<?php } ?>

<?php function details($results, $productCosts, $prodId, $category, $this_) { ?>

    <div class="col-md-8">

    <?php foreach ($results as $key => $value) { ?>
        <p>
        <?php 
            $mytype = '';

            if($category === 34) {
                $mytype = 'a';
                if(!empty($value['compability'])) {
                    echo '<b>Author/Speaker</b>' . $value['compability'] . '<br>';
                }
            }
            elseif($category === 32) {
                $mytype = 's';
                if(!empty($value['compability'])) {
                    echo '<b>Compatibility</b>' . $value['compability'] . '<br>';
                } 
            }
            elseif($category === 33 OR $category === 90) {
                $mytype = 'b';
                if(!empty($value['compability'])) {
                    echo '<b>Author/Compatibility</b>' . $value['compability'] . '<br>';
                } 
            }
            else {
                if(!empty($value['compability'])) {
                    echo $value['compability'] . '<br>';
                } 
            }

        ?>        
        </p>

        <p><b>Product Description</b></p>
        <p><i><?php echo $value['shortDesc']; ?></i></p>
        <p><?php echo $value['body']; ?></p>

        <?php if(!empty($productCosts)) { ?>
            <h2>Investment<?php echo (count($productCosts) > 1) ? 's' : ''; ?></h2>

            <p><a href="<?php echo base_url() . 'members'; ?>">Click here</a> if you would like to be eligible for our member discount investments</p>

            <table cellpadding="0" cellspacing="0" border="0">
            <?php 
                foreach ($productCosts as $k => $v) {

                    $ordering_url   = '';
                    $price          = $this_->encryption->encrypt($v['cost']);
                    $postage        = $this_->encryption->encrypt($v['postage']);
                    $name           = $value['name'] . ' : ' . $v['name'];

                    $ordering_url   = 'ppmsystem/ppm/ordering/addtocart/?isproduct=true&id=' . $prodId .'&name='. urlencode($name) . '&price=' . urlencode($price) . '&comp='. urlencode($value['compability'])  .'&mytype=' . $mytype . '&postage=' . urlencode($postage);
            ?>

                <tr>
                    <td><span class="body"><?php echo $v['name']; ?>&nbsp;</span></td>
                    <td><span class="body">&dollar;<?php echo number_format($v['cost'], 2); ?>&nbsp;</span></td>
                    <td nowrap><span class="body"><a href="<?php echo base_url() . $ordering_url; ?>">Add to cart</a></span></td>
                </tr>

            <?php } ?>

            </table>
            
            <br/>

            <p>Do you need assistance with your property management department?<br/>
            <i>Fast track the development of your internal property management system</i><br/>
            <a href="<?php echo base_url(); ?>ppmsystem">Click here</a> to find out more</p>

        <?php } ?>

    <?php } ?>

    </div>

    <div class="col-md-4">
        <?php

            $event_img = '';
            $debbie_img = '';

            foreach ($results as $key => $value) {
                if(!empty($value['d1'])) {
                    $event_img  = $value['d1'];
                }

                if(!empty($value['d2'])) {
                    $debbie_img = $value['d2'];
                }
            }
        ?>

        <?php if(!empty($event_img)) {?>
                <img src="<?php echo base_url(); ?>assets/uploads/files/<?php echo $event_img; ?>" class="img-responsive" />
        <?php } ?>

        <?php if(!empty($debbie_img)) {?>
                <img src="<?php echo base_url(); ?>assets/uploads/files/<?php echo $debbie_img; ?>" class="img-responsive" />
        <?php } ?>

    </div>

<?php } ?>

<?php function gallery($results, $this_) { ?>

    <style type="text/css">

    a.img-holder {
        display: block;
        overflow: hidden;
    }

    a.img-holder img{
        transition: all .45s ease;
        transform: scale(1);
    }
       
    a.img-holder:hover img{
        transform: scale(1.1);
    }

    figure {
        overflow: hidden;
    }

    figure figcaption {
        color: #fff;
        background: rgba(1,1,1,0.8);
        width: calc(100% - 30px)!important;
        text-align: center;
        padding: 10px 5px;
        transition: all .25s ease;
        position: absolute;
        transform: translateY(100%);
    }

    figure:hover figcaption {
        transform: translateY(-95%);
    }

    </style>

    <?php if(!empty($results['items'])) { ?>
        <div class="row">
        <?php foreach ($results['items'] as $key => $value) { 

            $img = ($this_->ppmsystemlib->get_file_type_by_ext($value['d1']) === 'img') ? base_url() . 'assets/uploads/files/' . $value['d1']: base_url() . 'assets/images/front/broken-image.gif';

            $link = base_url() . 'gallery/album/' . $value['id'];
        ?>

        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject" class="col-md-4">
            <a href="<?php echo $link; ?>" itemprop="contentUrl" class="img-holder">
            <img src="<?php echo $img; ?>" itemprop="thumbnail" alt="<?php echo $value['name']; ?>" width="100%" class="img-resposive"/>
            </a>
            <?php if(!empty($value['name'])) { ?>
            <figcaption itemprop="caption description" class="album"><?php echo $value['name']; ?><br><?php echo $this_->ppmsystemlib->check_date_time($value['mydate']); ?><br>
                <?php echo $value['shortdesc']; ?>
            </figcaption>
            <?php } ?>
        </figure>                

        <?php } ?>
        </div>

    <?php } else { ?>
        <p>Sorry no galleries exist</p>
    <?php } ?>

<?php } ?>