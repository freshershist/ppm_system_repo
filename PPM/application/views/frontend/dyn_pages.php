

    <style>
    .menu-active {
        font-weight: bolder;
        color: #A6A8AC !important;
    }
    </style>
    <?php
        #featured image 
        render_featured_img($slug);
    ?>

    <div class="container">
        <div class="row">
            <div class="col-md-3 left-column">
                <?php
                    echo '<ul class="main-page-menu">';
                    render_menu($slug);
                    echo '</ul>';
                ?>
            </div>
            <div id="print-page" class="col-md-9 right-column">
                <h1><?php echo get_title_by_slug($slug); ?></h1>
                <?php
                    echo render_page_content($slug);
                ?>
            </div>
        </div>    
    </div>


    <?php
        #featured image 
        //render_featured_img($slug, 'bottom');

    ?> 

<style>
.main-page-menu {
    list-style: none;
}

.main-page-menu ul{
    list-style: none;
}
</style>