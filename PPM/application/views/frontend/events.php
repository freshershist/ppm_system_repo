<?php 
    if(isset($print)) { 

        if(!empty($results)) _list($results, $category, $this);

    } else { ?>

<?php

if(!empty($slug)) render_featured_img($slug);
    
?>

<style>
.menu-active {
    font-weight: bolder;
    color: #A6A8AC !important;
}

.main-page-menu {
    list-style: none;
}

</style>

<div class="container">
    <div class="row">
        <div class="col-md-3 left-column">
            <ul class="main-page-menu">
            <?php if(!empty($slug)) render_menu($slug); ?>
            </ul>

            <ul class="main-page-menu">
            <?php 

                for($i = 0; $i < 12; $i++) {
                    $now =  date('F Y', now(PPM_TIMEZONE));
                    $date = date('F Y', strtotime('+' . $i . ' month' , strtotime($now)));

                    $month = date('m', strtotime($date));
                    $year = date('Y', strtotime($date));

                    $total_days = days_in_month($month, $year);

                    $startDate = '1/'.$month.'/'.$year;

                    $endDate = $total_days.'/'.$month.'/'.$year;

                    echo '<li><a href="' . base_url() . 'events/list/?startDate='.$startDate.'&endDate='. $endDate . '">' . $date . '</a></li>';
                }

            ?>
            </ul>

            <ul class="main-page-menu">
                <li><a href="javascript:void(0);" onClick="window.open('<?php echo base_url(); ?>events/print','<?php echo $title; ?>','width=660,height=450,directories=no,location=no,menubar=no,scrollbars=yes,status=no,toolbar=no,resizable=yes');"><i class="fa fa-print"></i> Print Training Calendar</a></li>
            </ul>

            


        </div>
        <div class="col-md-9 right-column">

            <div class="row">

                <h1><?php echo $title; ?></h1>

            <?php 
                if(!empty($list) || !empty($results)) {
                    switch($page) {
                        case 'home':
                        case 'list':
                            if(!empty($list)) home($list, $category, $event_title, $this, !empty($results));
                            if(!empty($results)) _list($results, $category, $this);
                            else echo '<p>Sorry no events exist for this category/month</p>';
                            break;

                        case 'show':
                            details($results, $arr, $eventCosts, $this);
                            break;

                        case 'gallery':
                            gallery($results, $this);
                            break;

                        default:
                            break;
                    }
                }
            ?>

            </div>
        </div>
    </div>
</div>

<?php } ?>

<?php function home($results, $category, $event_title, $this_, $hasResults) { ?>

<p>Welcome to our property management training and events.</p>
<p>Our focus is to motivate, educate, coach and inspire you  and your property management team members to reach their full potential and  become market leaders.  Learn and  discover the 'top 10%' ideas, strategies and activities that you can embrace  that many of your competitors may not be.</p>
<p>The PPM Group offer a wide variety of property management  training for receptionists, support team members, property managers, leasing  consultants, business development managers, department managers and principals.</p>

<h2>LIST ALL EVENTS</h2>

<form action="<?php echo base_url() . 'events/list'; ?>" method="post" name="searchform" class="form-horizontal">
    <?php echo form_dropdown('keywords', $results, $event_title, 'onChange="document.searchform.submit();" class="form-control"'); ?>

</form>

<br/>

<?php if(!isset($print) && intval($category) === 97) { ?>    

<a href="<?php echo base_url(); ?>contact-us/11" target="_blank">Click here</a> to add an event to our page<br>

<br/>

<?php } ?>

<div class="alert alert-info alert-dismissible fade in" role="alert">
    <strong><i class="fa fa-exclamation-circle" style="font-size:18px;"></i></strong> Click on the map location to search a training event near you
</div>            

<!-- start accordion -->
<div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel">
        <a class="panel-heading" role="tab" id="headingOne" data-toggle="collapse" data-parent="" href="#collapseOne" aria-expanded="<?php if(!$hasResults) echo 'true';?>" aria-controls="collapseOne">
          <h4 class="panel-title">Map</h4>
        </a>
        <div id="collapseOne" class="panel-collapse collapse <?php if(!$hasResults) echo 'in';?>" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body text-center">

                <img src="<?php echo base_url(); ?>assets/images/front/map.png" alt="" usemap="#mapbd6cc4e6" border="0">
                <map name="mapbd6cc4e6">
                <area shape="poly" coords="313,233,323,231,330,240,339,247,333,255,321,245,314,241" href="<?php echo base_url(); ?>events/list/26" data-state="26" alt="act">
                <area shape="poly" coords="279,311,312,312,306,349" href="<?php echo base_url(); ?>events/list/24" data-state="24" alt="tas">
                <area shape="poly" coords="145,41,121,40,96,52,76,79,3,110,4,168,25,206,24,238,58,244,97,229,142,207" href="<?php echo base_url(); ?>events/list/23" data-state="23" alt="wa">
                <area shape="poly" coords="146,156,254,154,250,281,143,208" href="<?php echo base_url(); ?>events/list/22" data-state="22" alt="sa">
                <area shape="poly" coords="228,154,142,153,145,43,170,9,214,20,230,89" href="<?php echo base_url(); ?>events/list/25" data-state="25" alt="nt">
                <area shape="poly" coords="252,234,337,276,283,294,251,282" href="<?php echo base_url(); ?>events/list/21" data-state="21" alt="vic">
                <area shape="poly" coords="251,182,368,179,331,269,314,262,252,232" href="<?php echo base_url(); ?>events/list/20" data-state="20" alt="nsw">
                <area shape="poly" coords="229,155,229,62,252,72,261,3,288,43,292,67,309,84,339,109,366,152,367,178,254,183,255,154" href="<?php echo base_url(); ?>events/list/19" data-state="19" alt="qld">
                <area shape="poly" coords="427,331,475,233,430,184,417,231,407,280,366,322" href="<?php echo base_url(); ?>events/list/27" data-state="27" alt="nz"><br class="space">
                </map>
            </div>
        </div>
    </div>
</div>

<?php } ?>

<?php function _list($results, $category, $this_){ ?>


    <p>&nbsp;</p>
    <p>Number of articles returned: <?php echo count($results); ?></p>
    <p>&nbsp;</p>

    <?php if(!empty($results)) { ?>
        <div class="row">
        <?php foreach ($results as $key => $value) { ?>
        
            <div class="col-md-4">
            <?php if(!empty($value['d1'])) {?>
                    <img src="<?php echo base_url(); ?>assets/uploads/files/<?php echo $value['d1']?>" class="img-responsive" />
            <?php } else { ?>
            <p><img src="<?php echo base_url(); ?>assets/images/front/broken-image.gif" class="img-responsive" /></p>
            <?php } ?>
            <br/>
            <p><a href="<?php echo base_url(); ?>events/show/<?php echo (!empty($category)) ? $category : 0; ?>/<?php echo $value['id']; ?>" target="_blank" ><?php echo $value['name']; ?></a></p>

            <p>
                <?php echo $this_->ppmsystemlib->check_date_time($value['startDate']);?>
                <?php if(!empty($value['endDate'])) { ?>
                    to <?php echo $this_->ppmsystemlib->check_date_time($value['endDate']); ?>
                <?php } ?>
                : <span class="highlight"><?php echo $value['location2']; ?></span>
            </p>
            <p><b><?php echo $value['name']; ?></b> <?php echo $value['shortdesc']; ?> <a href="<?php echo base_url(); ?>events/show/<?php echo (!empty($category)) ? $category : 0; ?>/<?php echo $value['id']; ?>" target="_blank" >more...</a></p>
            </div> 

        <?php } ?>
        </div>

    <?php } ?>
<?php } ?>

<?php function details($results, $arr, $eventCosts, $this_) { ?>

    <div class="col-md-8">

    <?php foreach ($results as $key => $value) { ?>

        <?php $arr['location2'] = $value['location2']; ?>

        <h2><?php echo $value['name']; ?></h2>
        <p><?php echo $value['shortdesc']; ?></p>
        <p><b>Event Code:</b> <?php echo $value['code']; ?></p>
        <p><b>Location:</b> <?php echo !($arr['location'] === 'All' && $value['location2'] === 'All States') ? $arr['location'] . '&nbsp;' : ''; echo $value['location2']; ?></p>
        <?php if(!empty($value['venue'])) { ?>
            <p><b>Venue: </b> <?php echo $value['venue']; ?></p>
        <?php } ?>

        <?php if(!empty($value['startDate']) && !empty($value['endDate'])) { ?>
            <p><b>From: </b> <?php echo date('d/m/Y', strtotime($value['startDate'])); ?> to <?php echo date('d/m/Y', strtotime($value['endDate'])); ?></p>
        <?php } else { ?>
            <p><b>Date:</b> <?php echo date('d/m/Y', strtotime($value['startDate'])); ?></p>
        <?php } ?>            

        <?php if(!empty($value['myTime'])) { ?>
            <p><b>Time: </b> <?php echo $value['myTime']; ?></p>
        <?php } ?>    
        
        <?php if(!empty($value['numOfPlaces'])) { ?>
            <p><b>Number of Places Remaining:</b> <?php echo $value['numOfPlaces']; ?></p>
        <?php } ?>

        <?php if(!empty($value['body'])) { ?>
            <p><b>Event Description:</b> <br/><?php echo $value['body']; ?></p>
        <?php } ?>

        <?php if(!empty($value['whoShouldAttend'])) { ?>
            <p><b>Who Should Attend:</b> <?php echo $value['whoShouldAttend']; ?></p>
        <?php } ?>

        <?php if(!empty($value['included'])) { ?>
            <p><b>Inclusions:</b> <?php echo $value['included']; ?></p>
        <?php } ?>

        <?php if(!empty($value['d1']) && $this_->ppmsystemlib->get_file_type_by_ext($value['d1']) === 'doc') { ?>
            <p>Download: <a href="<?php echo base_url(); ?>assets/uploads/files/<?php echo $value['d1'];?>"><?php echo $value['d1'];?></a></p>
        <?php } ?>

        <?php if(!empty($value['d2']) && $this_->ppmsystemlib->get_file_type_by_ext($value['d2']) === 'doc') { ?>
            <p>Download: <a href="<?php echo base_url(); ?>assets/uploads/files/<?php echo $value['d2'];?>"><?php echo $value['d2'];?></a></p>
        <?php } ?>

        <?php if(!empty($value['d3']) && $this_->ppmsystemlib->get_file_type_by_ext($value['d3']) === 'doc') { ?>
            <p>Download: <a href="<?php echo base_url(); ?>assets/uploads/files/<?php echo $value['d3'];?>"><?php echo $value['d3'];?></a></p>
        <?php } ?>

        <?php if(!empty($eventCosts)) { ?>
            <h2>Investment<?php echo (count($eventCosts) > 1) ? 's' : ''; ?></h2>

            <p><a href="<?php echo base_url() . 'members'; ?>">Click here</a> if you would like to be eligible for our member discount investments</p>

            <table cellpadding="0" cellspacing="0" border="0">
            <?php 
                foreach ($eventCosts as $k => $v) {

                    $ordering_url   = '';
                    $price          = $this_->encryption->encrypt($v['cost']);
                    $name           = $value['name'] . ' : ' . $v['name'];
                    $checkMemberType= '';

                    $ordering_url   = 'ppmsystem/ppm/ordering/addtocart/?id=' . $value['id'] . '&shortdesc='. $value['shortdesc'] .'&name='. $name .'&online=' . $v['onlineEvent'] . '&isconference=' . $arr['isConference'] . '&price=' . $price . '&mytype=e&myDate=' . date('d/m/Y', strtotime($value['startDate'])) . '&location=' . $value['location2'];

                    if(is_numeric($v['limitToMemberType'])) {
                        if(intval($v['limitToMemberType']) === 5) {
                            $ordering_url = base_url() . $ordering_url;
                        }
                        elseif(is_numeric($v['limitToMemberType'])) {
                            $checkMemberType = '&checkMemberType=' . $this_->encryption->encrypt($v['limitToMemberType']);
                        }
                    }
                    else {
                        $ordering_url   = '';
                        $ordering_url = base_url() . 'ppmsystem/ppm/ordering/addtocart/?id=' . $value['id'] . '&shortdesc='. urlencode($value['shortdesc']) .'&name='. urlencode($name) .'&online=' . $v['onlineEvent'] . '&isconference=' . $arr['isConference'] . '&price=' . urlencode($price) . '&mytype=e&myDate=' . urlencode(date('d/m/Y', strtotime($value['startDate']))) . '&location=' . urlencode($value['location2']);
                    }
            ?>

                <tr>
                    <td><span class="body"><?php echo $v['name']; ?>&nbsp;</span></td>
                    <td><span class="body">&dollar;<?php echo number_format($v['cost'], 2); ?>&nbsp;</span></td>

                    <?php if(intval($v['limitToMemberType']) === 5) { ?>
                        <td nowrap><span class="body"><a href="<?php echo base_url(); ?>onlinetraining/autoredirect/?redirect=<?php echo urlencode($ordering_url); ?>">Add to cart</a></span></td>
                    <?php } elseif(is_numeric($v['limitToMemberType'])) { ?>
                        <?php if(intval($v['limitToMemberType']) >= 0) { ?>
                            <td nowrap><span class="body"><a href="<?php echo base_url(); ?>members/autoredirect/?redirect=<?php echo urlencode($ordering_url) . $checkMemberType; ?>">Add to cart</a></span></td>
                        <?php } ?>
                    <?php } else { ?>
                        <td nowrap><span class="body"><a href="<?php echo $ordering_url; ?>">Add to cart</a></span></td>
                    <?php } ?>

                </tr>

            <?php } ?>

            </table>
        <?php } ?>

    <?php } ?>

    </div>

    <div class="col-md-4">
        <?php

            $event_img = '';
            $debbie_img = '';

            foreach ($results as $key => $value) {
                if(!empty($value['d1'])) {
                    $event_img  = $value['d1'];
                }

                if(!empty($value['d2'])) {
                    $debbie_img = $value['d2'];
                }
            }
        ?>

        <?php if(!empty($event_img)) {?>
                <img src="<?php echo base_url(); ?>assets/uploads/files/<?php echo $event_img; ?>" class="img-responsive" />
        <?php } ?>

        <?php if(!empty($debbie_img)) {?>
                <img src="<?php echo base_url(); ?>assets/uploads/files/<?php echo $debbie_img; ?>" class="img-responsive" />
        <?php } ?>

    </div>

<?php } ?>

<?php function gallery($results, $this_) { ?>

    <style type="text/css">

    a.img-holder {
        display: block;
        overflow: hidden;
    }

    a.img-holder img{
        transition: all .45s ease;
        transform: scale(1);
    }
       
    a.img-holder:hover img{
        transform: scale(1.1);
    }

    figure {
        overflow: hidden;
    }

    figure figcaption {
        color: #fff;
        background: rgba(1,1,1,0.8);
        width: calc(100% - 30px)!important;
        text-align: center;
        padding: 10px 5px;
        transition: all .25s ease;
        position: absolute;
        transform: translateY(100%);
    }

    figure:hover figcaption {
        transform: translateY(-95%);
    }

    </style>

    <?php if(!empty($results['items'])) { ?>
        <div class="row">
        <?php foreach ($results['items'] as $key => $value) { 

            $img = ($this_->ppmsystemlib->get_file_type_by_ext($value['d1']) === 'img') ? base_url() . 'assets/uploads/files/' . $value['d1']: base_url() . 'assets/images/front/broken-image.gif';

            $link = base_url() . 'gallery/album/' . $value['id'];
        ?>

        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject" class="col-md-4">
            <a href="<?php echo $link; ?>" itemprop="contentUrl" class="img-holder">
            <img src="<?php echo $img; ?>" itemprop="thumbnail" alt="<?php echo $value['name']; ?>" width="100%" class="img-resposive"/>
            </a>
            <?php if(!empty($value['name'])) { ?>
            <figcaption itemprop="caption description" class="album"><?php echo $value['name']; ?><br><?php echo $this_->ppmsystemlib->check_date_time($value['mydate']); ?><br>
                <?php echo $value['shortdesc']; ?>
            </figcaption>
            <?php } ?>
        </figure>                

        <?php } ?>
        </div>

    <?php } else { ?>
        <p>Sorry no galleries exist</p>
    <?php } ?>

<?php } ?>