<div class="container">
    <div class="row">
        <div class="col-md-3 left-column"></div>
        <div class="col-md-9 right-column">

        <?php if($page === 'partners') { ?>
            <h1><?php echo $title; ?></h1>

            <p>Welcome to our  industry partners. </p>
            <p>Are you looking  for value added companies that specialise in property management, who can  assist with the development, support, management or growth of your business?</p>
            <p>The PPM Group's  goal is to make it easy for property management team members to search for  these specialised industry service providers from one easy location.</p>
            <p>Click on one of  the below links to display the full details</p>

            <?php if(!empty($categories)) { ?>

                <div class="row">
                <?php foreach ($categories as $key => $value) { ?>

                    <div class="col-md-4">
                        <?php if(!empty($value['d1'])) {?>
                                <img src="<?php echo base_url(); ?>assets/uploads/files/<?php echo $value['d1']?>" class="img-responsive" />
                        <?php } else { ?>
                        <p><img src="<?php echo base_url(); ?>assets/images/front/broken-image.gif" class="img-responsive" /></p>
                        <?php } ?>
                        <p><a href="<?php echo base_url(); ?>partners/listlinks/<?php echo $value['id']; ?>"><?php echo $value['name']; ?></a></p>
                    </div>

                <?php } ?>

                </div>
            <?php } else { ?>
                <h1><?php echo $title; ?></h1>
                <p>No subcategories exist for this category</p>
            <?php } ?>
        <?php 
        }//END PAGE partners
        ?>

        <?php if($page === 'show') { ?>
            <?php if(!empty($links)) { ?>
                <?php foreach ($links as $key => $value) { ?>

                <table cellpadding="0" cellspacing="0" border="0">
                    <tr>
                    <td valign="top">
                    <h1>Links : <?php //selectedCategory ?> <span class="grey"><?php echo $value['catname']; ?></span></h1>
                    <span class="header"><?php echo $value['name']; ?></span><br>
                    <span class="body">
                    <?php if(!empty($value['pcontact'])) { ?>
                        <b>Contact: </b> <?php echo $value['pcontact']; ?><br>
                    <?php } ?>
                    <?php if(!empty($value['address'])) { ?>
                        <b>Address: </b> <?php echo $value['address']; ?><br>
                    <?php } ?>
                    <?php if(!empty($value['phone'])) { ?>
                        <b>Phone: </b> <?php echo $value['phone']; ?><br>
                    <?php } ?>
                    <?php if(!empty($value['fax'])) { ?>
                        <b>Fax: </b> <?php echo $value['fax']; ?><br>
                    <?php } ?>
                    <?php if(!empty($value['email'])) { ?>
                        <b>Email: </b><a href="mailto:<?php echo $value['email']; ?>">click here to email</a><br>
                    <?php } ?>
                    <?php if(!empty($value['website'])) { ?>
                        <b>Website: </b><a href="http://<?php echo $value['website']; ?>" target="_blank"><?php echo $value['website']; ?></a><br>
                        <?php } ?>
                    <?php if(!empty($value['body'])) { ?>
                        <br><?php echo $value['body']; ?><br>
                    <?php } ?>
                    <br />
                    <?php
                        $filePath = '';

                        if(!empty($value['d1']) && $this->ppmsystemlib->get_file_type_by_ext($value['d1']) === 'doc'){
                            $filePath = $value['d1'];
                        }

                        if(!empty($value['d2']) && $this->ppmsystemlib->get_file_type_by_ext($value['d2']) === 'doc'){
                            $filePath = $value['d2'];
                        }
                    ?>

                    <?php if(!empty($filePath)) { ?>
                        <a href="<?php echo base_url(); ?>assets/uploads/files/<?php echo $filePath; ?>">Download file <?php echo $filePath; ?></a><br>
                    <?php } ?>

                    <?php if($isLogin && $discount === 'true') {?>
                        <br />  
                        <span class="header2">Member Discount</span>
                        <br><?php echo $value['memberDiscounts']; ?><br>
                     <?php } ?>


                </td>
                <td><img src="<?php echo base_url(); ?>/assets/images/front/spacer.gif" alt="" height="1" width="15" border="0"></td>
                <td bgcolor="#dddddd"><img src="<?php echo base_url(); ?>/assets/images/front/spacer.gif" alt="" height="1" width="1" border="0"></td>
                <?php 
                    $imgPath1 = '';
                    $imgPath2 = '';

                    if(!empty($value['d1']) && $this->ppmsystemlib->get_file_type_by_ext($value['d1']) === 'img'){
                        $imgPath1 = $value['d1'];
                    }

                    if(!empty($value['d2']) && $this->ppmsystemlib->get_file_type_by_ext($value['d2']) === 'img'){
                        $imgPath2 = $value['d2'];
                    }       
                    
                    if(!empty($imgPath1) OR !empty($imgPath2)) { ?>
                    <td><img src="<?php echo base_url(); ?>/assets/images/front/spacer.gif" alt="" height="1" width="15" border="0"></td>
                    <td valign="top" width="180"><br>
                        <?php if(!empty($imgPath1)) { ?>
                        <img src="<?php echo base_url(); ?>assets/uploads/files/<?php echo $imgPath1; ?>" class="img-responsive" />
                        <?php } ?>
                        <?php if(!empty($imgPath2)) { ?>
                        <img src="<?php echo base_url(); ?>assets/uploads/files/<?php echo $imgPath2; ?>" class="img-responsive" />
                        <?php } ?>
                    </td>

                <?php } ?>
                </tr>
                </table>                

                <?php } ?>
            <?php } else { ?>
                <p>Sorry but that link has been removed from our system</p>
            <?php } ?>
        <?php 
        }//END PAGE showdownload
        ?>

        <?php if($page === 'listlinks') { ?>
            <?php if(!empty($links)) { ?>
                <h1><?php echo $title; ?></h1>

                <?php if($categoryid!=36) { ?>
                <span class="body"><a href="mailto:info@ppmsystem.com?subject=Request a link to be added to:">click here</a> to request a link to be added</span><br><br>
                <?php } ?>

                <table>
                <tr>
                <td width="350" valign="top">
                <?php 
                    $tempData = 0;
                    //nationalSubCatArray
                ?>
                <?php foreach ($links as $key => $value) { ?>
                    <?php if($categoryid === 35) {
                            $nationalSubCat = intval($value['nationalSubCat']);
                            if($tempData != $nationalSubCat) {
                                $tempData = $nationalSubCat
                    ?>
                            <br><span class="header2"><?php $this->ppmsystemlib->get_data_arr('nationalSubCatArray')[$tempData - 1]; ?></span><br>
                    <?php 
                                }
                            }    
                    ?>
                    <span class="body"><a href="<?php echo base_url(); ?>partners/show/<?php echo $value['id']; ?>/<?php echo $subcatid; ?>/<?php echo $categoryid; ?>"><?php echo $value['name']; ?></a><br>

                    <?php if($categoryid != 145 && !empty($value['shortDesc'])) { ?>
                        <?php echo $value['shortDesc']; ?><br><br>
                    <?php } ?>

                <?php } ?>
                    </td>
                </tr>
                </table>
            <?php } else { ?>
                <h1><?php echo $title; ?></h1>

                <?php if($categoryid!=36) { ?>
                <span class="body"><a href="mailto:info@ppmsystem.com?subject=Request a link to be added to:">click here</a> to request a link to be added</span><br><br>
                <?php } ?>
                <p>Sorry but that link has been removed from our system</p>
            <?php } ?>
        <?php 
        }//END PAGE listlinks
        ?>
        </div>
    </div>
</div>