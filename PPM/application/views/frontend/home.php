

    <?php if(!empty($body)) { echo $body; } ?>


    <div class="container line"></div>

    <div class="container">
        <div id="circles" class="rotator">

            <?php 
                if(isset($featured) && !empty($featured)) {
                    foreach($featured as $key => $value) { 
            ?>
                <div>
                    <a class="circle" href="<?php echo (!empty($value->link)) ? base_url() . $value->link : 'javascript:;'; ?>">
                        <div class="content">
                            <img src="<?php echo base_url(); ?>assets/uploads/files/<?php echo $value->icon; ?>" alt="<?php echo $value->title; ?>">
                            <?php echo $value->title; ?>
                        </div>
                        <img src="<?php echo base_url(); ?>assets/uploads/files/<?php echo $value->photo; ?>" class="background" alt="<?php echo $value->title; ?>">
                    </a>
                </div>

            <?php 
                    }
                } 
            ?>
        </div>
    </div>

    <div class="container-grey">
        <p><a href="<?php echo base_url(); ?>contact-us/9/healthcheck" class="btn btn-white">CLICK HERE</a> <span class="visible-xs-inline"><br></span>&nbsp; TO REQUEST A FREE PROPERTY MANAGEMENT HEALTH CHECK QUESTIONNAIRE</a></p>
    </div>

    <div class="container clients">
        <p class="h1">What do our clients say?</p>
        <br>
        <div class="row">

            <?php foreach ($what_client_say as $key => $value) { ?>
            <div class="col-md-3 col-sm-6">
                <blockquote class="blockquote text-center">
                <p class="mb-0 small text-center darkgrey"><?php echo $value['body']; ?></p>
                <footer class="blockquote-footer"><?php echo $value['name']; ?> <cite title="Source Title"><?php echo $value['company']; ?></cite></footer>
                </blockquote>
            </div> 
            <?php } ?>

        </div>

    </div>
    
    <br>
    
    <div class="container clients">
        <p class="h1">Keeping you updated</p>
        <br>
        <div class="row">
            <?php foreach ($news_articles as $key => $value) { ?>
            <div class="col-md-3 col-sm-6">
                <a href="<?php echo base_url(); ?>articles/showarticle/<?php echo $value['id']; ?>"><img src="<?php echo base_url(); ?>assets/uploads/files/<?php echo $value['d1']; ?>" class="img-responsive"></a>
                <br>
                <p><strong><?php echo $value['name']; ?></strong></p>
                <p class="small darkgrey"><?php echo $value['shortdesc']; ?> <a href="<?php echo base_url(); ?>articles/showarticle/<?php echo $value['id']; ?>">more...</a> </p>

            </div>
            <?php } ?>

            <?php foreach ($links as $key => $value) { ?>
            <div class="col-md-3 col-sm-6">
                <a href="<?php echo base_url(); ?>articles/showarticle/<?php echo $value['id']; ?>"><img src="<?php echo base_url(); ?>assets/uploads/files/<?php echo $value['d1']; ?>" class="img-responsive"></a>
                <br>
                <p><strong><?php echo $value['name']; ?></strong></p>
                <p class="small darkgrey"><?php echo $value['shortdesc']; ?> <a href="<?php echo base_url(); ?>articles/showarticle/<?php echo $value['id']; ?>">more...</a> </p>

            </div>
            <?php } ?>

            <?php foreach ($ppmtv as $key => $value) { ?>
            <div class="col-md-3 col-sm-6">
                <a href="<?php echo $value['youtubelink']; ?>"><img src="<?php echo base_url(); ?>assets/uploads/files/<?php echo $value['d1']; ?>" class="img-responsive"></a>
                <br>
                <p><strong><?php echo $value['name']; ?></strong></p>
                <p class="small darkgrey"><?php echo $value['shortdesc']; ?> <a href="<?php echo $value['youtubelink']; ?>" target="_blank" >watch...</a> </p>

            </div>
            <?php } ?>            

        </div>

    </div>

    <div class="container">
        <p class="h1">Our Partners</p>
        <br/>
        <div id="partners" class="rotator">

            <?php 
                if(isset($partners) && !empty($partners)) {
                    foreach($partners as $key => $value) { 
            ?>
                <div>
                    <?php if(!empty($value->link)) { ?>
                        <a href="<?php echo $value->link; ?>" target="_blank">
                            <img src="<?php echo base_url(); ?>assets/uploads/files/<?php echo $value->photo; ?>" class="background" alt="">
                        </a>
                    <?php } else { ?>
                    <img src="<?php echo base_url(); ?>assets/uploads/files/<?php echo $value->photo; ?>" class="background" alt="">
                    <?php } ?>
                </div>

            <?php 
                    }
                } 
            ?>
        </div>
    </div>    

