<?php

render_featured_img($slug);
    
?>

<style>
    div.subregion {
        display: none;
    }

    .rr-suburbs.grey {
        text-decoration: line-through;
    }

    .menu-active {
        font-weight: bolder;
        color: #A6A8AC !important;
    }

    .main-page-menu {
        list-style: none;
    }

</style>

<div class="container">
    <div class="row">
        <div class="col-md-3 left-column">
            <ul class="main-page-menu">
            <?php render_menu($slug); ?>
            </ul>            
        </div>
        <div class="col-md-9 right-column">

        <?php if($page === 'rentroll') { ?>
            <h1><?php echo $title; ?></h1>

            <p>Welcome to our rent roll sales  service.</p>
            <p>The PPM Group offer a range of  services to assist agencies and property management departments in selling  their rent rolls and/or real estate business.</p>
            <p>We can assist in connecting  buyers and sellers with no brokerage fee with our classified service, or  facilitate the sales process from start to finish with our experienced and  knowledgeable national rent roll sale brokers.</p>
            <p> <a href="<?php echo base_url(); ?>contact-us/9" class="btn btn-default">Click here</a> to request further information on our rent roll sales services
            </p>   

            <div class="alert alert-info alert-dismissible fade in" role="alert">
                <strong><i class="fa fa-exclamation-circle" style="font-size:18px;"></i></strong> Click on the map below to search our rent roll sales listings
            </div>            

            <!-- start accordion -->
            <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel">
                    <a class="panel-heading" role="tab" id="headingOne" data-toggle="collapse" data-parent="" href="#collapseOne" aria-expanded="<?php if(empty($state)) echo 'true';?>" aria-controls="collapseOne">
                      <h4 class="panel-title">Map</h4>
                    </a>
                    <div id="collapseOne" class="panel-collapse collapse <?php if(empty($state)) echo 'in';?>" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body text-center">

                            <img src="<?php echo base_url(); ?>assets/images/front/map.png" alt="" usemap="#mapbd6cc4e6" border="0">
                            <map name="mapbd6cc4e6">
                            <area shape="poly" coords="313,233,323,231,330,240,339,247,333,255,321,245,314,241" href="<?php echo base_url(); ?>rent-roll/8" data-state="8" alt="act">
                            <area shape="poly" coords="279,311,312,312,306,349" href="<?php echo base_url(); ?>rent-roll/6" data-state="6" alt="tas">
                            <area shape="poly" coords="145,41,121,40,96,52,76,79,3,110,4,168,25,206,24,238,58,244,97,229,142,207" href="<?php echo base_url(); ?>rent-roll/5" data-state="5" alt="wa">
                            <area shape="poly" coords="146,156,254,154,250,281,143,208" href="<?php echo base_url(); ?>rent-roll/4" data-state="4" alt="sa">
                            <area shape="poly" coords="228,154,142,153,145,43,170,9,214,20,230,89" href="<?php echo base_url(); ?>rent-roll/7" data-state="7" alt="nt">
                            <area shape="poly" coords="252,234,337,276,283,294,251,282" href="<?php echo base_url(); ?>rent-roll/3" data-state="3" alt="vic">
                            <area shape="poly" coords="251,182,368,179,331,269,314,262,252,232" href="<?php echo base_url(); ?>rent-roll/2" data-state="2" alt="nsw">
                            <area shape="poly" coords="229,155,229,62,252,72,261,3,288,43,292,67,309,84,339,109,366,152,367,178,254,183,255,154" href="<?php echo base_url(); ?>rent-roll/1" data-state="1" alt="qld">
                            <area shape="poly" coords="427,331,475,233,430,184,417,231,407,280,366,322" href="<?php echo base_url(); ?>rent-roll/9" data-state="9" alt="nz"><br class="space">
                            </map>
                        </div>
                    </div>
                </div>

            <?php if(!empty($state)) { ?>

                <div class="panel">
                    <a class="panel-heading collapsed" role="tab" id="headingTwo" data-toggle="collapse" data-parent="" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                    <h4 class="panel-title"><?php echo $state; ?> <small>(<?php echo $total_rent_roll; ?>)</small></h4>
                    </a>
                    <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
                        <div class="panel-body">
                            <!-- <h2>Number of items found: <?php echo $total_rent_roll; ?></h2> -->

                            <div class="col-md-12 <?php if($total_rent_roll === 0) echo 'hide'; ?>">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Show:</h2>
                                        <ul class="nav navbar-right panel_toolbox" style="min-width: auto!important;">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                            </li>
                                        </ul>
                                    <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content" style="display: block;">

                                        <div class="">
                                                <ul class="to_do">

                                <?php if(isset($sub_region)) { ?>
                                    
                                    <?php foreach ($sub_region as $key => $value) { ?>
                                        <!--<a href="javascript:;" class="rr-suburbs rr-show" data-id="<?php echo $value['value']; ?>"><?php echo $value['name'] . ' <small>(' . $sub_region[$value['name']]['total'] .')</small>'; ?></a> | -->

                                        <li>
                                            <p><input data-id="<?php echo $value['value']; ?>" type="checkbox" class="flat" checked /> <?php echo $value['name'] . ' <small>(' . $sub_region[$value['name']]['total'] .')</small>'; ?> </p>
                                        </li>                                        
                                    <?php } ?>
                                <?php } ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">

                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#for-sale-section">For Sale <small id="total-for-sale">(<?php echo $for_sale_total; ?>)</small></a></li>
                                    <li><a data-toggle="tab" href="#wanted-section">Wanted <small id="total-wanted">(<?php echo $wanted_total; ?>)</small></a></li>
                                    <li><a data-toggle="tab" href="#sold-section">Sold <small id="total-sold">(<?php echo $sold_total; ?>)</small></a></li>
                                </ul>

                                <div class="tab-content">
                                    <div id="for-sale-section" class="tab-pane fade in active">
                                        <div class="col-md-12" style="padding-top: 20px;">
                                        <?php
                                            if(!empty($rent_roll) && isset($rent_roll['FOR SALE'])) {

                                                foreach ($rent_roll['FOR SALE'] as $key => $value) {
                                        ?>
                                                    <div class="subregion subregion-<?php echo $value['stateSubRegion'];?> show">
                                                        <span class="label label-success pull-right"><?php echo $value['name']; ?></span>
                                                        <span class="body"><b>Ref:</b> <?php echo $value['code']; ?><br/></span>

                                                        <?php if(intval($value['numberOfEnquiries']) > 0) { ?>
                                                        <b>Current number of enquiries: </b><?php echo $value['numberOfEnquiries']; ?><br/>
                                                        <?php } ?>

                                                        <b>Location:</b> <?php echo $value['location']; ?><br />

                                                        <?php echo $value['shortdesc']; ?><br/>
                                                        <a href="<?php echo base_url() . 'contact-us/5/' . $value['id']; ?>">click here</a> to find out more<br/>

                                                        <hr class="style-two"/>
                                                    </div>
                                        <?php
                                                }//end foreach

                                            }
                                        ?>
                                        <span class="no-item body hide">No rent roll details are currently available</span>

                                        </div>
                                    </div>

                                    <div id="wanted-section" class="tab-pane fade">
                                        <div class="col-md-12" style="padding-top: 20px;">
                                        <?php
                                            if(!empty($rent_roll) && isset($rent_roll['WANTED'])) {

                                                foreach ($rent_roll['WANTED'] as $key => $value) {
                                        ?>
                                                    <div class="subregion subregion-<?php echo $value['stateSubRegion'];?> show">
                                                        <span class="label label-warning pull-right"><?php echo $value['name']; ?></span>
                                                        <span class="body"><b>Ref:</b> <?php echo $value['code']; ?><br/></span>

                                                        <?php if(intval($value['numberOfEnquiries']) > 0) { ?>
                                                        <b>Current number of enquiries: </b><?php echo $value['numberOfEnquiries']; ?><br/>
                                                        <?php } ?>

                                                        <b>Location:</b> <?php echo $value['location']; ?><br />

                                                        <?php echo $value['shortdesc']; ?><br/>
                                                        <a href="<?php echo base_url() . 'contact-us/5/' . $value['id']; ?>">click here</a> to find out more<br/>

                                                        <hr class="style-two"/>
                                                    </div>
                                        <?php
                                                }//end foreach

                                            }
                                        ?>
                                        <span class="no-item body hide">No rent roll details are currently available</span>

                                        </div>
                                    </div>

                                    <div id="sold-section" class="tab-pane fade">
                                        <div class="col-md-12" style="padding-top: 20px;">
                                        <?php
                                            if(!empty($rent_roll) && isset($rent_roll['SOLD'])) {

                                                foreach ($rent_roll['SOLD'] as $key => $value) {
                                        ?>
                                                    <div class="subregion subregion-<?php echo $value['stateSubRegion'];?> show">
                                                        <span class="label label-danger pull-right"><?php echo $value['name']; ?></span>
                                                        <span class="body"><b>Ref:</b> <?php echo $value['code']; ?><br/></span>

                                                        <?php if(intval($value['numberOfEnquiries']) > 0) { ?>
                                                        <b>Current number of enquiries: </b><?php echo $value['numberOfEnquiries']; ?><br/>
                                                        <?php } ?>

                                                        <b>Location:</b> <?php echo $value['location']; ?><br />

                                                        <?php echo $value['shortdesc']; ?><br/>
                                                        <a href="<?php echo base_url() . 'contact-us/5/' . $value['id']; ?>">click here</a> to find out more<br/>

                                                        <hr class="style-two"/>
                                                    </div>
                                        <?php
                                                }//end foreach

                                            }
                                        ?>
                                        <span class="no-item body hide">No rent roll details are currently available</span>

                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            <?php } ?>

            </div>
            <!-- end of accordion -->            


        <?php 
        }//END PAGE partners
        ?>

        </div>
    </div>
</div>

<script>

    $(document).ready(function(){
        var filterCollect           = $('.rr-suburbs');
        var totalForSale            = $('#total-for-sale');
        var totalWanted             = $('#total-wanted');
        var totalSold               = $('#total-sold');
        var totalForSaleSection     = $('#for-sale-section');
        var totalWantedSection      = $('#wanted-section');
        var totalSoldSection        = $('#sold-section');

        $('.collapse-link').on('click', function() {
            var $BOX_PANEL = $(this).closest('.x_panel'),
                $ICON = $(this).find('i'),
                $BOX_CONTENT = $BOX_PANEL.find('.x_content');
            
            // fix for some div with hardcoded fix class
            if ($BOX_PANEL.attr('style')) {
                $BOX_CONTENT.slideToggle(200, function(){
                    $BOX_PANEL.removeAttr('style');
                });
            } else {
                $BOX_CONTENT.slideToggle(200); 
                $BOX_PANEL.css('height', 'auto');  
            }

            $ICON.toggleClass('fa-chevron-up fa-chevron-down');
        });

        $('.close-link').click(function () {
            var $BOX_PANEL = $(this).closest('.x_panel');

            $BOX_PANEL.remove();
        });

        if ($("input.flat")[0]) {
            $(document).ready(function () {
                $('input.flat').iCheck({
                    checkboxClass: 'icheckbox_flat-green',
                    radioClass: 'iradio_flat-green'
                }).on('ifChecked', function(event){
                    toggleItem(event.target.attributes[0].value, true);
                }).on('ifUnchecked', function(event){
                    toggleItem(event.target.attributes[0].value, false);
                });
            });
        }

        function toggleItem (id, isChecked) {

            if(isChecked) {
                $('div.subregion-' + id).addClass('show');
            }
            else {
                $('div.subregion-' + id).removeClass('show');
            }

            updateCount();            
        }                  

        function updateCount() {
            var totalFS = totalForSaleSection.find('div.subregion.show').length;
            var totalW = totalWantedSection.find('div.subregion.show').length;
            var totalS = totalSoldSection.find('div.subregion.show').length;    

            totalForSale.text('('+ totalFS + ')');
            totalWanted.text('('+ totalW + ')');
            totalSold.text('('+ totalS + ')');


            if(totalFS == 0) {
                totalForSaleSection.find('span.no-item').removeClass('hide').addClass('show');
            } 
            else {
                totalForSaleSection.find('span.no-item').removeClass('show').addClass('hide');
            }

            if(totalW == 0) {
                totalWantedSection.find('span.no-item').removeClass('hide').addClass('show');
            } 
            else {
                totalWantedSection.find('span.no-item').removeClass('show').addClass('hide');
            }

            if(totalS == 0) {
                totalSoldSection.find('span.no-item').removeClass('hide').addClass('show');
            } 
            else {
                totalSoldSection.find('span.no-item').removeClass('show').addClass('hide');
            }
        }

        updateCount();
    });

</script>