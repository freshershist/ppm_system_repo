<?php

render_featured_img($slug);
    
?>

<style>
.menu-active {
    font-weight: bolder;
    color: #A6A8AC !important;
}

.main-page-menu {
    list-style: none;
}

</style>

<div class="container">
    <div class="row">
        <div class="col-md-3 left-column">
            <ul class="main-page-menu">
            <?php render_menu($slug); ?>
            </ul>
        </div>
        <div class="col-md-9 right-column">
            <h1><?php echo $title; ?></h1>

            <?php 
                if(!empty($results)) {
                    switch($page) {
                        case 'conference-details':
                            conference_details($results, $arr, $location, $eventCosts, $this);
                            break;

                        case 'speakers-program':
                            speakers_program($results, $this);
                            break;

                        case 'delegate-testimonials':
                            testimonials($results);
                            break;

                        case 'social-events':
                        case 'venue-accomodation':
                        case 'travel':
                        case 'sponsorship-opportunities':
                            $sub_page_content = (!empty($sub_page_content)) ? $sub_page_content : '';

                            common($results, $content, $files1, $files2, intval($file_arr_index), $this, $sub_page_content);
                            break;

                        case 'sponsor-testimonials':
                            testimonials($results);
                            break;

                        case 'sponsors':
                            sponsors($results, $sub_page_content, $this);
                            break;

                        case 'register-online':
                            register_online($results);
                            break;

                        case 'gallery':
                            gallery($results, $this);
                            break;

                        default:
                            break;
                    }
                }
                else {
                    switch($page) {
                        case 'conference-details':
                            
                            break;

                        case 'speakers-program':
                            break;

                        case 'delegate-testimonials':
                            break;

                        case 'social-events':
                        case 'venue-accomodation':
                        case 'travel':
                            break;

                        case 'sponsor-testimonials':
                            break;

                        case 'sponsors':
                        case 'sponsorship-opportunities':
                            if(!empty($sub_page_content)) {
                                display_content($sub_page_content);
                            }
                            break;

                        case 'register-online':
                            break;

                        case 'gallery':
                            break;

                        default:
                            break;
                    }
                }
            ?>

        </div>
    </div>
</div>

<?php function conference_details($results, $arr, $location, $eventCosts, $this_) { ?>

    <?php foreach ($results as $key => $value) { ?>
        <h2><?php echo $value['name']; ?></h2>
        <p><?php echo $value['shortdesc']; ?></p>
        <p><b>Event Code:</b> <?php echo $value['code']; ?></p>
        <p><b>Location:</b> <?php echo !($location === 'All' && $value['location2'] === 'All States') ? $location . '&nbsp;' : ''; echo $value['location2']; ?></p>
        <?php if(!empty($value['venue'])) { ?>
            <p><b>Venue: </b> <?php echo $value['venue']; ?></p>
        <?php } ?>

        <?php if(!empty($value['startDate']) && !empty($value['endDate'])) { ?>
            <p><b>From: </b> <?php echo date('d/m/Y', strtotime($value['startDate'])); ?> to <?php echo date('d/m/Y', strtotime($value['endDate'])); ?></p>
        <?php } else { ?>
            <p><b>Date:</b> <?php echo date('d/m/Y', strtotime($value['startDate'])); ?></p>
        <?php } ?>            

        <?php if(!empty($value['myTime'])) { ?>
            <p><b>Time: </b> <?php echo $value['myTime']; ?></p>
        <?php } ?>    
        
        <?php if(!empty($value['numOfPlaces'])) { ?>
            <p><b>Number of Places Remaining:</b> <?php echo $value['numOfPlaces']; ?></p>
        <?php } ?>

        <?php if(!empty($value['body'])) { ?>
            <p><b>Event Description:</b> <br/><?php echo $value['body']; ?></p>
        <?php } ?>

        <?php if(!empty($value['whoShouldAttend'])) { ?>
            <p><b>Who Should Attend:</b> <?php echo $value['whoShouldAttend']; ?></p>
        <?php } ?>

        <?php if(!empty($value['included'])) { ?>
            <p><b>Inclusions:</b> <?php echo $value['included']; ?></p>
        <?php } ?>

        <?php if(!empty($value['d1']) && $this_->ppmsystemlib->get_file_type_by_ext($value['d1']) === 'doc') { ?>
            <p>Download: <a href="<?php echo base_url(); ?>assets/uploads/files/<?php echo $value['d1'];?>"><?php echo $value['d1'];?></a></p>
        <?php } ?>

        <?php if(!empty($value['d2']) && $this_->ppmsystemlib->get_file_type_by_ext($value['d2']) === 'doc') { ?>
            <p>Download: <a href="<?php echo base_url(); ?>assets/uploads/files/<?php echo $value['d2'];?>"><?php echo $value['d2'];?></a></p>
        <?php } ?>

        <?php if(!empty($value['d3']) && $this_->ppmsystemlib->get_file_type_by_ext($value['d3']) === 'doc') { ?>
            <p>Download: <a href="<?php echo base_url(); ?>assets/uploads/files/<?php echo $value['d3'];?>"><?php echo $value['d3'];?></a></p>
        <?php } ?>

        <?php if(!empty($eventCosts)) { ?>
            <h2>Investment<?php echo (count($eventCosts) > 1) ? 's' : ''; ?></h2>

            <p><a href="<?php echo base_url() . 'members'; ?>">Click here</a> if you would like to be eligible for our member discount investments</p>

            <table cellpadding="0" cellspacing="0" border="0">
            <?php 
                foreach ($eventCosts as $k => $v) {

                    $ordering_url   = '';
                    $price          = $this_->encryption->encrypt($v['cost']);
                    $name           = $value['name'] . ' : ' . $v['name'];
                    $checkMemberType= '';

                    $ordering_url   = 'ppmsystem/ppm/ordering/addtocart/?id=' . $value['id'] . '&shortdesc='. $value['shortdesc'] .'&name='. $name .'&online=' . $v['onlineEvent'] . '&isconference=' . $arr['isConference'] . '&price=' . $price . '&mytype=e&myDate=' . date('d/m/Y', strtotime($value['startDate'])) . '&location=' . $value['location2'];

                    if(is_numeric($v['limitToMemberType'])) {
                        if(intval($v['limitToMemberType']) === 5) {
                            $ordering_url = base_url() . $ordering_url;
                        }
                        elseif(is_numeric($v['limitToMemberType']) && intval($v['limitToMemberType']) >= 0) {
                            $checkMemberType = '&checkMemberType=' . $this_->encryption->encrypt($v['limitToMemberType']);
                        }
                    }
                    else {
                        $ordering_url   = '';
                        $ordering_url = base_url() . 'ppmsystem/ppm/ordering/addtocart/?id=' . $value['id'] . '&shortdesc='. urlencode($value['shortdesc']) .'&name='. urlencode($name) .'&online=' . $v['onlineEvent'] . '&isconference=' . $arr['isConference'] . '&price=' . urlencode($price) . '&mytype=e&myDate=' . urlencode(date('d/m/Y', strtotime($value['startDate']))) . '&location=' . urlencode($value['location2']);
                    }
            ?>

                <tr>
                    <td><span class="body"><?php echo $v['name']; ?>&nbsp;</span></td>
                    <td><span class="body">&dollar;<?php echo number_format($v['cost'], 2); ?>&nbsp;</span></td>

                    <?php if(intval($v['limitToMemberType']) === 5) { ?>
                        <td nowrap><span class="body"><a href="<?php echo base_url(); ?>onlinetraining/autoredirect/?redirect=<?php echo urlencode($ordering_url); ?>">Add to cart</a></span></td>
                    <?php } elseif(is_numeric($v['limitToMemberType'])) { ?>
                        <?php if(intval($v['limitToMemberType']) >= 0) { ?>
                            <td nowrap><span class="body"><a href="<?php echo base_url(); ?>members/autoredirect/?redirect=<?php echo urlencode($ordering_url) . $checkMemberType; ?>">Add to cart</a></span></td>
                        <?php } ?>
                    <?php } else { ?>
                        <td nowrap><span class="body"><a href="<?php echo $ordering_url; ?>">Add to cart</a></span></td>
                    <?php } ?>

                </tr>

            <?php } ?>

            </table>
        <?php } ?>


    <?php } ?>

<?php } ?>

<?php function speakers_program($results, $this_) { ?>

    <?php foreach ($results as $key => $value) { ?>
        <div class="row" style="padding-bottom: 2%;">
            <div class="col-md-3">
                <?php if(!empty($value['photo']) && $this_->ppmsystemlib->check_file_exists($value['photo'])) { ?>
                <div class="text-center"><img src="<?php echo base_url(); ?>assets/uploads/files/<?php echo $value['photo']; ?>"  /></div>
                <?php } else { ?>
                    <img src="<?php echo base_url(); ?>assets/images/front/broken-image.gif" width="200"/>
                <?php } ?>
            </div>
            <div class="col-md-9">

            <strong><?php echo $value['name']; ?></strong><br />
            <?php echo $value['description']; ?>
            </div>
        </div>
    <?php } ?> 

<?php } ?>

<?php function testimonials($results) { ?>

    <?php

    foreach ($results as $key => $value) { ?>

    <blockquote class="blockquote">
        <p class="mb-0"><?php echo str_replace("''", "'", word_limiter(strip_tags($value['body']), 20)) ; ?> <?php if(count(explode(' ', $value['body'])) > 21) { ?><a href="javascript:;" class="testi" data-container="body" title="<?php echo $value['name'] . ' - ' . $value['company']; ?>" data-trigger="focus" data-toggle="popover" data-placement="top" data-content="<?php echo strip_tags($value['body']); ?>"> more ... </a><?php } ?></p>
        <footer class="blockquote-footer"><?php echo $value['name']; ?> <cite title="<?php echo $value['company']; ?>"><?php echo $value['company']; ?></cite></footer>
    </blockquote>

    <?php } ?>

    <script>
        
        $(document).ready(function(){
            $('.testi').popover({
                container: 'body'
            });
        });

    </script>    

<?php } ?>

<?php function common($results, $content, $files1, $files2, $file_arr_index, $this_, $sub_page_content = '') { ?>
    <?php foreach ($results as $key => $value) { ?>
        <?php echo $content; ?>
    <?php } ?>

    <?php for($i = $file_arr_index; $i < $file_arr_index + 5; $i++) { ?>
        <?php if(!empty($files1[$i]) && $this_->ppmsystemlib->get_file_type_by_ext($files1[$i]) === 'doc') { ?>
            <a href="<?php echo base_url(); ?>assets/uploads/files/<?php echo $files1[$i]; ?>">Download <?php echo $files1[$i]; ?></a>
        <?php } ?>
    <?php } ?>

    <div class="row">

    <?php for($i = $file_arr_index; $i < $file_arr_index + 5; $i++) { ?>
        <?php if(!empty($files1[$i]) && $this_->ppmsystemlib->get_file_type_by_ext($files1[$i]) === 'img') { ?>
            <div class="col-md-4">
                <img src="<?php echo base_url(); ?>assets/uploads/files/<?php echo $files1[$i]; ?>" class="img-responsive" />
            </div>
        <?php } ?>
    <?php } ?>

    </div>

    <?php if(!empty($sub_page_content)) display_content($sub_page_content); ?>

<?php } ?>

<?php function register_online($results) { ?>


    <h2>Investment<?php echo (count($results) > 1) ? 's' : ''; ?></h2>

    <p><a href="<?php echo base_url() . 'members'; ?>">Click here</a> if you would like to be eligible for our member discount investments</p>

    <table cellpadding="0" cellspacing="0" border="0">
    <?php foreach ($results as $key => $value) { ?>

        <tr>
            <td><span class="body"><?php echo $value['name']; ?>&nbsp;</span></td>
            <td><span class="body">&dollar;<?php echo money_format('%.2n', $value['cost']); ?>&nbsp;</span></td>

            <?php if(intval($value['limitToMemberType']) === 5) { ?>
                <td nowrap><span class="body"><a href="/onlineTraining3.asp">Add to cart</a></span></td>
            <?php } elseif(!empty($value['limitToMemberType'])) { ?>
                <?php //if(intval($value['limitToMemberType']) >= 0) { ?>
                    <td nowrap><span class="body"><a href="/members.asp">Add to cart</a></span></td>
            <?php } else { ?>
                <td nowrap><span class="body"><a href="/ordering.asp">Add to cart</a></span></td>
            <?php } ?>

        </tr>
    <?php } ?> 
    </table>

<?php } ?>

<?php function gallery($results, $this_) { ?>

    <style type="text/css">

    a.img-holder {
        display: block;
        overflow: hidden;
    }

    a.img-holder img{
        transition: all .45s ease;
        transform: scale(1);
    }
       
    a.img-holder:hover img{
        transform: scale(1.1);
    }

    figure {
        overflow: hidden;
    }

    figure figcaption {
        color: #fff;
        background: rgba(1,1,1,0.8);
        width: calc(100% - 30px)!important;
        text-align: center;
        padding: 10px 5px;
        transition: all .25s ease;
        position: absolute;
        transform: translateY(100%);
    }

    figure:hover figcaption {
        transform: translateY(-95%);
    }

    </style>

    <?php if(!empty($results['items'])) { ?>
        <div class="row">
        <?php foreach ($results['items'] as $key => $value) { 

            $img = ($this_->ppmsystemlib->get_file_type_by_ext($value['d1']) === 'img') ? base_url() . 'assets/uploads/files/' . $value['d1']: base_url() . 'assets/images/front/broken-image.gif';

            $link = base_url() . 'gallery/album/' . $value['id'];
        ?>

        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject" class="col-md-4">
            <a href="<?php echo $link; ?>" itemprop="contentUrl" class="img-holder">
            <img src="<?php echo $img; ?>" itemprop="thumbnail" alt="<?php echo $value['name']; ?>" width="100%" class="img-resposive"/>
            </a>
            <?php if(!empty($value['name'])) { ?>
            <figcaption itemprop="caption description" class="album"><?php echo $value['name']; ?><br><?php echo $this_->ppmsystemlib->check_date_time($value['mydate']); ?><br>
                <?php echo $value['shortdesc']; ?>
            </figcaption>
            <?php } ?>
        </figure>                

        <?php } ?>
        </div>

    <?php } else { ?>
        <p>Sorry no galleries exist</p>
    <?php } ?>

<?php } ?>

<?php function sponsors($results, $sub_page_content, $this_) { ?>

    <?php if(!empty($results)) { ?>
        
        <?php foreach ($results as $key => $value) { ?>
            <div class="row"><?php echo $value['name']; ?></div>


            <div class="row">
                <div class="col-md-3"><img src="<?php echo $value['photo']; ?>" style="max-width:100%;" /></div></div>
                <div class="col-md-9">
                    
                    <strong><?php echo $value['name']; ?></strong><br />
                        <?php echo $value['description']; ?><br>") %>
                </div>
            </div>

        <?php } ?>

    <?php } ?>

    <?php if(!empty($sub_page_content)) display_content($sub_page_content); ?>

<?php } ?>

<?php function display_content($content) { ?>
<div class="row three-boxes">
    <?php echo $content; ?>
</div>
<?php } ?>