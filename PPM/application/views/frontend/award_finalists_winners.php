<style>
.main-page-menu {
    list-style: none;
}

.main-page-menu ul{
    list-style: none;
}
</style>

<?php
    #featured image 
    render_featured_img($slug);
?>

<div class="container">
    <div class="row">
        <div class="col-md-3 left-column">
            <?php
                echo '<ul class="main-page-menu">';
                render_menu($slug);
                echo '</ul>';
            ?>            
        </div>
        <div class="col-md-9 right-column">
            <h1><?php echo $title; ?></h1> 

			<p>Welcome to our Award Finalists and Winners.</p>
			<p>Congratulations on achieving this outstanding national title.</p>

            <form action="<?php echo base_url() . 'property-management-awards-winners'; ?>" method="GET" name="searchform" class="form-horizontal">
            	<div class="row">
            		<div class="col-md-4">
            			<?php echo form_dropdown('topic', $topics, $topic, 'onChange="document.searchform.submit();" class="form-control"'); ?>
            		</div>
            		<div class="col-md-4">
            			<?php echo form_dropdown('mytype', $types, $mytype, 'onChange="document.searchform.submit();" class="form-control"'); ?>
            		</div>
            		<div class="col-md-4">
            			<?php echo form_dropdown('myyear', $years, $myyear, 'onChange="document.searchform.submit();" class="form-control"'); ?>
            		</div>
            	</div>
            </form>

            <?php if(!empty($articles)) { ?>

                <p>&nbsp;</p>
                <p>Number of winners returned:  <?php echo $articles_start_row; ?> to <?php echo $articles_end_row; ?> records of <?php echo $articles_total; ?> articles</p>
                <p>&nbsp;</p>

                <div class="row">
                <?php foreach ($articles as $key => $value) { ?>
                
                    <div class="col-md-3">
                    <?php if(!empty($value['d1'])) {?>
                            <img src="<?php echo base_url(); ?>assets/uploads/files/<?php echo $value['d1']?>" class="img-responsive" />
                    <?php } else { ?>
	                    <p><img src="<?php echo base_url(); ?>assets/images/front/broken-image.gif" class="img-responsive" /></p>
	                    <?php } ?>
	                    <p></p>
	                    <p><strong><?php echo $value['name']; ?></strong><br><?php echo $value['myyear']; ?></p>
	                    <p class="small"><?php echo $value['body']; ?></p>
                    </div> 

                <?php } ?>
                </div>

                <?php
                    if(!empty($articles_pagination) && intval($articles_total) > 12) {
                        echo '<div class="text-center">'.$articles_pagination.'</div>';
                    }
                ?>

            <?php } else { ?>
                <p>&nbsp;</p>
                <p>Sorry no articles exist for this category</p>
            <?php } ?>

        </div>
    </div>
</div>