<div class="container">
    <div class="row">
        <div class="col-md-3 left-column">
            <div id="replacement-sub-nav">

                <p><a href="<?php echo base_url(); ?>onlinetraining">onlinetraining home</a></p>
            
            <?php if(isset($this->is_ppmsystem) && $this->is_ppmsystem) { ?>

                <p><a href="<?php echo base_url(); ?>members">members home</a></p>
                <p><a href="<?php echo base_url(); ?>ppmsystem-logout">members logout</a></p>                

            <?php } elseif(isset($this->is_onlinetraining) && $this->is_onlinetraining) { ?>
                <p><a href="<?php echo base_url(); ?>onlinetraining-logout">onlinetraining logout</a></p>
            <?php } ?>

            </div>
        </div>
        <div id="print-page" class="col-md-9 right-column">
            <h1><?php echo $title; ?></h1>

            <?php if($page === 'home') { ?>

                <?php if(isset($error)) { ?>
                <div class="alert alert-danger fade in" role="alert">
                <p class="text-danger"><?php echo $error; ?></p>
                </div>
                <?php } ?>

                <?php if($_SESSION['ot_id'] === -1) { ?>

                <?php } else { ?>
                <span class="body"><font color="red"><b>You have <?php echo $days_left; ?> days until your On-Line Training subscription expires.  <a href="<?php echo $subscribe_link; ?>"><br/>Click here to renew</a></b></font></span>
                <?php } ?>

                <h2>Search</h2>

                <form action="<?php echo base_url(); ?>onlinetraining" method="post" style="margin:0px;">
                    <p><strong>Topic:</strong></p>

                    <?php
                        if(!empty($topics)) {
                            echo $this->ppmsystemlib->createDropdown('topic', $topics, $topic, FALSE, TRUE, TRUE);
                        }
                    ?>
                    
                    <br />
                    <p><strong>Keywords:</strong></p>
                    <input type="text" maxlength="255" size="20" name="keywords" value="<?php echo $keywords; ?>" class="form-control">
            
                    <input type="submit" value="search">
                </form>                

                <h2>Welcome to our  on-line training training portal.</h2>
                <p>
                Our  focus is to motivate, educate and have you feeling refreshed and energised. <br />
                You can choose from our wide array of DVDs  and CD audio training sessions by searching for a topic title in our drop down search  option or utilise the global&nbsp;keyword search.</p>

                <?php if(!empty($training)) { ?>

                    <?php foreach ($training as $key => $value) { ?>
                    
                        <p style="margin-bottom:3px;">
                        <?php if(intval($value['myType']) === 1) { ?>
                            <img src="<?php echo base_url(); ?>assets/images/front/mmIcons_01.gif" style="vertical-align:middle" />
                        <?php } else { ?>
                            <img src="<?php echo base_url(); ?>assets/images/front/mmIcons_02.gif" style="vertical-align:middle" />
                        <?php } ?>&nbsp;
                        <span class="body"><?php echo $value['topic']; ?>&nbsp;</span></p>
                        <a href="<?php echo base_url() . 'onlinetraining/show/' . $value['id']; ?>"><b><?php echo $value['title']; ?></b></a><br>
                        <?php echo $value['shortDesc']; ?><br> 
                        <strong>Speaker:</strong> <?php echo $value['speaker']; ?><br> 
                        <strong> Duration: </strong> <?php echo $value['duration']; ?><br> 
                        <br />              

                    <?php } ?>

                <?php } else { ?>
                <p>No On-Line Training items were found for this criteria</p>
                <?php } ?>

            <?php }//END PAGE home ?>

            <?php if($page === 'signup') { ?>

                <p>Welcome to our online training.</p>
                <p>Our focus is to motivate, energise,  educate, coach and inspire you and your property management team to reach their  full potential and be market leaders.</p>
                <p>The online training subscription is for those busy agencies that  don't have the time for team members to be away from the office for the day.</p>
                <p>As an online training subscriber you will have access to our weekly  property management LIVE training broadcasts (including open forum discussion  and question time).  </p>
                <p>You will also receive special access to our DVD and audio  library portal where you can have 24 hour access to many topics and different  speakers that have been pre-recorded. </p>
                <p>There is an annual on-line credit  card fee of $999 incl. GST <strong>per  individual agency</strong> (less than $20.00 per week) for your entire agency to  access the training. </p>
                <p>The online training sessions are presented by Debbie Palmer  (Managing Director of the PPM Group) as well as many invited leading industry  expert guests.  </p>
                <p><a href="<?php echo base_url() . 'onlinetraining/sessions'; ?>">Click here</a> to view our online training session topics</p>
                <p><a href="<?php echo base_url() . 'events'; ?>">Click here</a> to view our training calendar</p>

                <h2>Free Trial Period</h2>
                <p style="margin-bottom:0;">Please enter your promotional code below</p>

                <form action="trainingtrial.asp?action=check" method="post" class="form-inline">
                    <input type="text" name="code" value="" class="form-control" /> <input type="submit" value="ENTER" class="btn btn-default"  class="form-control" />
                </form>

                <?php 
                    if($this->is_ppmsystem) { 
                        $ot_id = (isset($_SESSION['ot_id'])) ? $this->session->ot_id : $this->session->id;
                ?>

                    <p><a href="<?php echo base_url() . 'ppmsystem/ppm/onlinetraining/step1/' . $ot_id . '/' . $this->session->memberIdConfirm; ?>" class="btn btn-default">Click here</a> to become an on-line training subscriber</p>

                <?php } else { ?>     

                    <p><a href="<?php echo base_url() . 'ppmsystem/ppm/onlinetraining/step1'; ?>" class="btn btn-default">Click here</a> to become an on-line training subscriber</p>

                <?php } ?>

                <p>There is an annual online credit card fee of $999 incl. GST (Discounts apply to PPMsystem members via the member login area)</p>
                <p>There are copyright laws that apply to the training subscription where you cannot record or share the information to other agencies outside of your agency. </p>  

            <?php } ?>

            <?php if($page === 'sessions') { ?>

                <p>Welcome to a sneak peak selection  of some of our online training sessions.</p>
                <p>If you would like to know when a  particular training session is being delivered <a href="events.asp?action=list&all=true&category=">click here</a> links to list  all training &amp; events page</p>
                <ul>
                    <li>Advertising and renting properties</li>
                    <li>Delivering a 5 star service</li>
                    <li>Double your property management profits</li>
                    <li>How to be the master of your time</li>
                    <li>How to become a whiz PM negotiator</li>
                    <li>How to boost your PM's team morale</li>
                    <li>How to deduce your risk of a law suit</li>
                    <li>How to have the lowest arrears in town</li>
                    <li>How to secure the best tenant</li>
                    <li>Managing difficult clients</li>
                    <li>Managing the end of tenancy</li>
                    <li>Marketing tips, ideas and strategies</li>
                    <li>Preparing for a listing presentation </li>
                    <li>Safety tips for property managers</li>
                    <li>Smart routine inspections</li>
                    <li>Structuring your department</li>
                    <li>The ABC's of listing presentations – What to say in a listing  presentation</li>
                    <li>The first class real estate receptionist</li>
                    <li>The invisible business owner</li>
                    <li>What you need to know before building the rent roll</li>
                </ul>
                <p>
                <br><br>

            <?php } ?>

            <?php if($page === 'show') { ?>
            <div class="row">
                    
                <?php foreach ($training as $key => $value) { ?>
                <div class="col-md-9">
                    <h2><?php echo $value['title']; ?></h2>
                    <p><strong>Speaker:</strong>&nbsp;<?php echo $value['speaker']; ?></p>
                    <p><strong>Duration:</strong>&nbsp;<?php echo $value['duration']; ?></p>
                    <p><strong>Number of Hits:</strong>&nbsp;<?php echo $value['hits']; ?></p>

                    <?php if(intval($value['myType']) === 1) { ?>
                    
                        <video id="ppm-vid" class="video-js vjs-default-skin" controls preload="none" width="480" height="368" poster="<?php echo base_url(); ?>assets/images/front/ppm-video-poster.png" data-setup="{}">
                            <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
                        </video>

                        <script type="text/javascript">
                        
                            $(document).ready(function(){
                                videojs('ppm-vid', {
                                    sources: [{
                                        src: '<?php echo base_url(); ?>assets/uploads/files/<?php echo $value['mediaFile']; ?>',
                                        type: 'video/mp4'
                                    }]
                                });
                            });
                            
                        </script>

                    <?php } else { ?>
                        <p>
                            <video controls="" controlsList="nodownload" autoplay="" name="media" height="60"><source src="<?php echo base_url(); ?>assets/uploads/files/<?php echo $value['mediaFile']; ?>" type="audio/mpeg" autostart="false"></video>
                        </p>
                    <?php } ?>

                    <p><?php echo $value['shortDesc']; ?></p>
                    <p><?php echo $value['body']; ?></p>
                    <p><?php echo $value['activity']; ?></p> 

                    <?php for ($i=1; $i <= 5; $i++) { 
                        if($this->ppmsystemlib->get_file_type_by_ext($value['d'.$i]) === 'doc') {
                    ?>
                        <p><a href="<?php echo base_url() . 'assets/uploads/files/'. $value['d'.$i]; ?>" target="_blank"><?php echo $value['c'.$i]; ?></a></p>

                        <?php } ?> 
                    <?php } ?>                 
                </div>
                <div class="col-md-3">
                <?php for ($i=1; $i <= 5; $i++) { 
                    if($this->ppmsystemlib->get_file_type_by_ext($value['d'.$i]) === 'img') {
                ?>
                    <p><img src="<?php echo base_url() . 'assets/uploads/files/'. $value['d'.$i]; ?>" class="img-responsive img-rounded" width="180"/></p>
                    <?php if(!empty($value['c'.$i])) { ?>
                    <span class="body"><?php echo $value['c'.$i]; ?></span><br />
                    <?php } ?>

                    <?php } ?> 
                <?php } ?>    
                
                </div>

                <?php } ?>

                <?php if(empty($training)) { ?>
                <p>This item has recently been removed from our website</p>
                <?php } ?>

            </div>
            <?php } ?>

            <?php if($page === 'expired') { ?>

            <p><b>Your On-Line Training subscription has now expired. </b>
            <p>(Expired: <?php echo $membershipExpires; ?>)</p>

            <?php 
                $memberIdConfirm = ($this->is_ppmsystem) ? $this->session->memberIdConfirm : $this->session->ot_memberIdConfirm;
            ?>

            <p>To renew your On-Line Training subscription <a href="<?php echo base_url() . 'ppmsystem/ppm/onlinetraining/step2/' . $this->session->ot_id . '/' . $memberIdConfirm; ?>">click here</a></p>
            <?php } ?>
           
        </div>
    </div>    
</div>
