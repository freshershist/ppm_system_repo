<div class="container">
    <div class="row">
        <div class="col-md-3 left-column">
        <?php if($this->is_newsletter) { ?>
            <div id="replacement-sub-nav">
                <p><a href="<?php echo base_url(); ?>newsletter">Home</a></p>
                <p><a href="<?php echo base_url(); ?>newsletter-logout">logout</a></p>               
            </div>
        <?php } ?>
        </div>
        <div id="print-page" class="col-md-9 right-column">
            <h1><?php echo $title; ?></h1>

            <?php if($page === 'home') { ?>

                <?php if(isset($error)) { ?>
                <div class="alert alert-danger fade in" role="alert">
                <p class="text-danger"><?php echo $error; ?></p>
                </div>
                <?php } ?>

                <?php if($_SESSION['m_nl_id'] === -1) { ?>

                <?php } else { ?>
                <span class="body"><font color="red"><b>You have <?php echo $days_left; ?> days until your Landlord Newsletter subscription expires.  <a href="<?php echo $subscribe_link; ?>"><br/>Click here to renew</a></b></font></span>
                <?php } ?>

                <p>
                Click on either the <img src="<?php echo base_url(). 'assets/images/front/zip.gif'; ?>" border="0"> Zip or <img src="<?php echo base_url(). 'assets/images/front/word.gif'; ?>" border="0"> Word icon to download the newsletter to your computer.<br><br>
                <img src="<?php echo base_url(). 'assets/images/front/zip.gif'; ?>" border="0"> ZIP - Simply follow the prompts! You will be asked to save the newsletter to "Disk".  Choose which location or drive on your computer you wish to save the document.
                To find the document, go to Windows Explorer and navigate to where you saved the document.  You will then need to double click on the file to unzip the document. If you are not using Windows XP, you will need WinZip.  After you have unzipped the file double click it to open in Word.
                <br><br>
                        <img src="<?php echo base_url(). 'assets/images/front/word.gif'; ?>" border="0"> WORD - Depending on your Browser you could be asked to "Open" or "Save"  the newsletter.

                If you choose "Open", it will open in your Browser. You next click somewhere inside the document then press Ctrl A (this will select all) then Ctrl C (will copy all). Next open Word and on a blank document press Ctrl V (paste all), you can now edit the document to your requirements.

                If you choose "Save", choose a location on your computer to save the document to.  Now go to Windows Explorer and navigate to where you saved the document.  You will then need to double click on the file to open the document in Word.<br><br>                
                </p>

                <?php if($total > 0) { ?>
                    <p>Number of items returned: <?php echo $total; ?></p>

                    <div class="table-responsive">
                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped">
                        <tr>
                        <td>Issue</td>
                        <td>Zip</td>
                        <td>Word</td>
                        </tr>
                        <?php foreach ($enews as $key => $value) { ?>
                        <tr>
                        <td><?php echo $value['name']; ?></td>
                        <td>
                        <?php 

                            $imgPath = '';

                            if(!empty($value['d1']) && $this->ppmsystemlib->get_file_type_by_ext($value['d1']) === 'doc') {
                                $imgPath = base_url(). 'assets/images/front/word.gif';
                            }
                            elseif(!empty($value['d1']) && $this->ppmsystemlib->get_file_type_by_ext($value['d1']) === 'zip') {
                                $imgPath = base_url(). 'assets/images/front/zip.gif';
                            }
                        ?>

                        <a href="<?php echo base_url(); ?>enews/nl/d1/<?php echo $value['id']; ?>"><img src="<?php echo $imgPath; ?>" border="0"></a>&nbsp;

                        </td>
                        <td>
                        <?php 

                            $imgPath = '';

                            if(!empty($value['d2']) && $this->ppmsystemlib->get_file_type_by_ext($value['d2']) === 'doc') {
                                $imgPath = base_url(). 'assets/images/front/word.gif';
                            }
                            elseif(!empty($value['d2']) && $this->ppmsystemlib->get_file_type_by_ext($value['d2']) === 'zip') {
                                $imgPath = base_url(). 'assets/images/front/zip.gif';
                            }
                        ?>

                        <a href="<?php echo base_url(); ?>enews/nl/d2/<?php echo $value['id']; ?>"><img src="<?php echo $imgPath; ?>" border="0"></a>&nbsp;

                        </td>
                        </tr>
                        <?php } ?>
                    </table>
                    </div>
                <?php } else { ?>
                <p>No items are currently available</p>
                <?php } ?>

            <?php }//END PAGE home ?>

            <?php if($page === 'signup') { ?>

                <p>Welcome to our Landlord  Newsletter.  How exciting… Imagine never  having to write another landlord newsletter again.</p>
                <p>We invite you to save precious  time each month by downloading our professionally written newsletter targeted  specifically for landlords and investors.<br />
                </p>
                <p><a href="<?php echo base_url(); ?>assets/uploads/files/documents/newsletter example.pdf" target="_blank">Click here</a>  to download an example of  our End of Month Landlord Newsletter</p>
                <p>The newsletter is specially  written to educate your landlords (investors) on important property management  topics, how they can prosper with lots of smart investing tips, as well as  keeping them updated on your latest news.</p>
                <p>There is an annual on-line credit  card fee of $299 incl. GST <strong>per  individual agency</strong> (less than $25.00 per month) to access the  newsletter.  This includes 12 monthly  double sided issues. </p>
                <p>The newsletter is written in an  editable Word document format that you can customise easily by adding your logo  and you can change text, images and articles.</p>
                <p>The information provided in the  newsletter is of a generic nature and does not include specific State updates  such as legislation changes.  If you wish  to include specific information for your landlords, you can visit our PPM Group  News section on our website, which outlines State legislation and update changes  that you can add to the newsletter.</p>
                <p>To subscribe to the monthly  newsletter you must register online through our secure connection credit card  payment system. You will be asked to enter a unique login and password code at  the time of registering. A tax invoice will automatically be emailed to you  upon approval of payment. </p>
                <p>To download the newsletter you  will be required to visit our website each month.  The newsletter is uploaded to our website by  the first week of each month to allow agencies time to customise the document.</p>
                <p><a href="<?php echo base_url() . 'ppmsystem/ppm/newsletter/step1'; ?>" class="btn btn-default">CLICK HERE</a>  to become a subscriber for our  Landlord Newsletter
                </p>
                </p>
                <p>There is an annual online credit  card fee of $299 incl. GST (PPMsystem members receive the Landlord Newsletter  complimentary) </p>
                <p>There are copyright laws that  apply to the newsletter subscription where you cannot share the information to  other agencies outside of your office.  </p>
            <?php } ?>

            <?php if($page === 'renew') { ?>
                <span class="body">
                <p><b>Your newsletter subscription has now expired. </b></p>
                <p>(Expired: <?php echo $this->ppmsystemlib->check_date_time($_SESSION['m_nl_membershipExpires']); ?>)</p>
                <p>To renew your newsletter subscription <a href="<?php echo base_url() . 'ppmsystem/ppm/newsletter/step1/' . $_SESSION['m_nl_id'] . '/' . $_SESSION['m_nl_memberIdConfirm']; ?>">click here</a></p>
                </span>
            <?php } ?>

        </div>
    </div>


</div>

<?php if($page === 'home') { ?>

<div id="download-iframe" class="hide"></div>

<script type="text/javascript">
    $(document).ready(function(){
        var div = $('#download-iframe');
        
        <?php if(isset($file_download)) { ?>
            var iframe = $('<iframe></iframe>');
            iframe.attr({'src':'<?php echo base_url() . "assets/uploads/files/" . $file_download; ?>',width:'0',height:0});
            iframe.appendTo(div);
        <?php } ?>
    });
</script> 

<?php } ?>