<?php

render_featured_img($slug);
    
?>

<div class="container">
    <div class="row">
        <div class="col-md-3 left-column"></div>
        <div class="col-md-9 right-column">
            <h1><?php echo $title; ?></h1> 

            <p>Welcome to our property management PPM TV.</p>
            <p>Keeping  you up-to-date on legislation changes, cutting edge property  management processes, stories and bright ideas.</p>
            <p>We invite you to enjoy our many property management topics.</p>                                  

            <h2>Search Topic</h2>
            <form action="<?php echo base_url() . 'ppm-tv'; ?>" method="post" name="searchform" class="form-horizontal">
                <?php echo form_dropdown('topic', $topics, $topic, 'onChange="document.searchform.submit();" class="form-control"'); ?>

            </form>

            <?php if(!empty($articles)) { ?>

                <p>&nbsp;</p>
                <p>Number of videos returned:  <?php echo $articles_start_row; ?> to <?php echo $articles_end_row; ?> records of <?php echo $articles_total; ?> articles</p>
                <p>&nbsp;</p>

                <div class="row">
                <?php foreach ($articles as $key => $value) { ?>
                
                    <div class="col-md-4">
                    <?php if(!empty($value['youtubelink'])) {?>
                            <img src="<?php echo base_url(); ?>assets/uploads/files/<?php echo $value['d1']?>" class="img-responsive" />
                    <?php } else { ?>
                    <p><img src="<?php echo base_url(); ?>assets/images/front/broken-image.gif" class="img-responsive" /></p>
                    <?php } ?>
                    <p>&nbsp;</p>
                    <p><strong><?php echo $value['name']; ?></strong> <?php echo $value['shortdesc']; ?> <a href="<?php echo $value['youtubelink']; ?>" target="_blank" >more...</a></p>
                    </div> 

                <?php } ?>
                </div>

                <?php
                    if(!empty($articles_pagination) && intval($articles_total) > 12) {
                        echo '<div class="text-center">'.$articles_pagination.'</div>';
                    }
                ?>

            <?php } else { ?>
                <p>&nbsp;</p>
                <p>Sorry no articles exist for this category</p>
            <?php } ?>

        </div>
    </div>
</div>                        