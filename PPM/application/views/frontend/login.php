<?php

    if(isset($slug) && !empty($slug)) render_featured_img($slug);
    
?>

<div class="container">

    <div class="row">
        <div class="col-md-3 col-sm-12 col-xs-12 left-column">
        <?php if(isset($sidemenu) && !empty($sidemenu)) echo $sidemenu; ?>           
        </div>
        <div class="col-md-9 col-sm-12 col-xs-12 right-column">

        <?php if($page === 'members') { ?>

        <h1 class="text-left">PPMsystem Member Login</h1>
        <p>Welcome to our PPMsystem Member  Login.</p>
        <p>This is an exclusive access area  for property management agencies who have implemented the complete <a href="<?php echo base_url(). 'ppmsystem'; ?>">PPMsystem</a>. </p>

        <form action="<?php echo base_url();?>ppmsystem-check" method="post">
        <table>
        <tr>
            <td><span class="body">username:</span></td>
            <td><input type="text" name="username" size="12"></td>
        </tr>
        <tr>
            <td valign="top"><p style="margin-top:10px;">password:</p></td>
            <td valign="top"><input type="password" name="password" size="12">
            
            <p style="margin-left:5px;">Forgotten your password? <a href="<?php echo base_url(). 'ppmsystem/forgot-password'; ?>">click here</a></p>
            </td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" name="submitButtonName" value="login" style="margin-top:5px;"><br>
                
            </td>
        </tr>
        </table>
        </form>
        <span class="error"><?php if(isset($error)) echo $error; ?></span>

        <p><a href="<?php echo base_url(). 'ppmsystem/ppm/member/pricing'; ?>">Click here to become a PPMsystem Member</a></p>

        <?php } ?>

        <?php if($page === 'newsletter') { ?>
        <h1>Landlord Newsletter Subscriber Login</h1>
        <p>Welcome to our Landlord Newsletter Login.</p>
        <p>This is an exclusive access area for agencies who have subscribed to download our monthly <a href="<?php echo base_url() . 'newsletter'; ?>">landlord newsletter</a> each month.</p>

        <form action="<?php echo base_url();?>newsletter-check" method="post">
            <table border="0" cellspacing="2" cellpadding="0">
                <tr>
                    <td><span class="body">username:</span></td>
                    <td><input type="text" name="username" size="12"></td>
                </tr>
                <tr>
                    <td valign="top"><p style="margin-top:10px;">password:</p></td>
                    <td valign="top"><input type="password" name="password" size="12">
                    <p style="margin-left:5px;">Forgotten your password? <a href="<?php echo base_url() . 'newsletter-forgot-password'; ?>">click here</a></p>
                    
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" name="submitButtonName"  style="margin-top:5px;" value="login">
                    </td>
                </tr>
                
            </table>
        </form>
        
        <span class="error"><?php if(isset($error)) echo $error; ?></span>
        
        
        <p><a href="<?php echo base_url() . 'ppmsystem/ppm/newsletter/step1'; ?>">Click here to become a Newsletter Subscriber</a><br>
        <?php } ?>

        <?php if($page === 'onlinetraining') { ?>
        <h1>ONLINE TRAINING SUBSCRIBER LOGIN</h1>
        <p>Welcome to our Online Training portal login.</p>
        <p>This is an exclusive access area for agencies to view our recorded training archives covering a wide range of property management training topics that you can listen to anytime from anywhere as well as access to our weekly <a href="<?php echo base_url(); ?>onlinetraining">online training</a> broadcasts.</p>

        <form action="<?php echo base_url();?>onlinetraining-check" method="post">
            <table border="0" cellspacing="2" cellpadding="0">
                <tr>
                    <td><span class="body">username:</span></td>
                    <td><input type="text" name="username" size="12"></td>
                </tr>
                <tr>
                    <td valign="top"><p style="margin-top:10px;">password:</p></td>
                    <td valign="top"><input type="password" name="password" size="12">
                    <p style="margin-left:5px;">Forgotten your password? <a href="<%= script_name %>?action=forgotPassword">click here</a></p>
                    
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" name="submitButtonName"  style="margin-top:5px;" value="login">
                    </td>
                </tr>
                
            </table>
        </form>
        
        <span class="error"><?php if(isset($error)) echo $error; ?></span>

            <?php 
                if($this->is_ppmsystem) { 
                    $ot_id = (isset($_SESSION['ot_id'])) ? $this->session->ot_id : $this->session->id;
            ?>

                <p><a href="<?php echo base_url() . 'ppmsystem/ppm/onlinetraining/step1/' . $ot_id . '/' . $this->session->memberIdConfirm; ?>">Click here to become an on-line training subscriber</a></p>

            <?php } else { ?>     

                <p><a href="<?php echo base_url() . 'ppmsystem/ppm/onlinetraining/step1'; ?>">Click here to become an on-line training subscriber</a></p>

            <?php } ?>



        <?php } ?>

        </div>
    </div>

</div>