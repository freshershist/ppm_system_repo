<style type="text/css">
.menu-active {
    font-weight: bolder;
    color: #A6A8AC !important;
}
</style>

<div class="container">
    <div class="row">
        <div class="col-md-3 left-column">
        <?php if(isset($sidemenu) && !empty($sidemenu)) echo $sidemenu; ?>
        </div>
        <div id="print-page" class="col-md-9 right-column">
            <h1><?php echo $title; ?></h1>

            <?php if($page === 'home') { ?>

            <span class="error"><?php if(isset($error)) echo $error; ?></span>    

            <p>Welcome to our exclusive  members area.</p>
            <p>This area has been created to  keep you up-to-date and informed on changes to legislation, news, system and  member upgrades.</p>
            <p>We invite you to navigate around  the member's areas to search for articles of interest and discounts offered to  member agencies.</p>
            <p><a href="<?php echo base_url() . 'contact-us/9'; ?>">Click here</a> to let us know if  there is a particular issue, article or member feature that you would like to  see included in this area.</p>

            <?php 
                if(isset($top3_news_articles)) { 

                foreach ($top3_news_articles as $key => $news) {
                
            ?>
            <h2><?php if ($news['number'] === 1) { echo 'Featured'; } if($news['filter'] === 2) echo 'Articles'; else echo 'News';?></span>&nbsp;
            <a href="<?php echo base_url(); ?>members/listnews">list all</a></h2>

            <?php 
                    if(isset($news) && count($news['data']) > 0) { 
                        echo '<div class="row">';

                        foreach ($news['data'] as $value) {
                        ?>
                            <div class="col-md-4">
                            <?php if(!empty($value['d1'])) {?>
                                    <img src="<?php echo base_url(); ?>assets/uploads/files/<?php echo $value['d1']?>" class="img-responsive" />
                            <?php } else { ?>
                            <p><img src="<?php echo base_url(); ?>assets/images/front/broken-image.gif" class="img-responsive" /></p>
                            <?php } ?>
                            <p><?php echo $this->ppmsystemlib->check_date_time($value['myDate']);?></p>
                            <p><b><?php echo $value['name']; ?></b> <?php echo $value['shortdesc']; ?><br /><a href="<?php echo base_url(); ?>members/showarticle/<?php echo $value['id']; ?>" >more...</a></p>
                            </div>  

                        <?php
                        }

                        echo '</div>';
                    }
            ?>

            <?php 
                }//END FOREACH $top3_news_articles
            }//END ISSET $top3_news_articles
            ?>

            <?php if(isset($galleries)) { ?>
                <h2>Member Galleries <a href="<?php echo base_url(); ?>members/gallery">list all</a></h2>
                
            <?php } ?>

            <?php 
                if(isset($galleries)) {
                    echo '<div class="row">';
                    foreach ($galleries as $key => $value) {
            ?>

                <div class="col-md-4">
        
                <?php if(!empty($value['d1'])) { ?>
                    <img src="<?php echo base_url(); ?>assets/uploads/files/<?php echo $value['d1']; ?>" class="img-responsive" />
                    <p class="body">"<?php echo $value['caption']; ?></p>
                <?php } ?>
                    <span class="body"><a href="<?php echo base_url(); ?>members/gallery/album/<?php echo $value['id']; ?>"><?php echo $value['name']; ?></a><?php echo $value['body']; ?></span><br />
                </div>

            <?php 
                }//END FOREACH $galleries
                echo '</div>';
            }//END ISSET $galleries
            ?>

            <?php
            }//END PAGE home   
            ?>

            <?php if($page === 'listnews') { ?>
                <p>Welcome to our latest news, keeping you up-to-date on important information from across the nation.</p>
                <form action="<?php echo base_url(); ?>members/listnews" method="POST" class="form-inline">
                    <select name="category" class="form-control">
                        <option value="">[ SELECT ]</option>
                        <option value="1" <?php if(!empty($category) && $category === 1) echo 'selected'; ?>>General News</option>
                        <option value="2" <?php if(!empty($category) && $category === 2) echo 'selected'; ?>>Legislation News</option>
                    </select>

                    <?php 
                        if(!empty($state_arr)) {
                            echo $this->ppmsystemlib->createDropdown('state', $state_arr, $state, TRUE, TRUE, FALSE, '', '', 'FOR STATE');
                        }
                    ?>

                    <input type="submit" value="show" class="btn btn-default" />
                </form>

                <p>Number of articles returned:  <?php echo $start_row; ?> to <?php echo $end_row; ?> records of <?php echo $total; ?> articles</p>

                <?php 
                    if(isset($news) && !empty($news)) { 
                        echo '<div class="row">';

                        foreach ($news as $key => $value) {

                ?>
                    <div class="col-md-4">
                        <?php if(!empty($value['d1'])) { ?>
                        <p><img src="<?php echo base_url(); ?>assets/uploads/files/<?php echo $value['d1']; ?>" class="img-responsive" /></p>
                        <?php } else { ?>
                        <p><img src="<?php echo base_url(); ?>assets/images/front/broken-image.gif" class="img-responsive" /></p>
                        <?php } ?>
                        <?php echo $this->ppmsystemlib->check_date_time($value['mydate']);?>&nbsp;<b><?php echo $value['name']; ?></b><br><?php echo $value['shortdesc']; ?>   <a href="<?php echo base_url(); ?>members/showarticle/<?php echo $value['id']; ?>" >more...</a>
                    </div>


                <?php 
                        }//END FOREACH

                        echo '</div>';

                        if(!empty($pagination)) {
                            echo '<div class="text-center">'.$pagination.'</div>';
                        }
                    }
                    else {
                        echo '<span class="body">Sorry no articles exist. </span>';
                    }
                ?>
            <?php 
            }//END PAGE listnews
            ?>

            <?php if($page === 'showarticle') { ?>
                <?php if(!empty($article)) { ?>
                    
                            
                    <?php foreach ($article as $key => $value) {
                        
                            $imgPath = '';
                            $content = '';

                            if(!empty($value['d1']) && $this->ppmsystemlib->get_file_type_by_ext($value['d1']) === 'img'){
                                $imgPath = $value['d1'];
                                $content = $value['c1'];
                            }

                            if(!empty($value['d2']) && $this->ppmsystemlib->get_file_type_by_ext($value['d2']) === 'img'){
                                $imgPath = $value['d2'];
                                $content = $value['c2'];
                            }

                            if(!empty($value['d3']) && $this->ppmsystemlib->get_file_type_by_ext($value['d3']) === 'img'){
                                $imgPath = $value['d3'];
                                $content = $value['c3'];
                            }

                            if(!empty($value['d4']) && $this->ppmsystemlib->get_file_type_by_ext($value['d4']) === 'img'){
                                $imgPath = $value['d4'];
                                $content = $value['c4'];
                            }

                            if(!empty($value['d5']) && $this->ppmsystemlib->get_file_type_by_ext($value['d5']) === 'img'){
                                $imgPath = $value['d5'];
                                $content = $value['c5'];
                            }
                        ?>
                        <div class="row">
                            <?php if(!empty($imgPath)) { ?>
                                <div class="col-md-9">
                            <?php }else{ ?>
                                <div class="col-md-12">
                            <?php } ?>
                                <article>
                        <?php if(!empty($value['code'])) { ?> <b>Article Code:</b> <?php echo $value['code']; ?> <br> <?php } ?>
                        <?php if(!empty($value['location'])) { ?> <b>Location:</b> <?php echo $value['location']; ?> <br> <?php } ?>
                            <i><?php echo $value['shortdesc']; ?></i><br><br class="space">
                            <?php echo $value['body']; ?><br><br>
                            <?php if(!empty($value['source'])) { ?> <b>Author:</b> <?php echo $value['source']; ?><br><br><?php } ?>
                            <?php if(!empty($related_downloads)) { ?>
                                <span class="body"><b>Related Downloads</b><br><br>
                                <?php foreach ($related_downloads as $k => $v) { ?>
                                <span class="body"><?php echo $this->ppmsystemlib->check_date_time($v["dateposted"]); ?>&nbsp;<b><?php echo $v["name"]; ?></b><br><?php echo $v["shortdesc"]; ?> <a href="<?php echo base_url(); ?>members/showdownload/<?php echo $v["id"]; ?>" >more...</a><br><br></span>    
                                <?php } ?>
                            <?php } ?>
                                </article>

                            </div>

                        <?php if(!empty($imgPath)) { ?>
                            <div class="col-md-3">
                                <img src="<?php echo base_url(); ?>assets/uploads/files/<?php echo $imgPath; ?>" width="200"><br><br class="space"><span class="body"><?php echo $content; ?></span><br><br>
                            </div>
                        <?php } ?>
                        </div>
                    <?php } ?>
                <?php } else { ?>
                <p>Sorry that article has been removed from our system</p>
                <?php } ?>
            <?php 
            }//END PAGE showarticle
            ?>

            <?php if($page === 'showdownload') { ?>
                <?php if(!empty($downloads)) { ?>
                        <?php foreach ($downloads as $key => $value) { ?>
                        
                <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                <td valign="top">
                
                    <p><a href="javascript:void(0);" onClick="printDiv()"><i class="fa fa-print"></i> Print this page</a></p>
                    
                    
                    <span class="body">

                    <?php if(!empty($value['code'])) { ?> <b>Download Code:</b> <?php echo $value['code']; ?> <br> <?php } ?>
                    <?php if(!empty($value['name'])) { ?> <b>Name:</b> <?php echo $value['name']; ?> <br> <?php } ?>
                    <?php if(!empty($value['description'])) { ?> <b>Description:</b> <?php echo $value['description']; ?> <br><br /> <?php } ?>
                    <?php if(empty($value['description']) && !empty($value['shortdesc'])) { ?> <b>Description:</b> <?php echo $value['shortdesc']; ?> <br><br /> <?php } ?>
                    <?php if(!empty($value['documentlocation'])) { ?> <b>Location:</b> <?php echo $value['documentlocation']; ?> <br> <?php } ?>
                    <?php if(!empty($value['compatibility'])) { ?> <b>Compatibility:</b> <?php echo $value['compatibility']; ?> <br> <?php } ?>
                    
                    <br /><br />
                    You MUST choose the 'SAVE AS' option to download a system upgrade document <br />and NOT 'OPEN' from the webpage</p>
                    <div style="clear:both;"></div>
                    <?php
                        $filePath = '';

                        if(!empty($value['d1']) && $this->ppmsystemlib->get_file_type_by_ext($value['d1']) === 'doc'){
                            $filePath = $value['d1'];
                        }

                        if(!empty($value['d2']) && $this->ppmsystemlib->get_file_type_by_ext($value['d2']) === 'doc'){
                            $filePath = $value['d2'];
                        }

                        if(!empty($value['d3']) && $this->ppmsystemlib->get_file_type_by_ext($value['d3']) === 'doc'){
                            $filePath = $value['d3'];
                        }
                    ?>

                    <?php if(!empty($filePath)) { ?>
                        <a href="<?php echo base_url(); ?>assets/uploads/files/<?php echo $filePath; ?>" target="_blank">Click here to download: <?php echo $filePath; ?></a><br>
                    <?php } ?>

                    </span>

                
                </td>
                </tr>
                </table>

                        <?php } ?>
            <?php } else { ?>
                <p>That download has recently been removed from our system</p>
                <?php } ?>
            <?php 
            }//END PAGE showdownload
            ?>

            <?php if($page === 'articles') { ?>
                <?php if(!empty($categories)) { ?>
                    <p>Welcome to our property management articles resource centre.</p>
                    <p>Select a category from one of the below links or utilise our search facility to preview 100's of articles.</p>

                    <form action="<?php echo base_url(); ?>members/searcharticles" method="GET" class="form-inline">
                        <input type="text" name="keywords" maxlength="50" class="form-control" placeholder="ENTER AUTHOR OR TOPIC">
                        <input type="submit" value="search" class="btn btn-default">

                    </form>

                    <?php foreach ($categories as $key => $value) { ?>
                        <p><a href="<?php echo base_url(); ?>members/listarticles/<?php echo $value['id']; ?>" target="_blank"><?php echo $value['name']; ?></a></p>
                    <?php } ?>

            <?php } else { ?>
                <p>There are no archives found in our system.</p>
                <?php } ?>
            <?php 
            }//END PAGE articles
            ?>

            <?php if($page === 'listarticles' || $page === 'searcharticles') { ?>
                <?php if(!empty($articles)) { ?>

                    <p>Number of articles returned:  <?php echo $start_row; ?> to <?php echo $end_row; ?> records of <?php echo $total; ?> articles</p>
                    
                    <div class="row">
                    <?php foreach ($articles as $key => $value) { 
                            $imgPath = '';

                            if(!empty($value['d1']) && $this->ppmsystemlib->get_file_type_by_ext($value['d1']) === 'img'){
                                $imgPath = $value['d1'];
                            }

                            if(!empty($value['d2']) && $this->ppmsystemlib->get_file_type_by_ext($value['d2']) === 'img'){
                                $imgPath = $value['d2'];
                            }

                        ?>
                        <div class="col-md-4">
                            <?php if(!empty($imgPath)) { ?>
                            <p><img src="<?php echo base_url(); ?>assets/uploads/files/<?php echo $imgPath; ?>" class="img-responsive" /></p>
                            <?php } else { ?>
                            <p><img src="<?php echo base_url(); ?>assets/images/front/broken-image.gif" class="img-responsive" /></p>
                            <?php } ?>
                            <?php echo $this->ppmsystemlib->check_date_time($value['mydate']);?>&nbsp;<b><?php echo $value['name']; ?></b><br><?php echo $value['shortdesc']; ?>   <a href="<?php echo base_url(); ?>members/showarticle/<?php echo $value['id']; ?>">more...</a>
                        </div>
                    <?php } ?>
                    </div>

                    <?php
                        if(!empty($pagination)) {
                            echo '<div class="text-center">'.$pagination.'</div>';
                        }
                    ?>
            <?php } else { ?>
                <p>There are no archives found in our system.</p>
                <?php } ?>
            <?php 
            }//END PAGE listarticles
            ?>

            <?php if($page === 'discounts') { ?>

                <p>Welcome to our member discounts.  We are constantly working towards value adding to our member agencies. </p>
                <p><a href="javascript:;">Click here</a> if you would like the PPM Group to negotiate a special member rate with a particular service provider</p>

                <?php if(!empty($discounts)) { ?>

                    <div class="row">
                    <?php foreach ($discounts as $key => $value) { 
                            $imgPath = '';

                            if(!empty($value['d1']) && $this->ppmsystemlib->get_file_type_by_ext($value['d1']) === 'img'){
                                $imgPath = $value['d1'];
                            }
                        ?>
                        <div class="col-md-4">
                            <?php if(!empty($imgPath)) { ?>
                            <p><img src="<?php echo base_url(); ?>assets/uploads/files/<?php echo $imgPath; ?>" class="img-responsive" /></p>
                            <?php } else { ?>
                            <p><img src="<?php echo base_url(); ?>assets/images/front/broken-image.gif" class="img-responsive" /></p>
                            <?php } ?>
                            <h2><a href="<?php echo base_url(); ?>partners/show/<?php echo $value['id'];?>/<?php echo $value['categoryid'];?>?discount=true"><?php echo $value['name']; ?></a></h2>
                            <?php if(!empty($value['shortdesc'])) { ?><p><?php echo $value['shortdesc']; ?></p> <br> <?php } ?>
                        </div>
                    <?php } ?>
                    </div>

                <?php } else { ?>
                <p>Currently there are no discounts on offer</p>
                <?php } ?>
            <?php 
            }//END PAGE listarticles
            ?>    
            
            <?php if($page === 'systemupgrades' OR $page === 'listupgrades') { ?>

                <!-- start accordion -->
                <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
                  <div class="panel">
                    <a class="panel-heading" role="tab" id="headingOne" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="<?php echo ($page === 'systemupgrades') ? 'true': 'false'; ?>" aria-controls="collapseOne">
                      <h4 class="panel-title">Map</h4>
                    </a>
                    <div id="collapseOne" class="panel-collapse collapse <?php echo ($page === 'systemupgrades') ? 'in': ''; ?>" role="tabpanel" aria-labelledby="headingOne">
                      <div class="panel-body">
                        <?php if($membershipType >= 2) { ?> 
                            <p class="body">The PPM Group system upgrade option is exclusively available to PPMsystem platinum and gold members.  To upgrade your membership, please feel welcome to contact our office.</p>
                        <?php } else { ?>
                            <p>Click on your State to view a list of the PPMsystem system upgrades.</p>
                            <img src="<?php echo base_url(); ?>assets/images/front/map.png" alt="" usemap="#mapbd6cc4e6" border="0">
                            <map name="mapbd6cc4e6">
                            <area shape="poly" coords="313,233,323,231,330,240,339,247,333,255,321,245,314,241" href="<?php echo base_url(); ?>members/listupgrades/107" alt="act">
                            <area shape="poly" coords="279,311,312,312,306,349" href="<?php echo base_url(); ?>members/listupgrades/109" alt="tas">
                            <area shape="poly" coords="145,41,121,40,96,52,76,79,3,110,4,168,25,206,24,238,58,244,97,229,142,207" href="<?php echo base_url(); ?>members/listupgrades/110" alt="wa">
                            <area shape="poly" coords="146,156,254,154,250,281,143,208" href="<?php echo base_url(); ?>members/listupgrades/103" alt="sa">
                            <area shape="poly" coords="228,154,142,153,145,43,170,9,214,20,230,89" href="<?php echo base_url(); ?>members/listupgrades/108" alt="nt">
                            <area shape="poly" coords="252,234,337,276,283,294,251,282" href="<?php echo base_url(); ?>members/listupgrades/104" alt="vic">
                            <area shape="poly" coords="251,182,368,179,331,269,314,262,252,232" href="<?php echo base_url(); ?>members/listupgrades/106" alt="nsw">
                            <area shape="poly" coords="229,155,229,62,252,72,261,3,288,43,292,67,309,84,339,109,366,152,367,178,254,183,255,154" href="<?php echo base_url(); ?>members/listupgrades/111" alt="qld">
                            <area shape="poly" coords="427,331,475,233,430,184,417,231,407,280,366,322" href="<?php echo base_url(); ?>members/listupgrades/105" alt="nz"><br class="space">


                        <?php } ?>
                      </div>
                    </div>
                  </div>
                  <div class="panel">
                    <a class="panel-heading collapsed" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="<?php echo ($page === 'listupgrades') ? 'true': 'false'; ?>" aria-controls="collapseTwo">
                      <h4 class="panel-title">Upgrade List <small></small></h4>
                    </a>
                    <div id="collapseTwo" class="panel-collapse collapse <?php echo ($page === 'listupgrades') ? 'in': ''; ?>" role="tabpanel" aria-labelledby="headingTwo">
                      <div class="panel-body">
                        
                        <?php if(isset($total) && intval($total)>0) { ?>
                        <p class="body" style="margin:2px 0px 2px 0px">Number of downloads returned: <?php echo $total; ?></p>
                        <p style="color:#FF0000; margin:2px 0px 2px 0px">If the date is red this refers to a new system upgrade that has not been downloaded</p>

                        <table id="upgradetable" cellpadding="0" cellspacing="0">
                        <tr>
                            <td><strong>Date</strong></td>
                            <td><strong>Code</strong></td>
                            <td><strong>Type</strong></td>
                            <td><strong>Name</strong></td>
                            <td><strong>Location</strong></td>
                            <td>&nbsp;</td>
                        </tr>

                        <?php foreach ($downloads as $key => $value) { ?>
                            
                        <tr valign="top">
                            <td>
                                <?php if(!empty($value['dateposted']) && !empty($value['lastdownload']) && ($this->ppmsystemlib->get_unix_from_date($value['dateposted']) > $this->ppmsystemlib->get_unix_from_date($value['lastdownload'])) OR empty($value['lastdownload']) ) { ?>
                                 <b><font color="red"><?php echo $this->ppmsystemlib->check_date_time($value['dateposted']); ?></font></b>
                                 <?php } else { 
                                        echo $this->ppmsystemlib->check_date_time($value['dateposted']);
                                    } 
                                 ?>
                            </td>
                            <td><?php echo $value['code']; ?></td>
                            <td><?php echo $value['documenttype']; ?></td>
                            <td><?php echo $value['name']; ?></td>
                            <td><?php echo $value['documentlocation']; ?></td>
                            <td><a href="<?php echo base_url(); ?>members/showdownload/<?php echo $value['id']; ?>" target="_blank"><img src="<?php echo base_url(); ?>assets/images/front/download.png" border="0" /></a></td>
                        </tr>
                        <tr>
                            <td colspan="6"><?php echo $value['shortdesc']; ?></td>
                        </tr>

                        <?php } ?>

                        </table>

                        <?php } else { ?>
                        <p class="body" style="margin:2px 0px 2px 0px">Sorry no upgrades are currently available</p>
                        <?php } ?>

                      </div>
                    </div>
                  </div>
                </div>
                <!-- end of accordion -->

            <?php 
            }//END PAGE systemupgrades
            ?>

            <?php if($page === 'monthlystats') { ?>

                <?php if($membershipType >= 2) { ?>
                <p class="body">The PPM Group monthly awards initiative is exclusively available to PPMsystem platinum and gold members.  To upgrade your membership, please feel welcome to contact our office.</p>
                <?php } else { ?>

                <p>Welcome to our member awards.</p>

                <p>Our members' area has not only been designed to keep you updated but to feature our member's achievements.</p>

                <p><a href="mailto:support@ppmsystem.com?subject=Member Gallery Photo Submission">Click here</a> to email your photos and we will post them to our member gallery.</p>

                <p><a href="property-management-awards-member.asp">Click here</a> to review our member award categories</p>

                <p>Members must submit their figures every month to be eligible for the quarterly achievement award.  You may choose to participate in one or all of the categories</p>

                <h1>FOR THE MONTH OF: <?php echo date('F, Y', now()); ?><br />
                <strong>MUST BE SUBMITTED BY THE 5TH OF EVERY MONTH</strong></h1>

                <div class="alert alert-info fade in" role="alert">
                <p><strong>Compulsary</strong> fields are marked by an asterix ( <i class="fa fa-asterisk text-danger"></i> ) </p>
                </div>

                <form action="<%= script_name %>?action=saveMonthlyStatsInput" method="post" name="dataform" style="margin:2px;">
                    <table border="0" cellpadding="3" cellspacing="0" class="hascompulsory" style="margin-left:-3px;">
                    <tr>
                        <td><span class="fieldname"><strong>VACANCY RATE %</strong><br>
                        (Please retain rental listing sheet as at the end of the month for verification) Taken from monthly KPI Sheet </span></td>
                        <td>&nbsp;</td>
                        <td nowrap><input type="text" name="vacancyRate" size="10" value="<?php if(!empty($vacancyRate)) echo $vacancyRate; ?>" maxlength="10">&nbsp;%&nbsp;<i class="fa fa-asterisk text-danger"></i></td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td><span class="fieldname"><strong>RENT ARREARS RATE %</strong><br>
                        (Please retain rent arrears printout as at the end of the month for verification) Calculate from greater than 3 days. Taken from monthly KPI Sheet </span></td>
                        <td>&nbsp;</td>
                        <td><input type="text" name="rentArrears" size="10" value="<?php if(!empty($rentArrears)) echo $rentArrears; ?>" maxlength="10">&nbsp;%&nbsp;<i class="fa fa-asterisk text-danger"></i></td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td><span class="fieldname"><strong>RENTAL INCREASES GENERATED FOR THE DEPT.</strong><br>
                        Taken from properties let, rent increases &amp; tenancy renewal KPI Sheet<br />
                        <span class="fieldname red">Please list the monthly total increase in management fees for rent increases</span>
                        </span></td>
                        <td>$</td>
                        <td><input type="text" name="comissionIncrease" size="10" value="<?php if(!empty($comissionIncrease)) echo $comissionIncrease; ?>" maxlength="10">&nbsp;<i class="fa fa-asterisk text-danger"></i></td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td><span class="fieldname"><strong>DOLLAR VALUE FOR PROPERTIES GAIN FOR THE DEPT.</strong><br>
                        Taken from properties gained KPI Sheet</span></td>
                        <td>$</td>
                        <td><input type="text" name="propertiesGainedDollar" size="10" value="<?php if(!empty($propertiesGainedDollar)) echo $propertiesGainedDollar; ?>" maxlength="10">&nbsp;<i class="fa fa-asterisk text-danger"></i></td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td><span class="fieldname"><strong>DOLLAR VALUE FOR LOST PROPERTIES FOR THE DEPT.</strong><br>
                        Taken from properties lost KPI Sheet</span></td>
                        <td>$</td>
                        <td><input type="text" name="propertiesLostDollar" size="10" value="<?php if(!empty($propertiesLostDollar)) echo $propertiesLostDollar; ?>" maxlength="10">&nbsp;<i class="fa fa-asterisk text-danger"></i></td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td><span class="fieldname"><strong>NUMBER OF PROPERTIES GAINED FOR THE DEPT. </strong><br>
                        (Excludes properties purchased) Taken from properties gained KPI Sheet </span></td>
                        <td>&nbsp;</td>
                        <td><input type="text" name="numberOfPropertiesGained" size="10" value="<?php if(!empty($numberOfPropertiesGained)) echo $numberOfPropertiesGained; ?>" maxlength="10">&nbsp;<i class="fa fa-asterisk text-danger"></i></td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td><span class="fieldname"><strong>NUMBER OF PROPERTIES LOST FOR THE DEPT.</strong><br>
                        Taken from properties lost KPI Sheet</span></td>
                        <td>&nbsp;</td>
                        <td><input type="text" name="numberOfPropertiesLost" size="10" value="<?php if(!empty($numberOfPropertiesLost)) echo $numberOfPropertiesLost; ?>" maxlength="10">&nbsp;<i class="fa fa-asterisk text-danger"></i></td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="3">
                        <span class="fieldname"><strong>INDIVIDUAL ACHIEVEMENT by nomination<br />
                        SUPPORT AWARD</strong><br>
                        Write in less than 100 words why you believe a member of your support team should receive an achievement award.</span> <br />
                      <textarea name="individualAchievement" cols="60" rows="5" style="margin-top:10px; width:100%;"><?php if(!empty($individualAchievement)) echo $individualAchievement; ?></textarea>         </td>
                    </tr>
                    </table>

                <br>
                <input type="submit" value="Save" class="btn btn-default"><br>
                </form>

                <?php } ?>
            <?php }//END PAGE monthlystats ?>

            <?php if($page === 'newsletters') { ?>

                <?php if(isset($error)) { ?>
                <div class="alert alert-danger fade in" role="alert">
                <p class="text-danger"><?php echo $error; ?></p>
                </div>
                <?php } ?>

                <p>Welcome to our Landlord Newsletter.</p><p>As a member we email the newsletter to you at the beginning of each month to allow agencies the time to review update and personalise the information.</p>
                <?php if($total > 0) { ?>
                    <p>Number of items returned: <?php echo $total; ?></p>

                    <div class="table-responsive">
                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped">
                        <tr>
                        <td>Issue</td>
                        <td>Zip</td>
                        <td>Word</td>
                        <?php foreach ($enews as $key => $value) { ?>
                        <tr>
                        <td><?php echo $value['name']; ?></td>
                        <td>
                        <?php 

                            $imgPath = '';

                            if(!empty($value['d1']) && $this->ppmsystemlib->get_file_type_by_ext($value['d1']) === 'doc') {
                                $imgPath = base_url(). 'assets/images/front/word.gif';
                            }
                            elseif(!empty($value['d1']) && $this->ppmsystemlib->get_file_type_by_ext($value['d1']) === 'zip') {
                                $imgPath = base_url(). 'assets/images/front/zip.gif';
                            }
                        ?>

                        <a href="<?php echo base_url(); ?>enews/m/d1/<?php echo $value['id']; ?>"><img src="<?php echo $imgPath; ?>" border="0"></a>&nbsp;

                        </td>
                        <td>
                        <?php 

                            $imgPath = '';

                            if(!empty($value['d2']) && $this->ppmsystemlib->get_file_type_by_ext($value['d2']) === 'doc') {
                                $imgPath = base_url(). 'assets/images/front/word.gif';
                            }
                            elseif(!empty($value['d2']) && $this->ppmsystemlib->get_file_type_by_ext($value['d2']) === 'zip') {
                                $imgPath = base_url(). 'assets/images/front/zip.gif';
                            }
                        ?>

                        <a href="<?php echo base_url(); ?>enews/m/d2/<?php echo $value['id']; ?>"><img src="<?php echo $imgPath; ?>" border="0"></a>&nbsp;

                        </td>
                        </tr>
                        <?php } ?>
                    </table>
                    </div>
                <?php } else { ?>
                <p>No items are currently available</p>
                <?php } ?>

            <?php }//END PAGE newsletter ?>

            <?php if($page === 'surveyresults') { ?>
                <p>Welcome to our national survey results.</p>
                <p>How do you compare on a national scale?</p>

                <?php foreach ($polls as $key => $value) { ?>
                <p><b><?php echo $this->ppmsystemlib->check_date_time($value['endDate'])?></b><br />
                <?php echo $value['pollName']; ?><br /><a href="#<?php echo $value['id']; ?>" target="_blank">Click here for results</a></p>
                <?php } ?>

                <?php if(empty($polls)) { ?>
                <p>There are currently no polls to display</p>
                <?php } ?>

            <?php } ?>

            <?php if($page === 'expired') { ?>
                <p><b>Your PPMSystem Membership subscription has now expired. </b></p>
                <p>(Expired: <?php echo $membershipExpires; ?>)</p>
                <p>To renew your PPMSystem Membership subscription <a href="<?php echo base_url() . 'ppmsystem/ppm/member/step2/' . $this->session->id . '/' . $this->session->memberIdConfirm; ?>">click here</a></p>
            <?php } ?>
        </div>
    </div>


</div>

<?php if($page === 'showdownload') { ?>
<script type="text/javascript">

function printDiv() {
  var divToPrint=document.getElementById('print-page');
  var header=document.getElementById('header');
  var head = document.getElementsByTagName('head');
  var newWin=window.open('','Print-Window');

    header.children.rightlinks.style.display = 'none';
    header.children.menubar.style.display = 'none';

  newWin.document.open();

  newWin.document.write('<html><head>'+head[0].innerHTML+'</head><body onload="window.print()"><div class="container">'+header.innerHTML+'</div>'+divToPrint.innerHTML+'</body></html>');

  newWin.document.close();

  setTimeout(function(){newWin.close();},10);

}    

</script>
<?php } ?>

<?php if($page === 'newsletters') { ?>

<div id="download-iframe" class="hide"></div>

<script type="text/javascript">
    $(document).ready(function(){
        var div = $('#download-iframe');
        
        <?php if(isset($file_download)) { ?>
            var iframe = $('<iframe></iframe>');
            iframe.attr({'src':'<?php echo base_url() . "assets/uploads/files/" . $file_download; ?>',width:'0',height:0});
            iframe.appendTo(div);
        <?php } ?>
    });
</script>

<?php } ?>    