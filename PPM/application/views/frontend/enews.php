<?php

render_featured_img($slug);
    
?>

<style>
    .menu-active {
        font-weight: bolder;
        color: #A6A8AC !important;
    }
</style>

<div class="container">
    <div class="row">
        <div class="col-md-3 left-column"></div>
        <div id="print-page" class="col-md-9 right-column">
            <h1><?php echo $title; ?></h1>

            <?php 
                if(isset($top3_news_articles)) { 

                foreach ($top3_news_articles as $key => $news) {
                
            ?>
            <h2><?php if($news['filter'] === 1) echo 'Articles'; else echo 'News';?>&nbsp;
            <a href="<?php echo base_url(); ?>articles">list all</a></h2>

            <?php 
                    if(isset($news) && count($news['data']) > 0) { 
                        echo '<div class="row">';

                        foreach ($news['data'] as $value) {
                        ?>
                            <div class="col-md-4">
                            <?php if(!empty($value['d1'])) {?>
                                    <img src="<?php echo base_url(); ?>assets/uploads/files/<?php echo str_replace(' ', '%20', $value['d1']); ?>" class="img-responsive" alt="" />
                            <?php } else { ?>
                            <p><img src="<?php echo base_url(); ?>assets/images/front/broken-image.gif" class="img-responsive" alt="" /></p>
                            <?php } ?>
                            <br />
                            <p><strong><?php echo $value['name']; ?></strong><br /><br /> <?php echo $value['shortdesc']; ?> <a href="<?php echo base_url(); ?>articles/showarticle/<?php echo $value['id']; ?>" >more...</a></p>
                            </div>  

                        <?php
                        }

                        echo '</div>';
                    }
            ?>

            <?php 
                }//END FOREACH $top3_news_articles
            }//END ISSET $top3_news_articles
            ?>

            <?php 
                if(isset($top3_events)) { ?>
                    <h2>Training &amp; Events &nbsp; <a href="<?php echo base_url(); ?>events">list all</a>
                    </h2>
                    <div class="row">

            <?php
                    foreach ($top3_events as $key => $value) { 
            ?>
                        <div class="col-md-4">
                        <?php if(!empty($value['d1'])) {?>
                                <img src="<?php echo base_url(); ?>assets/uploads/files/<?php echo str_replace(' ', '%20', $value['d1']); ?>" class="img-responsive" alt="" />
                        <?php } else { ?>
                        <p><img src="<?php echo base_url(); ?>assets/images/front/broken-image.gif" class="img-responsive" alt="" /></p>
                        <?php } ?>
                        <br />
                        <p><a href="<?php echo base_url(); ?>events/<?php echo str_replace(' ', '%20', $value['d1']); ?>"><?php echo $value['name']; ?></a><br />
                        <?php echo $this->ppmsystemlib->check_date_time($value['startDate']); ?>
                        <?php if(!empty($value['endDate'])) { ?>
                        to <?php echo $this->ppmsystemlib->check_date_time($value['endDate']); ?>
                        <?php } ?> : 
                        <span class="highlight"><?php echo $value['location2']; ?></span><br><?php echo $value['shortdesc']; ?> <a href="<?php echo base_url(); ?>events/show/0/<?php echo $value['id']; ?>">more...</a></p>
                        </div>  
            <?php 
                    }//END FOREACH $top3_events

                    echo '</div>';

                }//END ISSET $top3_events
            ?>

            <?php 
                if(isset($top3_products)) { ?>
                    <h2>Products &amp; Services&nbsp; <a href="<?php echo base_url(); ?>products">list all</a></h2>
                    <div class="row">

            <?php
                    foreach ($top3_products as $key => $value) { 
            ?>
                        <div class="col-md-4">
                        <?php if(!empty($value['d1'])) {?>
                                <img src="<?php echo base_url(); ?>assets/uploads/files/<?php echo str_replace(' ', '%20', $value['d1']); ?>" class="img-responsive" alt=""/>
                        <?php } else { ?>
                        <p><img src="<?php echo base_url(); ?>assets/images/front/broken-image.gif" class="img-responsive" alt="" /></p>
                        <?php } ?>
                        <br />
                        <p><?php echo $value['code']; ?></p>
                        <p><a href="<?php echo base_url(); ?>products/show/0/<?php echo $value['id']; ?>"><?php echo $value['name']; ?></a><br />
                        <?php if(!empty($value['compability'])) { ?>
                        <br>Author: <?php echo $value['compability']; ?>
                        <?php } ?>
                        <?php if(!empty($value['shortdesc'])) { ?>
                        <br><?php echo $value['shortdesc']; ?>
                        <a href="<?php echo base_url(); ?>products/show/0/<?php echo $value['id']; ?>">more...</a><br>
                        <?php } ?>
                        </p>
                        </div>  
            <?php 
                    }//END FOREACH $top3_products

                    echo '</div>';

                }//END ISSET $top3_products
            ?>

            <?php if(!empty($sub_page_content)) { ?>
            <div class="row three-boxes">
                <?php echo $sub_page_content; ?>
            </div>
            <?php } ?>   

        </div>
    </div> 
</div>
