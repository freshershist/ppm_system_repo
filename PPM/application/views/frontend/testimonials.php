<?php

if(isset($slug))render_featured_img($slug);
    
?>

<style>
.menu-active {
    font-weight: bolder;
    color: #A6A8AC !important;
}

.main-page-menu {
    list-style: none;
}

</style>

<div class="container">
    <div class="row">
        <div class="col-md-3 left-column">
            <ul class="main-page-menu">
            <?php if(isset($slug))render_menu($slug); ?>
            </ul>
        </div>
        <div class="col-md-9 right-column">
            <h1><?php echo $title; ?></h1>

            <?php 
                if(!empty($results)) {
                    switch($page) {
                        case 'home':
                            home($results);
                            break;
                        case 'list':
                            _list($results, $category, $testimonials_start_row, $testimonials_end_row, $testimonials_total);
                            break;

                        default:
                            break;
                    }
                }
                else {
                    echo '<p>Sorry no testimonials exist for this category</p>';
                }
            ?>

            <?php
                if(!empty($testimonials_pagination) && intval($testimonials_total) > 12) {
                    echo '<div class="text-center">'.$testimonials_pagination.'</div>';
                }
            ?>             

        </div>
    </div>
</div>

<?php function home($results) { ?>

    <p>Welcome to our testimonials that  we are very proud to showcase.</p>
    <p>The PPM Group encourage all  feedback on what we are doing right and what we can do a little better.</p>
    <p>We invite you to share your  thoughts and feedback.</p>
                    
    <p><a href="mailto:info@ppmsystem.com?subject=Testimonial Submitted">Click here</a> to submit a testimonial.</p>


    <?php foreach ($results as $key => $value) { ?>

    <p><a href="<?php echo base_url(); ?>testimonials/list/<?php echo $value['id']; ?>"><?php echo $value['name']; ?> Testimonials</a></p>

    <?php } ?> 
<?php } ?>

<?php function _list($results, $category, $testimonials_start_row, $testimonials_end_row, $testimonials_total) { ?>

    <?php if($category === 71) { ?>
    <p>Welcome to our training testimonials.</p>
    <p>At the PPM Group we always encourage feedback on what we are doing right and what we can do a little better.</p>
    <p>We take pride in our 1000's of testimonials received.</p>
    <?php } ?>     

    <p>Number of testimonials returned:  <?php echo $testimonials_start_row; ?> to <?php echo $testimonials_end_row; ?> records of <?php echo $testimonials_total; ?> testimonials</p>
    <p>&nbsp;</p>

    <?php 



    foreach ($results as $key => $value) { ?>

    <blockquote class="blockquote">
        <p class="mb-0"><?php echo str_replace("''", "'", word_limiter(strip_tags($value['body']), 20)) ; ?> <?php if(count(explode(' ', $value['body'])) > 21) { ?><a href="javascript:;" class="testi" data-container="body" title="<?php echo $value['name'] . ' - ' . $value['company']; ?>" data-trigger="focus" data-toggle="popover" data-placement="top" data-content="<?php echo strip_tags($value['body']); ?>"> more ... </a><?php } ?></p>
        <footer class="blockquote-footer"><?php echo $value['name']; ?> <cite title="<?php echo $value['company']; ?>"><?php echo $value['company']; ?></cite></footer>
    </blockquote>

    <?php } ?> 
<?php } ?>

<script>
    
    $(document).ready(function(){
        $('.testi').popover({
            container: 'body'
        });
    });

</script>