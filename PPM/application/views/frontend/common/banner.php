<?php if(isset($banner)) { ?>
<div id="banner1" class="rotator">

  <div style="background-image:url('<?php echo base_url()?>assets/uploads/files/<?php echo $banner; ?>');">
    <div class="container">
      <p><?php echo $banner_text; ?></p>
    </div>
    <img src="<?php echo base_url()?>assets/uploads/files/<?php echo $banner; ?>" class="visible-xs-block" style="width:100%;" alt="" />
  </div>

</div>
<?php } ?>

<?php if(isset($slug) && !empty($slug)) render_featured_img($slug); ?>