<?php 
    if(isset($slug) && !empty($slug)) { 
        $partners = render_partners($slug);
        
		$partners = (!empty($partners[0]['photos'])) ? $this->ppmsystemlib->is_json($partners[0]['photos'], TRUE) : '';

        if(!empty($partners)) {
    ?>
        <div class="container">
            <p class="h1">Our Partners</p>
            <br/>
            <div id="partners" class="rotator">

                <?php 
                    if(isset($partners) && !empty($partners)) {
                        foreach($partners as $key => $value) { 
                ?>
                    <div>
                        <?php if(!empty($value->link)) { ?>
                            <a href="<?php echo $value->link; ?>" target="_blank">
                                <img src="<?php echo base_url(); ?>assets/uploads/files/<?php echo $value->photo; ?>" class="background" alt="">
                            </a>
                        <?php } else { ?>
                        <img src="<?php echo base_url(); ?>assets/uploads/files/<?php echo $value->photo; ?>" class="background" alt="">
                        <?php } ?>
                    </div>

                <?php 
                        }
                    } 
                ?>
            </div>
        </div>  
<?php 
        }
    } 

?>