<div id="replacement-sub-nav">

<?php if(isset($this->is_ppmsystem) OR isset($this->is_newsletter) OR isset($this->is_onlinetraining)) { ?>
  <?php if(isset($is_member) && !$is_member) { ?>
    <?php if(isset($this->is_ppmsystem) && $this->is_ppmsystem) { ?>
    
      <p><a href="<?php echo base_url(); ?>members">members home</a></p>
      <p><a href="<?php echo base_url(); ?>ppmsystem-logout">members logout</a></p>
    
    <?php } ?>

    <?php if(isset($this->is_newsletter) && $this->is_newsletter) { ?>

      <p><a href="<?php echo base_url(); ?>newsletter">newsletter home</a></p>
      <p><a href="<?php echo base_url(); ?>newsletter-logout">newsletter logout</a></p>

    <?php } ?>

    <?php if(isset($this->is_onlinetraining) && $this->is_onlinetraining) { ?>

      <p><a href="<?php echo base_url(); ?>onlinetraining">onlinetraining home</a></p>
      <p><a href="<?php echo base_url(); ?>onlinetraining-logout">onlinetraining logout</a></p>

    <?php } ?>

  <?php } else { ?>

    <?php if((isset($this->is_newsletter) && $this->is_newsletter) OR (isset($this->is_ppmsystem) && $this->is_ppmsystem) && $page !== 'expired') {?>

    <p><a href="<?php echo base_url(); ?>members/" <?php if($page === 'home') echo 'class="menu-active"';?>>home</a></p>
    <?php if($membershipType >= 0 && $membershipType <= 3){ ?> 
    <p><a href="<?php echo base_url(); ?>members/articles" <?php if($page === 'articles') echo 'class="menu-active"';?>>articles</a></p>
    <?php } ?>
    <p><a href="<?php echo base_url(); ?>members/listnews" <?php if($page === 'listnews') echo 'class="menu-active"';?>>news</a></p>
    <?php if($membershipType >= 0 && $membershipType <= 3){ ?>
         <p><a href="<?php echo base_url(); ?>members/discounts" <?php if($page === 'discounts') echo 'class="menu-active"';?>>discounts</a></p>
         <p><a href="<?php echo base_url(); ?>members/systemupgrades" <?php if($page === 'systemupgrades' || $page === 'listupgrades') echo 'class="menu-active"';?>>system upgrades</a></p>
         <p><a href="<?php echo base_url(); ?>members/monthlystats" <?php if($page === 'monthlystats') echo 'class="menu-active"';?>>member awards</a></p>
         <p><a href="<?php echo base_url(); ?>members/newsletters" <?php if($page === 'newsletters') echo 'class="menu-active"';?>>landlord newsletter</a></p>
         <p><a href="<?php echo base_url(); ?>onlinetraining">Online training</a></p>
         <p><a href="<?php echo base_url(); ?>members/ppmtutorials" <?php if($page === 'ppmtutorials') echo 'class="menu-active"';?>>PPMsystem Tutorials</a></p>
    <?php } ?>

    <?php if($membershipType === 5) { ?>
    <p><a href="<?php echo base_url(); ?>members/onlinetraining">online training</a></p>
    <?php } ?>

     <p><a href="<?php echo base_url(); ?>members/gallery" <?php if($page === 'gallery') echo 'class="menu-active"';?>>gallery</a></p>
     <p><a href="<?php echo base_url(); ?>members/surveyresults" <?php if($page === 'surveyresults') echo 'class="menu-active"';?>>survey results</a></p>
     <p><a href="<?php echo base_url(); ?>ppmsystem-logout">logout</a></p>
    <?php } elseif($page === 'expired' && (isset($this->is_ppmsystem) && $this->is_ppmsystem)) { ?>
      <p><a href="<?php echo base_url(); ?>ppmsystem-logout">logout</a></p>
    <?php }?>

  <?php } ?>

 <?php } ?>

</div>