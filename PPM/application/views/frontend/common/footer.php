<footer>

  <div class="container-body">    

      <br>
      <div class="container">
              <!-- banner <% call banner(page,3) %> -->
      </div>
      
      <br>


      <div class="container-aqua subscribe">
          <div class="container">
              <div class="row">
                  <div class="col-md-4">
                      <p class="h1 nocase">PPM ENews</p>
                      <p class="small">Join thousands of industry professionals today to receive the latest news, helpful tips and articles.</p>
                  </div>
                  <div class="col-md-8">
                    <form action="<?php echo base_url(); ?>contact-us/14" method="post" name="footerenewsform" onsubmit="return check_footer_email();">
                      <div class="row">
                          <div class="col-md-4"><input type="text" class="form-control" name="firstname" placeholder="First Name"></div>
                          <div class="col-md-4"><input type="text" class="form-control" name="surname" placeholder="Surname"></div>
                          <div class="col-md-4"><input type="text" class="form-control" name="companyName" placeholder="Company"></div>
                      </div>
                      <div class="row">
                          <div class="col-md-4"><input type="text" class="form-control" name="email" placeholder="Email"></div>
                          <div class="col-md-4"><input type="text" class="form-control" name="confirmemail" placeholder="Confirm Email"></div>
                          <div class="col-md-4"><input type="text" class="form-control" name="phone" placeholder="Phone"></div>
                      </div>
                      <input type="submit" class="btn btn-white pull-right" value="Sign up">
                    </form>
                  </div>
              </div>
            </div>
      </div>

      <div class="container book">
          <div class="row">
              <div class="col-md-6">
                  <a href="<?php echo base_url(); ?>contact-us/3" class="btn btn-default">Click here</a>

                  <div class="clearfix visible-xs-block"></div>
                  <span>TO BOOK A PPMSYSTEM DEMONSTRATION</span>
              </div>

                  <div class="visible-xs-block"><br></div>

              <div class="col-md-6">
                  <a href="<?php echo base_url(); ?>contact-us/9/freeconsult" class="btn btn-default">Click here</a>

                  <div class="clearfix visible-xs-block"></div>
                  <span>TO BOOK A FREE CONSULTATION</span>

              </div>

          </div>
      </div>

      <div id="container-footer">
          <div class="container">
              <div class="row">
                  <div class="col-md-2 col-sm-4 col-xs-6">
                    <p><strong><a href="<?php echo base_url(); ?>articles">NEWS &amp; ARTICLES</a></strong></p>
                    <p><strong><a href="<?php echo base_url(); ?>ppm-tv">PPM TV</a></strong></p>

                    <p><strong><a href="<?php echo base_url(); ?>events/home">Training &amp; Events</a></strong>

                    <br/>
                    
                    <a href="<?php echo base_url(); ?>events/list/756">Online Training</a><br />
                    <a href="<?php echo base_url(); ?>events/list/757">PPMsystem Tutorials</a><br />
                    <a href="<?php echo base_url(); ?>events/list/29">Workshops</a><br />
                    <a href="<?php echo base_url(); ?>events/list/893">Coaching Programs</a><br />
                    <a href="<?php echo base_url(); ?>testimonials/list/71">Testimonials</a>
                    </p>

                    <p><strong><a href="<?php echo base_url(); ?>corporate-speaking">Corporate Speaking</a></strong>

                    <br />
                    
                    <a href="<?php echo base_url(); ?>why-choose-debbie">Why Choose Debbie</a><br />
                    <a href="<?php echo base_url(); ?>guest-speaking-engagements">Speaking Engagements</a><br />
                    </p>
                  </div>

                  <div class="col-md-2 col-sm-4 col-xs-6">
                    <p><strong><a href="<?php echo base_url(); ?>ppm-conference">PPM Conference</a></strong><br />
                    <a href="<?php echo base_url(); ?>ppm-conference/conference-details">Conference Details</a><br />
                    <a href="<?php echo base_url(); ?>ppm-conference-download-brochure">Download Brochure</a><br />
                    <a href="<?php echo base_url(); ?>ppm-conference/speakers-program">Speakers & Program</a><br />
                    <a href="<?php echo base_url(); ?>ppm-conference/delegate-testimonials">Delegate Testimonials</a><br />
                    <a href="<?php echo base_url(); ?>ppm-conference/social-events">Social Events</a><br />
                    <a href="<?php echo base_url(); ?>ppm-conference/venue-accomodation">Venue & Accommodation</a><br />
                    <a href="<?php echo base_url(); ?>ppm-conference/travel">Travel</a><br />
                    <a href="<?php echo base_url(); ?>ppm-conference/sponsorship-opportunities">Sponsorship Opportunities</a><br />
                    <a href="<?php echo base_url(); ?>ppm-conference/sponsors">Sponsors</a><br />
                    <a href="<?php echo base_url(); ?>ppm-conference/sponsor-testimonials">Sponsor Testimonials</a><br />
                    <a href="<?php echo base_url(); ?>ppm-conference/gallery/952">Gallery</a><br />
                    <a href="<?php echo base_url(); ?>property-management-awards">National Awards</a><br />
                    <a href="<?php echo base_url(); ?>ppm-conference/register-online">Register Online</a><br />
                    </p>
                  </div>
          
                  <div class="clearfix visible-xs-block"></div>

                  <div class="col-md-2 col-sm-4 col-xs-6">
          
                    <p>
                      <strong><a href="<?php echo base_url(); ?>property-management-awards">National Awards</a></strong><br />
                          
                      <a href="<?php echo base_url(); ?>property-management-awards-member">Member Awards</a><br />
                      <a href="<?php echo base_url(); ?>property-management-awards-national">National Awards</a><br />
                      <a href="<?php echo base_url(); ?>property-management-awards-hall-of-fame">Hall of Fame</a><br />
                      <a href="<?php echo base_url(); ?>property-management-awards-winners">Award Finalists & Winners</a><br />
                      <a href="<?php echo base_url(); ?>assets/uploads/files/documents/PPM%20Awards%20Submission.doc" target="_blank">Download Award Submission</a><br />
                    </p>
          
                    <p>
                      <strong><a href="<?php echo base_url(); ?>ppmsystem">PPMsystem</a></strong><br />
                      <a href="<?php echo base_url(); ?>why-choose-us">Why Choose PPM</a><br />
                      <a href="<?php echo base_url(); ?>membership-support">Membership &amp; Support</a><br />
                      <a href="<?php echo base_url(); ?>implementation-training">Implementation &amp; Training</a><br />
                      <a href="<?php echo base_url(); ?>property-management-awards-member">Member Awards</a><br />
                      <a href="<?php echo base_url(); ?>testimonials/list/72">Testimonials</a>
                    </p> 
                  </div>

                  <div class="clearfix visible-sm-block"></div>

                  <div class="col-md-2 col-sm-4 col-xs-6">
                    <p>
                      <strong><a href="<?php echo base_url(); ?>consulting-coaching">Consulting &amp; Coaching</a></strong><br />
                      <a href="<?php echo base_url(); ?>consulting">Consulting</a><br />
                      <a href="<?php echo base_url(); ?>ppmsystem-consulting-coaching">PPMsystem</a><br />
                      <a href="<?php echo base_url(); ?>performance-assessment">Performance Assessment</a><br />
                      <a href="<?php echo base_url(); ?>consulting-consulting-coaching-programs">Coaching Programs</a>
                    </p>
          
                    <p>
                      <strong><a href="<?php echo base_url(); ?>products">Products</a></strong><br />
                      <a href="<?php echo base_url(); ?>newsletter">Landlord Newsletter</a><br />
                      <a href="<?php echo base_url(); ?>products/list/32">Modules, Forms &amp; Letters</a><br />
                      <a href="<?php echo base_url(); ?>products/list/33">CDs</a><br />
                      <a href="<?php echo base_url(); ?>products/list/34">DVD's &amp; Audios</a><br />
                      <a href="<?php echo base_url(); ?>ppmsystem">PPMsystem</a><br />
                      <a href="<?php echo base_url(); ?>products/list">List All Products</a>
                    </p>
                  </div>

                  <div class="col-md-2 col-sm-4 col-xs-6">
                    <p>
                      <strong><a href="<?php echo base_url(); ?>rent-roll">Rent Roll Sales</a></strong><br />
                      <a href="<?php echo base_url(); ?>classifieds">Classifieds</a><br />
                      <a href="<?php echo base_url(); ?>brokerage">Brokerage</a><br />
                      <a href="<?php echo base_url(); ?>due-diligence">Due Diligence</a>
                    </p>
                    
                    <br>
                    <p><strong><a href="<?php echo base_url(); ?>partners">Industry Partners</a></strong></p>
                    <p><strong><a href="<?php echo base_url(); ?>recruitment">Recruitment</a></strong></p>
                    <p><strong><a href="<?php echo base_url(); ?>advertise">Advertise</a></strong></p>

                    <p>
                      <strong><a href="<?php echo base_url(); ?>gallery">Gallery</a></strong><br />
                    </p>
            
                    <p>
                      <strong><a href="<?php echo base_url(); ?>enews">ENEWS</a></strong>
                    </p>
                  </div>

                  <div class="col-md-2 col-sm-4 col-xs-6">

                    <p>
                      <strong><a href="<?php echo base_url(); ?>about">About</a></strong><br />
                      <a href="<?php echo base_url(); ?>about-ppm-team">PPM Team</a><br />
                      <a href="<?php echo base_url(); ?>about-testimonials">Testimonials</a><br />
                      <a href="<?php echo base_url(); ?>about-charities">Charities</a><br />
                      <a href="<?php echo base_url(); ?>about-mission-vision">Vision & Mission</a><br />
                      <a href="<?php echo base_url(); ?>about-values">Values</a><br />
                      <a href="<?php echo base_url(); ?>about-careers">PPM Careers</a><br />
                      <a href="<?php echo base_url(); ?>about-terms-conditions">Terms & Conditions</a><br />
                    </p>
          
                    <p>
                      <strong><a href="<?php echo base_url(); ?>contact-us">Contact us</a></strong><br />
                      Postal Address: <br />
                      PO Box 5019<br />
                      Robina Town Centre<br />
                      QLD 4230<br />
                      P. 07 5562 0037<br />
                      E. <a href="mailto:info@ppmgroup.com.au">info@ppmgroup.com.au</a><br />
                    </p>

                    <p>
                      <a href="https://www.facebook.com/ppmgroup" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-facebook"></i></a>
                      <a href="https://twitter.com/ppmgroup" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-twitter"></i></a>
                      <a href="https://www.linkedin.com/in/debbiep1" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-linkedin"></i></a>
                      <a href="https://www.youtube.com/user/PPMGroup" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-youtube"></i></a>

                    </p>
                    <p>PPM Group | Est. 2000</p>
                  </div>
              </div>
          </div>
      </div>

  </div>

  <button id="scroll-top-btn" type="button" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-chevron-up"></i></button>

</footer>

<?php 
  if(isset($other_scripts)) {
    foreach ($other_scripts as $value) {
      echo $value;
    }
  }
?>  


<script src="<?php echo base_url(); ?>assets/js/slick/slick.min.js"></script>

<script src="<?php echo base_url(); ?>assets/front/js/ppm.js?t=<?php echo time(); ?>"></script>


