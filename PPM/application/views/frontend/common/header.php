<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge">-->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo WEB_TITLE; ?> <?php if(isset($title)) echo $title; ?></title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url(); ?>assets/front/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url(); ?>assets/gentelella/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/slick/slick-theme.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/slick/slick.css"/>

    <?php 
        if(isset($other_styles)) {
            foreach ($other_styles as $value) {
                echo $value;
            }
        }
    ?>  

    <!-- Custom Theme Style -->
    <link href="<?php echo base_url(); ?>assets/front/css/custom.css" rel="stylesheet">  
    
    <!-- jQuery -->
    <script src="<?php echo base_url(); ?>assets/gentelella/vendors/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap -->
    <script src="<?php echo base_url(); ?>assets/front/bootstrap/js/bootstrap.min.js"></script>
    
  </head>

  <body>
  <header>
  <div class="container-fluid" id="header">
    <div class="container header">
        <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>/assets/images/front/ppm-logo.gif" alt="PPM Group Logo" id="logo"></a>

        <div id="rightlinks">
               <a href="tel:0755620037" class="grey" id="phone"><img src="<?php echo base_url(); ?>/assets/images/front/phone.png" alt="Phone Icon" title="Click to call PPM Group"> 07 5562 0037</a>

               <?php if(isset($_SESSION['member_logged_in']) OR isset($_SESSION['newsletter_logged_in']) OR isset($_SESSION['onlinetraining_logged_in'])) { ?>

                <?php
                  
                    if(isset($_SESSION['member_logged_in']) && $_SESSION['member_logged_in']) {
                      $membershipType = intval($this->session->membershipType);
                      if($membershipType === 0) {
                        $membershipTypeLabel = 'PLATINUM';
                      }
                      elseif($membershipType === 1) {
                        $membershipTypeLabel = 'GOLD';
                      }
                    }
                   

                ?>

               <div class="btn-group noprint">
                <a href="javascript:void(0);" class="btn btn-default dropdown-toggle" data-toggle="dropdown" ><i class="fa fa-user"></i> <?php if(isset($_SESSION['username'])) { echo ' <small class="grey">( ' . $_SESSION['username'] . ' )</small>'; } ?></a>
                <ul class="dropdown-menu">
                    <?php if(isset($_SESSION['member_logged_in']) && $_SESSION['member_logged_in']) { ?><li><a href="<?php echo base_url();?>members"><i class="fa fa-home"></i> PPMsystem <small>(<?php echo $membershipTypeLabel; ?>)</small></a></li> <?php } ?>
                    <?php if(isset($_SESSION['newsletter_logged_in']) && $_SESSION['newsletter_logged_in']) { ?><li><a href="<?php echo base_url();?>newsletter"><i class="fa fa-home"></i> Landlord Newsletter</a></li> <?php } ?>
                    <?php if(isset($_SESSION['onlinetraining_logged_in']) && $_SESSION['onlinetraining_logged_in']) { ?><li><a href="<?php echo base_url();?>onlinetraining"><i class="fa fa-home"></i> Online Training</a></li> <?php } ?>
                    <li><hr style="margin: 2px 0;" /></li>
                    <li><a href="<?php echo base_url();?>profile"><i class="fa fa-gear"></i> Edit Profile</a></li>
                  </ul>
               </div>

              <?php } ?>

               <div class="btn-group noprint" <?php if(isset($_SESSION['member_logged_in']) && isset($_SESSION['onlinetraining_logged_in'])) { ?> style="display: none;" <?php } ?> >
                <a href="javascript:void(0);" class="btn btn-default dropdown-toggle" data-toggle="dropdown" >Login</a>
                <ul class="dropdown-menu">
                    <?php if(!isset($_SESSION['member_logged_in'])) { ?><li><a href="<?php echo base_url();?>ppmsystem-login">PPMsystem Member</a></li> <?php } ?>
                    <?php if(!isset($_SESSION['newsletter_logged_in']) && !isset($_SESSION['member_logged_in'])) { ?><li><a href="<?php echo base_url();?>newsletter-login">Landlord Newsletter</a></li> <?php } ?>
                    <?php if(!isset($_SESSION['onlinetraining_logged_in'])) { ?><li><a href="<?php echo base_url();?>onlinetraining-login">Online Training</a></li> <?php } ?>
                  </ul>
               </div>

               <div class="btn-group noprint">
               <a href="javascript:void(0);" class="btn btn-default dropdown-toggle" data-toggle="dropdown" >Sign Up</a>
               <ul class="dropdown-menu">
                   <?php if(!isset($_SESSION['member_logged_in']) && !isset($_SESSION['newsletter_logged_in'])) { ?><li><a href="<?php echo base_url();?>newsletter/signup">Landlord Newsletter</a></li><?php } ?>
                   <?php if(!isset($_SESSION['onlinetraining_logged_in'])) { ?><li><a href="<?php echo base_url();?>onlinetraining">Online Training</a></li><?php } ?>
                   <li><a href="<?php echo base_url();?>contact-us/14">PPM ENews</a></li>
                   <?php if(!isset($_SESSION['member_logged_in'])) { ?><li><a href="<?php echo base_url(); ?>ppmsystem">Membership</a></li><?php } ?>
                 </ul>
              </div>

               <a href="" class="btn btn-primary noprint">My Cart</a>
               <div id="menu" class="noprint">
                    <div id="hamburger">
                    <span></span>
                    <span></span>
                    <span></span>
                    </div>
                    <p>MENU</p>
               </div>

        </div>

    </div>
    <div class="container header-submenu">
       <div id="menubar" class="">

          <p><a id="menu-close" href="javascript:void(0);">CLOSE X</a></p>
          <ul>
              <li><a href="<?php echo base_url() . ''; ?>">Home</a></li>
              <li><a href="<?php echo base_url() . 'articles'; ?>">News &amp; Articles</a></li>
              <li><a href="<?php echo base_url() . 'ppm-tv'; ?>">PPM TV</a></li>
              <li><a href="<?php echo base_url() . 'events/home'; ?>">Training &amp; Events</a></li>
              <li><a href="<?php echo base_url() . 'corporate-speaking'; ?>">Corporate Speaking</a></li>
              <li><a href="<?php echo base_url() . 'ppm-conference'; ?>">PPM Conference</a></li>
              <li><a href="<?php echo base_url() . 'property-management-awards'; ?>">National Awards</a></li>
              <li><a href="<?php echo base_url() . 'ppmsystem'; ?>">PPMsystem</a></li>
              <li><a href="<?php echo base_url() . 'consulting-coaching'; ?>">Consulting &amp; coaching</a></li>
              <li><a href="<?php echo base_url() . 'products'; ?>">Products</a></li>
              <li><a href="<?php echo base_url() . 'rent-roll'; ?>">Rent Roll Sales</a></li>
              <li><a href="<?php echo base_url() . 'partners'; ?>">Industry Partners</a></li>
              <li><a href="<?php echo base_url() . 'recruitment'; ?>">Recruitment</a></li>
              <li><a href="<?php echo base_url() . 'advertise'; ?>">Advertise</a></li>
              <li><a href="<?php echo base_url() . 'about'; ?>">About</a></li>
              <li><a href="<?php echo base_url() . 'gallery'; ?>">GALLERY</a></li>
              <li><a href="<?php echo base_url() . 'contact-us'; ?>">Contact</a></li>
          </ul>

        </div>      
    </div>
  </div>

  <div style="height:82px; width: 100%;"></div>
  </header>
