<?php  $colCount = count($periodArr); ?>
<table id="contents1-table" class="table table-bordered table-striped hide table-hover text-left  dt-responsive nowrap" data-page-length='100' cellspacing="0" width="100%"><!-- table-bordered  -->
	<thead>
	<tr>
	<th style="text-align:left!important; vertical-align: middle!important;" colspan="2"></th>
	<th style="text-align:center!important; vertical-align: middle!important;" colspan="<?php echo $colCount + 1; ?>">Vacancy Rate</th>
	<th style="text-align:center!important; vertical-align: middle!important;" colspan="<?php echo $colCount + 1; ?>">Arrears Rate</th>
	<th style="text-align:center!important; vertical-align: middle!important;" colspan="<?php echo $colCount + 1; ?>">Rent Increase <br> $ Value</th>
	<th style="text-align:center!important; vertical-align: middle!important;" colspan="<?php echo $colCount; ?>">Properties (Gain)</th>
	<th style="text-align:center!important; vertical-align: middle!important;" colspan="<?php echo $colCount; ?>">Properties (Loss)</th>
		<th style="text-align:center!important; vertical-align: middle!important;" colspan="<?php echo $colCount + 1; ?>">Net New Manage</th>
	<th style="text-align:center!important; vertical-align: middle!important;" colspan="<?php echo $colCount; ?>">Dollar (Gain)</th>
	<th style="text-align:center!important; vertical-align: middle!important;" colspan="<?php echo $colCount; ?>">Dollar (loss)</th>
	<th style="text-align:center!important; vertical-align: middle!important;" colspan="<?php echo $colCount + 1; ?>">Dollar increase</th>
	</tr>
	<tr>
	<th style="text-align:left!important; vertical-align: middle!important;">ID</th>
	<th style="text-align:left!important; vertical-align: middle!important;">Member</th>
    	<?php
    		for ($i=0; $i <= 8 ; $i++) { 
    			foreach ($periodArr as $value) {
    				echo '<th style="text-align:left!important; vertical-align: middle!important;">'.$value.'</th>';
    			}
    			if(!in_array($i, array(3,4,6,7))) {
    				echo '<th class="text-success" style="text-align:left!important; vertical-align: middle!important;">Avg</th>';
    			}
    		}
    	?>
	</tr>
	</thead>
	<tbody></tbody>
</table>

<script>
	$(document).ready(function(){
		if(monthlyStats!=null) {
			monthlyStatsTable = {
				processMonthlyStatsReportList: function(){

			    	var columns = [
							      <?php
							      	echo "{ data: 'id' },{ data: 'company' },";

						    		for ($i=0; $i <= 8 ; $i++) { 
						    			foreach ($periodArr as $value) {
						    				echo "{ data: '" . $colArr[$i] . "_" . $value . "'},";
						    			}
						    			if(!in_array($i, array(3,4,6,7))) {
						    				echo "{ data: '" . $colArr[$i] . "_avg" . "'},";
						    			}
						    		}
							      ?>
							    ];


					monthlyStats.processMonthlyStatsReportList(columns, this.groupMonthlyStatsReportData());
				},				
				groupMonthlyStatsReportData: function(){

					var masterList = <?php echo $masterList; ?>;

					var arr = [];

					$.each(masterList, function(i,data){
						arr.push(new ColList(data));
					});

					function ColList(data){
						this.id = data.id;
						this.company = data.company;

						<?php
				    		for ($i=0; $i <= 8 ; $i++) { 
				    			foreach ($periodArr as $value) {
				    				echo "this." . $colArr[$i] . "_" . $value . " = data." . $colArr[$i] . "." . $value . ";";
				    			}
				    			if(!in_array($i, array(3,4,6,7))) {
				    				echo "this." . $colArr[$i] . "_avg" . " = data." . $colArr[$i] . ".avg" . ";";
				    			}
				    		}
						?>
		            }

		            masterList = null;

		            return arr;

				}
			};
		}
	});
</script>
