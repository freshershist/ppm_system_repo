<style>
	hr.style-two {
	    border: 0;
	    height: 0;
	    border-top: 1px solid rgba(0, 0, 0, 0.1);
	    border-bottom: 1px solid rgba(255, 255, 255, 0.3);
	}	

	.drop-here {
		position: absolute;
		z-index: 1;
		top: calc(50% - 10px);
		text-align: center;
		width: 100%;		
	}

	ul.to_do li:hover {
		cursor: pointer;
	}

	.categories-selected.to_do li {
	    background: #1abb9c!important;
	    color: #fff!important;
	}	
</style>

<div class="container">

	<div class="col-md-12 col-sm-12 col-xs-12">
		<div id="copy-title"class="alert alert-success fade in" role="alert" style="display: none;">
			<strong></strong>
		</div>
		<p class="lead"><small class="text-info small">Compulsary fields are marked by an asterisk ( <i class="fa fa-asterisk text-danger small"></i> )</small></p>

		<form id="add-edit-form" class="form-horizontal form-label-left">
			<input id="data-id" type="hidden" name="id" value="" />
			
			<!-- start accordion -->
			<div class="accordion" id="accordion-add-edit" role="tablist" aria-multiselectable="true">

				<!-- General -->	
				<div class="panel">
					<a class="panel-heading" role="tab" id="contents-add-edit_1" data-toggle="collapse" data-parent="#accordion-add-edit" href="#add-edit_1" aria-expanded="true" aria-controls="add-edit_1">
						<h4 class="panel-title">General</h4>
					</a>
					<div id="add-edit_1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="contents-add-edit_1">
						<div class="panel-body">

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-12 col-xs-12">User Type: <i class="fa fa-asterisk text-danger"></i></label>
                                <div class="col-md-5 col-sm-12 col-xs-12">
                                    <?php
                                        echo form_dropdown('userType', $usertypeArr, '', 'id="data-userType" class="form-control"');

                                    ?>
                                </div>
                            </div>

							<div class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Username: <i class="fa fa-asterisk text-danger"></i></label>
								<div class="col-md-5 col-sm-12 col-xs-12">
									<input id="data-username" name="username" type="text" class="form-control" placeholder="" value="" />
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Password: <i class="fa fa-asterisk text-danger"></i></label>
								<div class="col-md-5 col-sm-12 col-xs-12 input-group">
								<input id="data-password" name="password" type="password" class="form-control" style="margin-left: 10px;width: 97%;" placeholder="" value=""/>
								<span class="input-group-btn">
                                	<button id="password-btn" type="button" class="btn btn-success" data-type="show">Show</button>
                                </span>
							</div>
							</div>	

							<div class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Confirm Password: <i class="fa fa-asterisk text-danger"></i></label>
								<div class="col-md-5 col-sm-12 col-xs-12">
									<input id="data-confirmPassword" name="confirmPassword" type="password" class="form-control" placeholder="" value="" />
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Access: </label>
                                <div class="col-md-9 col-sm-12 col-xs-12">
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

										<div class="checkbox" style="padding-bottom:5px;">
							                <label><span class="badge bg-default">Drag items to Selected Access:</span></label>
							                <button type="button" id="select-all-btn" class="btn btn-xs btn-warning pull-right">Select All</button>
										</div>							

										<div style="max-height: 285px; overflow-y: scroll; border-radius: 5px;border: 1px solid #f3f3f3;">
											<h2 class="drop-here"><i>Drop here ...</i></h2>
											<ul id="access-available" class="to_do" style="min-height: 268px; position:relative; z-index:2;">
											<?php	
											$access_selected_arr = array();

											if(isset($accessArr)) {
												foreach ($accessArr as $key=>$value) {

													
											?>
												<li id="access-<?php echo $key; ?>" data-id="<?php echo $key; ?>" style="margin:4px;" draggable="true">
													<p><?php echo $value; ?></p>
												</li>
											<?php
													

												}
											}

											?>

											</ul>

										</div>
				
									</div>

									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

										<div class="checkbox" style="padding-bottom:5px;">
											<label><span class="badge bg-green">Selected Access:</span></label>
											<button type="button" id="deselect-all-btn" class="btn btn-xs btn-danger pull-right">Deselect All</button>
										</div>
										<div style="max-height: 285px; overflow-y: auto; border-radius: 5px;border: 1px solid #f3f3f3;">
											<h2 class="drop-here"><i>Drop here ...</i></h2>
											<ul id="access-selected" class="to_do categories-selected" style="min-height: 268px; position:relative; z-index:2;">
											</ul>
										</div>
										<div id="acs-selected" class="hide"></div>
				
									</div>								
								</div>
							</div>                            

						</div>	
					</div>
				</div>
				<!-- General -->

			</div>
	
		</form>

	</div>	

	<div class="row">
		<div class="panel">
			<div class="col-md-12 text-right">
				<button id="cancel-btn" type="button" class="btn btn-sm btn-warning" style="display: none;">Cancel Changes</button>
				<button id="update-btn" type="button" class="btn btn-sm btn-success" style="display: none;">Update</button>
				<button id="cancel-copy-btn" type="button" class="btn btn-sm btn-warning" style="display: none;">Cancel Copy</button>
				<button id="add-btn" type="button" class="btn btn-sm btn-success add-btn">Add</button>
			</div>
			<div class="clearfix"></div>
		</div>	
	</div>

</div>

<div class="row hide">
	<div class="col-md-5">
		<div class="input-group">
		<input type="text" class="form-control">
		<span class="input-group-btn">
		<button id="ppm-browse-image" type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
		</span>
		</div>
	</div>
</div>

<script>

	$(document).ready(function(){
		var form = $('#add-edit-form');
		var fields = {
						id: 		$('#data-id'),
						username: 	$('#data-username'),
						access:		$('#acs-selected'),
						password: 	$('#data-password'),
						userType: 	$('#data-userType'),
						confirmPassword: $('#data-confirmPassword')
					};
		var access = null;

		var contents = {
			accessArr: <?php echo json_encode($accessArr); ?>,
			init: function(){

				addBtn.bind('click', function(){
					contents.submitForm();
				});

				updateBtn.bind('click', function(){
					contents.submitForm();
				});

				access = this.initDragDrop({holder:'access-available',selectedHolder:'access-selected',selected:'acs-selected',name:'access', type: 'access'});

				$('#select-all-btn').on('click', function(){
					access.selectAll();
				});

				$('#deselect-all-btn').on('click', function(){
					access.resetList();
				});

				$('#password-btn').on('click', function(){
					if($(this).attr('data-type') == 'show') {
						$(this).attr({'data-type':'hide'}).text('Hide');
						fields.password.attr({'type':'text'});
					}
					else {
						$(this).attr({'data-type':'show'}).text('Show');
						fields.password.attr({'type':'password'});
					}
				});

			},
			initDragDrop: function(params) {

				var dragDrop = {
					holder: 		$('#' + params.holder),
					selectedHolder: $('#' + params.selectedHolder),
					selected: 		$('#' + params.selected),
					itemId: 		'',
					name: 			params.name,
					type: 			params.type,
					init: function(){
						this.holder.on('drop', function(e){dragDrop.drop(e,$(this));});
						this.selectedHolder.on('drop', function(e){dragDrop.drop(e,$(this));});
						this.holder.on('dragover', function(e){dragDrop.allowDrop(e);});
						this.selectedHolder.on('dragover', function(e){dragDrop.allowDrop(e);});
						this.holder.find('li').on('dragstart', function(e){dragDrop.drag(e);});
						this.selectedHolder.find('li').on('dragstart', function(e){dragDrop.drag(e);});
					},
					drop: function(ev, holder) {
						ev.preventDefault();
		    			
		    			if($(ev.target).parents('ul:first').length==0) {
		    				$('#'+this.itemId).appendTo($(ev.target));
		    			}
		    			else {
		    				var li = ($(ev.target)['context'].localName == 'li') ? $(ev.target) : $(ev.target).parents('li:first');
		    				$('#'+this.itemId).insertBefore(li);
		    			}

		    			this.updateList();
					},
					allowDrop: function(ev) {
					    ev.preventDefault();
					},
					drag:function(ev) {
						this.itemId = ev.target.id;
						ev.originalEvent.dataTransfer.setData("text/plain", ev.target.id);
						
					},					
					updateList: function(){
						this.selected.html('');
						var ctr = 0;
						$.each(this.selectedHolder.find('li'), function(k,v){
							var checkbox = $('<input type="checkbox" checked name="'+dragDrop.name+'[]" value="'+$(v).data().id+'" />');
							checkbox.appendTo(dragDrop.selected);
							ctr++;
						});
					},
					resetList: function(){
						this.selected.html('');
						$.each(this.selectedHolder.find('li'), function(k,v){
							$(v).appendTo(dragDrop.holder);
						});
					},
					populateList: function(ids){
						$.each(ids, function(k,v){
							$('#'+dragDrop.type + '-' + v).appendTo(dragDrop.selectedHolder);
						});

						this.updateList();
					},
					selectAll: function(){
						this.selected.html('');
						$.each(dragDrop.holder.find('li'), function(k,v){
							$(v).appendTo(dragDrop.selectedHolder);
						});

						this.updateList();
					}
				}

				dragDrop.init();

				return dragDrop;
			},			
			submitForm: function(){

				var isEdit = $.isNumeric(fields.id.val());

				swal({
				    title: (isEdit)?"Update Item" : "Add Item",
				    text: "Are you sure?",
				    icon: "warning",
				    buttons: ["Cancel", true],
				    dangerMode: false,
				})
				.then(willSubmit => {
				    if (willSubmit) {
						var request = $.ajax({
				          url: "<?php echo base_url(); ?>admin/users/add_update",
				          method: "POST",
				          data: form.serialize(),
				          dataType: 'json'
				        });

				        request.done(function( res ) {
				            if(res!=null) {

				            	if(isEdit) {
				            		if(res.hasOwnProperty('error')) {
				            			var errMsg = '';

				            			$.each(res.error, function(k, v){
				            				errMsg += '* ' + v + '\n';
				            			});

				            			new PNotify({
											title: 'Invalid or Required!',
											text: errMsg,
											type: 'error',
											styling: 'bootstrap3'
			                            });

				            			swal({title:"Update Failed!",text:"Some fields are required (*) or have invalid values",icon:'error',button:false, timer:3000});
				            		}
				            		else {
				            			filter.applyUpdateChanges(res);

				            			swal({title:"Updated!",text:'Changes have been saved.',icon:'success',button:false, timer:3000});
				            		}
				            		
				            	}
				            	else {
				            		if(res.hasOwnProperty('error')) {
										var errMsg = '';

				            			$.each(res.error, function(k, v){
				            				errMsg += '* ' + v + '\n';
				            			});

				            			new PNotify({
											title: 'Invalid or Required!',
											text: errMsg,
											type: 'error',
											styling: 'bootstrap3'
			                            });

			                            swal({title:"Add Failed!",text:'Some fields are required (*) or have invalid values',icon:'error',button:false, timer:3000});
				            		}
				            		else {
				            			fields.id.val(res.id);
				            			contents.showUpdateButton();
				            			addTab.html('<i class="fa fa-pencil"></i> Edit');
				            			
				            			swal({title:"Success!",text:"Added item with id: " + res.id,icon:'success',button:false, timer:3000});
				            			filter.addNewItem(res);
				            		}
				            	}
				            }
		        		});

				      	request.error(function(_data){
		        			if(_data.readyState == 4 && _data.status == 200) {//Session expired and redirected

		        				swal({
								    title: "Session Expired!",
								    text: "Reloading page.",
								    icon: "warning",
								    dangerMode: true,
								})
								.then(ok => {
								    if (ok) {
								    	window.location.href = '<?php echo base_url();?>admin/login';
								    }
								});
		        			}
		        		});
					}
				}); 

			},
			resetForm: function(){
				$.each(fields, function(k,v){
					if(v.length>0) {
						if(k == 'userType') {
							v.val(0);
						}
						else {
							v.val('');
						}
					}
				});

				access.resetList();
			},
			populateForm: function(data){

				if(data!=null) {

					this.resetForm();

					setTimeout(function(){
						$.each(data, function(k,v){
							if(fields.hasOwnProperty(k) && fields[k].length>0) {
								if(k == 'access') {
									try {
										access.populateList(JSON.parse(v).data);
									}
									catch(e) {}
								}
								else {
									fields[k].val(v);
								}
							}
						});
						
						contents.showUpdateButton();

						if(addTab!=null) addTab.click();
					}, 50);
				}
			},
			showUpdateButton: function(){
				cancelBtn.show();
				updateBtn.show();
				addBtn.hide();	
			},
			showAddButton: function(){
				cancelBtn.hide();
				updateBtn.hide();
				addBtn.show();	
			},
			cancelUpdate: function(){

				swal({
				    title: "Cancel Changes",
				    text: "Are you sure?",
				    icon: "warning",
				    buttons: ["Cancel", true],
				    dangerMode: false,
				})
				.then(willCancel => {
				    if (willCancel) {
				    	contents.resetTab();
				    }
				});
			},
			resetTab: function(){
		    	addTab.html('<i class="fa fa-plus"></i> Add');
		    	contents.resetForm();
				contents.showAddButton();
				searchTab.trigger('click');
			}
		};

		contents.init();

	    addEdit = contents;
	});
</script>