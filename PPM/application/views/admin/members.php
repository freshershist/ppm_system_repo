<script type="text/javascript">
	var membersAddEditHolder,addTab,searchTab,showLogBtn;
	var funcFilterHolder = {};
	var funcAddEditHolder = null;

	$(document).ready(function(){
		membersAddEditHolder	= $('#tab_content2');
		addTab					= $('#add-tab');
		searchTab				= $('#search-tab');
		showLogBtn 				= $('#show-log-btn');

		$('[data-toggle="tooltip"]').tooltip({
			container: 'body'
		});
	});

</script>

<!-- page content -->
<div class="right_col" role="main">

	<div class="col-md-12 col-lg-12 col-xl-10" style="padding-bottom: 60px;">
		<button id="show-log-btn" type="button" class="btn btn-primary hide" data-toggle="modal" data-target=".bs-example-modal-lg">Large modal</button>
		<div class="x_panel">
			<div class="x_title">
				<h2>Members | Clients</h2>
				<ul class="nav navbar-right panel_toolbox" style="min-width:auto!important;">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">

			    <div class="" role="tabpanel" data-example-id="togglable-tabs">
					<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
					<li role="presentation" <?php if(!empty($action) && $action === 'list'){ echo 'class="active"'; } ?>><a href="#tab_content1" id="search-tab" role="tab" data-toggle="tab" aria-expanded="<?php if(!empty($action) && $action === 'list'){ echo 'true'; }else{ echo 'false'; } ?>"><i class="fa fa-search"></i> Search</a>
					</li>
					<li role="presentation" <?php if(!empty($action) && ($action === 'add' OR $action === 'view')){ echo 'class="active"'; }  ?>><a href="#tab_content2" role="tab" id="add-tab" data-toggle="tab" aria-expanded="<?php if(!empty($action) && ($action === 'add' OR $action === 'view')){ echo 'true'; }else{ echo 'false'; } ?>"><i class="fa fa-plus"></i> Add</a>
					</li>
					</ul>
					<div id="myTabContent" class="tab-content">
						<div role="tabpanel" class="tab-pane fade <?php if(!empty($action) && $action === 'list'){?>active in<?php } ?>" id="tab_content1" aria-labelledby="search-tab">

		                    <!-- start accordion -->
		                    <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
		                      <div class="panel">
		                        <a class="panel-heading" role="tab" id="headingOne" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
		                          <h4 class="panel-title">Filters</h4>
		                        </a>
		                        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
		                          <div class="panel-body">
		                          	<div class="col-md-12">
		                          	<?php 
		                          		if(isset($filters)) {
		                          			echo $filters;
		                          		}
		                          	?>
				                    </div>
		                          </div>
		                        </div>
		                      </div>
		                      <div class="panel">
		                        <a class="panel-heading collapsed" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
		                          <h4 class="panel-title">Results <small class="badge bg-green" style="color:#fff;"></small></h4>
		                        </a>
		                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
		                          <div class="panel-body">
		                          	
		                          	<!--<div style="max-height: 420px; overflow-y: scroll; text-rendering:optimizelegibility"> -->
			                         	<table id="members-table" class="table table-striped table-hover text-left hide dt-responsive nowrap" data-order='[[ 2, "asc" ]]' data-page-length='10' cellspacing="0" width="100%"><!-- table-bordered  -->
			                              <thead>
			                                <tr>
			                                  <th style="text-align:center!important; vertical-align: middle!important;">ID</th>
			                                  <th style="text-align:center!important; vertical-align: middle!important;">Client Status</th>
			                                  <th style="text-align:center!important; vertical-align: middle!important;" data-class-name="priority">Company Name</th>
			                                  <th style="text-align:center!important; vertical-align: middle!important;">Contact Name</th>
			                                  <th style="text-align:center!important; vertical-align: middle!important;">Client Type</th>
			                                  <th style="text-align:center!important; vertical-align: middle!important;">State</th>
			                                  <th style="text-align:center!important; vertical-align: middle!important;">Payments</th>
			                                  <th style="text-align:center!important; vertical-align: middle!important;">Invest</th>
			                                  <th style="text-align:center!important; vertical-align: middle!important;">Billing Cycle</th>
			                                  <th style="text-align:center!important; vertical-align: middle!important;">Commencement</th>
			                                  <th style="text-align:center!important; vertical-align: middle!important;">Expires</th>
			                                  <th style="text-align:center!important; vertical-align: middle!important;">Next Audit</th>
			                                  <th style="text-align:center!important; vertical-align: middle!important;">Consul Hrs Remain</th>
			                                  <th style="text-align:center!important; vertical-align: middle!important;"></th>		                                  
			                                </tr>
			                              </thead>
			                              <tbody id="row-holder">
			                              </tbody>
			                            </table>
		                            <!-- </div> -->

		                            <div id="loader" class="progress-bar progress-bar-success hide" data-transitiongoal="100" aria-valuenow="0" style="height:2px;"></div>
		                            <div class="clearfix"><p>&nbsp;</p></div>
		                            <div id="total-label" class="text-center"><p class="hide"></p></div>

		                          </div>
		                        </div>
		                      </div>
		                    </div>
		                    <!-- end of accordion -->

						</div>

						<div role="tabpanel" class="tab-pane fade <?php if(!empty($action) && ($action === 'add' OR $action === 'view')){?>active in<?php } ?>" id="tab_content2" aria-labelledby="add-tab">
                      	<?php 
                      		if(isset($add_edit)) {
                      			echo $add_edit;
                      		}
                      	?>
						</div>
					</div>
			    </div>

			</div>
		</div>
	</div>

	<div class="clearfix"></div>
</div>

<div class="hide">
	<!-- Download Log -->
	<table id="members-download-table" class="table table-striped table-hover text-left" data-order='[[ 1, "desc" ]]' data-page-length='10' cellspacing="0" width="100%"><!-- table-bordered  -->
		<thead>
		<tr>
		<th style="vertical-align: middle!important;">Download Name</th>
		<th style="vertical-align: middle!important;" data-class-name="priority">When</th>
		<th style="vertical-align: middle!important;">Type</th>
		</tr>
		</thead>
		<tbody></tbody>
	</table>
	<!-- Download Log -->

	<!-- Email Log -->
	<table id="members-email-table" class="table table-striped table-hover text-left" data-order='[[ 4, "desc" ]]' data-page-length='10' cellspacing="0" width="100%"><!-- table-bordered  -->
		<thead>
		<tr>
		<th style="vertical-align: middle!important;">To</th>
		<th style="vertical-align: middle!important;">From</th>
		<th style="vertical-align: middle!important;">Subject</th>
		<th style="vertical-align: middle!important;">When</th>
		<th style="vertical-align: middle!important;">Status</th>
		<th style="vertical-align: middle!important;">Opened</th>
		<th style="vertical-align: middle!important;">View</th>
		</tr>
		</thead>
		<tbody></tbody>
	</table>
	<!-- Email Log -->

	<!-- Login Log -->
	<table id="members-login-table" class="table table-striped table-hover text-left" data-order='[[ 1, "desc" ]]' data-page-length='10' cellspacing="0" width="100%"><!-- table-bordered  -->
		<thead>
		<tr>
		<th style="vertical-align: middle!important;">Date / Time</th>
		<th style="vertical-align: middle!important;">IP</th>
		</tr>
		</thead>
		<tbody></tbody>
	</table>
	<!-- Login Log -->	
</div>

<div id="member-logs" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

			<div class="modal-header">
				<h4 class="modal-title">Title | <small></small></h4>
			</div>
			<div class="modal-body">
				<div id="download-log-search" class="form-group profile_title" style="margin-bottom:10px;">
					<label class="control-label col-md-1 col-sm-1 col-xs-12">Show:</label>
					<div class="col-md-3 col-sm-3 col-xs-12">
						<select class="form-control">
							<option value="">All</option>
							<option value="Download">Download</option>
							<option value="System Upgrade">System Upgrade</option>
							<option value="Conference Download">Conference Download</option>
						</select>
					</div>
					<div class="clearfix"></div>
				</div>

				<div id="email-log-search" class="form-group profile_title" style="margin-bottom:10px;">
					<label class="control-label col-md-1 col-sm-1 col-xs-12">Show:</label>
					<div class="col-md-3 col-sm-3 col-xs-12">
						<select class="form-control">
							<option value="all">All</option>
							<option value="campaign">Campaign Emails</option>
							<option value="oneoffs">One Offs</option>
						</select>
					</div>
					<div class="clearfix"></div>
				</div>
				
				<div id="log-content"></div>
				<div class="text-center"><label id="log-loader"></label></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>

		</div>
	</div>
</div>

<div id="member-award-stats-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

			<div class="modal-header">
				<h4 class="modal-title">Monthly Award Statistics Data</h4>
			</div>
			<div class="modal-body">

				<form id="monthlystats-form" class="form-horizontal form-label-left">
					<input id="data-statId" type="hidden" name="statId" value="" />
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-12 col-xs-12">For the month of:</label>
						<div class="col-md-5 col-sm-12 col-xs-12">
					        <div class='input-group date' id='forMonth'>
					            <input id="data-forMonth" name="forMonth" type='text' class="form-control" placeholder="ex. <?php echo date('F', now()) . ' ' . date('Y', now()); ?>" />
					            <span class="input-group-addon">
					            <span class="glyphicon glyphicon-calendar"></span>
					            </span>
					        </div>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-12 col-xs-12">Member: <i class="fa fa-asterisk text-danger"></i></label>
						<div class="col-md-5 col-sm-12 col-xs-12">
							<input id="data-memberId" type="hidden" name="memberId" value="" />
							<input id="data-memberName" type="text" class="form-control" placeholder="" value="" readonly="true" />
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-12 col-xs-12">Vacancy rate: <i class="fa fa-asterisk text-danger"></i></label>
						<div class="col-md-5 col-sm-12 col-xs-12">
							<div class="input-group input-group-md">
								<label class="input-group-addon" for="vacancyRate"><i class="fa fa-percent"></i></label>
								<input id="data-vacancyRate" name="vacancyRate" type="text" class="form-control" placeholder="" value="" data-toggle="tooltip" data-toggle="tooltip" data-placement="right" title="Please retain rental listing sheet as at the end of the month for verification (Total department average vacancy %)"/>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-12 col-xs-12">Rent arrears rate: <i class="fa fa-asterisk text-danger"></i></label>
						<div class="col-md-5 col-sm-12 col-xs-12">
							<div class="input-group input-group-md">
								<label class="input-group-addon" for="rentArrears"><i class="fa fa-percent"></i></label>
								<input id="data-rentArrears" name="rentArrears" type="text" class="form-control" placeholder="" value="" data-toggle="tooltip" data-toggle="tooltip" data-placement="right" title="Please retain rent arrears printout as at the end of the month (for verification Total department average arrears % calculate from greater than 1 days)" />
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-12 col-xs-12">Commission increase for rent increases for the dept: <i class="fa fa-asterisk text-danger"></i></label>
						<div class="col-md-5 col-sm-12 col-xs-12">
							<div class="input-group input-group-md">
								<label class="input-group-addon" for="comissionIncrease"><i class="fa fa-dollar"></i></label>
								<input id="data-comissionIncrease" name="comissionIncrease" type="text" class="form-control" placeholder="" value="" data-toggle="tooltip" data-toggle="tooltip" data-placement="right" title="Taken from properties let & tenancy renewal sheet" />
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-12 col-xs-12">Dollar value for properties gain for the dept: <i class="fa fa-asterisk text-danger"></i></label>
						<div class="col-md-5 col-sm-12 col-xs-12">
							<div class="input-group input-group-md">
								<label class="input-group-addon" for="propertiesGainedDollar"><i class="fa fa-dollar"></i></label>
								<input id="data-propertiesGainedDollar" name="propertiesGainedDollar" type="text" class="form-control" placeholder="" value="" data-toggle="tooltip" data-toggle="tooltip" data-placement="right" title="Taken from properties gained sheet" />
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-12 col-xs-12">Dollar value for lost properties for the dept: <i class="fa fa-asterisk text-danger"></i></label>
						<div class="col-md-5 col-sm-12 col-xs-12">
							<div class="input-group input-group-md">
								<label class="input-group-addon" for="propertiesLostDollar"><i class="fa fa-dollar"></i></label>
								<input id="data-propertiesLostDollar" name="propertiesLostDollar" type="text" class="form-control" placeholder="" value="" data-toggle="tooltip" data-toggle="tooltip" data-placement="right" title="Taken from properties lost sheet" />
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-12 col-xs-12">Number of properties gained for the dept: <i class="fa fa-asterisk text-danger"></i></label>
						<div class="col-md-5 col-sm-12 col-xs-12">
							<input id="data-numberOfPropertiesGained" name="numberOfPropertiesGained" type="text" class="form-control" placeholder="" value="" data-toggle="tooltip" data-toggle="tooltip" data-placement="right" title="Taken from properties gained sheet (excludes properties purchased)"/>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-12 col-xs-12">Number of properties lost for the dept: <i class="fa fa-asterisk text-danger"></i></label>
						<div class="col-md-5 col-sm-12 col-xs-12">
							<input id="data-numberOfPropertiesLost" name="numberOfPropertiesLost" type="text" class="form-control" placeholder="" value="" data-toggle="tooltip" data-toggle="tooltip" data-placement="right" title="Taken from properties lost sheet"/>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-12 col-xs-12">Support award: </label>
						<div class="col-md-9 col-sm-12 col-xs-12">
							<textarea id="data-individualAchievement" name="individualAchievement" class="form-control" rows="5" data-toggle="tooltip" data-toggle="tooltip" data-placement="left" title="Write in less than 100 words why you believe a member of your support team should receive an achievement award."></textarea>
						</div>
					</div>

				</form>

			</div>
			<div class="modal-footer">
				<button id="mas-cancel-btn" type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
				<button id="mas-add-btn" type="button" class="btn btn-success">Add</button>
				<button id="mas-update-btn" type="button" class="btn btn-success">Update</button>
			</div>

		</div>
	</div>
</div>

<script>
	var monthlyAwardStats = null;

	$(document).ready(function(){

		var fields = {
			forMonth 					: $('#data-forMonth'),
			vacancyRate 				: $('#data-vacancyRate'),
			rentArrears 				: $('#data-rentArrears'),
			comissionIncrease 			: $('#data-comissionIncrease'),
			propertiesGainedDollar 		: $('#data-propertiesGainedDollar'),
			propertiesLostDollar 		: $('#data-propertiesLostDollar'),
			numberOfPropertiesGained 	: $('#data-numberOfPropertiesGained'),
			numberOfPropertiesLost 		: $('#data-numberOfPropertiesLost'),
			individualAchievement 		: $('#data-individualAchievement'),
			statId 						: $('#data-statId'),
			memberName					: $('#data-memberName')
		};

		var form 						= $('#monthlystats-form');
		var listTable 					= null;
		var currentDataTable 			= null;
		var currentTable 				= null;
		var tableId 					= '';
		var cancelBtn 					= $('#mas-cancel-btn');
		var addBtn 						= $('#mas-add-btn');
		var updateBtn 					= $('#mas-update-btn');	

		monthlyAwardStats = {
			isEdit: false,
			masterList: null,
			tempFunc: null,
			init: function(){
				$('#forMonth').datetimepicker({
					format: 'MMMM YYYY'
				});

				$('#member-award-stats-modal').on('hidden.bs.modal', function () {
					monthlyAwardStats.isEdit = false;
					monthlyAwardStats.resetForm();
				});

				addBtn.bind('click', function(){
					monthlyAwardStats.submitForm();
				});

				updateBtn.bind('click', function(){
					monthlyAwardStats.submitForm();
				});				
			},
			setMember: function(id, name){
				$('#data-memberId').val(id);
				fields.memberName.val(name);
			},
			populateList: function(list){
				listTable = $('#monthly-award-stats-table');
				this.masterList = list;

				$('#members-add-edit_11').on('click', function(){
					if($(this).hasClass('collapsed')) {
						monthlyAwardStats.refreshList();
						monthlyAwardStats.processList();
					}
				});
			},
			processList: function(){
				
				setTimeout(function(){
	            	
	            	var columns = [
	            					{ data: 'forMonth' },
							        { data: 'edit' },
							        { data: 'delete' }        
							    ];

					currentDataTable = currentTable.DataTable( {
						data: monthlyAwardStats.groupData(),
					    columns: columns,
					    "scrollY": (Object.keys(monthlyAwardStats.masterList).length >=10) ? 380 : false,
	        			"scrollX": true,
	        			"ordering": false,
	        			"searching": false,
	        			"initComplete" : function(settings, json){
							monthlyAwardStats.initListAction();
	        			}
					});

					currentDataTable.on( 'draw.dt', function () {
						monthlyAwardStats.initListAction();
					});

	            }, 1);

			},
			groupData: function(){

				var arr = [];

				$.each(monthlyAwardStats.masterList, function(i,data){
					arr.push(new ColList(data));
				});

				function ColList(data){

					this.forMonth = moment(data.forMonth).format('MMMM YYYY');
					this.edit = '<button type="button" data-id="'+data.statId+'" data-action="edit" class="action-btn btn btn-xs btn-warning"><i class="fa fa-pencil"></i></button>';
					this.delete = '<button type="button" data-id="'+data.statId+'" data-action="delete" class="action-btn btn btn-xs btn-danger"><i class="fa fa-close"></i></button>';
	            }

	            return arr;

			},
			initListAction: function(){
				$.each($('#'+tableId+' button.action-btn'), function(i,v){
					$(v).on('click', function(){
						var id = $(v).data('id');
						$(this).parents('tr').attr({id:'row-'+id})
						monthlyAwardStats.processAction({id:id, memberId: $('#data-memberId').val(), action:$(v).data('action')});
					});
				});
			},		
			processAction: function(data){

				if(data!=null) {

			    	monthlyAwardStats.tempFunc = function(data) {
			    		var request = $.ajax({
				          url: "<?php echo base_url(); ?>admin/members/monthlystats",
				          method: "POST",
				          data: data,
				          dataType: 'json'
				        });

				        request.done(function( _data ) {
				            if(_data!=null) {

				            	if(data.action == 'edit') {
				            		monthlyAwardStats.isEdit = true;

				            		_data.statId = data.id;
				            		monthlyAwardStats.populateForm(_data);
				            	}
				            	else if(data.action == 'delete') {
				            		if(_data.hasOwnProperty('success')) {
				            			currentDataTable.row('#row-'+data.id).remove().draw( false );
				            			swal({title:"Success!",text:"Deleted item with ID: " + data.id, icon:'success',button:false, timer:3000});
				            		}
				            		else {
				            			swal({title:"Deleted Failed!",text:"No record was found.", icon:'error',button:false, timer:3000});
				            		}
				            	}

				            	_data = null;
				            }
		        		});

		        		request.error(function(_data){
		        			if(_data.readyState == 4 && _data.status == 200) {//Session expired and redirected
		        				swal({
								    title: "Session Expired!",
								    text: "Reloading page.",
								    icon: "warning",
								    dangerMode: false,
								})
								.then(willDelete => {
								    if (willDelete) {
								    	window.location.href = '<?php echo base_url();?>admin/login';
								    }
								});
		        			}
		        		});
			    	}

					var showAddEdit = false;

	            	if(data.action == 'edit') {
						showAddEdit = true;
	            	}
	            	else if(data.action == 'delete') {
	            		swal({
							title: "Delete Item",
							text: "Are you sure?",
							icon: "warning",
							buttons: ["Cancel", true],
							dangerMode: true,
							})
							.then(willDelete => {
							
								if(willDelete) {
									monthlyAwardStats.tempFunc.call(this, data);
								}
							
							}); 
	            	}

	            	if(showAddEdit) {
	            		if(monthlyAwardStats.tempFunc!=null) monthlyAwardStats.tempFunc.call(this, data);
			    	}
		    	}
			},
			populateForm: function(data){
				if(data!=null) {

					this.resetForm();

					setTimeout(function(){
						$.each(data, function(k,v){
							if(fields.hasOwnProperty(k)  && fields[k].length>0) {
								fields[k].val(v);
							}
						});

						$('#member-award-stats-btn').trigger('click');

					}, 50);
				}
			},
			submitForm: function(){

				var isEdit = $.isNumeric(fields.statId.val());

				swal({
				    title: (isEdit)?"Update Item" : "Add Item",
				    text: "Are you sure?",
				    icon: "warning",
				    buttons: ["Cancel", true],
				    dangerMode: false,
				})
				.then(willSubmit => {
				    if (willSubmit) {
						var request = $.ajax({
				          url: "<?php echo base_url(); ?>admin/members/monthlystats/add_update",
				          method: "POST",
				          data: form.serialize(),
				          dataType: 'json'
				        });

				        request.done(function( res ) {
				            if(res!=null) {
				            	if(isEdit) {
				            		if(res.hasOwnProperty('error')) {
				            			var errMsg = '';

				            			$.each(res.error, function(k, v){
				            				errMsg += '* ' + v + '\n';
				            			});

				            			new PNotify({
											title: 'Invalid or Required!',
											text: errMsg,
											type: 'error',
											styling: 'bootstrap3'
			                            });

				            			swal({title:"Update Failed!",text:"Some fields are required (*) or have invalid values", icon:'error',button:false, timer:3000});
				            		}
				            		else {
				            			monthlyAwardStats.applyUpdateChanges(res);

				            			swal({title:"Success!",text: "Changes have been saved.", icon:'success',button:false, timer:3000});

				            			cancelBtn.trigger('click');

				            		}
				            		
				            	}
				            	else {
				            		if(res.hasOwnProperty('error')) {
										var errMsg = '';

				            			$.each(res.error, function(k, v){
				            				errMsg += '* ' + v + '\n';
				            			});

				            			new PNotify({
											title: 'Invalid or Required!',
											text: errMsg,
											type: 'error',
											styling: 'bootstrap3'
			                            });

			                            swal({title:"Add Failed!",text:"Some fields are required (*) or have invalid values", icon:'error',button:false, timer:3000});
				            		}
				            		else {
				            			fields.statId.val(res.statId);
				            			monthlyAwardStats.showUpdateButton();
				            			swal({title:"Success!",text:"Added item with id: " + res.statId, icon:'success',button:false, timer:3000});
				            			monthlyAwardStats.addNewItem(res);
				            		}
				            	}
				            }
		        		});

				      	request.error(function(_data){
		        			if(_data.readyState == 4 && _data.status == 200) {//Session expired and redirected

		        				swal({
								    title: "Session Expired!",
								    text: "Reloading page.",
								    icon: "warning",
								    dangerMode: true,
								})
								.then(ok => {
								    if (ok) {
								    	window.location.href = '<?php echo base_url();?>admin/login';
								    }
								});
		        			}
		        		});
					}
				}); 

			},
			applyUpdateChanges: function(data){

				if(data!=null) {
					var row = $('#row-'+data.statId);

					row.children('td')[0].innerHTML = data.forMonth;
				}
			},
			addNewItem: function(res) {
				if(res!=null) {

					var data = [{
						      "forMonth":res.forMonth,
						      "edit":res.edit,
						      "delete":res.delete
							}]; 	

	            	currentDataTable.rows.add(data);
	            	currentDataTable.page( 'next' ).draw( false );
	            	currentDataTable.page( 'previous' ).draw( false );
				}
			},
			resetForm: function(){

				$.each(fields, function(k,v){
					if(v.length>0 && k!='memberName') {
						v.val('');
					}
				});
			},	
			refreshList: function(){
				if(tableId!='') {
					$('#' + tableId + '_wrapper').remove();
					currentTable = null;
					currentDataTable = null;
				}

				tableId = this.generateID();

				var tempTable = null;
					tempTable = listTable.clone().removeClass('hide').attr({id:tableId});
					tempTable.insertAfter(listTable);

				currentTable = $('#' + tableId);
				tempTable = null;
			},
			generateID: function() {
		      var charSet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
		      var charCount = 20;
		      var charSetSize = charSet.length;
		      var id = '';

		      for (var i = 1; i <= charCount; i++) {
		          var randPos = Math.floor(Math.random() * charSetSize);
		          id += charSet[randPos];
		      }

		      return id;      
		    },
		    showAddButton: function(){
				addBtn.show();
				updateBtn.hide();		    	
		    },
		    showUpdateButton: function(){
				addBtn.hide();
				updateBtn.show();		    	
		    }		    
		};

		monthlyAwardStats.init();

		<?php if($action === 'view') { ?>
			addTab.html('<i class="fa fa-pencil"></i> Edit');
		<?php } ?>
	});

</script>

<!-- /page content -->