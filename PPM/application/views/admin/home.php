<!-- page content -->
<div class="right_col" role="main">
	<div class="row">
		<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 page-title">
			<div class="title_left">
			<h3><?php echo $page_title; ?></h3>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" style="padding-bottom: 60px;">
			<div class="x_panel">
				<div class="x_title">
					<h2>Member | Client Search</h2>
					<ul class="nav navbar-right panel_toolbox" style="min-width:auto!important;">
						<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
						</li>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->