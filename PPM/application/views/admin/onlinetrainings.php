<!-- page content -->
<div class="right_col" role="main" style="min-height: 100vh;">

  <div class="row">
    <div class="col-md-12">

      <h1><i class="fa fa-cloud"></i> Online Trainings</h1>
      <button class="btn btn-primary" type="button" id="btnAddNew" > <i class="fa fa-plus"></i> Add New </button>
      <hr>
      <div class="row" >
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-3">
              <div class="form-group">
                <label>Topic</label>
                <select  name="selTopic" id="selTopic" class="form-control">
                    <option value="">[select]</option>
                    <?php
                      foreach($ol_training_list['result'] as $ol_topic){
                        echo '<option value="'.$ol_topic['name'].'">'.$ol_topic['name'].'</option>';
                      }
                    ?>
                </select>
              </div>
            </div>
          </div>

          <?php 
            // print_r($trainig_list);
          ?>
          <!-- START : LIST TABLE -->
          <table class="table table-striped" id="tblProductList">
            <thead>
              <tr>
                <!-- <td><input type="checkbox" id="chkAllItems" class="flat"></td> -->
                <td>Topic</td>
                <td>Title</td>
                <td>Speaker</td>
                <td>Duration</td>
                <td>Type</td>
                <td>Hits</td>
                <td>Status</td>
                <td>Action</td>
              </tr>
            </thead>
            <tbody>
                <?php
                  foreach($trainig_list['result'] as $tlist){
// topic
// title
// speaker
// duration
                    echo '
                        <tr>
                          <!-- <td><input type="checkbox" id="chkAllItems" class="flat"></td> -->
                          <td>'.$tlist['topic'].'</td>
                          <td>'.$tlist['title'].'</td>
                          <td>'.$tlist['speaker'].'</td>
                          <td>'.$tlist['duration'].'</td>
                          <td>'.$tlist['myType'].'</td>
                          <td>'.$tlist['hits'].'</td>
                          <td>'.(($tlist['draft'] == '1') ? '<span style="color:red;">DRAFT</span>':'<span style="color:green;">LIVE</span>') .'</td>
                          <td>
                              <button class="btn btn-success btnEditPage" type="button" data-pid="'.$tlist['id'].'"><i class="fa fa-pencil"></i> Edit</button>
                              <button class="btn btn-danger btnDeletePage" type="button" data-pid="'.$tlist['id'].'"><i class="fa fa-trash"></i> Delete</button>
                          </td>
                        </tr>
                    ';
                  }
                  // $e_list = get_list_events();
                  // foreach($e_list as $list){
                  //       echo '<tr>
                  //               <td>'.$list['name'].'</td>
                  //               <td>'.$list['shortdesc'].'</td>
                  //               <td>'.date('d/m/Y', strtotime($list['startDate'])).'</td>
                  //               <!--<td>'.date('d/m/Y', strtotime($list['endDate'])).'</td>-->
                  //               <td>'.$list['myTime'].'</td>
                  //               <td></td>
                  //             </tr>
                  //       ';
                  // }

                ?>
            </tbody>
          </table>
          <!-- START : LIST TABLE -->

        </div>


<!--         <div class="x_panel">
          <div class="x_title">
            <h2><i class="fa fa-pagelines"></i> Events</h2>
            <button class="btn btn-primary" type="button" id="btnAddNew" style="margin-left: 20px;"> <i class="fa fa-plus"></i> Add New </button>
            <ul class="nav navbar-right panel_toolbox" style="min-width:auto!important;">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
          

          </div>
        </div> -->


      </div>

    </div>

  </div>

	<div class="clearfix"></div>
</div>

<div id="member-logs" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

			<div class="modal-header">
				<h4 class="modal-title">Title</h4>
			</div>
			<div class="modal-body">

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>

		</div>
	</div>
</div>

<!-- /page content -->

<style>
.spacer10 {
  clear: both;
  width: 100%;
  height: 10px;
}
.spacer20 {
  clear: both;
  width: 100%;
  height: 20px;
}
.spacer30 {
  clear: both;
  width: 100%;
  height: 30px;
}

#tblProductList thead td {
  font-weight: bold;
}

</style>

<script>
  var tblProdData = "";
  $(document).ready(function(){
    var baseurl = '<?php echo base_url(); ?>';

    $('#chkAllItems').iCheck({
      checkboxClass: 'icheckbox_square',
      radioClass: 'iradio_square',
      increaseArea: '20%' // optional
    });

    // setTimeout(function(){
      tblProdData = $('#tblProductList').DataTable({
        "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        "columnDefs": [{ "orderable": false, "targets": [0, 4] }],
        "order": [[ 0, "asc" ]],
        "scrollY": "400px",
        "scrollCollapse": true,
        "paging": true
      });
    // },1500);

    //EDIT
    $('#tblProductList tbody').on('click', ' td > .btnEditPage', function(){
      // alert('Test' + $(this).data('pid'));
      window.location = baseurl + 'admin/onlinetrainings/edit/' + $(this).data('pid') + '/';
    });

    //DELETE
    $('#tblProductList tbody').on('click', ' td > .btnDeletePage', function(){
        var page_id = $(this).data('pid');
        // var q = confirm('Are you sure you want to delete the page?');
        var $this = $(this);
        swal({
          title: "Are you sure?",
          text: "Once deleted, you will not be able to recover this data!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            // swal("Poof! Your imaginary file has been deleted!", {
            //   icon: "success",
            // });
            $.ajax({
              type : 'post',
              url  : baseurl + 'admin/onlinetrainings/delete',
              data : {
                'page_id' : page_id
              },
              beforeSend : function(){

              },
              success : function(result){
                tblProdData.row( $this.parents('tr') ).remove().draw();
                swal("Poof! Page has been Deleted!", {
                  icon: "success",
                });
              }
            });

          } else {
            // swal("Your imaginary file is safe!");
          }
        });

    });

    $('#btnAddNew').click(function(){
      // $.ajax({
      //   type: 'post',
      //   url : baseurl + 'admin/pages/test',
      //   success:function(res){
      //     console.log(res);
      //   }
      // });
      window.location = baseurl + 'admin/onlinetrainings/add';
    });

    $('#selTopic').change(function(){
      var s = $(this).val();
      if(s != ''){
        tblProdData.column(0).search(s).draw();
      }else{
        window.location.reload();
      }
      
    });
    

  });
</script>
