        <!-- footer content -->
        <footer style="border-top: 1px solid rgba(0,0,0,0.1);">

          <div class="pull-right">
            <?php if(isset($show_categories)) { ?>
              <?php echo $show_categories; ?>
            <?php } else { ?>
              <p><?php echo APP_TITLE; ?></p>
            <?php } ?>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- Bootstrap -->
    <script src="<?php echo base_url(); ?>assets/gentelella/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url(); ?>assets/gentelella/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url(); ?>assets/gentelella/vendors/nprogress/nprogress.js"></script>
    <!-- jQuery custom content scroller -->
    <script src="<?php echo base_url(); ?>assets/gentelella/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/sweetalert/dist/sweetalert.min.js"></script>
    
    <?php 
      if(isset($other_scripts)) {
        foreach ($other_scripts as $value) {
          echo $value;
        }
      }
    ?>

    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url(); ?>assets/gentelella/build/js/custom.ppm.js"></script>
	
  </body>
</html>