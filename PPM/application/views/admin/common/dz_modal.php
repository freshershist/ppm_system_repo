<?php 

/**

Note: 

**/

function check_subdirectory($key, $subDir){
	echo '<li><label data-path="' . $key . '">'.$key.'</label><ul>';

	foreach ($subDir as $k1 => $v1) {
		if(is_array($v1)) {
            check_subdirectory(str_replace('\\', '/', $k1), $v1);
		}
	}
	echo '</ul></li>';
}

?>

<script src="<?php echo base_url() . 'assets/ckeditor/ckeditor.js'; ?>"></script>

<style type="text/css">

.gallery-del {
	position: absolute;
	top: 8px;
	right: 5px;	
}

.gallery-view {
	position: absolute;
	top: 8px;
	right: 30px;
}

.tree, .tree ul {
    margin:0;
    padding:0;
    list-style:none
}
.tree ul {
    margin-left:1em;
    position:relative
}
.tree ul ul {
    margin-left:.5em
}
.tree ul:before {
    content:"";
    display:block;
    width:0;
    position:absolute;
    top:0;
    bottom:0;
    left:0;
    border-left:1px solid
}
.tree li {
    margin:0;
    padding:0 1em;
    line-height:2em;
    color:#E7E7E7;/*1ABB9C*/
    font-weight:700;
    position:relative
}
.tree ul li:before {
    content:"";
    display:block;
    width:10px;
    height:0;
    border-top:1px solid;
    margin-top:-1px;
    position:absolute;
    top:1em;
    left:0
}
.tree ul li:last-child:before {
    background:#2a3f54;
    height:auto;
    top:1em;
    bottom:0
}
.indicator {
    margin-right:5px;
}
.tree li a {
    text-decoration: none;
    color:#E7E7E7;
}
.tree li button, .tree li button:active, .tree li button:focus {
    text-decoration: none;
    color:#E7E7E7;
    border:none;
    background:transparent;
    margin:0px 0px 0px 0px;
    padding:0px 0px 0px 0px;
    outline: 0;
}
.tree li label {
	cursor: pointer;
}

.delete-btn {
    float: right;
    margin-top: -32px;
    margin-right: 10px;
    position: relative;
    z-index: 10;
}

div.dir-holder {
    position: absolute;
    background: #2a3f54;
    z-index: 10000;
    height: 60vh;
}

form.dropzone {
    min-height: 60vh;
}

.fade-in {
	opacity: 0.95;
    -webkit-transform: translate(-100%, 0);
       -moz-transform: translate(-100%, 0);
        -ms-transform: translate(-100%, 0);
            transform: translate(-100%, 0);
    transition: all .45s ease;
}

.fade-in.show {
    -webkit-transform: translate(0, 0);
       -moz-transform: translate(0, 0);
        -ms-transform: translate(0, 0);
            transform: translate(0, 0);
}

.dir-btn {
    background: #2a3f54;
    color: #E7E7E7;
    display: inline-block;
    margin: 0 1em 1em 0;    
    border-bottom-right-radius: 90px;
    border-top-right-radius: 90px;
    border-top-right-shadow:  
    height: 45px;
    width: 25px;
    position: absolute;
    right: 0;
    margin-right: -25px;
    top: calc(50% - 22px);
    font-size: 25px;
    padding-top: 5px;
    padding-left: 3px;
    text-decoration: none;
}

.dir-btn.show {
    border-bottom-left-radius: 90px;
    border-top-left-radius: 90px;
    border-bottom-right-radius: 0px;
    border-top-right-radius: 0px;
    padding-left: 8px;      
    -webkit-transform: rotate(180deg);
       -moz-transform: rotate(180deg);
        -ms-transform: rotate(180deg);
            transform: rotate(180deg);
}

.dir-btn:active {
    color: #E7E7E7!important;
    text-decoration: none; 
}

.dropzone .dz-preview .dz-image {
	border: 1px solid rgba(0,0,0,0.1);
}

.dropzone .dz-preview .dz-image img {
	display: block;
	margin: auto;
}

.dropzone.not-image .dz-image img{
	margin: auto;
	margin-top: calc(50% - 32px);
}

.dropzone.not-image .dz-details {
	border: 1px solid rgba(42,63,84,0.1);
	border-radius: 20px;
	padding: 1.3em 1em;
}

</style>

<script type="text/javascript">
$.fn.extend({
    treed: function (o) {
      
      var openedClass = 'glyphicon-minus-sign';
      var closedClass = 'glyphicon-plus-sign';
      
      if (typeof o != 'undefined'){
        if (typeof o.openedClass != 'undefined'){
        openedClass = o.openedClass;
        }
        if (typeof o.closedClass != 'undefined'){
        closedClass = o.closedClass;
        }
      };
      
        //initialize each of the top levels
        var tree = $(this);
        tree.addClass("tree");
        tree.find('li').has("ul").each(function () {
            var branch = $(this); //li with children ul
            branch.prepend("<i class='indicator glyphicon " + closedClass + "'></i>");
            branch.addClass('branch');
            branch.on('click', function (e) {
                if (this == e.target) {
                    var icon = $(this).children('i:first');
                    icon.toggleClass(openedClass + " " + closedClass);
                    $(this).children().children().toggle();
                }
            })
            branch.children().children().toggle();
        });
        //fire event from the dynamically added icon
      tree.find('.branch .indicator').each(function(){
        $(this).on('click', function () {
            $(this).closest('li').click();
        });
      });
        //fire event to open branch if the li contains an anchor instead of text
        tree.find('.branch>a').each(function () {
            $(this).on('click', function (e) {
                $(this).closest('li').click();
                e.preventDefault();
            });
        });
        //fire event to open branch if the li contains a button instead of text
        tree.find('.branch>button').each(function () {
            $(this).on('click', function (e) {
                $(this).closest('li').click();
                e.preventDefault();
            });
        });
    }
});

</script>


<!-- Large modal -->
<button type="button" id="ppm-modal-btn" class="btn btn-primary hide" data-toggle="" data-target="">Show Gallery</button>

<div id="ppm-modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-xl">
  <div class="modal-content">

    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
      </button>
      <h4 class="modal-title" id="gallery-title"><font>Gallery </font> | <small id="dir-breadcrumbs">files / </small></h4>
    </div>
    <div class="modal-body">
		<div class="alert alert-info">
			<h2 class="pull-left">Drop files below or Click browse button to Upload.</h2>
			<button id="browse" type="button" class="btn btn-info btn-md pull-right">Browse</button>
			<div class="clearfix"></div>
		</div>

    	<div class="hide-overflow" style="max-height: 65vh; min-height: 65vh;">
	    	<div class="dir-holder fade-in show">
				<div class="form-group top_search" style="display: inline-table; padding: 10px 10px 0px 10px;">
					<div class="input-group">
					<input id="create-folder" type="text" class="form-control" placeholder="New Folder...">
					<span class="input-group-btn">
					  <button id="create-folder-btn" class="btn btn-default" type="button">Create</button>
					</span>
					</div>
				</div>

				<div class="scroll-overflow" style="height: 50vh;">
	            	<ul id="directory" class="hide">
	                    <li>
	                        <label id="path-root" data-path="files/">files/</label>
	                        <ul>
	                            <li>
	                        	<?php
	                        		if(isset($directory) && is_array($directory)) {
	                        			foreach ($directory as $key => $value) {
	                        				if(is_array($value)) {
	                        					check_subdirectory(str_replace('\\', '/', $key), $value);
	                        				}
	                        			}
	                        		}
	                        	?>
	                            </li>
	                        </ul>
	                    </li>    
	            	</ul>
            	</div>
                <a id="show-dir-btn" href="javascript:;" class="dir-btn show"><i class="fa fa-chevron-right"></i></a>
            </div>

            <div class="scroll-overflow" style="height: 60vh; overflow-x:hidden;">
			<?php
				$data = array(
			        'id'         => 'ppm-dropzone',
			        'class'		 => 'dropzone'
				);

				echo form_open('admin/uploads', $data);
				echo '<input id="form-path" type="hidden" name="path" value=""/>';
				echo '<div class="dz-message hide"><h2>Drop here.</h2></div>';
				echo '<button type="button" class="hide browse">browse</button>';
				echo form_close();
			?>
			</div>
		</div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>

  </div>
</div>
</div>

<div id="dz-tpl" class="hide">
	<div class="dz-preview dz-image-preview">
		<div class="dz-image">
			<img data-dz-thumbnail="" alt="" src="">
		</div>
		<div class="dz-details">
			<button type="button" class="btn btn-xs btn-success gallery-view hide"><i class="fa fa-eye"></i></button>
			<button type="button" class="btn btn-xs btn-danger gallery-del"><i class="fa fa-trash"></i></button>
			<span style="font-size:20px; color: white; ">&nbsp;</span>
			<br/>
			<span style="font-size:20px; color: white;  text-shadow: 0 0 5px #2A3F54;">Select</span>
			<br/>
			<span style="font-size:20px; color: white; ">&nbsp;</span>
			<div class="dz-filename"><span data-dz-name=""></span></div>  
		</div>
		<div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress=""></span></div>  
		<div class="dz-error-message"><span data-dz-errormessage=""></span></div>  
		<div class="dz-success-mark">    
			<svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">      <title>Check</title>      <defs></defs>      <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">        <path d="M23.5,31.8431458 L17.5852419,25.9283877 C16.0248253,24.3679711 13.4910294,24.366835 11.9289322,25.9289322 C10.3700136,27.4878508 10.3665912,30.0234455 11.9283877,31.5852419 L20.4147581,40.0716123 C20.5133999,40.1702541 20.6159315,40.2626649 20.7218615,40.3488435 C22.2835669,41.8725651 24.794234,41.8626202 26.3461564,40.3106978 L43.3106978,23.3461564 C44.8771021,21.7797521 44.8758057,19.2483887 43.3137085,17.6862915 C41.7547899,16.1273729 39.2176035,16.1255422 37.6538436,17.6893022 L23.5,31.8431458 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" stroke-opacity="0.198794158" stroke="#747474" fill-opacity="0.816519475" fill="#FFFFFF" sketch:type="MSShapeGroup"></path>      </g>    
			</svg>  
		</div>  
		<div class="dz-error-mark">    
			<svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">      <title>Error</title>      <defs></defs>      <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">        <g id="Check-+-Oval-2" sketch:type="MSLayerGroup" stroke="#747474" stroke-opacity="0.198794158" fill="#FFFFFF" fill-opacity="0.816519475">          <path d="M32.6568542,29 L38.3106978,23.3461564 C39.8771021,21.7797521 39.8758057,19.2483887 38.3137085,17.6862915 C36.7547899,16.1273729 34.2176035,16.1255422 32.6538436,17.6893022 L27,23.3431458 L21.3461564,17.6893022 C19.7823965,16.1255422 17.2452101,16.1273729 15.6862915,17.6862915 C14.1241943,19.2483887 14.1228979,21.7797521 15.6893022,23.3461564 L21.3431458,29 L15.6893022,34.6538436 C14.1228979,36.2202479 14.1241943,38.7516113 15.6862915,40.3137085 C17.2452101,41.8726271 19.7823965,41.8744578 21.3461564,40.3106978 L27,34.6568542 L32.6538436,40.3106978 C34.2176035,41.8744578 36.7547899,41.8726271 38.3137085,40.3137085 C39.8758057,38.7516113 39.8771021,36.2202479 38.3106978,34.6538436 L32.6568542,29 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" sketch:type="MSShapeGroup"></path>        </g>      </g>  </svg>  
		</div>
	</div>
</div>

<script>
	var selectedFilePath = '';

	if($!=null) {

		var PPMGallery = {};
		
		$(function() {

			var ppm_modal 			= $('#ppm-modal');
			var processing 			= false;
			var base_url 			= '<?php echo base_url(); ?>';
			var file_info 			= null;
			var myDropzone 			= null;
			var params              = {'path':"files/"};
			var dir_breadcrumbs     = $('#dir-breadcrumbs');
			var createFolder        = $('#create-folder');
			var createFolderBtn     = $('#create-folder-btn');
			var pathSelected        = $('#path-root');
			var directory           = $('#directory');
			var formPath 			= $('#form-path');
			var galleryTitle 		= $('#gallery-title font');
			var fileIconPath 		= 'assets/images/file_icons/';
			var pdfIcon				= 'pdf.png';
			var xlsIcon 			= 'xls.png';
			var pptIcon 			= 'ppt.png';
			var psdIcon 			= 'psd.png';
			var zipIcon 			= 'zip.png';
			var docIcon 			= 'doc.png';
			var mp3Icon				= 'mp3.png';
			var flvIcon				= 'flv.png';
			var wmvIcon				= 'wmv.png';
			var aviIcon				= 'avi.png';
			var movIcon				= 'mov.png';
			var thumbnailSize		= 128;
			var inputSelected 		= null;

			$('#browse').on('click', function(){
				myDropzone.clickableElements[0].click();
			});

			$("#ppm-modal-btn").click(function(){

	            if(Dropzone.options.ppmDropzone!=null) Dropzone.options.ppmDropzone.clearGallery();

	            var request = $.ajax({
	                url: base_url + "admin/gallery/get_files",
	                method: "POST",
	                data: params,
	                dataType: 'json'
	            });

	            request.done(function( res ) {
	                if(res!=null) {
	                    if(res.length>0) {
	                        file_info = res;
	                        if(Dropzone.options.ppmDropzone!=null) {
	                        	Dropzone.options.ppmDropzone.populateGallery();
	                        }
	                    }
	                    else {
	                    	ppm_modal.modal({backdrop: "static"});
	                    }
	                }
	            });

	            request.error(function(_data){
        			if(_data.readyState == 4 && _data.status == 200) {//Session expired and redirected
        				swal({
						    title: "Session Expired!",
						    text: "Reloading page.",
						    icon: "warning",
						    dangerMode: false,
						})
						.then(willDelete => {
						    if (willDelete) {
						    	window.location.href = '<?php echo base_url();?>admin/login';
						    }
						});
        			}
        		});

	    	});

			ppm_modal.on('hide.bs.modal', function () {
				PPMGallery.isEditor = false;
				myDropzone.removeAllFiles(true);
			});

			ppm_modal.on('show.bs.modal', function(){
		    	<?php if(isset($is_homepage) && $is_homepage) { ?>
		    		var _label = $('<label data-path="homepage/">homepage/</label>');
		    		openFolder(_label);
		    	<?php } ?>				
			});

		    Dropzone.options.ppmDropzone = {
		        parallelUploads: 30,
		        maxFiles: 30,
		        thumbnailWidth:thumbnailSize,
		        thumbnailHeight:thumbnailSize,
		        acceptedFiles: null,
		        addRemoveLinks: false,
		        previewTemplate: $('#dz-tpl').html(),
		        clickable: [".browse"],
		        params: params,

		        // Dropzone settings
		        init: function () {
		            myDropzone = this;

		            myDropzone.on("sendingmultiple", function (files) {

		            });

		            myDropzone.on("errormultiple", function (files, response) {
		                return false;
		            });

		            myDropzone.on("success", function(file, responseText) {
		            	try {
		            		//console.log(responseText);
		            		var res = JSON.parse(responseText);

		            		if(res.hasOwnProperty('error')) {
								var error_msg = file.previewElement.querySelector("[data-dz-errormessage]").innerHTML;
				            	var error = $(res.error.error);

				            	swal("Oops!", error.text(), "error");

				            	this.removeFile(file);
		            		}
		            		else {
		            			
		            			file_info = [{name:res.filename, size:file.size}];

		            			this.removeFile(file);
		            			Dropzone.options.ppmDropzone.populateGallery();
		            		}
				    	}
				    	catch(e){};
				    });

		            myDropzone.on("addedfile", function(file) {
		            	//console.log(file);
					    var dz_details 		= file.previewElement.children[1].children[4];
					    var removeButton 	= file.previewElement.children[1].children[1];
					    var viewButton 		= file.previewElement.children[1].children[0];

					    dz_details.addEventListener("click", function(e) {
							e.preventDefault();
							e.stopPropagation();

							if(!processing) {
								processing = true;
								var filename = file.previewElement.querySelector("[data-dz-name]").innerHTML;
								
								var	path = (params.path != 'files/') ? params.path : '';
								

								if(filename!='') Dropzone.options.ppmDropzone.addImageToEditor(base_url + 'assets/uploads/files/' + path + filename);

								if(inputSelected!=null && PPMGallery.AddImage==null) {
									inputSelected.val(path + filename).trigger('change');

									selectedFilePath = '<?php echo base_url();?>assets/uploads/files/' + path + filename;
								}
							}

					    });

					    dz_details.addEventListener("mouseover", function(e) {
							e.preventDefault();
							e.stopPropagation();

							this.style.cursor = "pointer";

					    });

					    removeButton.addEventListener("click", function(e) {
					      // Make sure the button click doesn't submit the form:
					      e.preventDefault();
					      e.stopPropagation();

					      Dropzone.options.ppmDropzone.deleteFile(file);

					    });

					    viewButton.addEventListener("click", function(e) {
					      // Make sure the button click doesn't submit the form:
					      e.preventDefault();
					      e.stopPropagation();

					      //View code here

					    });

		            });

		            myDropzone.on("removedfile", function(file) {});

		            myDropzone.on("error", function(file){

		            	var error_msg = file.previewElement.querySelector("[data-dz-errormessage]").innerHTML;
		            	
		            	swal("Oops!", error_msg, "error");

		            	this.removeFile(file);
		            });
		        },
		        deleteFile: function (file) {

	        	  swal({
				    title: "Delete File.",
				    text: "Are you sure?",
				    icon: "warning",
				    buttons: ["Cancel", true],
				    dangerMode: false,
				  })
				  .then(willDelete => {
				    if (willDelete) {
						var _data = {filename:file.previewElement.querySelector("[data-dz-name]").innerHTML, path:params.path};

						var request = $.ajax({
						url: base_url + "admin/gallery_delete",
						method: "POST",
						data: _data,
						dataType: 'json'
						});

						request.done(function( res ) {
							if(res!=null) {
								try{
									if(res.result == 'success') {
										myDropzone.removeFile(file);
										swal("Deleted!", file.name + " has been deleted.", "success");
									}
									else {
										console.log("Failed");
									}
									
								}
								catch(e){console.log("Invalid Format.");}
							}
							
						});

						request.error(function(_data){
		        			if(_data.readyState == 4 && _data.status == 200) {//Session expired and redirected
		        				swal({
								    title: "Session Expired!",
								    text: "Reloading page.",
								    icon: "warning",
								    dangerMode: false,
								})
								.then(willDelete => {
								    if (willDelete) {
								    	window.location.href = '<?php echo base_url();?>admin/login';
								    }
								});
		        			}
		        		});
				      
				    }
				  });
		        },
		        addImageToEditor: function (filename) {
		        	if(PPMGallery.AddImage!=null) {
						PPMGallery.AddImage.call(this, filename);
						PPMGallery.AddImage = null;
					}
					//console.log(myDropzone.getUploadingFiles().length);
					if(myDropzone.getUploadingFiles().length==0) {
						ppm_modal.delay(500).hide(0, function() {
					        ppm_modal.modal('hide');
					    });
					}
		        },
		        clearGallery: function(){
		            $.each($('.dz-preview.dz-complete.dz-image-preview'), function(k,v){
		                $(v).remove();
		            });
		        },
		        populateGallery: function(){
		        	var ctr = 0;

		        	thumbnailSize = (params.file_type != 'image') ? 64 : 128;

		        	myDropzone.options.thumbnailWidth = thumbnailSize;
		        	myDropzone.options.thumbnailHeight = thumbnailSize;

		        	if(params.file_type != 'image') {
		        		$('form.dropzone').addClass('not-image');
		        	}
		        	else {
		        		$('form.dropzone').removeClass('not-image');
		        	}

		        	//console.log(file_info);

		            $.each(file_info, function(k,v){
		            	ctr++;
		                // Create the mock file:
		                var mockFile = { name: v.name, size: parseInt(v.size) };

		                // Call the default addedfile event handler
		                myDropzone.emit("addedfile", mockFile);
		                
		                var filePath = '';

		                if(params.file_type == 'application') {
		                	//console.log(v);
		                	var iconName = '';

		                	switch(getFileExtension(v.name)) {
		                		case 'pdf':
		                		iconName = pdfIcon;
		                		break;

		                		case 'xls':
		                		case 'xlsx':
		                		iconName = xlsIcon;
		                		break;

		               			case 'doc':
		                		case 'docx':
		                		iconName = docIcon;
		                		break;

		               			case 'psd':
		                		iconName = psdIcon;
		                		break;

		               			case 'ppt':
		                		iconName = pptIcon;
		                		break;

		               			case 'zip':
		                		iconName = zipIcon;
		                		break;

		                		default:
		                		break;
		                	}

		                	filePath = base_url + fileIconPath + iconName;
		                }
		                else if(params.file_type == 'audio') {
		                	switch(getFileExtension(v.name)) {
		                		case 'mp3':
		                		iconName = mp3Icon;
		                		break;

		                		default:
		                		break;
		                	}

		                	filePath = base_url + fileIconPath + iconName;
		                }
		                else if(params.file_type == 'video') {
		                	switch(getFileExtension(v.name)) {
								case 'flv':
		                		iconName = flvIcon;
		                		break;

		                		case 'wmv':
		                		iconName = wmvIcon;
		                		break;

								case 'avi':
		                		iconName = aviIcon;
		                		break;

		                		case 'mov':
		                		iconName = movIcon;
		                		break;

		                		default:
		                		break;
		                	}

		                	filePath = base_url + fileIconPath + iconName;
		                }
		                else if(params.file_type == 'image') {
		                	filePath = (params.path == 'files/') ? base_url + 'assets/uploads/files/' + v.name : base_url + 'assets/uploads/files/' + params.path + v.name;
		            	}

		                // And optionally show the thumbnail of the file:
		                myDropzone.emit("thumbnail", mockFile, filePath);
		                // Or if the file on your server is not yet in the right
		                // size, you can let Dropzone download and resize it
		                // callback and crossOrigin are optional.
		                myDropzone.createThumbnailFromUrl(mockFile, filePath, null, null);

		                // Make sure that there is no progress bar, etc...
		                myDropzone.emit("complete", mockFile);
		            }); 

		            if(ctr > 0) {
						processing = false;
					}

					ppm_modal.modal({backdrop: "static"});           
		        }
		    };

		    function openFolder (_this) {

		        if(params.path != $(_this).data('path')) {
		            pathSelected = $(_this);

		            params.path = $(_this).data('path');
		            dir_breadcrumbs.text(params.path.replace(/\//g, ' / '));

		            formPath.val(params.path);

		            if(Dropzone.options.ppmDropzone!=null) Dropzone.options.ppmDropzone.clearGallery();

		            var request = $.ajax({
		                url: base_url + "admin/gallery/get_files",
		                method: "POST",
		                data: params,
		                dataType: 'json'
		            });

		            request.done(function( res ) {
		                if(res!=null) {
		                    if(res.length>0) {
		                    	//console.log(res);
		                        file_info = res;
		                        if(Dropzone.options.ppmDropzone!=null) Dropzone.options.ppmDropzone.populateGallery();
		                    }
		                }
		                
		            });

		            request.error(function(_data){
	        			if(_data.readyState == 4 && _data.status == 200) {//Session expired and redirected
	        				swal({
							    title: "Session Expired!",
							    text: "Reloading page.",
							    icon: "warning",
							    dangerMode: false,
							})
							.then(willDelete => {
							    if (willDelete) {
							    	window.location.href = '<?php echo base_url();?>admin/login';
							    }
							});
	        			}
	        		});
		        }
		    }

		    function assignPath() {
		        $.each($('#directory label'), function(k,label){
		            var path = '';
		            var folderArr = [];
		            
		            if($(label).text() != 'files/') {
		                $.each($(this).parents('li'), function(k1,v1){
		                    var folder = $(v1).children('label').text();

		                    folderArr.push((folder != 'files/') ? folder : '');
		                });
		                
		                while(folderArr.length>0) {
		                    path += folderArr.pop();
		                }

		                $(label).attr({'data-path':path});
		            }
		            
		        });
		    }

		    function openTree(){
		        directory.find('[data-path="'+params.path+'"]').parents('li.branch').find('i:first').trigger('click');
		    }

			function getFileExtension(filename) {
				return (/[.]/.exec(filename)) ? /[^.]+$/.exec(filename)[0] : undefined;
			}

		    directory.treed({openedClass:'glyphicon-folder-open', closedClass:'glyphicon-folder-close'});

		    $('#directory li label').on('click', function(e){
		        e.preventDefault();
		        e.stopPropagation();

		        openFolder($(this));
		    });	

		    createFolderBtn.on('click', function(){
		        var folder = createFolder.val();
		        var request = $.ajax({
		            url: base_url + "admin/gallery/new_folder",
		            method: "POST",
		            data: {path:params.path, folder: folder},
		            dataType: 'json'
		        });

		        request.done(function( data ) {
		            if(data!=null) {

		                if(Object.keys(data).length>0) {
		                    if(data.result == 'success') {
		                        //console.log(pathSelected.siblings('ul'));
		                        createFolder.val('');

		                        $('#directory').html(data.dir).treed({openedClass:'glyphicon-folder-open', closedClass:'glyphicon-folder-close'});

		                        $('#directory li label').on('click', function(e){
		                            e.preventDefault();
		                            e.stopPropagation();
		                            openFolder($(this));
		                        });

		                        
		                        params.path = (params.path=='files/') ? folder + '/' : params.path + folder + '/';
		                        dir_breadcrumbs.text(params.path.replace(/\//g, ' / '));

		                        formPath.val(params.path);
		                        
		                        assignPath();

		                        if(Dropzone.options.ppmDropzone!=null) Dropzone.options.ppmDropzone.clearGallery();

		                        openTree();
		                    }
		                }
		            }
		            
		        });

        		request.error(function(_data){
        			if(_data.readyState == 4 && _data.status == 200) {//Session expired and redirected
        				swal({
						    title: "Session Expired!",
						    text: "Reloading page.",
						    icon: "warning",
						    dangerMode: false,
						})
						.then(willDelete => {
						    if (willDelete) {
						    	window.location.href = '<?php echo base_url();?>admin/login';
						    }
						});
        			}
        		});
		    });		    	    

			if($('#directory li ul li').hasClass('branch')) $('#directory li i:first').trigger('click');
			directory.removeClass('hide');
			
			assignPath();

		    $('#show-dir-btn').on('click', function(){
		        $(this).toggleClass('show');
		        $(this).blur();
		        $('.fade-in').toggleClass('show');
		    });

		    $('.ppm-browse').on('click', function(){
		    	initButton($(this));
		    });

		    PPMGallery.initButton = function(id) {
		    	$('#'+id).on('click', function(){
		    		initButton($(this));
			    });
		    };

		    function initButton(btn) {
		    	params.file_type = $(btn).data('type');

		    	switch(params.file_type) {
		    		case 'image':
		    		galleryTitle.text('Image Gallery');
		    		break;

		    		case 'application':
		    		galleryTitle.text('Document Gallery');
		    		break;

		    		case 'audio':
		    		galleryTitle.text('Audio Gallery');
		    		break;

		    		case 'video':
		    		galleryTitle.text('Video Gallery');
		    		break;

		    		default:
		    		break;
		    	}

		    	inputSelected = $(btn).parents('span').siblings('input:first');
		    	//console.log(inputSelected);
		    	$("#ppm-modal-btn").click();
		    }

		    <?php if(isset($is_homepage) && $is_homepage) { ?>
		    $('#show-dir-btn').hide().trigger('click');
			<?php } ?>

		});
	}
</script>

<!-- /page content -->