<?php

  $access = json_decode($_SESSION['admin_access']);

  $user_access = array();

  if (json_last_error() === JSON_ERROR_NONE) {
    foreach ($access->data as $key => $value) {
      $user_access[] = $value;
    }
  }

?>
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo base_url(); ?>admin/home" class="site_title"><i class="fa fa-cogs"></i> <span><?php echo APP_TITLE; ?></span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo base_url(); ?>assets/images/user.png" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2><?php if(isset($_SESSION['admin_username'])) echo $_SESSION['admin_username']; ?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3><?php if(isset($_SESSION['admin_user_type'])) echo $_SESSION['admin_user_type']; ?></h3>
                <ul class="nav side-menu">

                  <li><a href="<?php echo base_url(); ?>admin/home"><i class="fa fa-dashboard"></i> Dashboard <span class="fa fa-chevron-right"></a>
                  </li>

                <?php if(in_array(21, $user_access)) { ?>
                  <li><a href="<?php echo base_url(); ?>admin/homepage"><i class="fa fa-home"></i> Home Page <span class="fa fa-chevron-right"></a>
                  </li>                  
                <?php } ?>  
                
                <?php if(in_array(1, $user_access)) { ?>
                  <li><a><i class="fa fa-users"></i> Clients | Members <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>admin/members/add">Add Client | Member</a></li>
                      <li><a href="<?php echo base_url(); ?>admin/members/list">List Clients | Members</a></li>
                    </ul>
                  </li>
                <?php } ?>  

                  <?php if(false) { ?><li><a href="javascript:;"><i class="fa fa-check-square"></i> Tasks</a></li> <?php } ?>
                <?php if(in_array(2, $user_access)) { ?>
                  <li><a><i class="fa fa-calendar"></i> Events <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>admin/events/add">Add Event</a></li> 
                      <li><a href="<?php echo base_url(); ?>admin/events/list">List Events</a></li>
                    </ul>
                  </li>
                <?php } ?>  

                <?php if(in_array(3, $user_access)) { ?>
                  <li><a><i class="fa fa-cloud"></i> Online Training <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>admin/onlinetrainings/add">Add Training</a></li> 
                      <li><a href="<?php echo base_url(); ?>admin/onlinetrainings/list">List Training</a></li>
                    </ul>
                  </li>
                <?php } ?>

                <?php if(in_array(4, $user_access)) { ?>  
                  <li><a><i class="fa fa-newspaper-o"></i> Contents <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>admin/contents/news-articles">News & Article</a></li>
                      <li><a href="<?php echo base_url(); ?>admin/contents/rent-roll">Rent Roll</a></li>
                      <li><a href="<?php echo base_url(); ?>admin/contents/ppm-tv">PPM TV</a></li>
                      <li><a href="<?php echo base_url(); ?>admin/contents/promotions">Promotions</a></li>
                      <li><a href="<?php echo base_url(); ?>admin/contents/others">Others</a></li>
                    </ul>
                  </li>
                <?php } ?>

                <?php if(in_array(5, $user_access)) { ?>
                  <li><a><i class="fa fa-shopping-cart"></i> Products <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="javascript:;">Add Product</a></li>
                      <li><a href="<?php echo base_url(); ?>admin/products">List Products</a></li> 
                    </ul>
                  </li>
                <?php } ?>

                <?php if(in_array(6, $user_access)) { ?>
                  <li><a><i class="fa fa-list-alt"></i> Menu Builder <span class="fa fa-chevron-down"></span></a> 
                    <ul class="nav child_menu"> 
                      <li><a href="<?php echo base_url(); ?>admin/menubuilder">Add Menu</a></li> 
                      <li><a href="<?php echo base_url(); ?>admin/menus">List Menus</a></li> 
                    </ul> 
                  </li> 
                <?php } ?>

                <?php if(in_array(7, $user_access)) { ?>
                  <li><a><i class="fa fa-pagelines"></i> Page Builder <span class="fa fa-chevron-down"></span></a> 
                    <ul class="nav child_menu"> 
                      <li><a href="<?php echo base_url(); ?>admin/pages/add">Add Page</a></li> 
                      <li><a href="<?php echo base_url(); ?>admin/pages">List Pages</a></li> 
                    </ul> 
                  </li> 
                <?php } ?>

                <?php if(in_array(8, $user_access)) { ?>
                  <li><a><i class="fa fa-image"></i> Banners <span class="fa fa-chevron-down"></span></a> 
                    <ul class="nav child_menu"> 
                      <li><a href="<?php echo base_url(); ?>admin/banners/add">Add Banner</a></li> 
                      <li><a href="<?php echo base_url(); ?>admin/banners">List Banners</a></li> 
                    </ul> 
                  </li> 
                <?php } ?>

                <?php if(in_array(22, $user_access)) { ?>
                  <li><a href="<?php echo base_url(); ?>admin/partners"><i class="fa fa-users"></i> Partners <span class="fa fa-chevron-right"></a>
                  </li>                  
                <?php } ?>               

                <?php if(in_array(9, $user_access)) { ?>
                  <li><a><i class="fa fa-link"></i> Links | Discounts <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>admin/links/national-links">National Links</a></li>
                      <li><a href="<?php echo base_url(); ?>admin/links/industry-service-providers">Industry Service Providers</a></li>
                      <li><a href="<?php echo base_url(); ?>admin/links/system-member-offices">System Member Offices</a></li>
                    </ul>
                  </li>
                <?php } ?>

                <?php if(isset($_SESSION['admin_status']) && $_SESSION['admin_status']) { ?>
                  <li><a><i class="fa fa-user"></i> Users <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>admin/users/add">Add User</a></li>
                      <li><a href="<?php echo base_url(); ?>admin/users/list">List Users</a></li>
                    </ul>
                  </li>
                <?php } ?>

                <?php if(in_array(10, $user_access)) { ?>
                  <li><a><i class="fa fa-comments-o"></i> Testimonials | Staff <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>admin/testimonials/add">Add Testimonial | Staff</a></li>
                      <li><a href="<?php echo base_url(); ?>admin/testimonials">List Testimonials | Staff</a></li>
                    </ul>
                  </li>
                <?php } ?>

                <?php if(in_array(11, $user_access)) { ?>
                  <li><a><i class="fa fa-newspaper-o"></i> eNews | Newsletter <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>admin/news/add">Add eNews | Newsletter</a></li>
                      <li><a href="<?php echo base_url(); ?>admin/news/list">List eNews | Newsletter</a></li>
                    </ul>
                  </li>
                <?php } ?>

                <?php if(in_array(12, $user_access)) { ?>
                   <li><a><i class="fa fa-trophy"></i> Awards <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>admin/awards/add">Add Awards</a></li>
                      <li><a href="<?php echo base_url(); ?>admin/awards/list">List Awards</a></li>
                    </ul>
                  </li>                                  
                <?php } ?>

                <?php if(in_array(13, $user_access)) { ?>                
                  <li><a><i class="fa fa-image"></i> Gallery <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>admin/gallery/add">Add Gallery</a></li>
                      <li><a href="<?php echo base_url(); ?>admin/gallery/list">List Gallery</a></li>
                    </ul>
                  </li>                  
                <?php } ?>

                <?php if(in_array(15, $user_access)) { ?>                
                  <li><a><i class="fa fa-download"></i> Download | Upgrade <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>admin/downloads/add">Add Download | System Upgrade</a></li>
                      <li><a href="<?php echo base_url(); ?>admin/downloads/list">List Download | System Upgrade</a></li>
                    </ul>
                  </li>                  
                <?php } ?>

                <?php if(in_array(16, $user_access)) { ?>                
                  <li><a><i class="fa fa-tags"></i> Promo Codes <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>admin/promocodes/add">Add Promo Codes</a></li>
                      <li><a href="<?php echo base_url(); ?>admin/promocodes/list">List Promo Codes</a></li>
                    </ul>
                  </li>                  
                <?php } ?>
                
                <?php if(in_array(17, $user_access) && FALSE) { ?> 
                  <li><a><i class="fa fa-check-square-o"></i> Polls <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="javascript:;">Add Polls</a></li>
                      <li><a href="javascript:;">List Polls</a></li>
                    </ul>
                  </li>
                <?php } ?>

                <?php if(in_array(18, $user_access)) { ?>
                  <li><a href="<?php echo base_url(); ?>admin/monthlyawardstats"><i class="fa fa-bar-chart"></i> Monthly Award Stats <span class="fa fa-chevron-right"></a></li>                
                <?php } ?>

                <?php if(in_array(19, $user_access)) { ?>                
                  <li><a href="<?php echo base_url(); ?>admin/reporting"><i class="fa fa-line-chart"></i> Reporting <span class="fa fa-chevron-right"></a></li>                
                <?php } ?> 

                <?php if(in_array(20, $user_access) && FALSE) { ?>                
                  <li><a href="javascript:;"><i class="fa fa-database"></i> Export DB <span class="fa fa-chevron-right"></a></li>                
                <?php } ?>

                <?php if(FALSE) { ?>   
                  <li><a><i class="fa fa-credit-card"></i> Orders Payments <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="javascript:;">List Payments</a></li>
                      <li><a href="javascript:;">List Summary</a></li>
                    </ul>
                  </li>
                <?php } ?>  

                </ul>
              </div>

            </div>
            <!-- /sidebar menu -->
          </div>
        </div>