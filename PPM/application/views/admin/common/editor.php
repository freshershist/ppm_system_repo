<!-- editor -->

<?php if(isset($editor_id)) { ?>

<div class="<?php echo $editor_id; ?> error-space"></div>

<textarea name="<?php echo $editor_id; ?>" id="<?php echo $editor_id; ?>" rows="10" cols="80"><?php if(isset($text)) { echo $text; } ?></textarea>
<script>
	$(document).ready(function(){
		CKEDITOR.replace( '<?php echo $editor_id; ?>', { allowedContent : true } ); 
	});
</script>

<?php } ?>

<!-- editor -->