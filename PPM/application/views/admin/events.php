<!-- page content -->
<div class="right_col" role="main" style="min-height: 100vh;">

  <div class="row">
    <div class="col-md-12">

      <h1><i class="fa fa-pagelines"></i> Events</h1>
      <button class="btn btn-primary" type="button" id="btnAddNew" > <i class="fa fa-plus"></i> Add New </button>
      <hr>
      <div class="row" >
        <div class="col-md-12">


          <!-- START : LIST TABLE -->
          <table class="table table-striped" id="tblProductList">
            <thead>
              <tr>
                <!-- <td><input type="checkbox" id="chkAllItems" class="flat"></td> -->
                <td>Event Name</td>
                <td>Short Desc</td>
                <td>Start Date</td>
                <!-- <td>End Date</td> -->
                <td>Time</td>
                <td>Action</td>
              </tr>
            </thead>
            <tbody>
                <?php
                  $e_list = get_list_events();
                  // print_r($e_list);
                  foreach($e_list as $list){
                        echo '<tr>
                                <td style="width: 30%">'.$list['name'].'</td>
                                <td style="width: 30%">'.$list['shortdesc'].'</td>
                                <td style="width: 10%">'.date('d/m/Y', strtotime($list['startDate'])).'</td>
                                <!--<td>'.date('d/m/Y', strtotime($list['endDate'])).'</td>-->
                                <td style="width: 10%">'.$list['myTime'].'</td>
                                <td style="width: 20%">
                                  <button type="button" class="btn btn-primary btnEditPage" data-pid="'.$list['id'].'"><i class="fa fa-pencil"></i></button>
                                  <button type="button" class="btn btn-danger btnDeletePage" data-pid="'.$list['id'].'"><i class="fa fa-trash"></i></button>
                                </td>
                              </tr>
                        ';
                  }

                ?>
            </tbody>
          </table>
          <!-- START : LIST TABLE -->

        </div>


<!--         <div class="x_panel">
          <div class="x_title">
            <h2><i class="fa fa-pagelines"></i> Events</h2>
            <button class="btn btn-primary" type="button" id="btnAddNew" style="margin-left: 20px;"> <i class="fa fa-plus"></i> Add New </button>
            <ul class="nav navbar-right panel_toolbox" style="min-width:auto!important;">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
          

          </div>
        </div> -->


      </div>

    </div>

  </div>

	<div class="clearfix"></div>
</div>

<div id="member-logs" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

			<div class="modal-header">
				<h4 class="modal-title">Title</h4>
			</div>
			<div class="modal-body">

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>

		</div>
	</div>
</div>

<!-- /page content -->

<style>
.spacer10 {
  clear: both;
  width: 100%;
  height: 10px;
}
.spacer20 {
  clear: both;
  width: 100%;
  height: 20px;
}
.spacer30 {
  clear: both;
  width: 100%;
  height: 30px;
}

#tblProductList thead td {
  font-weight: bold;
}

</style>

<script>
  var tblProdData = "";
  $(document).ready(function(){
    var baseurl = '<?php echo base_url(); ?>';

    $('#chkAllItems').iCheck({
      checkboxClass: 'icheckbox_square',
      radioClass: 'iradio_square',
      increaseArea: '20%' // optional
    });

    // setTimeout(function(){
      tblProdData = $('#tblProductList').DataTable({
        "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        "columnDefs": [{ "orderable": false, "targets": [0, 4] }],
        "order": [[ 0, "asc" ]],
        "scrollY": "400px",
        "scrollX": true,
        "scrollCollapse": true,
        "paging": true
      });
    // },1500);

    //EDIT
    $('#tblProductList tbody').on('click', ' td > .btnEditPage', function(){
      // alert('Test' + $(this).data('pid'));
      window.location = baseurl + 'admin/events/edit/' + $(this).data('pid') + '/';
    });

    //DELETE
    $('#tblProductList tbody').on('click', ' td > .btnDeletePage', function(){
        var page_id = $(this).data('pid');
        // var q = confirm('Are you sure you want to delete the page?');
        var $this = $(this);
        swal({
          title: "Are you sure?",
          text: "Once deleted, you will not be able to recover this data!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            // swal("Poof! Your imaginary file has been deleted!", {
            //   icon: "success",
            // });
            $.ajax({
              type : 'post',
              url  : baseurl + 'admin/events/delete',
              data : {
                'page_id' : page_id
              },
              beforeSend : function(){

              },
              success : function(result){
                tblProdData.row( $this.parents('tr') ).remove().draw();
                swal("Poof! Page has been Deleted!", {
                  icon: "success",
                });
              }
            });

          } else {
            // swal("Your imaginary file is safe!");
          }
        });

    });

    $('#btnAddNew').click(function(){
      // $.ajax({
      //   type: 'post',
      //   url : baseurl + 'admin/pages/test',
      //   success:function(res){
      //     console.log(res);
      //   }
      // });
      window.location = baseurl + 'admin/events/add';
    });

  });
</script>
