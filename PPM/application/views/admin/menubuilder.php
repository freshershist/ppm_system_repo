<!-- page content -->
<div class="right_col" role="main" style="min-height: 100vh;">

  <div class="row">
    <div class="col-md-12">
      <?php if(empty($menu_id) || $menu_id == 0){ ?>
      <h1><i class="fa fa-list-alt"></i> New Menu</h1>
      <?php }else{ ?>
      <h1><i class="fa fa-list-alt"></i> Edit Menu</h1>
      <?php } ?>
      <div class="row">
        <div class="col-md-12" id="dispMessages"></div>
      </div>
      <div class="spacer20"></div>
      <div class="row">
          <div class="col-md-12">
              <a href="<?php echo base_url().'admin/menus'; ?>" class="btn btn-primary">
                  <i class="fa fa-arrow-left"></i> Back
              </a>
          </div>
      </div>

      <hr/>
      <div class="spacer20"></div>
      <div class="row <?php echo (isset($menu_id))? 'hide-me' : ''; ?> ">
        <div class="col-md-6">
            <div class="form-group">
                <label>Menu Name</label>
                <input type="text" id="txtMenuName" name="txtMenuName" class="form-control">
                <input type="hidden" id="txtMenuID" name="txtMenuID" class="form-control" value=<?php echo (isset($menu_id))? $menu_id : '';  ?>>
            </div>
            <div class="spacer20"></div>
            <button type="button" class="btn btn-primary" id="btnNewMenuName"> <i class="fa fa-save"></i> Create Menu </button>
        </div>
      </div>
      <div class="row" id="divMenuBuilder">
        <div class="col-md-12">

          <!-- START : FORM -->


          <?php
            // if(isset($is_parent_page)){
            //   echo '<pre>';
            //   print_r($is_parent_page);
            //   echo '</pre>';              
            // }
            if(isset($menu_id)):
            echo '<pre class="hide-me">';
            print_r($menu_details['result'][0]['menu_list']);
            echo '</pre>';
            echo '<pre class="hide-me">';
            print_r($menu_details['result'][0]['menu_data']);
            print_r(json_decode($menu_details['result'][0]['menu_data']));
            echo '</pre>';
            endif;
            // $s = $menu_details['result'][0]['menu_list'];
            // // $s = '[{"id":13,"children":[{"id":14}]},{"id":15,"children":[{"id":16},{"id":17},{"id":18}]}]';
            // // $s = '[{"id":13},{"id":14},{"id":15,"children":[{"id":16},{"id":17},{"id":18}]}]';
            // $var = json_decode($s);
            // foreach($var as $r){
            //   echo $r->id.'<br/>';
            //   if(isset($r->children)){
            //     $c = count($r->children);
            //     if($c > 0){
            //       foreach ($r->children as $r2) {
            //         echo '-----'.$r2->id.'<br/>';
            //         //parsed
            //       }
            //     }                
            //   }


            // }
            // echo '<pre>';
            // print_r($var);
            // echo '</pre>';
          ?>

          <!-- START : MENU BUILDER -->
          <div class="row hide-me1" >
            <hr>
            <div class="col-md-6">
              <h3>Menu Builder</h3>
              <small><em>(Note: Auto-saves every change[s] you made)</em></small>
              <div class="spacer10"></div>
              <button type="button" class="btn btn-primary" id="btnAddMenuItem"><i class="fa fa-plus"></i> Item</button>
              <div class="clearfix spacer20"></div>
                <!-- sTART : MENU DRAGGABLES -->
                <div class="dd" id="nestable3">
                    <ol class="dd-list" id="d-list-container">
                        <?php
                        // if(isset($menu_id)){
                        //   $s = $menu_details['result'][0]['menu_list'];
                        //   $var = json_decode($s);
                        //   foreach($var as $r){
                        //     // echo $r->id.'<br/>';
                        //     echo '
                        //       <li class="dd-item dd3-item" data-id="'. $r->id.'">
                        //           <div class="dd-handle dd3-handle">Drag</div><div class="dd3-content">Item '. $r->id.'</div>
                        //     ';
                        //     if(isset($r->children)){
                        //       $c = count($r->children);
                        //       if($c > 0){
                        //         echo '<ol class="dd-list">';
                        //         foreach ($r->children as $r2) {
                        //           // echo '-----'.$r2->id.'<br/>';
                        //           echo '
                        //             <li class="dd-item dd3-item" data-id="'.$r2->id.'">
                        //                 <div class="dd-handle dd3-handle">Drag</div><div class="dd3-content">Item '.$r2->id.'</div>
                        //             </li>
                        //           ';
                        //           //parsed
                        //         }
                        //         echo '</ol>';
                        //       }                
                        //     }else{
                        //       echo '</li>';
                        //     }


                        //   }
                        // }
                        ?>
                        <!-- 
                        <li class="dd-item dd3-item" data-id="13">
                            <div class="dd-handle dd3-handle">Drag</div><div class="dd3-content">Item 13</div>
                        </li>
                        <li class="dd-item dd3-item" data-id="14">
                            <div class="dd-handle dd3-handle">Drag</div><div class="dd3-content">Item 14</div>
                        </li>
                        <li class="dd-item dd3-item" data-id="15">
                            <div class="dd-handle dd3-handle">Drag</div><div class="dd3-content">Item 15</div>
                            <ol class="dd-list">
                                <li class="dd-item dd3-item" data-id="16">
                                    <div class="dd-handle dd3-handle">Drag</div><div class="dd3-content">Item 16</div>
                                </li>
                                <li class="dd-item dd3-item" data-id="17">
                                    <div class="dd-handle dd3-handle">Drag</div><div class="dd3-content">Item 17</div>
                                </li>
                                <li class="dd-item dd3-item" data-id="18">
                                    <div class="dd-handle dd3-handle">Drag</div><div class="dd3-content">Item 18</div>
                                </li>
                            </ol>
                        </li>
                         -->
                    </ol>
                </div>
                <div class="clearfix spacer20"></div>
                <!-- END  : MENU DRAGGABLES -->
            </div>
            <div class="col-md-6">
              <h3>Menu Item Info</h3>
              <textarea id="nestable3-output" class="form-control hide-me"></textarea>
              <div class="form-group">
                  <label>Label</label>
                  <input type="text" id="txtItemLabel" class="form-control">
                  <input type="hidden" id="txtItemID" class="form-control">
                  
              </div>

              <div class="form-group">
                  <label>Link</label>
                  <input type="text" id="txtItemLink" class="form-control">
              </div>

              <button type="button" class="btn btn-primary" id="btnSaveLabel"><i class="fa fa-save"></i> Save Item</button>
            </div>
          </div>
          <!-- END  : MENU BUILDER -->


          <!-- END : FORM -->
        </div>

        <div class="col-md-12">
          <button type="button" class="btn btn-primary btn-lg" id="btnSaveMenuData"><i class="fa fa-save"></i> SAVE MENU</button>
        </div>
      </div>


    </div>

  </div>

  <div class="clearfix"></div>
</div>

<div id="member-logs" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title">Title</h4>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>

<!-- /page content -->

<style>
.spacer10 {
  clear: both;
  width: 100%;
  height: 10px;
}
.spacer15 {
  clear: both;
  width: 100%;
  height: 15px;
}
.spacer20 {
  clear: both;
  width: 100%;
  height: 20px;
}
.spacer25 {
  clear: both;
  width: 100%;
  height: 25px;
}
.spacer30 {
  clear: both;
  width: 100%;
  height: 30px;
}

.hide-me {
  display: none;
}

</style>
<script src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
<script>
  function saveMenuName(callback){
    var menuName = $('#txtMenuName').val();
    var menuID = $('#txtMenuID').val();
    if(menuName != ''){
      //ajax
      $.ajax({
        type : 'post',
        url  : '<?php echo base_url(); ?>admin/menubuilder/saveMenu',
        data : {
          'menuName' : menuName,
          'menuID'   : menuID
        },
        beforeSend : function(){

        },
        success : function(res){
            console.log(res);
            if(res == 'exists'){
                swal({
                    title: "Error",
                    text: "Menu Name already exists!",
                    icon: "error",
                    // buttons: ["Cancel", true],
                    dangerMode: true
                });
            }else{
                $('#txtMenuID').val(res);
                if(callback){
                  callback();
                }              
            }
        }
      });

    }else{
        swal({
            title: "Error",
            text: "Please enter Menu Name!",
            icon: "error",
            // buttons: ["Cancel", true],
            dangerMode: true
        });
    }


  }

  function searchFromObject(nameKey, myArray){
    for (var i=0; i < myArray.length; i++) {
        if (myArray[i].name === nameKey) {
            return myArray[i];
        }
    }
  }

  // function propagateDetails(this_){
  //     var label_ = this_.find('.dd3-content').html();
  //     var labelID_ = this_.data('id');
  //     console.log(labelID_);
  //     // $('#txtItemLabel').val(label_);
  //     // $('#txtItemID').val(labelID_)
  //     var so = list_items.find(o => o.label_id === "" + labelID_);

  //     console.log(so);
  //     $('#txtItemID').val(so.label_id);
  //     $('#txtItemLabel').val(so.label_name);
  //     $('#txtItemLink').val(so.label_link);
  // }

  
  <?php if(isset($menu_id)){ ?>
  var item_ctr_ = <?php echo count(json_decode($menu_details['result'][0]['menu_data'])); ?>;
  var list_items = <?php echo $menu_details['result'][0]['menu_data']; ?>;
  <?php }else{ ?>
  var item_ctr_ = 0;
  var list_items = Array();
  <?php } ?>
  
  $(document).ready(function(){
    <?php if(empty($menu_id) || $menu_id == 0){ ?>
    $('#divMenuBuilder').hide();
    <?php }else{ ?>
    $('#divMenuBuilder').show();
    <?php } ?>

    $('#btnNewMenuName').click(function(){
      saveMenuName(function(){
        $('#divMenuBuilder').show();
      });
      
    });


    $('#btnAddMenuItem').click(function(){
      item_ctr_++;
      // var list_items = $('#nestable3-output').val();
      // onclick="propagateDetails($(this));"
      var item_ = '\
                  <li class="dd-item dd3-item" data-id="'+ item_ctr_ +'">\
                      <div class="dd-handle dd3-handle">Drag</div><div class="dd3-content">Item '+ item_ctr_ +'</div>\
                  </li>\
                ';

      $('#d-list-container').append(item_);

      updateOutput($('#nestable3').data('output', $('#nestable3-output')))
      list_items.push( {"label_id" : "" + item_ctr_, "label_name" : "Item " + item_ctr_, "label_link" : "" } );
      console.log(list_items);
      // var so = searchFromObject('Item 1', list_items);
    });

    $('.dd-list').on('click', '.dd-item', function(e){
      e.stopPropagation();
      console.log($(this).data('id'));
      var label_ = $(this).find('.dd3-content').html();
      var labelID_ = $(this).data('id');
      console.log(labelID_);
      var so = list_items.find(o => o.label_id === "" + labelID_);
      console.log(so);
      $('#txtItemID').val(so.label_id);
      $('#txtItemLabel').val(so.label_name);
      $('#txtItemLink').val(so.label_link);
    });

    $('#btnSaveLabel').click(function(){
      var lblName = $('#txtItemLabel').val();
      var lblID = $('#txtItemID').val();
      // console.log(list_items.find(o => o.label_name === 'Item 1'));
      var so = list_items.find(o => o.label_id === ""+lblID)
      // so = JSON.parse(so);
      console.log(so);
      var lblLink = $('#txtItemLink').val();
      so.label_name = lblName;
      so.label_link = lblLink;
      if(lblLink != ''){
        lblLink = "<small style=\"font-weight: normal;\"> - <em>"+ lblLink +"</em></small>";
      }
      // console.log(so.label_name);
      $('.dd-list [data-id="'+ lblID +'"] > .dd3-content').html(lblName + lblLink);
      console.log(list_items);
    });


    $('#btnSaveMenuData').click(function(){
      var nest_list = $('#nestable3-output').val();
      var nest_data = list_items;
      var menuID = $('#txtMenuID').val();

      var len = list_items.length;
      if(len > 0){
        //ajax save here\
        $.ajax({
          type : 'post',
          url  : '<?php echo base_url(); ?>admin/menubuilder/saveMenuItem',
          data : {
            "nest_list" : nest_list,
            "nest_data" : nest_data,
            "menuID" : menuID
          },
          before: function(){

          },
          success: function(res){
            swal({
                title: "Success",
                text: "Menu data saved!",
                icon: "success",
                // buttons: ["Cancel", true],
                dangerMode: false
            });
          }
        })
      }else{
        swal({
            title: "Error",
            text: "No Items for Menu yet!",
            icon: "error",
            // buttons: ["Cancel", true],
            dangerMode: true
        });
      }
    });

    /*=============================================================================================================================*/


    $('#chkAllItems').iCheck({
      checkboxClass: 'icheckbox_square',
      radioClass: 'iradio_square',
      increaseArea: '20%' // optional
    });

    $('#btnSavePage').click(function(){
      // alert('btnSavePage');
      // saveMenu();
      var txtPageTitle = $('#txtPageTitle').val();
      var editor_1 = CKEDITOR.instances.editor_1.getData();
      var pageStatus = $('#pageStatus').val();
      var txtOrder = $('#txtOrder').val();
      var page_id = $('#pageID').val();
      var selParentPage = $('#selParentPage').val();

      $.ajax({
        type : 'post',
        url  : '<?php echo base_url(); ?>admin/pages/savePage',
        data : {
          'txtPageTitle' : txtPageTitle,
          'editor_1'     : editor_1,
          'pageStatus'   : pageStatus,
          'txtOrder'     : txtOrder,
          'page_id'      : page_id,
          'selParentPage': selParentPage
        },
        beforeSend : function(){
          $('#btnSavePage').html(' <i class="fa fa-spinner fa-pulse"></i> Saving... ');
        },
        success : function(result){
          console.log(result);
          $('#btnSavePage').html(' <i class="fa fa-save"></i> Save Page ');
        }
      });
    });
  });
</script>

<script src="<?php echo base_url(); ?>assets/js/nestable/jquery.nestable.js"></script>
<!--<script src="//cdnjs.cloudflare.com/ajax/libs/nestable2/1.6.0/jquery.nestable.min.js"></script>-->
<?php if(empty($page_id) || $page_id == 0){ ?>
<?php }else{ ?>
<?php } ?>
<script>
// $(function  () {
//   $("ol.example").sortable();
// });

// var oldContainer;
// $("ol.example").sortable({
//   group: 'nested',
//   afterMove: function (placeholder, container) {
//     if(oldContainer != container){
//       if(oldContainer)
//         oldContainer.el.removeClass("active");
//       container.el.addClass("active");

//       oldContainer = container;
//     }
//   },
//   onDrop: function ($item, container, _super) {
//     container.el.removeClass("active");
//     _super($item, container);
//   }
// });

  var saveMenu = function(){
    $.ajax({
      type : 'post',
      url  : '<?php echo base_url(); ?>admin/pages/saveMenu',
      data : {
        axn : 'save',
      },
      success : function(result){
        // alert(result);
        $('#dispMessages').html(result)
      }
    })
  };


  var updateOutput = function(e)
  {
      var list   = e.length ? e : $(e.target),
          output = list.data('output');
      if (window.JSON) {
          output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
      } else {
          output.val('JSON browser support required for this demo.');
      }
  };

  $(document).ready(function(){
    $('#nestable3').nestable().on('change', updateOutput);

    updateOutput($('#nestable3').data('output', $('#nestable3-output')));


    // setTimeout(function(){
    //     // var json = $('#nestable3-output').val('[{"id":13,"children":[{"id":14}]},{"id":15,"children":[{"id":16},{"id":17},{"id":18}]}]');
    //     var json = '[{"id":13,"children":[{"id":14}]},{"id":15,"children":[{"id":16},{"id":17},{"id":18}]}]';
    //     var options = {'json': json };
    //     $('#nestable-json').nestable(options);
    // }, 10000);

  });


</script>
<?php if(isset($menu_id)): ?>
<script>
$(document).ready(function(){
    // var obj = '[{"id":1},{"id":2},{"id":3,"children":[{"id":4},{"id":5}]}]';
    var obj = '<?php echo $menu_details['result'][0]['menu_list']; ?>';
    var output = '';
    function buildItem(item) {
        var so = list_items.find(o => o.label_id === ""+ item.id);
        // so.label_name = lblName;
        // so.label_link = lblLink;
        var lblLink = so.label_link;
        if(lblLink != ''){
          lblLink = "<small style=\"font-weight: normal;\"> - <em>"+ lblLink +"</em></small>";
        }   

        var html = "<li class='dd-item' data-id='" + item.id + "'><div class=\"dd-handle dd3-handle\">Drag</div>";
        // html += "<div class='dd3-content'>Item " + item.id + "</div>";
        html += "<div class='dd3-content'>" + so.label_name + "" + lblLink +"</div>";

        if (item.children) {

            html += "<ol class='dd-list'>";
            $.each(item.children, function (index, sub) {
                html += buildItem(sub);
            });
            html += "</ol>";

        }

        html += "</li>";

        return html;
    }

    $.each(JSON.parse(obj), function (index, item) {

        output += buildItem(item);

    });

    $('#d-list-container').html(output);
    $('#nestable3').nestable();
    updateOutput($('#nestable3').data('output', $('#nestable3-output')));
});
</script>
<?php endif; ?>
<style>
/*.cf:after { visibility: hidden; display: block; font-size: 0; content: " "; clear: both; height: 0; }
* html .cf { zoom: 1; }
*:first-child+html .cf { zoom: 1; }

html { margin: 0; padding: 0; }
body { font-size: 100%; margin: 0; padding: 1.75em; font-family: 'Helvetica Neue', Arial, sans-serif; }

h1 { font-size: 1.75em; margin: 0 0 0.6em 0; }

a { color: #2996cc; }
a:hover { text-decoration: none; }

p { line-height: 1.5em; }
.small { color: #666; font-size: 0.875em; }
.large { font-size: 1.25em; }*/

/**
 * Nestable
 */

.dd { position: relative; display: block; margin: 0; padding: 0; max-width: 600px; list-style: none; font-size: 13px; line-height: 20px; }

.dd-list { display: block; position: relative; margin: 0; padding: 0; list-style: none; }
.dd-list .dd-list { padding-left: 30px; }
.dd-collapsed .dd-list { display: none; }

.dd-item,
.dd-empty,
.dd-placeholder { display: block; position: relative; margin: 0; padding: 0; min-height: 20px; font-size: 13px; line-height: 20px; }

.dd-handle { display: block; height: 30px; margin: 5px 0; padding: 5px 10px; color: #333; text-decoration: none; font-weight: bold; border: 1px solid #ccc;
    background: #fafafa;
    background: -webkit-linear-gradient(top, #fafafa 0%, #eee 100%);
    background:    -moz-linear-gradient(top, #fafafa 0%, #eee 100%);
    background:         linear-gradient(top, #fafafa 0%, #eee 100%);
    -webkit-border-radius: 3px;
            border-radius: 3px;
    box-sizing: border-box; -moz-box-sizing: border-box;
}
.dd-handle:hover { color: #2ea8e5; background: #fff; }

.dd-item > button { display: block; position: relative; cursor: pointer; float: left; width: 25px; height: 20px; margin: 5px 0; padding: 0; text-indent: 100%; white-space: nowrap; overflow: hidden; border: 0; background: transparent; font-size: 12px; line-height: 1; text-align: center; font-weight: bold; }
.dd-item > button:before { content: '+'; display: block; position: absolute; width: 100%; text-align: center; text-indent: 0; }
.dd-item > button[data-action="collapse"]:before { content: '-'; }

.dd-placeholder,
.dd-empty { margin: 5px 0; padding: 0; min-height: 30px; background: #f2fbff; border: 1px dashed #b6bcbf; box-sizing: border-box; -moz-box-sizing: border-box; }
.dd-empty { border: 1px dashed #bbb; min-height: 100px; background-color: #e5e5e5;
    background-image: -webkit-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
                      -webkit-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
    background-image:    -moz-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
                         -moz-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
    background-image:         linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
                              linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
    background-size: 60px 60px;
    background-position: 0 0, 30px 30px;
}

.dd-dragel { position: absolute; pointer-events: none; z-index: 9999; }
.dd-dragel > .dd-item .dd-handle { margin-top: 0; }
.dd-dragel .dd-handle {
    -webkit-box-shadow: 2px 4px 6px 0 rgba(0,0,0,.1);
            box-shadow: 2px 4px 6px 0 rgba(0,0,0,.1);
}

/**
 * Nestable Extras
 */

.nestable-lists { display: block; clear: both; padding: 30px 0; width: 100%; border: 0; border-top: 2px solid #ddd; border-bottom: 2px solid #ddd; }

#nestable-menu { padding: 0; margin: 20px 0; }

#nestable-output,
#nestable2-output { width: 100%; height: 7em; font-size: 0.75em; line-height: 1.333333em; font-family: Consolas, monospace; padding: 5px; box-sizing: border-box; -moz-box-sizing: border-box; }

#nestable2 .dd-handle {
    color: #fff;
    border: 1px solid #999;
    background: #bbb;
    background: -webkit-linear-gradient(top, #bbb 0%, #999 100%);
    background:    -moz-linear-gradient(top, #bbb 0%, #999 100%);
    background:         linear-gradient(top, #bbb 0%, #999 100%);
}
#nestable2 .dd-handle:hover { background: #bbb; }
#nestable2 .dd-item > button:before { color: #fff; }

@media only screen and (min-width: 700px) {

    .dd { float: left; width: 48%; }
    .dd + .dd { margin-left: 2%; }

}

.dd-hover > .dd-handle { background: #2ea8e5 !important; }

/**
 * Nestable Draggable Handles
 */

.dd3-content { display: block; height: 30px; margin: 5px 0; padding: 5px 10px 5px 40px; color: #333; text-decoration: none; font-weight: bold; border: 1px solid #ccc;
    background: #fafafa;
    background: -webkit-linear-gradient(top, #fafafa 0%, #eee 100%);
    background:    -moz-linear-gradient(top, #fafafa 0%, #eee 100%);
    background:         linear-gradient(top, #fafafa 0%, #eee 100%);
    -webkit-border-radius: 3px;
            border-radius: 3px;
    box-sizing: border-box; -moz-box-sizing: border-box;
}
.dd3-content:hover { color: #2ea8e5; background: #fff; cursor: pointer; }

.dd-dragel > .dd3-item > .dd3-content { margin: 0; }

.dd3-item > button { margin-left: 30px; }

.dd3-handle { position: absolute; margin: 0; left: 0; top: 0; cursor: pointer; width: 30px; text-indent: 100%; white-space: nowrap; overflow: hidden;
    border: 1px solid #aaa;
    background: #ddd;
    background: -webkit-linear-gradient(top, #ddd 0%, #bbb 100%);
    background:    -moz-linear-gradient(top, #ddd 0%, #bbb 100%);
    background:         linear-gradient(top, #ddd 0%, #bbb 100%);
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
}
.dd3-handle:before { content: '≡'; display: block; position: absolute; left: 0; top: 3px; width: 100%; text-align: center; text-indent: 0; color: #fff; font-size: 20px; font-weight: normal; }
.dd3-handle:hover { background: #ddd; cursor: move; }


</style>