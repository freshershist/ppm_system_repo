<!-- page content -->
<div class="right_col" role="main" style="min-height: 100vh;">

  <div class="row">
    <div class="col-md-12">
      <div id="divHead">

          <?php if(empty($page_id) || $page_id == 0){ ?>
          <h1><i class="fa fa-calendar"></i> New Product</h1>
          <?php }else{ ?>
          <h1><i class="fa fa-calendar"></i> Edit Product</h1>
          <?php 
            // print_r($training_info);

          ?>

          <?php } ?>
          <div class="row">
            <div class="col-md-12" id="dispMessages"></div>
          </div>
          <div class="spacer20"></div>
          <div class="row">
              <div class="col-md-12">
                  <a href="<?php echo base_url().'admin/products'; ?>" class="btn btn-primary">
                      <i class="fa fa-arrow-left"></i> Back
                  </a>
                  <button type="button" class="btn btn-primary" id="btnSave"><i class="fa fa-save"></i> Save</button>
              </div>
          </div>

      </div>
      <div class="spacer10"></div>
      <hr/>
      <!-- <div class="spacer10"></div> -->
      <!-- START FORM -->
      <form id="frmProducts">
        <div class="row">
          <div class="col-md-3">

              <div class="form-group">
                  <label>* Name : </label>
                  <input type="text" name="prodName" id="prodName" class="form-control">
              </div>

              <div class="form-group">
                  <label>Product of the Month:</label>
                  <div class="row">
                      <div class="col-md-6">
                          <select class="form-control" name="selProdMonth">
                            <option value="-">select</option>
                            <option value="01">January</option>
                            <option value="02">February</option>
                            <option value="03">March</option>
                            <option value="04">April</option>
                            <option value="05">May</option>
                            <option value="06">June</option>
                            <option value="07">July</option>
                            <option value="08">August</option>
                            <option value="09">September</option>
                            <option value="10">October</option>
                            <option value="11">November</option>
                            <option value="12">December</option>
                          </select>
                      </div>
                      <div class="col-md-6">
                          <select class="form-control" name="selProdYear">
                            <option value="-">select</option>
                            <?php
                              $yearstart = 2009;
                              $yearend = 2025;
                              for($x = $yearstart; $x <= $yearend; $x++){
                                echo '<option value="'.$x.'">'.$x.'</option>';
                              }
                            ?>
                          </select>
                      </div>
                  </div>
              </div>
              <div class="form-group">
                  <label>* Product Code : </label>
                  <input type="text" name="prodCode" id="prodCode" class="form-control">
              </div>

              <div class="form-group">
                  <label>* Category : </label>
                  <!-- <input type="text" name="prodCode" id="prodCode" class="form-control"> -->
                  <select class="form-control" name="inpCat" id="inpCat" >
                      <option value="-">-select category-</option>
                  <?php 
                    foreach ($category_list['result'] as $cat_list) {
                      # code...
                      echo '<option value="'.$cat_list['id'].'">'.$cat_list['name'].'</option>';
                    }
                  ?>
                  </select>
              </div>

              <div class="form-group">
                  <label>* Compatibility / Author : </label>
                  <input type="text" name="prodAuthor" id="prodAuthor" class="form-control">
              </div>

              <div class="form-group">
                  <label>Short Description : </label>
                  <!-- <input type="text" name="prodCode" id="prodCode" class="form-control"> -->
                  <textarea class="form-control" rows="5" id="prodShortD" name="prodShortD"></textarea>
              </div>

              <div class="form-group">
                  <label>Description : </label>
                  <!-- <input type="text" name="prodCode" id="prodCode" class="form-control"> -->
                  <textarea class="form-control" rows="5" name="prodDesc" id="prodDesc"></textarea>
              </div>

          </div>
          <div class="col-md-3">
              <div class="form-group">
                  <label>Related Events : </label>
                  <!-- <input type="text" name="prodCode" id="prodCode" class="form-control"> -->
                  <select  name="txtRelatedEvents[]" class="form-control" style="height: 102px;" multiple>
                    <?php 
                      foreach($events_list['result'] as $evs){
                        echo '<option value="'.$evs['id'].'">'.$evs['name'].'</option>';
                      } 
                    ?>
                  </select>
              </div>              

              <div class="form-group">
                  <label>Related Products : </label>
                  <!-- <input type="text" name="prodCode" id="prodCode" class="form-control"> -->
                  <select  name="txtRelatedProd[]" class="form-control" style="height: 102px;" multiple>
                    <?php 
                      foreach($product_list['result'] as $prods){
                        echo '<option value="'.$prods['id'].'">'.$prods['name'].'</option>';
                      } 
                    ?>
                  </select>
              </div>
          </div>

          <div class="col-md-6">
              <div class="form-group">
                  <label>Costs : </label>
                  <table class="table table-striped" id="tblCosts">
                      <tr>
                          <td></td>
                          <td>Option Name</td>
                          <td>Cost</td>
                          <td>Postage</td>
                      </tr>
                      <tr>
                          <td></td>
                          <td><input type="text" class="form-control" name="costs[]"></td>
                          <td><input type="text" class="form-control" name="costs[]"></td>
                          <td><input type="text" class="form-control" name="costs[]"></td>
                      </tr>
                  </table>
                  <button type="button" class="btn btn-primary" id="btnAddCosts"><i class="fa fa-plus"></i></button>
              </div>
              <hr>
              <div class="form-group" style="overflow-y: auto; height: 300px; overflow-x: hidden;">
                  <label>Attachments : </label>
                  <div class="row">
                      <div class="col-md-6">
                          <div class="row">
                              <div class="col-md-12">
                                  <div class="form-group">
                                      <input type="text" name="attachment[]" class="form-control" placeholder="Caption 1" />
                                  </div>
                              </div>
                              <div class="col-md-12">
                                  <div class="input-group">
                                      <input type="text" class="form-control" name="attachment[]" value="" placeholder="Image 1">
                                      <span class="input-group-btn">
                                            <button  type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
                                            </span>
                                  </div>
                              </div>
                          </div>

                          <div class="row">
                              <div class="col-md-12">
                                  <div class="form-group">
                                      <input type="text" name="attachment[]" class="form-control" placeholder="Caption 2" />
                                  </div>
                              </div>
                              <div class="col-md-12">
                                  <div class="input-group">
                                      <input type="text" class="form-control" name="attachment[]" value="" placeholder="Image 2">
                                      <span class="input-group-btn">
                                            <button  type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
                                            </span>
                                  </div>
                              </div>
                          </div>

                          <div class="row">
                              <div class="col-md-12">
                                  <div class="form-group">
                                      <input type="text" name="attachment[]" class="form-control" placeholder="Caption 3" />
                                  </div>
                              </div>
                              <div class="col-md-12">
                                  <div class="input-group">
                                      <input type="text" class="form-control" name="attachment[]" value="" placeholder="Image 3">
                                      <span class="input-group-btn">
                                            <button  type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
                                            </span>
                                  </div>
                              </div>
                          </div>

                          <div class="row">
                              <div class="col-md-12">
                                  <div class="form-group">
                                      <input type="text" name="attachment[]" class="form-control" placeholder="Caption 4" />
                                  </div>
                              </div>
                              <div class="col-md-12">
                                  <div class="input-group">
                                      <input type="text" class="form-control" name="attachment[]" value="" placeholder="Image 4">
                                      <span class="input-group-btn">
                                            <button  type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
                                            </span>
                                  </div>
                              </div>
                          </div>

                          <div class="row">
                              <div class="col-md-12">
                                  <div class="form-group">
                                      <input type="text" name="attachment[]" class="form-control" placeholder="Caption 5" />
                                  </div>
                              </div>
                              <div class="col-md-12">
                                  <div class="input-group">
                                      <input type="text" class="form-control" name="attachment[]" value="" placeholder="Image 5">
                                      <span class="input-group-btn">
                                            <button  type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
                                            </span>
                                  </div>
                              </div>
                          </div>
                      </div>

                      <div class="col-md-6">
                          <div class="row">
                              <div class="col-md-12">
                                  <div class="form-group">
                                      <input type="text" name="attachment[]" class="form-control" placeholder="Caption 6" />
                                  </div>
                              </div>
                              <div class="col-md-12">
                                  <div class="input-group">
                                      <input type="text" class="form-control" name="attachment[]" value="" placeholder="Image 6">
                                      <span class="input-group-btn">
                                            <button  type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
                                            </span>
                                  </div>
                              </div>
                          </div>

                          <div class="row">
                              <div class="col-md-12">
                                  <div class="form-group">
                                      <input type="text" name="attachment[]" class="form-control" placeholder="Caption 7" />
                                  </div>
                              </div>
                              <div class="col-md-12">
                                  <div class="input-group">
                                      <input type="text" class="form-control" name="attachment[]" value="" placeholder="Image 7">
                                      <span class="input-group-btn">
                                            <button  type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
                                            </span>
                                  </div>
                              </div>
                          </div>

                          <div class="row">
                              <div class="col-md-12">
                                  <div class="form-group">
                                      <input type="text" name="attachment[]" class="form-control" placeholder="Caption 8" />
                                  </div>
                              </div>
                              <div class="col-md-12">
                                  <div class="input-group">
                                      <input type="text" class="form-control" name="attachment[]" value="" placeholder="Image 8">
                                      <span class="input-group-btn">
                                            <button  type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
                                            </span>
                                  </div>
                              </div>
                          </div>

                          <div class="row">
                              <div class="col-md-12">
                                  <div class="form-group">
                                      <input type="text" name="attachment[]" class="form-control" placeholder="Caption 9" />
                                  </div>
                              </div>
                              <div class="col-md-12">
                                  <div class="input-group">
                                      <input type="text" class="form-control" name="attachment[]" value="" placeholder="Image 9">
                                      <span class="input-group-btn">
                                            <button  type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
                                            </span>
                                  </div>
                              </div>
                          </div>

                          <div class="row">
                              <div class="col-md-12">
                                  <div class="form-group">
                                      <input type="text" name="attachment[]" class="form-control" placeholder="Caption 10" />
                                  </div>
                              </div>
                              <div class="col-md-12">
                                  <div class="input-group">
                                      <input type="text" class="form-control" name="attachment[]" value="" placeholder="Image 10">
                                      <span class="input-group-btn">
                                            <button  type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
                                            </span>
                                  </div>
                              </div>
                          </div>

                      </div>
                  </div>

              </div>
          </div>

        </div>
      </form>
      <!-- END FORM -->


    </div>

  </div>
  <div class="spacer50"></div>
	<!-- <div class="clearfix"></div> -->
</div>


<!-- /page content -->

<style>
.spacer10 {
  clear: both;
  width: 100%;
  height: 10px;
}
.spacer15 {
  clear: both;
  width: 100%;
  height: 15px;
}
.spacer20 {
  clear: both;
  width: 100%;
  height: 20px;
}
.spacer25 {
  clear: both;
  width: 100%;
  height: 25px;
}
.spacer30 {
  clear: both;
  width: 100%;
  height: 30px;
}

.hide-me {
  display: none;
}

.right_col {
  min-height: 100vh !important;
}

</style>
<script src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
<script>
$(window).scroll(function(){
  var bn = $('#divHead');
  if($(window).scrollTop() > 0) {
    bn.addClass('stickme');
  }
  else{
    bn.removeClass('stickme');
  }
});
</script>

<script>
$(document).ready(function(){
  $('#btnAddCosts').click(function(){
    var html_ = '\
                      <tr>\
                          <td><button type="button" class="btn btn-danger btnRemEventPrice"><i class="fa fa-minus"></i></button></td>\
                          <td><input type="text" class="form-control" name="costs[]"></td>\
                          <td><input type="text" class="form-control" name="costs[]"></td>\
                          <td><input type="text" class="form-control" name="costs[]"></td>\
                      </tr>\
    ';
    $('#tblCosts tbody').append(html_);    
  });


  // $('#btnAddEventPrice').click(function(){
  //   var html_ = '<tr>'
  //                + '<td><button type="button" class="btn btn-danger btnRemEventPrice"><i class="fa fa-minus"></i></button></td>'
  //                + '<td><input type="text" class="form-control"></td>'
  //                + '<td><input type="text" class="form-control"></td>'
  //                + '<td><select class="form-control"></select></td>'
  //                + '</tr>'
  //   $('#tblEventPrice tbody').append(html_);
  // });


  $('#tblCosts').on('click', '.btnRemEventPrice', function(){
      $(this).parent().parent().remove();

  });

  $('.btnMedia-audio').hide();
  $('.btnMedia-video').hide();

  $('#inpType1').change(function(){
      var v = $(this).val();
      $('#inpType').val(v);

      if(v != '-'){
        var m = '';
        switch(v){
          case '1':
            m = 'video';
          break;
          case '2':
            m = 'audio';
          break;
        }
        $('.btnMedia-image').hide();
        $('.btnMedia-audio').hide();
        $('.btnMedia-video').hide();

        $('.btnMedia-'+m).show();
      }else{
        $('.btnMedia-audio').hide();
        $('.btnMedia-video').hide();
        $('.btnMedia-image').show();
      }
      
  });

  $('#inpIsDraft1').change(function(){
      var v = $(this).val();
      $('#inpIsDraft').val(v);
  });

  $('#inpTopic1').change(function(){
      var v = $(this).val();
      $('#inpTopic').val(v);
  });




  $('#btnSave').click(function(){
      var ser = $('#frmProducts').serialize();
      console.log(ser);
      $.ajax({
          type : 'post',
          url  : '<?php echo base_url(); ?>admin/products/save_products',
          data : ser,
          beforeSend: function(){

          },
          success: function(res){
            console.log(res);
            swal("Success", "Data has been saved", "success");
          }
      });
  });
});

</script>

<script>


</script>

<style>
/*.cf:after { visibility: hidden; display: block; font-size: 0; content: " "; clear: both; height: 0; }
* html .cf { zoom: 1; }
*:first-child+html .cf { zoom: 1; }

html { margin: 0; padding: 0; }
body { font-size: 100%; margin: 0; padding: 1.75em; font-family: 'Helvetica Neue', Arial, sans-serif; }

h1 { font-size: 1.75em; margin: 0 0 0.6em 0; }

a { color: #2996cc; }
a:hover { text-decoration: none; }

p { line-height: 1.5em; }
.small { color: #666; font-size: 0.875em; }
.large { font-size: 1.25em; }*/

/**
 * Nestable
 */

.dd { position: relative; display: block; margin: 0; padding: 0; max-width: 600px; list-style: none; font-size: 13px; line-height: 20px; }

.dd-list { display: block; position: relative; margin: 0; padding: 0; list-style: none; }
.dd-list .dd-list { padding-left: 30px; }
.dd-collapsed .dd-list { display: none; }

.dd-item,
.dd-empty,
.dd-placeholder { display: block; position: relative; margin: 0; padding: 0; min-height: 20px; font-size: 13px; line-height: 20px; }

.dd-handle { display: block; height: 30px; margin: 5px 0; padding: 5px 10px; color: #333; text-decoration: none; font-weight: bold; border: 1px solid #ccc;
    background: #fafafa;
    background: -webkit-linear-gradient(top, #fafafa 0%, #eee 100%);
    background:    -moz-linear-gradient(top, #fafafa 0%, #eee 100%);
    background:         linear-gradient(top, #fafafa 0%, #eee 100%);
    -webkit-border-radius: 3px;
            border-radius: 3px;
    box-sizing: border-box; -moz-box-sizing: border-box;
}
.dd-handle:hover { color: #2ea8e5; background: #fff; }

.dd-item > button { display: block; position: relative; cursor: pointer; float: left; width: 25px; height: 20px; margin: 5px 0; padding: 0; text-indent: 100%; white-space: nowrap; overflow: hidden; border: 0; background: transparent; font-size: 12px; line-height: 1; text-align: center; font-weight: bold; }
.dd-item > button:before { content: '+'; display: block; position: absolute; width: 100%; text-align: center; text-indent: 0; }
.dd-item > button[data-action="collapse"]:before { content: '-'; }

.dd-placeholder,
.dd-empty { margin: 5px 0; padding: 0; min-height: 30px; background: #f2fbff; border: 1px dashed #b6bcbf; box-sizing: border-box; -moz-box-sizing: border-box; }
.dd-empty { border: 1px dashed #bbb; min-height: 100px; background-color: #e5e5e5;
    background-image: -webkit-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
                      -webkit-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
    background-image:    -moz-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
                         -moz-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
    background-image:         linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
                              linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
    background-size: 60px 60px;
    background-position: 0 0, 30px 30px;
}

.dd-dragel { position: absolute; pointer-events: none; z-index: 9999; }
.dd-dragel > .dd-item .dd-handle { margin-top: 0; }
.dd-dragel .dd-handle {
    -webkit-box-shadow: 2px 4px 6px 0 rgba(0,0,0,.1);
            box-shadow: 2px 4px 6px 0 rgba(0,0,0,.1);
}

/**
 * Nestable Extras
 */

.nestable-lists { display: block; clear: both; padding: 30px 0; width: 100%; border: 0; border-top: 2px solid #ddd; border-bottom: 2px solid #ddd; }

#nestable-menu { padding: 0; margin: 20px 0; }

#nestable-output,
#nestable2-output { width: 100%; height: 7em; font-size: 0.75em; line-height: 1.333333em; font-family: Consolas, monospace; padding: 5px; box-sizing: border-box; -moz-box-sizing: border-box; }

#nestable2 .dd-handle {
    color: #fff;
    border: 1px solid #999;
    background: #bbb;
    background: -webkit-linear-gradient(top, #bbb 0%, #999 100%);
    background:    -moz-linear-gradient(top, #bbb 0%, #999 100%);
    background:         linear-gradient(top, #bbb 0%, #999 100%);
}
#nestable2 .dd-handle:hover { background: #bbb; }
#nestable2 .dd-item > button:before { color: #fff; }

@media only screen and (min-width: 700px) {

    .dd { float: left; width: 48%; }
    .dd + .dd { margin-left: 2%; }

}

.dd-hover > .dd-handle { background: #2ea8e5 !important; }

/**
 * Nestable Draggable Handles
 */

.dd3-content { display: block; height: 30px; margin: 5px 0; padding: 5px 10px 5px 40px; color: #333; text-decoration: none; font-weight: bold; border: 1px solid #ccc;
    background: #fafafa;
    background: -webkit-linear-gradient(top, #fafafa 0%, #eee 100%);
    background:    -moz-linear-gradient(top, #fafafa 0%, #eee 100%);
    background:         linear-gradient(top, #fafafa 0%, #eee 100%);
    -webkit-border-radius: 3px;
            border-radius: 3px;
    box-sizing: border-box; -moz-box-sizing: border-box;
}
.dd3-content:hover { color: #2ea8e5; background: #fff; }

.dd-dragel > .dd3-item > .dd3-content { margin: 0; }

.dd3-item > button { margin-left: 30px; }

.dd3-handle { position: absolute; margin: 0; left: 0; top: 0; cursor: pointer; width: 30px; text-indent: 100%; white-space: nowrap; overflow: hidden;
    border: 1px solid #aaa;
    background: #ddd;
    background: -webkit-linear-gradient(top, #ddd 0%, #bbb 100%);
    background:    -moz-linear-gradient(top, #ddd 0%, #bbb 100%);
    background:         linear-gradient(top, #ddd 0%, #bbb 100%);
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
}
.dd3-handle:before { content: '≡'; display: block; position: absolute; left: 0; top: 3px; width: 100%; text-align: center; text-indent: 0; color: #fff; font-size: 20px; font-weight: normal; }
.dd3-handle:hover { background: #ddd; }


#divHead {
  background-color: #F6F6F6;
  width: 100%;
}

.stickme {
  position:sticky ;
  top :10px;
  position : -webkit-sticky;
  z-index: 10;
}
</style>
<script>
$(document).ready(function(){
    if ($(".js-switch")[0]) {
      var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
      elems.forEach(function (html) {
          var switchery = new Switchery(html, {
              color: '#26B99A'
          });
      });
    }


    

});
</script>
<div class="row hide-me">
  <div class="col-md-5">
    <div class="input-group">
    <input type="text" class="form-control">
    <span class="input-group-btn">
    <button id="ppm-browse-image" type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
    </span>
    </div>
  </div>
</div>
<?php if(isset($upload)) echo $upload; ?>