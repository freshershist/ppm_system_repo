<style>
	hr.style-two {
	    border: 0;
	    height: 0;
	    border-top: 1px solid rgba(0, 0, 0, 0.1);
	    border-bottom: 1px solid rgba(255, 255, 255, 0.3);
	}	
</style>

<div class="container">

	<div class="col-md-12 col-sm-12 col-xs-12">
		<div id="copy-title"class="alert alert-success fade in" role="alert" style="display: none;">
			<strong></strong>
		</div>
		<p class="lead"><small class="text-info small">Compulsary fields are marked by an asterisk ( <i class="fa fa-asterisk text-danger small"></i> )</small></p>

		<form id="add-edit-form" class="form-horizontal form-label-left">
			<input id="data-id" type="hidden" name="id" value="" />
			<input type="hidden" name="category" value="<?php echo $parent_category; ?>" />
			<a id="preview-new-window-btn" href="javascript:;" target="_blank" class="hide">preview</a>
			<!-- start accordion -->
			<div class="accordion" id="accordion-add-edit" role="tablist" aria-multiselectable="true">

				<!-- General -->	
				<div class="panel">
					<a class="panel-heading" role="tab" id="contents-add-edit_1" data-toggle="collapse" data-parent="#accordion-add-edit" href="#add-edit_1" aria-expanded="true" aria-controls="add-edit_1">
						<h4 class="panel-title">General</h4>
					</a>
					<div id="add-edit_1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="contents-add-edit_1">
						<div class="panel-body">
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Active:</label>
								<div class="col-md-5 col-sm-12 col-xs-12">
									
									<input id="data-active" name="isActive" type="checkbox" class="js-switch" checked />
									
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Display on Links Page?:</label>
								<div class="col-md-5 col-sm-12 col-xs-12">
									
									<input id="data-displaylinkspage" name="displaylinkspage" type="checkbox" class="js-switch" />
									
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Company Name: <i class="fa fa-asterisk text-danger"></i></label>
								<div class="col-md-5 col-sm-12 col-xs-12">
									<input id="data-name" name="name" type="text" class="form-control" placeholder="Name" value="" />
								</div>
							</div>													

							<div class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Primary Contact: <i class="fa fa-asterisk text-danger"></i></label>
								<div class="col-md-5 col-sm-12 col-xs-12">
									<input id="data-pcontact" name="pcontact" type="text" class="form-control" placeholder="Name" value="" />
								</div>
							</div>

							<div class="form-group" >
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Sub Category: <i class="fa fa-asterisk text-danger"></i></label>
								<div class="col-md-5 col-sm-12 col-xs-12">
									<?php

									echo $this->ppmsystemlib->createDropdown('subCategory', $sub_categories_arr[$tab], '', FALSE, TRUE);

									?>
								</div>
							</div>

							<?php if($parent_category === 35) { ?>
							<div class="form-group" >
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Sub Category 2: <i class="fa fa-asterisk text-danger"></i></label>
								<div class="col-md-5 col-sm-12 col-xs-12">
									<?php

									echo $this->ppmsystemlib->createDropdown('nationalSubCat', $sub_categories2_arr, '', TRUE, TRUE);

									?>
								</div>
							</div>	
							<?php } ?>

							<div class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Website: <i class="fa fa-asterisk text-danger"></i></label>
								<div class="col-md-5 col-sm-12 col-xs-12">
									<div class="input-group input-group-sm">
										<label class="input-group-addon" for="website">http://</label>
										<input id="data-website" name="website" type="text" class="form-control" placeholder="" value="" />
									</div>
								</div>
							</div>												

							<div class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Postcode: <i class="fa fa-asterisk text-danger"></i></label>
								<div class="col-md-5 col-sm-12 col-xs-12">
									<input id="data-postcode" name="postcode" type="text" class="form-control" placeholder="" value="" />
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Address: </label>
								<div class="col-md-5 col-sm-12 col-xs-12">
									<textarea id="data-address" name="address" class="form-control" rows="5"></textarea>
								</div>
							</div>							

							<div class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Phone: </i></label>
								<div class="col-md-5 col-sm-12 col-xs-12">
									<input id="data-phone" name="phone" type="text" class="form-control" placeholder="" value="" />
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Fax: </i></label>
								<div class="col-md-5 col-sm-12 col-xs-12">
									<input id="data-fax" name="fax" type="text" class="form-control" placeholder="" value="" />
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Email: </i></label>
								<div class="col-md-5 col-sm-12 col-xs-12">
									<input id="data-email" name="email" type="text" class="form-control" placeholder="" value="" />
								</div>
							</div>

							<?php if($parent_category !== 145) { ?>
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Short Description: </label>
								<div class="col-md-5 col-sm-12 col-xs-12">
									<textarea id="data-shortDesc" name="shortDesc" class="form-control" rows="5"></textarea>
								</div>
							</div>
							<?php } ?>						

						</div>	
					</div>
				</div>
				<!-- General -->

				<?php if($parent_category !== 145) { ?>
				<!-- Body -->	
				<div id="body-container" class="panel">
					<a class="panel-heading" role="tab" id="contents-add-edit_2" data-toggle="collapse" data-parent="#accordion-add-edit" href="#add-edit_2" aria-expanded="false" aria-controls="add-edit_2">
						<h4 class="panel-title">Link Description</h4>
					</a>
					<div id="add-edit_2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="contents-add-edit_2">
						<div class="panel-body">

							<div class="form-group">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<?php if(isset($editor)) echo $editor; ?>
								</div>
							</div>

						</div>
					</div>
				</div>
				<!-- Body -->	

				<?php } ?>

				<!-- Files -->	
				<div id="files-container" class="panel">
					<a class="panel-heading" role="tab" id="contents-add-edit_5" data-toggle="collapse" data-parent="#accordion-add-edit" href="#add-edit_5" aria-expanded="false" aria-controls="add-edit_5">
						<h4 class="panel-title">File Attachments</h4>
					</a>
					<div id="add-edit_5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="contents-add-edit_5">
						<div class="panel-body">

						<?php

							for ($i=1; $i <=2; $i++) { 

						?>
							<div class="form-group">
								<label class="control-label col-md-1 col-sm-12 col-xs-12">File <?php echo $i; ?>: </label>
								<div class="col-lg-5 col-md-8 col-sm-12 col-xs-12">
									<div class="input-group">
										<input id="data-d<?php echo $i; ?>" type="text" name="d<?php echo $i; ?>" readonly="true" class="form-control">
										<span class="input-group-btn">
										<button type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
										</span>
									</div>
								</div>							
							</div>

						<?php } ?>

						</div>
					</div>
				</div>
				<!-- Files -->			

				<?php if($parent_category === 37) { ?>
				<!-- Body -->	
				<div class="panel">
					<a class="panel-heading" role="tab" id="contents-add-edit_3" data-toggle="collapse" data-parent="#accordion-add-edit" href="#add-edit_3" aria-expanded="false" aria-controls="add-edit_3">
						<h4 class="panel-title">Member Discount | Payment</h4>
					</a>
					<div id="add-edit_3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="contents-add-edit_3">
						<div class="panel-body">

							<div class="form-group">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-12 col-xs-12">Display on Members Page (Discounts)?:</label>
										<div class="col-md-5 col-sm-12 col-xs-12">
											
											<input id="data-displaymember" name="displaymember" type="checkbox" class="js-switch" />
											
										</div>
									</div>

									<div class="form-group">
										<label class="control-label col-md-3 col-sm-12 col-xs-12">Discount: </label>
										<div class="col-md-5 col-sm-12 col-xs-12">
											<textarea id="data-memberDiscounts" name="memberDiscounts" class="form-control" rows="5"></textarea>
										</div>
									</div>

									<div class="form-group">
										<label class="control-label col-md-3 col-sm-12 col-xs-12">Amount: </i></label>
										<div class="col-md-5 col-sm-12 col-xs-12">
											<div class="input-group input-group-sm">
												<label class="input-group-addon" for="cost"><i class="fa fa-dollar"></i></label>
												<input id="data-cost" name="cost" type="text" class="form-control" placeholder="" value="" />
											</div>
										</div>
									</div>

									<div class="form-group">
										<label class="control-label col-md-3 col-sm-12 col-xs-12">Expiry Date: </label>
										<div class="col-md-5 col-sm-12 col-xs-12">
									        <div class="input-group date" id="expiry-date">
									            <input id="data-expiry" name="expiry" type="text" class="form-control" placeholder="dd/mm/yyyy" />
									            <span class="input-group-addon">
									            <span class="glyphicon glyphicon-calendar"></span>
									            </span>
									        </div>
										</div>
									</div>									

								</div>
							</div>

						</div>
					</div>
				</div>
				<!-- Body -->	

				<?php } ?>

			</div>
	
		</form>

	</div>	

	<div class="row">
		<div class="panel">
			<div class="col-md-12 text-right">
				<button id="cancel-btn" type="button" class="btn btn-sm btn-warning" style="display: none;">Cancel Changes</button>
				<button id="update-btn" type="button" class="btn btn-sm btn-success" style="display: none;">Update</button>
				<button id="add-btn" type="button" class="btn btn-sm btn-success add-btn">Add</button>
			</div>
			<div class="clearfix"></div>
		</div>	
	</div>

</div>

<div class="row hide">
	<div class="col-md-5">
		<div class="input-group">
		<input type="text" class="form-control">
		<span class="input-group-btn">
		<button id="ppm-browse-image" type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
		</span>
		</div>
	</div>
</div>

<script>

	$(document).ready(function(){
		var body = <?php if(isset($editor_id)) { ?> $('#<?php echo $editor_id; ?>'); <?php } else { echo 'null';}?>;
		var form = $('#add-edit-form');
		var products;
		var downloads;
		var fields = {
						id: 					$('#data-id'),
						isActive: 				$('#data-active'),
						displaylinkspage: 		$('#data-displaylinkspage'),
						displaymember: 			$('#data-displaymember'),
						name: 					$('#data-name'),
						pcontact: 				$('#data-pcontact'),
						categoryID: 			$('#subCategory'),
						nationalSubCat: 		$('#nationalSubCat'),
						website: 				$('#data-website'),
						postcode: 				$('#data-postcode'),
						address: 				$('#data-address'),
						phone: 					$('#data-phone'),
						fax: 					$('#data-fax'),
						email: 					$('#data-email'),
						shortDesc: 				$('#data-shortDesc'),
						body: 					$('#data_body'),					
						memberDiscounts: 		$('#data-memberDiscounts'),
						cost: 					$('#data-cost'),
						expiry: 				$('#data-expiry'),
						d1:						$('#data-d1'),
						d2: 					$('#data-d2')
					};

		var contents = {
			init: function(){

			<?php if($parent_category === 37) { ?>
				$('#expiry-date').datetimepicker({format: 'DD/MM/YYYY'});
			<?php } ?>

				/*fields.categories.on('change', function(){

					contents.checkLocation();
					contents.checkNewsCategory();
				});

				fields.state.on('change', function(){
					contents.checkState();
				});*/

				addBtn.bind('click', function(){
					contents.submitForm();
				});

				updateBtn.bind('click', function(){
					contents.submitForm();
				});

			},
			submitForm: function(){

				if(CKEDITOR.instances.<?php echo $editor_id; ?>!=undefined) {
					var bodyText = CKEDITOR.instances.<?php echo $editor_id; ?>.getData();

					body.text($.trim(bodyText.replace(/[\t\n]+/g, ' ')));
				}

				var isEdit = $.isNumeric(fields.id.val());

				swal({
				    title: (isEdit)?"Update Item" : "Add Item",
				    text: "Are you sure?",
				    icon: "warning",
				    buttons: ["Cancel", true],
				    dangerMode: false,
				})
				.then(willSubmit => {
				    if (willSubmit) {
						var request = $.ajax({
				          url: "<?php echo base_url(); ?>admin/links/add_update",
				          method: "POST",
				          data: form.serialize(),
				          dataType: 'json'
				        });

				        request.done(function( res ) {
				            if(res!=null) {

				            	if(isEdit) {
				            		if(res.hasOwnProperty('error')) {
				            			var errMsg = '';

				            			$.each(res.error, function(k, v){
				            				errMsg += '* ' + v + '\n';
				            			});

				            			new PNotify({
											title: 'Invalid or Required!',
											text: errMsg,
											type: 'error',
											styling: 'bootstrap3'
			                            });

				            			swal({title:"Update Failed!",text:"Some fields are required (*) or have invalid values",icon:'error',button:false, timer:3000});
				            		}
				            		else {
				            			filter.applyUpdateChanges(res);

				            			swal({title:"Updated!",text:'Changes have been saved.',icon:'success',button:false, timer:3000});
				            		}
				            		
				            	}
				            	else {
				            		if(res.hasOwnProperty('error')) {
										var errMsg = '';

				            			$.each(res.error, function(k, v){
				            				errMsg += '* ' + v + '\n';
				            			});

				            			new PNotify({
											title: 'Invalid or Required!',
											text: errMsg,
											type: 'error',
											styling: 'bootstrap3'
			                            });

			                            swal({title:"Add Failed!",text:"Some fields are required (*) or have invalid values",icon:'error',button:false, timer:3000});
				            		}
				            		else {
				            			fields.id.val(res.id);
				            			contents.showUpdateButton();
				            			addTab.html('<i class="fa fa-pencil"></i> Edit');
				            			
				            			swal({title:"Success!",text:"Added item with id: " + res.id,icon:'success',button:false, timer:3000});
				            			filter.addNewItem(res);
				            		}
				            	}
				            }
		        		});

				      	request.error(function(_data){
		        			if(_data.readyState == 4 && _data.status == 200) {//Session expired and redirected

		        				swal({
								    title: "Session Expired!",
								    text: "Reloading page.",
								    icon: "warning",
								    dangerMode: false,
								})
								.then(ok => {
								    if (ok) {
								    	window.location.href = '<?php echo base_url();?>admin/login';
								    }
								});
		        			}
		        		});
					}
				}); 

			},
			resetForm: function(){

				$.each(fields, function(k,v){
					if(v.length>0) {
						if(k == 'isActive') {
							if(!v.prop('checked')) {
								v.click();
							}
						}
						else if(k == 'displaylinkspage' || k == 'displaymember') {
							if(v.prop('checked')) {
								v.click();
							}
						}
						else if(k == 'body') {
							CKEDITOR.instances.<?php echo $editor_id; ?>.setData('');
						}
						else {
							v.val('');
						}
					}
				});
			},
			populateForm: function(data){

				if(data!=null) {

					this.resetForm();

					setTimeout(function(){
						$.each(data, function(k,v){

							if(fields.hasOwnProperty(k)  && fields[k].length>0) {

								if(k == 'isActive' || k == 'displaylinkspage' || k == 'displaymember') {
									if(fields[k].prop('checked') && (v == '0' || v == null)) {
										fields[k].click();
									}
									else if(!fields[k].prop('checked') && v == '1') {
										fields[k].click();
									}
								}
								else if(k == 'body') {
									CKEDITOR.instances.<?php echo $editor_id; ?>.setData(v);
								}
								else {
									fields[k].val(v);
								}
							}

						});

						contents.showUpdateButton();

						if(addTab!=null) addTab.click();
					}, 50);
				}
			},
			showUpdateButton: function(){
				cancelBtn.show();
				updateBtn.show();
				addBtn.hide();	
			},
			showAddButton: function(){
				cancelBtn.hide();
				updateBtn.hide();
				addBtn.show();	
			},
			cancelUpdate: function(){

				swal({
				    title: "Cancel Changes",
				    text: "Are you sure?",
				    icon: "warning",
				    buttons: ["Cancel", true],
				    dangerMode: false,
				})
				.then(willCancel => {
				    if (willCancel) {
				    	contents.resetTab();
				    }
				});
			},
			resetTab: function(){
		    	addTab.html('<i class="fa fa-plus"></i> Add');
		    	contents.resetForm();
				contents.showAddButton();
				searchTab.trigger('click');
			}
		};

		contents.init();

	    if ($(".js-switch")[0]) {
	        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
	        elems.forEach(function (html) {
	            var switchery = new Switchery(html, {
	                color: '#26B99A'
	            });
	        });
	    }

	    addEdit = contents;
	});
</script>