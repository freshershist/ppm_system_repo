<!-- page content -->
<div class="right_col" role="main" style="min-height: 100vh;">

  <div class="row">
    <div class="col-md-12">
      <div id="divHead">

          <?php if(empty($page_id) || $page_id == 0){ ?>
          <h1><i class="fa fa-calendar"></i> New Event</h1>
          <?php }else{ ?>
          <h1><i class="fa fa-calendar"></i> Edit Event</h1>
          <?php } ?>
          <div class="row">
            <div class="col-md-12" id="dispMessages"></div>
          </div>
          <div class="spacer20"></div>
          <div class="row">
              <div class="col-md-12">
                  <a href="<?php echo base_url().'admin/events'; ?>" class="btn btn-primary">
                      <i class="fa fa-arrow-left"></i> Back
                  </a>
                  <button type="button" class="btn btn-primary" id="btnSave"><i class="fa fa-save"></i> Save</button>
              </div>
          </div>

      </div>
      <div class="spacer10"></div>
      <hr/>
      <!-- <div class="spacer10"></div> -->
      <form id="frmEvents">

      <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#home">Event Details</a></li>
              <li><a data-toggle="tab" href="#menuX1">Event Pricing</a></li>
              <li id="menu_menuX2"><a data-toggle="tab" href="#menuX2">Conference Details</a></li>
              <li id="menu_menuX3"><a data-toggle="tab" href="#menuX3">Attachments</a></li>
            </ul>

            <div class="tab-content">
              <div id="home" class="tab-pane fade in active">
                <div class="spacer10"></div>

                  <!-- START : MAIN FORM -->
                  
                  <div class="row">
                    <!-- START : LEFT PANEL -->
                    <div class="col-md-6">
                      <!-- START : FORM -->
                      <div class="panel panel-primary">
                        <!-- <div class="panel-heading"><strong>Event Info</strong></div> -->
                        <div class="panel-body">
                          <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                  <input type="hidden" name="inpEventId" id="inpEventId">
                                  <label>* Category : </label>
                                  <?php
                                    // print_r($category_list);
                                  ?>
                                  <select class="form-control" name="inpCat" id="inpCat" >
                                      <option value="-">-select category-</option>
                                  <?php 
                                    foreach ($category_list['result'] as $cat_list) {
                                      # code...
                                      echo '<option value="'.$cat_list['id'].'">'.$cat_list['name'].'</option>';
                                    }
                                  ?>
                                  </select>
                                </div>

                                <div class="form-group">
                                    <label>* Event Code : </label>
                                    <input type="text" class="form-control"  name="inpEventCode" id="inpEventCode" >
                                </div>

                                <div class="form-group">
                                  <label>* Location : </label>
                                  <select class="form-control" name="inpEventLoc" id="inpEventLoc" >
                                      <option value="-">-select location-</option>
                                  <?php 
                                    foreach ($location_list['result'] as $loc_list) {
                                      # code...
                                      echo '<option value="'.$loc_list['id'].'">'.$loc_list['name'].'</option>';
                                    }
                                  ?>
                                  </select>
                                </div>        

                                <div class="form-group">
                                    <label>* Start Date : </label>
                                    <input type="date" class="form-control" name="startDate" id="startDate" >
                                </div>

                                <div class="form-group">
                                    <label>Time : </label>
                                    <input type="text" class="form-control"  name="inpTime">
                                </div>
                                <div class="form-group">
                                    <label>Places Left : </label>
                                    <input type="text" class="form-control"  name="placesLeft">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>* Event Name : </label>
                                    <input type="text" class="form-control"  name="inpEventName">
                                </div>

                                <div class="form-check">
                                  <small>Only tick if a general on-line sesion - NOT member or corporate sessions</small>
                                  <div class="spacer5"></div>
                                  <input type="checkbox" class="form-check-input" id="exampleCheck1"  name="inpIsOnline">
                                  <label class="form-check-label" for="exampleCheck1">Online Event</label>
                                  <div class="spacer20"></div>
                                </div>

                                <div class="form-group" style="margin-top:2px;">
                                    <label>Region / City : </label>
                                    <input type="text" class="form-control"  name="inpRegion">
                                </div>

                                <div class="form-group">
                                    <label>End Date : </label>
                                    <input type="date" class="form-control"  name="endDate">
                                </div>

                                <div class="form-group">
                                    <label>Venue : </label>
                                    <textarea class="form-control" style="resize: none; height: 102px;"  name="txtVenue"></textarea>
                                </div>

                            </div>
                          </div>

                        </div>
                      </div>
                      



                      <!-- END : FORM -->
                    </div>
                    <!-- END  : LEFT PANEL -->

                    <!-- START : RIGHT PANEL -->
                    <div class="col-md-6">
                      <!-- START : FORM -->
                      <div class="panel panel-primary">
                        <!-- <div class="panel-heading"><strong>Event Details</strong></div> -->
                        <div class="panel-body">
                          <div class="row">
                            <div class="col-md-6">

                                <div class="form-group">
                                    <label>Short Description : </label>
                                    <textarea  name="txtShortDesc" class="form-control" style="resize: none; height: 102px;"></textarea>
                                </div>

                                <div class="form-group">
                                    <label>Who should attend : </label>
                                    <textarea  name="txtAttendees" class="form-control" style="resize: none; height: 102px;"></textarea>
                                </div>

                                <div class="form-group">
                                    <label>Related Products : </label>
<!--                                     <textarea  name="txtRelatedProd" class="form-control" style="resize: none; height: 102px;"></textarea> -->
                                    
                                  <select  name="txtRelatedProd[]" class="form-control" style="height: 102px;" multiple>
                                    <?php 
                                      foreach($product_list['result'] as $prods){
                                        echo '<option value="'.$prods['id'].'">'.$prods['name'].'</option>';
                                      } 
                                    ?>
                                  </select>
                                </div>
                            </div>
                            <div class="col-md-6">

                                <div class="form-group">
                                    <label>Description : </label>
                                    <textarea  name="txtDesc" class="form-control" style="resize: none; height: 102px;"></textarea>
                                </div>

                                <div class="form-group">
                                    <label>Included : </label>
                                    <textarea  name="txtIncluded" class="form-control" style="resize: none; height: 102px;"></textarea>
                                </div>

                                <div class="form-group">
                                    <label>Related Galleries : </label>
                                    <!-- <textarea class="form-control" style="resize: none; height: 102px;"></textarea> -->
                                  <select  name="txtRelatedGall[]" class="form-control" style="height: 102px;" multiple>
                                    <?php 
                                      foreach($gallery_list['result'] as $gallery){
                                        echo '<option value="'.$gallery['id'].'">'.$gallery['name'].'</option>';
                                      } 
                                    ?>
                                  </select>

                                </div>
                            </div>
                          </div>
                        </div>

                          
                      </div>
                      <!-- END : FORM -->
                    </div>
                    <!-- END   : RIGHT PANEL -->
                  </div>
                  
                  <!-- END   : MAIN FORM -->
                
              </div>
              <div id="menuX1" class="tab-pane fade">
                <div class="spacer10"></div>
<!--                 <h3>Menu 1</h3>
                <p>Some content in menu 1.</p> -->
                  <!-- START : MAIN FORM -->
                  <div class="row">
                    <!-- START : LEFT PANEL -->
                    <div class="col-md-8">
                      <!-- START : FORM -->
                      <div class="panel panel-primary">
                        <!-- <div class="panel-heading"><strong>Event Pricing</strong></div> -->
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Cocktail Party Cost</label>
                                        <input type="text" name="inpCocktailCost" id="inpCocktailCost" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="text" name="inpEmail" id="inpEmail" class="form-control">
                                    </div>
                                </div>
                            </div>



                            <table class="table table-striped" id="tblEventPrice">
                                <thead>
                                  <th></th>
                                  <th>OPTION NAME</th>
                                  <th>COST</th>
                                  <th>LIMIT TO</th>
                                </thead>
                              <tbody>
                                <tr>
                                  <td>&nbsp;</td>
                                  <td><input type="text" class="form-control" name="event_pricing[]"></td>
                                  <td><input type="text" class="form-control" name="event_pricing[]"></td>
                                  <td><select class="form-control"  name="event_pricing[]">
                                    <?php 
                                      $limitToSubscriberArray = ["On-Line Subscribers","Silver","Gold","Platinum","All Members (Silver,Gold,Platinum)"];
                                      $x = 1;
                                      echo '<option value="0">Select</option>';
                                      foreach ($limitToSubscriberArray as $key => $value) {
                                        # code...
                                        
                                        echo '<option value="'.($x++).'">'.$value.'</option>';
                                      }
                                    ?>
                                  </select></td>
                                </tr>
                              </tbody>
                            </table>

                            <div class="row">
                              <div class="col-md-6">
                                <button class="btn btn-primary" id="btnAddEventPrice" type="button"><i class="fa fa-plus"></i></button>
                              </div>
                            </div>
                        </div>

                          
                      </div>
                      <!-- END : FORM -->
                    </div>
                    <!-- END  : LEFT PANEL -->

                    <!-- START : RIGHT PANEL -->
                    <div class="col-md-4">

                    </div>
                    <!-- END   : RIGHT PANEL -->
                  </div>
                  <!-- END   : MAIN FORM -->

              </div>

              <!-- START MENUX3 -->
              <div id="menuX3" class="tab-pane fade">
                <div class="spacer10"></div>
                <small><strong>File</strong><br/>
                These images must be uploaded at 200 Pixels Wide, RGB Jpeg Files at 72 DPI.</small>
                <div style="clear: both"></div>
                <div class="row">
                  <div class="col-md-3">
                      <div class="panel panel-primary">
                        <div class="panel-body">
                        <label>General Events</label>
                            <div class="row">
                              <div class="col-md-12">
                                  <div class="input-group">
                                  <input type="text" class="form-control" name="genEvents[]"  value="">
                                  <span class="input-group-btn">
                                  <button  type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
                                  </span>
                                  </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-12">
                                  <div class="input-group">
                                  <input type="text" class="form-control" name="genEvents[]"  value="">
                                  <span class="input-group-btn">
                                  <button  type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
                                  </span>
                                  </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-12">
                                  <div class="input-group">
                                  <input type="text" class="form-control" name="genEvents[]"  value="">
                                  <span class="input-group-btn">
                                  <button  type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
                                  </span>
                                  </div>
                              </div>
                            </div>
                        </div>
                      </div>
                  </div>
                </div>


                <div class="row">
                  <div class="col-md-12">

                    <!-- START ACCORDION -->

                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                                                        Conference Venue Images & Conference Travel Images</a>
                                                      </h4>
                                </div>
                                <div id="collapse1" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="row">

                                            <div class="col-md-6">
                                                <div class="panel panel-primary">
                                                    <div class="panel-body">
                                                        <label>Conference Venue Images</label>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <input type="text" name="confVenue[]" class="form-control" placeholder="Caption 1" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control" name="confVenue[]" value="" placeholder="Image 1">
                                                                    <span class="input-group-btn">
                                                                          <button  type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
                                                                          </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <input type="text" name="confVenue[]" class="form-control" placeholder="Caption 2" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control" name="confVenue[]" value="" placeholder="Image 2">
                                                                    <span class="input-group-btn">
                                                                          <button  type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
                                                                          </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <input type="text" name="confVenue[]" class="form-control" placeholder="Caption 3" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control" name="confVenue[]" value="" placeholder="Image 3">
                                                                    <span class="input-group-btn">
                                                                          <button  type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
                                                                          </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <input type="text" name="confVenue[]" class="form-control" placeholder="Caption 4" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control" name="confVenue[]" value="" placeholder="Image 4">
                                                                    <span class="input-group-btn">
                                                                          <button  type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
                                                                          </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <input type="text" name="confVenue[]" class="form-control" placeholder="Caption 5" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control" name="confVenue[]" value="" placeholder="Image 5">
                                                                    <span class="input-group-btn">
                                                                          <button  type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
                                                                          </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <input type="text" name="confVenue[]" class="form-control" placeholder="Caption 6" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control" name="confVenue[]" value="" placeholder="Image 6">
                                                                    <span class="input-group-btn">
                                                                          <button  type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
                                                                          </span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="panel panel-primary">
                                                    <div class="panel-body">
                                                        <label>Conference Travel Images</label>

                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <input type="text" name="confTravel[]" class="form-control" placeholder="Caption 1" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control" name="confTravel[]" value="" placeholder="Image 1">
                                                                    <span class="input-group-btn">
                                                                          <button  type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
                                                                          </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <input type="text" name="confTravel[]" class="form-control" placeholder="Caption 2" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control" name="confTravel[]" value="" placeholder="Image 2">
                                                                    <span class="input-group-btn">
                                                                          <button  type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
                                                                          </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <input type="text" name="confTravel[]" class="form-control" placeholder="Caption 3" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control" name="confTravel[]" value="" placeholder="Image 3">
                                                                    <span class="input-group-btn">
                                                                          <button  type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
                                                                          </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <input type="text" name="confTravel[]" class="form-control" placeholder="Caption 4" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control" name="confTravel[]" value="" placeholder="Image 4">
                                                                    <span class="input-group-btn">
                                                                          <button  type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
                                                                          </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <input type="text" name="confTravel[]" class="form-control" placeholder="Caption 5" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control" name="confTravel[]" value="" placeholder="Image 5">
                                                                    <span class="input-group-btn">
                                                                          <button  type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
                                                                          </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <input type="text" name="confTravel[]" class="form-control" placeholder="Caption 6" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control" name="confTravel[]" value="" placeholder="Image 6">
                                                                    <span class="input-group-btn">
                                                                          <button  type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
                                                                          </span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                                                            Conference Social Images & Conference Sponsorship Opportunity Images</a>
                                                          </h4>
                                </div>
                                <div id="collapse2" class="panel-collapse collapse">
                                    <div class="panel-body">

                                        <div class="row">

                                            <div class="col-md-6">
                                                <div class="panel panel-primary">
                                                    <div class="panel-body">
                                                        <label>Conference Social Images</label>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <input type="text" name="confSocial[]" class="form-control" placeholder="Caption 1" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control" name="confSocial[]" value="" placeholder="Image 1">
                                                                    <span class="input-group-btn">
                                                                              <button  type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
                                                                              </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <input type="text" name="confSocial[]" class="form-control" placeholder="Caption 2" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control" name="confSocial[]" value="" placeholder="Image 2">
                                                                    <span class="input-group-btn">
                                                                              <button  type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
                                                                              </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <input type="text" name="confSocial[]" class="form-control" placeholder="Caption 3" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control" name="confSocial[]" value="" placeholder="Image 3">
                                                                    <span class="input-group-btn">
                                                                              <button  type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
                                                                              </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <input type="text" name="confSocial[]" class="form-control" placeholder="Caption 4" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control" name="confSocial[]" value="" placeholder="Image 4">
                                                                    <span class="input-group-btn">
                                                                              <button  type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
                                                                              </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <input type="text" name="confSocial[]" class="form-control" placeholder="Caption 5" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control" name="confSocial[]" value="" placeholder="Image 5">
                                                                    <span class="input-group-btn">
                                                                              <button  type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
                                                                              </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <input type="text" name="confSocial[]" class="form-control" placeholder="Caption 6" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control" name="confSocial[]" value="" placeholder="Image 6">
                                                                    <span class="input-group-btn">
                                                                              <button  type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
                                                                              </span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="panel panel-primary">
                                                    <div class="panel-body">
                                                        <label>Conference Sponsorship Opportunity Images</label>

                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <input type="text" name="confSponsorship[]" class="form-control" placeholder="Caption 1" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control" name="confSponsorship[]" value="" placeholder="Image 1">
                                                                    <span class="input-group-btn">
                                                                              <button  type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
                                                                              </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <input type="text" name="confSponsorship[]" class="form-control" placeholder="Caption 2" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control" name="confSponsorship[]" value="" placeholder="Image 2">
                                                                    <span class="input-group-btn">
                                                                              <button  type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
                                                                              </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <input type="text" name="confSponsorship[]" class="form-control" placeholder="Caption 3" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control" name="confSponsorship[]" value="" placeholder="Image 3">
                                                                    <span class="input-group-btn">
                                                                              <button  type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
                                                                              </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <input type="text" name="confSponsorship[]" class="form-control" placeholder="Caption 4" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control" name="confSponsorship[]" value="" placeholder="Image 4">
                                                                    <span class="input-group-btn">
                                                                              <button  type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
                                                                              </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <input type="text" name="confSponsorship[]" class="form-control" placeholder="Caption 5" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control" name="confSponsorship[]" value="" placeholder="Image 5">
                                                                    <span class="input-group-btn">
                                                                              <button  type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
                                                                              </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <input type="text" name="confSponsorship[]" class="form-control" placeholder="Caption 6" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control" name="confSponsorship[]" value="" placeholder="Image 6">
                                                                    <span class="input-group-btn">
                                                                              <button  type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
                                                                              </span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>


                    <!-- END ACCORDION -->

                  </div>
                </div>

              </div>
              <!-- END MENUX3 -->


              <!-- START MENUX2 -->
              <div id="menuX2" class="tab-pane fade">

                  <div class="spacer10"></div>
                  <div class="row">
                      <div class="col-md-6">
                          <div class="panel panel-primary">
                              <div class="panel-body">

                                  <div class="row">
                                      <div class="col-md-12">

                                  <label>Conference Speakers</label>
                                  <table class="table table-striped" id="tblEventSpeakers">
                                      <thead>
                                          <th></th>
                                          <th>NAME</th>
                                          <th>DESCRIPTION</th>
                                          <th>ORDER BY</th>
                                          <th>PHOTO</th>
                                      </thead>
                                      <tbody>
                                          <tr>
                                              <td>&nbsp;</td>
                                              <td>
                                                  <input type="text" class="form-control" name="speakers[]">
                                              </td>
                                              <td>
                                                  <input type="text" class="form-control" name="speakers[]">
                                              </td>
                                              <td>
                                                  <input type="text" class="form-control" name="speakers[]">
                                              </td>
                                              <td>
                                                  <div class="row">
                                                      <div class="col-md-12">
                                                          <div class="input-group">
                                                              <input type="text" class="form-control" name="speakers[]" value="<?php echo (isset($d1)) ? $d1:''; ?>">
                                                              <span class="input-group-btn">
                                                          <button  type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
                                                          </span>
                                                          </div>
                                                      </div>
                                                  </div>
                                              </td>
                                          </tr>
                                      </tbody>
                                  </table>
                                  <button type="button" class="btn btn-primary" id="btnAddSpeakers"><i class="fa fa-plus"></i></button>

                                      </div>
                                  </div>

                              </div>
                          </div>

                          <div style="clear: both"></div>

                          <div class="panel panel-primary">
                              <div class="panel-body">
                                  <div class="row">
                                      <div class="col-md-12">

                                          <label>Breakout Sessions</label>
                                          <table class="table table-striped" id="tblEventBreakouts">
                                              <thead>
                                                  <th></th>
                                                  <th>BREAKOUT</th>
                                                  <th>SESSION 1</th>
                                                  <th>SESSION 2</th>
                                                  <th>ORDER BY</th>
                                              </thead>
                                              <tbody>
                                                  <tr>
                                                      <td>&nbsp;</td>
                                                      <td>
                                                          <input type="text" class="form-control" name="breakout[]">
                                                      </td>
                                                      <td>
                                                          <input type="text" class="form-control" name="breakout[]">
                                                      </td>
                                                      <td>
                                                          <input type="text" class="form-control" name="breakout[]">
                                                      </td>
                                                      <td>
                                                          <input type="text" class="form-control" name="breakout[]">
                                                      </td>
                                                  </tr>
                                              </tbody>
                                          </table>
                                          <button type="button" class="btn btn-primary" id="btnAddBreakout"><i class="fa fa-plus"></i></button>
                                      
                                      </div>
                                  </div>

                              </div>
                          </div>
                      </div>

                      <div class="col-md-6">
                          <div class="panel panel-primary">
                              <div class="panel-body">
                                  <div class="row">
                                      <div class="col-md-12">
                                              <label>Conference Sponsors</label>
                                              <table class="table table-striped" id="tblEventConfSponsor">
                                                  <thead>
                                                      <th></th>
                                                      <th>NAME</th>
                                                      <th>DESCRIPTION</th>
                                                      <th>ORDER BY</th>
                                                      <th>PHOTO</th>
                                                  </thead>
                                                  <tbody>
                                                      <tr>
                                                          <td>&nbsp;</td>
                                                          <td>
                                                              <input type="text" class="form-control" name="confSponsors[]">
                                                          </td>
                                                          <td>
                                                              <input type="text" class="form-control" name="confSponsors[]">
                                                          </td>
                                                          <td>
                                                              <input type="text" class="form-control" name="confSponsors[]">
                                                          </td>
                                                          <td>
                                                              <div class="row">
                                                                  <div class="col-md-12">
                                                                      <div class="input-group">
                                                                          <input type="text" class="form-control" name="confSponsors[]" value="<?php echo (isset($d1)) ? $d1:''; ?>">
                                                                          <span class="input-group-btn">
                                                                      <button  type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
                                                                      </span>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </td>
                                                      </tr>
                                                  </tbody>
                                              </table>
                                              <button type="button" class="btn btn-primary" id="btnAddconfSponsors"><i class="fa fa-plus"></i></button>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          <div style="clear: both"></div>
                          <div class="panel panel-primary">
                              <div class="panel-body">
                                  <div class="row">
                                      <div class="col-md-12">
                                          <div class="form-group">
                                          <label>Conference Sponsorship Page</label>
                                          <textarea name="txtConferenceSponsorShip" class="form-control" rows="5"></textarea>
                                          </div>
                                      </div>
                                  </div>

                              </div>
                          </div>
                      </div>

                  </div>

              </div>
              <!-- END MENUX2 -->

            </div>

        </div>

      </div>

    </form>

    </div>

  </div>

	<!-- <div class="clearfix"></div> -->
</div>


<!-- /page content -->

<style>
.spacer10 {
  clear: both;
  width: 100%;
  height: 10px;
}
.spacer15 {
  clear: both;
  width: 100%;
  height: 15px;
}
.spacer20 {
  clear: both;
  width: 100%;
  height: 20px;
}
.spacer25 {
  clear: both;
  width: 100%;
  height: 25px;
}
.spacer30 {
  clear: both;
  width: 100%;
  height: 30px;
}

.hide-me {
  display: none;
}

.right_col {
  min-height: 100vh !important;
}

.panel-group {
  margin-bottom: 0px !important;
}

</style>
<script src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
<script>
$(window).scroll(function(){
  var bn = $('#divHead');
  if($(window).scrollTop() > 0) {
    bn.addClass('stickme');
  }
  else{
    bn.removeClass('stickme');
  }
});
</script>

<script>
$(document).ready(function(){
  $('#btnAddEventPrice').click(function(){
    var html_ = '<tr>'
                 + '<td><button type="button" class="btn btn-danger btnRemEventPrice"><i class="fa fa-minus"></i></button></td>'
                 + '<td><input type="text" class="form-control" name="event_pricing[]"></td>'
                 + '<td><input type="text" class="form-control" name="event_pricing[]"></td>'
                 + '<td><select class="form-control" name="event_pricing[]">'
                <?php 
                  $x = 1;
                  echo '+ \'<option value="0">Select</option>\'';
                  foreach ($limitToSubscriberArray as $key => $value) {
                    # code...
                    
                    echo '+ \'<option value="'.($x++).'">'.$value.'</option>\'';
                  }
                ?>
                 + '</select></td>'
                 + '</tr>';
    $('#tblEventPrice tbody').append(html_);
  });


  $('#tblEventPrice').on('click', '.btnRemEventPrice', function(){
      $(this).parent().parent().remove();
  });


  // setTimeout(function(){
    $('#menu_menuX2').hide();
    $('#menuX2').hide();

    $('#accordion').hide();
    // $('#menu_menuX3').hide();
    // $('#menuX3').hide();
  // }, 5000);
    $('#tblEventSpeakers').hide();
    $('#tblEventBreakouts').hide();
    $('#tblEventConfSponsor').hide();


  $('#inpCat').change(function(){
    var x = $(this).val();
    switch(x){
      case '30' : //PPm Conference
          $('#menu_menuX2').show();
          $('#menuX2').show();

          $('#accordion').show();
          $('#tblEventSpeakers').show();
          $('#tblEventBreakouts').show();
          $('#tblEventConfSponsor').show();
          // $('#menu_menuX3').show();
          // $('#menuX3').show();
      break;

      default : 
          $('#menu_menuX2').hide();
          $('#menuX2').hide();

          $('#accordion').hide();
          $('#tblEventSpeakers').hide();
          $('#tblEventBreakouts').hide();
          $('#tblEventConfSponsor').hide();
          // $('#menu_menuX3').hide();
          // $('#menuX3').hide();
      break;
    }

  });


  $('#btnSave').click(function(){
      var inpCat = $('#inpCat').val();
      var inpEventCode = $('#inpEventCode').val();
      var inpEventLoc = $('#inpEventLoc').val();
      var startDate = $('#startDate').val();
      var inpEventName = $('#inpEventName').val();
      // alert(startDate);

      if(inpCat !== '-' && inpEventCode !== '' && inpEventLoc !== '-' && startDate !== '' && inpEventName !== ''){
          var x = $('#frmEvents').serialize();
          console.log(x);
          $.ajax({
              type : 'post',
              url  : '<?php echo base_url(); ?>admin/events/save_events',
              data : x,
              beforeSend: function(){

              },
              success: function(res){
                console.log(res);
                $('#inpEventId').val(res);
                swal("Success", "Data has been saved", "success");
              }
          });
      }else{
          swal("Error", "Please fill-in required fields!", "error");
      }


  });


  var ctr_btnphoto_speaker = 0;
  $('#btnAddSpeakers').click(function(){
    ctr_btnphoto_speaker++;
    var html_ = '\
         <tr>\
            <td><button type="button" class="btn btn-danger btnRemSpeakers"><i class="fa fa-minus"></i></button></td>\
            <td><input type="text" class="form-control" name="speakers[]"></td>\
            <td><input type="text" class="form-control" name="speakers[]"></td>\
            <td><input type="text" class="form-control" name="speakers[]"></td>\
            <td>\
                <div class="row">\
                  <div class="col-md-12">\
                    <div class="input-group">\
                    <input type="text" class="form-control" name="speakers[]"  value="">\
                    <span class="input-group-btn">\
                    <button id="ppm-browse-image'+ ctr_btnphoto_speaker +'"  type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>\
                    </span>\
                    </div>\
                  </div>\
                </div>\
            </td>\
          </tr>\
    ';

    $('#tblEventSpeakers tbody').append(html_);
    PPMGallery.initButton('ppm-browse-image'+ ctr_btnphoto_speaker );
  });

  $('#tblEventSpeakers').on('click', '.btnRemSpeakers', function(){
      $(this).parent().parent().remove();
  });

  $('#btnAddBreakout').click(function(){
    var html_ = '\
            <tr>\
              <td><button type="button" class="btn btn-danger btnRemBreakout"><i class="fa fa-minus"></i></button></td>\
              <td><input type="text" class="form-control" name="breakout[]"></td>\
              <td><input type="text" class="form-control" name="breakout[]"></td>\
              <td><input type="text" class="form-control" name="breakout[]"></td>\
              <td><input type="text" class="form-control" name="breakout[]"></td>\
            </tr>\
    ';
    $('#tblEventBreakouts tbody').append(html_);
  });

  $('#tblEventBreakouts').on('click', '.btnRemBreakout', function(){
      $(this).parent().parent().remove();
  });




  var ctr_btnphoto_sponsor = 0;
  $('#btnAddconfSponsors').click(function(){
    ctr_btnphoto_sponsor++;
    var html_ = '\
         <tr>\
            <td><button type="button" class="btn btn-danger btnRemConfSponsors"><i class="fa fa-minus"></i></button></td>\
            <td><input type="text" class="form-control" name="confSponsors[]"></td>\
            <td><input type="text" class="form-control" name="confSponsors[]"></td>\
            <td><input type="text" class="form-control" name="confSponsors[]"></td>\
            <td>\
                <div class="row">\
                  <div class="col-md-12">\
                    <div class="input-group">\
                    <input type="text" class="form-control" name="confSponsors[]"  value="">\
                    <span class="input-group-btn">\
                    <button id="ppm-browse-image'+ ctr_btnphoto_sponsor +'"  type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>\
                    </span>\
                    </div>\
                  </div>\
                </div>\
            </td>\
          </tr>\
    ';

    $('#tblEventConfSponsor tbody').append(html_);
    PPMGallery.initButton('ppm-browse-image'+ ctr_btnphoto_sponsor );
  });

  $('#tblEventConfSponsor').on('click', '.btnRemConfSponsors', function(){
      $(this).parent().parent().remove();
  });

});

</script>

<script>


</script>

<style>
/*.cf:after { visibility: hidden; display: block; font-size: 0; content: " "; clear: both; height: 0; }
* html .cf { zoom: 1; }
*:first-child+html .cf { zoom: 1; }

html { margin: 0; padding: 0; }
body { font-size: 100%; margin: 0; padding: 1.75em; font-family: 'Helvetica Neue', Arial, sans-serif; }

h1 { font-size: 1.75em; margin: 0 0 0.6em 0; }

a { color: #2996cc; }
a:hover { text-decoration: none; }

p { line-height: 1.5em; }
.small { color: #666; font-size: 0.875em; }
.large { font-size: 1.25em; }*/

/**
 * Nestable
 */

.dd { position: relative; display: block; margin: 0; padding: 0; max-width: 600px; list-style: none; font-size: 13px; line-height: 20px; }

.dd-list { display: block; position: relative; margin: 0; padding: 0; list-style: none; }
.dd-list .dd-list { padding-left: 30px; }
.dd-collapsed .dd-list { display: none; }

.dd-item,
.dd-empty,
.dd-placeholder { display: block; position: relative; margin: 0; padding: 0; min-height: 20px; font-size: 13px; line-height: 20px; }

.dd-handle { display: block; height: 30px; margin: 5px 0; padding: 5px 10px; color: #333; text-decoration: none; font-weight: bold; border: 1px solid #ccc;
    background: #fafafa;
    background: -webkit-linear-gradient(top, #fafafa 0%, #eee 100%);
    background:    -moz-linear-gradient(top, #fafafa 0%, #eee 100%);
    background:         linear-gradient(top, #fafafa 0%, #eee 100%);
    -webkit-border-radius: 3px;
            border-radius: 3px;
    box-sizing: border-box; -moz-box-sizing: border-box;
}
.dd-handle:hover { color: #2ea8e5; background: #fff; }

.dd-item > button { display: block; position: relative; cursor: pointer; float: left; width: 25px; height: 20px; margin: 5px 0; padding: 0; text-indent: 100%; white-space: nowrap; overflow: hidden; border: 0; background: transparent; font-size: 12px; line-height: 1; text-align: center; font-weight: bold; }
.dd-item > button:before { content: '+'; display: block; position: absolute; width: 100%; text-align: center; text-indent: 0; }
.dd-item > button[data-action="collapse"]:before { content: '-'; }

.dd-placeholder,
.dd-empty { margin: 5px 0; padding: 0; min-height: 30px; background: #f2fbff; border: 1px dashed #b6bcbf; box-sizing: border-box; -moz-box-sizing: border-box; }
.dd-empty { border: 1px dashed #bbb; min-height: 100px; background-color: #e5e5e5;
    background-image: -webkit-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
                      -webkit-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
    background-image:    -moz-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
                         -moz-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
    background-image:         linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
                              linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
    background-size: 60px 60px;
    background-position: 0 0, 30px 30px;
}

.dd-dragel { position: absolute; pointer-events: none; z-index: 9999; }
.dd-dragel > .dd-item .dd-handle { margin-top: 0; }
.dd-dragel .dd-handle {
    -webkit-box-shadow: 2px 4px 6px 0 rgba(0,0,0,.1);
            box-shadow: 2px 4px 6px 0 rgba(0,0,0,.1);
}

/**
 * Nestable Extras
 */

.nestable-lists { display: block; clear: both; padding: 30px 0; width: 100%; border: 0; border-top: 2px solid #ddd; border-bottom: 2px solid #ddd; }

#nestable-menu { padding: 0; margin: 20px 0; }

#nestable-output,
#nestable2-output { width: 100%; height: 7em; font-size: 0.75em; line-height: 1.333333em; font-family: Consolas, monospace; padding: 5px; box-sizing: border-box; -moz-box-sizing: border-box; }

#nestable2 .dd-handle {
    color: #fff;
    border: 1px solid #999;
    background: #bbb;
    background: -webkit-linear-gradient(top, #bbb 0%, #999 100%);
    background:    -moz-linear-gradient(top, #bbb 0%, #999 100%);
    background:         linear-gradient(top, #bbb 0%, #999 100%);
}
#nestable2 .dd-handle:hover { background: #bbb; }
#nestable2 .dd-item > button:before { color: #fff; }

@media only screen and (min-width: 700px) {

    .dd { float: left; width: 48%; }
    .dd + .dd { margin-left: 2%; }

}

.dd-hover > .dd-handle { background: #2ea8e5 !important; }

/**
 * Nestable Draggable Handles
 */

.dd3-content { display: block; height: 30px; margin: 5px 0; padding: 5px 10px 5px 40px; color: #333; text-decoration: none; font-weight: bold; border: 1px solid #ccc;
    background: #fafafa;
    background: -webkit-linear-gradient(top, #fafafa 0%, #eee 100%);
    background:    -moz-linear-gradient(top, #fafafa 0%, #eee 100%);
    background:         linear-gradient(top, #fafafa 0%, #eee 100%);
    -webkit-border-radius: 3px;
            border-radius: 3px;
    box-sizing: border-box; -moz-box-sizing: border-box;
}
.dd3-content:hover { color: #2ea8e5; background: #fff; }

.dd-dragel > .dd3-item > .dd3-content { margin: 0; }

.dd3-item > button { margin-left: 30px; }

.dd3-handle { position: absolute; margin: 0; left: 0; top: 0; cursor: pointer; width: 30px; text-indent: 100%; white-space: nowrap; overflow: hidden;
    border: 1px solid #aaa;
    background: #ddd;
    background: -webkit-linear-gradient(top, #ddd 0%, #bbb 100%);
    background:    -moz-linear-gradient(top, #ddd 0%, #bbb 100%);
    background:         linear-gradient(top, #ddd 0%, #bbb 100%);
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
}
.dd3-handle:before { content: '≡'; display: block; position: absolute; left: 0; top: 3px; width: 100%; text-align: center; text-indent: 0; color: #fff; font-size: 20px; font-weight: normal; }
.dd3-handle:hover { background: #ddd; }


#divHead {
  background-color: #F6F6F6;
  width: 100%;
}

.stickme {
  position:sticky ;
  top :0px;
  position : -webkit-sticky;
  z-index: 10;
  padding:10px 0px;
}
</style>
<?php if(isset($upload)) echo $upload; ?> 