<?php

    $access = json_decode($_SESSION['admin_access']);

    $user_access = array();

    if (json_last_error() === JSON_ERROR_NONE) {
        foreach ($access->data as $key => $value) {
            $user_access[] = $value;
        }
    }

?>

<?php if(in_array(14, $user_access)) { ?>
    
<button id="show-categories" type="button" class="btn btn-info">Show Categories</button>

<div id="show-categories-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
          </button>
          <h4 class="modal-title"><font>Categories </font></h4>
        </div>
        <div class="modal-body">

            <div class="alert alert-info fade in" role="alert" style="margin-bottom: 30px !important;">
                <strong>YOU CANNOT RENAME A CATEGORY</strong> - You can only Add or Delete.
            </div>
                <div role="tabpanel" data-example-id="togglable-tabs">
                    <ul id="myTabPopup" class="nav nav-tabs bar_tabs" role="tablist">
                    
                    <li role="presentation" class="active"><a href="#tab_content4" id="list-popup-tab" role="tab" data-toggle="tab" aria-expanded="true"><i class="fa fa-list"></i> List</a>
                    </li>

                    <li role="presentation"><a href="#tab_content5" role="tab" id="add-popup-tab" data-toggle="tab" aria-expanded="false"><i class="fa fa-plus"></i> Add</a>
                    </li>

                    </ul>
                    <div id="myTabContentPop" class="tab-content">
                        <form id="categories-form">
                            <?php 
                                $section = array();
                                $section_edit = array(""=>"[Select]");
                                
                                foreach ($section_name as $key => $value) { 
                                    $section[] = $this->dashboard->data_collect['cat_sections'][$value]['title'];
                                    $section_edit[$value] = $this->dashboard->data_collect['cat_sections'][$value]['title'];
                            ?>
                                <input type="hidden" name="section[]" value="<?php echo $value; ?>" />
                            <?php } ?>
                        </form>
                        <div role="tabpanel" class="tab-pane fade active in" id="tab_content4" aria-labelledby="list-tab">  
                            <?php if(count($section_name) > 1) { ?>
                            <div id="section-search" class="row">
                                <div class="col-md-2 col-sm-12">
                                <label for="state">By Section:</label>
                                <?php
                                    if(!empty($section)) {
                                        echo $this->ppmsystemlib->createDropdown('cat-section', $section, '', FALSE, FALSE, TRUE);
                                    }
                                ?>
                                
                                </div>
                            </div>
                            <p>&nbsp;</p>
                            
                            <?php } ?>                        

                            <table id="categories-table" class="table table-striped table-hover text-left hide dt-responsive nowrap" data-page-length='10' cellspacing="0" width="100%"><!-- table-bordered  -->
                              <thead>
                                <tr>
                                    <th style="text-align:left!important; vertical-align: middle!important;">ID</th>
                                    <th style="text-align:left!important; vertical-align: middle!important;">Name</th>
                                    <th style="text-align:left!important; vertical-align: middle!important;">Section</th>
                                    <th style="text-align:left!important; vertical-align: middle!important;">Order</th>
                                    <th style="text-align:left!important; vertical-align: middle!important;"></th>                                  
                                </tr>
                              </thead>
                              <tbody>
                              </tbody>
                            </table>

                            <div id="cat-loader" class="progress-bar progress-bar-success hide" data-transitiongoal="100" aria-valuenow="0" style="height:2px;"></div>
                                <div class="clearfix"><p>&nbsp;</p></div>
                            <div id="cat-total-label" class="text-center"><p class="hide"></p></div>                            

                        </div>
                        
                        <div role="tabpanel" class="tab-pane fade" id="tab_content5" aria-labelledby="add-tab">
                            <p class="lead"><small class="text-info small">Compulsary fields are marked by an asterisk ( <i class="fa fa-asterisk text-danger small"></i> )</small></p>

                            <form id="cat-add-edit-form" class="form-horizontal form-label-left">
                                <input id="data-cat-id" type="hidden" name="id" value="" />

                                <div class="form-group">

                                    <label class="control-label col-md-3 col-sm-12 col-xs-12">Section: <i class="fa fa-asterisk text-danger"></i></label>
                                    <div class="col-md-5 col-sm-12 col-xs-12">
                                        <?php
                                            echo form_dropdown('section', $section_edit, $section_name[0], 'id="data-cat-section" class="form-control"');

                                        ?>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-12 col-xs-12">Name: <i class="fa fa-asterisk text-danger"></i></label>
                                    <div class="col-md-5 col-sm-12 col-xs-12">
                                        <input id="data-cat-name" name="name" type="text" class="form-control" placeholder="Name" value="" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-12 col-xs-12">Order By: <i class="fa fa-asterisk text-danger"></i></label>
                                    <div class="col-md-5 col-sm-12 col-xs-12">
                                        <input id="data-cat-order" name="order" type="text" class="form-control" placeholder="" value="" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-12 col-xs-12">Body: </label>
                                    <div class="col-md-5 col-sm-12 col-xs-12">
                                        <textarea id="data-cat-body" name="body" class="form-control" rows="5"></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-12 col-xs-12">Image: </label>
                                    <div class="col-md-5 col-sm-12 col-xs-12">
                                        <div class="input-group">
                                            <input id="data-cat-d1" type="text" name="d1" readonly="true" class="form-control">
                                            <span class="input-group-btn">
                                            <button type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
                                            </span>
                                        </div>
                                    </div>                          
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-12 col-xs-12"></label>
                                    <div class="col-md-5 col-sm-12 col-xs-12">
                                        <div class="input-group">
                                            <button id="cat-cancel-btn" type="button" class="btn btn-sm btn-warning" style="display: none;">Cancel Changes</button>
                                            <button id="cat-update-btn" type="button" class="btn btn-sm btn-success" style="display: none;">Update</button>
                                            <button id="cat-add-btn" type="button" class="btn btn-sm btn-success add-btn">Add</button>
                                        </div>
                                    </div>                          
                                </div> 

                            </form>
                        </div>

                    </div>
                </div>

        </div>
        <div class="modal-footer">          
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>

      </div>
    </div>
</div>

<script>

    $(document).ready(function(){

        var cancelBtn       = $('#cat-cancel-btn');
        var addBtn          = $('#cat-add-btn');
        var updateBtn       = $('#cat-update-btn');
        var searchTab       = $('#list-popup-tab');
        var addTab          = $('#add-popup-tab');

        var showCategoriesBtn = $('#show-categories');
        var _modal = $('#show-categories-modal');

        _modal.prependTo('#main-body');

        var masterList                  = null;
        var catForm                     = $('#categories-form');
        var catAddEditForm              = $('#cat-add-edit-form');
        var listTable                   = $('#categories-table');
        var loader                      = $('#cat-loader');
        var totalLabel                  = $('#cat-total-label p');
        var search                      = $('#section-search');
        var tempListHolder              = $('<div></div>');
        var currentDataTable            = null;
        var currentTable                = null;
        var tableId                     = '';
        var percentage                  = 0;
        var progressCtr                 = 0;
        var total                       = 0;
        var tempListStr                 = '';
        var selectedCategory            = 0;
        var fields = {
                        id:             $('#data-cat-id'),
                        section:        $('#data-cat-section'),
                        d1:             $('#data-cat-d1'),
                        sort:           $('#data-cat-order'),
                        name:           $('#data-cat-name'),
                        body:           $('#data-cat-body')
                    };        

        var contents = {
            tempFunc: null,
            init: function(){
                $('#show-categories').on('click', function(){_modal.modal({backdrop: "static"});});
                _modal.on('shown.bs.modal', function () {contents.getList();});
                _modal.on('hide.bs.modal', function () {
                    if(tableId!='') {
                        $('#' + tableId + '_wrapper').remove();
                        currentTable = null;
                        currentDataTable = null;
                    }

                    contents.resetForm();
                    contents.resetTab();
                });

                search.find('select#cat-section').unbind('change').bind('change', function(){
                    currentDataTable.columns( 2 )
                    .search( $(this).val() )
                    .draw();
                });

                addBtn.on('click', function(){
                    contents.submitForm();
                });

                updateBtn.bind('click', function(){
                    contents.submitForm();
                });                

                cancelBtn.on('click', function(){
                    contents.cancelUpdate();
                });

                fields.sort.on('change', function(){
                    var val = $(this).val();

                    if($.isNumeric(val)) {
                        if(parseInt(val) < 0) {
                            $(this).val(0);
                        }
                        else {
                            $(this).val(parseInt(val));
                        }
                    }
                });                           
            },
            getList: function(){

                totalLabel.removeClass('hide');
                totalLabel.html('Fetching Records... <i class="fa fa-spinner fa-spin"></i>');
                
                var request = $.ajax({
                  url: "<?php echo base_url(); ?>admin/get_categories",
                  method: "POST",
                  data: catForm.serialize(),
                  dataType: 'json'
                });
                 
                request.done(function( data ) {
                    
                    if(data!=null) {
                        contents.refreshList();

                        masterList = data;//data.result;
                        total = masterList.length;

                        if(total>0) {
                            totalLabel.removeClass('hide');

                            tempListHolder.html('');

                            loader.addClass('hide');
                            totalLabel.html('Updating list with ' + numeral(total).format('0,0') + ' records. <i class="fa fa-spinner fa-spin"></i>');

                            contents.processList();
                        }
                        else {
                            contents.updateLoader(false);
                            currentDataTable = currentTable.DataTable();
                        }

                        data = null;
                    }
                });

                request.error(function(_data){
                    if(_data.readyState == 4 && _data.status == 200) {//Session expired and redirected
                        swal({
                            title: "Session Expired!",
                            text: "Reloading page.",
                            icon: "warning",
                            dangerMode: false,
                        })
                        .then(ok => {
                            if (ok) {
                                window.location.href = '<?php echo base_url();?>admin/login';
                            }
                        });
                    }
                });
            },
            processList: function(){

                setTimeout(function(){
                    
                    var columns = [];

                    columns = [
                                { data: 'id' },
                                { data: 'name' },
                                { data: 'area' },
                                { data: 'sort' },
                                { data: 'action' }                  
                            ];


                    currentDataTable = currentTable.DataTable( {
                        data: masterList,
                        columns: columns,
                        "scrollY": (masterList.length >=10) ? 320 : false,
                        "scrollX": true,
                        "order": [[ 2, "asc" ]]
                    } );

                    currentTable.children('tbody').on('click', 'td.col-action', function () {
                        contents.showActionPanel($(this));
                    });

                    contents.initListAction();

                    contents.updateLoader(false);

                    currentDataTable.on( 'draw.dt', function () {
                        contents.initListAction();
                    });

                    search.find('select#cat-section').trigger('change');           

                }, 1);

            },
            initListAction: function(){
                $.each($(currentTable).find('tr'), function(k,v){
                    var id = $(v).children('td:first').text();

                    if($.isNumeric(id)) {
                        var td = $(v).children('td:nth-last-child(1)');
                        td.addClass('col-action').attr({'data-id':id});
                    }
                });
            },
            refreshList: function(){
                if(tableId!='') {
                    $('#' + tableId + '_wrapper').remove();
                    currentTable = null;
                    currentDataTable = null;
                }

                tableId = contents.generateID();

                var tempTable = null;

                tempTable = listTable.clone().removeClass('hide').attr({id:tableId});

                tempTable.insertAfter(listTable);

                currentTable = $('#' + tableId);

                tempTable = null;
            },
            updateResultsCount: function(){
                var str = '';

                if($('tr.row-item').length>0){

                    str = '('+ numeral($('tr.row-item').length).format('0,0')+' records found)';
                }
                else {
                    str = '';
                }

                resultsLabel.text(str);
            },
            checkDateTime: function(dateTimeStr){
                var dateTime = dateTimeStr.split(' ');

                if(dateTime.length > 1) {

                    if(dateTime[1]!='00:00:00') {
                        return moment(dateTimeStr).format('D/MM/YYYY h:mm:ss A');
                    }
                    else {
                        return moment(dateTimeStr).format('D/MM/YYYY');
                    }
                }

                return moment(dateTimeStr).format('D/MM/YYYY');
            },
            showActionPanel: function(td){
                var col         = $(td);
                var id          = col.data().id;
                var edit        = col.find('ul li ul li a.edit');
                var deleteLnk   = col.find('ul li ul li a.delete');

                col.find('ul li ul li a label.action-id').text(id);

                edit.unbind('click').bind('click', function(){contents.requestByAction({'id':id, 'action':$(this).attr('class'), 'section':$(this).data('section')})});
                
                deleteLnk.unbind('click').bind('click', function(){contents.requestByAction({'id':id, 'action':$(this).attr('class')})});

                col.parents('tr').attr({id:'row-'+id})

                edit = deleteLnk = null;
            },
            requestByAction: function(data){

                if(data!=null) {

                    contents.tempFunc = function(data) {
                        var request = $.ajax({
                          url: "<?php echo base_url(); ?>admin/category/action",
                          method: "POST",
                          data: data,
                          dataType: 'json'
                        });

                        request.done(function( _data ) {
                            if(_data!=null) {

                                if(data.action == 'edit') {
                                    _data.id = data.id;
                                    //console.log(_data);
                                    contents.populateForm(_data);
                                }
                                else if(data.action == 'delete') {
                                    if(_data.hasOwnProperty('success')) {
                                        contents.resetTab();
                                        currentDataTable.row('#row-'+data.id).remove().draw( false );
                                        swal({title:"Success!",text:"Deleted item with ID: " + data.id,icon:'success',button:false, timer:3000});

                                        contents.reloadPage();
                                    }
                                    else {
                                        swal({title:"Deleted Failed!",text:"No record was found.",icon:'error',button:false, timer:3000});
                                    }
                                }

                                _data = null;
                            }
                        });

                        request.error(function(_data){
                            if(_data.readyState == 4 && _data.status == 200) {//Session expired and redirected
                                swal({
                                    title: "Session Expired!",
                                    text: "Reloading page.",
                                    icon: "warning",
                                    dangerMode: false,
                                })
                                .then(willDelete => {
                                    if (willDelete) {
                                        window.location.href = '<?php echo base_url();?>admin/login';
                                    }
                                });
                            }
                        });
                    }

                    var showAddEditTab = false;

                    if(data.action == 'edit') {
                        showAddEditTab = true;
                        addTab.html('<i class="fa fa-pencil"></i> ' + data.action.charAt(0).toUpperCase() + data.action.substr(1));
                    }
                    else if(data.action == 'delete') {
                        swal({
                            title: "Delete Item",
                            text: "Are you sure?",
                            icon: "warning",
                            buttons: ["Cancel", true],
                            dangerMode: true,
                            })
                            .then(willDelete => {
                            
                                if(willDelete) {
                                    contents.tempFunc.call(this, data);
                                }
                            
                            }); 
                    }

                    if(showAddEditTab) {
                        if(contents.tempFunc!=null) contents.tempFunc.call(this, data);
                    }
                }
            },
            updateLoader: function(show){
                if(show) {
                    var percentage = progressCtr / total;

                    if(progressCtr % 20 == 0) loader.css({width: (percentage * 100) + '%'});

                    progressCtr++;

                    totalLabel.text('Fetching ' + numeral(progressCtr).format('0,0') + ' of ' + numeral(total).format('0,0') + ' records');
                }
                else {
                    progressCtr = total = 0;

                    loader.css({width: '0%'}).addClass('hide');
                    totalLabel.text('').addClass('hide');
                }
            },
            generateID: function() {
              var charSet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
              var charCount = 20;
              var charSetSize = charSet.length;
              var id = '';

              for (var i = 1; i <= charCount; i++) {
                  var randPos = Math.floor(Math.random() * charSetSize);
                  id += charSet[randPos];
              }

              return id;
            },
            resetForm: function(){

                $.each(fields, function(k,v){
                    if(v.length>0) {
                        v.val('');
                        if(k == 'name') fields.name.attr({'readonly':false});
                    }
                });
            },
            populateForm: function(data){

                if(data!=null) {

                    this.resetForm();

                    setTimeout(function(){
                        $.each(data, function(k,v){
                            if(fields.hasOwnProperty(k) && fields[k].length>0) {
                                fields[k].val(v);

                                if(k == 'name') fields.name.attr({'readonly':true});
                            }
                        });

                        contents.showUpdateButton();

                        if(addTab!=null) addTab.click();                        
                    }, 50);
                }
            },
            submitForm: function(){

                var isEdit = $.isNumeric(fields.id.val());

                swal({
                    title: (isEdit)?"Update Item" : "Add Item",
                    text: "Are you sure?",
                    icon: "warning",
                    buttons: ["Cancel", true],
                    dangerMode: false,
                })
                .then(willSubmit => {
                    if (willSubmit) {
                        var request = $.ajax({
                          url: "<?php echo base_url(); ?>admin/category/add_update",
                          method: "POST",
                          data: catAddEditForm.serialize(),
                          dataType: 'json'
                        });

                        request.done(function( res ) {
                            if(res!=null) {

                                if(isEdit) {
                                    if(res.hasOwnProperty('error')) {
                                        var errMsg = '';

                                        $.each(res.error, function(k, v){
                                            errMsg += '* ' + v + '\n';
                                        });

                                        new PNotify({
                                            title: 'Invalid or Required!',
                                            text: errMsg,
                                            type: 'error',
                                            styling: 'bootstrap3'
                                        });

                                        swal({title:"Update Failed!",text:"Some fields are required (*) or have invalid values",icon:'error',button:false, timer:3000});
                                    }
                                    else {
                                        contents.applyUpdateChanges(res);

                                        swal({title:"Updated!",text:'Changes have been saved.',icon:'success',button:false, timer:3000});
                                    }
                                }
                                else {
                                    if(res.hasOwnProperty('error')) {
                                        var errMsg = '';

                                        $.each(res.error, function(k, v){
                                            errMsg += '* ' + v + '\n';
                                        });

                                        new PNotify({
                                            title: 'Invalid or Required!',
                                            text: errMsg,
                                            type: 'error',
                                            styling: 'bootstrap3'
                                        });

                                        swal({title:"Add Failed!",text:"Some fields are required (*) or have invalid values",icon:'error',button:false, timer:3000});
                                    }
                                    else {
                                        fields.id.val(res.id);                                        
                                        contents.showUpdateButton();
                                        addTab.html('<i class="fa fa-pencil"></i> Edit');
                                        swal({title:"Success!",text:"Added item with id: " + res.id,icon:'success',button:false, timer:3000});
                                        contents.reloadPage();
                                    }
                                }
                            }
                        });

                        request.error(function(_data){
                            if(_data.readyState == 4 && _data.status == 200) {//Session expired and redirected

                                swal({
                                    title: "Session Expired!",
                                    text: "Reloading page.",
                                    icon: "warning",
                                    dangerMode: false,
                                })
                                .then(ok => {
                                    if (ok) {
                                        window.location.href = '<?php echo base_url();?>admin/login';
                                    }
                                });
                            }
                        });
                    }
                }); 

            },
            applyUpdateChanges: function(data){

                if(data!=null && data.hasOwnProperty('id')) {

                    var row = $('#row-'+data.id);

                    if(row.length > 0) {
                        row.children('td')[1].innerHTML = data.name;
                        row.children('td')[2].innerHTML = data.area;
                        row.children('td')[3].innerHTML = data.sort;
                    }
                }
            },
            addNewItem: function(res) {
                if(res!=null) {

                    var data = {
                                  "id":res.id,
                                  "name":res.name,
                                  "area":res.area,
                                  "sort":res.sort,
                                  "action":res.action
                                };      

                    currentDataTable.row.add(data).node().id = 'row-'+res.id;
                    currentDataTable.page( 'next' ).draw( false );
                    currentDataTable.page( 'previous' ).draw( false );
                }
            },
            showUpdateButton: function(){
                cancelBtn.show();
                updateBtn.show();
                addBtn.hide();  
            },
            showAddButton: function(){
                cancelBtn.hide();
                updateBtn.hide();
                addBtn.show();  
            },
            cancelUpdate: function(){

                swal({
                    title: "Cancel Changes",
                    text: "Are you sure?",
                    icon: "warning",
                    buttons: ["Cancel", true],
                    dangerMode: false,
                })
                .then(willCancel => {
                    if (willCancel) {
                        contents.resetTab();
                    }
                });
            },
            resetTab: function(){
                addTab.html('<i class="fa fa-plus"></i> Add');
                contents.resetForm();
                contents.showAddButton();
                searchTab.trigger('click');
            },
            reloadPage: function(){
                setTimeout(function(){
                    swal({title:"Page Reload!",text:"The Page will reload to see changes.",icon:'info',button:false, timer:3000});

                    setTimeout(function(){
                        window.location.href = window.location.href;
                    },3000);
                },1000);                
            }

        };

        contents.init();        

    });

</script>

<?php } else { ?>
<p><?php echo APP_TITLE; ?></p>
<?php } ?>    
