<!-- page content -->
<div class="right_col" role="main" style="min-height: 100vh;">

  <div class="row">
    <div class="col-md-12">
      <h1><i class="fa fa-shopping-cart"></i> Products</h1>
      <button class="btn btn-primary" type="button" id="btnAddNew" > <i class="fa fa-plus"></i> Add New </button>
      <hr/>
      <div class="spacer20"></div>
      <div class="row">
        <div class="col-md-12">
          <!-- START : LIST TABLE -->
          <div class="row">
              <div class="col-md-3">
                  <div class="form-group">
                      <label>Show</label>
                      <?php
                      // print_r($product_list);
                      ?>
                      <select id="selProdCat" class="form-control">
                        <option value="">[ select ]</option>
                        <option value="all">List All</option>
                        <?php
                          foreach($product_catlist['result'] as $p_catlist){
                            echo '<option value="'.$p_catlist['id'].'">'.$p_catlist['name'].'</option>';
                          }
                        ?>
                      </select>
                  </div>
              </div>
          </div>
          <!-- <?php print_r($product_list); ?> -->
          <table class="table table-striped" id="tblProductList">
            <thead>
              <tr>
                <td>Code</td>
                <td>Product Name</td>
                <td>Product of the Month</td>
                <td>Description</td>
                <td>Action</td>
              </tr>
            </thead>
            <tbody><?php
              if(isset($product_list)){
                foreach ($product_list['result'] as $p_list) {
                  # code...
                        echo '
                        <tr>
                          <td style="width: 10%;">'.$p_list['code'].'</td>
                          <td style="width: 20%;">'.$p_list['name'].'</td>
                          <td style="width: 20%;">'.$p_list['productOfTheMonth'].'</td>
                          <td style="width: 30%;">'.$p_list['shortdesc'].'</td>
                          <td>
                              <button type="button" class="btn btn-primary btnEditProd"><i class="fa fa-pencil"></i></button>
                              <button type="button" class="btn btn-danger btnDeleteProd"><i class="fa fa-trash"></i></button>
                          </td>
                        </tr>
                        ';
                }
              }

            ?>
              <!-- <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr> -->
            </tbody>
          </table>
          <!-- END : LIST TABLE -->
        </div>
      </div>
      <?php
        // print_r($list_array);
      ?>
    </div>

  </div>

	<div class="clearfix"></div>
</div>

<div id="member-logs" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

			<div class="modal-header">
				<h4 class="modal-title">Title</h4>
			</div>
			<div class="modal-body">

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>

		</div>
	</div>
</div>

<!-- /page content -->

<style>
.spacer10 {
  clear: both;
  width: 100%;
  height: 10px;
}
.spacer20 {
  clear: both;
  width: 100%;
  height: 20px;
}
.spacer30 {
  clear: both;
  width: 100%;
  height: 30px;
}

#tblProductList td:nth-child(1){
  width: 10%;
}

#tblProductList td:nth-child(2),
#tblProductList td:nth-child(3){
  width: 20%;
}

#tblProductList td:nth-child(4){
  width: 30%;
}

</style>

<script>
  var tblProductList = "";

  $(document).ready(function(){
    var baseurl = '<?php echo base_url(); ?>';
    
    $('#chkAllItems').iCheck({
      checkboxClass: 'icheckbox_square',
      radioClass: 'iradio_square',
      increaseArea: '20%' // optional
    });

    var tbl_data = $('#tblProductList').DataTable({
      "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
      "columnDefs": [{ "orderable": false, "targets": [ 4] }],
      "order": [[ 1, "asc" ]],
      "scrollY": "400px",
      "scrollCollapse": true,
    });

    $('#selProdCat').change(function(){
      var s = $(this).val();
      if(s != ''){
        // tblProdData.column(0).search(s).draw();
        $.ajax({
          type: 'post',
          url  : '<?php echo base_url(); ?>admin/products/get_products',
          dataType : 'json',
          data : {
            'catid' : s
          },
          success: function(res){
            console.log(res);
            // add datatable routine here....
            var len = res.result.length;
            if(typeof len !== 'undefined'){
              tbl_data.clear().draw();
              for(var i=0; i< len; i++){
                var obj = res.result[i];
                tbl_data.row.add( [
                    obj.code,
                    obj.name,
                    obj.productOfTheMonth,
                    obj.shortdesc,
                    '<button type="button" class="btn btn-primary btnEditProd"><i class="fa fa-pencil"></i></button>\
                     <button type="button" class="btn btn-danger btnDeleteProd"><i class="fa fa-trash"></i></button>\
                    '
                ] ).draw( false );
              }
            }
          }
        });
        
      }else{
        window.location.reload();
      } 
    });

    $('#tblProductList').on('click', '.btnEditProd', function(){
      alert('edit')
    });

    $('#tblProductList').on('click', '.btnDeleteProd', function(){
      alert('delete')
    });
    
    $('#btnAddNew').click(function(){
      // $.ajax({
      //   type: 'post',
      //   url : baseurl + 'admin/pages/test',
      //   success:function(res){
      //     console.log(res);
      //   }
      // });
      window.location = baseurl + 'admin/products/add';
    });

  });
</script>
