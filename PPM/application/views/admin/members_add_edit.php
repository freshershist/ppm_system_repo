<?php

$member 			= NULL;
$memberCategoryArr 	= NULL;
$tasks 				= NULL;
$isEdit 			= FALSE;
$exclude 			= TRUE;
$membershipType     = NULL;
$hideFieldFromMember = FALSE;
$hideShowCompulsory = FALSE;
$memberId 			= NULL;

if(isset($member_data['member']) && is_array($member_data['member'])) {
	if(!empty($member_data['member'])) {
		$member = $member_data['member'][0];
		$memberId = $member['id'];
		$membershipType = intval($member['membershipType']);
		$hideFieldFromMember = ($membershipType === 6 OR ($membershipType >= 8 && $membershipType <= 12)) ? TRUE : FALSE;
		$hideShowCompulsory = ($membershipType === 6 OR $membershipType === 4 OR $membershipType === 5 OR ($membershipType >= 8 && $membershipType <= 12)) ? TRUE : FALSE;
		$isEdit = true;
	}
}

if($isEdit && is_array($member_data['memberCategory'])) {
	if(!empty($member_data['memberCategory'])) {
		$memberCategoryArr = $member_data['memberCategory'];
	}
}

if($isEdit && is_array($member_data['tasks'])) {
	if(!empty($member_data['tasks'])) {
		$tasks = $member_data['tasks'];
	}
}

// echo '<pre>';
// print_r($member_data);
// die();

?>

<style type="text/css">
	.form-horizontal .to_do li:hover {
		cursor: pointer;
	}

	.drop-here {
		position: absolute;
		z-index: 1;
		top: calc(50% - 10px);
		text-align: center;
		width: 100%;		
	}

	#categories-selected.to_do li {
	    background: #1abb9c!important;
	    color: #fff!important;
	}
</style>

<p class="lead"><?php echo ($isEdit)?'Update Member / Client':'Add Member / Client' ?> | <small class="text-info small">Compulsary fields are marked by an asterisk ( <i class="fa fa-asterisk text-danger small"></i> )</small></p>

<form id="members-add-form" class="form-horizontal form-label-left">
	<input id="member-id" type="hidden" name="id" value="<?php if($isEdit) echo $memberId; ?>">
	<!-- start accordion -->
	<div class="accordion" id="accordion-add-edit" role="tablist" aria-multiselectable="true">

		<!-- General -->	
		<div class="panel">
			<a class="panel-heading" role="tab" id="members-add-edit_1" data-toggle="collapse" data-parent="#accordion-add-edit" href="#add-edit_1" aria-expanded="false" aria-controls="add-edit_1">
				<h4 class="panel-title">General</h4>
			</a>
			<div id="add-edit_1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="members-add-edit_1">
				<div class="panel-body">
					<div class="col-md-12">

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Client Type: <i class="fa fa-asterisk text-danger"></i></label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								
								<?php 

								if(isset($membershipTypeDictionary)) {
									echo $this->ppmsystemlib->createDropdown('membershipType', $membershipTypeDictionary, $member['membershipType'], FALSE, $isEdit);
								}

								?>

								
							</div>
						</div>

						<?php if(!$exclude) { ?>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Partner Given Access:</label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<?php 

								if(isset($users)) {
									echo $this->ppmsystemlib->createDropdown('partnerID', $users, $member['partnerID'], FALSE, $isEdit);
								}

								?>

							</div>
						</div>
						<?php } ?>

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Company: <i class="fa fa-asterisk text-danger"></i></label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<input id="company" name="company" type="text" class="form-control" placeholder="" value="<?php if($member!=NULL) echo $member['company']; ?>" />
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Category:</label>
							<div class="col-md-5 col-sm-12 col-xs-12" >
								<div class="checkbox" style="padding-bottom:5px;">
					                <label><span class="badge bg-default">Drag items to Selected Categories:</span></label>
								</div>
								<div style="max-height: 285px; overflow-y: scroll; border-radius: 5px;border: 1px solid #f3f3f3;">

								<ul id="categories-available" class="to_do">
								<?php 

								$members_categories_selected_arr = array();

								if(isset($members_categories)) {
									foreach ($members_categories as $value) {

										if($isEdit && !empty($memberCategoryArr) && in_array($value['id'], $memberCategoryArr)) {
											$members_categories_selected_arr[] = array('id'=>$value['id'], 'name'=>$value['name']);
										}
										else {
								?>
									<li id="cat-<?php echo $value['id']; ?>" data-catid="<?php echo $value['id']; ?>" style="margin:4px;" draggable="true">
										<p><?php echo $value['name']; ?></p>
									</li>
								<?php
										}

									}
								}

								?>
								</ul>

								</div>
	                          
							</div>
							<div class="col-md-4 col-sm-12 col-xs-12" >
								<div class="checkbox" style="padding-bottom:5px;">
									<label><span class="badge bg-green">Selected Categories:</span></label>
								</div>
								<div style="max-height: 285px; overflow-y: auto; border-radius: 5px;border: 1px solid #f3f3f3;">
									<h2 class="drop-here"><i>Drop here ...</i></h2>
									<ul id="categories-selected" class="to_do" style="position:relative; z-index:2; min-height: 268px;">
									<?php
										if(!empty($members_categories_selected_arr)) {
											foreach ($members_categories_selected_arr as $value) {
												
									?>
										<li id="cat-<?php echo $value['id']; ?>" data-catid="<?php echo $value['id']; ?>" style="margin:4px;" draggable="true">
											<p><?php echo $value['name']; ?></p>
										</li>
									<?php
											}

										}
									?>
									</ul>
								</div>
								<div id="cat-selected" class="hide"></div>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">General Accounts Contact: <i id="generalCompulsoryFlag1" <?php if($hideShowCompulsory) echo 'style="display:none;"'; ?> class="fa fa-asterisk text-danger"></i></label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<input id="contactName" name="contactName" type="text" class="form-control" placeholder="" value="<?php if($isEdit){ echo $member['contactName']; } ?>"/>
							</div>
						</div>						

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">General Accounts Email: <i id="generalCompulsoryFlag2" <?php if($hideShowCompulsory) echo 'style="display:none;"'; ?> class="fa fa-asterisk text-danger"></i></label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<input name="emailAddress" type="email" class="form-control" placeholder="" value="<?php if($isEdit){ echo $member['emailAddress']; } ?>" />
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Username: <i id="usernameCompulsoryFlag" class="fa fa-asterisk text-danger"></i></label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<input name="username" type="text" class="form-control" placeholder="" value="<?php if($isEdit){ echo $member['username']; } ?>"/>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Password: <i id="passwordCompulsoryFlag" class="fa fa-asterisk text-danger"></i></label>
							<div class="col-md-5 col-sm-12 col-xs-12 input-group">
								<input id="password-input" name="password" type="password" class="form-control" style="margin-left: 10px;width: 97%;" placeholder="" value="<?php if($isEdit){ echo $member['password']; } ?>"/>
								<span class="input-group-btn">
                                	<button id="password-btn" type="button" class="btn btn-success" data-type="show">Show</button>
                                </span>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Contact Number 1:<i id="contactNumberCompulsoryFlag" class="fa fa-asterisk text-danger"></i></label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<input id="contactNumber" name="contactNumber" type="text" class="form-control"  placeholder="" value="<?php if($isEdit){ echo $member['contactNumber']; } ?>"/>
							</div>
						</div>						

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Contact Number 2:</label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<input name="contactNumber2" type="text" class="form-control" placeholder="" value="<?php if($isEdit){ echo $member['contactNumber2']; } ?>" />
							</div>
						</div>	

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Contact Number 3:</label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<input name="contactNumber3" type="text" class="form-control" placeholder="" value="<?php if($isEdit){ echo $member['contactNumber3']; } ?>" />
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Fax 1:</label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<input name="fax" type="text" class="form-control" placeholder="(xx) xxxx-xxxx" value="<?php if($isEdit){ echo $member['fax']; } ?>"/>
							</div>
						</div>							

						<?php if(!$exclude) { ?>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Fax 2:</label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<input name="fax2" type="text" class="form-control" placeholder="(xx) xxxx-xxxx" value="<?php if($isEdit){ echo $member['fax2']; } ?>" />
							</div>
						</div>
						<?php } ?>

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Mobile:</label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<input name="mobile" type="text" class="form-control" placeholder="" value="<?php if($isEdit){ echo $member['mobile']; } ?>" />
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Postal Address 1:</label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<input name="address" type="text" class="form-control" placeholder="" value="<?php if($isEdit){ echo $member['address']; } ?>" />
							</div>
						</div>	

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Postal Address 2:</label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<input name="address2" type="text" class="form-control" placeholder="" value="<?php if($isEdit){ echo $member['address2']; } ?>"/>
							</div>
						</div>	

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Suburb:</label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<input name="suburb" type="text" class="form-control" placeholder="" value="<?php if($isEdit){ echo $member['suburb']; } ?>" />
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Postcode:</label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<input name="postcode" type="text" class="form-control" placeholder="" value="<?php if($isEdit){ echo $member['postcode']; } ?>" />
							</div>
						</div>

						<!-- State -->

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">State: <i class="fa fa-asterisk text-danger"></i></label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<select id="state-add-edit" name="state" class="select2_multiple form-control">
								<option value="">[Select]</option>
								<?php 

									if(isset($state)) {
										foreach ($state as $value) {

											if(is_array($value)) {

												$opt_v = $value['id'];

												foreach ($value as $k => $v) {
													if($k === 'overrideDropdownValue' && !empty($v)) {
														$opt_v = $v;
														break;
													}
												}

												$selected = ($isEdit && $opt_v === $member['state']) ? ' selected ' : '';
												
												echo '<option value="' . $opt_v .'" ' . $selected . '>' . $value['name'] . '</option>';	
											}
										}
									}

								?>
								</select>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-12 state-subregion" style="display:none;">	
								<?php
									if(isset($state_subregion)) {
										echo $this->ppmsystemlib->createDropdown('stateSubRegion', $state_subregion, $member['stateSubRegion'], TRUE, $isEdit, TRUE, 'Sub Region', 'state-subregion');
									}

								?>
							</div>
						</div>
						<!-- State -->

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Business Address 1:</label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<input name="businessAddress1" type="text" class="form-control" placeholder="" value="<?php if($isEdit){ echo $member['businessAddress1']; } ?>" />
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Business Address 2:</label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<input name="businessAddress2" type="text" class="form-control" placeholder="" value="<?php if($isEdit){ echo $member['businessAddress2']; } ?>" />
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Business Suburb:</label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<input name="businessSuburb" type="text" class="form-control" placeholder="" value="<?php if($isEdit){ echo $member['businessSuburb']; } ?>" />
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Business Postcode:</label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<input name="businessPostcode" type="text" class="form-control" placeholder="" value="<?php if($isEdit){ echo $member['businessPostCode']; } ?>" />
							</div>
						</div>

						<!-- Business State -->

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Business State:</label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<select id="businessState" name="businessState" class="form-control">

								<option value="">[Select]</option>

								<?php 

									if(isset($state)) {
										$selected = '';

										foreach ($state as $value) {

											if(is_array($value)) {

												$opt_v = $value['id'];

												foreach ($value as $k => $v) {
													if($k === 'overrideDropdownValue' && !empty($v)) {
														$opt_v = $v;
														break;
													}
												}

												if($isEdit && intval($opt_v) === intval($member['businessState'])) {
													$selected = 'selected';
												}
												else {
													$selected = '';
												}
												
												echo '<option ' . $selected . ' value="' . $opt_v .'">' . $value['name'] . '</option>';	
											}
										}
									}

								?>
								</select>
							</div>
						</div>
						<!-- Business State -->	

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Website:</label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<div class="input-group input-group-sm">
									<label class="input-group-addon" for="website">http://</label>
									<input name="website" type="text" class="form-control" id="website" placeholder="" value="<?php if($isEdit){ echo $member['website']; } ?>" />
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Last Date Contact Details Updated:</label>
							<div class="col-md-5 col-sm-12 col-xs-12">
						        <div class="input-group date" id="last-updated-date">
						            <input name="lastUpdatedDate" type="text" class="form-control date-time" placeholder="dd/mm/yyyy" value="<?php if($isEdit){ echo $this->ppmsystemlib->check_date_time($member['lastUpdatedDate']); } ?>" />
						            <span class="input-group-addon">
						            <span class="glyphicon glyphicon-calendar"></span>
						            </span>
						        </div>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-12">
								<?php 

								if(isset($users)) {
									echo $this->ppmsystemlib->createDropdown('lastUpdatedUser', $users, $member['lastUpdatedUser'], FALSE, $isEdit);
								}

								?>


							</div>
						</div>

						<div id="linkedmemberidcontainer" class="form-group" <?php if($isEdit && $membershipType != 5) echo 'style="display:none;"'; ?> >
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Linked Member ID:</label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<input name="linkedmemberid" type="text" class="form-control" placeholder="" value="<?php if($isEdit){ echo $member['linkedmemberid']; } ?>" />
							</div>
						</div>	

						<?php 
							$howdidtheyhear_arr = array();

							foreach ($howdidtheyhear as $value) {
								$howdidtheyhear_arr[$value] = $value;
							}

							$howdidtheyhear = '';
							$is_other = FALSE;

							if($isEdit) {
								if(!in_array($member['howdidtheyhear'], $howdidtheyhear_arr) && !empty($member['howdidtheyhear'])) {
									$howdidtheyhear = 'Other';
									$is_other = TRUE;
								}
								else {
									if($member['howdidtheyhear'] === 'Other') $is_other = TRUE;
									$howdidtheyhear = $member['howdidtheyhear'];
									$member['howdidtheyhear'] = '';
								}
							}

						?>

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">How did they hear about us? <i id="howdidtheyhearcompulsory" class="fa fa-asterisk text-danger" <?php if(!$is_other) echo 'style="display:none;"'; ?>></i></label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<?php
									echo $this->ppmsystemlib->createDropdown('howdidtheyhear', $howdidtheyhear_arr, $howdidtheyhear, FALSE, $isEdit, TRUE);
								?>

							</div>
							<div class="col-md-4 col-sm-4 col-xs-12">
								<input id="howdidtheyhearother" name="howdidtheyhearother" type="text" class="form-control" placeholder="Reason" <?php if(!$is_other) { ?>style="display:none;" <?php } ?> value="<?php if($isEdit){echo $member['howdidtheyhear'];} ?>"/>
							</div>
						</div>

						<?php if(!$exclude) { ?>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Logo:</label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<input name="logo" type="text" class="form-control" placeholder="" value="<?php if($isEdit){echo $member['logo'];} ?>"/>
							</div>
							<div class="col-md-4 col-sm-12 col-xs-12">
								<button type="button" class="btn btn-sm btn-primary">Browse</button>
							</div>
						</div>											
						<?php } ?>

						<div id="statuscontainer" class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Client Status <i id="generalCompulsoryFlag6" class="fa fa-asterisk text-danger"></i></label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<?php
									echo $this->ppmsystemlib->createDropdown('memberStatus', $member_status, $member['memberStatus'], TRUE, $isEdit);
								?>
							</div>
						</div>

						<div id="everyonebutgeneral2" class="form-group" <?php if($hideFieldFromMember) echo 'style="display:none;"'; ?> >
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Commencement Date: <i id="generalCompulsoryFlag5" class="fa fa-asterisk text-danger"></i></label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<div class="input-group" id="membershipCommencementDate">
						            <input name="membershipCommencementDate" type="text" class="form-control" placeholder="dd/mm/yyyy" value="<?php if($isEdit){echo $this->ppmsystemlib->check_date_time($member['membershipCommencementDate']);} ?>" />
						            <span class="input-group-addon">
						            <span class="glyphicon glyphicon-calendar"></span>
						            </span>
						        </div>
							</div>
						</div>						

						<div id="everyonebutgeneral3" class="form-group" <?php if($hideFieldFromMember) echo 'style="display:none;"'; ?> >
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Expires: <i id="expirescompulsory" class="fa fa-asterisk text-danger"></i></label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<div class="input-group" id="membershipExpires">
						            <input name="membershipExpires" type="text" class="form-control" placeholder="dd/mm/yyyy" value="<?php if($isEdit){echo $this->ppmsystemlib->check_date_time($member['membershipExpires']);} ?>" />
						            <span class="input-group-addon">
						            <span class="glyphicon glyphicon-calendar"></span>
						            </span>
						        </div>
							</div>
						</div>

						<?php 

							$removedReason = array("Don't have time to read","Don't have PM Department","Unknown","I don't remember subscribing to your database", "Other");

							$removedReason_arr = array();

							foreach ($removedReason as $value) {
								$removedReason_arr[$value] = $value;
							}

							$removedReason = '';
							$is_other = FALSE;

							if($isEdit) {
								if(!in_array($member['removedReason'], $removedReason_arr) && !empty($member['removedReason'])) {
									$removedReason = 'Other';
									$is_other = TRUE;
								}
								else {
									if($member['removedReason'] === 'Other') $is_other = TRUE;
									$removedReason = $member['removedReason'];
									$member['removedReason'] = '';
								}
							}

						?>

						<div id="removedcontainer" class="form-group" <?php if($hideFieldFromMember) echo 'style="display:none;"'; ?> >
							<div class="row">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Date Removed: <i class="removedReasonOthercompulsory fa fa-asterisk text-danger" <?php if(!$is_other) echo 'style="display:none;"'; ?>></i></label>
								<div class="col-md-5 col-sm-12 col-xs-12">
									<div class="input-group date" id="dateRemoved">
							            <input name="dateRemoved" type="text" class="form-control" placeholder="dd/mm/yyyy" value="<?php if($isEdit) echo $this->ppmsystemlib->check_date_time($member['dateRemoved']); ?>"/>
							            <span class="input-group-addon">
							            <span class="glyphicon glyphicon-calendar"></span>
							            </span>
							        </div>
								</div>
							</div>
							<div class="row">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Reason Removed: <i class="removedReasonOthercompulsory fa fa-asterisk text-danger" <?php if(!$is_other) echo 'style="display:none;"'; ?>></i></label>
								<div class="col-md-5 col-sm-12 col-xs-12">
									<?php
										echo $this->ppmsystemlib->createDropdown('removedReason', $removedReason_arr, $removedReason, FALSE, $isEdit, TRUE);
									?>
								</div>
								<div class="col-md-4 col-sm-12 col-xs-12">
									<input id="removedReasonOther" name="removedReasonOther" type="text" class="form-control" placeholder="Reason" <?php if(!$is_other) { ?>style="display:none;" <?php } ?> value="<?php if($isEdit){echo $member['removedReason'];} ?>"/>
								</div>
							</div>

						</div>

						<?php 

							$unsubscribeReason = array("Don't have PM Department","Cutting back on costs","Creates their own","Not of Value","Franchise distributes","Other");

							$unsubscribeReason_arr = array();

							foreach ($unsubscribeReason as $value) {
								$unsubscribeReason_arr[$value] = $value;
							}

							$unsubscribeReason = '';
							$is_other = FALSE;

							if($isEdit) {
								if(!in_array($member['unsubscribeReason'], $unsubscribeReason_arr) && !empty($member['unsubscribeReason'])) {
									$unsubscribeReason = 'Other';
									$is_other = TRUE;
								}
								else {
									if($member['unsubscribeReason'] === 'Other') $is_other = TRUE;
									$unsubscribeReason = $member['unsubscribeReason'];
									$member['unsubscribeReason'] = '';
								}
							}
						?>

						<div id="unsubscribecontainer" class="form-group" <?php if($hideFieldFromMember) echo 'style="display:none;"'; ?> >
							<div class="row">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Date Unsubscribed: <i class="unsubscribeReasonOthercompulsory fa fa-asterisk text-danger" <?php if(!$is_other) echo 'style="display:none;"'; ?>></i></label>
								<div class="col-md-5 col-sm-12 col-xs-12">
									<div class="input-group date" id="dateUnsubscribed">
							            <input name="dateUnsubscribed" type="text" class="form-control" placeholder="dd/mm/yyyy" value="<?php if($isEdit) echo $this->ppmsystemlib->check_date_time($member['dateUnsubscribed']); ?>" />
							            <span class="input-group-addon">
							            <span class="glyphicon glyphicon-calendar"></span>
							            </span>
							        </div>
								</div>
							</div>
							<div class="row">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Reason Unsubscribed: <i class="unsubscribeReasonOthercompulsory fa fa-asterisk text-danger" <?php if(!$is_other) echo 'style="display:none;"'; ?>></i></label>
								<div class="col-md-5 col-sm-12 col-xs-12">
									<?php
										echo $this->ppmsystemlib->createDropdown('unsubscribeReason', $unsubscribeReason_arr, $unsubscribeReason, FALSE, $isEdit, TRUE);
									?>
								</div>
								<div class="col-md-4 col-sm-12 col-xs-12">
									<input id="unsubscribeReasonOther" name="unsubscribeReasonOther" type="text" class="form-control" placeholder="Reason" <?php if(!$is_other) { ?>style="display:none;" <?php } ?> value="<?php if($isEdit){echo $member['unsubscribeReason'];} ?>"/>
								</div>
							</div>
						</div>						

						<div id="dateCancelledContainer" class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Date Cancelled: <i class="reasonCancelledOthercompulsory fa fa-asterisk text-danger" <?php if(!$is_other) echo 'style="display:none;"'; ?>></i></label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<div class="input-group date" id="dateCancelled">
						            <input name="dateCancelled" type="text" class="form-control" placeholder="dd/mm/yyyy" value="<?php if($isEdit){echo $this->ppmsystemlib->check_date_time($member['dateCancelled']);} ?>" />
						            <span class="input-group-addon">
						            <span class="glyphicon glyphicon-calendar"></span>
						            </span>
						        </div>
							</div>
						</div>

						<?php

							$reason_cancelled_arr = array();

							foreach ($reason_cancelled as $value) {
								$reason_cancelled_arr[$value] = $value;
							}

							$reason_cancelled = '';
							$is_other = FALSE;

							if($isEdit) {
								if(!in_array($member['reasonCancelled'], $reason_cancelled_arr) && !empty($member['reasonCancelled'])) {
									$reason_cancelled = 'Other - Text';
									$is_other = TRUE;
								}
								else {
									if($member['reasonCancelled'] === 'Other - Text') $is_other = TRUE;
									$reason_cancelled = $member['reasonCancelled'];
									$member['reasonCancelled'] = '';
								}
							}							

						?>

						<div id="reasonCancelledContainer" class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Reason Cancelled: <i class="reasonCancelledOthercompulsory fa fa-asterisk text-danger" <?php if(!$is_other) echo 'style="display:none;"'; ?>></i></label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<?php 
									echo $this->ppmsystemlib->createDropdown('reasonCancelled', $reason_cancelled_arr, $reason_cancelled, FALSE, $isEdit, TRUE);
								?>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-12">
								<input id="reasonCancelledOther" name="reasonCancelledOther" type="text" class="form-control" placeholder="Reason" <?php if(!$is_other) { ?>style="display:none;"<?php } ?> value="<?php if($isEdit){echo $member['reasonCancelled'];} ?>"/>
							</div>
						</div>						

						<?php if(!$exclude) { ?>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Payments:</label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<?php 
									echo $this->ppmsystemlib->createDropdown('membershipPayments', $membershipPayments, $member['membershipPayments'], TRUE, $isEdit);
								?>
							</div>
						</div>
						<?php } ?>

						<div id="everyonebutgeneral5" class="form-group" <?php if($hideFieldFromMember) echo 'style="display:none;"'; ?> >
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Membership Investment:</label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<div class="input-group input-group-sm">
									<label class="input-group-addon" for="membershipInvestment"><i class="fa fa-dollar"></i></label>
									<input id="membershipInvestment" name="membershipInvestment" type="text" class="form-control" id="membershipInvestment" placeholder="" value="<?php if($isEdit && is_numeric($member['membershipInvestment'])){echo round($member['membershipInvestment'],2);} ?>" />
								</div>
							</div>
						</div>

						<div id="everyonebutgeneral6" class="form-group" <?php if($hideFieldFromMember) echo 'style="display:none;"'; ?> >
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Billing Cycle:</label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<?php 
									echo $this->ppmsystemlib->createDropdown('billingCycle', $billingCycle, $member['billingCycle'], TRUE, $isEdit);
								?>

							</div>
						</div>

						<div id="everyonebutgeneral7" class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Consulting Rate:</label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<div class="input-group input-group-sm">
									<label class="input-group-addon" for="consultingRate"><i class="fa fa-dollar"></i></label>
									<input name="consultingRate" type="text" class="form-control" id="consultingRate" placeholder="" value="<?php if($isEdit){echo $member['consultingRate'];} ?>" />
								</div>
							</div>
						</div>						

					</div>
				</div>
			</div>
		</div>
		<!-- General -->

		<!-- Interests -->	
		<div id="interests" class="panel" <?php if(!$hideFieldFromMember) echo 'style="display:none;"'; ?> >
			<a class="panel-heading" role="tab" id="members-add-edit_2" data-toggle="collapse" data-parent="#accordion-add-edit" href="#add-edit_2" aria-expanded="false" aria-controls="add-edit_2">
				<h4 class="panel-title">Interests</h4>
			</a>
			<div id="add-edit_2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="members-add-edit_2">
				<div class="panel-body">
					<div class="col-md-12">

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">System Enquiry:</label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<div class="input-group date" id="systemEnquiryDate">
						            <input name="systemEnquiryDate" type="text" class="form-control" placeholder="dd/mm/yyyy" value="<?php if($isEdit){echo $this->ppmsystemlib->check_date_time($member['systemEnquiryDate']);} ?>" />
						            <span class="input-group-addon">
						            <span class="glyphicon glyphicon-calendar"></span>
						            </span>
						        </div>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-12">
								<?php
									echo $this->ppmsystemlib->createDropdown('systemEnquiryUser', $users, $member['systemEnquiryUser'], FALSE, $isEdit, FALSE, 'Users');
								?>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">System Presentation:</label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<div class="input-group date" id="systemPresentationDate">
						            <input name="systemPresentationDate" type="text" class="form-control" placeholder="dd/mm/yyyy" value="<?php if($isEdit){echo $this->ppmsystemlib->check_date_time($member['systemPresentationDate']);} ?>" />
						            <span class="input-group-addon">
						            <span class="glyphicon glyphicon-calendar"></span>
						            </span>
						        </div>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-12">
								<?php
									echo $this->ppmsystemlib->createDropdown('systemPresentationUser', $users, $member['systemPresentationUser'], FALSE, $isEdit, FALSE, 'Users');
								?>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Training:</label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<div class="input-group date" id="trainingInterestDate">
						            <input name="trainingInterestDate" type="text" class="form-control" placeholder="dd/mm/yyyy" value="<?php if($isEdit){echo $this->ppmsystemlib->check_date_time($member['trainingInterestDate']);}?>" />
						            <span class="input-group-addon">
						            <span class="glyphicon glyphicon-calendar"></span>
						            </span>
						        </div>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-12">
								<?php
									echo $this->ppmsystemlib->createDropdown('trainingInterestUser', $users, $member['trainingInterestUser'], FALSE, $isEdit, FALSE, 'Users');
								?>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Product:</label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<div class="input-group date" id="productInterestDate">
						            <input name="productInterestDate" type="text" class="form-control" placeholder="dd/mm/yyyy" value="<?php if($isEdit){echo $this->ppmsystemlib->check_date_time($member['productInterestDate']);} ?>" />
						            <span class="input-group-addon">
						            <span class="glyphicon glyphicon-calendar"></span>
						            </span>
						        </div>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-12">
								<?php
									echo $this->ppmsystemlib->createDropdown('productInterestUser', $users, $member['productInterestUser'], FALSE, $isEdit, FALSE, 'Users');
								?>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Conference:</label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<div class="input-group date" id="conferenceInterestDate">
						            <input name="conferenceInterestDate" type="text" class="form-control" placeholder="dd/mm/yyyy" value="<?php if($isEdit){echo $this->ppmsystemlib->check_date_time($member['conferenceInterestDate']);} ?>" />
						            <span class="input-group-addon">
						            <span class="glyphicon glyphicon-calendar"></span>
						            </span>
						        </div>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-12">
								<?php
									echo $this->ppmsystemlib->createDropdown('conferenceInterestUser', $users, $member['conferenceInterestUser'], FALSE, $isEdit, FALSE, 'Users');
								?>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Online Training:</label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<div class="input-group date" id="onlineTrainingInterestDate">
						            <input name="onlineTrainingInterestDate" type="text" class="form-control" placeholder="dd/mm/yyyy" value="<?php if($isEdit){echo $this->ppmsystemlib->check_date_time($member['onlineTrainingInterestDate']);} ?>" />
						            <span class="input-group-addon">
						            <span class="glyphicon glyphicon-calendar"></span>
						            </span>
						        </div>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-12">
								<?php
									echo $this->ppmsystemlib->createDropdown('onlineTrainingInterestUser', $users, $member['onlineTrainingInterestUser'], FALSE, $isEdit, FALSE, 'Users');
								?>
							</div>
						</div>																		

					</div>
				</div>
			</div>
		</div>
		<!-- Interests -->

		<!-- Other -->	
		<div id="other" class="panel" <?php if(empty($membershipType) OR ($isEdit && $membershipType >= 2)) echo 'style="display:none;"'; ?> >
			<a class="panel-heading" role="tab" id="members-add-edit_3" data-toggle="collapse" data-parent="#accordion-add-edit" href="#add-edit_3" aria-expanded="false" aria-controls="add-edit_3">
				<h4 class="panel-title">Other</h4>
			</a>
			<div id="add-edit_3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="members-add-edit_3">
				<div class="panel-body">
					<div class="col-md-12">

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Number of Residential Properties:</label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<input name="numberOfProperties" type="text" class="form-control" placeholder="" value="<?php if($isEdit){echo $member['numberOfProperties'];} ?>" />
							</div>
							<div class="col-md-4 col-sm-4 col-xs-12">
								<div class="input-group date">
									<label class="input-group-addon" for="asAt">As At</label>
						            <input id="asAt" name="asAt" type="text" class="form-control" value="<?php if($isEdit){echo $this->ppmsystemlib->check_date_time($member['asAt']);} ?>" />
						            <span class="input-group-addon">
						            <span class="glyphicon glyphicon-calendar"></span>
						            </span>
						        </div>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Number of Commercial Properties:</label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<input name="numberOfPropertiesCommercial" type="text" class="form-control" placeholder="" value="<?php if($isEdit){echo $member['numberOfPropertiesCommercial'];}?>" />
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Number of Holiday Properties:</label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<input name="numberOfPropertiesHoliday" type="text" class="form-control" placeholder="" value="<?php if($isEdit){echo $member['numberOfPropertiesHoliday'];}?>" />
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">PPMsystem Investment:</label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<div class="input-group input-group-sm">
									<label class="input-group-addon" for="ppmSystemInvestment"><i class="fa fa-dollar"></i></label>
									<input name="ppmSystemInvestment" type="text" class="form-control" id="ppmSystemInvestment" placeholder="" value="<?php if($isEdit){echo $member['ppmSystemInvestment'];}?>" >
								</div>
							</div>
						</div>

						<?php if(!$exclude) { ?>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">System Type:</label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<?php echo $this->ppmsystemlib->createDropdown('systemType', $system_type, $member['systemType'], TRUE, $isEdit); ?>

							</div>
						</div>

						<div class="form-group <?php if($isEdit && intval($member['systemType']) === 4) echo 'hide'; ?>" >
							<label class="control-label col-md-3 col-sm-12 col-xs-12">PPMsystem Powered by<br /> REI Master Modules:</label>
							<div class="col-md-9 col-sm-12 col-xs-12" >
								<div class="checkbox">
									<label><span class="badge bg-green">Selected Modules: <small>(0)</small></span></label>
								</div>	
								<div style="max-height: 285px; overflow-y: auto;">
								<?php 

								if(isset($rei_master_modules)) {
									foreach ($rei_master_modules as $value) {
								?>
								<div class="checkbox">
									<label>
									<input type="checkbox" name="reiMasterModules[]" class="flat" value="<?php echo $value['id'];?>" />    <?php echo $value['name']; ?>
									</label>
								</div>
								<?php

									}
								}

								?>
								</div>
	                          
							</div>
						</div>
						<?php } ?>

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Trust Account Program:</label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<?php echo $this->ppmsystemlib->createDropdown('trustAccountProgram', $trust_account_program, $member['trustAccountProgram'], FALSE, $isEdit); ?>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Trust Account Program (other):</label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<input name="trustAccountProgramOther" type="text" class="form-control" placeholder="" value="<?php if($isEdit){echo $member['trustAccountProgramOther'];} ?>"/>
							</div>
						</div>											

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Server Driver:</label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<input name="serverDriver" type="text" class="form-control" placeholder="" value="<?php if($isEdit){echo $member['serverDriver'];} ?>"/>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">System Implementation Date:</label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<div class="input-group date" id="systemCommencementDate">
						            <input name="systemCommencementDate" type="text" class="form-control" value="<?php if($isEdit){echo $this->ppmsystemlib->check_date_time($member['systemCommencementDate']);} ?>" />
						            <span class="input-group-addon">
						            <span class="glyphicon glyphicon-calendar"></span>
						            </span>
						        </div>
							</div>
						</div>						

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Num of Registered System Licences:</label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<input name="numberOfRegisteredSystemLicences" type="text" class="form-control" value="<?php if($isEdit){echo $member['numberOfRegisteredSystemLicences'];} ?>" placeholder="" />
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Total Consultancy Hours Purchased:</label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<div class="input-group input-group-sm">
									<label class="input-group-addon" for="totalConsultancyHoursPurchased">Hrs</label>
									<input name="totalConsultancyHoursPurchased" type="text" class="form-control" id="totalConsultancyHoursPurchased" placeholder="" value="<?php if($isEdit){echo round($member['totalConsultancyHoursPurchased'], 2);} ?>">
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Consultancy Hours Used:</label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<div class="input-group input-group-sm">
									<label class="input-group-addon" for="consultancyHoursUsed">Hrs</label>
									<input name="consultancyHoursUsed" type="text" class="form-control" id="consultancyHoursUsed" placeholder="" value="<?php if($isEdit){echo round($member['consultancyHoursUsed'], 2);} ?>">
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Consulting Hours Remaining:</label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<label class="control-label text-primary"><font id="consultingHoursRemaining">0</font> hr(s)</label>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Additional Consulting Hours Billed</label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<div class="input-group input-group-sm">
									<label class="input-group-addon" for="AdditionalConsultinghoursbilled">Hrs</label>
									<input name="AdditionalConsultinghoursbilled" type="text" class="form-control" id="AdditionalConsultinghoursbilled" placeholder="" value="<?php if($isEdit){echo round($member['additionalConsultinghoursbilled'], 2);} ?>">
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Total Billed Hours to Date</label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<label class="control-label text-primary"><font id="totalBilledHoursToDateLabel"><?php if($isEdit){echo round($member['totalBilledHoursToDate'], 2);} ?></font> hr(s)</label>
								<div class="input-group input-group-sm hide">
									<label class="input-group-addon" for="totalBilledHoursToDate">Hrs</label>
									<input name="totalBilledHoursToDate" readonly type="text" class="form-control" id="totalBilledHoursToDate" placeholder="" value="<?php if($isEdit){echo round($member['totalBilledHoursToDate'], 2);} ?>">
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Last Audit Date:</label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<div class="input-group lastAuditDate">
						            <input id="lastAuditDate" name="lastAuditDate" type="text" class="form-control" value="<?php if($isEdit){echo $this->ppmsystemlib->check_date_time($member['lastAuditDate']);} ?>" />
						            <span class="input-group-addon">
						            <span class="glyphicon glyphicon-calendar"></span>
						            </span>
						        </div>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Frequency Of Audits:</label>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<?php
								echo $this->ppmsystemlib->createDropdown('frequencyOfAudits', $frequency_of_audits, $member['frequencyOfAudits'], TRUE, $isEdit);
								?>

							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12">Next Audit Due:</label>
							<div class="col-md-5 col-sm-12 col-xs-12">
						    	<input id="nextAuditDue" name="nextAuditDue" type="text" class="form-control" readonly value="<?php if($isEdit){echo $this->ppmsystemlib->check_date_time($member['nextAuditDue']);} ?>" />
							</div>
						</div>						

					</div>
				</div>
			</div>
		</div>
		<!-- Other -->

		<!-- Other (Plans) -->	
		<div id="other_plans" class="panel">
			<a class="panel-heading" role="tab" id="members-add-edit_4" data-toggle="collapse" data-parent="#accordion-add-edit" href="#add-edit_4" aria-expanded="false" aria-controls="add-edit_4">
				<h4 class="panel-title">Plans <small>(Date Completed)</small></h4>
			</a>
			<div id="add-edit_4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="members-add-edit_4">
				<div class="panel-body">
				
					<div class="form-group">
						<label class="control-label col-md-5 col-sm-12 col-xs-12">Action Plan 1: Pre-system Requirements:</label>
						<div class="col-md-5 col-sm-12 col-xs-12">
							<div class="input-group date" id="aPlan1">
					            <input name="aPlan1" type="text" class="form-control" placeholder="dd/mm/yyyy" value="<?php if($isEdit){echo $this->ppmsystemlib->check_date_time($member['aPlan1']);} ?>" />
					            <span class="input-group-addon">
					            <span class="glyphicon glyphicon-calendar"></span>
					            </span>
					        </div>
						</div>
					</div>	

					<div class="form-group">
						<label class="control-label col-md-5 col-sm-12 col-xs-12">Action Plan 2: Principal Discussions:</label>
						<div class="col-md-5 col-sm-12 col-xs-12">
							<div class="input-group date" id="aPlan2">
					            <input name="aPlan2" type="text" class="form-control" placeholder="dd/mm/yyyy" value="<?php if($isEdit){echo $this->ppmsystemlib->check_date_time($member['aPlan2']);} ?>" />
					            <span class="input-group-addon">
					            <span class="glyphicon glyphicon-calendar"></span>
					            </span>
					        </div>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-5 col-sm-12 col-xs-12">Action Plan 3: Install:</label>
						<div class="col-md-5 col-sm-12 col-xs-12">
							<div class="input-group date" id="aPlan3">
					            <input name="aPlan3" type="text" class="form-control" placeholder="dd/mm/yyyy" value="<?php if($isEdit){echo $this->ppmsystemlib->check_date_time($member['aPlan3']);} ?>" />
					            <span class="input-group-addon">
					            <span class="glyphicon glyphicon-calendar"></span>
					            </span>
					        </div>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-5 col-sm-12 col-xs-12">Action Plan 3a: Customised Forms & Office Layout:</label>
						<div class="col-md-5 col-sm-12 col-xs-12">
							<div class="input-group date" id="aPlan3a">
					            <input name="aPlan3a" type="text" class="form-control" placeholder="dd/mm/yyyy" value="<?php if($isEdit){echo $this->ppmsystemlib->check_date_time($member['aPlan3a']);} ?>" />
					            <span class="input-group-addon">
					            <span class="glyphicon glyphicon-calendar"></span>
					            </span>
					        </div>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-5 col-sm-12 col-xs-12">Action Plan 4: Customise Checklists  & Training:</label>
						<div class="col-md-5 col-sm-12 col-xs-12">
							<div class="input-group date" id="aPlan4">
					            <input name="aPlan4" type="text" class="form-control" placeholder="dd/mm/yyyy" value="<?php if($isEdit){echo $this->ppmsystemlib->check_date_time($member['aPlan4']);} ?>" />
					            <span class="input-group-addon">
					            <span class="glyphicon glyphicon-calendar"></span>
					            </span>
					        </div>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-5 col-sm-12 col-xs-12">Action Plan 5: Department Audit:</label>
						<div class="col-md-5 col-sm-12 col-xs-12">
							<div class="input-group date" id="aPlan5">
					            <input name="aPlan5" type="text" class="form-control" placeholder="dd/mm/yyyy" value="<?php if($isEdit){echo $this->ppmsystemlib->check_date_time($member['aPlan5']);} ?>" />
					            <span class="input-group-addon">
					            <span class="glyphicon glyphicon-calendar"></span>
					            </span>
					        </div>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-5 col-sm-12 col-xs-12">Action Plan 6: Human Resource Management:</label>
						<div class="col-md-5 col-sm-12 col-xs-12">
							<div class="input-group date" id="aPlan6">
					            <input name="aPlan6" type="text" class="form-control" placeholder="dd/mm/yyyy" value="<?php if($isEdit){echo $this->ppmsystemlib->check_date_time($member['aPlan6']);} ?>" />
					            <span class="input-group-addon">
					            <span class="glyphicon glyphicon-calendar"></span>
					            </span>
					        </div>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-5 col-sm-12 col-xs-12">Action Plan 7: Marketing & Customer Service:</label>
						<div class="col-md-5 col-sm-12 col-xs-12">
							<div class="input-group date" id="aPlan7">
					            <input name="aPlan7" type="text" class="form-control" placeholder="dd/mm/yyyy" value="<?php if($isEdit){echo $this->ppmsystemlib->check_date_time($member['aPlan7']);} ?>" />
					            <span class="input-group-addon">
					            <span class="glyphicon glyphicon-calendar"></span>
					            </span>
					        </div>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-5 col-sm-12 col-xs-12">Action Plan 8: KPI Report & Bottom-Line Report:</label>
						<div class="col-md-5 col-sm-12 col-xs-12">
							<div class="input-group date" id="aPlan8">
					            <input name="aPlan8" type="text" class="form-control" placeholder="dd/mm/yyyy" value="<?php if($isEdit){echo $this->ppmsystemlib->check_date_time($member['aPlan8']);} ?>" />
					            <span class="input-group-addon">
					            <span class="glyphicon glyphicon-calendar"></span>
					            </span>
					        </div>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-5 col-sm-12 col-xs-12">REI Checklist : Installation:</label>
						<div class="col-md-5 col-sm-12 col-xs-12">
							<div class="input-group date" id="rei1">
					            <input name="rei1" type="text" class="form-control" placeholder="dd/mm/yyyy" value="<?php if($isEdit){echo $this->ppmsystemlib->check_date_time($member['rei1']);} ?>" />
					            <span class="input-group-addon">
					            <span class="glyphicon glyphicon-calendar"></span>
					            </span>
					        </div>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-5 col-sm-12 col-xs-12">REI Lesson Plan 1: Data Entry:</label>
						<div class="col-md-5 col-sm-12 col-xs-12">
							<div class="input-group date" id="rei2">
					            <input name="rei2" type="text" class="form-control" placeholder="dd/mm/yyyy" value="<?php if($isEdit){echo $this->ppmsystemlib->check_date_time($member['rei2']);} ?>" />
					            <span class="input-group-addon">
					            <span class="glyphicon glyphicon-calendar"></span>
					            </span>
					        </div>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-5 col-sm-12 col-xs-12">REI Lesson Plan 1: Data Conversion:</label>
						<div class="col-md-5 col-sm-12 col-xs-12">
							<div class="input-group date" id="rei3">
					            <input name="rei3" type="text" class="form-control" placeholder="dd/mm/yyyy" value="<?php if($isEdit){echo $this->ppmsystemlib->check_date_time($member['rei3']);} ?>" />
					            <span class="input-group-addon">
					            <span class="glyphicon glyphicon-calendar"></span>
					            </span>
					        </div>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-5 col-sm-12 col-xs-12">REI Lesson Plan 2: Daily Applications:</label>
						<div class="col-md-5 col-sm-12 col-xs-12">
							<div class="input-group date" id="rei4">
					            <input name="rei4" type="text" class="form-control" placeholder="dd/mm/yyyy" value="<?php if($isEdit){echo $this->ppmsystemlib->check_date_time($member['rei4']);} ?>" />
					            <span class="input-group-addon">
					            <span class="glyphicon glyphicon-calendar"></span>
					            </span>
					        </div>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-5 col-sm-12 col-xs-12">REI Lesson Plan 3: Interim Disbursements:</label>
						<div class="col-md-5 col-sm-12 col-xs-12">
							<div class="input-group date" id="rei5">
					            <input name="rei5" type="text" class="form-control" placeholder="dd/mm/yyyy" value="<?php if($isEdit){echo $this->ppmsystemlib->check_date_time($member['rei5']);} ?>" />
					            <span class="input-group-addon">
					            <span class="glyphicon glyphicon-calendar"></span>
					            </span>
					        </div>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-5 col-sm-12 col-xs-12">REI Lesson Plan 4: Advanced Features:</label>
						<div class="col-md-5 col-sm-12 col-xs-12">
							<div class="input-group date" id="rei6">
					            <input name="rei6" type="text" class="form-control" placeholder="dd/mm/yyyy" value="<?php if($isEdit){echo $this->ppmsystemlib->check_date_time($member['rei6']);} ?>" />
					            <span class="input-group-addon">
					            <span class="glyphicon glyphicon-calendar"></span>
					            </span>
					        </div>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-5 col-sm-12 col-xs-12">REI Lesson Plan 5: Pre End of Month:</label>
						<div class="col-md-5 col-sm-12 col-xs-12">
							<div class="input-group date" id="rei7">
					            <input name="rei7" type="text" class="form-control" placeholder="dd/mm/yyyy" value="<?php if($isEdit){echo $this->ppmsystemlib->check_date_time($member['rei7']);} ?>" />
					            <span class="input-group-addon">
					            <span class="glyphicon glyphicon-calendar"></span>
					            </span>
					        </div>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-5 col-sm-12 col-xs-12">REI Lesson Plan 6: End of Month:</label>
						<div class="col-md-5 col-sm-12 col-xs-12">
							<div class="input-group date" id="rei8">
					            <input name="rei8" type="text" class="form-control" placeholder="dd/mm/yyyy" value="<?php if($isEdit){echo $this->ppmsystemlib->check_date_time($member['rei8']);} ?>" />
					            <span class="input-group-addon">
					            <span class="glyphicon glyphicon-calendar"></span>
					            </span>
					        </div>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-5 col-sm-12 col-xs-12">REI Lesson Plan 7: Sales Trust:</label>
						<div class="col-md-5 col-sm-12 col-xs-12">
							<div class="input-group date" id="rei9">
					            <input name="rei9" type="text" class="form-control" placeholder="dd/mm/yyyy" value="<?php if($isEdit){echo $this->ppmsystemlib->check_date_time($member['rei9']);} ?>" />
					            <span class="input-group-addon">
					            <span class="glyphicon glyphicon-calendar"></span>
					            </span>
					        </div>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-5 col-sm-12 col-xs-12">REI Lesson Plan 8: Commercial:</label>
						<div class="col-md-5 col-sm-12 col-xs-12">
							<div class="input-group date" id="rei10">
					            <input name="rei10" type="text" class="form-control" placeholder="dd/mm/yyyy" value="<?php if($isEdit){echo $this->ppmsystemlib->check_date_time($member['rei10']);} ?>" />
					            <span class="input-group-addon">
					            <span class="glyphicon glyphicon-calendar"></span>
					            </span>
					        </div>
						</div>
					</div>


				</div>
			</div>
		</div>				
		<!-- Other (Plans) -->

		<!-- Staff -->	
		<div class="panel">
			<a class="panel-heading" role="tab" id="members-add-edit_5" data-toggle="collapse" data-parent="#accordion-add-edit" href="#add-edit_5" aria-expanded="false" aria-controls="add-edit_5">
				<h4 class="panel-title">Staff</h4>
			</a>
			<div id="add-edit_5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="members-add-edit_5">
				<div class="panel-body">
					<div class="col-md-12">

						<table id="staff-table" class="table table-striped table-hover table-condensed text-center"><!-- table-bordered  -->
						<thead>
							<tr>
								<th style="text-align:center!important; vertical-align: middle!important;">First Name</th>
								<th style="text-align:center!important; vertical-align: middle!important;">Last Name</th>
								<th style="text-align:center!important; vertical-align: middle!important;">Email</th>
								<th style="text-align:center!important; vertical-align: middle!important;">Mobile</th>
								<th style="text-align:center!important; vertical-align: middle!important;">Direct</th>
								<th style="text-align:center!important; vertical-align: middle!important;">Position</th>
								<th style="text-align:center!important; vertical-align: middle!important;">Delete</th>		                                  
							</tr>
						</thead>
						<tbody id="staff-holder">
							<tr class="staff-copy hide">
								<td><input type="text" class="form-control staff-fn" placeholder="" value="" /></td>
								<td><input type="text" class="form-control staff-ln" placeholder="" value="" /></td>
								<td>
									<div class="input-group">
										<input type="text" class="form-control staff-email" placeholder="" value="" />
										<div class="input-group-btn">
											<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Action <span class="caret"></span>
											</button>
											<ul class="dropdown-menu dropdown-menu-right" role="menu">
												<li><a href="javascript:;">Open Email Form</a>
												</li>
												<li><a href="mailto:" class="email-label">Mail To:</a>
												</li>
											</ul>
										</div>
									<!-- /btn-group -->
									</div>

								</td>
								<td><input type="text" class="form-control staff-mobile" placeholder="" value="" /></td>
								<td><input type="text" class="form-control staff-direct" placeholder="" value="" /></td>
								<td>
									<select class="form-control staff-position">
									<option value="">[Select]</option>
									<?php 

										if(isset($staff_positions)) {
											foreach ($staff_positions as $key=>$value) {
												echo '<option value="' . $key .'">' . $value . '</option>';
											}
										}

									?>
									</select>

								</td>
								<td style="text-align:center!important; vertical-align: middle!important;"><button type="button" class="btn btn-xs btn-danger staff-delete"><i class="fa fa-close"></i></button></td>
							</tr>
						</tbody>
						</table>

						<div class="pull-right"><button id="staff-add-row" type="button" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Add Row</button></div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
		<!-- Staff -->

		<!-- Comments -->	
		<div class="panel">
			<a class="panel-heading" role="tab" id="members-add-edit_6" data-toggle="collapse" data-parent="#accordion-add-edit" href="#add-edit_6" aria-expanded="false" aria-controls="add-edit_6">
				<h4 class="panel-title">Comments</h4>
			</a>
			<div id="add-edit_6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="members-add-edit_6">
				<div class="panel-body">
					<div class="col-md-12">

						<div class="form-group">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<p><button type="button" id="add-date-btn" class="btn btn-xs btn-success">Add Date</button></p>
								<textarea id="comments-text" name="comments" class="form-control" rows="15"><?php if($isEdit){echo $member['comments'];} ?></textarea>
							</div>
						</div>
						<?php if(!$exclude) { ?>
						<div class="form-group">
							<label class="control-label col-md-12 col-sm-12 col-xs-12" style="text-align:left!important;">Consulting Hours Summary:</label>
							<div class="col-md-12 col-sm-12 col-xs-12">
								<textarea name="consultingHourSummary" class="form-control" rows="5"><?php if($isEdit){echo $member['consultingHourSummary'];} ?></textarea>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-12 col-sm-12 col-xs-12" style="text-align:left!important;">General Accounts Comments:</label>
							<div class="col-md-12 col-sm-12 col-xs-12">
								<textarea name="generalAccountsComments" class="form-control" rows="5"><?php if($isEdit){echo $member['generalAccountsComments'];} ?></textarea>
							</div>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
		<!-- Comments -->			

		<?php if($isEdit) {?>

		<?php if(!$exclude) { ?>
		<!-- Tasks -->	
		<div class="panel">
			<a class="panel-heading" role="tab" id="members-add-edit_7" data-toggle="collapse" data-parent="#accordion-add-edit" href="#add-edit_7" aria-expanded="false" aria-controls="add-edit_7">
				<h4 class="panel-title">Tasks</h4>
			</a>
			<div id="add-edit_7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="members-add-edit_7">
				<div class="panel-body">
					<div class="col-md-12 col-sm-12 col-xs-12" style="max-height: 410px; overflow-y: auto;">

						<table id="tasks-table" class="table table-striped table-hover table-condensed text-left"><!-- table-bordered  -->
						<thead>
							<tr>
								<th style="vertical-align: middle!important;">Status</th>
								<th style="vertical-align: middle!important;">User</th>
								<th style="vertical-align: middle!important;">Type</th>
								<th style="vertical-align: middle!important;">Name</th>
								<th style="vertical-align: middle!important;">Due</th>
								<th style="vertical-align: middle!important;">Recurs (days)</th>
								<th style="vertical-align: middle!important;">Edit</th>		                                  
							</tr>
						</thead>
						<tbody>
							<?php
								if($tasks!=NULL) {
									foreach ($tasks as $value) {
							?>
							<tr>
								<td><?php echo $status_arr[intval($value['status']) - 1]; ?></td>
								<td><?php echo $value['username']; ?></td>
								<td><?php echo $value['myType']; ?></td>
								<td><?php echo $value['name']; ?></td>
								<td><?php echo $value['dateDue']; ?></td>
								<td><?php echo $value['regenerate']; ?></td>
								<td style="text-align:center!important; vertical-align: middle!important;"><button type="button" class="btn btn-xs btn-warning"><i class="fa fa-pencil"></i></button></td>
							</tr>

							<?php 
									}
								}
							?>
						</tbody>
						</table>

					</div>
				</div>
			</div>
		</div>
		<!-- Tasks -->
		<?php } ?>
		
		<!-- Payment Histories -->	
		
		<?php
			if(!empty($member_data['paymentHistories']) && !$hideFieldFromMember) {
		?>
		
		<div class="panel">
			<a class="panel-heading" role="tab" id="members-add-edit_8" data-toggle="collapse" data-parent="#accordion-add-edit" href="#add-edit_8" aria-expanded="false" aria-controls="add-edit_8">
				<h4 class="panel-title">Payment Histories</h4>
			</a>
			<div id="add-edit_8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="members-add-edit_8">
				<div class="panel-body">
					<div class="col-md-12 col-sm-12 col-xs-12" style="max-height: 410px; overflow-y: auto;">

						<table id="tasks-table" class="table table-striped table-hover table-condensed text-left"><!-- table-bordered  -->
						<thead>
							<tr>
								<th style="vertical-align: middle!important;">UID</th>
								<th style="vertical-align: middle!important;">Status</th>
								<th style="vertical-align: middle!important;">Date</th>
								<th style="vertical-align: middle!important;">Trxn Ref</th>
								<th style="vertical-align: middle!important;">Auth Code</th>
								<th style="vertical-align: middle!important;">Amount</th>
							</tr>
						</thead>
						<tbody>
							<?php
								
								foreach ($member_data['paymentHistories'] as $value) {
							?>
							<tr>
								<td><?php echo $value['orderId']; ?></td>
								<td><?php echo ($value['paymentStatus']==1) ? 'OK' : ''; ?></td>
								<td><?php echo $value['myDateTime']; ?></td>
								<td><?php echo $value['trxnReference']; ?></td>
								<td><?php echo $value['authCode']; ?></td>
								<td><?php echo $value['amount']; ?></td>
							</tr>

							<?php 
								}
							?>
						</tbody>
						</table>

					</div>
				</div>
			</div>
		</div>

		<?php
			}//end if Payment Histories


			if(!$hideFieldFromMember) {

		?>
		
		<!-- Payment Histories -->
		<?php if(FALSE) { ?>
		<div class="panel">
			<div class="col-md-12">
				<p></p>
				<p><button type="button" class="btn btn-xs btn-primary">Add Payment</button></p>
			</div>
			<div class="clearfix"></div>
		</div>
		<?php }?>

		<!-- Products Purchased -->	
		
		<?php 
			if(!empty($member_data['productsPurchased'])) {
		?>

		<div class="panel">
			<a class="panel-heading" role="tab" id="members-add-edit_9" data-toggle="collapse" data-parent="#accordion-add-edit" href="#add-edit_9" aria-expanded="false" aria-controls="add-edit_9">
				<h4 class="panel-title">Products Purchased</h4>
			</a>
			<div id="add-edit_9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="members-add-edit_9">
				<div class="panel-body">
					<div class="col-md-12 col-sm-12 col-xs-12" style="max-height: 410px; overflow-y: auto;">

						<table id="tasks-table" class="table table-striped table-hover table-condensed text-left"><!-- table-bordered  -->
						<thead>
							<tr>
								<th style="vertical-align: middle!important;">Date</th>
								<th style="vertical-align: middle!important;">Product Code</th>
								<th style="vertical-align: middle!important;">Product</th>
							</tr>
						</thead>
						<tbody>
							<?php
								
								foreach ($member_data['productsPurchased'] as $value) {
							?>
							<tr>
								<td><?php echo $value['myDateTime']; ?></td>
								<td><?php echo ($value['code']==1) ? 'OK' : ''; ?></td>
								<td><?php echo $value['name']; ?></td>
							</tr>

							<?php 
								}
							?>
						</tbody>
						</table>

					</div>
				</div>
			</div>
		</div>

		<?php
			}//end if Products Purchased

		?>
		
		<!-- Products Purchased -->


		<!-- Training Attended -->	
		
		<?php 
			if(!empty($member_data['trainingAttended'])) {
		?>

		<div class="panel">
			<a class="panel-heading" role="tab" id="members-add-edit_9" data-toggle="collapse" data-parent="#accordion-add-edit" href="#add-edit_9" aria-expanded="false" aria-controls="add-edit_9">
				<h4 class="panel-title">Training Attended</h4>
			</a>
			<div id="add-edit_9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="members-add-edit_9">
				<div class="panel-body">
					<div class="col-md-12 col-sm-12 col-xs-12" style="max-height: 410px; overflow-y: auto;">

						<table id="tasks-table" class="table table-striped table-hover table-condensed text-left"><!-- table-bordered  -->
						<thead>
							<tr>
								<th style="vertical-align: middle!important;">Date</th>
								<th style="vertical-align: middle!important;">Name</th>
								<th style="vertical-align: middle!important;">Link</th>
							</tr>
						</thead>
						<tbody>
							<?php
								
								foreach ($member_data['trainingAttended'] as $value) {
							?>
							<tr>
								<td><?php echo $value['myDateTime']; ?></td>
								<td><?php echo $value['name']; ?></td>
								<td><a href="javascript:;">Attendees</a></td>
							</tr>

							<?php 
								}
							?>
						</tbody>
						</table>

					</div>
				</div>
			</div>
		</div>

		<?php
			}//end if Training Attended

		?>
		
		<!-- Training Attended -->

		<!-- Conference Delegates-->	
		
		<?php 
			if(!empty($member_data['conferenceDelegates'])) {
		?>

		<div class="panel">
			<a class="panel-heading" role="tab" id="members-add-edit_10" data-toggle="collapse" data-parent="#accordion-add-edit" href="#add-edit_10" aria-expanded="false" aria-controls="add-edit_10">
				<h4 class="panel-title">Conference Delegates</h4>
			</a>
			<div id="add-edit_10" class="panel-collapse collapse" role="tabpanel" aria-labelledby="members-add-edit_10">
				<div class="panel-body">
					<div class="col-md-12 col-sm-12 col-xs-12" style="max-height: 410px; overflow-y: auto;">

						<table id="tasks-table" class="table table-striped table-hover table-condensed text-left"><!-- table-bordered  -->
						<thead>
							<tr>
								<th style="vertical-align: middle!important;">Date</th>
								<th style="vertical-align: middle!important;">Name</th>
								<th style="vertical-align: middle!important;">Link</th>
							</tr>
						</thead>
						<tbody>
							<?php
								
								foreach ($member_data['conferenceDelegates'] as $value) {
							?>
							<tr>
								<td><?php echo $value['myDateTime']; ?></td>
								<td><?php echo $value['name']; ?></td>
								<td><a href="javascript:;">Attendees</a></td>
							</tr>

							<?php 
								}
							?>
						</tbody>
						</table>

					</div>
				</div>
			</div>
		</div>

		<?php
			}//end if Conference Delegates

		?>
		
		<!-- Conference Delegates -->

		<!-- Monthly Award Stats -->	
		
		<?php 
			// wrap condition: if membershipType < 3

			if(!empty($member_data['monthlyAwardStats']) OR $isEdit) {
		?>

		<div class="panel">
			<a class="panel-heading" role="tab" id="members-add-edit_11" data-toggle="collapse" data-parent="#accordion-add-edit" href="#add-edit_11" aria-expanded="false" aria-controls="add-edit_11">
				<h4 class="panel-title">Monthly Award Stats</h4>
			</a>
			<div id="add-edit_11" class="panel-collapse collapse" role="tabpanel" aria-labelledby="members-add-edit_11">
				<div class="panel-body">
					<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
						<table id="monthly-award-stats-table" class="table table-striped table-hover table-condensed text-left hide" style="width:100%;"><!-- table-bordered  -->
						<thead>
							<tr>
								<th style="vertical-align: middle!important;">Date</th>
								<th style="vertical-align: middle!important;">Edit</th>
								<th style="vertical-align: middle!important;">Delete</th>
							</tr>
						</thead>
						<tbody>
							<?php
								
								foreach ($member_data['monthlyAwardStats'] as $value) {
									break;
							?>
							<tr>
								<td><?php echo (!empty($value['forMonth'])) ? date("F",strtotime($value['forMonth'])) : ''; echo (!empty($value['forMonth'])) ? ' ' . date("Y",strtotime($value['forMonth'])) : '';?></td>
								<td><button type="button" data-id="<?php echo $value['statId']; ?>" data-action="edit" class="action-btn btn btn-xs btn-warning"><i class="fa fa-pencil"></i></button></td>
								<td><button type="button" data-id="<?php echo $value['statId']; ?>" data-action="delete" class="action-btn btn btn-xs btn-danger"><i class="fa fa-close"></i></button></td>
							</tr>

							<?php 
								}
							?>
						</tbody>
						</table>
					</div>

					<div class="col-md-12 col-sm-12 col-xs-12">
						<p>&nbsp;</p>
						<button id="member-award-stats-btn" type="button" class="btn btn-xs btn-success"><i class="fa fa-plus"></i> Add Monthly Award Stats Data</button>
					</div>
				</div>
			</div>
		</div>

		<?php
			}//end if Monthly Award Stats

		}//end if(!$hideFieldFromMember)

		?>
		
		<!-- Monthly Award Stats -->

		<!-- Log Action -->

		<div class="panel">
			<div class="col-md-6">
				<ul class="nav nav-pills" role="tablist">			    
					<li role="presentation" class="dropdown">
						<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">View Logs<span class="caret"></span></a>
						<ul class="dropdown-menu animated fadeInDown" role="menu" aria-labelledby="drop6" >
							<li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="log-btn" data-type="download-log" >Download Log</a>
							</li>
							<li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="log-btn" data-type="email-log" >Email Log</a>
							</li>
							<li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="log-btn" data-type="login-log" >Login Log</a>
							</li>
						</ul>
					</li>
					<?php 
						if($isEdit && $member_data['isResetNewsletter'] == 1) {
					?>
					<li role="presentation" class="dropdown">
						<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">Reset Newsletter Download<span class="caret"></span></a>
						<ul class="dropdown-menu animated fadeInDown" role="menu" aria-labelledby="drop6" >
							<?php 
								foreach ($member_data['newsletter'] as $value) {
							?>
							<li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="reset-btn" data-newsid="<?php echo $value['id']; ?>" data-news="<?php echo $value['name']; ?>"><?php echo $value['name']; ?></a>
							</li>
							<?php } ?>
						</ul>
					</li>
					<?php } ?>
				</ul>
			</div>
			<div class="col-md-6 text-right">
				<button id="cancel-update" type="button" class="btn btn-sm btn-warning">Cancel Changes</button>
				<button id="add-update-btn" type="button" class="btn btn-sm btn-success">Update</button>
				<?php if(!$exclude) { ?><button type="button" class="btn btn-sm btn-success">Update and Add Task</button> <?php } ?>
			</div>			
			<div class="clearfix"></div>
		</div>

		<!-- Log Action -->

		<?php
		}//end if $isEdit
		else {
		
		?>

		<div class="panel">
			<div class="col-md-12 text-right">
				<button id="add-update-btn" type="button" class="btn btn-sm btn-success">Add</button>
			</div>
			<div class="clearfix"></div>
		</div>

		<?php
		}
		?>

	</div>
	<!-- end of accordion -->

</form>

<script type="text/javascript">

	var membersAddEdit = $(document).ready(function(){
		var staffList 				= <?php if(isset($member_data['staff'])) {echo json_encode($member_data['staff']);}else{echo '[]';} ?>;
		var staffTable 				= $('#staff-table');
		var staffHolder 			= $('#staff-holder');
		var staffRowCopy 			= staffHolder.find('tr.staff-copy');
		var staffAddRowBtn			= $('#staff-add-row');
		var cancelUpdateBtn			= $('#cancel-update');
		var addUpdateBtn 			= $('#add-update-btn');
		var addDateBtn 				= $('#add-date-btn');
		var passwordBtn 			= $('#password-btn');
		var passwordInput   		= $('#password-input');
		var commentsText 			= $('#comments-text');
		var howDidTheyHear  		= $('#howdidtheyhear');
		var howDidTheyHearOther 	= $('#howdidtheyhearother');
		var reasonCancelled 		= $('#reasonCancelled');
		var reasonCancelledOther	= $('#reasonCancelledOther');
		var removedReason 			= $('#removedReason');
		var removedReasonOther 		= $('#removedReasonOther');
		var unsubscribeReason 		= $('#unsubscribeReason');
		var unsubscribeReasonOther 	= $('#unsubscribeReasonOther');
		var state 					= $('#state-add-edit');
		var stateSubRegion 			= $('#state-subregion');
		var tempData 				= [{"name":"","lastName":"","email":"","mobile":"","direct":"","myPosition":""}];
		var downloadTable 			= $('#members-download-table');
		var emailTable 				= $('#members-email-table');
		var loginTable   			= $('#members-login-table');
		var logContent 				= $('#log-content');
		var tableId 				= '';
		var memberId 				= <?php if($isEdit) echo $member['id']; else echo 'null'; ?>;
		var catHolder 				= $('#categories-available');
		var catSelectedHolder		= $('#categories-selected');
		var catSelected 			= $('#cat-selected');
		var catItemId 				= '';
		var _lastAuditDate 			= null;
		var memberAwardStatsModal	= $('#member-award-stats-modal');
		var memberAwardStatsBtn		= $('#member-award-stats-btn');

		var members 		= {
			tempFunc: null,
			init: function(){
				this.staffList();

				staffAddRowBtn.on('click', function(){
					members.staffAddRow(tempData);
				});

				cancelUpdateBtn.on('click', function(){members.getAddForm()});

				$('.log-btn').on('click', function(){showLogBtn.trigger('click');});

				addUpdateBtn.on('click', function(){members.addUpdate();});

				addDateBtn.on('click', function(){members.addCommentDate();});

				passwordBtn.on('click', function(){
					if($(this).attr('data-type') == 'show') {
						$(this).attr({'data-type':'hide'}).text('Hide');
						passwordInput.attr({'type':'text'});
					}
					else {
						$(this).attr({'data-type':'show'}).text('Show');
						passwordInput.attr({'type':'password'});
					}
				});

				howDidTheyHear.on('change', function(){
					members.showOther($(this), howDidTheyHearOther);
				});

				reasonCancelled.on('change', function(){
					members.showOther($(this), reasonCancelledOther);
				});

				removedReason.on('change', function(){
					members.showOther($(this), removedReasonOther);
				});

				unsubscribeReason.on('change', function(){
					members.showOther($(this), unsubscribeReasonOther);
				});

				state.on('change', function(){
					members.checkState();
				});

				catHolder.on('drop', function(e){members.catDrop(e,$(this));});
				catSelectedHolder.on('drop', function(e){members.catDrop(e,$(this));});
				catHolder.on('dragover', function(e){members.catAllowDrop(e);});
				catSelectedHolder.on('dragover', function(e){members.catAllowDrop(e);});
				catHolder.find('li').on('dragstart', function(e){members.catDrag(e);});
				catSelectedHolder.find('li').on('dragstart', function(e){members.catDrag(e);});

				$('.lastAuditDate').datetimepicker({
					format: 'DD/MM/YYYY'
				}).on('dp.change', function(e){members.recalculateNextDue(e);});
				$('#frequencyOfAudits').on('change', function(){members.recalculateNextDue();});

				$('.reset-btn').on('click', function(){members.resetNewsletter($(this));});

				$("#membershipCommencementDate").datetimepicker({
					format: 'DD/MM/YYYY hh:mm A'
				});

				$("#membershipCommencementDate").on("dp.change", function(e) {
			        $('#membershipExpires').data("DateTimePicker").minDate(e.date);
			    });

				$("#membershipExpires").datetimepicker({
					format: 'DD/MM/YYYY'
				});
			    
			    $("#membershipExpires").on("dp.change", function(e) {
			        $('#membershipCommencementDate').data("DateTimePicker").maxDate(e.date);
			    });

			    $('#totalConsultancyHoursPurchased').on('change', function(){members.calculateConsultingHours();});
			    $('#consultancyHoursUsed').on('change', function(){members.calculateConsultingHours();});
			    $('#AdditionalConsultinghoursbilled').on('change', function(){members.calculateConsultingHours();});

			    memberAwardStatsBtn.on('click', function(){
			    	members.showMemberAwardStatsForm();
			    });

				this.initAddEditForm();
				this.setDate();
				this.checkState();
				this.updateCategoryList();
				this.recalculateNextDue();
				this.calculateConsultingHours();

				funcAddEditHolder = members.showLogs;
			},
			catDrop: function(ev, holder) {
				ev.preventDefault();
    			
    			if($(ev.target).parents('ul:first').length==0) {
    				$('#'+catItemId).appendTo($(ev.target));
    			}
    			else {
    				var li = ($(ev.target)['context'].localName == 'li') ? $(ev.target) : $(ev.target).parents('li:first');
    				$('#'+catItemId).insertBefore(li);
    			}

    			this.updateCategoryList();
			},
			catAllowDrop: function(ev) {
			    ev.preventDefault();
			},
			catDrag:function(ev) {
				catItemId = ev.target.id;
				ev.originalEvent.dataTransfer.setData("text/plain", ev.target.id);
				
			},
			updateCategoryList: function(){
				catSelected.html('');
				$.each(catSelectedHolder.find('li'), function(k,v){
					var checkbox = $('<input type="checkbox" checked name="memberCategory[]" value="'+$(v).data().catid+'" />');
					checkbox.appendTo(catSelected);
				});
			},
			staffList: function(){
				if(staffList.length>0) {
					$.each(staffList, function(k,v){
						members.staffAddRow(v);
					});
				}
			},
			staffAddRow: function(data){
				var row 		= staffRowCopy.clone().removeClass('staff-copy hide').addClass('row-staff');
				var firstName 	= row.find('.staff-fn');
				var lastName 	= row.find('.staff-ln');
				var email 		= row.find('.staff-email');
				var mobile 		= row.find('.staff-mobile');
				var direct 		= row.find('.staff-direct');
				var position 	= row.find('.staff-position');
				var delBtn 		= row.find('.staff-delete');

				firstName.attr({name:'staff[]'}).val(data.name);
				lastName.attr({name:'staff[]'}).val(data.lastName);
				email.attr({name:'staff[]'}).val(data.email);
				mobile.attr({name:'staff[]'}).val(data.mobile);
				direct.attr({name:'staff[]'}).val(data.direct);
				
				if(data.myPosition == 0) {
					position.attr({name:'staff[]'}).val('');
				}
				else {
					position.attr({name:'staff[]'}).val([data.myPosition]);
				}
				

				delBtn.on('click', function(){members.staffDeleteRow(this)});

				row.appendTo(staffHolder);

				data = row = firstName = lastName = email = mobile = direct = position = delBtn = null;
			},
			staffDeleteRow: function(button){
				//popup confirmation first

				$(button).parents('tr').remove();
			},
			getAddForm: function(){

				swal({
				    title: "Cancel Changes",
				    text: "Are you sure?",
				    icon: "warning",
				    buttons: ["Cancel", true],
				    dangerMode: false,
				})
				.then(willDelete => {
				    if (willDelete) {
						var request = $.ajax({
				          url: "<?php echo base_url(); ?>admin/members/action",
				          method: "POST",
				          data: {id:0}
				        });

				        request.done(function( data ) {
				            if(data!=null) {
				            	searchTab.trigger('click');
				            	if(membersAddEdit!=null) membersAddEdit = null;
				            	membersAddEditHolder.html(data);
				            	addTab.html('<i class="fa fa-plus"></i> Add');
				            	data = null;
				            }
		        		});	

		        		request.error(function(_data){
		        			if(_data.readyState == 4 && _data.status == 200) {//Session expired and redirected
		        				swal({
								    title: "Session Expired!",
								    text: "Reloading page.",
								    icon: "warning",
								    dangerMode: false,
								})
								.then(willDelete => {
								    if (willDelete) {
								    	window.location.href = '<?php echo base_url();?>admin/login';
								    }
								});
		        			}
		        		});
					}
				});

							
			},
			initAddEditForm: function() {
				
				$('#membershipType').on('change', function(){
					
					members.hideShowFields(parseInt($(this).val()));
				});

				members.hideShowFields(parseInt($('#membershipType').val()));

				$('.log-btn').on('click', function(){
					var data = {id:memberId, action: $(this).data('type')};
					members.showLogs(data);
				});
				
			},
			hideShowFields: function(val){

				$('#generalCompulsoryFlag1').show();
				$('#generalCompulsoryFlag2').show();
				$('#generalCompulsoryFlag5').show();
				$('#generalCompulsoryFlag6').show();
				$('#usernameCompulsoryFlag').show();
				$('#passwordCompulsoryFlag').show();
				$('#everyonebutgeneral1').show();
				$('#everyonebutgeneral2').show();
				$('#everyonebutgeneral3').show();
				$('#everyonebutgeneral4').show();
				$('#everyonebutgeneral5').show();
				$('#everyonebutgeneral6').show();
				$('#everyonebutgeneral7').show();
				$('#interests').show();
				$('#statuscontainer').show();
				$('#dateCancelledContainer').show();
				$('#reasonCancelledContainer').show();

				var idShow = ['everyonebutgeneral1','everyonebutgeneral2','everyonebutgeneral3','everyonebutgeneral4','everyonebutgeneral5','everyonebutgeneral6','everyonebutgeneral7','interests','statuscontainer','dateCancelledContainer','reasonCancelledContainer'];

				this.disableFields(idShow, false);

				$('#other').hide();
				$('#other_plans').hide();
				$('#unsubscribecontainer').hide();
				$('#removedcontainer').hide();
				$('#linkedmemberidcontainer').hide();
				$('#expirescompulsory').hide();

				var idHide = ['other','other_plans','unsubscribecontainer','removedcontainer','linkedmemberidcontainer'];

				this.disableFields(idHide, true);

				if(val < 2) {
					$('#expirescompulsory').show();
					$('#other').show();
					$('#other_plans').show();

					this.disableFields(['other','other_plans'], false);
				}
				else if(val == 4 || val == 5){
					$('#unsubscribecontainer').show();
					$('#expirescompulsory').show();
					$('#generalCompulsoryFlag1').hide();
					$('#generalCompulsoryFlag2').hide();
					$('#dateCancelledContainer').hide();
					$('#reasonCancelledContainer').hide();
					$('#interests').hide();

					this.disableFields(['unsubscribecontainer'], false);

					if(val == 5) {
						$('#linkedmemberidcontainer').show();
						this.disableFields(['linkedmemberidcontainer'], false);
					}

					idHide = ['generalCompulsoryFlag1','generalCompulsoryFlag2','dateCancelledContainer','reasonCancelledContainer','interests'];

					this.disableFields(idHide, true);
				}
				else if(val == 6 || val >= 8 && val <= 12 || val > 100) {
					$('#generalCompulsoryFlag1').hide();
					$('#generalCompulsoryFlag2').hide();
					$('#generalCompulsoryFlag5').hide();
					$('#generalCompulsoryFlag6').hide();
					$('#generalCompulsoryFlag7').hide();
					$('#everyonebutgeneral1').hide();
					$('#everyonebutgeneral2').hide();
					$('#everyonebutgeneral3').hide();
					$('#everyonebutgeneral4').hide();
					$('#everyonebutgeneral5').hide();
					$('#everyonebutgeneral6').hide();
					$('#everyonebutgeneral7').hide();
					$('#dateCancelledContainer').hide();
					$('#reasonCancelledContainer').hide();
					$('#usernameCompulsoryFlag').hide();
					$('#passwordCompulsoryFlag').hide();
					$('#removedcontainer').show();
					$('#interests').show();
					$('#statuscontainer').show();

					idHide = ['everyonebutgeneral1','everyonebutgeneral2','everyonebutgeneral3','everyonebutgeneral4','everyonebutgeneral5','everyonebutgeneral6','everyonebutgeneral7','interests','statuscontainer','dateCancelledContainer','reasonCancelledContainer'];

					this.disableFields(idHide, true);
					this.disableFields(['removedcontainer','interests','statuscontainer'], false);
				}
				else if(val == 7){
					$('#howdidtheyhearcompulsory').hide();
					$('#interests').hide();
					this.disableFields(['interests'], true);
				}
			},
			disableFields: function(ids, hide){
				$.each(ids, function(k,v){
					$('#' + v + ' .form-control').attr({disabled:hide});
				});
			},
			showLogs: function(data){
				//console.log(data);

				var title = '';
				var tempTable = null;
				var col_data = [];
				var loaderText = 'Fetching list... <i class="fa fa-spinner fa-spin"></i>';

				logContent.html('');

				tableId = funcFilterHolder.generateID.call(this);

				$('#email-log-search').hide();
				$('#download-log-search').hide();
				$('#' + tableId + '_filter').show();

				if(data.action == 'download-log') {
					title = 'Download Log';
					tempTable = downloadTable.clone().attr({id:tableId});
					col_data = [{data: 'name'}, {data:'mydate'}, {data:'mysection'}];
					$('#member-logs .modal-dialog').addClass('modal-md').removeClass('modal-xl');

					$('#download-log-search select').unbind('change').bind('change', function(){
						$('#' + tableId + '_filter input').val($(this).val()).keyup();
					});

					$('#download-log-search').show();
				}
				else if(data.action == 'email-log') {
					title = 'Email Log';
					tempTable = emailTable.clone().attr({id:tableId});
					col_data = [
								{data: 'emailAddress'}, 
								{data:'fromAddress'}, 
								{data:'subject'},
								{data:'myDateTime'},
								{data:'sent'},
								{data:'openDateTime'},
								{data:'logId'}
							];
					$('#member-logs .modal-dialog').addClass('modal-xl').removeClass('modal-md');

					$('#email-log-search select').unbind('change').bind('change', function(){
						logContent.html('');
						tableId = funcFilterHolder.generateID.call(this);
						data.emailtype = $(this).val();
						tempTable = emailTable.clone().attr({id:tableId});
						tempTable.appendTo(logContent);

						$('#log-loader').html(loaderText).show();
						members.tempFunc.call(this, col_data, data);
					});

					$('#email-log-search').show();
				}
				else if(data.action == 'login-log') {
					title = 'Login Log';
					tempTable = loginTable.clone().attr({id:tableId});
					col_data = [{data:'myDateTime'},{data:'ip'}];
					$('#member-logs .modal-dialog').addClass('modal-md').removeClass('modal-xl');
				}

				tempTable.appendTo(logContent);

				$('#member-logs h4.modal-title').html(title + ' | <small></small>');
				$('#member-logs h4.modal-title small').text('UID: ' + data.id);

				$('#log-loader').html(loaderText).show();
				
				$('#member-logs').modal({backdrop: "static"});

				members.tempFunc = function(col_data, data){

					var request = $.ajax({
			          url: "<?php echo base_url(); ?>admin/members/logs",
			          method: "POST",
			          data: data,
			          dataType: 'json'
			        });

			        request.done(function( res ) {
			            if(res!=null) {
			            	$(tempTable).DataTable({
			            		data: res,
			            		columns: col_data
			            	});
			            	$('#log-loader').hide();
			            	$('#' + tableId + '_filter').hide();
			            }
	        		});

	        		request.error(function(_data){
	        			if(_data.readyState == 4 && _data.status == 200) {//Session expired and redirected
	        				swal({
							    title: "Session Expired!",
							    text: "Reloading page.",
							    icon: "warning",
							    dangerMode: false,
							})
							.then(willDelete => {
							    if (willDelete) {
							    	window.location.href = '<?php echo base_url();?>admin/login';
							    }
							});
	        			}
	        		});
        		};

        		members.tempFunc.call(this, col_data, data);
			},
			addUpdate: function(){

				var isEdit = $.isNumeric(memberId);

				swal({
				    title: (isEdit)?"Update Member." : "Add Member",
				    text: "Are you sure?",
				    icon: "warning",
				    buttons: ["Cancel", true],
				    dangerMode: false,
				})
				.then(willDelete => {
				    if (willDelete) {
						var request = $.ajax({
				          url: "<?php echo base_url(); ?>admin/members/update_member",
				          method: "POST",
				          data: $('#members-add-form').serialize(),
				          dataType: 'json'
				        });

				        request.done(function( res ) {
				            if(res!=null) {

				            	if(isEdit) {
				            		if(Object.keys(res.error).length>0) {
				            			var errMsg = '';

				            			$.each(res.error, function(k, v){
				            				errMsg += '* ' + v + '\n';
				            			});

				            			new PNotify({
											title: 'Invalid or Required!',
											text: errMsg,
											type: 'error',
											styling: 'bootstrap3'
			                            });

				            			swal({title:"Update Failed!",text:"Some fields are required (*) or have invalid values",icon:'error',button:false, timer:3000});
				            		}
				            		else {

				            			var membershipType = parseInt($('#membershipType option:selected').val());
				            			var memberStatus = parseInt($('#memberStatus option:selected').val());
				            			var statusText = '';

				            			if(memberStatus == 1) {
											if(membershipType == 4 || membershipType == 5) {
												statusText = 'Current';
											}
											else {
												statusText = 'Active';
											}
				            			}
				            			else if(memberStatus == 2){
				            				statusText = 'Suspended';
				            			}
				            			else if(memberStatus == 3) {
				            				if(membershipType == 4 || membershipType == 5) {
												statusText = 'Unsubscribed';
											}
											else {
												statusText = 'Cancelled';
											}
				            			}

				            			var data = {
				            				id: 						memberId,
				            				memberStatus: 				statusText,
				            				company: 					$('#company').val(),
				            				contactName: 				$('#contactName').val(),
				            				membershipType: 			$('#membershipType option:selected').text(),
				            				state: 						$('#state-add-edit option:selected').text(),
				            				membershipInvestment: 		$('#membershipInvestment').val(),
				            				membershipCommencementDate: $('#membershipCommencementDate input').val(),
				            				membershipExpires: 			$('#membershipExpires input').val(),
				            				nextAuditDue: 				$('#nextAuditDue').val(),
				            				consultingHoursRemaining: 	$('#consultingHoursRemaining').text()
				            			};

				            			funcFilterHolder.applyUpdateChanges.call(this, data);
				            			swal({title:"Updated!",text:'Changes have been saved.',icon:'success',button:false, timer:3000});
				            		}
				            		
				            	}
				            	else {
				            		if(Object.keys(res.error).length>0) {
										var errMsg = '';

				            			$.each(res.error, function(k, v){
				            				errMsg += '* ' + v + '\n';
				            			});

				            			new PNotify({
											title: 'Invalid or Required!',
											text: errMsg,
											type: 'error',
											styling: 'bootstrap3'
			                            });

			                            swal({title:"Add Failed!",text:"Some fields are required (*) or have invalid values",icon:'error',button:false, timer:3000});
				            		}
				            		else {
				            			swal({title:"Success!",text:"Added new member with id: " + res.id, icon:'success',button:false, timer:3000});
				            		}
				            	}
				            	console.log(res);
				            }
		        		});

				      	request.error(function(_data){
		        			if(_data.readyState == 4 && _data.status == 200) {//Session expired and redirected
		        				swal({
								    title: "Session Expired!",
								    text: "Reloading page.",
								    icon: "warning",
								    dangerMode: false,
								})
								.then(willDelete => {
								    if (willDelete) {
								    	window.location.href = '<?php echo base_url();?>admin/login';
								    }
								});
		        			}
		        		});
					}
				});        						
			},
			setDate: function() {
				
				$.each($('#members-add-form .date'), function(k,v){
					
					if($(v).hasClass('date-time')) {
						$(v).datetimepicker({
							format: 'DD/MM/YYYY hh:mm A'
						});
					}
					else {
						$(v).datetimepicker({
							format: 'DD/MM/YYYY'
						});
					}
				});
			},
			addCommentDate: function(){
				var str = commentsText.val();

				var now = moment().tz('<?php echo PPM_TIMEZONE; ?>').format('DD/MM/YYYY h:mm:ss A');

				commentsText.val(now + ' - \n\n' + str);
			},
			showOther: function(select, input) {
				if(select.val() == 'Other' || select.val() == 'Other - Text') {
					if(select.attr('id') == 'howdidtheyhear') $('#howdidtheyhearcompulsory').show();
					else if(select.attr('id') == 'removedReason') $('.removedReasonOthercompulsory').show();
					else if(select.attr('id') == 'unsubscribeReason') $('.unsubscribeReasonOthercompulsory').show();
					else if(select.attr('id') == 'reasonCancelled') $('.reasonCancelledOthercompulsory').show();
					input.attr({disabled:false}).show().focus();
				}
				else {
					if(select.attr('id') == 'howdidtheyhear') $('#howdidtheyhearcompulsory').hide();
					else if(select.attr('id') == 'removedReason') $('.removedReasonOthercompulsory').hide();
					else if(select.attr('id') == 'unsubscribeReason') $('.unsubscribeReasonOthercompulsory').hide();
					else if(select.attr('id') == 'reasonCancelled') $('.reasonCancelledOthercompulsory').hide();
					input.attr({disabled:true}).hide();
				}

			},
			checkState: function() {
				if (state.val() == '1') {
					$('#members-add-form .state-subregion select').attr({disabled:false});
					$('#members-add-form .state-subregion').show();
				} else {
					$('#members-add-form .state-subregion select').attr({disabled:true});
					$('#members-add-form .state-subregion').hide();
				}
			},
			recalculateNextDue: function(e){
				if(e!=null){
					_lastAuditDate = e;
				}

				var nextAuditDue 		= $('#nextAuditDue');
				var frequencyOfAudits 	= $('#frequencyOfAudits');
				var nextAuditDueVal     = '';

				if(_lastAuditDate!=null) {
					var addVal 	= frequencyOfAudits.val();
					var date 	= _lastAuditDate.date.format('DD/MM/YYYY').split('/');
					var day 	= date[0];
					var month 	= date[1];
					var year 	= date[2];

					addVal = (addVal=='') ? 0 : parseInt(addVal);

					switch(addVal){

						case 1:
							nextAuditDue.val(moment(month + '-' + day + '-' + year, "MM-DD-YYYY").add(14, 'days').format('DD/MM/YYYY'));
						break;

						case 2:
							nextAuditDue.val(moment(month + '-' + day + '-' + year, "MM-DD-YYYY").add(1, 'months').format('DD/MM/YYYY'));
						break;

						case 3:
							nextAuditDue.val(moment(month + '-' + day + '-' + year, "MM-DD-YYYY").add(2, 'months').format('DD/MM/YYYY'));
						break;

						case 4:
							nextAuditDue.val(moment(month + '-' + day + '-' + year, "MM-DD-YYYY").add(3, 'months').format('DD/MM/YYYY'));
						break;

						case 5:
							nextAuditDue.val(moment(month + '-' + day + '-' + year, "MM-DD-YYYY").add(4, 'months').format('DD/MM/YYYY'));
						break;

						case 6:
							nextAuditDue.val(moment(month + '-' + day + '-' + year, "MM-DD-YYYY").add(6, 'months').format('DD/MM/YYYY'));
						break;

						case 7:
							nextAuditDue.val(moment(month + '-' + day + '-' + year, "MM-DD-YYYY").add(12, 'months').format('DD/MM/YYYY'));
						break;

						default:
							nextAuditDue.val(_lastAuditDate.date.format('DD/MM/YYYY'));
						break;
					}

				}
			},
			resetNewsletter: function(obj){
				swal({
				    title: "Reset Newsletter Download",
				    text: "Are you sure?",
				    icon: "warning",
				    buttons: ["Cancel", true],
				    dangerMode: false,
				})
				.then(willDelete => {
				    if (willDelete) {
						var request = $.ajax({
				          url: "<?php echo base_url(); ?>admin/members/reset_newsletter_download",
				          method: "POST",
				          data: {id:$('#member-id').val(),eNewsId:obj.data().newsid},
				          dataType: 'json'
				        });

				        request.done(function( res ) {
				            if(res!=null) {
				            	if(res.success) {
				            		swal({title:"Success!",text:"Newsletter Download for ( "+ obj.data().news.trim() +" ) has been reset.",icon:'success',button:false, timer:3000});
				            	}
				            	else {
				            		swal({title:"Reset Newsletter Download",text:"Reset failed.",icon:'error',button:false, timer:3000});
				            	}
				            }
		        		});

		        		request.error(function(_data){
		        			if(_data.readyState == 4 && _data.status == 200) {//Session expired and redirected
		        				swal({
								    title: "Session Expired!",
								    text: "Reloading page.",
								    icon: "warning",
								    dangerMode: false,
								})
								.then(willDelete => {
								    if (willDelete) {
								    	window.location.href = '<?php echo base_url();?>admin/login';
								    }
								});
		        			}
		        		});
					}
				});
			},
			calculateConsultingHours: function() {
				var consultancyHoursUsed 				= $('#consultancyHoursUsed');
				var totalConsultancyHoursPurchased 		= $('#totalConsultancyHoursPurchased');
				var AdditionalConsultinghoursbilled 	= $('#AdditionalConsultinghoursbilled');
				var totalBilledHoursToDateLabel 		= $('#totalBilledHoursToDateLabel');
				var totalBilledHoursToDate 				= $('#totalBilledHoursToDate');
				var consultingHoursRemaining 			= $('#consultingHoursRemaining');
				var consultingHoursRemainingVal 		= 0;
				var consultancyHoursUsedVal 			= consultancyHoursUsed.val();
				var totalConsultancyHoursPurchasedVal 	= totalConsultancyHoursPurchased.val();
				var AdditionalConsultinghoursbilledVal 	= AdditionalConsultinghoursbilled.val();
				var totalBilledHoursToDateVal 			= 0;

				if(consultancyHoursUsedVal<0) {
					consultancyHoursUsedVal = 0;
					consultancyHoursUsed.val(0);
				}

				if(totalConsultancyHoursPurchasedVal<0) {
					totalConsultancyHoursPurchasedVal = 0;
					totalConsultancyHoursPurchased.val(0);
				}

				if(AdditionalConsultinghoursbilledVal<0 || !$.isNumeric(AdditionalConsultinghoursbilledVal)) {
					AdditionalConsultinghoursbilledVal = 0;
					AdditionalConsultinghoursbilled.val(0);
				}

				consultancyHoursUsedVal = ($.isNumeric(consultancyHoursUsedVal)) ? parseFloat(consultancyHoursUsedVal) : 0;
				totalConsultancyHoursPurchasedVal = ($.isNumeric(totalConsultancyHoursPurchasedVal)) ? parseFloat(totalConsultancyHoursPurchasedVal) : 0;

				consultingHoursRemainingVal = (totalConsultancyHoursPurchasedVal>=consultancyHoursUsedVal)?totalConsultancyHoursPurchasedVal-consultancyHoursUsedVal:-1;
				
				if(consultingHoursRemainingVal>=0) {
					consultingHoursRemaining.text(consultingHoursRemainingVal);

					if(consultancyHoursUsedVal>=0) {
						totalBilledHoursToDateVal = parseFloat(AdditionalConsultinghoursbilledVal) + consultancyHoursUsedVal;
						totalBilledHoursToDateLabel.text(totalBilledHoursToDateVal);
						totalBilledHoursToDate.val(totalBilledHoursToDateVal);
					}
				}
				else {
					consultingHoursRemaining.text(0);
					consultancyHoursUsed.val(totalConsultancyHoursPurchasedVal);
				}
			},
			showMemberAwardStatsForm: function(){
				if(monthlyAwardStats!=null) {
					if(monthlyAwardStats.isEdit) {
						monthlyAwardStats.showUpdateButton();
					}
					else {
						monthlyAwardStats.showAddButton();
					}
				}
				memberAwardStatsModal.modal({backdrop: "static"});
			}
		};

		members.init();

		setTimeout(function(){$('#members-add-edit_1').trigger('click');},10);

	    if ($("#members-add-form input.flat")[0]) {
	        $(document).ready(function () {
	            $('#members-add-form input.flat').iCheck({
	                checkboxClass: 'icheckbox_flat-green',
	                radioClass: 'iradio_flat-green'
	            });
	        });
	    }

	    <?php if($isEdit) { ?>
			setTimeout(function(){
			   	if(monthlyAwardStats!=null) {
			    	monthlyAwardStats.populateList(<?php echo (!empty($member_data['monthlyAwardStats'])) ? json_encode($member_data['monthlyAwardStats']) : '{}'; ?>);
			    	monthlyAwardStats.setMember(<?php echo $memberId; ?>, <?php echo (!empty($member)) ? '"' . $member['company'] . '"': ""; ?>);
			    }			
			},10);
	    <?php } ?>    
	
	});
</script>
