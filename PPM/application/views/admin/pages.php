<!-- page content -->
<div class="right_col" role="main" style="min-height: 100vh;">

  <div class="row">
    <div class="col-md-12">
      <h1><i class="fa fa-pagelines"></i> Pages</h1>

      <div class="spacer20"></div>
      <button class="btn btn-primary" type="button" id="btnAddNew"> <i class="fa fa-plus"></i> Add New </button>
      <hr/>
      <div class="spacer20"></div>
      <div class="row">
        <div class="col-md-12">
          <!-- START : LIST TABLE -->
          <table class="table table-striped" id="tblProductList">
            <thead>
              <tr>
                <!-- <td><input type="checkbox" id="chkAllItems" class="flat"></td> -->
                <td>Page Name</td>
                <td>URL</td>
                <td>Parent Page</td>
                <td>Date Created</td>
                <td>Action</td>
              </tr>
            </thead>
            <tbody>
                <?php
                  if(count($list_array['result']) > 0):
                      foreach($list_array['result'] as $arr){
                        //`pid`, `title`, `content`, `parent_id`, `menu`, `page_order`, `user_id`, `banner_top`, `banner_bottom`, `status`, `date_added`
                        if($arr['status'] == 'draft'){
                          $status = ' - <strong><em>Draft</em></strong>';
                        }else{
                          $status = '';
                        }

                        // $current_url = base_url().''.str_replace('+', '%20',urlencode($arr['title']));

                        $current_url = base_url().''. ( (empty($arr['slug'])) ? str_replace(' ','-', strtolower($arr['title'])) : $arr['slug'] );
                        echo '<tr>
                                <!--<td><input type="checkbox" id="chk'.$arr['pid'].'" class="flat"></td>-->
                                <td>'.$arr['title'].''.$status.'</td>
                                <td><a href="'.$current_url.'" target="_blank">'.$current_url.'</a></td>
                                <td>'.getPageTitle($arr['parent_id']).'</td>
                                <td>'.date('m/d/Y H:i:s', strtotime($arr['date_added'])).'</td>
                                <td>
                                  <button class="btn btn-success btnEditPage" type="button" data-pid="'.$arr['pid'].'"><i class="fa fa-pencil"></i></button>
                                  <button class="btn btn-danger btnDeletePage" type="button"  data-pid="'.$arr['pid'].'"><i class="fa fa-trash"></i></button>
                                </td>
                              </tr>
                        ';
                      }
                  endif;
                ?>
            </tbody>
          </table>
                <?php
                  // $b = new Pages();
                  // print_r(getPageTitle('2'));
                  // echo '<pre>';
                  // // print_r($list_array);
                  // echo '</pre>';
                  // if(count($list_array['result']) > 0):
                  //     echo 'hello';
                  //     foreach($list_array['result'] as $arr){
                  //       //`pid`, `title`, `content`, `parent_id`, `menu`, `page_order`, `user_id`, `banner_top`, `banner_bottom`, `status`, `date_added`
                  //       echo $arr['uid'].':'.$arr['username'].'<br>';
                  //     }
                  // endif;
                // $this->ppmsystemlib->get_accepted_mimes();
                ?>
          <!-- END : LIST TABLE -->
        </div>
      </div>

    </div>

  </div>

	<!-- <div class="clearfix"></div> -->
</div>

<div id="member-logs" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

			<div class="modal-header">
				<h4 class="modal-title">Title</h4>
			</div>
			<div class="modal-body">

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>

		</div>
	</div>
</div>

<!-- /page content -->

<style>
.spacer10 {
  clear: both;
  width: 100%;
  height: 10px;
}
.spacer20 {
  clear: both;
  width: 100%;
  height: 20px;
}
.spacer30 {
  clear: both;
  width: 100%;
  height: 30px;
}

#tblProductList thead td {
  font-weight: bold;
}

</style>

<script>
  var tblProdData = "";
  $(document).ready(function(){
    var baseurl = '<?php echo base_url(); ?>';

    $('#chkAllItems').iCheck({
      checkboxClass: 'icheckbox_square',
      radioClass: 'iradio_square',
      increaseArea: '20%' // optional
    });

    // setTimeout(function(){
      tblProdData = $('#tblProductList').DataTable({
        "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        "columnDefs": [{ "orderable": false, "targets": [0, 4] }],
        "order": [[ 0, "asc" ]],
        "scrollY": "400px",
        "scrollCollapse": true,
        "paging": true
      });
    // },1500);

    //EDIT
    $('#tblProductList tbody').on('click', ' td > .btnEditPage', function(){
      // alert('Test' + $(this).data('pid'));
      window.location = baseurl + 'admin/pages/edit/' + $(this).data('pid') + '/';
    });

    //DELETE
    $('#tblProductList tbody').on('click', ' td > .btnDeletePage', function(){
        var page_id = $(this).data('pid');
        // var q = confirm('Are you sure you want to delete the page?');
        var $this = $(this);
        swal({
          title: "Are you sure?",
          text: "Once deleted, you will not be able to recover this data!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            // swal("Poof! Your imaginary file has been deleted!", {
            //   icon: "success",
            // });
            $.ajax({
              type : 'post',
              url  : baseurl + 'admin/pages/delete',
              data : {
                'page_id' : page_id
              },
              beforeSend : function(){

              },
              success : function(result){
                tblProdData.row( $this.parents('tr') ).remove().draw();
                swal("Poof! Page has been Deleted!", {
                  icon: "success",
                });
              }
            });

          } else {
            // swal("Your imaginary file is safe!");
          }
        });

    });

    $('#btnAddNew').click(function(){
      // $.ajax({
      //   type: 'post',
      //   url : baseurl + 'admin/pages/test',
      //   success:function(res){
      //     console.log(res);
      //   }
      // });
      window.location = baseurl + 'admin/pages/add';
    });

  });
</script>
