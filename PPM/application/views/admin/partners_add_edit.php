<style>
	hr.style-two {
	    border: 0;
	    height: 0;
	    border-top: 1px solid rgba(0, 0, 0, 0.1);
	    border-bottom: 1px solid rgba(255, 255, 255, 0.3);
	}

	ul.to_do.sortable {
		overflow-y: auto;
		overflow-x: hidden;
		max-height: 55vh;
	}

	ul.to_do.sortable li {
		min-height: 58px;
	}

	ul.to_do li:hover {
		cursor: pointer;
	}

	ul.to_do li.sortable-placeholder {
		background: none;
		border: 2px dashed rgba(60,118,61,0.5);/*#3c763d;*/
	}

	.sort-order span {
    	font-size: 15px;
		line-height: 35px;
	}

	li.photo-item {
		overflow: hidden;
	}

	li.photo-item:hover {
	    cursor: move; /* fallback if grab cursor is unsupported */
	    cursor: grab;
	    cursor: -moz-grab;
	    cursor: -webkit-grab;
	}

	 /* (Optional) Apply a "closed-hand" cursor during drag operation. */
	li.photo-item:active { 
	    cursor: grabbing;
	    cursor: -moz-grabbing;
	    cursor: -webkit-grabbing;
	}

	.photo-action {
	    font-size: 20px;
	    line-height: 40px;
	    width: 50px;   	
	}

	.drop-here {
		position: absolute;
		z-index: 1;
		top: calc(50% - 10px);
		text-align: center;
		width: 100%;		
	}

	.pages-selected.to_do li {
	    background: #1abb9c!important;
	    color: #fff!important;
	}	

</style>

<div class="container">

	<div class="col-md-12 col-sm-12 col-xs-12">
		<div id="copy-title"class="alert alert-success fade in" role="alert" style="display: none;">
			<strong></strong>
		</div>
		<p class="lead"><small class="text-info small">Compulsary fields are marked by an asterisk ( <i class="fa fa-asterisk text-danger small"></i> )</small></p>

		<form id="add-edit-form" class="form-horizontal form-label-left">
			<input id="data-id" type="hidden" name="id" value="" />
			
			<!-- start accordion -->
			<div class="accordion" id="accordion-add-edit" role="tablist" aria-multiselectable="true">

				<!-- General -->	
				<div class="panel">
					<a class="panel-heading" role="tab" id="contents-add-edit_1" data-toggle="collapse" data-parent="" href="#add-edit_1" aria-expanded="true" aria-controls="add-edit_1">
						<h4 class="panel-title">General</h4>
					</a>
					<div id="add-edit_1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="contents-add-edit_1">
						<div class="panel-body">

							<div class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Name: <i class="fa fa-asterisk text-danger"></i></label>
								<div class="col-md-5 col-sm-12 col-xs-12">
									<input id="data-name" name="name" type="text" class="form-control" placeholder="" value="" />
								</div>
							</div>	

							<div class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Show on Pages: </label>
                                <div class="col-md-9 col-sm-12 col-xs-12">
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

										<div class="checkbox" style="padding-bottom:5px;">
							                <label><span class="badge bg-default">Drag items to Selected Pages:</span></label>
							                <button type="button" id="select-all-btn" class="btn btn-xs btn-warning pull-right">Select All</button>
										</div>							

										<div style="max-height: 285px; overflow-y: scroll; border-radius: 5px;border: 1px solid #f3f3f3;">
											<h2 class="drop-here"><i>Drop here ...</i></h2>
											<ul id="pages-available" class="to_do" style="min-height: 268px; position:relative; z-index:2;">
											<?php	
											$pages_selected_arr = array();

											if(isset($pagesArr)) {
												foreach ($pagesArr as $key=>$value) {

													
											?>
												<li id="pages-<?php echo $value['slug']; ?>" data-id="<?php echo $value['slug']; ?>" style="margin:4px;" draggable="true">
													<p><?php echo $value['title']; ?></p>
												</li>
											<?php
													

												}
											}

											?>

											</ul>

										</div>
				
									</div>

									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

										<div class="checkbox" style="padding-bottom:5px;">
											<label><span class="badge bg-green">Selected Pages:</span></label>
											<button type="button" id="deselect-all-btn" class="btn btn-xs btn-danger pull-right">Deselect All</button>
										</div>
										<div style="max-height: 285px; overflow-y: auto; border-radius: 5px;border: 1px solid #f3f3f3;">
											<h2 class="drop-here"><i>Drop here ...</i></h2>
											<ul id="pages-selected" class="to_do pages-selected" style="min-height: 268px; position:relative; z-index:2;">
											</ul>
										</div>
										<div id="acs-selected" class="hide"></div>
				
									</div>								
								</div>
							</div>                            
		

						</div>	
					</div>
				</div>
				<!-- General -->

				<!-- Partners -->	
				<div id="files-container" class="panel">
					<a class="panel-heading" role="tab" id="contents-add-edit_4" data-toggle="collapse" data-parent="" href="#add-edit_4" aria-expanded="false" aria-controls="add-edit_4">
							<h4 class="panel-title">Partners <label id="partners-count" class="small"></label></h4>
					</a>
					<div id="add-edit_4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="contents-add-edit_4">
						<div class="panel-body">
							<ul class="to_do" style="padding: 0px 5px;">
								<li>
									<div class="row">
										<div class="col-md-1 col-sm-12 col-xs-12 text-center">
											<label>Order</label>
										</div>
										<div class="col-md-5 col-sm-12 col-xs-12 text-center">
											<label>Photo</label>
										</div>
										<div class="col-md-4 col-sm-12 col-xs-12 text-center">
											<label>Link</label>
										</div>
										<div class="col-md-1 col-sm-12 col-xs-12 text-center">
											<label>Action</label>
										</div>
									</div>
								</li>		
							</ul>
							<ul id="partners-holder" class="sortable to_do" style="padding: 0px 5px;">
								<li class="sortable-item-copy hide">
									<div class="row">
										<div class="col-md-1 col-sm-12 col-xs-12 text-center sort-order">
											<span>1</span>
											<input type="hidden" name="partnersItems[]" value="" disabled="true" class="data-order"/>
										</div>
										<div class="col-md-5 col-sm-12 col-xs-12">
											<div class="input-group">
												<input type="text" name="partnersItems[]" class="form-control data-photo" disabled="true">
												<span class="input-group-btn">
												<button type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
												</span>
											</div>
										</div>
										<div class="col-md-4 col-sm-12 col-xs-12">
											<div class="form-group">
												<input name="partnersItems[]" type="text" class="form-control data-link" placeholder="" value="" disabled="true" />
											</div>
										</div>
										<div class="col-md-1 col-sm-12 col-xs-12 text-center">
											<a class="photo-action action-view" href="javascript:;"><i class="fa fa-eye text-success"></i></a>
											<span class="photo-action"> | </span>
											<a class="photo-action action-delete" href="javascript:;"><i class="fa fa-trash text-danger"></i></a>
										</div>
									</div>
								</li>
							</ul>

							<button id="partners-add-btn" type="button" class="btn btn-sm btn-success pull-right"><i class="fa fa-plus"></i> Add Row</button>

						</div>
					</div>
				</div>	
				<!-- Partners -->

			</div>
	
		</form>

	</div>	

	<div class="row">
		<div class="panel">
			<div class="col-md-12 text-right">
				<button id="cancel-btn" type="button" class="btn btn-sm btn-warning" style="display: none;">Cancel Changes</button>
				<button id="update-btn" type="button" class="btn btn-sm btn-success" style="display: none;">Update</button>
				<button id="add-btn" type="button" class="btn btn-sm btn-success add-btn">Add</button>
			</div>
			<div class="clearfix"></div>
		</div>	
	</div>

</div>

<div class="row hide">
	<div class="col-md-5">
		<div class="input-group">
		<input type="text" class="form-control">
		<span class="input-group-btn">
		<button id="ppm-browse-image" type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
		</span>
		</div>
	</div>
</div>

<script type="text/javascript" src="<?php echo base_url() . 'assets/js/fancybox-master/dist/jquery.fancybox.min.js'; ?>"></script>

<script>
	var json = null;
	$(document).ready(function(){
		var form = $('#add-edit-form');
		var fields = {
						id: 			$('#data-id'),
						name: 			$('#data-name')
					};
		var pages = null;

		var contents = {
			_partners: null,
			img_path: '<?php echo base_url() . 'assets/uploads/files/'; ?>',
			init: function(){

				addBtn.bind('click', function(){
					contents.submitForm();
				});

				updateBtn.bind('click', function(){
					contents.submitForm();
				});

				contents._partners = contents.carousel('partners', '#partners-holder', '.sortable-item-copy', '#partners-count');

				$('#partners-add-btn').on('click', function(){
					contents._partners.addPhotoRow();
				});

				contents._partners.initSortable();

				pages = this.initDragDrop({holder:'pages-available',selectedHolder:'pages-selected',selected:'acs-selected',name:'pages', type: 'pages'});

				$('#select-all-btn').on('click', function(){
					pages.selectAll();
				});

				$('#deselect-all-btn').on('click', function(){
					pages.resetList();
				});				

			},
			initDragDrop: function(params) {

				var dragDrop = {
					holder: 		$('#' + params.holder),
					selectedHolder: $('#' + params.selectedHolder),
					selected: 		$('#' + params.selected),
					itemId: 		'',
					name: 			params.name,
					type: 			params.type,
					init: function(){
						this.holder.on('drop', function(e){dragDrop.drop(e,$(this));});
						this.selectedHolder.on('drop', function(e){dragDrop.drop(e,$(this));});
						this.holder.on('dragover', function(e){dragDrop.allowDrop(e);});
						this.selectedHolder.on('dragover', function(e){dragDrop.allowDrop(e);});
						this.holder.find('li').on('dragstart', function(e){dragDrop.drag(e);});
						this.selectedHolder.find('li').on('dragstart', function(e){dragDrop.drag(e);});
					},
					drop: function(ev, holder) {
						ev.preventDefault();
		    			
		    			if($(ev.target).parents('ul:first').length==0) {
		    				$('#'+this.itemId).appendTo($(ev.target));
		    			}
		    			else {
		    				var li = ($(ev.target)['context'].localName == 'li') ? $(ev.target) : $(ev.target).parents('li:first');
		    				$('#'+this.itemId).insertBefore(li);
		    			}

		    			this.updateList();
					},
					allowDrop: function(ev) {
					    ev.preventDefault();
					},
					drag:function(ev) {
						this.itemId = ev.target.id;
						ev.originalEvent.dataTransfer.setData("text/plain", ev.target.slug);
						
					},					
					updateList: function(){
						this.selected.html('');
						var ctr = 0;
						$.each(this.selectedHolder.find('li'), function(k,v){
							var checkbox = $('<input type="checkbox" checked name="'+dragDrop.name+'[]" value="'+$(v).data().id+'" />');
							checkbox.appendTo(dragDrop.selected);
							ctr++;
						});
					},
					resetList: function(){
						this.selected.html('');
						$.each(this.selectedHolder.find('li'), function(k,v){
							$(v).appendTo(dragDrop.holder);
						});
					},
					populateList: function(ids){
						$.each(ids, function(k,v){
							$('#'+dragDrop.type + '-' + v).appendTo(dragDrop.selectedHolder);
						});

						this.updateList();
					},
					selectAll: function(){
						this.selected.html('');
						$.each(dragDrop.holder.find('li'), function(k,v){
							$(v).appendTo(dragDrop.selectedHolder);
						});

						this.updateList();
					}
				}

				dragDrop.init();

				return dragDrop;
			},
			submitForm: function(){			

				var isEdit = $.isNumeric(fields.id.val());

				swal({
				    title: (isEdit)?"Update Item" : "Add Item",
				    text: "Are you sure?",
				    icon: "warning",
				    buttons: ["Cancel", true],
				    dangerMode: false,
				})
				.then(willSubmit => {
				    if (willSubmit) {
						var request = $.ajax({
				          url: "<?php echo base_url(); ?>admin/partners/add_update",
				          method: "POST",
				          data: form.serialize(),
				          dataType: 'json'
				        });

				        request.done(function( res ) {
				            if(res!=null) {

				            	if(isEdit) {
				            		if(res.hasOwnProperty('error')) {
				            			var errMsg = '';

				            			$.each(res.error, function(k, v){
				            				errMsg += '* ' + v + '\n';
				            			});

				            			new PNotify({
											title: 'Invalid or Required!',
											text: errMsg,
											type: 'error',
											styling: 'bootstrap3'
			                            });

				            			swal({title:"Update Failed!",text:"Some fields are required (*) or have invalid values",icon:'error',button:false, timer:3000});
				            		}
				            		else {
				            			filter.applyUpdateChanges(res);

				            			swal({title:"Updated!",text:'Changes have been saved.',icon:'success',button:false, timer:3000});
				            		}
				            		
				            	}
				            	else {
				            		if(res.hasOwnProperty('error')) {
										var errMsg = '';

				            			$.each(res.error, function(k, v){
				            				errMsg += '* ' + v + '\n';
				            			});

				            			new PNotify({
											title: 'Invalid or Required!',
											text: errMsg,
											type: 'error',
											styling: 'bootstrap3'
			                            });

			                            swal({title:"Add Failed!",text:"Some fields are required (*) or have invalid values",icon:'error',button:false, timer:3000});
				            		}
				            		else {
				            			fields.id.val(res.id);
				            			contents.showUpdateButton();
				            			addTab.html('<i class="fa fa-pencil"></i> Edit');
				            			swal({title:"Success!",text:"Added item with id: " + res.id,button:false, timer:3000});
				            			filter.addNewItem(res);
				            		}
				            	}
				            }
		        		});

				      	request.error(function(_data){
		        			if(_data.readyState == 4 && _data.status == 200) {//Session expired and redirected

		        				swal({
								    title: "Session Expired!",
								    text: "Reloading page.",
								    icon: "warning",
								    dangerMode: true,
								})
								.then(ok => {
								    if (ok) {
								    	window.location.href = '<?php echo base_url();?>admin/login';
								    }
								});
		        			}
		        		});
					}
				}); 

			},
			resetForm: function(){

				$.each(fields, function(k,v){
					if(v.length>0) {
						v.val('');
					}
				});

				pages.resetList();
			},
			populateForm: function(data){

				if(data!=null) {
					json = data;

					this.resetForm();

					setTimeout(function(){
						$.each(data, function(k,v){
							if(fields.hasOwnProperty(k)  && fields[k].length>0) {
								fields[k].val(v);
							}
							else {
								if(k == 'partners') {
									if(v!=null) contents._partners.populateRow(v);
								}
								else if(k == 'pages') {
									try {
										pages.populateList(v);
									}
									catch(e) {}
								}
							}
						});
						
						contents.showUpdateButton();

						if(addTab!=null) addTab.click();
					}, 50);
				}
			},
			showUpdateButton: function(){
				cancelBtn.show();
				updateBtn.show();
				addBtn.hide();	
			},
			showAddButton: function(){
				cancelBtn.hide();
				updateBtn.hide();
				addBtn.show();	
			},
			cancelUpdate: function(){

				swal({
				    title: "Cancel Changes",
				    text: "Are you sure?",
				    icon: "warning",
				    buttons: ["Cancel", true],
				    dangerMode: false,
				})
				.then(willCancel => {
				    if (willCancel) {
				    	contents.resetTab();
				    }
				});
			},
			resetTab: function(){
		    	addTab.html('<i class="fa fa-plus"></i> Add');
		    	contents.resetForm();
				contents.showAddButton();
				contents._partners.resetRows();
				searchTab.trigger('click');
			},
			carousel: function(type, photoHolder, photoRowCopy, photoCount){
				var _contents = {
					type: type,
					photoHolder: $(photoHolder),
					photoRowCopy: $(photoHolder + ' ' + photoRowCopy),
					photoCount: $(photoCount),
					initSortable: function(){

						this.updateSortOrder();

						$('.sortable').sortable('destroy')
						$('.sortable').sortable().unbind('sortupdate').bind('sortupdate', function() {
							_contents.updateSortOrder();
						});;
					},
					updateSortOrder: function() {
						var total = this.photoHolder.find('.sort-order').length;

						$.each(this.photoHolder.find('.sort-order'), function(k,v){
							var order = parseInt(k)+1;
							$(v).find('span').text(order);
							$(v).find('input[type=hidden]').val(order);
						});
					},
					addPhotoRow: function(data) {
						var row 		= this.photoRowCopy.clone().removeClass('sortable-item-copy hide').addClass('photo-item');
						var browseBtn 	= row.find('button.ppm-browse');
						var viewBtn 	= row.find('a.action-view');
						var deleteBtn 	= row.find('a.action-delete');

						row.appendTo(this.photoHolder);

						var order 		= row.find('.data-order');
						var photo 		= row.find('.data-photo');
						var link 		= row.find('.data-link');

						if(this.type == 'featured') {
							var icon 		= row.find('.data-icon');
							var title 		= row.find('.data-title');
						}

						var _file = 'javascript:;';

						if(data!=null) {
							order.val(data.order);
							photo.val(data.photo);
							link.val(data.link);

							if(this.type == 'featured') {
								icon.val(data.icon);
								title.val(data.title);
							}

							_file = (data.photo == '') ? 'javascript:;' : contents.img_path + data.photo;
						}

						var photo_id = filter.generateID();

						photo.attr({id: photo_id});

						viewBtn.attr({href:_file, 'data-fancybox':"images"});
						this.viewPhotoRow(viewBtn, photo_id);		

						this.initSortable();

						$.each(browseBtn, function(i,v){
							var id = filter.generateID();
							$(v).attr({id: id});

							PPMGallery.initButton(id);
						});

						row.find('input').attr({disabled:false});

						this.updatePhotoCount();
						this.deletePhotoRow(deleteBtn);
					},
					updatePhotoCount: function() {
						this.photoCount.text('('+this.photoHolder.find('.photo-item').length + ')');
					},
					viewPhotoRow: function(btn, photo_id){
						$('#' + photo_id).on('change', function() {
							_contents.checkPhotoExist(contents.img_path + $(this).val())
							.on("error", function(e) {
								$(btn).attr({href: 'javascript:;'});
							})
							.on("load", function(e) {
								$(btn).attr({href: contents.img_path + $('#' + photo_id).val()});
							});
						});
					},
					checkPhotoExist(src) {
						return $("<img>").attr('src', src);
					},
					deletePhotoRow: function(btn){
						$(btn).on('click', function(){
							$(this).parents('li').remove();
							_contents.updateSortOrder();
							_contents.updatePhotoCount();
						});
					},
					resetRows: function(){
						this.photoHolder.html('');
						this.updatePhotoCount();
					},
					populateRow: function(data) {
						this.resetRows();

						$.each(data, function(i,v){
							_contents.addPhotoRow(v);
						});
					}
				};

				return _contents;
			}
		};

		contents.init();

	    addEdit = contents;
    
	});
</script>