<!-- page content -->
<div class="right_col" role="main" style="min-height: 100vh;">

  <div class="row">
    <div class="col-md-12">
      <h1><i class="fa fa-list-alt"></i> Menus</h1>

      <div class="spacer20"></div>
      <button class="btn btn-primary" type="button" id="btnAddNew"> <i class="fa fa-plus"></i> Add New </button>
      <hr/>
      <div class="spacer20"></div>
      <div class="row">
        <div class="col-md-8">
          <!-- START : LIST TABLE -->
          <table class="table table-striped" id="tblMenuList">
            <thead>
              <tr>
                <!-- <td><input type="checkbox" id="chkAllItems" class="flat"></td> -->
                <td>Menu Name</td>
                <td>Code to Display</td>
                <td>Date Created</td>
                <td>Action</td>
              </tr>
            </thead>
            <tbody>
                <?php
                  if(count($list_array['result']) > 0):
                      foreach($list_array['result'] as $arr){
                        //`pid`, `title`, `content`, `parent_id`, `menu`, `page_order`, `user_id`, `banner_top`, `banner_bottom`, `status`, `date_added`
                        // `menu_id`, `name`
                        echo '<tr>
                                <!--<td><input type="checkbox" id="chk'.$arr['menu_id'].'" class="flat"></td>-->
                                <td>'.$arr['name'].'</td>
                                <td><code>&lt;&#63;php display_menus(\''.$arr['name'].'\'); &#63;&gt;</code></td>
                                <td>'.date('m/d/Y H:i:s', strtotime($arr['date_added'])).'</td>
                                <td>
                                  <button class="btn btn-success btnEditMenu" type="button" data-menuid="'.$arr['menu_id'].'"><i class="fa fa-pencil"></i></button>
                                  <button class="btn btn-danger btnDeleteMenu" type="button"  data-menuid="'.$arr['menu_id'].'"><i class="fa fa-trash"></i></button>
                                </td>
                              </tr>
                        ';
                      }
                  endif;
                ?>
            </tbody>
          </table>
                <?php
                  // echo '<pre>';
                  // // print_r($list_array);
                  // echo '</pre>';
                  // if(count($list_array['result']) > 0):
                  //     echo 'hello';
                  //     foreach($list_array['result'] as $arr){
                  //       //`pid`, `title`, `content`, `parent_id`, `menu`, `page_order`, `user_id`, `banner_top`, `banner_bottom`, `status`, `date_added`
                  //       echo $arr['uid'].':'.$arr['username'].'<br>';
                  //     }
                  // endif;
                // $this->ppmsystemlib->get_accepted_mimes();
                ?>
          <!-- END : LIST TABLE -->
<!--           <div class="spacer30"></div>
          <div class="spacer30"></div> -->
          <?php
           // display_menus('tester');
          ?>
        </div>
        <div class="col-md-4">
          <div class="panel panel-default">
            <div class="panel-heading"><strong><i class="fa fa-life-ring" aria-hidden="true"></i> Help Info</strong></div> 
            <div class="panel-body">Help Content here...</div>
          </div>
        </div>
      </div>

    </div>

  </div>

  <div class="clearfix"></div>
</div>

<div id="member-logs" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title">Title</h4>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>

<!-- /page content -->

<style>
.spacer10 {
  clear: both;
  width: 100%;
  height: 10px;
}
.spacer20 {
  clear: both;
  width: 100%;
  height: 20px;
}
.spacer30 {
  clear: both;
  width: 100%;
  height: 30px;
}

#tblMenuList thead td {
  font-weight: bold;
}

</style>

<script>
  $(document).ready(function(){
    var baseurl = '<?php echo base_url(); ?>';

    $('#btnAddNew').click(function(){
        window.location = baseurl + 'admin/menubuilder';
    });

    $('#tblMenuList').on('click', '.btnEditMenu', function(){
        var menuID = $(this).data('menuid');
        window.location = baseurl + 'admin/menubuilder/edit/' + menuID;
    });

    $('#tblMenuList').on('click', '.btnDeleteMenu', function(){
        var menuID = $(this).data('menuid');
        var $this = $(this);
        swal({
          title: "Are you sure?",
          text: "Once deleted, you will not be able to recover this data!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            // swal("Poof! Your imaginary file has been deleted!", {
            //   icon: "success",
            // });
            $.ajax({
              type : 'post',
              url  : baseurl + 'admin/menubuilder/delete',
              data : {
                'menuID' : menuID
              },
              beforeSend : function(){

              },
              success : function(result){
                  table
                      .row( $this.parents('tr') )
                      .remove()
                      .draw();

                  swal("Poof! Menu has been deleted!", {
                    icon: "success",
                  });
              }
            });
          } else {
            // swal("Your imaginary file is safe!");
          }
        });
    });

    var table = $('#tblMenuList').DataTable({
        "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        "columnDefs": [{ "orderable": false, "targets": [0, 1, 3] }],
        "order": [[ 0, "asc" ]]
    });

  });
</script>