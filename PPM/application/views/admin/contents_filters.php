<?php
	$exclude = TRUE;
?>
<!-- Category: Client type -->

<style>
	.text-word-wrap {
		width: 250px;
		display: inline-block;
		white-space: normal;
		line-height: 18px;
	}
</style>

<form id="contents-form" class="form-horizontal form-label-left">
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Category:</label>
		<div class="col-md-9 col-sm-9 col-xs-12">
			<?php 
				$category_arr = array(
					'news-articles' =>'News & Articles',
					'rent-roll'		=>'Rent Roll',
					'ppm-tv'		=>'PPM TV',
					'topics'		=>'Topics',
					'promotions'	=>'Promotions',
					'others'		=>'Others'
				);

				//echo $this->ppmsystemlib->createDropdown('category-type', $category_arr, '', FALSE, FALSE);

			?>
			<p></p>
			

			<?php 

				$category_count = (count($categories_arr[$page])>=6) ? 6 : count($categories_arr[$page]);

				$hide = '';

				if($category_count === 1) {
					$hide = 'hide';
					echo '<h4>' . $page_title . '</h4>';
				}

				echo form_multiselect('category', $categories_arr[$page], '', 'id="category-list" class="select2_multiple form-control ' . $hide . '" size="' . $category_count . '" ');

			?>
			<select id="category-list-holder" class="select2_multiple form-control hide" size="6"></select>
		</div>
	</div>

	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">&nbsp;</label>
		<div class="col-md-9 col-sm-9 col-xs-12">
			<button id="contents-search-btn" type="button" class="btn btn-success">Search</button>
		</div>
	</div>	

</form>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/numerals.js/numeral.min.js"></script>

<script type="text/javascript">

$(document).ready(function(){
	var masterList 					= null;
	var contentsForm 				= $('#contents-form');
	var listTable 					= $('#contents-table');
	var listTableComplete 			= $('#contents-table-complete');
	var filtersSection 				= $('#headingOne');
	var resultsSection 				= $('#headingTwo');
	var totalLabel 					= $('#total-label p');
	var categoryType 				= $('#category-type');
	var categoryList 				= $('#category-list');
	var categoryListHolder 			= $('#category-list-holder');
	var enquiryDetailsTable			= $('#enquiry-details-table');
	var stateDictionaryMember 		= <?php if(isset($stateDictionaryMember)){echo json_encode($stateDictionaryMember);}else{echo 'null';}?>;
	var resultsLabel 				= $('#headingTwo .panel-title small');
	var tempListHolder 				= $('<div></div>');
	var currentDataTable 			= null;
	var currentTable 				= null;
	var tableId 					= '';
	var loader 						= $('#loader');
	var percentage 					= 0;
	var progressCtr 				= 0;
	var total 						= 0;
	var tempListStr 				= '';
	var selectedCategory 			= 0;

	var contents = {
		_dt: null,
		tempFunc: null,
		init: function(){
			$('#contents-search-btn').on('click', function(){contents.getList()});

			contents.initFilters();
			searchTab.on('click', function(){
				if(currentDataTable!=null) {
					setTimeout(function(){
						currentDataTable.draw(false);
					}, 300);
				}
			});
		},
		immediateSearch : function(){
			$('#contents-search-btn').trigger('click');
		},
		initFilters: function (){

			//categoryList.removeClass('hide').hide();

			categoryType.on('change', function(){
				var val = $(this).val();
				
				if(val == '') {
					categoryList.val(val);
					categoryList.hide();
				}
				else {
					categoryList.val([]);
					categoryList.trigger('change');
					categoryList.show();

					categoryList.find('optgroup.opt-group-label').appendTo(categoryListHolder);
					categoryListHolder.find('optgroup.'+val+'-label').appendTo(categoryList);

					var optionsCount = categoryList.find('optgroup.'+val+'-label option').length;

					if(optionsCount >=10) {
						categoryList.attr({'size':10});
					}
					else {
						categoryList.attr({'size':optionsCount+1});
					}

					categoryList.val(parseInt(categoryList.find('optgroup.'+val+'-label option:first').val()));

					if(val == 'rent-roll' || val == 'ppm-tv') {
						categoryList.hide();
					}
				}
			});
		},
		setDate: function(fromId, toId) {
	    	$('#' + fromId).datetimepicker({
				format: 'DD/MM/YYYY'
			});
	    
		    $('#' + toId).datetimepicker({
		        useCurrent: false,
		        format: 'DD/MM/YYYY'
		    });
		    
		    $("#" + fromId).on("dp.change", function(e) {
		        $('#' + toId).data("DateTimePicker").minDate(e.date);
		    });
		    
		    $("#" + toId).on("dp.change", function(e) {
		        $('#' + fromId).data("DateTimePicker").maxDate(e.date);
		    });
	    },
	    checkState: function() {
			if ($('#state').val() == '1') {
				$('#members-form .state-subregion').show();
			} else {
				$('#members-form .state-subregion').hide();
			}
		},
		getList: function(){

			if(contents.validateForm()) {
				selectedCategory = parseInt(categoryList.val());

				contents.showList();
				contents.scrollTop();
				totalLabel.removeClass('hide');
				totalLabel.html('Fetching Records... <i class="fa fa-spinner fa-spin"></i>');
				resultsLabel.text('');

				if(tableId!='') {
					$('#' + tableId + '_wrapper').remove();
					currentTable = null;
					currentDataTable = null;
				}

				var request = $.ajax({
		          url: "<?php echo base_url(); ?>admin/contents/get_list",
		          method: "POST",
		          data: contentsForm.serialize(),
		          dataType: 'json'
		        });
		         
		        request.done(function( data ) {
		        	//console.log(data);

		            if(data!=null) {
		            	contents.refreshList();

		            	masterList = data;//data.result;
		            	total = masterList.length;

		            	if(total>0) {
	    					totalLabel.removeClass('hide');

							tempListHolder.html('');

							loader.addClass('hide');
							totalLabel.html('Updating list with ' + numeral(total).format('0,0') + ' records. <i class="fa fa-spinner fa-spin"></i>');

		            		contents.processList();
		            	}
		            	else {
		            		contents.updateLoader(false);
		            		currentDataTable = currentTable.DataTable();
		            	}

		            	resultsLabel.text(categoryList.find('option:selected').text());

						data = null;
		            }
        		});

        		request.error(function(_data){
        			if(_data.readyState == 4 && _data.status == 200) {//Session expired and redirected
        				swal({
						    title: "Session Expired!",
						    text: "Reloading page.",
						    icon: "warning",
						    dangerMode: false,
						})
						.then(willDelete => {
						    if (willDelete) {
						    	window.location.href = '<?php echo base_url();?>admin/login';
						    }
						});
        			}
        		});
	    	}
		},
		processList: function(){

            setTimeout(function(){
            	
            	var columns = [];
            	var order = [];

            	switch(selectedCategory) {
            		case 5:
            			columns = [
							        { data: 'id' },
							        { data: 'name' },
							        { data: 'location' },
							        { data: 'state' },
							        { data: 'numberOfEnquiries' },
							        { data: 'code' },
							        { data: 'contactName' },
							        { data: 'companyName' },
							        { data: 'email' },
							        { data: 'contactNumber' },
							        { data: 'shortdesc' },
							        { data: 'mydate' },
							        { data: 'comments' },
							        { data: 'draft' },
							        { data: 'action' }				        
							    ];
						order = [[ 11, "desc" ]];

            		break;

            		default:
            		    columns = [
								    { data: 'id' },
								    { data: 'name' },
								    { data: 'shortdesc' },
								    { data: 'mydate' },
								    { data: 'draft' },
								    { data: 'action' }
								];
						order = [[ 3, "desc" ]];		
            		break;
            	}

				currentDataTable = currentTable.DataTable( {
					data: masterList,
				    columns: columns,
				    "scrollY": (masterList.length >=10) ? 480 : false,
        			"scrollX": true,
        			"order": order
				} );

				contents._dt = currentDataTable;

				currentTable.children('tbody').on('click', 'td.col-action', function () {
					contents.showActionPanel($(this));
				});

				contents.initListAction();

				contents.updateLoader(false);

				currentDataTable.on( 'draw.dt', function () {
				    contents.initListAction();
				});


				if(selectedCategory == 5 || selectedCategory === 4) {
					$('#state-search').show();

					$('#collapseTwo select#state').unbind('change').bind('change', function(){
						$('#' + tableId + '_filter input').val($(this).val()).keyup();
					});
				}
				else {
					$('#state-search').hide();
				}


            }, 1);

		},
		initListAction: function(){
			$.each($(currentTable).find('tr'), function(k,v){
		    	var id = $(v).children('td:first').text();

		    	if($.isNumeric(id)) {
		    		var td = $(v).children('td:nth-last-child(1)');
		    		td.addClass('col-action').attr({'data-id':id});
		    	}
		    });
		},
		refreshList: function(){
			if(tableId!='') {
				$('#' + tableId + '_wrapper').remove();
				currentTable = null;
				currentDataTable = null;
			}

			tableId = contents.generateID();

			var tempTable = null;

			if(selectedCategory == 5) {
				tempTable = listTableComplete.clone().removeClass('hide').attr({id:tableId});

				tempTable.insertAfter(listTableComplete);
			}
			else {
				tempTable = listTable.clone().removeClass('hide').attr({id:tableId});

				tempTable.insertAfter(listTable);
			}

			currentTable = $('#' + tableId);

			tempTable = null;
		},
		updateResultsCount: function(){
			var str = '';

			if($('tr.row-item').length>0){

				str = '('+ numeral($('tr.row-item').length).format('0,0')+' records found)';
			}
			else {
				str = '';
			}

			resultsLabel.text(str);
		},
		checkDateTime: function(dateTimeStr){
			var dateTime = dateTimeStr.split(' ');

			if(dateTime.length > 1) {

				if(dateTime[1]!='00:00:00') {
					return moment(dateTimeStr).format('D/MM/YYYY h:mm:ss A');
				}
				else {
					return moment(dateTimeStr).format('D/MM/YYYY');
				}
			}

			return moment(dateTimeStr).format('D/MM/YYYY');
		},
		showActionPanel: function(td){
			var col 		= $(td);
			var id 			= col.data().id;
			var edit 		= col.find('ul li ul li a.edit');
			var copy 		= col.find('ul li ul li a.copy');
			var deleteLnk 	= col.find('ul li ul li a.delete');
			var enquiryLnk 	= col.find('ul li ul li a.enquiry');

			col.find('ul li ul li a label.action-id').text(id);

			edit.unbind('click').bind('click', function(){contents.requestByAction({'id':id, 'action':$(this).attr('class')})});
			copy.unbind('click').bind('click', function(){contents.requestByAction({'id':id, 'action':$(this).attr('class')})});
			
			deleteLnk.unbind('click').bind('click', function(){contents.requestByAction({'id':id, 'action':$(this).attr('class')})});

			if(enquiryLnk.length > 0) {
				enquiryLnk.unbind('click').bind('click', function(){contents.requestByAction({'id':id, 'action':$(this).attr('class')})});
			}

			col.parents('tr').attr({id:'row-'+id})

			edit = copy = deleteLnk = enquiryLnk = null;
		},
		requestByAction: function(data){

			if(data!=null) {

		    	contents.tempFunc = function(data) {
		    		var request = $.ajax({
			          url: "<?php echo base_url(); ?>admin/contents/action",
			          method: "POST",
			          data: data,
			          dataType: 'json'
			        });

			        request.done(function( _data ) {
			            if(_data!=null) {

			            	if(data.action == 'edit' || data.action == 'copy') {
				            	if(addEdit!=null) {
				            		if(data.action == 'copy') addEdit.isCopy = true;
				            		addEdit.populateForm(_data);
				            	}
			            	}
			            	else if(data.action == 'enquiry') {
			            		contents.showEnquiryDetails(_data, data.id);
			            	}
			            	else if(data.action == 'delete') {
			            		if(_data.hasOwnProperty('success')) {
			            			addEdit.resetTab();
			            			currentDataTable.row('#row-'+data.id).remove().draw( false );
			            			swal({title:"Success!",text:"Deleted item with ID: " + data.id,icon:'success',button:false, timer:3000});
			            		}
			            		else {
			            			swal({title:"Delete Failed!",text:"No record was found.",icon:'error',button:false, timer:3000});
			            		}
			            	}

			            	_data = null;
			            }
	        		});

	        		request.error(function(_data){
	        			if(_data.readyState == 4 && _data.status == 200) {//Session expired and redirected
	        				swal({
							    title: "Session Expired!",
							    text: "Reloading page.",
							    icon: "warning",
							    dangerMode: false,
							})
							.then(willDelete => {
							    if (willDelete) {
							    	window.location.href = '<?php echo base_url();?>admin/login';
							    }
							});
	        			}
	        		});
		    	}

				var showAddEditTab = false;

            	if(data.action == 'edit') {
					showAddEditTab = true;
					addTab.html('<i class="fa fa-pencil"></i> ' + data.action.charAt(0).toUpperCase() + data.action.substr(1));
            	}
            	else if(data.action == 'copy') {
            		swal({
						title: "Copy Item",
						text: "Are you sure?",
						icon: "warning",
						buttons: ["Cancel", true],
						dangerMode: false,
						})
						.then(willDelete => {
						
							if(willDelete) {
								addTab.html('<i class="fa fa-pencil"></i> ' + data.action.charAt(0).toUpperCase() + data.action.substr(1));
								contents.tempFunc.call(this, data);
							}
						
						}); 
            	}
            	else if(data.action == 'delete') {
            		swal({
						title: "Delete Item",
						text: "Are you sure?",
						icon: "warning",
						buttons: ["Cancel", true],
						dangerMode: true,
						})
						.then(willDelete => {
						
							if(willDelete) {
								contents.tempFunc.call(this, data);
							}
						
						}); 
            	}
            	else if(data.action == 'enquiry') {
            		contents.tempFunc.call(this, data);
            	}

            	if(showAddEditTab) {
            		if(contents.tempFunc!=null) contents.tempFunc.call(this, data);
		    	}
	    	}
		},
		showEnquiryDetails: function(data, newId){

			var title = '';
			var tempTable = null;
			var col_data = [];
			var loaderText = 'Fetching list... <i class="fa fa-spinner fa-spin"></i>';

			$('#enquiry-content').html('');

			var ed_tableId = this.generateID();

			title = 'Enquiry Details';
			tempTable = enquiryDetailsTable.clone().attr({id:ed_tableId});
			col_data = [{data : 'id'},
						{data : 'date'},
						{data : 'CompanyName'},
						{data : 'Title'},
						{data : 'FirstName'},
						{data : 'Surname'},
						{data : 'Email'},
						{data : 'Phone'},
						{data : 'Street'},
						{data : 'Suburb'},
						{data : 'Location'},
						{data : 'PostCode'},
						{data : 'Mobile'},
						{data : 'Fax'},
						{data : 'Other'}
					];

			tempTable.appendTo($('#enquiry-content'));

			$('#enquiry-details h4.modal-title').html(title + ' | <small></small>');
			$('#enquiry-details h4.modal-title small').text('UID: ' + newId);

			$('#enquiry-loader').html(loaderText).show();
			
			$('#enquiry-details').modal({backdrop: "static"});

			setTimeout(function(){
				$(tempTable).DataTable({
	        		data: data,
	        		columns: col_data,
	        		"scrollY": (data.length >=10) ? 480 : false,
	        		"scrollX": true
	        	});

	        	$('#enquiry-loader').hide();
			},300);
		},
		applyUpdateChanges: function(data){

			if(data!=null) {

				var row = $('#row-'+data.id);

				if(selectedCategory != 5) {
					row.children('td')[1].innerHTML = data.name;
					row.children('td')[2].innerHTML = data.shortdesc;
					row.children('td')[3].innerHTML = data.mydate;
					row.children('td')[4].innerHTML = data.draft;
				}
				else {
					row.children('td')[1].innerHTML = data.name;
					row.children('td')[2].innerHTML = data.location;
					row.children('td')[3].innerHTML = data.state;
					row.children('td')[4].innerHTML = data.numberOfEnquiries;
					//row.children('td')[5].innerHTML = data.code;
					row.children('td')[6].innerHTML = data.contactName;
					row.children('td')[7].innerHTML = data.companyName;
					row.children('td')[8].innerHTML = data.email;
					row.children('td')[9].innerHTML = data.contactNumber;
					row.children('td')[10].innerHTML = data.shortdesc;
					row.children('td')[11].innerHTML = data.mydate;
					row.children('td')[12].innerHTML = data.comments;
					row.children('td')[13].innerHTML = data.draft;								
				}
			}
		},
		addNewItem: function(res) {
			if(res!=null) {

				var data = {};

            	switch(selectedCategory) {
            		case 5:
            			data = {
							      "id":res.id,
							      "name":res.name,
							      "location":res.location,
							      "state":res.state,
							      "numberOfEnquiries":res.numberOfEnquiries,
							      "code":res.code,
							      "contactName":res.contactName,
							      "companyName":res.companyName,
							      "email":res.email,
							      "contactNumber":res.contactNumber,
							      "shortdesc":res.shortdesc,						      
							      "mydate":res.mydate,
							      "comments":res.comments,
							      "draft":res.draft,
							      "action":res.action			        
							    };

            		break;

            		default:
            		    data = {
								  "id":res.id,
								  "name":res.name,
								  "shortdesc":res.shortdesc,
								  "mydate":res.mydate,
								  "draft":res.draft,
								  "action":res.action
								};		
            		break;
            	}

            	currentDataTable.row.add(data).node().id = 'row-'+res.id;
            	currentDataTable.page( 'next' ).draw( false );
            	currentDataTable.page( 'previous' ).draw( false );
			}
		},
		updateLoader: function(show){
			if(show) {
				var percentage = progressCtr / total;

				if(progressCtr % 20 == 0) loader.css({width: (percentage * 100) + '%'});

		        progressCtr++;

		        totalLabel.text('Fetching ' + numeral(progressCtr).format('0,0') + ' of ' + numeral(total).format('0,0') + ' records');
	    	}
	    	else {
	    		progressCtr = total = 0;

	    		loader.css({width: '0%'}).addClass('hide');
	    		totalLabel.text('').addClass('hide');
	    	}
		},
		scrollTop: function(){
			$('html').scrollTop(0);
		},
		showList: function(){
			if(resultsSection.attr('aria-expanded') != "true") resultsSection.trigger('click');
		},
		validateForm: function(){
			var isValid = false;

			if(categoryList.val() == '') {
				return false;
			}

			return true;
		},
		generateID: function() {
	      var charSet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
	      var charCount = 20;
	      var charSetSize = charSet.length;
	      var id = '';

	      for (var i = 1; i <= charCount; i++) {
	          var randPos = Math.floor(Math.random() * charSetSize);
	          id += charSet[randPos];
	      }

	      return id;      
	    }

	};

	contents.init();

	filter = contents;

	<?php if($category_count === 1) { ?>
		$('#headingOne').hide();
		contents.immediateSearch();
	<?php } ?>

});

</script>