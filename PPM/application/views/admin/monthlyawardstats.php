<!-- page content -->
<div class="right_col" role="main" style="min-height: 100vh;">

	<div class="col-md-12 col-lg-12 col-xl-10" style="padding-bottom: 60px;">
		<div class="x_panel">
			<div class="x_title">
				<h2><?php echo $page_title; ?></h2>
				<ul class="nav navbar-right panel_toolbox" style="min-width:auto!important;">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">

			    <div class="" role="tabpanel" data-example-id="togglable-tabs">
					<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
					
					<li role="presentation" class="active"><a href="#tab_content1" id="search-tab" role="tab" data-toggle="tab" aria-expanded="true"><i class="fa fa-search"></i> Search</a>
					</li>

					</ul>
					<div id="myTabContent" class="tab-content">
						<div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="search-tab">

		                    <!-- start accordion -->
		                    <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
		                      <div class="panel">
		                        <a class="panel-heading" role="tab" id="headingOne" data-toggle="collapse" data-parent="" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
		                          <h4 class="panel-title">Filters</h4>
		                        </a>
		                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
		                          <div class="panel-body">
		                          	<div class="col-md-12">
										<form id="contents-form" class="form-horizontal form-label-left">
											<div class="form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12">Category:</label>
												<div class="col-lg-5 col-md-9 col-sm-9 col-xs-12">
													<?php 
														
														echo form_dropdown('category', $categoryArr, '', 'id="data-category" class="form-control"');

													?>	
												</div>
											</div>

											<div id="annual-report" class="form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12">Report For:</label>
												<div class="col-lg-5 col-md-9 col-sm-9 col-xs-12">
													<?php 
														
														echo form_dropdown('fiscalYear', $reportYear, '', 'id="data-fiscalYear" class="form-control"');

													?>	
												</div>
											</div>

											<div id="other-report" class="form-group" style="display: none;">
												<label class="control-label col-md-3 col-sm-3 col-xs-12">Report For:</label>
												<div class="col-md-4 col-sm-4 col-xs-12">
											        <div class='input-group date' id='startDate'>
											            <input name="startDate" type='text' class="form-control" placeholder="yyyy mm" />
											            <span class="input-group-addon">
											            <span class="glyphicon glyphicon-calendar"></span>
											            </span>
											        </div>
												</div>
												<div class="col-md-1 col-sm-1 col-xs-12"><label class="control-label">TO</label></div>
												<div class="col-md-4 col-sm-4 col-xs-12">
											        <div class='input-group date' id='endDate'>
											            <input name="endDate" type='text' class="form-control" placeholder="yyyy mm" />
											            <span class="input-group-addon">
											            <span class="glyphicon glyphicon-calendar"></span>
											            </span>
											        </div>
												</div>
											</div>											

											<div class="form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12">&nbsp;</label>
												<div class="col-lg-5 col-md-9 col-sm-9 col-xs-12">
													<button id="contents-search-btn" type="button" class="btn btn-success">Search</button>
												</div>
											</div>	

										</form>


				                    </div>
		                          </div>
		                        </div>
		                      </div>
		                      <div class="panel">
		                        <a class="panel-heading collapsed" role="tab" id="headingTwo" data-toggle="collapse" data-parent="" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
		                          <h4 class="panel-title">Results <small class="badge bg-green" style="color:#fff;"></small></h4>
		                        </a>
		                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
		                          <div class="panel-body">

			                         	<table id="contents2-table" class="table table-bordered table-striped hide table-hover text-left  dt-responsive nowrap" data-page-length='10' cellspacing="0" width="100%"><!-- table-bordered  -->
											<thead>
											<tr>
											<th style="text-align:left!important; vertical-align: middle!important;" colspan="2"></th>
											<th style="text-align:center!important; vertical-align: middle!important;" colspan="5">Vacancy Rate</th>
											<th style="text-align:center!important; vertical-align: middle!important;" colspan="5">Arrears Rate</th>
											<th style="text-align:center!important; vertical-align: middle!important;" colspan="5">Rent Increase</th>
											<th style="text-align:center!important; vertical-align: middle!important;" colspan="5">Net New Manage</th>
											<th style="text-align:center!important; vertical-align: middle!important;" colspan="5">Dollar increase</th>
											</tr>
											<tr>
											<th style="text-align:left!important; vertical-align: middle!important;">ID</th>
											<th style="text-align:left!important; vertical-align: middle!important;">Member</th>

			                                	<?php

			                                		$quarterArr = array('Jul - Sep','Oct - Dec','Jan - Mar','Apr - Jun');

			                                		for ($i=1; $i <= 5 ; $i++) { 
			                                			foreach ($quarterArr as $value) {
			                                				echo '<th style="text-align:left!important; vertical-align: middle!important;">'.$value.'</th>';
			                                			}

			                                			echo '<th class="text-success" style="text-align:left!important; vertical-align: middle!important;">Avg</th>';
			                                		}


			                                	?>
											</tr>
											</thead>
											<tbody></tbody>
			                            </table>

										<table id="contents3-table" class="table table-striped hide table-hover text-left  dt-responsive nowrap" data-page-length='10' cellspacing="0" width="100%"><!-- table-bordered  -->
											<thead>
											<tr>
											<th style="text-align:left!important; vertical-align: middle!important;">Member</th>
											<th style="text-align:left!important; vertical-align: middle!important;">For Month Starting</th>
											<th style="text-align:left!important; vertical-align: middle!important;">Comments</th>
											</tr>
											</thead>
											<tbody>
											</tbody>

			                            </table>

			                            <div id="contents1-table-holder"></div>		                            

		                            <div id="loader" class="progress-bar progress-bar-success hide" data-transitiongoal="100" aria-valuenow="0" style="height:2px;"></div>
		                            <div class="clearfix"><p>&nbsp;</p></div>
		                            <div id="total-label" class="text-center"><p class="hide"></p></div>

		                          </div>
		                        </div>
		                      </div>
		                    </div>
		                    <!-- end of accordion -->

						</div>

					</div>
			    </div>

			</div>
		</div>
	</div>

	<div class="clearfix"></div>
</div>

<!-- /page content -->

<style>
	.text-word-wrap {
		width: 100%;
		display: inline-block;
		white-space: normal;
		line-height: 18px;
	}
</style>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/numerals.js/numeral.min.js"></script>

<script type="text/javascript">

var monthlyStats 		= null;
var monthlyStatsTable 	= null;

$(document).ready(function(){
	var masterList 					= null;
	var form 						= $('#contents-form');
	var listTable1 					= null;
	var listTable2 					= $('#contents2-table');
	var listTable3 					= $('#contents3-table');
	var filtersSection 				= $('#headingOne');
	var resultsSection 				= $('#headingTwo');
	var totalLabel 					= $('#total-label p');
	var resultsLabel 				= $('#headingTwo .panel-title small');
	var tempListHolder 				= $('<div></div>');
	var currentDataTable 			= null;
	var currentTable 				= null;
	var tableId 					= '';
	var loader 						= $('#loader');
	var percentage 					= 0;
	var progressCtr 				= 0;
	var total 						= 0;
	var tempListStr 				= '';
	var selectedCategory 			= 0;
	var annualReport 				= $('#annual-report');
	var	otherReport 				= $('#other-report');
	var contentsTableHolder 		= $('#contents1-table-holder');

	var contents = {
		tempFunc: null,
		init: function(){
			$('#contents-search-btn').on('click', function(){contents.getList();});

			this.setDate('startDate','endDate');
			this.checkCategory();
		},
		setDate: function(fromId, toId) {
	    	$('#' + fromId).datetimepicker({
				format: 'YYYY MMMM'
			});
	    
		    $('#' + toId).datetimepicker({
		        format: 'YYYY MMMM'
		    });
		    
		    $("#" + fromId).on("dp.change", function(e) {
		        $('#' + toId).data("DateTimePicker").minDate(e.date);
		    });
		    
		    $("#" + toId).on("dp.change", function(e) {
		        $('#' + fromId).data("DateTimePicker").maxDate(e.date);
		    });
	    },
	    checkCategory: function(){
	    	$('#data-category').on('change', function(){
	    		var val = $(this).val();

	    		selectedCategory = parseInt(val);

	    		if(parseInt(val) == 2) {
	    			annualReport.show();
	    			annualReport.find('select').attr({disabled:false});
	    			otherReport.hide();
	    			otherReport.find('input').attr({disabled:true});
	    		}
	    		else {
					annualReport.hide();
					annualReport.find('select').attr({disabled:true});
	    			otherReport.show();
	    			otherReport.find('input').attr({disabled:false});
	    		}
	    	});
	    },	
		getList: function(){

			contents.showList();
			contents.scrollTop();
			totalLabel.removeClass('hide');
			totalLabel.html('Fetching Records... <i class="fa fa-spinner fa-spin"></i>');
			resultsLabel.text('');

			if(tableId!='') {
				$('#' + tableId + '_wrapper').remove();
				currentTable = null;
				currentDataTable = null;
			}

			var dataType = (selectedCategory == 1) ? 'text' : 'json';

			var request = $.ajax({
	          url: "<?php echo base_url(); ?>admin/monthlyawardstats/get_list",
	          method: "POST",
	          data: form.serialize(),
	          dataType: dataType
	        });
	         
	        request.done(function( data ) {
	            if(data!=null) {

	            	if(selectedCategory == 1) {
	            		contentsTableHolder.html(data);

	            		listTable1 	= $('#contents1-table');

	            		contents.refreshList();

	            		if(monthlyStatsTable!=null) {
	            			monthlyStatsTable.processMonthlyStatsReportList();
	            		}
	            	}
	            	else {
		            	contents.refreshList();

		            	masterList = data;//data.result;
		            	total = (selectedCategory == 2) ? Object.keys(masterList).length : masterList.length;

		            	if(total>0) {
	    					totalLabel.removeClass('hide');

							tempListHolder.html('');

							loader.addClass('hide');
							totalLabel.html('Updating list with ' + numeral(total).format('0,0') + ' records. <i class="fa fa-spinner fa-spin"></i>');
							
							if(selectedCategory == 2) {
		            			contents.processAnnualReportList();
		            		}
		            		else if(selectedCategory == 3) {
		            			contents.processIndividualAchievementReportList();
		            		}
		            	}
		            	else {
		            		contents.updateLoader(false);
		            		currentDataTable = currentTable.DataTable();
		            	}
	            	}

					data = null;
	            }
    		});

    		request.error(function(_data){
    			if(_data.readyState == 4 && _data.status == 200) {//Session expired and redirected
    				swal({
					    title: "Session Expired!",
					    text: "Reloading page.",
					    icon: "warning",
					    dangerMode: false,
					})
					.then(willDelete => {
					    if (willDelete) {
					    	window.location.href = '<?php echo base_url();?>admin/login';
					    }
					});
    			}
    		});
		},
		processMonthlyStatsReportList: function(columns, data){
			
			setTimeout(function(){

				currentDataTable = currentTable.DataTable( {
					data: data,
				    columns: columns,
				    "scrollY": (data.length >=10) ? 480 : false,
        			"scrollX": true,
        			"ordering": false,
        			"searching": false
				} );

				contents.updateLoader(false);

            }, 1);

		},
		processAnnualReportList: function(){
			
			setTimeout(function(){
            	
            	var columns = [
            					{ data: 'id' },
						        { data: 'company' },

						        { data: 'vacancyRate_q1' },
						        { data: 'vacancyRate_q2' },
						        { data: 'vacancyRate_q3' },
						        { data: 'vacancyRate_q4' },
						        { data: 'vacancyRate_avg' },

						        { data: 'rentArrears_q1' },
						        { data: 'rentArrears_q2' },
						        { data: 'rentArrears_q3' },
						        { data: 'rentArrears_q4' },
						        { data: 'rentArrears_avg' },

						        { data: 'comissionIncrease_q1' },
						        { data: 'comissionIncrease_q2' },
						        { data: 'comissionIncrease_q3' },
						        { data: 'comissionIncrease_q4' },
						        { data: 'comissionIncrease_avg' },

						        { data: 'net_q1' },
						        { data: 'net_q2' },
						        { data: 'net_q3' },
						        { data: 'net_q4' },
						        { data: 'net_avg' },

						        { data: 'dollarIncrease_q1' },
						        { data: 'dollarIncrease_q2' },
						        { data: 'dollarIncrease_q3' },
						        { data: 'dollarIncrease_q4' },
						        { data: 'dollarIncrease_avg' }	        
						    ];

				currentDataTable = currentTable.DataTable( {
					data: contents.groupAnnualReportData(masterList),
				    columns: columns,
				    "scrollY": (masterList.length >=10) ? 480 : false,
        			"scrollX": true
				} );

				contents.updateLoader(false);

            }, 1);

		},
		groupAnnualReportData: function(list){

			var arr = [];

			$.each(list, function(i,data){
				arr.push(new ColList(data));
			});

			function ColList(data){
				this.id = data.id;
				this.company = data.company;
				
				this.vacancyRate_q1 = data.vacancyRate.q1;
				this.vacancyRate_q2 = data.vacancyRate.q2;
				this.vacancyRate_q3 = data.vacancyRate.q3;
				this.vacancyRate_q4 = data.vacancyRate.q4;
				this.vacancyRate_avg = data.vacancyRate.avg;

				this.rentArrears_q1 = data.rentArrears.q1;
				this.rentArrears_q2 = data.rentArrears.q2;
				this.rentArrears_q3 = data.rentArrears.q3;
				this.rentArrears_q4 = data.rentArrears.q4;
				this.rentArrears_avg = data.rentArrears.avg;

				this.comissionIncrease_q1 = data.comissionIncrease.q1;
				this.comissionIncrease_q2 = data.comissionIncrease.q2;
				this.comissionIncrease_q3 = data.comissionIncrease.q3;
				this.comissionIncrease_q4 = data.comissionIncrease.q4;
				this.comissionIncrease_avg = data.comissionIncrease.avg;

				this.net_q1 = data.net.q1;
				this.net_q2 = data.net.q2;
				this.net_q3 = data.net.q3;
				this.net_q4 = data.net.q4;
				this.net_avg = data.net.avg;

				this.dollarIncrease_q1 = data.dollarIncrease.q1;
				this.dollarIncrease_q2 = data.dollarIncrease.q2;
				this.dollarIncrease_q3 = data.dollarIncrease.q3;
				this.dollarIncrease_q4 = data.dollarIncrease.q4;
				this.dollarIncrease_avg = data.dollarIncrease.avg;
            }

            return arr;

		},
		processIndividualAchievementReportList: function(){
			
			setTimeout(function(){
            	
            	var columns = [
						        { data: 'company' },
						        { data: 'forMonth' },
						        { data: 'individualAchievement' }     
						    ];

				currentDataTable = currentTable.DataTable( {
					data: masterList,
				    columns: columns,
				    "scrollY": (masterList.length >=10) ? 480 : false,
        			"scrollX": true
				} );

				contents.updateLoader(false);

            }, 1);

		},		
		refreshList: function(){
			if(tableId!='') {
				$('#' + tableId + '_wrapper').remove();
				currentTable = null;
				currentDataTable = null;
			}

			tableId = contents.generateID();

			var tempTable = null;

			if(selectedCategory == 1) {
				tempTable = listTable1.clone().removeClass('hide').attr({id:tableId});
				tempTable.insertAfter(listTable1);
			}
			else if(selectedCategory == 2) {
				tempTable = listTable2.clone().removeClass('hide').attr({id:tableId});
				tempTable.insertAfter(listTable2);
			}
			else {
				tempTable = listTable3.clone().removeClass('hide').attr({id:tableId});
				tempTable.insertAfter(listTable3);
			}

			currentTable = $('#' + tableId);

			tempTable = null;
		},
		updateResultsCount: function(){
			var str = '';

			if($('tr.row-item').length>0){

				str = '('+ numeral($('tr.row-item').length).format('0,0')+' records found)';
			}
			else {
				str = '';
			}

			resultsLabel.text(str);
		},
		updateLoader: function(show){
			if(show) {
				var percentage = progressCtr / total;

				if(progressCtr % 20 == 0) loader.css({width: (percentage * 100) + '%'});

		        progressCtr++;

		        totalLabel.text('Fetching ' + numeral(progressCtr).format('0,0') + ' of ' + numeral(total).format('0,0') + ' records');
	    	}
	    	else {
	    		progressCtr = total = 0;

	    		loader.css({width: '0%'}).addClass('hide');
	    		totalLabel.text('').addClass('hide');
	    	}
		},
		scrollTop: function(){
			$('html').scrollTop(0);
		},
		showList: function(){
			if(resultsSection.attr('aria-expanded') != "true") resultsSection.trigger('click');
		},
		generateID: function() {
	      var charSet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
	      var charCount = 20;
	      var charSetSize = charSet.length;
	      var id = '';

	      for (var i = 1; i <= charCount; i++) {
	          var randPos = Math.floor(Math.random() * charSetSize);
	          id += charSet[randPos];
	      }

	      return id;      
	    }
	};

	contents.init();

	monthlyStats = contents;

});

</script>