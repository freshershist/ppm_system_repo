<script>
	
	var report = {};
	var _users = <?php echo json_encode($usersArray); ?>;

	$(document).ready(function(){

		report = {
			instance: function(){
				var _report = {
					callback: null,
					form: null,
					content: null,
					button: null,
					init: function(){
						this.button.on('click', function(){_report.getList();});
						this.setDate(this.form.attr('id') + ' .startDate',this.form.attr('id') + ' .endDate');
					},
					setDate: function(fromId, toId) {
				    	$('#' + fromId).datetimepicker({
							format: 'DD/MM/YYYY'
						});
				    
					    $('#' + toId).datetimepicker({
					        format: 'DD/MM/YYYY'
					    });
					    
					    $("#" + fromId).on("dp.change", function(e) {
					        $('#' + toId).data("DateTimePicker").minDate(e.date);
					    });
					    
					    $("#" + toId).on("dp.change", function(e) {
					        $('#' + fromId).data("DateTimePicker").maxDate(e.date);
					    });
				    },
					getList: function(){
						var request = $.ajax({
				          url: "<?php echo base_url(); ?>admin/reporting/get_list",
				          method: "POST",
				          data: _report.form.serialize(),
				          dataType: 'html'
				        });
				         
				        request.done(function( data ) {
				            if(data!=null) {
								if(typeof(_report.callback)!=undefined) {
									_report.callback.call(this, data);
								}

								data = null;
				            }
		        		});

		        		request.error(function(_data){
		        			if(_data.readyState == 4 && _data.status == 200) {//Session expired and redirected
		        				swal({
								    title: "Session Expired!",
								    text: "Reloading page.",
								    icon: "warning",
								    dangerMode: false,
								})
								.then(willDelete => {
								    if (willDelete) {
								    	window.location.href = '<?php echo base_url();?>admin/login';
								    }
								});
		        			}
		        		});
					}
				};

				return _report;
			}	
		}

		$('[data-toggle="tooltip"]').tooltip({
			container: 'body'
		});		
	});

</script>


<style>
	hr.style-two {
	    border: 0;
	    height: 0;
	    border-top: 1px solid rgba(0, 0, 0, 0.1);
	    border-bottom: 1px solid rgba(255, 255, 255, 0.3);
	}	
</style>
<!-- page content -->
<div class="right_col" role="main" style="min-height: 100vh;">
	<div class="row">
		<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 page-title">
			<div class="title_left">
			<h3><?php echo $page_title; ?></h3>
			</div>
		</div>
	</div>
	<div class="col-md-12 col-lg-12 col-xl-10" style="padding-bottom: 60px;">
		<div class="x_panel">
			<div class="x_title">
				<h2>Report Type</h2>
				<ul class="nav navbar-right panel_toolbox" style="min-width:auto!important;">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">

			    <div class="col-xs-12 col-md-3">
					<ul id="myTab" class="nav nav-tabs tabs-left" role="tablist">
					
						<li role="presentation" class="active"><a href="#tab1"role="tab" data-toggle="tab" aria-expanded="true">Client Report</a>
						</li>

						<li role="presentation" ><a href="#tab2" role="tab" data-toggle="tab" aria-expanded="false">General Client Interest Report</a>
						</li>

						<li role="presentation" ><a href="#tab3" role="tab" data-toggle="tab" aria-expanded="false">How did they hear about us?</a>
						</li>

						<li role="presentation" ><a href="#tab4" role="tab" data-toggle="tab" aria-expanded="false">Remove | Unsubscribe | Cancelled Report</a>
						</li>

						<li role="presentation" ><a href="#tab5" role="tab" data-toggle="tab" aria-expanded="false">Member Log</a>
						</li>

						<li role="presentation" ><a href="#tab6" role="tab" data-toggle="tab" aria-expanded="false">Automated Unsubscribers Report</a>
						</li>
					</ul>
				</div>	
				<div class="col-xs-12 col-md-9">
					<div id="myTabContent" class="tab-content">
						<div role="tabpanel" class="tab-pane fade active in" id="tab1" aria-labelledby="">
							<?php client_report(); ?>
						</div>
						
						<div role="tabpanel" class="tab-pane fade" id="tab2" aria-labelledby="">
							<?php client_intereset_report($typeOfInterestArray, $usersArray); ?>
						</div>

						<div role="tabpanel" class="tab-pane fade" id="tab3" aria-labelledby="">
							<?php how_did_you_hear_report(); ?>
						</div>

						<div role="tabpanel" class="tab-pane fade" id="tab4" aria-labelledby="">
							<?php removed_unsubscribed_report(); ?>
						</div>

						<div role="tabpanel" class="tab-pane fade" id="tab5" aria-labelledby="">
							<?php member_log($membershipTypeArray, $memberStatusArray); ?>
						</div>

						<div role="tabpanel" class="tab-pane fade" id="tab6" aria-labelledby="">
							<?php automated_unsubscribers_report($unsubscribereasonArray); ?>
						</div>																								

					</div>
			    </div>			

			</div>
		</div>
	</div>

	<div class="clearfix"></div>
</div>

<?php

function client_report(){

?>

<form id="form1" class="form-horizontal form-label-left">
	<input type="hidden" name="reportType" value="1" />
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Start Date:</label>
		<div class="col-md-4 col-sm-4 col-xs-12">
	        <div class="input-group date startDate">
	            <input id="data-startDate" name="startDate" type='text' class="form-control" placeholder="dd/mm/yyyy" />
	            <span class="input-group-addon">
	            <span class="glyphicon glyphicon-calendar"></span>
	            </span>
	        </div>
		</div>
		<div class="col-md-1 col-sm-1 col-xs-12"><label class="control-label">TO</label></div>
		<div class="col-md-4 col-sm-4 col-xs-12">
	        <div class="input-group date endDate">
	            <input id="data-endDate" name="endDate" type='text' class="form-control" placeholder="dd/mm/yyyy" />
	            <span class="input-group-addon">
	            <span class="glyphicon glyphicon-calendar"></span>
	            </span>
	        </div>
		</div>
	</div>

	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">&nbsp;</label>
		<div class="col-md-4 col-sm-4 col-xs-12"><button id="report1-btn" type="button" class="btn btn-success btn-sm">Run</button></div>
	</div>
</form>

<hr class="style-two">

<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div id="report-holder1"></div>
	</div>
</div>

<script>

	$(document).ready(function(){
		var _report = report.instance();

		_report.form 	= $('#form1');
		_report.content = $('#report-holder1');
		_report.button	= $('#report1-btn');
		_report.init();

		_report.callback = function(data){
        	_report.content.html(data);

			$('[data-toggle="tooltip"]').tooltip({
				container: 'body'
			});	

		};		

	});

</script>

<?php } ?>

<?php

function client_intereset_report($typeOfInterestArray, $usersArray){

?>

<form id="form2" class="form-horizontal form-label-left">
	<input type="hidden" name="reportType" value="2" />
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Start Date:</label>
		<div class="col-md-4 col-sm-4 col-xs-12">
	        <div class="input-group date startDate">
	            <input id="data-startDate" name="startDate" type='text' class="form-control" placeholder="dd/mm/yyyy" />
	            <span class="input-group-addon">
	            <span class="glyphicon glyphicon-calendar"></span>
	            </span>
	        </div>
		</div>
		<div class="col-md-1 col-sm-1 col-xs-12"><label class="control-label">TO</label></div>
		<div class="col-md-4 col-sm-4 col-xs-12">
	        <div class="input-group date endDate">
	            <input id="data-endDate" name="endDate" type='text' class="form-control" placeholder="dd/mm/yyyy" />
	            <span class="input-group-addon">
	            <span class="glyphicon glyphicon-calendar"></span>
	            </span>
	        </div>
		</div>
	</div>

	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Type of Interest:</label>
		<div class="col-lg-5 col-md-9 col-sm-9 col-xs-12">
			<?php 
				//Note choosing System Enquiry, will automatically exclude any general clients who have had a system presentation.
				echo form_dropdown('typeOfInterest', $typeOfInterestArray, '', 'class="form-control"');

			?>
		</div>
	</div>

	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">User: <span class="badge bg-blue" style="font-size: 12px;" data-toggle="tooltip" data-toggle="tooltip" data-placement="top" title="Note: choosing System Enquiry, will automatically exclude any general clients who have had a system presentation."><i class="fa fa-question"></i></span></label>
		<div class="col-lg-5 col-md-9 col-sm-9 col-xs-12">
			<?php 
				echo form_dropdown('user', $usersArray, '', 'class="form-control"');
			?>
		</div>
	</div>

	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">&nbsp;</label>
		<div class="col-md-4 col-sm-4 col-xs-12"><button id="report2-btn" type="button" class="btn btn-success btn-sm">Run</button></div>
	</div>	

	
</form>

<hr class="style-two">

<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div id="report-holder2"></div>
	</div>
</div>

<?php
/*
id: "1773"
company: "PRDnationwide Canberra"
contactName: " "
contactNumber: "(02) 6295-4999"
memberStatus: "3"
conferenceInterestDate: null
conferenceInterestUser: null
onlineTrainingInterestDate: null
onlineTrainingInterestUser: null
productInterestDate: null
productInterestUser: null
systemEnquiryDate: "2009-07-28 00:00:00"
systemEnquiryUser: null
systemPresentationDate: null
systemPresentationUser: null
trainingInterestDate: null
trainingInterestUser: null
*/
?>

<script>

	$(document).ready(function(){
		var _report = report.instance();

		_report.form 	= $('#form2');
		_report.content = $('#report-holder2');
		_report.button	= $('#report2-btn');
		_report.init();

		_report.callback = function(data){
        	_report.content.html(data);

        	if(typeof(_result)!=undefined) {

				var arr = [];

				$.each(_result, function(i,data){
					arr.push(new ColList(data));
				});

	        	var columns = [];

				if(_typeOfInterest == -1) {
					columns = [
						        { data: 'id' },
						        { data: 'company' },
						        { data: 'contactName' },
						        { data: 'contactNumber' },
						        { data: 'memberStatus' },
						        { data: 'systemEnquiryDate'},
						        { data: 'systemEnquiryUser'},
						        { data: 'systemPresentationDate'},
						        { data: 'systemPresentationUser'},
						        { data: 'trainingInterestDate'},
						        { data: 'trainingInterestUser'},
						        { data: 'productInterestDate'},
						        { data: 'productInterestUser'},
						        { data: 'conferenceInterestDate'},
						        { data: 'conferenceInterestUser'},
						        { data: 'onlineTrainingInterestDate'},
						        { data: 'onlineTrainingInterestUser'},
						        { data: 'edit'}
						    ];
				}
				else{
					columns = [
						        { data: 'id' },
						        { data: 'company' },
						        { data: 'contactName' },
						        { data: 'contactNumber' },
						        { data: 'memberStatus' },
						        { data: 'date'},
						        { data: 'user'},
						        { data: 'edit'}
						    ];
				}

				$('#report2-table').DataTable( {
					data: arr,
				    columns: columns,
				    "scrollY": (_result.length >=10) ? 480 : false,
	    			"scrollX": true,
	    			"order": [[ 0, "asc" ]]
				});
			}
		};

		function ColList(data){
			this.id 			= data.id;
			this.company 		= data.company;
			this.contactName 	= data.contactName;
			this.contactNumber 	= data.contactNumber;
			this.edit 			= '<a href="<?php echo base_url(); ?>admin/members/edit/' + data.id + '" class="btn btn-xs btn-warning" target="_blank"><i class="fa fa-pencil"></i></button>';

			switch (parseInt(data.memberStatus)) {
				case 1:
					this.memberStatus = '<span class="text-success">Active</span>';
					break;

				case 2:
					this.memberStatus = '<span class="text-danger">Susp</span>';
					break;

				case 3:
					this.memberStatus = '<span class="text-warning">Cancelled</span>';
					break;

				default:
					this.memberStatus = '';
					break;
			}

			if(_typeOfInterest == -1) {
				this.systemEnquiryDate 			= (data.systemEnquiryDate != '') ? moment(data.systemEnquiryDate).format('DD/MM/Y') : '';
				this.systemEnquiryUser 			= (data.systemEnquiryUser != '') ? _users[data.systemEnquiryUser] : '';					
				this.systemPresentationDate 	= (data.systemPresentationDate != '') ? moment(data.systemPresentationDate).format('DD/MM/Y') : '';
				this.systemPresentationUser 	= (data.systemPresentationUser != '') ? _users[data.systemPresentationUser] : '';					
				this.trainingInterestDate 		= (data.trainingInterestDate != '') ? moment(data.trainingInterestDate).format('DD/MM/Y') : '';
				this.trainingInterestUser 		= (data.trainingInterestUser != '') ? _users[data.trainingInterestUser] : '';					
				this.productInterestDate 		= (data.productInterestDate != '') ? moment(data.productInterestDate).format('DD/MM/Y') : '';
				this.productInterestUser 		= (data.productInterestUser != '') ? _users[data.productInterestUser] : '';				
				this.conferenceInterestDate 	= (data.conferenceInterestDate != '') ? moment(data.conferenceInterestDate).format('DD/MM/Y') : '';
				this.conferenceInterestUser 	= (data.conferenceInterestUser != '') ? _users[data.conferenceInterestUser] : '';					
				this.onlineTrainingInterestDate = (data.onlineTrainingInterestDate != '') ? moment(data.onlineTrainingInterestDate).format('DD/MM/Y') : '';
				this.onlineTrainingInterestUser = (data.onlineTrainingInterestUser != '') ? _users[data.onlineTrainingInterestUser] : '';

				this.systemEnquiryDate 			= (this.systemEnquiryDate == 'Invalid date') ? '' : this.systemEnquiryDate;
				this.systemPresentationDate 	= (this.systemPresentationDate == 'Invalid date') ? '' : this.systemPresentationDate;
				this.trainingInterestDate 		= (this.trainingInterestDate == 'Invalid date') ? '' : this.trainingInterestDate;
				this.productInterestDate 		= (this.productInterestDate == 'Invalid date') ? '' : this.productInterestDate;
				this.conferenceInterestDate 	= (this.conferenceInterestDate == 'Invalid date') ? '' : this.conferenceInterestDate;
				this.onlineTrainingInterestDate = (this.onlineTrainingInterestDate == 'Invalid date') ? '' : this.onlineTrainingInterestDate;
				this.systemEnquiryUser 			= (this.systemEnquiryUser == undefined) ? '' : this.systemEnquiryUser;
				this.systemPresentationUser 	= (this.systemPresentationUser == undefined) ? '' : this.systemPresentationUser;
				this.trainingInterestUser 		= (this.trainingInterestUser == undefined) ? '' : this.trainingInterestUser;
				this.productInterestUser 		= (this.productInterestUser  == undefined) ? '' : this.productInterestUser ; 
				this.conferenceInterestUser 	= (this.conferenceInterestUser == undefined) ? '' : this.conferenceInterestUser;
				this.onlineTrainingInterestUser = (this.onlineTrainingInterestUser == undefined) ? '' : this.onlineTrainingInterestUser;
			}
			else {

				switch (_typeOfInterest) {
					case 1:
						this.date = (data.systemEnquiryDate != '') ? moment(data.systemEnquiryDate).format('DD/MM/Y') : '';
						this.user = (data.systemEnquiryUser != '') ? _users[data.systemEnquiryUser] : '';
						break;

					case 2:
						this.date = (data.systemPresentationDate != '') ? moment(data.systemPresentationDate).format('DD/MM/Y') : '';
						this.user = (data.systemPresentationUser != '') ? _users[data.systemPresentationUser] : '';
						break;

					case 3:
						this.date = (data.trainingInterestDate != '') ? moment(data.trainingInterestDate).format('DD/MM/Y') : '';
						this.user = (data.trainingInterestUser != '') ? _users[data.trainingInterestUser] : '';
						break;

					case 4:
						this.date = (data.productInterestDate != '') ? moment(data.productInterestDate).format('DD/MM/Y') : '';
						this.user = (data.productInterestUser != '') ? _users[data.productInterestUser] : '';
						break;

					case 5:
						this.date = (data.conferenceInterestDate != '') ? moment(data.conferenceInterestDate).format('DD/MM/Y') : '';
						this.user =	(data.conferenceInterestUser != '') ? _users[data.conferenceInterestUser] : '';
						break;

					case 6:
						this.date = (data.onlineTrainingInterestDate != '') ? moment(data.onlineTrainingInterestDate).format('DD/MM/Y') : '';
						this.user = (data.onlineTrainingInterestUser != '') ? _users[data.onlineTrainingInterestUser] : '';
						break;

					default:
						this.date = '';
						this.user = '';
						break;
				}

				this.user = (this.user==undefined) ? '' : this.user;
				this.date = (this.date == 'Invalid date') ? '' : this.date;
			}
        }

			
	});

</script>

<?php } ?>

<?php
	function how_did_you_hear_report() {
?>

<form id="form3" class="form-horizontal form-label-left">
	<input type="hidden" name="reportType" value="3" />
</form>

<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div id="report-holder3"></div>
	</div>
</div>

<script>

	$(document).ready(function(){
		var _report = report.instance();

		_report.form 	= $('#form3');
		_report.content = $('#report-holder3');
		_report.callback = function(data){
        	_report.content.html(data);

			$('[data-toggle="tooltip"]').tooltip({
				container: 'body'
			});	

		};

		_report.getList();

	});

</script>

<?php } ?>

<?php
	function removed_unsubscribed_report() {
?>

<form id="form4" class="form-horizontal form-label-left">
	<input type="hidden" name="reportType" value="4" />
</form>

<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div id="report-holder4"></div>
	</div>
</div>

<script>

	$(document).ready(function(){
		var _report = report.instance();

		_report.form 	= $('#form4');
		_report.content = $('#report-holder4');
		_report.callback = function(data){
        	_report.content.html(data);

        	if(_result.length>0) {
        		$.each(_result, function(i, v) {
        			var _total = {
        				get: function(v){
	        				var total = 0;
	        				$.each($('.' + v + '-ql'), function(j,k){
	        					total += parseInt($(k).data('count'));
	        				});

	        				return total;
        				}
        			};

        			$('#' + v + '-total').text(_total.get(v));
        		});
        	}
		};

		_report.getList();

	});

</script>

<?php } ?>

<?php
	function automated_unsubscribers_report($unsubscribereasonArray = NULL) {
?>

<form id="form6" class="form-horizontal form-label-left">
	<input type="hidden" name="reportType" value="6" />
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Start Date:</label>
		<div class="col-md-4 col-sm-4 col-xs-12">
	        <div class="input-group date startDate">
	            <input id="data-startDate" name="startDate" type='text' class="form-control" placeholder="dd/mm/yyyy" />
	            <span class="input-group-addon">
	            <span class="glyphicon glyphicon-calendar"></span>
	            </span>
	        </div>
		</div>
		<div class="col-md-1 col-sm-1 col-xs-12"><label class="control-label">TO</label></div>
		<div class="col-md-4 col-sm-4 col-xs-12">
	        <div class="input-group date endDate">
	            <input id="data-endDate" name="endDate" type='text' class="form-control" placeholder="dd/mm/yyyy" />
	            <span class="input-group-addon">
	            <span class="glyphicon glyphicon-calendar"></span>
	            </span>
	        </div>
		</div>
	</div>

	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Reason:</label>
		<div class="col-lg-5 col-md-9 col-sm-9 col-xs-12">
			<?php 
				//Note choosing System Enquiry, will automatically exclude any general clients who have had a system presentation.
				echo form_dropdown('reportunsubscribereason', $unsubscribereasonArray, '', 'id="reportunsubscribereason" class="form-control"');

			?>
		</div>
	</div>

	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">&nbsp;</label>
		<div class="col-md-4 col-sm-4 col-xs-12"><button id="report6-btn" type="button" class="btn btn-success btn-sm">Run</button></div>
	</div>	

	
</form>

<hr class="style-two">

<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div id="report-holder6"></div>
	</div>
</div>

<script>

	$(document).ready(function(){
		var _report = report.instance();

		_report.form 	= $('#form6');
		_report.content = $('#report-holder6');
		_report.button	= $('#report6-btn');
		_report.init();

		_report.callback = function(data){
        	_report.content.html(data);

        	if(typeof(_result)!=undefined) {
				var arr = [];

				var reasonText 	= $('#reportunsubscribereason option:selected').text();
				var reasonId 	= $('#reportunsubscribereason option:selected').val();

				if(parseInt(reasonId) === -1) {

				}
				else {
					$.each(_result, function(i,data){
						arr.push(new ColList(data, reasonText));
					});

		        	var columns  = [
							        { data: 'name' },
							        { data: 'company' },
							        { data: 'email' },
							        { data: 'mydate' },
							        { data: 'unsubscribereason' },
							        { data: 'comment'},
							        { data: 'view'}
							    ];

					$('#report6-table').DataTable( {
						data: arr,
					    columns: columns,
					    "scrollY": (_result.length >=10) ? 480 : false,
		    			"scrollX": true,
		    			"order": [[ 0, "asc" ]]
					});

					function ColList(data, reasonText){
						this.name 				= data.name;
						this.company 			= data.company;
						this.email 				= data.email;
						this.mydate 			= (data.mydate != '') ? moment(data.mydate).format('DD/MM/Y') : '';
						this.unsubscribereason 	= reasonText;
						this.comment 			= data.comment;
						this.view 				= '<a href="<?php echo base_url(); ?>admin/members/edit/' + data.memberid + '" class="btn btn-xs btn-warning" target="_blank"><i class="fa fa-eye"></i></button>';

						this.mydate = (this.mydate == 'Invalid date') ? '' : this.mydate;
			        }
		    	}
				
			}
		}		

	});

</script>

<?php } ?>

<?php
	function member_log($membershipTypeArray, $memberStatusArray) {
?>

<form id="form5" class="form-horizontal form-label-left">
	<input type="hidden" name="reportType" value="5" />

	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Membership:</label>
		<div class="col-lg-5 col-md-9 col-sm-9 col-xs-12">
			<?php 
				echo form_dropdown('membershipType', $membershipTypeArray, '', 'id="membershipType" class="form-control"');

			?>
		</div>
	</div>

	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Status:</label>
		<div class="col-lg-5 col-md-9 col-sm-9 col-xs-12">
			<?php 
				echo form_dropdown('memberStatus', $memberStatusArray, '', 'id="memberStatus" class="form-control"');

			?>
		</div>
	</div>

	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">&nbsp;</label>
		<div class="col-md-4 col-sm-4 col-xs-12"><button id="report5-btn" type="button" class="btn btn-success btn-sm">Run</button></div>
	</div>	

	
</form>

<hr class="style-two">

<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div id="report-holder5"></div>
	</div>
</div>

<script>

	$(document).ready(function(){
		var _report = report.instance();

		_report.form 	= $('#form5');
		_report.content = $('#report-holder5');
		_report.button	= $('#report5-btn');
		_report.init();

		_report.callback = function(data){
        	_report.content.html(data);

        	if(typeof(_result)!=undefined) {
        		var arr = [];

				$.each(_result, function(i,data){
					arr.push(new ColList(data));
				});

	        	var columns  = [
						        { data: 'id' },
						        { data: 'company' },
						        { data: 'membershipTypeName' },
						        { data: 'stateName' },
						        { data: 'pendingDownloads' },
						        { data: 'downloads' },
						        { data: 'lastLogin' },
						        { data: 'totalLogin' },
						        { data: 'edit' }
						    ];

				$('#report5-table').DataTable( {
					data: arr,
				    columns: columns,
				    "scrollY": (_result.length >=10) ? 480 : false,
	    			"scrollX": true,
	    			"order": [[ 1, "asc" ]]
				});

				function ColList(data, reasonText){
					this.id 				= data.id;
					this.company 			= data.company;
					this.membershipTypeName = data.membershipTypeName;
					this.stateName			= data.stateName;
					this.pendingDownloads 	= (parseInt(data.downloadCount) < parseInt(data.totalDownloadCount)) ? '<i class="fa fa-check text-danger"></i>' : '';
					this.downloads 			= data.downloadCount + ' of ' + data.totalDownloadCount;
					this.lastLogin 			= (data.lastLogin != '') ? moment(data.lastLogin).format('DD-MMM-Y') : '';
					this.totalLogin 		= data.totalLogins;
					this.comment 			= data.comment;
					this.edit 				= '<a href="<?php echo base_url(); ?>admin/members/edit/' + data.id + '" class="btn btn-xs btn-success" target="_blank"><i class="fa fa-pencil"></i></button>';

					this.lastLogin = (this.lastLogin == 'Invalid date') ? '' : this.lastLogin;
		        }
			}
		}		

	});

</script>

<?php } ?>

<!-- /page content -->