<div style="max-height: 60vh; overflow-y: scroll; border-radius: 5px; padding: 0 5px;">

<?php if($reportType === 1) { ?>

<table class="table table-striped text-left dt-responsive nowrap" data-page-length='10' cellspacing="0" width="100%"><!-- table-bordered  -->
	<thead>
		<tr>
			<th colspan="3"><h4 class="text-center">General Clients</h4></th>
		</tr>
		<tr>
			<th style="text-align:left!important; vertical-align: middle!important;">Description</th>
			<th style="text-align:left!important; vertical-align: middle!important;">Type</th>
			<th style="text-align:left!important; vertical-align: middle!important;">Number</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Total General Clients</td>
			<td>Individual</td>
			<td><span class="badge bg-green"><?php echo number_format($generalClients['total_general_clients_individual']); ?></span></td>
		</tr>
		<tr>
			<td>Total General Clients</td>
			<td>Staff email addresses</td>
			<td><span class="badge bg-green"><?php echo number_format($generalClients['total_general_clients_staff']); ?></span></td>
		</tr>

		<?php
			foreach ($states as $key => $value) {
		?>

		<tr>
			<td><?php echo $value['individual'][0]['state']; ?></td>
			<td>Individual</td>
			<td><span class="badge bg-green"><?php echo number_format($value['individual'][0]['count']); ?></span></td>
		</tr>
		<tr>
			<td><?php echo $value['staff'][0]['state']; ?></td>
			<td>Staff Email</td>
			<td><span class="badge bg-green"><?php echo number_format($value['staff'][0]['count']); ?></span></td>
		</tr>		

		<?php
			

			}
		?>

		<tr><td colspan="3">&nbsp;</td></tr>

		<?php
			foreach ($membershipType_gc as $key => $value) {
		?>

		<tr>
			<td><?php echo $value['individual'][0]['name']; ?></td>
			<td>Individual</td>
			<td><span class="badge bg-green"><?php echo number_format($value['individual'][0]['count']); ?></span></td>
		</tr>
		<tr>
			<td><?php echo $value['staff'][0]['name']; ?></td>
			<td>Staff Email</td>
			<td><span class="badge bg-green"><?php echo number_format($value['staff'][0]['count']); ?></span></td>
		</tr>		

		<?php
			

			}
		?>

		<tr>
			<td>Total All Clients</td>
			<td>Individual</td>
			<td><span class="badge bg-green"><?php echo number_format($generalClients['total_all_clients_individual']); ?></span></td>
		</tr>
		<tr>
			<td>Total All Clients</td>
			<td>Staff email addresses</td>
			<td><span class="badge bg-green"><?php echo number_format($generalClients['total_all_clients_staff']); ?></span></td>
		</tr>		





	</tbody>
</table>

<table class="table table-striped text-left dt-responsive nowrap" data-page-length='10' cellspacing="0" width="100%"><!-- table-bordered  -->
	<thead>
		<tr>
			<th colspan="3"><h4 class="text-center">Member Type</h4></th>
		</tr>
		<tr>
			<th style="text-align:left!important; vertical-align: middle!important;">Description</th>
			<th style="text-align:left!important; vertical-align: middle!important;">Type</th>
			<th style="text-align:left!important; vertical-align: middle!important;">Number</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Total Members</td>
			<td>Individual</td>
			<td><span class="badge bg-green"><?php echo number_format($members['total_all_individual']); ?></span></td>
		</tr>
		<tr>
			<td>Total Members</td>
			<td>Staff email addresses</td>
			<td><span class="badge bg-green"><?php echo number_format($members['total_all_staff']); ?></span></td>
		</tr>

		<?php
			foreach ($membershipType_m as $key => $value) {
		?>	

		<tr>
			<td><?php echo $value['individual'][0]['name']; ?></td>
			<td>Individual</td>
			<td><span class="badge bg-green"><?php echo number_format($value['individual'][0]['count']); ?></span></td>
		</tr>
		<tr>
			<td><?php echo $value['staff'][0]['name']; ?></td>
			<td>Staff Email</td>
			<td><span class="badge bg-green"><?php echo number_format($value['staff'][0]['count']); ?></span></td>
		</tr>

		<?php
			}
		?>



		<tr>
			<td>New Member</td>
			<td></td>
			<td><span class="badge bg-green"><?php echo number_format($members['total_new']); ?></span></td>
		</tr>
		<tr>
			<td>Cancelled Members</td>
			<td></td>
			<td><span class="badge bg-green"><?php echo number_format($members['total_cancelled']); ?></span></td>
		</tr>	

	</tbody>
</table>

<?php
	foreach ($subscribers as $key => $value) {
?>	

<table class="table table-striped text-left dt-responsive nowrap" data-page-length='10' cellspacing="0" width="100%"><!-- table-bordered  -->
	<thead>
		<tr>
			<th colspan="3"><h4 class="text-center"><?php echo $value['label']; ?> Subscribers</h4></th>
		</tr>
		<tr>
			<th style="text-align:left!important; vertical-align: middle!important;">Description</th>
			<th colspan="2" style="text-align:left!important; vertical-align: middle!important;">Number</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Current</td>
			<td colspan="2"><span class="badge bg-green"><?php echo number_format($value['current']); ?></span></td>
		</tr>

		<?php if($key === 'newsletter') { ?>
		<tr>
			<td>Approx No. of Landlords to Receive</td>
			<td colspan="2"><span class="badge bg-green"><?php echo number_format($value['approx']); ?></span></td>
		</tr>
		<?php } ?>

		<tr>
			<td>Unsubscribed</td>
			<td colspan="2"><span class="badge bg-blue" style="font-size: 12px;" data-toggle="tooltip" data-toggle="tooltip" data-placement="top" title="<?php echo $value['unsubscribed_notes']; ?>"><i class="fa fa-question"></i></span> <span class="badge bg-green"><?php echo number_format($value['unsubscribed']); ?></span></td>
		</tr>
		<tr>
			<td>New</td>
			<td colspan="2"><span class="badge bg-blue" style="font-size: 12px;" data-toggle="tooltip" data-toggle="tooltip" data-placement="top" title="<?php echo $value['new_notes']; ?>"><i class="fa fa-question"></i></span> <span class="badge bg-green"><?php echo number_format($value['new']); ?></span></td>
		</tr>
		<tr>
			<td>Renewals</td>
			<td colspan="2"><span class="badge bg-blue" style="font-size: 12px;" data-toggle="tooltip" data-toggle="tooltip" data-placement="top" title="<?php echo $value['new_notes']; ?>"><i class="fa fa-question"></i></span> <span class="badge bg-green"><?php echo number_format($value['renewal']); ?></span></td>
		</tr>
		<tr>
			<td>Double Ups</td>
			<td colspan="2"><span class="badge bg-blue" style="font-size: 12px;" data-toggle="tooltip" data-toggle="tooltip" data-placement="top" title="<?php echo $value['doubleups_notes']; ?>"><i class="fa fa-question"></i></span> <span class="badge bg-green"><?php echo number_format($value['doubleups']); ?></span></td>
		</tr>
		<tr><td colspan="3"><strong>Expired:</strong></td></tr>
		<tr>
			<td colspan="3"><strong><?php echo (!empty($startDate)) ? 'Day 0 is: ' . $startDate : ''; ?></strong></td>
		</tr>

		<?php 
			if(is_array($value['expired'])) { 
				foreach ($value['expired'] as $k => $v) {
		?>
				<tr>
					<?php if($v['total'] > 0) { ?>
						<td><?php echo $v['days']; ?> Days</td>
						<td><span class="badge bg-green"><?php echo $v['total']; ?></span></td>
						<td>
							<div class="col-sm-1"><span class="badge bg-blue" style="font-size: 12px;" data-toggle="tooltip" data-toggle="tooltip" data-placement="top" title="<?php echo $v['notes']; ?>"><i class="fa fa-question"></i></span></div>
							<div class="col-sm-5"><?php echo '<strong>Ex. Gst: </strong> <i class="fa fa-dollar"></i>' . $v['ex_gst']; ?></div>
							<div class="col-sm-6"><?php echo '<strong>Inc Gst: </strong> <i class="fa fa-dollar"></i>' . $v['inc_gst']; ?></div>
						</td>
					<?php } else { ?>
						<td><?php echo $v['days']; ?> Days</td>
						<td colspan="2">No Data</td>
					<?php } ?>
				</tr>

		<?php 
				}
			}
		?>

	</tbody>
</table>

<?php
	

	}
?>

<table class="table table-striped text-left dt-responsive nowrap" data-page-length='10' cellspacing="0" width="100%"><!-- table-bordered  -->
	<thead>
		<tr>
			<th colspan="3"><h4 class="text-center">Remove / Unsubscribe Requests</h4></th>
		</tr>
		<tr>
			<th style="text-align:left!important; vertical-align: middle!important;">Description</th>
			<th style="text-align:left!important; vertical-align: middle!important;">Number</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>General Clients</td>
			<td><span class="badge bg-blue" style="font-size: 12px;" data-toggle="tooltip" data-toggle="tooltip" data-placement="top" title="<?php echo $unsubscribe_request['general_clients']['notes']; ?>"><i class="fa fa-question"></i></span> <span class="badge bg-green"><?php echo number_format($unsubscribe_request['general_clients']['count']); ?></span></td>
		</tr>
		<tr>
			<td>Newsletter Subscribers</td>
			<td><span class="badge bg-blue" style="font-size: 12px;" data-toggle="tooltip" data-toggle="tooltip" data-placement="top" title="<?php echo $unsubscribe_request['newsletter']['notes']; ?>"><i class="fa fa-question"></i></span> <span class="badge bg-green"><?php echo number_format($unsubscribe_request['newsletter']['count']); ?></span></td>
		</tr>	
		<tr>
			<td>On-Line Training Subscribers</td>
			<td><span class="badge bg-blue" style="font-size: 12px;" data-toggle="tooltip" data-toggle="tooltip" data-placement="top" title="<?php echo $unsubscribe_request['online']['notes']; ?>"><i class="fa fa-question"></i></span> <span class="badge bg-green"><?php echo number_format($unsubscribe_request['online']['count']); ?></span></td>
		</tr>		
	</tbody>
</table>

<table class="table table-striped text-left dt-responsive nowrap" data-page-length='10' cellspacing="0" width="100%"><!-- table-bordered  -->
	<thead>
		<tr>
			<th colspan="3"><h4 class="text-center">Contact Database Updates</h4></th>
		</tr>
		<tr>
			<th style="text-align:left!important; vertical-align: middle!important;">Description</th>
			<th style="text-align:left!important; vertical-align: middle!important;">Number <span class="badge bg-blue" style="font-size: 12px;" data-toggle="tooltip" data-toggle="tooltip" data-placement="top" title="<?php echo $db_updates['notes']; ?>"><i class="fa fa-question"></i></span></th>
		</tr>
	</thead>
	<tbody>
		<?php
			foreach ($db_updates['user'] as $key => $value) {
		?>	
		<tr>
			<td><?php echo $value['username']; ?></td>
			<td><span class="badge bg-green"><?php echo number_format($value['count']); ?></span></td>
		</tr>
		<?php
			}
		?>	
	</tbody>
</table>


<?php } ?>

<?php if($reportType === 3) { ?>

	<?php
		foreach ($howdidtheyhear as $key => $value) {
	?>

		<table class="table table-striped text-left dt-responsive nowrap" data-page-length='10' cellspacing="0" width="100%"><!-- table-bordered  -->
			<thead>
				<tr>
					<th colspan="3"><h4 class="text-center"><?php echo $value['title']; ?></h4></th>
				</tr>
				<tr>
					<th style="text-align:left!important; vertical-align: middle!important;">Description</th>
					<th style="text-align:left!important; vertical-align: middle!important;">Number</th>
				</tr>
			</thead>
			<tbody>
				<?php
					foreach ($value['data'] as $k => $v) {
				?>	
				<tr>
					<td><?php echo $v['title']; ?></td>
					<td><span class="badge bg-green"><?php echo number_format($v['count']); ?></span></td>
				</tr>
				<?php
					}
				?>	
			</tbody>
		</table>

	<?php
		}
	?>	

<?php } ?>

<?php if($reportType === 4) { ?>

	<?php
		$section = array();

		foreach ($removed_unsubscribed as $key => $value) {
			$section[] = $key;
	?>

		<table class="table table-striped text-left dt-responsive nowrap" data-page-length='10' cellspacing="0" width="100%"><!-- table-bordered  -->
			<thead>
				<tr>
					<th colspan="3"><h4 class="text-center"><?php echo $value['title']; ?></h4></th>
				</tr>
				<tr>
					<th style="text-align:left!important; vertical-align: middle!important;">Description</th>
					<th style="text-align:left!important; vertical-align: middle!important;">Number</th>
				</tr>
			</thead>
			<tbody>

				<tr>
					<td><?php echo $value['total']['total_title']; ?></td>
					<td><span class="badge bg-green"><?php echo number_format($value['total']['total_count']); ?></span></td>
				</tr>

				<?php
					$hasQL = FALSE;

					foreach ($value['states'] as $k => $v) {
				?>

					<?php 
						if(intval($v['state']) !== 1 && !empty($v['stateSubRegion'])) {
							continue;
						}
						elseif(intval($v['state']) === 1 && !empty($v['stateSubRegion'])) {
					?>
						<?php if(!$hasQL) { $hasQL = TRUE; ?>
					<tr>
						<td><?php echo $stateDictionaryMember[intval($v['state'])]; ?></td>
						<td><span id="<?php echo $key . '-total'; ?>" class="badge bg-blue">0</span></td>
					</tr>							
						<?php } ?>

					<tr>
						<td><?php echo " - " . $this->ppmsystemlib->get_data_arr('stateSubRegionArray')[intval($v['stateSubRegion']) - 1]; ?></td>
						<td><span class="<?php echo $key . '-ql'; ?> badge bg-green" data-count="<?php echo $v['count']; ?>"><?php echo number_format($v['count']); ?></span></td>
					</tr>
					<?php
						}
						else {
							$ql = (intval($v['state']) === 1 && empty($v['stateSubRegion'])) ? $key . '-ql' : '';
					?>

					<tr>
						<td><?php echo $stateDictionaryMember[intval($v['state'])]; if(intval($v['state']) === 1 && empty($v['stateSubRegion'])) echo ' (Sub-region not specified)'; ?></td>
						<td><span class="<?php echo $ql; ?> badge bg-green" data-count="<?php echo $v['count']; ?>"><?php echo number_format($v['count']); ?></span></td>
					</tr>

					<?php
						}
					?>	

				<?php
					}
				?>

				<tr><td colspan="2"><h4>Reason</h4></td></tr>

				<?php
					foreach ($value['reason'] as $k => $v) {
				?>
					<tr>
						<td><?php echo (empty($v['reason'])) ? 'Not Applied' : $v['reason']; ?></td>
						<td><span class="badge bg-green"><?php echo number_format($v['count']); ?></span></td>
					</tr>
				<?php
					}
				?>
			</tbody>
		</table>

	<?php
		}
	?>

<script>
	var _result = <?php echo json_encode($section); ?>;
</script>

<?php } ?>	

</div><?php //overflow scroll ?>

<?php if($reportType === 2) { ?>

<table id="report2-table" class="table table-striped table-hover text-left dt-responsive nowrap" data-page-length='10' cellspacing="0" width="100%"><!-- table-bordered  -->
	<thead>
		<tr>
			<th style="text-align:left!important; vertical-align: middle!important;">ID</th>
			<th style="text-align:left!important; vertical-align: middle!important;">Company Name</th>
			<th style="text-align:left!important; vertical-align: middle!important;">General Accounts Contact</th>
			<th style="text-align:left!important; vertical-align: middle!important;">Contact Number</th>
			<th style="text-align:left!important; vertical-align: middle!important;">Client Status</th>

            <?php if($typeOfInterest === -1 OR $typeOfInterest === 1) { ?>
                <th>System Enquiry Date</th>
                <th>System Enquiry User</th>
            <?php } ?>	

            <?php if($typeOfInterest === -1 OR $typeOfInterest === 2) { ?>
                <th>System Presentation Date</th>
                <th>System Presentation User</th>
            <?php } ?>

            <?php if($typeOfInterest === -1 OR $typeOfInterest === 3) { ?>
                <th>Training Interest Date</th>
                <th>Training Interest User</th>
            <?php } ?>

            <?php if($typeOfInterest === -1 OR $typeOfInterest === 4) { ?>
                <th>Product Interest Date</th>
                <th>Product Interest User</th>
            <?php } ?>

            <?php if($typeOfInterest === -1 OR $typeOfInterest === 5) { ?>
                <th>Conference Interest Date</th>
                <th>Conference Interest User</th>
            <?php } ?>

            <?php if($typeOfInterest === -1 OR $typeOfInterest === 6) { ?>
                <th>Online Training Interest Date</th>
                <th>Online Training Interest User</th>
            <?php } ?>
			<th style="text-align:left!important; vertical-align: middle!important;">Edit</th>                                
		</tr>
	</thead>
	<tbody>
	</tbody>
</table>

<script>
	var _result = <?php echo json_encode($result); ?>;
	var _typeOfInterest = <?php echo $typeOfInterest; ?>;
</script>

<?php } ?>

<?php if($reportType === 5) { ?>


	<table id="report5-table" class="table table-striped table-hover text-left dt-responsive nowrap" data-page-length='10' cellspacing="0" width="100%"><!-- table-bordered  -->
		<thead>
			<tr>
				<th style="text-align:left!important; vertical-align: middle!important;">ID</th>
				<th style="text-align:left!important; vertical-align: middle!important;">Company</th>
				<th style="text-align:left!important; vertical-align: middle!important;">Membership Type</th>
				<th style="text-align:left!important; vertical-align: middle!important;">State</th>
				<th style="text-align:left!important; vertical-align: middle!important;">Pending Downloads</th>
				<th style="text-align:left!important; vertical-align: middle!important;">Downloads</th>
			<?php if(FALSE) { ?>
				<th style="text-align:left!important; vertical-align: middle!important;">Total Emails</th>
				<th style="text-align:left!important; vertical-align: middle!important;">Opened Emails</th>
				<th style="text-align:left!important; vertical-align: middle!important;">Unopened Emails</th>
			<?php } ?>
				<th style="text-align:left!important; vertical-align: middle!important;">Last Login</th>
				<th style="text-align:left!important; vertical-align: middle!important;">Total Logins</th>
				<th style="text-align:left!important; vertical-align: middle!important;">Edit</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>	

	<script>
		var _result = <?php echo json_encode($result); ?>;
	</script>	

<?php } ?>

<?php if($reportType === 6) { ?>

	<?php if($isAll) { ?>

	<table id="report6-table" class="table table-striped table-hover text-left dt-responsive nowrap" data-page-length='10' cellspacing="0" width="100%"><!-- table-bordered  -->
		<thead>
			<tr>
				<th style="text-align:left!important; vertical-align: middle!important;">Reason</th>
				<th style="text-align:left!important; vertical-align: middle!important;">Count</th>                         
			</tr>
		</thead>
		<tbody>

		<?php
			foreach ($result as $key => $value) {
		?>	
		<tr>
			<td><?php echo $this->ppmsystemlib->get_data_arr('unsubscribeReasonArray')[intval($value['unsubscribereason']) - 1]; ?></td>
			<td><span class="badge bg-green"><?php echo number_format($value['count']); ?></span></td>
		</tr>
		<?php
			}
		?>	

		</tbody>
	</table>

	<?php } else { ?>
	<table id="report6-table" class="table table-striped table-hover text-left dt-responsive nowrap" data-page-length='10' cellspacing="0" width="100%"><!-- table-bordered  -->
		<thead>
			<tr>
				<th style="text-align:left!important; vertical-align: middle!important;">Name</th>
				<th style="text-align:left!important; vertical-align: middle!important;">Company</th>
				<th style="text-align:left!important; vertical-align: middle!important;">Email</th>
				<th style="text-align:left!important; vertical-align: middle!important;">Date</th>
				<th style="text-align:left!important; vertical-align: middle!important;">Unsubscribe Reason</th>
				<th style="text-align:left!important; vertical-align: middle!important;">Comment</th>
				<th style="text-align:left!important; vertical-align: middle!important;">View</th>                            
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>

	<script>
		var _result = <?php echo json_encode($result); ?>;
	</script>	
	<?php } ?>

<?php } ?>