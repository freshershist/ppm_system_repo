<!-- page content -->
<div class="right_col" role="main" style="min-height: 100vh;">

  <div class="row">
    <div class="col-md-12">
      <div id="divHead">

          <?php if(empty($page_id) || $page_id == 0){ ?>
          <h1><i class="fa fa-calendar"></i> New Online Training</h1>
          <?php }else{ ?>
          <h1><i class="fa fa-calendar"></i> Edit Online Training</h1>
          <?php 
            // print_r($training_info);
            foreach($training_info['result'] as $info){
              $id =$info['id'];
              $topic =$info['topic'];
              $title =$info['title'];
              $shortDesc =$info['shortDesc'];
              $speaker =$info['speaker'];
              $duration =$info['duration'];
              // $body =$info['body'];
              // $activity =$info['activity'];
              $d1 =$info['d1'];
              $d2 =$info['d2'];
              $d3 =$info['d3'];
              $d4 =$info['d4'];
              $d5 =$info['d5'];
              $c1 =$info['c1'];
              $c2 =$info['c2'];
              $c3 =$info['c3'];
              $c4 =$info['c4'];
              $c5 =$info['c5'];
              $myType =$info['myType'];
              $mediaFile =$info['mediaFile'];
              $hits =$info['hits'];
              $products =$info['products'];
              $articles =$info['articles'];
              $downloads =$info['downloads'];
              $orderby =$info['orderby'];
              $draft =$info['draft'];
              $guestAccess =$info['guestAccess'];
            }
          ?>

          <?php } ?>
          <div class="row">
            <div class="col-md-12" id="dispMessages"></div>
          </div>
          <div class="spacer20"></div>
          <div class="row">
              <div class="col-md-12">
                  <a href="<?php echo base_url().'admin/onlinetrainings'; ?>" class="btn btn-primary">
                      <i class="fa fa-arrow-left"></i> Back
                  </a>
                  <button type="button" class="btn btn-primary" id="btnSave"><i class="fa fa-save"></i> Save</button>
              </div>
          </div>

      </div>
      <div class="spacer10"></div>
      <hr/>
      <!-- <div class="spacer10"></div> -->
      <form id="frmOnlineTraining">
        <div class="row">
          <div class="col-md-3">

                  <div class="row">
                    <div class="col-md-4 form-group">
                        <label>* Type : </label>
                        <input type="hidden" name="inpOID" value="<?php echo (isset($id)) ? $id:''; ?>">
                        <select id="inpType1" name="inpType1" class="form-control">
                            <option value="-" <?php echo (isset($myType) && $myType == '-') ? 'selected':''; ?>>[Select]</option>
                            <option value="1" <?php echo (isset($myType) && $myType == '1') ? 'selected':''; ?>>Video</option>
                            <option value="2" <?php echo (isset($myType) && $myType == '2') ? 'selected':''; ?>>Audio</option>
                        </select>
                        <input type="hidden" id="inpType" name="inpType" value="<?php echo (isset($myType))? $myType:'';  ?>">
                    </div>
                    <div class="col-md-4 form-group">
                        <label>Status</label>
                        <select id="inpIsDraft1" name="inpIsDraft1" class="form-control">
                            <option value="1"  <?php echo (isset($draft) && $draft == '1') ? 'selected':''; ?>>Draft</option>
                            <option value="0"  <?php echo (isset($draft) && $draft == '0') ? 'selected':''; ?>>Live / Publish</option>
                        </select>
                        <input type="hidden" id="inpIsDraft" name="inpIsDraft" value="<?php echo (isset($draft))? $draft:'';  ?>">
                    </div>
                  </div>
                  <div class="row">
                      <div class="col-md-4 form-group">
                          <label>Guest Access</label>
                          <input id="data-active" name="isGuest" type="checkbox" class="js-switch"  <?php echo (isset($guestAccess) && $guestAccess == '1') ? 'checked':''; ?>/>
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-md-12 form-group">
                          <label>* Topic</label>
                          <select id="inpTopic1" name="inpTopic1" class="form-control">
                              <option value="-">[select]</option>
                              <?php
                                foreach($ol_training_list['result'] as $ol_topic){
                                  (isset($topic) && $topic == $ol_topic['name']) ? $sel = 'selected' : $sel = '';
                                  echo '<option value="'.$ol_topic['name'].'" '.$sel.'>'.$ol_topic['name'].'</option>';
                                }
                              ?>
                          </select>
                          <input type="hidden" id="inpTopic" name="inpTopic" value="<?php echo (isset($topic))? $topic:'';  ?>">

                      </div>
                  </div>

                  <div class="row">
                      <div class="col-md-12 form-group">
                          <label>* Order </label>
                          <input type="text" id="inpOrder" name="inpOrder" class="form-control" value="<?php echo (isset($orderby)) ? $orderby:''; ?>">
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-md-12 form-group">
                          <label>* Title </label>
                          <input type="text" id="inpTitle" name="inpTitle" class="form-control" value="<?php echo (isset($title)) ? $title:''; ?>">
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-md-12 form-group">
                          <label>Speaker </label>
                          <input type="text" id="inpSpeaker" name="inpSpeaker" class="form-control" value="<?php echo (isset($speaker)) ? $speaker:''; ?>">
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-md-12 form-group">
                          <label>Duration </label>
                          <input type="text" id="inpDuration" name="inpDuration" class="form-control" value="<?php echo (isset($duration)) ? $duration:''; ?>">
                      </div>
                  </div>

              
          </div>





          <div class="col-md-3">
                  <div class="row">
                      <div class="col-md-12 form-group">
                          <label>Short Description </label>
                          <textarea id="inpDescription" name="inpDescription" class="form-control"><?php echo (isset($shortDesc)) ? $shortDesc:''; ?></textarea>
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-md-12 form-group">
                          <label>Description </label>
                          <?php if(isset($editor)) echo $editor; ?>
                      </div>
                      <textarea id="sub_editor_1" name="sub_editor_1" style="display:none"></textarea>
                  </div>


          </div>

          <div class="col-md-3">
                  <div class="row">
                      <div class="col-md-12 form-group">
                          <label>Activity </label>
                          <?php if(isset($editor1)) echo $editor1; ?>
                      </div>
                      <textarea id="sub_editor_2" name="sub_editor_2" style="display:none"></textarea>
                  </div>
          </div>

          <div class="col-md-3">
                  <div class="row">
                      <div class="col-md-12 form-group">
                          <label>Attachments </label>
                          
                          <div class="row">
                            <div class="col-md-12">
                              <label>* Media File</label>
                              <div class="input-group">
                              <input type="text" class="form-control" name="inpMedia" value="<?php echo (isset($mediaFile)) ? $mediaFile:''; ?>">
                              <span class="input-group-btn">
                              <button type="button" class="ppm-browse btn btn-primary btnMedia-image" data-type="image">Browse | <i class="fa fa-image"></i></button>
                              <button type="button" class="ppm-browse btn btn-primary btnMedia-audio" data-type="audio">Browse | <i class="fa fa-file-audio-o"></i></button>
                              <button type="button" class="ppm-browse btn btn-primary btnMedia-video" data-type="video">Browse | <i class="fa fa-file-video-o"></i></button>
                              </span>
                              </div>
                            </div>
                          </div>

                          <hr>

                          <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#MoreAttachment">More Attachments</button>

                          <div id="MoreAttachment" class="collapse" style="margin-top: 10px; max-height: 400px; overflow: hidden auto; ">

                          <div class="row">
                            <div class="col-md-12">
                              <label>Attachment 1</label>
                              <div class="input-group">
                              <input type="text" class="form-control" name="inpD1"  value="<?php echo (isset($d1)) ? $d1:''; ?>">
                              <span class="input-group-btn">
                              <button  type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
                              </span>
                              </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Caption 1</label>
                                    <input type="text" name="inpC1" class="form-control"  value="<?php echo (isset($c1)) ? $c1:''; ?>">
                                </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-md-12">
                              <label>Attachment 2</label>
                              <div class="input-group">
                              <input type="text" class="form-control" name="inpD2"  value="<?php echo (isset($d2)) ? $d2:''; ?>">
                              <span class="input-group-btn">
                              <button type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
                              </span>
                              </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Caption 2</label>
                                    <input type="text" name="inpC2" class="form-control" value="<?php echo (isset($c2)) ? $c2:''; ?>">
                                </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-md-12">
                              <label>Attachment 3</label>
                              <div class="input-group">
                              <input type="text" class="form-control" name="inpD3" value="<?php echo (isset($d3)) ? $d3:''; ?>">
                              <span class="input-group-btn">
                              <button type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
                              </span>
                              </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Caption 3</label>
                                    <input type="text" name="inpC3" class="form-control" value="<?php echo (isset($c3)) ? $c3:''; ?>">
                                </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-md-12">
                              <label>Attachment 4</label>
                              <div class="input-group">
                              <input type="text" class="form-control" name="inpD4" value="<?php echo (isset($d4)) ? $d4:''; ?>">
                              <span class="input-group-btn">
                              <button  type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
                              </span>
                              </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Caption 4</label>
                                    <input type="text" name="inpC4" class="form-control" value="<?php echo (isset($c4)) ? $c4:''; ?>">
                                </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-md-12">
                              <label>Attachment 5</label>
                              <div class="input-group">
                              <input type="text" class="form-control" name="inpD5" value="<?php echo (isset($d5)) ? $d5:''; ?>">
                              <span class="input-group-btn">
                              <button  type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
                              </span>
                              </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Caption 5</label>
                                    <input type="text" name="inpC5" class="form-control" value="<?php echo (isset($c5)) ? $c5:''; ?>">
                                </div>
                            </div>
                          </div>

                          </div><!-- #MoreAttachment -->

                      </div>
                  </div>
          </div>

        </div>
      </form>



    </div>

  </div>

	<!-- <div class="clearfix"></div> -->
</div>


<!-- /page content -->

<style>
.spacer10 {
  clear: both;
  width: 100%;
  height: 10px;
}
.spacer15 {
  clear: both;
  width: 100%;
  height: 15px;
}
.spacer20 {
  clear: both;
  width: 100%;
  height: 20px;
}
.spacer25 {
  clear: both;
  width: 100%;
  height: 25px;
}
.spacer30 {
  clear: both;
  width: 100%;
  height: 30px;
}

.hide-me {
  display: none;
}

.right_col {
  min-height: 100vh !important;
}

</style>
<script src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
<script>
$(window).scroll(function(){
  var bn = $('#divHead');
  if($(window).scrollTop() > 0) {
    bn.addClass('stickme');
  }
  else{
    bn.removeClass('stickme');
  }
});
</script>

<script>
$(document).ready(function(){
  $('#btnAddEventPrice').click(function(){
    var html_ = '<tr>'
                 + '<td><button type="button" class="btn btn-danger btnRemEventPrice"><i class="fa fa-minus"></i></button></td>'
                 + '<td><input type="text" class="form-control"></td>'
                 + '<td><input type="text" class="form-control"></td>'
                 + '<td><select class="form-control"></select></td>'
                 + '</tr>'
    $('#tblEventPrice tbody').append(html_);
  });


  $('#tblEventPrice').on('click', '.btnRemEventPrice', function(){
      $(this).parent().parent().remove();

  });

  $('.btnMedia-audio').hide();
  $('.btnMedia-video').hide();

  $('#inpType1').change(function(){
      var v = $(this).val();
      $('#inpType').val(v);

      if(v != '-'){
        var m = '';
        switch(v){
          case '1':
            m = 'video';
          break;
          case '2':
            m = 'audio';
          break;
        }
        $('.btnMedia-image').hide();
        $('.btnMedia-audio').hide();
        $('.btnMedia-video').hide();

        $('.btnMedia-'+m).show();
      }else{
        $('.btnMedia-audio').hide();
        $('.btnMedia-video').hide();
        $('.btnMedia-image').show();
      }
      
  });

  $('#inpIsDraft1').change(function(){
      var v = $(this).val();
      $('#inpIsDraft').val(v);
  });

  $('#inpTopic1').change(function(){
      var v = $(this).val();
      $('#inpTopic').val(v);
  });




  $('#btnSave').click(function(){
      
      var inpType = $('#inpType').val();
      var inpTopic = $('#inpTopic').val();
      var inpOrder = $('#inpOrder').val();
      var inpTitle = $('#inpTitle').val();
      $('#sub_editor_1').val(CKEDITOR.instances.editor_1.getData());
      $('#sub_editor_2').val(CKEDITOR.instances.editor_2.getData());


      var s = $('#frmOnlineTraining').serialize();
      var error = 0;
      var errmsg = '';

      if(inpType == '-'){
        errmsg += 'Please select type.\n\r';
        error++;
      }

      if(inpTopic == '-'){
        errmsg += 'Please select topic.\n\r';
        error++;
      }

      if(inpOrder == ''){
        errmsg += 'Please enter order number.\n\r';
        error++;
      }

      if(inpTitle == ''){
        errmsg += 'Please enter title.\n\r';
        error++;
      }

      if(error > 0){
        swal({
          title: "Error!",
          text: errmsg,
          textStyle: 'text-align: center',
          icon: "error",
          dangerMode : true
        });
      }else{
        $.ajax({
          type : 'post',
          url  : '<?php echo base_url(); ?>admin/onlinetrainings/save_training',
          data : s,
          beforeSend: function(){},
          success: function(res){
            console.log(res);
            swal({
              title: "Success!",
              text: res,
              textStyle: 'text-align: center',
              icon: "success",
              dangerMode : false
            });
          }
        });
      }

      console.log(s);  
  });
});

</script>

<script>


</script>

<style>
/*.cf:after { visibility: hidden; display: block; font-size: 0; content: " "; clear: both; height: 0; }
* html .cf { zoom: 1; }
*:first-child+html .cf { zoom: 1; }

html { margin: 0; padding: 0; }
body { font-size: 100%; margin: 0; padding: 1.75em; font-family: 'Helvetica Neue', Arial, sans-serif; }

h1 { font-size: 1.75em; margin: 0 0 0.6em 0; }

a { color: #2996cc; }
a:hover { text-decoration: none; }

p { line-height: 1.5em; }
.small { color: #666; font-size: 0.875em; }
.large { font-size: 1.25em; }*/

/**
 * Nestable
 */

.dd { position: relative; display: block; margin: 0; padding: 0; max-width: 600px; list-style: none; font-size: 13px; line-height: 20px; }

.dd-list { display: block; position: relative; margin: 0; padding: 0; list-style: none; }
.dd-list .dd-list { padding-left: 30px; }
.dd-collapsed .dd-list { display: none; }

.dd-item,
.dd-empty,
.dd-placeholder { display: block; position: relative; margin: 0; padding: 0; min-height: 20px; font-size: 13px; line-height: 20px; }

.dd-handle { display: block; height: 30px; margin: 5px 0; padding: 5px 10px; color: #333; text-decoration: none; font-weight: bold; border: 1px solid #ccc;
    background: #fafafa;
    background: -webkit-linear-gradient(top, #fafafa 0%, #eee 100%);
    background:    -moz-linear-gradient(top, #fafafa 0%, #eee 100%);
    background:         linear-gradient(top, #fafafa 0%, #eee 100%);
    -webkit-border-radius: 3px;
            border-radius: 3px;
    box-sizing: border-box; -moz-box-sizing: border-box;
}
.dd-handle:hover { color: #2ea8e5; background: #fff; }

.dd-item > button { display: block; position: relative; cursor: pointer; float: left; width: 25px; height: 20px; margin: 5px 0; padding: 0; text-indent: 100%; white-space: nowrap; overflow: hidden; border: 0; background: transparent; font-size: 12px; line-height: 1; text-align: center; font-weight: bold; }
.dd-item > button:before { content: '+'; display: block; position: absolute; width: 100%; text-align: center; text-indent: 0; }
.dd-item > button[data-action="collapse"]:before { content: '-'; }

.dd-placeholder,
.dd-empty { margin: 5px 0; padding: 0; min-height: 30px; background: #f2fbff; border: 1px dashed #b6bcbf; box-sizing: border-box; -moz-box-sizing: border-box; }
.dd-empty { border: 1px dashed #bbb; min-height: 100px; background-color: #e5e5e5;
    background-image: -webkit-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
                      -webkit-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
    background-image:    -moz-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
                         -moz-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
    background-image:         linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
                              linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
    background-size: 60px 60px;
    background-position: 0 0, 30px 30px;
}

.dd-dragel { position: absolute; pointer-events: none; z-index: 9999; }
.dd-dragel > .dd-item .dd-handle { margin-top: 0; }
.dd-dragel .dd-handle {
    -webkit-box-shadow: 2px 4px 6px 0 rgba(0,0,0,.1);
            box-shadow: 2px 4px 6px 0 rgba(0,0,0,.1);
}

/**
 * Nestable Extras
 */

.nestable-lists { display: block; clear: both; padding: 30px 0; width: 100%; border: 0; border-top: 2px solid #ddd; border-bottom: 2px solid #ddd; }

#nestable-menu { padding: 0; margin: 20px 0; }

#nestable-output,
#nestable2-output { width: 100%; height: 7em; font-size: 0.75em; line-height: 1.333333em; font-family: Consolas, monospace; padding: 5px; box-sizing: border-box; -moz-box-sizing: border-box; }

#nestable2 .dd-handle {
    color: #fff;
    border: 1px solid #999;
    background: #bbb;
    background: -webkit-linear-gradient(top, #bbb 0%, #999 100%);
    background:    -moz-linear-gradient(top, #bbb 0%, #999 100%);
    background:         linear-gradient(top, #bbb 0%, #999 100%);
}
#nestable2 .dd-handle:hover { background: #bbb; }
#nestable2 .dd-item > button:before { color: #fff; }

@media only screen and (min-width: 700px) {

    .dd { float: left; width: 48%; }
    .dd + .dd { margin-left: 2%; }

}

.dd-hover > .dd-handle { background: #2ea8e5 !important; }

/**
 * Nestable Draggable Handles
 */

.dd3-content { display: block; height: 30px; margin: 5px 0; padding: 5px 10px 5px 40px; color: #333; text-decoration: none; font-weight: bold; border: 1px solid #ccc;
    background: #fafafa;
    background: -webkit-linear-gradient(top, #fafafa 0%, #eee 100%);
    background:    -moz-linear-gradient(top, #fafafa 0%, #eee 100%);
    background:         linear-gradient(top, #fafafa 0%, #eee 100%);
    -webkit-border-radius: 3px;
            border-radius: 3px;
    box-sizing: border-box; -moz-box-sizing: border-box;
}
.dd3-content:hover { color: #2ea8e5; background: #fff; }

.dd-dragel > .dd3-item > .dd3-content { margin: 0; }

.dd3-item > button { margin-left: 30px; }

.dd3-handle { position: absolute; margin: 0; left: 0; top: 0; cursor: pointer; width: 30px; text-indent: 100%; white-space: nowrap; overflow: hidden;
    border: 1px solid #aaa;
    background: #ddd;
    background: -webkit-linear-gradient(top, #ddd 0%, #bbb 100%);
    background:    -moz-linear-gradient(top, #ddd 0%, #bbb 100%);
    background:         linear-gradient(top, #ddd 0%, #bbb 100%);
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
}
.dd3-handle:before { content: '≡'; display: block; position: absolute; left: 0; top: 3px; width: 100%; text-align: center; text-indent: 0; color: #fff; font-size: 20px; font-weight: normal; }
.dd3-handle:hover { background: #ddd; }


#divHead {
  background-color: #F6F6F6;
  width: 100%;
}

.stickme {
  position:sticky ;
  top :10px;
  position : -webkit-sticky;
  z-index: 10;
}
</style>
<script>
$(document).ready(function(){
    if ($(".js-switch")[0]) {
      var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
      elems.forEach(function (html) {
          var switchery = new Switchery(html, {
              color: '#26B99A'
          });
      });
    }


    

});
</script>
<div class="row hide-me">
  <div class="col-md-5">
    <div class="input-group">
    <input type="text" class="form-control">
    <span class="input-group-btn">
    <button id="ppm-browse-image" type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
    </span>
    </div>
  </div>
</div>
<?php if(isset($upload)) echo $upload; ?>