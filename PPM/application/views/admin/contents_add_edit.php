<style>
	hr.style-two {
	    border: 0;
	    height: 0;
	    border-top: 1px solid rgba(0, 0, 0, 0.1);
	    border-bottom: 1px solid rgba(255, 255, 255, 0.3);
	}

	.categories-selected.to_do li {
	    background: #1abb9c!important;
	    color: #fff!important;
	}	
</style>

<div class="container">

	<div class="col-md-12 col-sm-12 col-xs-12">
		<div id="copy-title"class="alert alert-success fade in" role="alert" style="display: none;">
			<strong></strong>
		</div>
		<p class="lead"><small class="text-info small">Compulsary fields are marked by an asterisk ( <i class="fa fa-asterisk text-danger small"></i> )</small></p>

		<form id="add-edit-form" class="form-horizontal form-label-left">
			<input id="data-id" type="hidden" name="id" value="" />
			<a id="preview-new-window-btn" href="javascript:;" target="_blank" class="hide">preview</a>
			<!-- start accordion -->
			<div class="accordion" id="accordion-add-edit" role="tablist" aria-multiselectable="true">

				<!-- General -->	
				<div class="panel">
					<a class="panel-heading" role="tab" id="contents-add-edit_1" data-toggle="collapse" data-parent="#accordion-add-edit" href="#add-edit_1" aria-expanded="true" aria-controls="add-edit_1">
						<h4 class="panel-title">General</h4>
					</a>
					<div id="add-edit_1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="contents-add-edit_1">
						<div class="panel-body">
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Draft:</label>
								<div class="col-md-5 col-sm-12 col-xs-12">
									
									<input id="data-draft" name="draft" type="checkbox" class="js-switch" checked />

									<button id="preview-btn" type="button" class="btn btn-sm btn-success pull-right">Preview</button>
									
								</div>
							</div>

							<div class="form-group" <?php if($tab === 'ppm-tv' OR $tab === 'promotions' OR $tab === 'rent-roll') echo 'style="display:none;"'; ?> >
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Web Location: <i class="fa fa-asterisk text-danger"></i></label>
								<div class="col-md-5 col-sm-12 col-xs-12">
									<?php

									echo form_multiselect('category1[]', $categories_arr[$tab], '', 'id="data-category1" multiple="multiple" class="select2_multiple form-control"');

									?>
								</div>
							</div>

						<?php if($tab === 'rent-roll') { ?>	
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Title: <i class="fa fa-asterisk text-danger"></i></label>
								<div class="col-md-5 col-sm-12 col-xs-12">
									<?php
										$title_arr = array(''=>'[ Select ]', 'WANTED'=>'WANTED','FOR SALE'=>'FOR SALE','SOLD'=>'SOLD');
										echo form_dropdown('title', $title_arr, '', 'id="data-name" class="form-control"');

									?>
								</div>
							</div>
						<?php } else { ?>
							<div id="title-container" class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Title: <i class="fa fa-asterisk text-danger"></i></label>
								<div class="col-md-5 col-sm-12 col-xs-12">
									<input id="data-name" name="title" type="text" class="form-control" placeholder="" value="" />
								</div>
							</div>
						<?php } ?>	

						<?php if($tab === 'news-articles') { ?>
							<div id="news-category" class="form-group" style="display:none;">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">News Category:</label>
								<div class="col-md-5 col-sm-12 col-xs-12">
								<?php

									echo form_dropdown('category', $categoryNewsArray, '', 'id="data-news-category" class="form-control"');

								?>
								</div>
							</div>
						<?php } ?>

							<div class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Date Posted: <i class="fa fa-asterisk text-danger"></i></label>
								<div class="col-md-5 col-sm-12 col-xs-12">
							        <div class="input-group date" id="mydate">
							            <input id="data-mydate" name="mydate" type='text' class="form-control" placeholder="dd/mm/yyyy" />
							            <span class="input-group-addon">
							            <span class="glyphicon glyphicon-calendar"></span>
							            </span>
							        </div>
								</div>
							</div>
						
						<?php if($tab === 'promotions') { ?>

							<div class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">End Date: <i class="fa fa-asterisk text-danger"></i></label>
								<div class="col-md-5 col-sm-12 col-xs-12">
							        <div class="input-group date" id="enddate">
							            <input id="data-enddate" name="enddate" type='text' class="form-control" placeholder="dd/mm/yyyy" />
							            <span class="input-group-addon">
							            <span class="glyphicon glyphicon-calendar"></span>
							            </span>
							        </div>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Associated Poll:</label>
								<div class="col-md-5 col-sm-12 col-xs-12">
									<p>Note: the period of the poll must be the same or longer than the period of the promotion.</p>
								<?php

									echo form_dropdown('associatedPoll', $associatedPoll, '', 'id="data-associatedPoll" class="form-control"');

								?>
								</div>
							</div>							

							<div class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Draw Date: <i class="fa fa-asterisk text-danger"></i></label>
								<div class="col-md-5 col-sm-12 col-xs-12">
							        <div class="input-group date" id="drawdate">
							            <input id="data-drawdate" name="drawdate" type='text' class="form-control" placeholder="dd/mm/yyyy" />
							            <span class="input-group-addon">
							            <span class="glyphicon glyphicon-calendar"></span>
							            </span>
							        </div>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Drawn By: </label>
								<div class="col-md-5 col-sm-12 col-xs-12">
									<input id="data-drawperson" name="drawperson" type="text" class="form-control" placeholder="Name" value="" />
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Click Here Text: </label>
								<div class="col-md-5 col-sm-12 col-xs-12">
									<p>Note: Replace CHANGE_ME with the full URL of the page you want the user to go to. For example: http://www.ppmsystem.com.au/members</p>
									<textarea id="data-clickherepromotiontext" name="clickherepromotiontext" type="text" class="form-control" placeholder="The text that will be displayed at the bottom of your promotion. It may be blank." rows="6"></textarea>
								</div>
							</div>

						<?php } ?>


						<?php if($tab === 'news-articles' OR $tab === 'rent-roll') { ?>
							<div class="form-group location-state" <?php if($tab === 'news-articles') echo 'style="display:none;"'; ?> >
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Location: </label>
								<div class="col-md-5 col-sm-12 col-xs-12">
									<input id="data-location" name="location" type="text" class="form-control" placeholder="" value="" />
								</div>
							</div>

							<div class="form-group location-state" <?php if($tab === 'news-articles') echo 'style="display:none;"'; ?> >
								<label class="control-label col-md-3 col-sm-12 col-xs-12">State: <i class="fa fa-asterisk text-danger"></i></label>
								<div class="col-md-2 col-sm-12 col-xs-12">
								<?php

									echo form_dropdown('state', $stateArray, '', 'id="data-state" class="form-control"');

								?>
								</div>
								<div>
									<label class="control-label col-md-1 col-sm-12 col-xs-12">Region:</label>
									<div class="col-md-2 col-sm-12 col-xs-12">
									<?php

										echo form_dropdown('stateSubRegionQLD', $stateSubRegionArray, '', 'id="data-stateSubRegionQLD" class="form-control" disabled');

									?>
									</div>
								</div>
							</div>	
						<?php } ?>
																				
						<?php if($tab !== 'rent-roll' && $tab !== 'promotions') { ?>
							<div id="topic-container" class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Topic: </label>
								<div class="col-md-5 col-sm-12 col-xs-12">
									<?php

									$count = (count($categories_arr['topics']) >= 6) ? 10 : '';

									echo form_multiselect('category2[]', $categories_arr['topics'], '', 'id="data-category2" multiple="multiple" class="select2_multiple form-control" size="' . $count . '"');

									?>
								</div>
							</div>

							<div id="source-container" class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Source/Author: </label>
								<div class="col-md-5 col-sm-12 col-xs-12">
									<input id="data-source" name="source" type="text" class="form-control" placeholder="" value="" />
								</div>
							</div>
						<?php } ?>

							<div class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Short Description: </label>
								<div class="col-md-5 col-sm-12 col-xs-12">
								<?php if($tab === 'rent-roll') { ?>
									<p>E.g: Interested in purchasing a rent roll of approx XXX properties in the XXX area</p>
								<?php } ?>
									<textarea id="data-shortdesc" name="shortdesc" class="form-control" rows="6"></textarea>
								</div>
							</div>
						<?php if($tab === 'ppm-tv') { ?>
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Youtube Link: </label>
								<div class="col-md-5 col-sm-12 col-xs-12">
									<input id="data-youtubelink" name="youtubelink" type="text" class="form-control" placeholder="" value="" />
								</div>
							</div>
						<?php } ?>	

						<?php if($tab === 'rent-roll') { ?>
							<div class="form-group" >
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Email Address: <i class="fa fa-asterisk text-danger"></i></label>
								<div class="col-md-5 col-sm-12 col-xs-12">
									<input id="data-email" name="email" type="email" class="form-control" placeholder="" value="" />
								</div>
							</div>

							<div class="form-group" >
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Contact Name: <i class="fa fa-asterisk text-danger"></i></label>
								<div class="col-md-5 col-sm-12 col-xs-12">
									<input id="data-contactName" name="contactName" type="text" class="form-control" placeholder="" value="" />
								</div>
							</div>

							<div class="form-group" >
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Company Name: <i class="fa fa-asterisk text-danger"></i></label>
								<div class="col-md-5 col-sm-12 col-xs-12">
									<input id="data-companyName" name="companyName" type="text" class="form-control" placeholder="" value="" />
								</div>
							</div>

							<div class="form-group" >
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Contact Number: <i class="fa fa-asterisk text-danger"></i></label>
								<div class="col-md-5 col-sm-12 col-xs-12">
									<input id="data-contactNumber" name="contactNumber" type="text" class="form-control" placeholder="" value="" />
								</div>
							</div>

						<?php } ?>	

						</div>	
					</div>
				</div>
				<!-- General -->

			<?php if($tab === 'news-articles' OR $tab === 'others') { ?>

				<!-- Related Products -->	
				<div class="panel related-items">
					<a class="panel-heading" role="tab" id="contents-add-edit_2" data-toggle="collapse" data-parent="#accordion-add-edit" href="#add-edit_2" aria-expanded="false" aria-controls="add-edit_2">
						<h4 class="panel-title">Related Products <small>(0)</small></h4>
					</a>
					<div id="add-edit_2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="contents-add-edit_2">
						<div class="panel-body">
							<div class="form-group">

								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

									<div class="checkbox" style="padding-bottom:5px;">
						                <label><span class="badge bg-default">Drag items to Selected Products:</span></label>
									</div>								

									<div style="max-height: 285px; overflow-y: scroll; border-radius: 5px;border: 1px solid #f3f3f3;">

										<ul id="products-available" class="to_do">
										<?php	
										$related_products_selected_arr = array();

										if(isset($related_products)) {
											foreach ($related_products as $key=>$value) {

												
										?>
											<li id="products-<?php echo $key; ?>" data-catid="<?php echo $key; ?>" style="margin:4px;" draggable="true">
												<p><?php echo $value; ?></p>
											</li>
										<?php
												

											}
										}

										?>

										</ul>

									</div>
			
								</div>

								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

									<div class="checkbox" style="padding-bottom:5px;">
										<label><span class="badge bg-green">Selected Products:</span></label>
									</div>
									<div style="max-height: 285px; overflow-y: auto; position:relative; border-radius: 5px;border: 1px solid #f3f3f3;">
										<h2 class="drop-here"><i>Drop here ...</i></h2>
										<ul id="products-selected" class="to_do categories-selected" style="min-height: 268px; position:relative; z-index:2;">
										</ul>
									</div>
									<div id="prod-selected" class="hide"></div>
			
								</div>								
							</div>
						</div>	
					</div>
				</div>
				<!-- Related Products -->			

				<!-- Related Downloads -->	
				<div class="panel related-items">
					<a class="panel-heading" role="tab" id="contents-add-edit_3" data-toggle="collapse" data-parent="#accordion-add-edit" href="#add-edit_3" aria-expanded="false" aria-controls="add-edit_3">
						<h4 class="panel-title">Related Downloads <small>(0)</small></h4>
					</a>
					<div id="add-edit_3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="contents-add-edit_3">
						<div class="panel-body">

							<div class="form-group">

								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

									<div class="checkbox" style="padding-bottom:5px;">
						                <label><span class="badge bg-default">Drag items to Selected Downloads:</span></label>
									</div>								

									<div style="max-height: 285px; overflow-y: scroll; border-radius: 5px;border: 1px solid #f3f3f3;">

										<ul id="downloads-available" class="to_do">
										<?php	
										$related_downloads_selected_arr = array();

										if(isset($related_downloads)) {
											foreach ($related_downloads as $key=>$value) {

												
										?>
											<li id="downloads-<?php echo $key; ?>" data-catid="<?php echo $key; ?>" style="margin:4px;" draggable="true">
												<p><?php echo $value; ?></p>
											</li>
										<?php
												

											}
										}

										?>

										</ul>

									</div>
			
								</div>

								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

									<div class="checkbox" style="padding-bottom:5px;">
										<label><span class="badge bg-green">Selected Downloads:</span></label>
									</div>
									<div style="max-height: 285px; overflow-y: auto; border-radius: 5px;border: 1px solid #f3f3f3;">
										<h2 class="drop-here"><i>Drop here ...</i></h2>
										<ul id="downloads-selected" class="to_do categories-selected" style="min-height: 268px; position:relative; z-index:2;">
										</ul>
									</div>
									<div id="down-selected" class="hide"></div>
			
								</div>								

							</div>						

						</div>
					</div>
				</div>
				<!-- Related Downloads -->	

			<?php } ?>

			<?php if($tab != 'ppm-tv' && $tab !== 'rent-roll') { ?>
				<!-- Body -->	
				<div id="body-container" class="panel">
					<a class="panel-heading" role="tab" id="contents-add-edit_4" data-toggle="collapse" data-parent="#accordion-add-edit" href="#add-edit_4" aria-expanded="false" aria-controls="add-edit_4">
						<h4 class="panel-title">Body</h4>
					</a>
					<div id="add-edit_4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="contents-add-edit_4">
						<div class="panel-body">

							<div class="form-group">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<?php if(isset($editor)) echo $editor; ?>
								</div>
							</div>

						</div>
					</div>
				</div>
				<!-- Body -->	

			<?php } ?>

			<?php if($tab !== 'rent-roll') { ?>
				<!-- Files -->	
				<div id="files-container" class="panel">
					<a class="panel-heading" role="tab" id="contents-add-edit_5" data-toggle="collapse" data-parent="#accordion-add-edit" href="#add-edit_5" aria-expanded="false" aria-controls="add-edit_5">
						<?php if($tab === 'promotions') { ?>
							<h4 class="panel-title">Winners | File Attachment</h4>
						<?php } else { ?>
							<h4 class="panel-title">File Attachments</h4>
						<?php } ?>
					</a>
					<div id="add-edit_5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="contents-add-edit_5">
						<div class="panel-body">

						<?php if($tab === 'promotions') { ?>

							<div class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12 text-left">Do not display "winner drawn" :</label>
								<div class="col-md-5 col-sm-12 col-xs-12">

									<input id="data-donotdisplaywinnerdrawn" name="donotdisplaywinnerdrawn" type="checkbox" class="js-switch" />
									
								</div>
								
							</div>

							<hr class="style-two"/>					

							<?php for ($i=1; $i <=5; $i++) { 
								$name = 'w'.$i;
							?>
								<div class="form-group" >
									<label class="control-label col-md-1 col-sm-12 col-xs-12">Winner <?php echo $i; ?>: </label>
									<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
										<input id="data-<?php echo $name; ?>" name="<?php echo $name; ?>" type="text" class="form-control" placeholder="Name" value="" />
									</div>
								</div>

							<?php } ?>

							<hr class="style-two"/>
						
						<?php } ?>

						

						<?php 
							$total = ($tab === 'promotions') ? 1 : 5;

							for ($i=1; $i <=$total; $i++) { 

						?>
							<div class="form-group">
								<label class="control-label col-md-1 col-sm-12 col-xs-12">File <?php echo $i; ?>: </label>
								<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
									<div class="input-group">
										<input id="data-d<?php echo $i; ?>" type="text" name="d<?php echo $i; ?>" readonly="true" class="form-control">
										<span class="input-group-btn">
										<button type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
										</span>
									</div>
								</div>
								<label class="control-label col-md-1 col-sm-12 col-xs-12">Caption: </label>
								<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
									<input id="data-c<?php echo $i; ?>" type="text" name="c<?php echo $i; ?>" class="form-control">
								</div>								
							</div>

						<?php } ?>

						</div>
					</div>
				</div>
				<!-- Files -->	

			<?php } else { //RENT ROLL?>

				<div class="panel">
					<a class="panel-heading" role="tab" id="contents-add-edit_5" data-toggle="collapse" data-parent="#accordion-add-edit" href="#add-edit_5" aria-expanded="false" aria-controls="add-edit_5">
						<h4 class="panel-title">Comments</h4>
					</a>
					<div id="add-edit_5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="contents-add-edit_5">
						<div class="panel-body">
							<div class="alert alert-danger fade in" role="alert">
								<strong>WARNING PRIVACY &amp; CONFIDENTIALITY POLICY FOR RENT ROLL CLASSIFIEDS</strong>
								<ol>
									<li>Details such as office name and contact numbers on a rent roll "for sale" are never to be given out.</li>
									<li>When communicating with an office that has listed a rent roll or business "for sale", never say
what the call is about. Always state that it is a personal call.</li>
									<li>Only email wanted details to the below listed address in the comments box</li>
									<li>Never fax information to an office unless the Principal has directed you to do so, 
and always confirm the fax number that they wish the information to be sent to.</li>
								</ol>
							</div>

							<p></p>

							<textarea id="data-comments" class="form-control" name="comments" rows="20" style="resize: none;"></textarea>

						</div>
					</div>
				</div>

			<?php } ?>

			</div>
	
		</form>

	</div>	

	<div class="row">
		<div class="panel">
			<div class="col-md-12 text-right">
				<button id="cancel-btn" type="button" class="btn btn-sm btn-warning" style="display: none;">Cancel Changes</button>
				<button id="update-btn" type="button" class="btn btn-sm btn-success" style="display: none;">Update</button>
				<button id="cancel-copy-btn" type="button" class="btn btn-sm btn-warning" style="display: none;">Cancel Copy</button>
				<button id="add-btn" type="button" class="btn btn-sm btn-success add-btn">Add</button>
			</div>
			<div class="clearfix"></div>
		</div>	
	</div>

</div>

<div class="row hide">
	<div class="col-md-5">
		<div class="input-group">
		<input type="text" class="form-control">
		<span class="input-group-btn">
		<button id="ppm-browse-image" type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
		</span>
		</div>
	</div>
</div>

<script>

	$(document).ready(function(){
		var body = <?php if(isset($editor_id)) { ?> $('#<?php echo $editor_id; ?>'); <?php } else { echo 'null';}?>;
		var form = $('#add-edit-form');
		var products;
		var downloads;
		var fields = {
						id: 				$('#data-id'),
						draft: 				$('#data-draft'),
						categories: 		$('#data-category1'),
						topics: 			$('#data-category2'),
						location: 			$('#data-location'),
						state: 				$('#data-state'),
						stateSubRegionQLD: 	$('#data-stateSubRegionQLD'),
						name: 				$('#data-name'),
						mydate: 			$('#data-mydate'),
						source: 			$('#data-source'),
						shortdesc: 			$('#data-shortdesc'),
						body: 				$('#data_body'),
						d1: 				$('#data-d1'),
						d2: 				$('#data-d2'),
						d3: 				$('#data-d3'),
						d4: 				$('#data-d4'),
						d5: 				$('#data-d5'),
						c1: 				$('#data-c1'),
						c2: 				$('#data-c2'),
						c3: 				$('#data-c3'),
						c4: 				$('#data-c4'),
						c5: 				$('#data-c5'),
						w1: 				$('#data-w1'),
						w2: 				$('#data-w2'),
						w3: 				$('#data-w3'),
						w4: 				$('#data-w4'),
						w5: 				$('#data-w5'),						
						category: 			$('#data-news-category'),
						youtubelink: 		$('#data-youtubelink'),
						comments: 			$('#data-comments'),
						email: 				$('#data-email'),
						contactName: 		$('#data-contactName'),
						companyName: 		$('#data-companyName'),
						contactNumber: 		$('#data-contactNumber'),
						enddate:			$('#data-enddate'),
						associatedPoll:		$('#data-associatedPoll'),	
						drawdate:			$('#data-drawdate'),
						drawperson:			$('#data-drawperson'),
						donotdisplaywinnerdrawn:$('#data-donotdisplaywinnerdrawn'),
						clickherepromotiontext: $('#data-clickherepromotiontext')
					};
		var locationState = $('.location-state');
		var newsCategory = $('#news-category');
		var containers = {
						title: 			$('#title-container'),
						topic: 			$('#topic-container'),
						body: 			$('#body-container'),
						files: 			$('#files-container'),
						relatedItems: 	$('.related-items'),
						source:			$('#source-container')
					};
		var previewBtn 			= $('#preview-btn');
		var previewNewWinBtn 	= $('#preview-new-window-btn');
		var copyTitle 			= $('#copy-title');

		var contents = {
			isCopy: false,
			init: function(){
				products = this.initDragDrop({holder:'products-available',selectedHolder:'products-selected',selected:'prod-selected',name:'relatedProducts',header:'contents-add-edit_2', type: 'products'});
				downloads = this.initDragDrop({holder:'downloads-available',selectedHolder:'downloads-selected',selected:'down-selected',name:'relatedDownloads',header:'contents-add-edit_3', type: 'downloads'});

			<?php if($tab === 'promotions') { ?>
				filter.setDate('mydate', 'enddate');
				$('#drawdate').datetimepicker({format: 'DD/MM/YYYY'});
			<?php } else { ?>
				$('#mydate').datetimepicker({format: 'DD/MM/YYYY'});
			<?php } ?>

				
				fields.categories.on('change', function(){
					<?php if($tab === 'others') { ?>
						contents.toggleContainer($(this).val());
					<?php } ?>	

					contents.checkLocation();
					contents.checkNewsCategory();
				});

				fields.state.on('change', function(){
					contents.checkState();
				});

				addBtn.bind('click', function(){
					contents.submitForm();
				});

				updateBtn.bind('click', function(){
					contents.submitForm();
				});

				contents.checkLocation();
				contents.checkNewsCategory();

				<?php if($tab === 'ppm-tv' OR $tab === 'promotions' OR $tab === 'rent-roll') { ?>
					fields.categories.find('option:first').attr('selected',true);
				<?php } ?>

				<?php if($tab === 'others') { ?>

					$.each(containers, function(k,v){
						$(v).find('.form-control').attr({disabled:true});
						$(v).hide();
					});

				<?php } ?>

				previewBtn.on('click', function(){
					contents.preview();
				});

				previewNewWinBtn.on('click', function(){ window.open(this.href); return false;});
			},			
			toggleContainer: function(val){
				<?php if($tab === 'others') { ?>
					
					$.each(containers, function(k,v){
						$(v).find('.form-control').attr({disabled:true});
						$(v).hide();
					});

					if(val!=null) {
						//["289", "890", "78", "87", "445", "410"]

						$.each(val, function(k,v){

							if(v != '410' && v != '87' && v != '289' && v != '445') {
								containers.title.show();
								containers.title.find('.form-control').attr({disabled:false});

								containers.topic.show();
								containers.topic.find('.form-control').attr({disabled:false});

								containers.body.show();
								containers.body.find('.form-control').attr({disabled:false});

								containers.relatedItems.show();
								containers.relatedItems.find('.form-control').attr({disabled:false});

								containers.files.hide();
								containers.files.find('.form-control').attr({disabled:true});

								containers.source.show();	
								containers.source.find('.form-control').attr({disabled:false});							
							}

							if(v == '87') {
								containers.title.show();
								containers.title.find('.form-control').attr({disabled:false});	
							}

							if(v == '289') {
								containers.source.show();
								containers.source.find('.form-control').attr({disabled:false});	
							}
							
							if(v == '410') {
								containers.title.show();
								containers.title.find('.form-control').attr({disabled:false});

								containers.body.show();
								containers.body.find('.form-control').attr({disabled:false});	
							}
							
							if(v == '445') {
								containers.files.show();
								containers.files.find('.form-control').attr({disabled:false});	
							}
							
							if(v == '890') {
								containers.files.show();
								containers.files.find('.form-control').attr({disabled:false});	
							}
						});															
					}
				
				<?php } ?>
			},
			initDragDrop: function(params) {

				var dragDrop = {
					holder: 		$('#' + params.holder),
					selectedHolder: $('#' + params.selectedHolder),
					selected: 		$('#' + params.selected),
					itemId: 		'',
					name: 			params.name,
					header: 		$('#' + params.header + ' small'),
					type: 			params.type,
					init: function(){
						this.holder.on('drop', function(e){dragDrop.drop(e,$(this));});
						this.selectedHolder.on('drop', function(e){dragDrop.drop(e,$(this));});
						this.holder.on('dragover', function(e){dragDrop.allowDrop(e);});
						this.selectedHolder.on('dragover', function(e){dragDrop.allowDrop(e);});
						this.holder.find('li').on('dragstart', function(e){dragDrop.drag(e);});
						this.selectedHolder.find('li').on('dragstart', function(e){dragDrop.drag(e);});
					},
					drop: function(ev, holder) {
						ev.preventDefault();
		    			
		    			if($(ev.target).parents('ul:first').length==0) {
		    				$('#'+this.itemId).appendTo($(ev.target));
		    			}
		    			else {
		    				var li = ($(ev.target)['context'].localName == 'li') ? $(ev.target) : $(ev.target).parents('li:first');
		    				$('#'+this.itemId).insertBefore(li);
		    			}

		    			this.updateList();
					},
					allowDrop: function(ev) {
					    ev.preventDefault();
					},
					drag:function(ev) {
						this.itemId = ev.target.id;
						ev.originalEvent.dataTransfer.setData("text/plain", ev.target.id);
						
					},					
					updateList: function(){
						this.selected.html('');
						var ctr = 0;
						$.each(this.selectedHolder.find('li'), function(k,v){
							var checkbox = $('<input type="checkbox" checked name="'+dragDrop.name+'[]" value="'+$(v).data().catid+'" />');
							checkbox.appendTo(dragDrop.selected);
							ctr++;
						});

						this.header.text('('+ctr+')');
					},
					resetList: function(){
						this.selected.html('');
						this.header.text('(0)');
						$.each(this.selectedHolder.find('li'), function(k,v){
							$(v).appendTo(dragDrop.holder);
						});
					},
					populateList: function(ids){

						$.each(ids, function(k,v){
							$('#'+dragDrop.type + '-' + v).appendTo(dragDrop.selectedHolder);
						});

						this.updateList();
					}
				}

				dragDrop.init();

				return dragDrop;
			},
			submitForm: function(){

				if(CKEDITOR.instances.<?php echo $editor_id; ?>!=undefined) {
					var bodyText = CKEDITOR.instances.<?php echo $editor_id; ?>.getData();

					body.text($.trim(bodyText.replace(/[\t\n]+/g, ' ')));
				}

				var isEdit = $.isNumeric(fields.id.val());

				swal({
				    title: (isEdit)?"Update Item" : "Add Item",
				    text: "Are you sure?",
				    icon: "warning",
				    buttons: ["Cancel", true],
				    dangerMode: false,
				})
				.then(willSubmit => {
				    if (willSubmit) {
						var request = $.ajax({
				          url: "<?php echo base_url(); ?>admin/contents/add_update",
				          method: "POST",
				          data: form.serialize(),
				          dataType: 'json'
				        });

				        request.done(function( res ) {
				            if(res!=null) {

				            	if(isEdit) {
				            		if(res.hasOwnProperty('error')) {
				            			var errMsg = '';

				            			$.each(res.error, function(k, v){
				            				errMsg += '* ' + v + '\n';
				            			});

				            			new PNotify({
											title: 'Invalid or Required!',
											text: errMsg,
											type: 'error',
											styling: 'bootstrap3'
			                            });

				            			swal({title:"Update Failed!",text:"Some fields are required (*) or have invalid values",icon:'error',button:false, timer:3000});
				            		}
				            		else {
				            			filter.applyUpdateChanges(res);

				            			swal({title:"Updated!",text:'Changes have been saved.',icon:'success',button:false, timer:3000});
				            		}
				            		
				            	}
				            	else {
				            		if(res.hasOwnProperty('error')) {
										var errMsg = '';

				            			$.each(res.error, function(k, v){
				            				errMsg += '* ' + v + '\n';
				            			});

				            			new PNotify({
											title: 'Invalid or Required!',
											text: errMsg,
											type: 'error',
											styling: 'bootstrap3'
			                            });

			                            swal({title:"Add Failed!",text:"Some fields are required (*) or have invalid values",icon:'error',button:false, timer:3000});
				            		}
				            		else {
				            			fields.id.val(res.id);
				            			copyTitle.hide();
				            			contents.showUpdateButton();
				            			addTab.html('<i class="fa fa-pencil"></i> Edit');
				            			
				            			swal({title:"Success!",text:"Added item with id: " + res.id,icon:'success',button:false, timer:3000});
				            			filter.addNewItem(res);
				            		}
				            	}
				            }
		        		});

				      	request.error(function(_data){
		        			if(_data.readyState == 4 && _data.status == 200) {//Session expired and redirected

		        				swal({
								    title: "Session Expired!",
								    text: "Reloading page.",
								    icon: "warning",
								    dangerMode: false,
								})
								.then(ok => {
								    if (ok) {
								    	window.location.href = '<?php echo base_url();?>admin/login';
								    }
								});
		        			}
		        		});
					}
				}); 

			},
			resetRelated: function(){
				products.resetList();
				downloads.resetList();
			},
			populateRelated: function(params){
				if(params.type == 'products') {
					products.populateList(params.ids);
				}
				else {
					downloads.populateList(params.ids);
				}
			},
			resetForm: function(){
				this.resetRelated();

				$.each(fields, function(k,v){
					if(v.length>0) {
						if(k == 'categories') {
							if(v.find('option').length<2) v.find('option:first').attr('selected',true);
							else v.val('');
							if(fields.topics!=null) fields.topics.val('');
							contents.checkLocation();
						}
						else if(k == 'state') {
							v.val('');
							contents.checkState();
						}
						else if(k == 'draft' || k == 'donotdisplaywinnerdrawn') {
							if(!v.prop('checked')) {
								v.click();
							}
						}
						else if(k == 'body') {
							CKEDITOR.instances.<?php echo $editor_id; ?>.setData('');
						}
						else {
							v.val('');
						}
					}
				});
			},
			populateForm: function(data){

				if(data!=null) {

					this.resetForm();

					setTimeout(function(){
						$.each(data, function(k,v){

							if(fields.hasOwnProperty(k)  && fields[k].length>0) {

								if(k == 'categories') {
									fields[k].val(v);
									fields.topics.val(v);
									contents.checkLocation();
								}
								else if(k == 'state') {
									fields[k].val(v);
									contents.checkState();
								}
								else if(k == 'draft' || k == 'donotdisplaywinnerdrawn') {
									if(fields[k].prop('checked') && v == 0) {
										fields[k].click();
									}
									else if(!fields[k].prop('checked') && v == 1) {
										fields[k].click();
									}
								}
								else if(k == 'body') {
									CKEDITOR.instances.<?php echo $editor_id; ?>.setData(v);
								}
								else {
									fields[k].val(v);
								}
							}
							else {
								if(k == 'relatedDownloads') {
									contents.populateRelated({type:'downloads',ids:v});
								}
								else if(k == 'relatedproducts') {
									contents.populateRelated({type:'products',ids:v});
								}
							}

						});

						if(contents.isCopy) {
							fields.id.val('');
							copyTitle.text('This is a copy of "' + fields.name.val() + '"');
							copyTitle.show();
							contents.showCopyCancelButton();
						}
						else {
							copyTitle.hide();
							contents.showUpdateButton();
						}

						if(addTab!=null) addTab.click();
					}, 50);
				}
			},
			checkLocation: function(){

				<?php if($tab === 'news-articles') { ?>

				if($.inArray( "4", fields.categories.val() ) >= 0) {
					locationState.find('.form-control').attr({disabled:false});
					locationState.show();
					contents.checkState();
				}
				else {
					locationState.find('.form-control').attr({disabled:true});
					locationState.hide();
				}

				<?php } ?>
			},
			checkState: function(){
				if($.inArray( "1", fields.state.val()) >= 0){
					fields.stateSubRegionQLD.attr({disabled:false});
				}
				else {
					fields.stateSubRegionQLD.val('');
					fields.stateSubRegionQLD.attr({disabled:true});
				}
			},
			checkNewsCategory: function(){
				if($.inArray( "4", fields.categories.val() ) >= 0 || $.inArray( "77", fields.categories.val() ) >= 0) {
					fields.category.attr({disabled:false});
					newsCategory.show();
				}
				else {
					fields.category.attr({disabled:true});
					newsCategory.hide();
				}
			},
			showUpdateButton: function(){
				cancelBtn.show();
				updateBtn.show();
				cancelCopyBtn.hide();
				addBtn.hide();	
			},
			showAddButton: function(){
				cancelBtn.hide();
				updateBtn.hide();
				cancelCopyBtn.hide();
				addBtn.show();	
			},
			showCopyCancelButton: function(){
				cancelBtn.hide();
				updateBtn.hide();				
				cancelCopyBtn.show();
				addBtn.show();				
			},
			cancelUpdate: function(){

				swal({
				    title: (contents.isCopy) ? "Cancel Copy": "Cancel Changes",
				    text: "Are you sure?",
				    icon: "warning",
				    buttons: ["Cancel", true],
				    dangerMode: false,
				})
				.then(willCancel => {
				    if (willCancel) {
				    	contents.resetTab();
				    }
				});
			},
			preview: function(){
				if(fields.id.val() != '') {
					previewNewWinBtn.attr({href:'<?php echo base_url() . 'articles/showarticle/' ;?>' + fields.id.val() });
					previewNewWinBtn.click();

				}
				else {
					swal({title:"Preview",text:"Please add entry before viewing.",icon:'error',button:false, timer:3000});
				}
			},
			resetTab: function(){
		    	contents.isCopy = false;
		    	addTab.html('<i class="fa fa-plus"></i> Add');
		    	contents.resetForm();
				contents.showAddButton();
				copyTitle.hide();
				searchTab.trigger('click');
			}
		};

		contents.init();

	    if ($(".js-switch")[0]) {
	        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
	        elems.forEach(function (html) {
	            var switchery = new Switchery(html, {
	                color: '#26B99A'
	            });
	        });
	    }

	    addEdit = contents;
	});
</script>