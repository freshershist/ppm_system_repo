<?php
	$exclude = TRUE;
?>
<!-- Category: Client type -->

<form id="members-form" class="form-horizontal form-label-left">
<div class="form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Client Type:</label>
	<div class="col-md-9 col-sm-9 col-xs-12">
		<?php 
			$client_type = array(
				'all'		=>'All',
				'6'			=>'General Client',
				'member'	=>'Member',
				'newsletter'=>'Newsletter Subscriber',
				'online'	=>'On-Line Training Subscriber',
				'cat-manual'=>'From Category',
				'other'		=>'Other Type'
			);

			echo $this->ppmsystemlib->createDropdown('clientType', $client_type, '', FALSE, FALSE);

		?>
		<p></p>
		<select id="client-type-list" name="category[]" class="select2_multiple form-control hide client-type" multiple="multiple" size="6">

		<?php 

			$client_type = array(
				'all'			=> array('label'=>'','all'=>'All'),
				'6'				=> array('label'=>'','6'=>'General Client'),
				'member'		=> array(
								'label'=>'Member',
								'ma'=>'All',
								'mc'=>'Current',
								'ms'=>'Suspended',
								'mCancel'=>'Cancelled',
								'0'=>'Platinum',
								'1'=>'Gold',
								'2'=>'Silver',
								'3'=>'Bronze'),
				'newsletter'	=> array(
								'label'=>'Newsletter Subscriber',
								'4'=>'All',
								'4a'=>'Current',
								'4b'=>'Expired',
								'4e'=>'Unsubscribed'),
				'online'	=> array(
								'label'=>'On-Line Training Subscriber',				
								'5'=>'All',
								'5a'=>'Current',
								'5b'=>'Expired',
								'5e'=>'Unsubscribed',
								'5f'=>'Double Ups',
								'dbcat824'=>'Trial'),
				'other'		=> array(
								'label'=>'Other Type',
								'8'=>'Head Office',
								'9'=>'RUM',
								'10'=>'PM Services',
								'11'=>'Tafes and Colleges',
								'12'=>'Contributor')
				);

			$id_collect = array();

			foreach ($client_type as $key => $value) {
				if(is_array($value)) {
					foreach ($value as $k => $v) {
						if($k==='label'){ 
							echo '<optgroup class="'.$key.'-label opt-group-label" label="'.$v.'">';
						}
						else{
							$id_collect[] = $k;
							if(!empty($v)) echo '<option value="' . $k .'">' . $v . '</option>';	
						}
					}
					echo '</optgroup>';
				}

			}

			if(isset($categories) && !empty($categories)) {
				echo '<optgroup class="cat-manual-label opt-group-label" label="From Category">';

				foreach ($categories as $value) {
					if(is_array($value)) {

						$opt_v = 'dbcat'.$value['id'];

						$id_exist = false;

						foreach ($value as $k => $v) {
							if($k === 'overrideDropdownValue' && $v != NULL) {
								if(!array_key_exists($v, $id_collect)){
									$opt_v = 'dbcat'.$v;
									break;
								}
								else {
									$id_exist = true;
									break;
								}
							}
						}
						
						if(!$id_exist) echo '<option value="' . $opt_v .'">' . $value['name'] . '</option>';	
					}
				}

				echo '</optgroup>';
			}
		?>
		</select>
		<select id="client-type-list-holder" class="select2_multiple form-control hide" multiple="multiple" size="6"></select>
	</div>
</div>
<!-- Category: Client type -->

<?php if(!$exclude) { ?>

<!-- Member Status -->

<div class="form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Member Status:</label>
	<div class="col-md-9 col-sm-9 col-xs-12">
		<select name="memberStatus" class="form-control">
			<option value="">None</option>
		<?php 

			if(isset($member_status)) {
				foreach ($member_status as $key => $value) {
					echo '<option value="' . (intval($key) + 1) .'">' . $value . '</option>';
				}
			}

		?>
		</select>
	</div>
</div>
<!-- Member Status -->

<?php } ?>

<!-- Interest Type -->

<div class="form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Interest Type:</label>
	<div class="col-md-9 col-sm-9 col-xs-12">
		<select name="interestType" class="form-control">
			<option value="">None</option>
		<?php 

			if(isset($interest_type)) {
				foreach ($interest_type as $key => $value) {
					echo '<option value="' . (intval($key) + 1) .'">' . $value . '</option>';
				}
			}

		?>
		</select>
	</div>
</div>
<!-- Interest Type -->

<?php if(!$exclude) { ?>
<!-- Exlude Users with -->

<div class="form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Exclude Users with:</label>
	<div class="col-md-9 col-sm-9 col-xs-12">
		<label>
			<input name="excludeSystemPresentation" type="checkbox" class="js-switch" /> System Presentation
		</label>
	</div>
</div>
<!-- Exlude Users with -->
<?php } ?>

<!-- Trust Account Program -->

<div class="form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Trust Account Program:</label>
	<div class="col-md-9 col-sm-9 col-xs-12">
		<select name="trustAccountProgram" class="form-control">
			<option value="">None</option>
		<?php 

			if(isset($trust_account_program)) {
				foreach ($trust_account_program as $key => $value) {
					echo '<option value="' . $key .'">' . $value . '</option>';
				}
			}

		?>
		</select>
	</div>
</div>
<!-- Trust Account Program -->

<!-- Member Category -->

<div class="form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Category:</label>
	<div class="col-md-9 col-sm-9 col-xs-12">
		<select name="memberCategory[]" class="select2_multiple form-control" multiple="multiple" size="6">
		<?php 

			if(isset($members_categories)) {
				foreach ($members_categories as $value) {
					echo '<option value="' . $value['id'] .'">' . $value['name'] . '</option>';
				}
			}

		?>
		</select>

		<?php if(!$exclude) { ?>
		<br>
		
		<label>
			<input name="reverseMatch" type="checkbox" class="js-switch" /> Show members who do not match the above categories
		</label>
		<?php } ?>
	</div>
</div>
<!-- Member Category -->

<!-- State -->

<div class="form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">State:</label>
	<div class="col-md-4 col-sm-4 col-xs-12">
		<select id="state" name="state[]" class="select2_multiple form-control" multiple="multiple" size="6">
		<?php 

			if(isset($state)) {
				foreach ($state as $value) {

					if(is_array($value)) {

						$opt_v = $value['id'];

						foreach ($value as $k => $v) {
							if($k === 'overrideDropdownValue' && !empty($v)) {
								$opt_v = $v;
								break;
							}
						}
						
						echo '<option value="' . $opt_v .'">' . $value['name'] . '</option>';	
					}
				}
			}

		?>
		</select>
	</div>
	<div class="col-md-1 col-sm-1 col-xs-12 state-subregion"><label class="control-label">Sub Region:</label></div>
	<div class="col-md-4 col-sm-4 col-xs-12 state-subregion">
		<select id="stateSubRegion" name="stateSubRegion[]" class="select2_multiple form-control" multiple="multiple" size="6">
		<?php 

			if(isset($state_subregion)) {
				foreach ($state_subregion as $key => $value) {
					echo '<option value="' . (intval($key) + 1) .'">' . $value . '</option>';
				}
			}

		?>
		</select>
	</div>	
</div>
<!-- State -->

<!-- Postcode -->

<div class="form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Postcode:</label>
	<div class="col-md-4 col-sm-4 col-xs-12">
		<input type="text" name="postcodeFrom" class="form-control" placeholder="" />
	</div>
	<div class="col-md-1 col-sm-1 col-xs-12"><label class="control-label">TO</label></div>
	<div class="col-md-4 col-sm-4 col-xs-12">
		<input type="text" name="postcodeTo" class="form-control" placeholder="" />
	</div>
</div>
<!-- Postcode -->

<?php if(!$exclude) { ?>

<!-- Expire Date -->

<div class="form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Expire Date:</label>
	<div class="col-md-4 col-sm-4 col-xs-12">
        <div class='input-group date' id='expire-from'>
            <input name="startDate" type='text' class="form-control" placeholder="dd/mm/yyyy" />
            <span class="input-group-addon">
            <span class="glyphicon glyphicon-calendar"></span>
            </span>
        </div>
	</div>
	<div class="col-md-1 col-sm-1 col-xs-12"><label class="control-label">TO</label></div>
	<div class="col-md-4 col-sm-4 col-xs-12">
        <div class='input-group date' id='expire-to'>
            <input name="endDate" type='text' class="form-control" placeholder="dd/mm/yyyy" />
            <span class="input-group-addon">
            <span class="glyphicon glyphicon-calendar"></span>
            </span>
        </div>
	</div>
</div>
<!-- Expire Date -->

<!-- Renewal / Join Date -->

<div class="form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Renewal / Join Date:</label>
	<div class="col-md-4 col-sm-4 col-xs-12">
        <div class='input-group date' id='renewal-join-from'>
            <input name="startDateR" type='text' class="form-control" placeholder="dd/mm/yyyy" />
            <span class="input-group-addon">
            <span class="glyphicon glyphicon-calendar"></span>
            </span>
        </div>
	</div>
	<div class="col-md-1 col-sm-1 col-xs-12"><label class="control-label">TO</label></div>
	<div class="col-md-4 col-sm-4 col-xs-12">
        <div class='input-group date' id='renewal-join-to'>
            <input name="endDateR" type='text' class="form-control" placeholder="dd/mm/yyyy" />
            <span class="input-group-addon">
            <span class="glyphicon glyphicon-calendar"></span>
            </span>
        </div>
	</div>
</div>
<!-- Renewal / Join Date -->

<!-- Unsubscribe Date -->

<div class="form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Unsubscribe Date:</label>
	<div class="col-md-4 col-sm-4 col-xs-12">
        <div class='input-group date' id='unsubscribe-from'>
            <input name="startDateU" type='text' class="form-control" placeholder="dd/mm/yyyy" />
            <span class="input-group-addon">
            <span class="glyphicon glyphicon-calendar"></span>
            </span>
        </div>
	</div>
	<div class="col-md-1 col-sm-1 col-xs-12"><label class="control-label">TO</label></div>
	<div class="col-md-4 col-sm-4 col-xs-12">
        <div class='input-group date' id='unsubscribe-to'>
            <input name="endDateU" type='text' class="form-control" placeholder="dd/mm/yyyy" />
            <span class="input-group-addon">
            <span class="glyphicon glyphicon-calendar"></span>
            </span>
        </div>
	</div>
</div>
<!-- Unsubscribe Date -->

<!-- Cancel Date -->

<div class="form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Cancel Date:</label>
	<div class="col-md-4 col-sm-4 col-xs-12">
        <div class='input-group date' id='cancel-from'>
            <input name="startDateC" type='text' class="form-control" placeholder="dd/mm/yyyy" />
            <span class="input-group-addon">
            <span class="glyphicon glyphicon-calendar"></span>
            </span>
        </div>
	</div>
	<div class="col-md-1 col-sm-1 col-xs-12"><label class="control-label">TO</label></div>
	<div class="col-md-4 col-sm-4 col-xs-12">
        <div class='input-group date' id='cancel-to'>
            <input name="endDateC" type='text' class="form-control" placeholder="dd/mm/yyyy" />
            <span class="input-group-addon">
            <span class="glyphicon glyphicon-calendar"></span>
            </span>
        </div>
	</div>
</div>
<!-- Cancel Date -->

<!-- System Type -->

<div class="form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">System Type:</label>
	<div class="col-md-9 col-sm-9 col-xs-12">
		<select name="systemType" class="form-control">
			<option value="">None</option>
		<?php 

			if(isset($system_type)) {
				foreach ($system_type as $key => $value) {
					echo '<option value="' . (intval($key) + 1) .'">' . $value . '</option>';
				}
			}

		?>
		</select>
	</div>
</div>
<!-- System Type -->

<?php } ?>

<!-- Outstanding Action Plans -->

<div class="form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Outstanding Action Plans:</label>
	<div class="col-md-9 col-sm-9 col-xs-12">
		<select name="outstandingActionPlans" class="form-control">
		<?php

			$arr = array(
				'0'=>'None',
				'1'=>'Action Plan 1: Pre-system Requirements',
				'2'=>'Action Plan 2: Principal Discussions',
				'3'=>'Action Plan 3: System Set Up',
				'4'=>'Action Plan 4: Customerisation & Training',
				'5'=>'Action Plan 5: Department Audit',
				'6'=>'Action Plan 6: Human Resource Management',
				'7'=>'Action Plan 7: Marketing & Customer Service',
				'8'=>'Action Plan 8: KPI Report & Bottom-Line Report',
				'99'=>'Any Action Plan'
			);

			foreach ($arr as $key => $value) {
				echo '<option value="' . $key .'">' . $value . '</option>';
			}

		?>
		</select>
	</div>
</div>
<!-- Outstanding Action Plans -->

<?php if(!$exclude) { ?>

<!-- PPMsystem Powered by REI Master Modules -->

<div class="form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">PPMsystem Powered by REI Master Modules:</label>
	<div class="col-md-9 col-sm-9 col-xs-12">
		<select name="reiMasterModules" class="form-control">
			<option value="">None</option>
		<?php 

			if(isset($rei_master_modules)) {
				foreach ($rei_master_modules as $value) {
					echo '<option value="' . $value['id'] .'">' . $value['name'] . '</option>';
				}
			}

		?>
		</select>
	</div>
</div>
<!-- PPMsystem Powered by REI Master Modules -->

<!-- Keywords -->

<div class="form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Search Keywords:</label>
	<div class="col-md-9 col-sm-9 col-xs-12">
		<input name="keywords" type="text" class="form-control" placeholder="" />
	</div>
</div>
<!-- Keywords -->

<!-- Partners -->
	<!-- Code here -->
<!-- Partners -->

<!-- Forum User -->

<div class="form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Forum User:</label>
	<div class="col-md-9 col-sm-9 col-xs-12">
		<select name="forum" class="form-control">
		<?php

			$arr = array(
				'0'=>'None',
				'1'=>'Manager',
				'2'=>'Super Star'
			);

			foreach ($arr as $key => $value) {
				echo '<option value="' . $key .'">' . $value . '</option>';
			}

		?>
		</select>
	</div>
</div>

<?php } ?>

<div class="form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">&nbsp;</label>
	<div class="col-md-9 col-sm-9 col-xs-12">
		<button id="member-search-btn" type="button" class="btn btn-success">Run</button>
	</div>
</div>
<!-- Forum User -->

</form>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/numerals.js/numeral.min.js"></script>

<script type="text/javascript">

$(document).ready(function(){
	var masterList 					= null;
	var membersForm 				= $('#members-form');
	var listTable 					= $('#members-table');
	var listHolder 					= $('#row-holder');
	var listRowCopy 				= $('#row-holder .row-copy');
	var filtersSection 				= $('#headingOne');
	var resultsSection 				= $('#headingTwo');
	var totalLabel 					= $('#total-label p');
	var clientType 					= $('#clientType');
	var clientTypeList 				= $('#client-type-list');
	var clientTypeListHolder 		= $('#client-type-list-holder');
	var membershipTypeDictionary 	= <?php if(isset($membershipTypeDictionary)){echo json_encode($membershipTypeDictionary);}else{echo 'null';}?>;
	var stateDictionaryMember 		= <?php if(isset($stateDictionaryMember)){echo json_encode($stateDictionaryMember);}else{echo 'null';}?>;
	var membershipPayments 			= <?php if(isset($membershipPayments)){echo json_encode($membershipPayments);}else{echo 'null';}?>;
	var billingCycleArr 			= <?php if(isset($billingCycle)){echo json_encode($billingCycle);}else{echo 'null';}?>;
	var resultsLabel 				= $('#headingTwo .panel-title small');
	var tempListHolder 				= $('<div></div>');
	var currentDataTable 			= null;
	var currentTable 				= null;
	var tableId 					= '';
	var loader 						= $('#loader');
	var percentage 					= 0;
	var progressCtr 				= 0;
	var total 						= 0;
	var tempListStr 				= '';

	var members = {
		tempFunc: null,
		init: function(){
			$('#state').on('change', function(){members.checkState();});

			members.setDate('expire-from', 'expire-to');
			members.setDate('renewal-join-from', 'renewal-join-to');
			members.setDate('unsubscribe-from', 'unsubscribe-to');
			members.setDate('cancel-from', 'cancel-to');

			members.checkState();

			$('#member-search-btn').on('click', function(){members.getList()});

			resultsSection.on('click',  function(){members.scrollTop()});

			funcFilterHolder.applyUpdateChanges = this.applyUpdateChanges;
			funcFilterHolder.generateID = this.generateID;

			members.initFilters();

		},
		initFilters: function (){

			clientTypeList.removeClass('hide').hide();

			clientType.on('change', function(){
				var val = $(this).val();
				
				if(val == 'all' || val == '6' || val == '') {
					clientTypeList.val(val);
					clientTypeList.hide();
				}
				else {
					clientTypeList.val([]);
					clientTypeList.trigger('change');
					clientTypeList.show();

					clientTypeList.find('optgroup.opt-group-label').appendTo(clientTypeListHolder);
					clientTypeListHolder.find('optgroup.'+val+'-label').appendTo(clientTypeList);
					
					clientTypeList.val([clientTypeList.find('optgroup.'+val+'-label option:first').val()]);
				}
			});
		},
		setDate: function(fromId, toId) {
	    	$('#' + fromId).datetimepicker({
				format: 'DD/MM/YYYY'
			});
	    
		    $('#' + toId).datetimepicker({
		        useCurrent: false,
		        format: 'DD/MM/YYYY'
		    });
		    
		    $("#" + fromId).on("dp.change", function(e) {
		        $('#' + toId).data("DateTimePicker").minDate(e.date);
		    });
		    
		    $("#" + toId).on("dp.change", function(e) {
		        $('#' + fromId).data("DateTimePicker").maxDate(e.date);
		    });
	    },
	    checkState: function() {
			if ($('#state').val() == '1') {
				$('#members-form .state-subregion').show();
			} else {
				$('#members-form .state-subregion').hide();
			}
		},
		getList: function(){

			if(members.validateForm()) {

				members.showList();
				members.scrollTop();
				totalLabel.removeClass('hide');
				totalLabel.html('Fetching Records... <i class="fa fa-spinner fa-spin"></i>');
				resultsLabel.text('');

				if(tableId!='') {
					$('#' + tableId + '_wrapper').remove();
					currentTable = null;
					currentDataTable = null;
				}

				var request = $.ajax({
		          url: "<?php echo base_url(); ?>admin/members/get_list",
		          method: "POST",
		          data: membersForm.serialize(),
		          dataType: 'json'
		        });
		         
		        request.done(function( data ) {
		        	//console.log(data);

		            if(data!=null) {
		            	members.refreshList();
		            	//members.updateResultsCount();

		            	masterList = data;//data.result;
		            	total = masterList.length;

		            	if(total>0) {
		            		//loader.removeClass('hide');
	    					totalLabel.removeClass('hide');

							listHolder.html('');
							tempListHolder.html('');

							loader.addClass('hide');
							totalLabel.html('Updating list with ' + numeral(total).format('0,0') + ' records. <i class="fa fa-spinner fa-spin"></i>');

		            		members.processList();
		            	}
		            	else {
		            		members.updateLoader(false);
		            		currentDataTable = currentTable.DataTable();
		            	}
		            	
		            	resultsLabel.text(clientType.find('option:selected').text() + ' - ' + clientTypeList.find('option:selected').text());

						data = null;
		            }
        		});

        		request.error(function(_data){
        			if(_data.readyState == 4 && _data.status == 200) {//Session expired and redirected
        				swal({
						    title: "Session Expired!",
						    text: "Reloading page.",
						    icon: "warning",
						    dangerMode: false,
						})
						.then(willDelete => {
						    if (willDelete) {
						    	window.location.href = '<?php echo base_url();?>admin/login';
						    }
						});
        			}
        		});
	    	}
		},
		processList: function(){

            setTimeout(function(){

				currentDataTable = currentTable.DataTable( {
					data: masterList,
				    columns: [
				        { data: 'id' },
				        { data: 'memberStatus' },
				        { data: 'company' },
				        { data: 'contactName' },
				        { data: 'membershipType' },
				        { data: 'state' },
				        { data: 'membershipPayments' },
				        { data: 'membershipInvestment' },
				        { data: 'billingCycle' },
				        { data: 'memberShipCommencementDate' },
				        { data: 'membershipExpires' },
				        { data: 'nextAuditDue' },
				        { data: 'consultingHoursRemaining' },
				        { data: 'action' }
				    ],
				    "scrollY": 480,
        			"scrollX": true
				} );

				currentTable.children('tbody').on('click', 'td.col-action', function () {
					members.showActionPanel($(this));
				});

				members.initListAction();

				members.updateLoader(false);

				currentDataTable.on( 'draw.dt', function () {
				    members.initListAction();
				});

				//members.updateResultsCount();

            }, 1);

		},
		applyUpdateChanges: function(data){

			if(data!=null) {

				var row = $('#row-'+data.id);

				row.children('td')[1].innerHTML = data.memberStatus;
				row.children('td')[2].innerHTML = data.company;
				row.children('td')[3].innerHTML = data.contactName;
				row.children('td')[4].innerHTML = data.membershipType;
				row.children('td')[5].innerHTML = data.state;
				row.children('td')[7].innerHTML = data.membershipInvestment;
				row.children('td')[9].innerHTML = data.membershipCommencementDate;
				row.children('td')[10].innerHTML = data.membershipExpires;
				row.children('td')[11].innerHTML = data.nextAuditDue;
				row.children('td')[12].innerHTML = data.consultingHoursRemaining;
			}
		},
		initListAction: function(){
			$.each($(currentTable).find('tr'), function(k,v){
		    	var id = $(v).children('td:first').text();

		    	if($.isNumeric(id)) {
		    		var td = $(v).children('td:nth-last-child(1)');
		    		td.addClass('col-action').attr({'data-id':id});
		    	}
		    });
		},
		refreshList: function(){
			if(tableId!='') {
				$('#' + tableId + '_wrapper').remove();
				currentTable = null;
				currentDataTable = null;
			}

			tableId = members.generateID();

			var tempTable = listTable.clone().removeClass('hide').attr({id:tableId});

			tempTable.insertAfter(listTable);

			currentTable = $('#' + tableId);

			tempTable = null;
		},
		updateResultsCount: function(){
			var str = '';

			if($('tr.row-item').length>0){

				str = '('+ numeral($('tr.row-item').length).format('0,0')+' records found)';
			}
			else {
				str = '';
			}

			resultsLabel.text(str);
		},
		addItem: function(data){

			var row = listRowCopy.clone().removeClass('row-copy hide').addClass('row-item');
			
			row.attr({'id':'row-item-'+data.id, 'data-rowid':data.id});

			var id 				= row.children('td.col-id');
			var clientStatus 	= row.children('td.col-client-status');
			var companyName 	= row.children('td.col-company-name');
			var contactName 	= row.children('td.col-contact-name');
			var clientType 		= row.children('td.col-client-type');
			var state 			= row.children('td.col-state');
			var payments 		= row.children('td.col-payments');
			var invest 			= row.children('td.col-invest');
			var billingCycle 	= row.children('td.col-billing-cycle');
			var commencement 	= row.children('td.col-commencement');
			var expires 		= row.children('td.col-expires');
			var nextAudit 		= row.children('td.col-next-audit');
			var consulHrsRemain = row.children('td.col-consul-hrs-remain');
			var action 			= row.children('td.col-action');

			action.attr({'data-id':data.id});

			id.text(data.id);

			if($.isNumeric(data.memberStatus)) {
				var status = parseInt(data.memberStatus);
				var statusText = '';
				var membershipType = parseInt(data.membershipType);

				if(status == 1) {
					if(membershipType == 4 || membershipType == 5) {
						statusText = 'Current';
					}
					else {
						statusText = 'Active';
					}
				}
				else if(status == 2){
					statusText = 'Suspended';
				}
				else if(status == 3){
					if(membershipType == 4 || membershipType == 5) {
						statusText = 'Unsubscribed';
					}
					else {
						statusText = 'Cancelled';
					}
				}

				clientStatus.text(statusText);
			}

			companyName.text(data.company);
			contactName.text(data.contactName);
			clientType.text(membershipTypeDictionary[data.membershipType]);
			state.text(stateDictionaryMember[data.state]);

			if($.isNumeric(data.membershipPayments)) {
				payments.text(membershipPayments[parseInt(data.membershipPayments) - 1]);
			}

			if($.isNumeric(data.membershipInvestment)) {
				invest.text(parseFloat(data.membershipInvestment));
			}

			if($.isNumeric(data.billingCycle)) {
				billingCycle.text(billingCycleArr[parseInt(data.billingCycle) - 1]);
			}

			if(moment(data.memberShipCommencementDate).isValid()){
				commencement.text(members.checkDateTime(data.memberShipCommencementDate));
			}

			if(moment(data.membershipExpires).isValid()) {
				expires.text(members.checkDateTime(data.membershipExpires));
			}

			if(moment(data.nextAuditDue).isValid()){
				nextAudit.text(members.checkDateTime(data.nextAuditDue));
			}

			if($.isNumeric(data.consultingHoursRemaining)) {
				consulHrsRemain.text(parseFloat(data.consultingHoursRemaining));
			}

			row.appendTo(tempListHolder);

			row = id = clientStatus = companyName = contactName = clientType = state = payments = invest = billingCycle = commencement = expires = nextAudit = consulHrsRemain = action = null;

		},
		checkDateTime: function(dateTimeStr){
			var dateTime = dateTimeStr.split(' ');

			if(dateTime.length > 1) {

				if(dateTime[1]!='00:00:00') {
					return moment(dateTimeStr).format('D/MM/YYYY h:mm:ss A');
				}
				else {
					return moment(dateTimeStr).format('D/MM/YYYY');
				}
			}

			return moment(dateTimeStr).format('D/MM/YYYY');
		},
		showActionPanel: function(td){
			var col 		= $(td);
			var id 			= col.data().id;
			var addTask 	= col.find('ul li ul li a.add-task');
			var edit 		= col.find('ul li ul li a.edit');
			var copy 		= col.find('ul li ul li a.copy');
			var copyOLTrain = col.find('ul li ul li a.copy-ol-train');
			var deleteLnk 	= col.find('ul li ul li a.delete');
			var downloadLog = col.find('ul li ul li a.download-log');
			var emailLog 	= col.find('ul li ul li a.email-log');
			var loginLog 	= col.find('ul li ul li a.login-log');

			col.find('ul li ul li a label.action-id').text(id);

			addTask.unbind('click').bind('click', function(){members.requestByAction({'id':id, 'action':$(this).attr('class')})});
			edit.unbind('click').bind('click', function(){members.requestByAction({'id':id, 'action':$(this).attr('class')})});
			copy.unbind('click').bind('click', function(){members.requestByAction({'id':id, 'action':$(this).attr('class')})});
			copyOLTrain.unbind('click').bind('click', function(){members.requestByAction({'id':id, 'action':$(this).attr('class')})});
			deleteLnk.unbind('click').bind('click', function(){members.requestByAction({'id':id, 'action':$(this).attr('class')})});
			downloadLog.unbind('click').bind('click', function(){members.requestByAction({'id':id, 'action':$(this).attr('class')})});
			emailLog.unbind('click').bind('click', function(){members.requestByAction({'id':id, 'action':$(this).attr('class')})});
			loginLog.unbind('click').bind('click', function(){members.requestByAction({'id':id, 'action':$(this).attr('class')})});

			col.parents('tr').attr({id:'row-'+id})

			addTask = edit = copy = copyOLTrain = deleteLnk = downloadLog = emailLog = loginLog = null;
		},
		requestByAction: function(data){

			if(data!=null) {

		    	members.tempFunc = function(data) {
		    		var request = $.ajax({
			          url: "<?php echo base_url(); ?>admin/members/action",
			          method: "POST",
			          data: data
			        });

			        request.done(function( _data ) {
			            if(_data!=null) {
			            	if(membersAddEdit!=null) membersAddEdit = null;
			            	
			            	membersAddEditHolder.html(_data);

			            	if(data.action == 'copy' || data.action == 'copy-ol-train') {
			            		swal({title:"Success!",text:"Member Copied.",icon:'success',button:false, timer:3000});
			            	}
			            	else if(data.action == 'delete') {
			            		currentDataTable.row('#row-'+data.id).remove().draw( false );
			            		searchTab.trigger('click');
				            	membersAddEditHolder.html(_data);
				            	addTab.html('<i class="fa fa-plus"></i> Add');
				            	swal({title:"Success!",text:"Deleted member with ID: " + data.id,icon:'success',button:false, timer:3000});
			            	}

			            	if(data.action != 'delete') {
			            		addTab.trigger('click');
			            	}
			            	
			            	_data = null;
			            }
	        		});

	        		request.error(function(_data){
	        			if(_data.readyState == 4 && _data.status == 200) {//Session expired and redirected
	        				swal({
							    title: "Session Expired!",
							    text: "Reloading page.",
							    icon: "warning",
							    dangerMode: false,
							})
							.then(willDelete => {
							    if (willDelete) {
							    	window.location.href = '<?php echo base_url();?>admin/login';
							    }
							});
	        			}
	        		});
		    	}

				var showAddEditTab = false;

            	if(data.action == 'edit') {
					showAddEditTab = true;
					addTab.html('<i class="fa fa-pencil"></i> ' + data.action.charAt(0).toUpperCase() + data.action.substr(1));
            	}
            	else if(data.action == 'copy' || data.action == 'copy-ol-train') {
            		swal({
						title: (data.action == 'copy') ? "Copy Member" : "Copy Online Training",
						text: "Are you sure?",
						icon: "warning",
						buttons: ["Cancel", true],
						dangerMode: false,
						})
						.then(willDelete => {
						
							if(willDelete) {
								addTab.html('<i class="fa fa-pencil"></i> ' + data.action.charAt(0).toUpperCase() + data.action.substr(1));
								members.tempFunc.call(this, data);
							}
						
						}); 
            	}
            	else if(data.action == 'delete') {
            		swal({
						title: "Delete Member",
						text: "Are you sure?",
						icon: "warning",
						buttons: ["Cancel", true],
						dangerMode: true,
						})
						.then(willDelete => {
						
							if(willDelete) {
								members.tempFunc.call(this, data);
							}
						
						}); 
            	}
            	else if(data.action == 'download-log' || data.action == 'email-log' || data.action == 'login-log') {
            		if(funcAddEditHolder!=null) funcAddEditHolder.call(this, data);
            	}

            	if(showAddEditTab) {
            		if(members.tempFunc!=null) members.tempFunc.call(this, data);
		    	}
	    	}
		},
		updateLoader: function(show){
			if(show) {
				var percentage = progressCtr / total;

				if(progressCtr % 20 == 0) loader.css({width: (percentage * 100) + '%'});

		        progressCtr++;

		        totalLabel.text('Fetching ' + numeral(progressCtr).format('0,0') + ' of ' + numeral(total).format('0,0') + ' records');
	    	}
	    	else {
	    		progressCtr = total = 0;

	    		loader.css({width: '0%'}).addClass('hide');
	    		totalLabel.text('').addClass('hide');
	    	}
		},
		scrollTop: function(){
			$('html').scrollTop(0);
		},
		showList: function(){
			resultsSection.trigger('click');
		},
		validateForm: function(){
			var isValid = false;
			var category = $('#category');

			if(category.val() == null) {
				category.val(['all']);
			}

			return true;
		},
		generateID: function() {
	      var charSet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
	      var charCount = 20;
	      var charSetSize = charSet.length;
	      var id = '';

	      for (var i = 1; i <= charCount; i++) {
	          var randPos = Math.floor(Math.random() * charSetSize);
	          id += charSet[randPos];
	      }

	      return id;      
	    }

	};

	members.init();

	setTimeout(function(){$('#headingOne').trigger('click');},10);

});

</script>
