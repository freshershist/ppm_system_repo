<style>
	hr.style-two {
	    border: 0;
	    height: 0;
	    border-top: 1px solid rgba(0, 0, 0, 0.1);
	    border-bottom: 1px solid rgba(255, 255, 255, 0.3);
	}	
</style>

<div class="container">

	<div class="col-md-12 col-sm-12 col-xs-12">
		<div id="copy-title"class="alert alert-success fade in" role="alert" style="display: none;">
			<strong></strong>
		</div>
		<p class="lead"><small class="text-info small">Compulsary fields are marked by an asterisk ( <i class="fa fa-asterisk text-danger small"></i> )</small></p>

		<form id="add-edit-form" class="form-horizontal form-label-left">
			<input id="data-id" type="hidden" name="id" value="" />
			
			<!-- start accordion -->
			<div class="accordion" id="accordion-add-edit" role="tablist" aria-multiselectable="true">

				<!-- General -->	
				<div class="panel">
					<a class="panel-heading" role="tab" id="contents-add-edit_1" data-toggle="collapse" data-parent="#accordion-add-edit" href="#add-edit_1" aria-expanded="true" aria-controls="add-edit_1">
						<h4 class="panel-title">General</h4>
					</a>
					<div id="add-edit_1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="contents-add-edit_1">
						<div class="panel-body">

							<div class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Date Posted: <i class="fa fa-asterisk text-danger"></i></label>
								<div class="col-md-5 col-sm-12 col-xs-12">
							        <div class="input-group date" id="datePosted">
							            <input id="data-datePosted" name="datePosted" type='text' class="form-control" placeholder="dd-mmm-yyyy" />
							            <span class="input-group-addon">
							            <span class="glyphicon glyphicon-calendar"></span>
							            </span>
							        </div>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Section: <i class="fa fa-asterisk text-danger"></i></label>
								<div class="col-md-5 col-sm-12 col-xs-12">
								<?php

									echo form_dropdown('mysection', $categoryArray, '', 'id="data-mysection" class="form-control"');

								?>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Download Name: <i class="fa fa-asterisk text-danger"></i></label>
								<div class="col-md-5 col-sm-12 col-xs-12">
									<input id="data-name" name="name" type="text" class="form-control" placeholder="" value="" />
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Download Type: </label>
								<div class="col-md-5 col-sm-12 col-xs-12">
								<?php
									echo form_dropdown('documenttype', $downloadTypeArray, '', 'id="data-documenttype" class="form-control"');

								?>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Download Location: </label>
								<div class="col-md-5 col-sm-12 col-xs-12">
									<input id="data-documentlocation" name="documentlocation" type="text" class="form-control" placeholder="" value="" maxlength="250" />
								</div>
							</div>							

							<div class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Code: </label>
								<div class="col-md-5 col-sm-12 col-xs-12">
									<input id="data-code" name="code" type="text" class="form-control" placeholder="" value="" maxlength="250" />
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Compatibility: </label>
								<div class="col-md-5 col-sm-12 col-xs-12">
									<input id="data-compatibility" name="compatibility" type="text" class="form-control" placeholder="" value="" maxlength="250" />
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">State: </label>
								<div class="col-md-5 col-sm-12 col-xs-12">
									<?php
									$category_count = (count($state)>=6) ? 6 : count($state);

									echo form_multiselect('state[]', $state, '', 'id="data-state" multiple="multiple" class="select2_multiple form-control" size="' . $category_count . '" ');

									?>
								</div>
							</div>							

							<div class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Access Privileges: <i class="fa fa-asterisk text-danger"></i></label>
								<div class="col-md-5 col-sm-12 col-xs-12">
									<input id="access" type="hidden" name="access" value=""/>
									<label><input id="data-platnium" name="platnium" type="checkbox" class="flat access" /> Platinum</label>
									<label>&nbsp;</label>
									<label><input id="data-gold" name="gold" type="checkbox" class="flat access" /> Gold</label>
									<label>&nbsp;</label>
									<label><input id="data-silver" name="silver" type="checkbox" class="flat access" /> Silver</label>
									<label>&nbsp;</label>
									<label><input id="data-bronze" name="bronze" type="checkbox" class="flat access" /> Bronze</label>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Short Description: </label>
								<div class="col-md-5 col-sm-12 col-xs-12">
									<textarea id="data-shortdesc" name="shortdesc" class="form-control" rows="6"></textarea>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Description: </label>
								<div class="col-md-5 col-sm-12 col-xs-12">
									<textarea id="data-description" name="description" class="form-control" rows="6"></textarea>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Attachments: </label>
							</div>

						<?php 

							for ($i=1; $i <=3; $i++) { 

						?>
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">File <?php echo $i; ?>: </label>
								<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
									<div class="input-group">
										<input id="data-d<?php echo $i; ?>" type="text" name="d<?php echo $i; ?>" readonly="true" class="form-control">
										<span class="input-group-btn">
										<button type="button" class="ppm-browse btn btn-primary" data-type="application">Browse | <i class="fa fa-file-text"></i></button>
										</span>
									</div>
								</div>							
							</div>

						<?php } ?>							

						</div>	
					</div>
				</div>
				<!-- General -->

			</div>
	
		</form>

	</div>	

	<div class="row">
		<div class="panel">
			<div class="col-md-12 text-right">
				<button id="cancel-btn" type="button" class="btn btn-sm btn-warning" style="display: none;">Cancel Changes</button>
				<button id="update-btn" type="button" class="btn btn-sm btn-success" style="display: none;">Update</button>
				<button id="add-btn" type="button" class="btn btn-sm btn-success add-btn">Add</button>
			</div>
			<div class="clearfix"></div>
		</div>	
	</div>

</div>

<div class="row hide">
	<div class="col-md-5">
		<div class="input-group">
		<input type="text" class="form-control">
		<span class="input-group-btn">
		<button id="ppm-browse-image" type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
		</span>
		</div>
	</div>
</div>

<script>
	$(document).ready(function(){
		var form = $('#add-edit-form');
		var fields = {
						bronze: 			$('#data-bronze'),
						code: 				$('#data-code'),
						compatibility: 		$('#data-compatibility'),
						d1: 				$('#data-d1'),
						d2: 				$('#data-d2'),
						d3: 				$('#data-d3'),
						datePosted: 		$('#data-datePosted'),
						description: 		$('#data-description'),
						documentlocation: 	$('#data-documentlocation'),
						documenttype: 		$('#data-documenttype'),
						gold: 				$('#data-gold'),
						id: 				$('#data-id'),
						mysection: 			$('#data-mysection'),
						name: 				$('#data-name'),
						platnium: 			$('#data-platnium'),
						shortdesc: 			$('#data-shortdesc'),
						silver: 			$('#data-silver'),
						state:				$('#data-state')
					};

		var contents = {
			init: function(){

				addBtn.bind('click', function(){
					contents.submitForm();
				});

				updateBtn.bind('click', function(){
					contents.submitForm();
				});

				$("#datePosted").datetimepicker( {
					format: "DD-MMM-YYYY"
				});

			},
			submitForm: function(){

				var isEdit = $.isNumeric(fields.id.val());

				swal({
				    title: (isEdit)?"Update Item" : "Add Item",
				    text: "Are you sure?",
				    icon: "warning",
				    buttons: ["Cancel", true],
				    dangerMode: false,
				})
				.then(willSubmit => {
				    if (willSubmit) {
						var request = $.ajax({
				          url: "<?php echo base_url(); ?>admin/downloads/add_update",
				          method: "POST",
				          data: form.serialize(),
				          dataType: 'json'
				        });

				        request.done(function( res ) {
				            if(res!=null) {

				            	if(isEdit) {
				            		if(res.hasOwnProperty('error')) {
				            			var errMsg = '';

				            			$.each(res.error, function(k, v){
				            				errMsg += '* ' + v + '\n';
				            			});

				            			new PNotify({
											title: 'Invalid or Required!',
											text: errMsg,
											type: 'error',
											styling: 'bootstrap3'
			                            });

				            			swal({title:"Update Failed!",text:"Some fields are required (*) or have invalid values",icon:'error',button:false, timer:3000});
				            		}
				            		else {
				            			filter.applyUpdateChanges(res);

				            			swal({title:"Updated!",text:'Changes have been saved.',icon:'success',button:false, timer:3000});
				            		}
				            		
				            	}
				            	else {
				            		if(res.hasOwnProperty('error')) {
										var errMsg = '';

				            			$.each(res.error, function(k, v){
				            				errMsg += '* ' + v + '\n';
				            			});

				            			new PNotify({
											title: 'Invalid or Required!',
											text: errMsg,
											type: 'error',
											styling: 'bootstrap3'
			                            });

			                            swal({title:"Add Failed!",text:"Some fields are required (*) or have invalid values",icon:'error',button:false, timer:3000});
				            		}
				            		else {
				            			fields.id.val(res.id);
				            			contents.showUpdateButton();
				            			addTab.html('<i class="fa fa-pencil"></i> Edit');
				            			swal({title:"Success!",text:"Added item with id: " + res.id,icon:'success',button:false, timer:3000});
				            			filter.addNewItem(res);
				            		}
				            	}
				            }
		        		});

				      	request.error(function(_data){
		        			if(_data.readyState == 4 && _data.status == 200) {//Session expired and redirected

		        				swal({
								    title: "Session Expired!",
								    text: "Reloading page.",
								    icon: "warning",
								    dangerMode: true,
								})
								.then(ok => {
								    if (ok) {
								    	window.location.href = '<?php echo base_url();?>admin/login';
								    }
								});
		        			}
		        		});
					}
				}); 

			},
			resetForm: function(){

				$.each(fields, function(k,v){
					if(v.length>0) {

						if(k == 'platnium' || k == 'bronze' || k == 'silver' || k == 'gold') {
							if(v.val()) { 
								if(v.parents('.icheckbox_flat-green').hasClass('checked')) v.parents('.icheckbox_flat-green').click();
							}
						}
						else {
							v.val('');
						}
					}
				});
			},
			populateForm: function(data){

				if(data!=null) {

					this.resetForm();

					setTimeout(function(){
						$.each(data, function(k,v){
							if(fields.hasOwnProperty(k)  && fields[k].length>0) {
								if(k == 'platnium' || k == 'bronze' || k == 'silver' || k == 'gold') {
									if(v) { 
										if(!fields[k].parents('.icheckbox_flat-green').hasClass('checked')) fields[k].parents('.icheckbox_flat-green').click();
									}
									else{
										if(fields[k].parents('.icheckbox_flat-green').hasClass('checked')) fields[k].parents('.icheckbox_flat-green').click();
									}
								}
								else {
									fields[k].val(v);
								}
							}
						});
						
						contents.showUpdateButton();

						if(addTab!=null) addTab.click();
					}, 50);
				}
			},
			showUpdateButton: function(){
				cancelBtn.show();
				updateBtn.show();
				addBtn.hide();	
			},
			showAddButton: function(){
				cancelBtn.hide();
				updateBtn.hide();
				addBtn.show();	
			},
			cancelUpdate: function(){

				swal({
				    title: "Cancel Changes",
				    text: "Are you sure?",
				    icon: "warning",
				    buttons: ["Cancel", true],
				    dangerMode: false,
				})
				.then(willCancel => {
				    if (willCancel) {
				    	contents.resetTab();
				    }
				});
			},
			resetTab: function(){
		    	addTab.html('<i class="fa fa-plus"></i> Add');
		    	contents.resetForm();
				contents.showAddButton();
				searchTab.trigger('click');
			}
		};

		contents.init();

	    addEdit = contents;

        if ($("input.flat")[0]) {
            $(document).ready(function () {
                $('input.flat').iCheck({
                    checkboxClass: 'icheckbox_flat-green',
                    radioClass: 'iradio_flat-green'
                }).on('ifChecked', function(event){
					$('#access').val('1');
				}).on('ifUnchecked', function(event){
					if($('.access:checked').length > 0) {
						$('#access').val('1');
					}
					else {
						$('#access').val('');
					}
				});
            });
        }	    
	});
</script>