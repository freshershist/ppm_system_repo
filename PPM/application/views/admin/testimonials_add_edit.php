<!-- page content -->
<div class="right_col" role="main" style="min-height: 100vh;">

  <div class="row">
    <div class="col-md-12">
      <div id="divHead">

          <?php if(empty($page_id) || $page_id == 0){ ?>
          <h1><i class="fa fa-comments-o"></i> New Testimonial</h1>
          <?php }else{ ?>
          <h1><i class="fa fa-comments-o"></i> Edit Testimonial</h1>
          <?php } ?>
          <div class="row">
            <div class="col-md-12" id="dispMessages"></div>
          </div>
          <div class="spacer20"></div>
          <div class="row">
              <div class="col-md-12">
                  <a href="<?php echo base_url().'admin/testimonials'; ?>" class="btn btn-primary">
                      <i class="fa fa-arrow-left"></i> Back
                  </a>
                  <button type="button" class="btn btn-primary" id="btnSave"><i class="fa fa-save"></i> Save</button>
              </div>
          </div>

      </div>
      <div class="spacer10"></div>
      <hr/>
      <!-- <div class="spacer10"></div> -->
      <?php
      // print_r($testi_details);
      if(isset($testi_details)){
        foreach($testi_details['result'] as $tdetails){
          $t_id = $tdetails['id'];
          $t_name = $tdetails['name'];
          $t_body = $tdetails['body'];
          $t_title = $tdetails['title'];
          $t_company = $tdetails['company'];
          $t_d1 = $tdetails['d1'];
          $t_displayhome = $tdetails['displayonhome'];
        }
      }

      // print_r($category_id);
      if(isset($category_id)){
        foreach($category_id['result'] as $tcategory){
          $t_cid = $tcategory['categoryid'];
        }
      }
      ?>

      <div class="row">
        <div class="col-md-4">
          <!-- START : MAIN FORM -->
          <form id="frmForm">
              <div class="form-group">
                  <label>Cateogry *</label>
                  <select class="form-control" name="selCategory" >
                    <option value="">[Select]</option>
                  <?php
                    foreach ($cat_array['result'] as $arr) {
                      # code...
                      $selected = '';
                      if(isset($t_cid)){
                        if($t_cid == $arr['id']){
                          $selected = 'selected';
                        }
                      }
                      echo '<option value="'.$arr['id'].'" '.$selected.'>'. ucwords($arr['name']) .'</option>';  
                    }
                  ?>
                  </select>
              </div>
              <div class="form-group">
                  <label>Name *</label>
                  <input type="hidden" class="form-control" name="txtID" value="<?php echo (isset($t_id )) ? $t_id : "";  ?>"/>
                  <input type="text" class="form-control" name="txtName"  value="<?php echo (isset($t_name )) ? $t_name : "";  ?>"/>
              </div>

              <div class="form-group">
                  <label>Description/Testimonial *</label>
                  <textarea class="form-control" name="txtDescript"><?php echo (isset($t_body )) ? $t_body : "";  ?></textarea>
              </div>

              <div class="form-group">
                  <label>Company</label>
                  <input type="text" class="form-control" name="txtCompany"  value="<?php echo (isset($t_company )) ? $t_company : "";  ?>"/>
              </div>

              <div class="form-group">
                  <label>Title</label>
                  <input type="text" class="form-control" name="txtTitle"  value="<?php echo (isset($t_title )) ? $t_title : "";  ?>"/>
              </div>

              <div class="form-group">
                  <label>Display Home:</label>
<!--                   <select class="form-control" name="selIsHome">
                      <option value="0">NO</option>
                      <option value="1">YES</option>
                  </select> -->
                  <input id="data-active" name="isHome" type="checkbox" class="js-switch"  <?php echo (isset($t_displayhome ) && $t_displayhome == '1') ? "checked" : "";  ?>/>
              </div>


              <div class="form-group">
                <input type="hidden" id="bid" value=""/>
                <label>Image</label>
                <div class="row">
                  <div class="col-md-8">
                    <div class="input-group">
                    <input type="text" class="form-control" id="imgPath" name="imgPath" value="<?php echo (isset($t_d1)) ? $t_d1 : ''; ?>">
                    <span class="input-group-btn">
                    <button id="ppm-browse-image" type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
                    </span>
                    </div>
                  </div>
                </div>
                <span><b>Important Note</b><br>
                      Images must be uploaded at 200 Pixels Wide.<br>
                      RGB Jpeg Files at 72 DPI.</span>
              </div>
          </form>
          <!-- END : MAIN FORM -->
        </div>
      </div>



    </div>

  </div>

	<!-- <div class="clearfix"></div> -->
</div>
<?php if(isset($upload)) echo $upload; ?>

<!-- /page content -->

<style>
.spacer10 {
  clear: both;
  width: 100%;
  height: 10px;
}
.spacer15 {
  clear: both;
  width: 100%;
  height: 15px;
}
.spacer20 {
  clear: both;
  width: 100%;
  height: 20px;
}
.spacer25 {
  clear: both;
  width: 100%;
  height: 25px;
}
.spacer30 {
  clear: both;
  width: 100%;
  height: 30px;
}

.hide-me {
  display: none;
}

.right_col {
  min-height: 100vh !important;
}



</style>
<script src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
<script>
$(window).scroll(function(){
  var bn = $('#divHead');
  if($(window).scrollTop() > 0) {
    bn.addClass('stickme');
  }
  else{
    bn.removeClass('stickme');
  }
});
</script>

<script>
$(document).ready(function(){
  function checkRequired(thisIsMe){
    var v = thisIsMe.val();
    if(v == ''){
      thisIsMe.addClass('required_field');
    }else{
      thisIsMe.removeClass('required_field');
    }
  }


  $('#btnAddEventPrice').click(function(){
    var html_ = '<tr>'
                 + '<td><input type="text" class="form-control"></td>'
                 + '<td><input type="text" class="form-control"></td>'
                 + '<td><select class="form-control"></select></td>'
                 + '</tr>'
    $('#tblEventPrice tbody').append(html_);
  });

  $('#btnSave').click(function(){
    var selcat = $('select[name=selCategory]').val();
    var txtName = $('input[name=txtName]').val();
    var txtDesc = $('textarea[name=txtDescript]').val();
    if(selcat == '' || txtName == '' || txtDesc == ''){
        swal({
          title: "Error!",
          text: "Please select or fill-in the required fields!",
          icon: "error",
          dangerMode: true,
        });
        checkRequired($('select[name=selCategory]'));
        checkRequired($('input[name=txtName]'));
        checkRequired($('textarea[name=txtDescript]'));
    }else{
        $.ajax({
          type : 'post',
          url  : '<?php echo base_url(); ?>admin/testimonials/save_data',
          data : $('#frmForm').serialize(),
          success: function(res){
            console.log(res);
            if(res == 'added'){
              swal({
                title: "Success!",
                text: "A new record has been added!",
                icon: "success",
                dangerMode: false,
              });
            }
            if(res == 'updated'){
              swal({
                title: "Success!",
                text: "Data has been updated!",
                icon: "success",
                dangerMode: false,
              });
            }
          }
        });
    }
  });

  $('select[name=selCategory]').blur(function(){
    var v = $(this).val();
    if(v == ''){
      $(this).addClass('required_field');
    }else{
      $(this).removeClass('required_field');
    }
  });

  $('input[name=txtName]').blur(function(){
    var v = $(this).val();
    if(v == ''){
      $(this).addClass('required_field');
    }else{
      $(this).removeClass('required_field');
    }
  });

  $('textarea[name=txtDescript]').blur(function(){
    var v = $(this).val();
    if(v == ''){
      $(this).addClass('required_field');
    }else{
      $(this).removeClass('required_field');
    }
  });


  if ($(".js-switch")[0]) {
      var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
      elems.forEach(function (html) {
          var switchery = new Switchery(html, {
              color: '#26B99A'
          });
      });
  }




});

</script>

