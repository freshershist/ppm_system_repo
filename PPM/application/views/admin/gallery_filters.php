<style>
	.text-word-wrap {
		width: 250px;
		display: inline-block;
		white-space: normal;
		line-height: 18px;
	}
</style>

<form id="gallery-form" class="form-horizontal form-label-left">
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Category: <i class="fa fa-asterisk text-danger"></i></label>
		<div class="col-md-9 col-sm-9 col-xs-12">
			<?php 

				$category_count = (count($categories_arr)>=6) ? 6 : count($categories_arr);

				echo form_multiselect('category', $categories_arr, '', 'id="category-list" class="select2_multiple form-control" size="' . $category_count . '" ');

			?>
		</div>
	</div>

	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Date Posted:</label>
		<div class="col-md-4 col-sm-4 col-xs-12">
	        <div class='input-group date' id='posted-from'>
	            <input name="datePostedFrom" type='text' class="form-control" placeholder="dd/mm/yyyy" />
	            <span class="input-group-addon">
	            <span class="glyphicon glyphicon-calendar"></span>
	            </span>
	        </div>
		</div>
		<div class="col-md-1 col-sm-1 col-xs-12"><label class="control-label">TO</label></div>
		<div class="col-md-4 col-sm-4 col-xs-12">
	        <div class='input-group date' id='posted-to'>
	            <input name="datePostedTo" type='text' class="form-control" placeholder="dd/mm/yyyy" />
	            <span class="input-group-addon">
	            <span class="glyphicon glyphicon-calendar"></span>
	            </span>
	        </div>
		</div>
	</div>

	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">&nbsp;</label>
		<div class="col-md-9 col-sm-9 col-xs-12">
			<button id="contents-search-btn" type="button" class="btn btn-success">Search</button>
		</div>
	</div>	

</form>

<script type="text/javascript">

$(document).ready(function(){
	var masterList 					= null;
	var form 						= $('#gallery-form');
	var listTable 					= $('#contents-table');
	var filtersSection 				= $('#headingOne');
	var resultsSection 				= $('#headingTwo');
	var totalLabel 					= $('#total-label p');
	var categoryType 				= $('#category-type');
	var categoryList 				= $('#category-list');
	var categoryListHolder 			= $('#category-list-holder');
	var resultsLabel 				= $('#headingTwo .panel-title small');
	var tempListHolder 				= $('<div></div>');
	var currentDataTable 			= null;
	var currentTable 				= null;
	var tableId 					= '';
	var loader 						= $('#loader');
	var percentage 					= 0;
	var progressCtr 				= 0;
	var total 						= 0;
	var tempListStr 				= '';
	var selectedCategory 			= 0;

	var contents = {
		tempFunc: null,
		init: function(){
			$('#contents-search-btn').on('click', function(){contents.getList()});

			searchTab.on('click', function(){
				if(currentDataTable!=null) {
					setTimeout(function(){
						currentDataTable.draw(false);
					}, 300);
				}
			});

			this.setDate('posted-from', 'posted-to');
		},
		getList: function(){

			if(contents.validateForm()) {
				addEdit.resetTab();

				selectedCategory = parseInt(categoryList.val());

				contents.showList();
				contents.scrollTop();
				totalLabel.removeClass('hide');
				totalLabel.html('Fetching Records... <i class="fa fa-spinner fa-spin"></i>');
				resultsLabel.text('');

				if(tableId!='') {
					$('#' + tableId + '_wrapper').remove();
					currentTable = null;
					currentDataTable = null;
				}

				var request = $.ajax({
		          url: "<?php echo base_url(); ?>admin/gallery/get_list",
		          method: "POST",
		          data: form.serialize(),
		          dataType: 'json'
		        });
		         
		        request.done(function( data ) {
		            if(data!=null) {
		            	contents.refreshList();

		            	masterList = data;//data.result;
		            	total = masterList.length;

		            	if(total>0) {
	    					totalLabel.removeClass('hide');

							tempListHolder.html('');

							loader.addClass('hide');
							totalLabel.html('Updating list with ' + numeral(total).format('0,0') + ' records. <i class="fa fa-spinner fa-spin"></i>');

		            		contents.processList();
		            	}
		            	else {
		            		contents.updateLoader(false);
		            		currentDataTable = currentTable.DataTable();
		            	}

		            	resultsLabel.text(categoryList.find('option:selected').text());

						data = null;
		            }
        		});

        		request.error(function(_data){
        			if(_data.readyState == 4 && _data.status == 200) {//Session expired and redirected
        				swal({
						    title: "Session Expired!",
						    text: "Reloading page.",
						    icon: "warning",
						    dangerMode: false,
						})
						.then(willDelete => {
						    if (willDelete) {
						    	window.location.href = '<?php echo base_url();?>admin/login';
						    }
						});
        			}
        		});
	    	}
		},
		processList: function(){

            setTimeout(function(){
            	
            	var columns = [
						        { data: 'id' },
						        { data: 'name' },
						        { data: 'category' },
						        { data: 'shortdesc'},
						        { data: 'body' },
						        { data: 'mydate' },
						        { data: 'action' }			        
						    ];

				currentDataTable = currentTable.DataTable( {
					data: masterList,
				    columns: columns,
				    "scrollY": (masterList.length >=10) ? 480 : false,
        			"scrollX": true,
        			"order": [[ 5, "desc" ]]
				} );

				currentTable.children('tbody').on('click', 'td.col-action', function () {
					contents.showActionPanel($(this));
				});

				contents.initListAction();

				contents.updateLoader(false);

				currentDataTable.on( 'draw.dt', function () {
				    contents.initListAction();
				});


            }, 1);

		},
		initListAction: function(){
			$.each($(currentTable).find('tr'), function(k,v){
		    	var id = $(v).children('td:first').text();

		    	if($.isNumeric(id)) {
		    		var td = $(v).children('td:nth-last-child(1)');
		    		td.addClass('col-action').attr({'data-id':id});
		    	}
		    });
		},
		refreshList: function(){
			if(tableId!='') {
				$('#' + tableId + '_wrapper').remove();
				currentTable = null;
				currentDataTable = null;
			}

			tableId = contents.generateID();

			var tempTable = null;

			tempTable = listTable.clone().removeClass('hide').attr({id:tableId});

			tempTable.insertAfter(listTable);

			currentTable = $('#' + tableId);

			tempTable = null;
		},
		updateResultsCount: function(){
			var str = '';

			if($('tr.row-item').length>0){

				str = '('+ numeral($('tr.row-item').length).format('0,0')+' records found)';
			}
			else {
				str = '';
			}

			resultsLabel.text(str);
		},
		showActionPanel: function(td){
			var col 		= $(td);
			var id 			= col.data().id;
			var edit 		= col.find('ul li ul li a.edit');
			var copy 		= col.find('ul li ul li a.copy');
			var deleteLnk 	= col.find('ul li ul li a.delete');

			col.find('ul li ul li a label.action-id').text(id);

			edit.unbind('click').bind('click', function(){contents.requestByAction({'id':id, 'action':$(this).attr('class')})});
			
			deleteLnk.unbind('click').bind('click', function(){contents.requestByAction({'id':id, 'action':$(this).attr('class')})});

			col.parents('tr').attr({id:'row-'+id})

			edit = deleteLnk = null;
		},
		requestByAction: function(data){

			if(data!=null) {

		    	contents.tempFunc = function(data) {
		    		if(parseInt(data.id) == 24 && data.action == 'delete') {
		    			swal({title:"Delete Item!",text:"You cannot delete this gallery, as there are static links in the website to this gallery.",icon:'warning',button:false, timer:3000});
		    			return false;
		    		}

		    		var request = $.ajax({
			          url: "<?php echo base_url(); ?>admin/gallery/action",
			          method: "POST",
			          data: data,
			          dataType: 'json'
			        });

			        request.done(function( _data ) {
			            if(_data!=null) {
			            	if(data.action == 'edit') {
				            	if(addEdit!=null) {
				            		_data.id = data.id;
				            		addEdit.populateForm(_data);
				            	}
			            	}
			            	else if(data.action == 'delete') {
			            		if(_data.hasOwnProperty('success')) {
			            			addEdit.resetTab();
			            			currentDataTable.row('#row-'+data.id).remove().draw( false );
			            			swal({title:"Success!",text:"Deleted item with ID: " + data.id,icon:'success',button:false, timer:3000});
			            		}
			            		else {
			            			swal({title:"Delete Failed!",text:"No record was found.",icon:'error',button:false, timer:3000});
			            		}
			            	}

			            	_data = null;
			            }
	        		});

	        		request.error(function(_data){
	        			if(_data.readyState == 4 && _data.status == 200) {//Session expired and redirected
	        				swal({
							    title: "Session Expired!",
							    text: "Reloading page.",
							    icon: "warning",
							    dangerMode: false,
							})
							.then(willDelete => {
							    if (willDelete) {
							    	window.location.href = '<?php echo base_url();?>admin/login';
							    }
							});
	        			}
	        		});
		    	}

				var showAddEditTab = false;

            	if(data.action == 'edit') {
					showAddEditTab = true;
					addTab.html('<i class="fa fa-pencil"></i> ' + data.action.charAt(0).toUpperCase() + data.action.substr(1));
            	}
            	else if(data.action == 'delete') {

            		if(parseInt(data.id) == 24) {
            			swal({title:"Delete Item!",text:"You cannot delete this gallery, as there are static links in the website to this gallery.",icon:'warning',button:false, timer:3000});
            		}
            		else {
	            		swal({
							title: "Delete Item",
							text: "Are you sure?",
							icon: "warning",
							buttons: ["Cancel", true],
							dangerMode: true,
						})
						.then(willDelete => {
						
							if(willDelete) {
								contents.tempFunc.call(this, data);
							}
						
						}); 
					}
            	}

            	if(showAddEditTab) {
            		if(contents.tempFunc!=null) contents.tempFunc.call(this, data);
		    	}
	    	}
		},
		applyUpdateChanges: function(data){

			if(data!=null) {

				var row = $('#row-'+data.id);

				row.children('td')[1].innerHTML = data.name;
				row.children('td')[2].innerHTML = '<span class="text-word-wrap">'+addEdit.categoryArr[data.category]+'</span>';
				row.children('td')[3].innerHTML = data.shortdesc;
				row.children('td')[4].innerHTML = data.body;
				row.children('td')[5].innerHTML = data.mydate;
			}
		},
		addNewItem: function(res) {
			if(res!=null) {

				var data = {
					      "id":res.id,
					      "name":res.name,
					      "category":'<span class="text-word-wrap">'+addEdit.categoryArr[res.category]+'</span>',
					      "shortdesc": res.shortdesc,
					      "body":res.body,
					      "mydate":res.mydate,
					      "action":res.action
						}; 	

            	currentDataTable.row.add(data).node().id = 'row-'+res.id;
            	currentDataTable.page( 'next' ).draw( false );
            	currentDataTable.page( 'previous' ).draw( false );
			}
		},
		updateLoader: function(show){
			if(show) {
				var percentage = progressCtr / total;

				if(progressCtr % 20 == 0) loader.css({width: (percentage * 100) + '%'});

		        progressCtr++;

		        totalLabel.text('Fetching ' + numeral(progressCtr).format('0,0') + ' of ' + numeral(total).format('0,0') + ' records');
	    	}
	    	else {
	    		progressCtr = total = 0;

	    		loader.css({width: '0%'}).addClass('hide');
	    		totalLabel.text('').addClass('hide');
	    	}
		},
		scrollTop: function(){
			$('html').scrollTop(0);
		},
		showList: function(){
			if(resultsSection.attr('aria-expanded') != "true") resultsSection.trigger('click');
		},
		validateForm: function(){
			var isValid = false;

			if(categoryType.val() == '') {
				return false;
			}

			return true;
		},
		generateID: function() {
	      var charSet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
	      var charCount = 20;
	      var charSetSize = charSet.length;
	      var id = '';

	      for (var i = 1; i <= charCount; i++) {
	          var randPos = Math.floor(Math.random() * charSetSize);
	          id += charSet[randPos];
	      }

	      return id;      
	    },
		setDate: function(fromId, toId) {
	    	$('#' + fromId).datetimepicker({
				format: 'DD/MM/YYYY'
			});
	    
		    $('#' + toId).datetimepicker({
		        useCurrent: false,
		        format: 'DD/MM/YYYY'
		    });
		    
		    $("#" + fromId).on("dp.change", function(e) {
		        $('#' + toId).data("DateTimePicker").minDate(e.date);
		    });
		    
		    $("#" + toId).on("dp.change", function(e) {
		        $('#' + fromId).data("DateTimePicker").maxDate(e.date);
		    });
	    }

	};

	contents.init();

	filter = contents;

});

</script>