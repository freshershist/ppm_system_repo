<style>
	hr.style-two {
	    border: 0;
	    height: 0;
	    border-top: 1px solid rgba(0, 0, 0, 0.1);
	    border-bottom: 1px solid rgba(255, 255, 255, 0.3);
	}	
</style>

<div class="container">

	<div class="col-md-12 col-sm-12 col-xs-12">
		<div id="copy-title"class="alert alert-success fade in" role="alert" style="display: none;">
			<strong></strong>
		</div>
		<p class="lead"><small class="text-info small">Compulsary fields are marked by an asterisk ( <i class="fa fa-asterisk text-danger small"></i> )</small></p>

		<form id="add-edit-form" class="form-horizontal form-label-left">
			<input id="data-id" type="hidden" name="id" value="" />
			
			<!-- start accordion -->
			<div class="accordion" id="accordion-add-edit" role="tablist" aria-multiselectable="true">

				<!-- General -->	
				<div class="panel">
					<a class="panel-heading" role="tab" id="contents-add-edit_1" data-toggle="collapse" data-parent="#accordion-add-edit" href="#add-edit_1" aria-expanded="true" aria-controls="add-edit_1">
						<h4 class="panel-title">General</h4>
					</a>
					<div id="add-edit_1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="contents-add-edit_1">
						<div class="panel-body">
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Name: <i class="fa fa-asterisk text-danger"></i></label>
								<div class="col-md-5 col-sm-12 col-xs-12">
									<input id="data-name" name="name" type="text" class="form-control" placeholder="" value="" />
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Code: <i class="fa fa-asterisk text-danger"></i></label>
								<div class="col-md-5 col-sm-12 col-xs-12">
									<div class="input-group">
										<input id="data-code" type="text" name="code" class="form-control">
										<span class="input-group-btn">
										<button id="generate-code" type="button" class="btn btn-primary"><i class="fa fa-refresh"></i></button>
										</span>
									</div>
								</div>							
							</div>                            

							<div class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Start Date: <i class="fa fa-asterisk text-danger"></i></label>
								<div class="col-md-5 col-sm-12 col-xs-12">
							        <div class="input-group date" id="startdate">
							            <input id="data-startdate" name="startdate" type='text' class="form-control" placeholder="dd/mm/yyyy" />
							            <span class="input-group-addon">
							            <span class="glyphicon glyphicon-calendar"></span>
							            </span>
							        </div>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">End Date: <i class="fa fa-asterisk text-danger"></i></label>
								<div class="col-md-5 col-sm-12 col-xs-12">
							        <div class="input-group date" id="enddate">
							            <input id="data-enddate" name="enddate" type='text' class="form-control" placeholder="dd/mm/yyyy" />
							            <span class="input-group-addon">
							            <span class="glyphicon glyphicon-calendar"></span>
							            </span>
							        </div>
								</div>
							</div>							
						</div>	
					</div>
				</div>
				<!-- General -->

			</div>
	
		</form>

	</div>	

	<div class="row">
		<div class="panel">
			<div class="col-md-12 text-right">
				<button id="cancel-btn" type="button" class="btn btn-sm btn-warning" style="display: none;">Cancel Changes</button>
				<button id="update-btn" type="button" class="btn btn-sm btn-success" style="display: none;">Update</button>
				<button id="add-btn" type="button" class="btn btn-sm btn-success add-btn">Add</button>
			</div>
			<div class="clearfix"></div>
		</div>	
	</div>

</div>

<div class="row hide">
	<div class="col-md-5">
		<div class="input-group">
		<input type="text" class="form-control">
		<span class="input-group-btn">
		<button id="ppm-browse-image" type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
		</span>
		</div>
	</div>
</div>

<script>

	$(document).ready(function(){
		var form = $('#add-edit-form');
		var fields = {
						id: 		$('#data-id'),
						name: 		$('#data-name'),
						code: 		$('#data-code'),
						startdate: 	$('#data-startdate'),
						enddate:	$('#data-enddate')
					};

		var contents = {
			init: function(){

				addBtn.bind('click', function(){
					contents.submitForm();
				});

				updateBtn.bind('click', function(){
					contents.submitForm();
				});

				this.setDate('startdate', 'enddate');

				$('#generate-code').on('click', function(){
					fields.code.val(contents.generateCode(8));
				});

			},
			setDate: function(fromId, toId) {
		    	$('#' + fromId).datetimepicker({
					format: 'DD/MM/YYYY'
				});
		    
			    $('#' + toId).datetimepicker({
			        useCurrent: false,
			        format: 'DD/MM/YYYY'
			    });
			    
			    $("#" + fromId).on("dp.change", function(e) {
			        $('#' + toId).data("DateTimePicker").minDate(e.date);
			    });
			    
			    $("#" + toId).on("dp.change", function(e) {
			        $('#' + fromId).data("DateTimePicker").maxDate(e.date);
			    });
		    },
			submitForm: function(){

				var isEdit = $.isNumeric(fields.id.val());

				swal({
				    title: (isEdit)?"Update Item" : "Add Item",
				    text: "Are you sure?",
				    icon: "warning",
				    buttons: ["Cancel", true],
				    dangerMode: false,
				})
				.then(willSubmit => {
				    if (willSubmit) {
						var request = $.ajax({
				          url: "<?php echo base_url(); ?>admin/promocodes/add_update",
				          method: "POST",
				          data: form.serialize(),
				          dataType: 'json'
				        });

				        request.done(function( res ) {
				            if(res!=null) {

				            	if(isEdit) {
				            		if(res.hasOwnProperty('error')) {
				            			var errMsg = '';

				            			$.each(res.error, function(k, v){
				            				errMsg += '* ' + v + '\n';
				            			});

				            			new PNotify({
											title: 'Invalid or Required!',
											text: errMsg,
											type: 'error',
											styling: 'bootstrap3'
			                            });

				            			swal({title:"Update Failed!",text:"Some fields are required (*) or have invalid values",icon:'error',button:false, timer:3000});
				            		}
				            		else {
				            			filter.applyUpdateChanges(res);

				            			swal({title:"Updated!",text:'Changes have been saved.',icon:'success',button:false, timer:3000});
				            		}
				            		
				            	}
				            	else {
				            		if(res.hasOwnProperty('error')) {
										var errMsg = '';

				            			$.each(res.error, function(k, v){
				            				errMsg += '* ' + v + '\n';
				            			});

				            			new PNotify({
											title: 'Invalid or Required!',
											text: errMsg,
											type: 'error',
											styling: 'bootstrap3'
			                            });

			                            swal({title:"Add Failed!",text:"Some fields are required (*) or have invalid values",icon:'error',button:false, timer:3000});
				            		}
				            		else {
				            			fields.id.val(res.id);
				            			contents.showUpdateButton();
				            			addTab.html('<i class="fa fa-pencil"></i> Edit');
				            			swal({title:"Success!",text:"Added item with id: " + res.id,icon:'success',button:false, timer:3000});
				            			filter.addNewItem(res);
				            		}
				            	}
				            }
		        		});

				      	request.error(function(_data){
		        			if(_data.readyState == 4 && _data.status == 200) {//Session expired and redirected

		        				swal({
								    title: "Session Expired!",
								    text: "Reloading page.",
								    icon: "warning",
								    dangerMode: true,
								})
								.then(ok => {
								    if (ok) {
								    	window.location.href = '<?php echo base_url();?>admin/login';
								    }
								});
		        			}
		        		});
					}
				}); 

			},
			resetForm: function(){

				$.each(fields, function(k,v){
					if(v.length>0) {
						v.val('');
					}
				});
			},
			populateForm: function(data){

				if(data!=null) {

					this.resetForm();

					setTimeout(function(){
						$.each(data, function(k,v){
							if(fields.hasOwnProperty(k)  && fields[k].length>0) {
								fields[k].val(v);
							}
						});
						
						contents.showUpdateButton();

						if(addTab!=null) addTab.click();
					}, 50);
				}
			},
			showUpdateButton: function(){
				cancelBtn.show();
				updateBtn.show();
				addBtn.hide();	
			},
			showAddButton: function(){
				cancelBtn.hide();
				updateBtn.hide();
				addBtn.show();	
			},
			cancelUpdate: function(){

				swal({
				    title: "Cancel Changes",
				    text: "Are you sure?",
				    icon: "warning",
				    buttons: ["Cancel", true],
				    dangerMode: false,
				})
				.then(willCancel => {
				    if (willCancel) {
				    	contents.resetTab();
				    }
				});
			},
			generateCode: function(size) {
		      var charSet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*(){}';
		      var charCount = size;
		      var charSetSize = charSet.length;
		      var code = '';

		      for (var i = 1; i <= charCount; i++) {
		          var randPos = Math.floor(Math.random() * charSetSize);
		          code += charSet[randPos];
		      }

		      return code;      
		    },
			resetTab: function(){
		    	addTab.html('<i class="fa fa-plus"></i> Add');
		    	contents.resetForm();
				contents.showAddButton();
				searchTab.trigger('click');
			}
		};

		contents.init();

	    addEdit = contents;
	});
</script>