<script type="text/javascript">
	var addTab,searchTab;
	var filter 			= null;
	var addEdit 		= null;
	var cancelBtn 		= null;
	var addBtn 			= null;
	var updateBtn 		= null;
	var searchTab 		= null;

	$(document).ready(function(){
		addTab					= $('#add-tab');
		searchTab				= $('#search-tab');
		cancelBtn 				= $('#cancel-btn');
		addBtn 					= $('#add-btn');
		updateBtn 				= $('#update-btn');

		cancelBtn.on('click', function(){
			if(addEdit!=null) addEdit.cancelUpdate();
		});		
	});

</script>

<!-- page content -->
<div class="right_col" role="main" style="min-height: 100vh;">

	<div class="col-md-12 col-lg-12 col-xl-10" style="padding-bottom: 60px;">
		<div class="x_panel">
			<div class="x_title">
				<h2><?php echo $page_title; ?></h2>
				<ul class="nav navbar-right panel_toolbox" style="min-width:auto!important;">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">

			    <div class="" role="tabpanel" data-example-id="togglable-tabs">
					<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
					
					<li role="presentation" <?php if(!empty($action) && $action === 'list'){ echo 'class="active"'; } ?>><a href="#tab_content1" id="search-tab" role="tab" data-toggle="tab" aria-expanded="<?php if(!empty($action) && $action === 'list'){ echo 'true'; }else{ echo 'false'; } ?>"><i class="fa fa-search"></i> Search</a>
					</li>

					<li role="presentation" <?php if(!empty($action) && ($action === 'add')){ echo 'class="active"'; }  ?> ><a href="#tab_content2" role="tab" id="add-tab" data-toggle="tab" aria-expanded="<?php if(!empty($action) && ($action === 'add')){ echo 'true'; }else{ echo 'false'; } ?>"><i class="fa fa-plus"></i> Add</a>
					</li>

					</ul>
					<div id="myTabContent" class="tab-content">
						<div role="tabpanel" class="tab-pane fade <?php if(!empty($action) && $action === 'list'){?>active in<?php } ?>" id="tab_content1" aria-labelledby="search-tab">

		                    <!-- start accordion -->
		                    <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
		                      <div class="panel hide">
		                        <a class="panel-heading" role="tab" id="headingOne" data-toggle="collapse" data-parent="" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
		                          <h4 class="panel-title">Filters</h4>
		                        </a>
		                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
		                          <div class="panel-body">
		                          	<div class="col-md-12">
		                          	<?php 
		                          		if(isset($filters)) {
		                          			echo $filters;
		                          		}
		                          	?>
				                    </div>
		                          </div>
		                        </div>
		                      </div>
		                      <div class="panel">
		                        <a class="panel-heading collapsed" role="tab" id="headingTwo" data-toggle="collapse" data-parent="" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
		                          <h4 class="panel-title">Results <small class="badge bg-green" style="color:#fff;"></small></h4>
		                        </a>
		                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
		                          <div class="panel-body">

			                         	<table id="contents-table" class="table table-striped table-hover text-left hide dt-responsive nowrap" data-page-length='10' cellspacing="0" width="100%"><!-- table-bordered  -->
			                              <thead>
			                                <tr>
												<th style="text-align:left!important; vertical-align: middle!important;">ID</th>
												<th style="text-align:left!important; vertical-align: middle!important;">Page Title</th>
												<th style="text-align:left!important; vertical-align: middle!important;">Date Added</th>
												<th style="text-align:left!important; vertical-align: middle!important;"></th>                                  
			                                </tr>
			                              </thead>
			                              <tbody>
			                              </tbody>
			                            </table>

		                            <div id="loader" class="progress-bar progress-bar-success hide" data-transitiongoal="100" aria-valuenow="0" style="height:2px;"></div>
		                            <div class="clearfix"><p>&nbsp;</p></div>
		                            <div id="total-label" class="text-center"><p class="hide"></p></div>

		                          </div>
		                        </div>
		                      </div>
		                    </div>
		                    <!-- end of accordion -->

						</div>
						
						<div role="tabpanel" class="tab-pane fade <?php if(!empty($action) && ($action === 'add')){?>active in<?php } ?>" id="tab_content2" aria-labelledby="add-tab">
							<?php 
								if(isset($add_edit)) {
									echo $add_edit;
								}
							?>
						</div>

					</div>
			    </div>

			</div>
		</div>
	</div>

	<div class="clearfix"></div>
</div>

<?php if(isset($upload)) echo $upload; ?>

<!-- /page content -->