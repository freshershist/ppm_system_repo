<!-- page content -->
<div class="right_col" role="main" style="min-height: 100vh;">

  <div class="row">
    <div class="col-md-12">
      <h1><i class="fa fa-comments-o"></i> Testimonials</h1>

      <div class="spacer20"></div>
      <button class="btn btn-primary" type="button" id="btnAddNew"> <i class="fa fa-plus"></i> Add New </button>
      <hr/>
      <div class="spacer20"></div>
      <div class="row">
        <div class="col-md-12">

      <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#testimonials">Testimonials</a></li>
        <li><a data-toggle="tab" href="#Staff">Staff</a></li>
      </ul>

      <!-- START : TAB CONTENT -->

      <div class="tab-content">
        <div id="testimonials" class="tab-pane fade in active">
          <h3>TESTIMONIALS</h3>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group col-md-4">
                <label>Select Category:</label>
                <?php
                  // echo '<pre>';
                  // print_r($cat_array);
                  // echo '</pre>';
                ?>
                <select id="selCat" class="form-control">
                  <option>All</option>
                  <?php
                    foreach ($cat_array['result'] as $arr) {
                      # code...
                      if(strtolower($arr['name']) !== 'staff'){
                        echo '<option value="'.$arr['id'].'">'. ucwords($arr['name']) .'</option>';  
                      }
                    }
                  ?>
                </select>
              </div>
            </div>
          </div>
          <!-- START : LIST TABLE -->
            <table class="table table-striped" id="tblTestiList">
              <thead>
                <tr>
                  <!-- <td><input type="checkbox" id="chkAllItems" class="flat"></td> -->

                  <td>Name</td>
                  <td width="40%">Description</td>
                  <td>Company</td>
                  <td>Action</td>
                </tr>
              </thead>
              <tbody>
                  <?php
                    // function is_url_exist($url){
                    //     $ch = curl_init($url);    
                    //     curl_setopt($ch, CURLOPT_NOBODY, true);
                    //     curl_exec($ch);
                    //     $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                    //     if($code == 200){
                    //        $status = true;
                    //     }else{
                    //       $status = false;
                    //     }
                    //     curl_close($ch);
                    //    return $status;
                    // }
                    // `id`, `name`, `body`, `title`, `company`, `d1`, `displayonhome`
                    if(count($list_array['result']) > 0):
                        foreach($list_array['result'] as $arr){
                          //`pid`, `title`, `content`, `parent_id`, `menu`, `page_order`, `user_id`, `banner_top`, `banner_bottom`, `status`, `date_added`
                          // if($arr['status'] == 'draft'){
                          //   $status = ' - <strong><em>Draft</em></strong>';
                          // }else{
                          //   $status = '';
                          // }
                          // if(is_url_exist($arr['image'])){
                          //   $image = '<img src="'.$arr['image'].'" class="img-rounded" style="width: 100px;">';
                          // }else{
                          //   $image = '<i class="fa fa-image fa-3x"></i>';
                          // }

                          echo '<tr>
                                  <!--<td><input type="checkbox" id="chk'.$arr['id'].'" class="flat"></td>-->
                                  <td>'.$arr['name'].'</td>
                                  <td>'.$arr['body'].'</td>
                                  <td>'.$arr['company'].'</td>
                                  <td>
                                    <button class="btn btn-success btnEditPage" type="button" data-pid="'.$arr['id'].'"><i class="fa fa-pencil"></i></button>
                                    <button class="btn btn-danger btnDeletePage" type="button"  data-pid="'.$arr['id'].'"><i class="fa fa-trash"></i></button>
                                  </td>
                                </tr>
                          ';
                        }
                    endif;
                  ?>
              </tbody>
            </table>

                <?php

                  // echo '<pre>';
                  // // print_r($list_array);
                  // echo '</pre>';
                  // if(count($list_array['result']) > 0):
                  //     echo 'hello';
                  //     foreach($list_array['result'] as $arr){
                  //       //`pid`, `title`, `content`, `parent_id`, `menu`, `page_order`, `user_id`, `banner_top`, `banner_bottom`, `status`, `date_added`
                  //       echo $arr['uid'].':'.$arr['username'].'<br>';
                  //     }
                  // endif;
                // $this->ppmsystemlib->get_accepted_mimes();
                ?>
          <!-- END : LIST TABLE -->
          <div class="spacer30"></div>
          <div class="spacer30"></div>
        </div>
        <div id="Staff" class="tab-pane fade">
          <h3>STAFF</h3>
          <p>Some content in menu 1.</p>
          <!-- START : LIST TABLE -->
            <table class="table table-striped" id="tblStaffList">
              <thead>
                <tr>
                  <!-- <td><input type="checkbox" id="chkAllItems" class="flat"></td> -->

                  <td>Name</td>
                  <td width="40%">Description</td>
                  <td>Company</td>
                  <td>Action</td>
                </tr>
              </thead>
              <tbody>
                  <?php
                    // function is_url_exist($url){
                    //     $ch = curl_init($url);    
                    //     curl_setopt($ch, CURLOPT_NOBODY, true);
                    //     curl_exec($ch);
                    //     $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                    //     if($code == 200){
                    //        $status = true;
                    //     }else{
                    //       $status = false;
                    //     }
                    //     curl_close($ch);
                    //    return $status;
                    // }
                    // `id`, `name`, `body`, `title`, `company`, `d1`, `displayonhome`
                    if(count($staffs['result']) > 0):
                        foreach($staffs['result'] as $sarr){
                          //`pid`, `title`, `content`, `parent_id`, `menu`, `page_order`, `user_id`, `banner_top`, `banner_bottom`, `status`, `date_added`
                          // if($arr['status'] == 'draft'){
                          //   $status = ' - <strong><em>Draft</em></strong>';
                          // }else{
                          //   $status = '';
                          // }
                          // if(is_url_exist($arr['image'])){
                          //   $image = '<img src="'.$arr['image'].'" class="img-rounded" style="width: 100px;">';
                          // }else{
                          //   $image = '<i class="fa fa-image fa-3x"></i>';
                          // }

                          echo '<tr>
                                  <!--<td><input type="checkbox" id="chk'.$sarr['id'].'" class="flat"></td>-->
                                  <td>'.$sarr['name'].'</td>
                                  <td>'.$sarr['body'].'</td>
                                  <td>'.$sarr['company'].'</td>
                                  <td>
                                    <button class="btn btn-success btnEditPage" type="button" data-pid="'.$sarr['id'].'"><i class="fa fa-pencil"></i></button>
                                    <button class="btn btn-danger btnDeletePage" type="button"  data-pid="'.$sarr['id'].'"><i class="fa fa-trash"></i></button>
                                  </td>
                                </tr>
                          ';
                        }
                    endif;
                  ?>
              </tbody>
            </table>

                <?php
                  // echo '<pre>';
                  // // print_r($list_array);
                  // echo '</pre>';
                  // if(count($list_array['result']) > 0):
                  //     echo 'hello';
                  //     foreach($list_array['result'] as $arr){
                  //       //`pid`, `title`, `content`, `parent_id`, `menu`, `page_order`, `user_id`, `banner_top`, `banner_bottom`, `status`, `date_added`
                  //       echo $arr['uid'].':'.$arr['username'].'<br>';
                  //     }
                  // endif;
                // $this->ppmsystemlib->get_accepted_mimes();
                ?>
          <!-- END : LIST TABLE -->

        </div>
      </div>

      <!-- END : TAB CONTENT -->

        </div>
      </div>

    </div>

  </div>

	<div class="clearfix"></div>
</div>
<div class="clearfix"></div>
<div id="member-logs" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

			<div class="modal-header">
				<h4 class="modal-title">Title</h4>
			</div>
			<div class="modal-body">

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>

		</div>
	</div>
</div>

<!-- /page content -->

<style>
.spacer10 {
  clear: both;
  width: 100%;
  height: 10px;
}
.spacer20 {
  clear: both;
  width: 100%;
  height: 20px;
}
.spacer30 {
  clear: both;
  width: 100%;
  height: 30px;
}

#tblTestiList thead td {
  font-weight: bold;
}

</style>

<script>
  $(document).ready(function(){
    var baseurl = '<?php echo base_url(); ?>';

    $('#chkAllItems').iCheck({
      checkboxClass: 'icheckbox_square',
      radioClass: 'iradio_square',
      increaseArea: '20%' // optional
    });

    // setTimeout(function(){
      var tblProdData = $('#tblTestiList').DataTable({
        "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        "columnDefs": [{ "orderable": false, "targets": [2] }],
        "order": [[ 0, "asc" ]],
        "scrollY": "400px",
        "scrollCollapse": true,
        "paging": true
      });
      // $RIGHT_COL.css('min-height', $(window).height() + 200);
    // },1500);

    var tblStaffData 
    setTimeout(function(){
        tblStaffData = $('#tblStaffList').DataTable({
        "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        "columnDefs": [{ "orderable": false, "targets": [2] }],
        "order": [[ 0, "asc" ]],
        "scrollY": "400px",
        "scrollCollapse": true,
        "paging": true
      });

    }, 2000);

    //EDIT
    $('#tblTestiList tbody').on('click', ' td > .btnEditPage', function(){
      // alert('Test' + $(this).data('pid'));
      window.location = baseurl + 'admin/testimonials/edit/' + $(this).data('pid') + '/';
    });

    //EDIT
    $('#tblStaffList tbody').on('click', ' td > .btnEditPage', function(){
      // alert('Test' + $(this).data('pid'));
      window.location = baseurl + 'admin/testimonials/edit/' + $(this).data('pid') + '/';
    });

    //DELETE
    $('#tblTestiList tbody').on('click', ' td > .btnDeletePage', function(){
        var page_id = $(this).data('pid');
        var q = confirm('Are you sure you want to delete the page?');
        if(q){
          console.log(tblProdData);
          // console.log( $(this).parents('tr') );
          // var row = tblProdData.row(  $(this).closest('tr') );
          // var rowNode = row.node();
          // row.remove();
          tblProdData.row( $(this).parents('tr') ).remove().draw();

          $.ajax({
            type : 'post',
            url  : baseurl + 'admin/testimonials/delete',
            data : {
              'page_id' : page_id
            },
            beforeSend : function(){

            },
            success : function(result){

            }
          });
        }
    });

    $('#btnAddNew').click(function(){
      // $.ajax({
      //   type: 'post',
      //   url : baseurl + 'admin/pages/test',
      //   success:function(res){
      //     console.log(res);
      //   }
      // });
      window.location = baseurl + 'admin/testimonials/add';
    });


    $('#selCat').change(function(){
      // console.log($(this).val());
      var cat_id = $(this).val();
      $.ajax({
        type : 'post',
        url  : baseurl + 'admin/testimonials/category',
        data : {
          'cat_id' : cat_id
        },
        dataType : 'json',
        beforeSend : function(){

        },
        success : function(res){
          console.log(res);

          tblProdData.clear().draw();


          if(typeof res.result !== 'undefined'){
            var len = res.result.length;
            console.log(len);
            for(var i = 0; i < len; i++){
                var btn_ = '\
                    <button class="btn btn-success btnEditPage" type="button" data-pid="'+ res.result[i].id +'"><i class="fa fa-pencil"></i></button>\
                    <button class="btn btn-danger btnDeletePage" type="button"  data-pid="'+ res.result[i].id +'"><i class="fa fa-trash"></i></button>\
                ';
                tblProdData.row.add( [
                  res.result[i].name,
                  res.result[i].body,
                  res.result[i].company,
                  btn_
                ]).draw( false );              
            }
          }




        }
      });
    });





    // START : column adjust on bootstrap tabs fix ===============================
    // reference: https://www.gyrocode.com/articles/jquery-datatables-column-width-issues-with-bootstrap-tabs/
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
       $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
    });
    // END   : column adjust on bootstrap tabs fix ===============================
  });
</script>
