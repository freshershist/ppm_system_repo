<!-- page content -->
<div class="right_col" role="main" style="min-height: 100vh;">
	<div class="row">
		<div class="col-md-12">
			<?php if(empty($page_id) || $page_id == 0){ ?>
			<h1><i class="fa fa-image"></i> Add New Banner</h1>
			<?php }else{ ?>
			<h1><i class="fa fa-image"></i> Edit Banner</h1>
			<?php } ?>
		</div>

	</div>
<?php


	if(empty($page_id) || $page_id == 0){

	}else{
		// echo '<pre>';
		// print_r($banner_details); 
		if(count($banner_details['result']) > 0 ){
			$isActive = $banner_details['result'][0]['activate'];
			$image = $banner_details['result'][0]['image'];
			$myLink = $banner_details['result'][0]['myLink'];
			$placement = $banner_details['result'][0]['placement'];
			$contentType = $banner_details['result'][0]['contentType'];
			$bid = $banner_details['result'][0]['id'];
		}

		// if(count($banner_child) > 0){
		// 	foreach($banner_child['result'] as $banner_child){

		// 	}
		// }
		// print_r($banner_child);
		// echo '</pre>';		
	}
?>
	<div class="row">
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-12">
					<!-- START PANEL -->
					<div class="panel panel-default">
					  <div class="panel-heading"><strong>Banner Details</strong></div>
					  <div class="panel-body">
					  		<!-- START FORM  -->
					  		<div class="row">
								<div class="col-md-12">
									<div class="row">
										<div class="col-md-5">
											<div class="form-group">
												<label><input type="checkbox" id="chkIsActive" class="flat" value="<? echo (!empty($page_id))? $isActive : '';  ?>" <?php echo (isset($isActive) && $isActive == '1')? 'checked': ''; ?> > Active?</label>
											</div>

											<div class="form-group">
												<label>Type :</label>
												<select id="bannerType" class="form-control">
													<option value="1" <?php echo (isset($contentType) && $contentType == '1') ? 'selected' : ''; ?> >Banner</option>
													<option value="2" <?php echo (isset($contentType) && $contentType == '2') ? 'selected' : ''; ?> >Featured</option>
													<option value="3" <?php echo (isset($contentType) && $contentType == '3') ? 'selected' : ''; ?> >Carousel</option>
												</select>
											</div>
										</div>
									</div>
									<div class="row" id="divImageCarousel">
										<div class="col-md-12">

											<button type="button" class="btn btn-primary" id="btnAddImage"><i class="fa fa-plus"></i></button>

											<div class="form-group">
												<input type="hidden" id="bid" value="<?php echo (isset($bid)) ? $bid : ''; ?>"/>
												<label>Image</label>
												<div class="row">
												  <div class="col-md-8">
												    <div class="input-group">
												    <input type="text" class="form-control" id="txtCPath0" value="<?php echo (isset($image)) ? $image : ''; ?>">
												    <span class="input-group-btn">
												    <button id="ppm-browse-image0" type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
												    </span>
												    </div>
												  </div>
												</div>
												<?php //if(isset($upload)) echo $upload; ?>
											</div>
											<?php
												if(empty($page_id) || $page_id == 0){

												}else{
													if(count($banner_child['result']) > 0){
														$y = count($banner_child['result']);
														foreach($banner_child['result'] as $bchild){

															echo '
															<div class="form-group">
																<label>Image</label>
																<div class="row">
																  <div class="col-md-8">
																    <div class="input-group">
																    <input type="text" class="form-control" id="txtCPath'.$y++.'" value="'.$bchild['image'].'">
																    <span class="input-group-btn">
																    <button id="ppm-browse-image0" type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
																    </span>
																    </div>
																  </div>
																</div>
															</div>
															';
														}
													}
												}
											?>
										</div>
									</div>

									<div class="row" id="divImageInfo">
										<div class="col-md-12">

											<div class="form-group">
												<input type="hidden" id="bid" value=""/>
												<label>Image</label>
												<div class="row">
												  <div class="col-md-8">
												    <div class="input-group">
												    <input type="text" class="form-control" id="txtPath" value="<?php echo (isset($image)) ? $image : ''; ?>">
												    <span class="input-group-btn">
												    <button id="ppm-browse-image" type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
												    </span>
												    </div>
												  </div>
												</div>
												
											</div>

											<div class="form-group">
												<label>Link</label>
												<input type="text" name="txtLink" id="txtLink" class="form-control">
											</div>

											<div class="form-group">
												<label>Placement</label>
												<div class="row">
													<div class="col-md-6">
														<select name="placement" id="placement" class="form-control">
															<option value="">- SELECT -</option>
															<option value="1" <?php echo (isset($placement) && $placement == '1') ? 'selected' : ''; ?> >Top Banner</option>
		<!-- 													<option value="2">Text Banner</option>
															<option value="3">Partner Banner</option> -->
															<option value="4" <?php echo (isset($placement) && $placement == '4') ? 'selected' : ''; ?> >Bottom Banner</option>
														</select>
													</div>
												</div>				
											</div>



											<div class="form-group" style="display: none;">
												<label>Display On</label>
												<div class="row">
													<div class="col-md-6">
														<?php
															// print_r($pages);
														?>
														<select name="displayOn" id="displayOn" class="form-control" multiple>
														<?php
															
															foreach($pages['result'] as $p){
										                        if($p['status'] == 'draft'){
										                          $status = ' *';
										                        }else{
										                          $status = '';
										                        }
																echo '<option value="'.$p['pid'].'">'.$p['title'].' '.$status.'</option>';
															}
														?>
														</select>
													</div>
												</div>				
											</div>

										</div>
									</div>

									<button type="button" class="btn btn-primary pull-right" id="btnSave"> <i class="fa fa-save"></i> SAVE </button>

								</div>
					  		</div>
					  		<!-- END FORM  -->
					  </div>
					</div>
					<!-- END PANEL -->
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-8">
					<div class="alert alert-warning">
						<p><strong>Note:</strong></p>
						<p>Top Banners are to be 635 Pixels Wide X 118 Pixels High.</p>
						<p>Side Banners to be: 190 Pixels Wide Pixels High.</p>
						<p>* - Pages that are DRAFT</p>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

<?php if(isset($upload)) echo $upload; ?>

<script>
$(document).ready(function(){
	// $('input').on('ifChecked', function(event){
	//   alert(event.type + ' callback');
	// });
	<?php
		if(empty($page_id) || $page_id == 0){ 
	?>
			$('#divImageCarousel').hide();
			var x = 0;
	<?php  
		}else{
			if(count($banner_child['result']) > 0){
				?>
				$('#divImageInfo').hide();
				$('#divImageCarousel').show();
				var x = <?php echo count($banner_child['result']); ?>;
				<?php
			}
		} 
	?>

	$('#bannerType').change(function(){
		var val = $(this).val();
		if(val == '1'){
			$('#divImageInfo').show();
			$('#divImageCarousel').hide();
		}
		if(val == '2'){
			$('#divImageInfo').show();
			$('#divImageCarousel').hide();
		}
		if(val == '3'){
			$('#divImageInfo').hide();
			$('#divImageCarousel').show();
		}
	});

	
	$('#btnAddImage').click(function(){
		x++;	
		var btnHTML = '\
		<div class="form-group">\
			<input type="hidden" id="bid" value=""/>\
			<label>Image</label>\
			<div class="row">\
			  <div class="col-md-8">\
			    <div class="input-group">\
			    <input type="text" class="form-control" id="txtCPath'+ x +'">\
			    <span class="input-group-btn">\
			    <button id="ppm-browse-image'+ x +'" type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>\
			    </span>\
			    </div>\
			  </div>\
			</div>\
		</div>\
		';

		$('#divImageCarousel > div ').append(btnHTML);
		PPMGallery.initButton('ppm-browse-image'+ x  );
	});


	$('#btnSave').click(function(){
		var displayOn = $('#displayOn');
  		var array_displayOn = [];
    	displayOn.each(function(){ array_displayOn.push($(this).val()) });
		console.log(array_displayOn);

		var bid	= $('#bid').val();
		var txtLink	= $('#txtLink').val();
		var placement	= $('#placement').val();
		var chkIsActive = $('#chkIsActive');
		var isActive = '';

		var bannerType = $('#bannerType').val();

		if(chkIsActive.prop('checked')){
			// alert('Hello');
			isActive = "1";
		}else{
			// alert('world');
			isActive = "0";
		}

		if(selectedFilePath != ''){
			// if(placement == 0){
			//     swal({
			//         title: "Error",
			//         text: "Please select Placement!",
			//         icon: "error",
			//         // buttons: ["Cancel", true],
			//         dangerMode: true
			//     })
			// }else{
			if(bannerType == 3){
				// var txtCPath0 = $('#txtCPath0').val();
				// var txtCPath1 = $('#txtCPath1').val();
				var imgPathC =  $('#divImageCarousel input[type=text]');
				var imgPathC_arr = [];
				imgPathC.each(function(){
					imgPathC_arr.push($(this).val());
				});
				console.log(imgPathC_arr);
				console.log(x);
				$.ajax({
					type : 'post',
					url  : '<?php echo base_url(); ?>admin/banners/saveCarousel',
					data : {
						'bid' : bid,
						'imgPathC_arr': imgPathC_arr,
						'count' : x,
						'isActive': isActive
					},
					// dataType : 'json',
					beforeSend: function(){
					  console.log('Sending..');
					},success : function(res){
					  console.log(res);
					  // var len = res.result.length;
					  // if(len > 0){
					  //   $('#txtPageTitle').val(res.result[0].title);
					  //   CKEDITOR.instances.editor_1.setData(res.result[0].content);
					  //   $('#pageStatus').val(res.result[0].status);
					  //   $('#selParentPage').val(res.result[0].parent_id);
					  // }
					  // console.log('length : ' + len);
					    swal({
					        title: "Success",
					        text: "The New Banner has been added!",
					        icon: "success",
					        // buttons: ["Cancel", true],
					        dangerMode: false
					    }).then((value) => {
						  window.location.reload();
						});
					}
				});

				
				// console.log(imgPathC_arr);

			 //    swal({
			 //        title: "Error",
			 //        text: "Carousel Banner!" + txtCPath1,
			 //        icon: "error",
			 //        // buttons: ["Cancel", true],
			 //        dangerMode: true
			 //    })
			}else{
		      $.ajax({
		        type : 'post',
		        url  : '<?php echo base_url(); ?>admin/banners/saveBanner',
		        data : {
		        	'bid' : bid,
					'txtLink': txtLink,
					'placement': placement,
					'selectedFilePath': $('#txtPath').val(),
					'isActive': isActive
		        },
		        dataType : 'json',
		        beforeSend: function(){
		          console.log('Sending..');
		        },success : function(res){
		          console.log(res);
		          // var len = res.result.length;
		          // if(len > 0){
		          //   $('#txtPageTitle').val(res.result[0].title);
		          //   CKEDITOR.instances.editor_1.setData(res.result[0].content);
		          //   $('#pageStatus').val(res.result[0].status);
		          //   $('#selParentPage').val(res.result[0].parent_id);
		          // }
		          // console.log('length : ' + len);
				    swal({
				        title: "Success",
				        text: "The New Banner has been added!",
				        icon: "success",
				        // buttons: ["Cancel", true],
				        dangerMode: false
				    })
		        }
		      });				
			}
		
			// }

		}else{
			// alert('Please upload or select your image!');
		    swal({
		        title: "Error",
		        text: "Please upload or select your image!",
		        icon: "error",
		        // buttons: ["Cancel", true],
		        dangerMode: true
		    })
		}
		//alert(selectedFilePath);
	});
});
</script>