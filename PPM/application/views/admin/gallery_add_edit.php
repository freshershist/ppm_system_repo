<style>
	hr.style-two {
	    border: 0;
	    height: 0;
	    border-top: 1px solid rgba(0, 0, 0, 0.1);
	    border-bottom: 1px solid rgba(255, 255, 255, 0.3);
	}

	ul.to_do.sortable {
		overflow-y: auto;
		overflow-x: hidden;
		max-height: 55vh;
	}

	ul.to_do.sortable li {
		min-height: 58px;
	}

	ul.to_do li.sortable-placeholder {
		background: none;
		border: 2px dashed rgba(60,118,61,0.5);/*#3c763d;*/
	}

	.sort-order span {
    	font-size: 15px;
		line-height: 35px;
	}

	li.photo-item {
		overflow: hidden;
	}

	li.photo-item:hover {
	    cursor: move; /* fallback if grab cursor is unsupported */
	    cursor: grab;
	    cursor: -moz-grab;
	    cursor: -webkit-grab;
	}

	 /* (Optional) Apply a "closed-hand" cursor during drag operation. */
	li.photo-item:active { 
	    cursor: grabbing;
	    cursor: -moz-grabbing;
	    cursor: -webkit-grabbing;
	}

	.photo-action {
	    font-size: 20px;
	    line-height: 40px;
	    width: 50px;   	
	}

</style>

<div class="container">

	<div class="col-md-12 col-sm-12 col-xs-12">
		<p class="lead"><small class="text-info small">Compulsary fields are marked by an asterisk ( <i class="fa fa-asterisk text-danger small"></i> )</small></p>

		<form id="add-edit-form" class="form-horizontal form-label-left">
			<input id="data-id" type="hidden" name="id" value="" />
			
			<!-- start accordion -->
			<div class="accordion" id="accordion-add-edit" role="tablist" aria-multiselectable="true">

				<!-- General -->	
				<div class="panel">
					<a class="panel-heading" role="tab" id="contents-add-edit_1" data-toggle="collapse" data-parent="#accordion-add-edit" href="#add-edit_1" aria-expanded="true" aria-controls="add-edit_1">
						<h4 class="panel-title">General</h4>
					</a>
					<div id="add-edit_1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="contents-add-edit_1">
						<div class="panel-body">

							<div class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Name: <i class="fa fa-asterisk text-danger"></i></label>
								<div class="col-md-5 col-sm-12 col-xs-12">
									<input id="data-name" name="name" type="text" class="form-control" placeholder="Name" value="" />
								</div>
							</div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-12 col-xs-12">Category: <i class="fa fa-asterisk text-danger"></i></label>
                                <div class="col-md-5 col-sm-12 col-xs-12">
                                    <?php
                                        echo form_dropdown('category', $categories_arr, '', 'id="data-category" class="form-control"');

                                    ?>
                                </div>
                            </div>							

							<div class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Date Posted: <i class="fa fa-asterisk text-danger"></i></label>
								<div class="col-md-5 col-sm-12 col-xs-12">
							        <div class="input-group date" id="mydate">
							            <input id="data-mydate" name="mydate" type='text' class="form-control" placeholder="dd/mm/yyyy" />
							            <span class="input-group-addon">
							            <span class="glyphicon glyphicon-calendar"></span>
							            </span>
							        </div>
								</div>
							</div>								

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-12 col-xs-12">Short Description: </label>
                                <div class="col-md-5 col-sm-12 col-xs-12">
                                    <textarea id="data-shortdesc" name="shortdesc" class="form-control" rows="6"></textarea>
                                </div>
                            </div>

							<div class="form-group">
								<label class="control-label col-md-3 col-sm-12 col-xs-12">Body: </label>
								<div class="col-md-5 col-sm-12 col-xs-12">
									<textarea id="data-body" name="body" class="form-control" rows="6"></textarea>
								</div>
							</div>

						</div>	
					</div>
				</div>
				<!-- General -->

				<div id="files-container" class="panel">
					<a class="panel-heading" role="tab" id="contents-add-edit_2" data-toggle="collapse" data-parent="#accordion-add-edit" href="#add-edit_2" aria-expanded="false" aria-controls="add-edit_2">
							<h4 class="panel-title">Photos <label id="photo-count" class="small"></label></h4>
					</a>
					<div id="add-edit_2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="contents-add-edit_2">
						<div class="panel-body">
							<ul class="to_do" style="padding: 0px 5px;">
								<li>
									<div class="row">
										<div class="col-md-1 col-sm-12 col-xs-12 text-center">
											<label>Order</label>
										</div>
										<div class="col-md-4 col-sm-12 col-xs-12 text-center">
											<label>Photo</label>
										</div>
										<div class="col-md-3 col-sm-12 col-xs-12 text-center">
											<label>Caption</label>
										</div>
										<div class="col-md-3 col-sm-12 col-xs-12 text-center">
											<label>Description</label>
										</div>
										<div class="col-md-1 col-sm-12 col-xs-12 text-center">
											<label>Action</label>
										</div>
									</div>
								</li>		
							</ul>
							<ul id="photo-holder" class="sortable to_do" style="padding: 0px 5px;">
								<li class="sortable-item-copy hide">
									<div class="row">
										<div class="col-md-1 col-sm-12 col-xs-12 text-center sort-order">
											<span>1</span>
											<input type="hidden" name="galleryitem[]" value="" disabled="true" class="data-order"/>
										</div>
										<div class="col-md-4 col-sm-12 col-xs-12">
											<div class="input-group">
												<input type="text" name="galleryitem[]" class="form-control data-photo" disabled="true">
												<span class="input-group-btn">
												<button type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
												</span>
											</div>
										</div>
										<div class="col-md-3 col-sm-12 col-xs-12">
											<div class="form-group">
												<input name="galleryitem[]" type="text" class="form-control data-caption" placeholder="" value="" disabled="true" />
											</div>
										</div>
										<div class="col-md-3 col-sm-12 col-xs-12">
											<div class="form-group">
												<input name="galleryitem[]" type="text" class="form-control data-desc" placeholder="" value="" disabled="true" />
											</div>
										</div>
										<div class="col-md-1 col-sm-12 col-xs-12 text-center">
											<a class="photo-action action-view" href="javascript:;"><i class="fa fa-eye text-success"></i></a>
											<span class="photo-action"> | </span>
											<a class="photo-action action-delete" href="javascript:;"><i class="fa fa-trash text-danger"></i></a>
										</div>
									</div>
								</li>
							</ul>

							<button id="photo-add-btn" type="button" class="btn btn-sm btn-success pull-right"><i class="fa fa-plus"></i> Add Row</button>

						</div>
					</div>
				</div>
				<!-- Files -->					

			</div>
	
		</form>

	</div>	

	<div class="row">
		<div class="panel">
			<div class="col-md-12 text-right">
				<button id="cancel-btn" type="button" class="btn btn-sm btn-warning" style="display: none;">Cancel Changes</button>
				<button id="update-btn" type="button" class="btn btn-sm btn-success" style="display: none;">Update</button>
				<button id="add-btn" type="button" class="btn btn-sm btn-success add-btn">Add</button>
			</div>
			<div class="clearfix"></div>
		</div>	
	</div>

</div>

<div class="row hide">
	<div class="col-md-5">
		<div class="input-group">
		<input type="text" class="form-control">
		<span class="input-group-btn">
		<button id="ppm-browse-image" type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
		</span>
		</div>
	</div>
</div>

<script type="text/javascript" src="<?php echo base_url() . 'assets/js/fancybox-master/dist/jquery.fancybox.min.js'; ?>"></script>

<script>

	$(document).ready(function(){
		var form = $('#add-edit-form');
		var fields = {
						id: 		$('#data-id'),
						name: 		$('#data-name'),
						mydate: 	$('#data-mydate'),
						category: 	$('#data-category'),
						body: 		$('#data-body'),
						shortdesc:	$('#data-shortdesc')
					};
		var photoHolder = $('#photo-holder');
		var photoRowCopy = $('.sortable-item-copy');
		var photoCount = $('#photo-count');

		var contents = {
			categoryArr: <?php echo json_encode($categories_arr); ?>,
			img_path: '<?php echo base_url() . 'assets/uploads/files/'; ?>',
			init: function(){

				addBtn.bind('click', function(){
					contents.submitForm();
				});

				updateBtn.bind('click', function(){
					contents.submitForm();
				});

				$('#mydate').datetimepicker({format: 'DD/MM/YYYY'});

				$('#photo-add-btn').on('click', function(){
					contents.addPhotoRow();
				});

				contents.initSortable();

				this.updatePhotoCount();

			},
			submitForm: function(){

				var isEdit = $.isNumeric(fields.id.val());

				swal({
				    title: (isEdit)?"Update Item" : "Add Item",
				    text: "Are you sure?",
				    icon: "warning",
				    buttons: ["Cancel", true],
				    dangerMode: false,
				})
				.then(willSubmit => {
				    if (willSubmit) {
						var request = $.ajax({
				          url: "<?php echo base_url(); ?>admin/gallery/add_update",
				          method: "POST",
				          data: form.serialize(),
				          dataType: 'json'
				        });

				        request.done(function( res ) {
				            if(res!=null) {

				            	if(isEdit) {
				            		if(res.hasOwnProperty('error')) {
				            			var errMsg = '';

				            			$.each(res.error, function(k, v){
				            				errMsg += '* ' + v + '\n';
				            			});

				            			new PNotify({
											title: 'Invalid or Required!',
											text: errMsg,
											type: 'error',
											styling: 'bootstrap3'
			                            });

				            			swal({title:"Update Failed!",text:"Some fields are required (*) or have invalid values",icon:'error',button:false, timer:3000});
				            		}
				            		else {
				            			filter.applyUpdateChanges(res);

				            			swal({title:"Updated!",text:'Changes have been saved.',icon:'success',button:false, timer:3000});
				            		}
				            		
				            	}
				            	else {
				            		if(res.hasOwnProperty('error')) {
										var errMsg = '';

				            			$.each(res.error, function(k, v){
				            				errMsg += '* ' + v + '\n';
				            			});

				            			new PNotify({
											title: 'Invalid or Required!',
											text: errMsg,
											type: 'error',
											styling: 'bootstrap3'
			                            });

			                            swal({title:"Add Failed!",text:"Some fields are required (*) or have invalid values",icon:'error',button:false, timer:3000});
				            		}
				            		else {
				            			fields.id.val(res.id);
				            			contents.showUpdateButton();
				            			addTab.html('<i class="fa fa-pencil"></i> Edit');
				            			
				            			swal({title:"Success!",text:"Added item with id: " + res.id,icon:'success',button:false, timer:3000});
				            			filter.addNewItem(res);
				            		}
				            	}
				            }
		        		});

				      	request.error(function(_data){
		        			if(_data.readyState == 4 && _data.status == 200) {//Session expired and redirected

		        				swal({
								    title: "Session Expired!",
								    text: "Reloading page.",
								    icon: "warning",
								    dangerMode: false,
								})
								.then(ok => {
								    if (ok) {
								    	window.location.href = '<?php echo base_url();?>admin/login';
								    }
								});
		        			}
		        		});
					}
				}); 

			},
			resetForm: function(){

				$.each(fields, function(k,v){
					if(v.length>0) {
						v.val('');
					}
				});

				$('.photo-item').remove();

				this.updatePhotoCount();
			},
			populateForm: function(data){

				if(data!=null) {

					this.resetForm();

					setTimeout(function(){

						if(data.hasOwnProperty('details')) {
							$.each(data.details, function(k,v){
								if(fields.hasOwnProperty(k)  && fields[k].length>0) {
									fields[k].val(v);
								}
							});
						}

						if(data.hasOwnProperty('items')) {
							$.each(data.items, function(i, v) {
								contents.addPhotoRow(v);
							});
						}

						contents.showUpdateButton();

						if(addTab!=null) addTab.click();
					}, 50);
				}
			},
			showUpdateButton: function(){
				cancelBtn.show();
				updateBtn.show();
				addBtn.hide();	
			},
			showAddButton: function(){
				cancelBtn.hide();
				updateBtn.hide();
				addBtn.show();	
			},
			cancelUpdate: function(){

				swal({
				    title: "Cancel Changes",
				    text: "Are you sure?",
				    icon: "warning",
				    buttons: ["Cancel", true],
				    dangerMode: false,
				})
				.then(willCancel => {
				    if (willCancel) {
				    	contents.resetTab();
				    }
				});
			},
			resetTab: function(){
		    	addTab.html('<i class="fa fa-plus"></i> Add');
		    	contents.resetForm();
				contents.showAddButton();
				searchTab.trigger('click');
			},
			initSortable: function(){

				contents.updateSortOrder();

				$('.sortable').sortable('destroy')
				$('.sortable').sortable().unbind('sortupdate').bind('sortupdate', function() {
					contents.updateSortOrder();
				});;
			},
			updateSortOrder: function() {
				var total = $('.sort-order').length;

				$.each($('.sort-order'), function(k,v){
					var order = total - parseInt(k);
					$(v).find('span').text(order);
					$(v).find('input[type=hidden]').val(order);
				});
			},
			addPhotoRow: function(data) {
				var row 		= photoRowCopy.clone().removeClass('sortable-item-copy hide').addClass('photo-item');
				var browseBtn 	= row.find('button.ppm-browse');
				var viewBtn 	= row.find('a.action-view');
				var deleteBtn 	= row.find('a.action-delete');

				row.appendTo(photoHolder);

				var order 		= row.find('.data-order');
				var photo 		= row.find('.data-photo');
				var caption 	= row.find('.data-caption');
				var desc 		= row.find('.data-desc');

				var _file = 'javascript:;';

				if(data!=null) {
					order.val(data.orderBy);
					photo.val(data.d1);
					caption.val(data.caption);
					desc.val(data.body);

					_file = (data.d1 == '') ? 'javascript:;' : contents.img_path + data.d1;

					viewBtn.attr({'data-caption':data.caption});
				}

				var photo_id = filter.generateID();

				photo.attr({id: photo_id});

				var caption_id = filter.generateID();

				caption.attr({id: caption_id});

				viewBtn.attr({href:_file, 'data-fancybox':"images"});
				contents.viewPhotoRow(viewBtn, photo_id, caption_id);		

				this.initSortable();

				var id = filter.generateID();
				browseBtn.attr({id: id});

				PPMGallery.initButton(id);

				row.find('input').attr({disabled:false});

				this.updatePhotoCount();
				this.deletePhotoRow(deleteBtn);
			},
			updatePhotoCount: function() {
				photoCount.text('('+$('.photo-item').length + ')');
			},
			viewPhotoRow: function(btn, photo_id, caption_id){
				$('#' + photo_id).on('change', function() {
					contents.checkPhotoExist(contents.img_path + $(this).val())
					.on("error", function(e) {
						$(btn).attr({href: 'javascript:;'});
					})
					.on("load", function(e) {
						$(btn).attr({href: contents.img_path + $('#' + photo_id).val()});
					});
				});

				$('#' + caption_id).on('change', function() {
					$(btn).attr({'data-caption': $(this).val()});
				});				
			},
			checkPhotoExist(src) {
				return $("<img>").attr('src', src);
			},
			deletePhotoRow: function(btn){
				$(btn).on('click', function(){
					$(this).parents('li').remove();
					contents.updateSortOrder();
					contents.updatePhotoCount();
				});
			}
		};

		contents.init();

	    addEdit = contents;
	});
</script>