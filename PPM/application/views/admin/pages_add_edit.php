<!-- page content -->
<div class="right_col" role="main" style="min-height: 100vh;">

  <div class="row">
    <div class="col-md-12">
      <?php if(empty($page_id) || $page_id == 0){ ?>
      <h1><i class="fa fa-pagelines"></i> New Page</h1>
      <?php }else{ ?>
      <h1><i class="fa fa-pagelines"></i> Edit Page</h1>
      <?php 
                foreach ($page_info['result'] as $pi) {
                  $slug = $pi['slug'];
                  $ptitle = $pi['title'];
                }
                if(!empty($slug)){
                  $slug = clean_string_dash(strtolower($slug));
                }else{
                  $slug = $ptitle;
                  $slug = clean_string_dash(strtolower($slug));
                }

            } 

      ?>
      <div class="row">
        <div class="col-md-12" id="dispMessages"></div>
      </div>
      <div class="spacer20"></div>
      <div class="row">
          <div class="col-md-12">
              <a href="<?php echo base_url().'admin/pages'; ?>" class="btn btn-primary">
                  <i class="fa fa-arrow-left"></i> Back
              </a>
          </div>
      </div>
      <div class="spacer20"></div>
      <hr/>
      <div class="spacer20"></div>
      <div class="row">
        <div class="col-md-12">

          <!-- START : FORM -->

          <div class="row">
            <!-- START : LEFT PANEL -->
            <div class="col-md-8">
              <div class="form-group">
                <label>Page Title</label>
                <input type="text" class="form-control" id="txtPageTitle">
                <input type="hidden" class="form-control" id="pageID" value="<?php echo (empty($page_id) || $page_id == 0)? '' : $page_id; ?>">
              </div>
              <div class="form-group">
                <label>Page Slug</label>
                <?php //echo base_url(); ?><input type="text" class="form-control" id="txtPageSlug" value="<?php echo (isset($slug))? $slug : '';  ?>">
              </div>
              
              <div class="spacer20"></div>

              <div class="form-group">
                <label>Content</label>
                <!-- <textarea id="editor"></textarea> -->
                <?php //if(isset($upload)) echo $upload; ?>

                <?php //if(isset($editor)) echo $editor; ?>

<div class="row hide-me">
  <div class="col-md-5">
    <div class="input-group">
    <input type="text" class="form-control">
    <span class="input-group-btn">
    <button id="ppm-browse-image" type="button" class="ppm-browse btn btn-primary" data-type="image">Browse | <i class="fa fa-image"></i></button>
    </span>
    </div>
  </div>
</div>

<!-- <div class="row">
  <div class="col-md-5">
    <div class="input-group">
    <input type="text" class="form-control">
    <span class="input-group-btn">
    <button type="button" class="ppm-browse btn btn-primary" data-type="application">Browse | <i class="fa fa-file-text"></i></button>
    </span>
    </div>
  </div>
</div> -->



<!-- <div class="row">
  <div class="col-md-5">
    <div class="input-group">
    <input type="text" class="form-control">
    <span class="input-group-btn">
    <button type="button" class="ppm-browse btn btn-primary" data-type="audio">Browse | <i class="fa fa-file-audio-o"></i></button>
    </span>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-5">
    <div class="input-group">
    <input type="text" class="form-control">
    <span class="input-group-btn">
    <button type="button" class="ppm-browse btn btn-primary" data-type="video">Browse | <i class="fa fa-file-video-o"></i></button>
    </span>
    </div>
  </div>
</div> -->

<?php if(isset($upload)) echo $upload; ?>
<?php if(isset($editor)) echo $editor; ?>

<div class="spacer20"></div>



              </div>



            </div>
            <!-- END : LEFT PANEL -->

            <div class="col-md-2">
              <!-- START : RIGHT PANEL -->
              <div class="form-group">
                <label>Status</label>
                <select class="form-control" id="pageStatus">
                  <option value="draft">Draft</option>
                  <option value="publish">Publish</option>
                </select>
              </div>

              <div class="form-group text-right">
                <button type="button" class="btn btn-default" id="btnPreviewPage"> Preview </button>
                <button type="button" class="btn btn-primary" id="btnSavePage"> <i class="fa fa-save"></i> Save Page </button>
              </div>


              <hr/>
              <h4>Parent Page</h4>
              <div class="form-group">
                <!-- <label>Page</label> -->
                <select class="form-control" id="selParentPage">
                  <option value="0">-select page-</option>
                  <?php 
                    if(count($list_array['result']) > 0):
                        foreach($list_array['result'] as $arr){
                          //`pid`, `title`, `content`, `parent_id`, `menu`, `page_order`, `user_id`, `banner_top`, `banner_bottom`, `status`, `date_added`
                          if($arr['status'] == 'draft'){
                            $status = ' - <strong><em>Draft</em></strong>';
                          }else{
                            $status = '';
                          }

                          if($page_id != $arr['pid']):
                            echo '<option value="'.$arr['pid'].'">'.$arr['title'].'</option>';
                          endif;
                          // echo '<tr>
                          //         <td><input type="checkbox" id="chk'.$arr['pid'].'" class="flat"></td>
                          //         <td>'.$arr['title'].''.$status.'</td>
                          //         <td>'.$arr['user_id'].'</td>
                          //         <td>'.date('m/d/Y H:i:s', strtotime($arr['date_added'])).'</td>
                          //         <td>
                          //           <button class="btn btn-success btnEditPage" type="button" data-pid="'.$arr['pid'].'"><i class="fa fa-pencil"></i></button>
                          //           <button class="btn btn-danger btnDeletePage" type="button"  data-pid="'.$arr['pid'].'"><i class="fa fa-trash"></i></button>
                          //         </td>
                          //       </tr>
                          // ';
                        }
                    endif;
                    
                  ?>
                </select>
              </div>
              <div class="form-group">
                <h4>Menu</h4>
                <select id="selMenu" class="form-control">
                  <option value="">-Select Menu-</option>
                <?php
                 // echo menu_listing();
                  $menu_list = menu_listing();
                  foreach($menu_list as $menu){
                    echo '<option value="'.$menu['menu_id'].'">'.$menu['name'].'</option>';
                  }
                ?>
                </select>

              </div>
              <div class="hide-me">
                <hr/>
                <h4>Page Attribute</h4>
                <div class="form-group">
                  <label>Order</label>
                  <input type="text" class="form-control" id="txtOrder">
                </div>
              </div>

              <hr/>
              <h4>Banner</h4>
              <div class="form-group" id="divPageBannerSelection">
                <?php

                  $hidBanners = 0;
                  if(empty($page_id) || $page_id == 0){
                  }else{
                    // print_r($page_info);
                    // foreach($page_info as $p){
                      foreach ($page_info['result'] as $k) {
                        $banner_top = $k['banner_top'];
                        $banner_bottom = $k['banner_bottom'];
                        if(!empty($banner_top) || $banner_top != ''){
                          $hidBanners++;
                        }
                        if(!empty($banner_bottom) || $banner_bottom != ''){
                          $hidBanners++;
                        }
                      }
                    // }
                  }

                ?>
                <input type="hidden" id="hidBanners" value="<?php echo $hidBanners; ?>">
                <button type="button" class="btn btn-default" id="btnBanner"> <i class="fa fa-search"></i> Browse </button>
                <?php
                  if($hidBanners > 0){
                    // for($x=1; $x <= $hidBanners; $x++){
                      $x = 1;
                      if(!empty($banner_top)){
                        echo '
                          <div class="spacer10"></div>
                          <div class="row feat-img" data-id="'. $page_info['result'][0]['pid'] .'" id="divfeatImg_'.$x++.'">
                            <div class="col-md-12">
                              <button type="button" class="btn btn-danger" style="position: absolute; right: 10px; top: 10px; z-index: 2;" onclick="removeFeatImg('. $page_info['result'][0]['pid'] .', $(this))"><i class="fa fa-trash"></i></button>
                              <img src="'.$banner_top.'" class="img-rounded" style="width: 100%; z-index: 0;" >
                              <select class="form-control">
                                <option value="top" selected>TOP</option>
                                <option value="bottom">BOTTOM</option>
                              </select>
                            </div>
                          </div>
                        ';                      
                      }
                      if(!empty($banner_bottom)){
                        echo '
                          <div class="spacer10"></div>
                          <div class="row feat-img" data-id="'. $page_info['result'][0]['pid'] .'" id="divfeatImg_'.$x++.'">
                            <div class="col-md-12">
                              <button type="button" class="btn btn-danger" style="position: absolute; right: 10px; top: 10px; z-index: 2;" onclick="removeFeatImg('. $page_info['result'][0]['pid'] .', $(this))"><i class="fa fa-trash"></i></button>
                              <img src="'.$banner_bottom.'" class="img-rounded" style="width: 100%; z-index: 0;" >
                              <select class="form-control">
                                <option value="top">TOP</option>
                                <option value="bottom" selected>BOTTOM</option>
                              </select>
                            </div>
                          </div>
                        ';                      
                      }                      
                    // }

                  }
                ?>
              </div>
              <div class="spacer30"></div>
              <!-- END   : RIGHT PANEL -->
            </div>

            <div class="col-md-2">
              <!-- SPACER -->
            </div>

          </div>

          <?php
            // if(isset($is_parent_page)){
            //   echo '<pre>';
            //   print_r($is_parent_page);
            //   echo '</pre>';              
            // }


            // $s = '[{"id":13,"children":[{"id":14}]},{"id":15,"children":[{"id":16},{"id":17},{"id":18}]}]';
            // // $s = '[{"id":13},{"id":14},{"id":15,"children":[{"id":16},{"id":17},{"id":18}]}]';
            // $var = json_decode($s);
            // foreach($var as $r){
            //   echo $r->id.'<br/>';
            //   if(isset($r->children)){
            //     $c = count($r->children);
            //     if($c > 0){
            //       foreach ($r->children as $r2) {
            //         echo '-----'.$r2->id.'<br/>';
            //       }
            //     }                
            //   }


            // }
            // echo '<pre>';
            // print_r($var);
            // echo '</pre>';
          ?>

          <!-- START : MENU BUILDER -->
          <div class="row hide-me">
            <div class="col-md-6">
              <h3>Menu Builder</h3>
              <small><em>(Note: Auto-saves every change[s] you made)</em></small>
              <div class="clearfix spacer20"></div>
                <!-- sTART : MENU DRAGGABLES -->
                <div class="dd" id="nestable3">
                    <ol class="dd-list">
                        <li class="dd-item dd3-item" data-id="13">
                            <div class="dd-handle dd3-handle">Drag</div><div class="dd3-content">Item 13</div>
                        </li>
                        <li class="dd-item dd3-item" data-id="14">
                            <div class="dd-handle dd3-handle">Drag</div><div class="dd3-content">Item 14</div>
                        </li>
                        <li class="dd-item dd3-item" data-id="15">
                            <div class="dd-handle dd3-handle">Drag</div><div class="dd3-content">Item 15</div>
                            <ol class="dd-list">
                                <li class="dd-item dd3-item" data-id="16">
                                    <div class="dd-handle dd3-handle">Drag</div><div class="dd3-content">Item 16</div>
                                </li>
                                <li class="dd-item dd3-item" data-id="17">
                                    <div class="dd-handle dd3-handle">Drag</div><div class="dd3-content">Item 17</div>
                                </li>
                                <li class="dd-item dd3-item" data-id="18">
                                    <div class="dd-handle dd3-handle">Drag</div><div class="dd3-content">Item 18</div>
                                </li>
                            </ol>
                        </li>
                    </ol>
                </div>
                <div class="clearfix spacer20"></div>
                <!-- END  : MENU DRAGGABLES -->
            </div>
            <div class="col-md-6">
              <textarea id="nestable3-output" class="form-control"></textarea>
            </div>
          </div>
          <!-- END  : MENU BUILDER -->


          <!-- END : FORM -->
        </div>
      </div>


    </div>

  </div>

	<div class="clearfix"></div>
</div>

<div id="bannerPanel" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

			<div class="modal-header">
				<h4 class="modal-title">Banner</h4>
			</div>
			<div class="modal-body">
        <div class="row">
          <div class="col-md-12">
          <table class="table table-striped" id="tblBannerList" style="width: 100%;">
            <thead>
              <tr>
                <td>Images</td>
                <td>Banner Type</td>
                <td>Action</td>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
          </div>
        </div>
<!--         <div class="row">
          <div class="col-md-12">

              <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#divBannerList">List</a></li>
                <li><a data-toggle="tab" href="#divBannerSearch">Search</a></li>
              </ul>

              <div class="tab-content">
                <div id="divBannerList" class="tab-pane fade in active">
                  <div class="row">
                    <div class="col-md-12">
                    <table class="table table-striped" id="tblBannerList" style="width: 100%;">
                      <thead>
                        <tr>
                          <td>Images</td>
                          <td>Banner Type</td>
                          <td>Action</td>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                    </div>
                  </div>
                </div>
                <div id="divBannerSearch" class="tab-pane fade">
                    <div class="form-group">
                      <label>Search</label>
                      <input type="text" id="bannerSearch" class="form-control">
                    </div>
                </div>

              </div>



          </div>
        </div> -->
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>

		</div>
	</div>
</div>

<!-- /page content -->

<style>
.spacer10 {
  clear: both;
  width: 100%;
  height: 10px;
}
.spacer15 {
  clear: both;
  width: 100%;
  height: 15px;
}
.spacer20 {
  clear: both;
  width: 100%;
  height: 20px;
}
.spacer25 {
  clear: both;
  width: 100%;
  height: 25px;
}
.spacer30 {
  clear: both;
  width: 100%;
  height: 30px;
}

.hide-me {
  display: none;
}

</style>
<script src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
<script>
  $(document).ready(function(){
    $('#chkAllItems').iCheck({
      checkboxClass: 'icheckbox_square',
      radioClass: 'iradio_square',
      increaseArea: '20%' // optional
    });

    $('#btnSavePage').click(function(){
      // alert('btnSavePage');
      // saveMenu();
      var txtPageTitle = $('#txtPageTitle').val();
      var editor_1 = CKEDITOR.instances.editor_1.getData();
      var pageStatus = $('#pageStatus').val();
      var txtOrder = $('#txtOrder').val();
      var page_id = $('#pageID').val();
      var selParentPage = $('#selParentPage').val();
      var numFeatimage = $('#hidBanners').val();
      var featImgTop = '';
      var featImgBottom = '';

      var selMenu = $('#selMenu').val();
      var slug = $('#txtPageSlug').val();

      var featImg = [];
      if(numFeatimage > 0){
        for(var i=1; i<=numFeatimage; i++){
          featImg.push( $('#divfeatImg_'+ i + ' img').attr('src') );
          var pos = $('#divfeatImg_'+ i + ' select').val();
          if(pos == 'top'){
            featImgTop = $('#divfeatImg_'+ i + ' img').attr('src');
          }

          if(pos == 'bottom'){
            featImgBottom = $('#divfeatImg_'+ i + ' img').attr('src');
          }
        }
      }
      console.log(featImg)

      $.ajax({
        type : 'post',
        url  : '<?php echo base_url(); ?>admin/pages/savePage',
        data : {
          'txtPageTitle' : txtPageTitle,
          'editor_1'     : editor_1,
          'pageStatus'   : pageStatus,
          'txtOrder'     : txtOrder,
          'page_id'      : page_id,
          'selParentPage': selParentPage,
          'banner_top'   : featImgTop,
          'banner_bottom': featImgBottom,
          'selMenu' : selMenu,
          'slug' : slug
        },
        beforeSend : function(){
          $('#btnSavePage').attr('disabled', true);
          $('#btnSavePage').html(' <i class="fa fa-spinner fa-pulse"></i> Saving... ');
        },
        success : function(result){
          console.log(result);
          $('#pageID').val(result);
          $('#btnSavePage').removeAttr('disabled');
          $('#btnSavePage').html(' <i class="fa fa-save"></i> Save Page ');
          swal("Poof! Page has been saved!", {
            icon: "success",
          });
          setTimeout(function(){
            swal.close();
          }, 2000);
        }
      });
    });

    $('#txtPageTitle').blur(function(){
      var txtPageSlug = $('#txtPageSlug').val();
      if(txtPageSlug == ''){
        var me = $(this).val();
        $('#txtPageSlug').val(me.split(" ").join('-'));
      }
    });

    // $('#txtPageSlug').blur(function(){
    //   var txtPageSlug = $('#txtPageSlug').val();
    //   if(txtPageSlug != ''){
    //     var me = $(this).val();
    //     $('#txtPageSlug').val(me.split(" ").join('-'));
    //   }
    // });

    $('#btnPreviewPage').click(function(){
      $.ajax({
        type : 'post',
        url  : '<?php echo base_url(); ?>admin/pages/preview',
        data : {
          'page_id' : '<?php if(!empty($page_id)) echo $page_id; ?>'
        },
        success : function(res){
          window.open('<?php echo base_url(); ?>' + res, '_blank');
        }
      });
    });

  });
</script>

<script src="<?php echo base_url(); ?>assets/js/nestable/jquery.nestable.js"></script>
<!--<script src="//cdnjs.cloudflare.com/ajax/libs/nestable2/1.6.0/jquery.nestable.min.js"></script>-->
<?php if(empty($page_id) || $page_id == 0){ ?>
<?php }else{ ?>
<script>
  function AjaxGetPageDetails(){
      $.ajax({
        type : 'post',
        url  : '<?php echo base_url(); ?>admin/pages/page_details',
        data : {
          'page_id' : '<?php echo  $page_id; ?>'
        },
        dataType : 'json',
        beforeSend: function(){
          console.log('Sending..');
        },success : function(res){
          console.log(res);
          var len = res.result.length;
          if(len > 0){
            $('#txtPageTitle').val(res.result[0].title);
            CKEDITOR.instances.editor_1.setData(res.result[0].content);
            $('#pageStatus').val(res.result[0].status);
            $('#selParentPage').val(res.result[0].parent_id);
            $('#selMenu').val(res.result[0].menu);
          }
          console.log('length : ' + len);
        }
      });
  }

  $(document).ready(function(){
    AjaxGetPageDetails();
  });
</script>
<?php } ?>
<script>
// $(function  () {
//   $("ol.example").sortable();
// });

// var oldContainer;
// $("ol.example").sortable({
//   group: 'nested',
//   afterMove: function (placeholder, container) {
//     if(oldContainer != container){
//       if(oldContainer)
//         oldContainer.el.removeClass("active");
//       container.el.addClass("active");

//       oldContainer = container;
//     }
//   },
//   onDrop: function ($item, container, _super) {
//     container.el.removeClass("active");
//     _super($item, container);
//   }
// });

  var saveMenu = function(){
    $.ajax({
      type : 'post',
      url  : '<?php echo base_url(); ?>admin/pages/saveMenu',
      data : {
        axn : 'save',
      },
      success : function(result){
        // alert(result);
        $('#dispMessages').html(result)
      }
    })
  };


  var updateOutput = function(e)
  {
      var list   = e.length ? e : $(e.target),
          output = list.data('output');
      if (window.JSON) {
          output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
      } else {
          output.val('JSON browser support required for this demo.');
      }
  };

  $(document).ready(function(){
    $('#nestable3').nestable().on('change', updateOutput);

    updateOutput($('#nestable3').data('output', $('#nestable3-output')));


    // setTimeout(function(){
    //     // var json = $('#nestable3-output').val('[{"id":13,"children":[{"id":14}]},{"id":15,"children":[{"id":16},{"id":17},{"id":18}]}]');
    //     var json = '[{"id":13,"children":[{"id":14}]},{"id":15,"children":[{"id":16},{"id":17},{"id":18}]}]';
    //     var options = {'json': json };
    //     $('#nestable-json').nestable(options);
    // }, 10000);

  });


  var table = "";
  $(function() {
    // Handler for .ready() called.
    table = $('#tblBannerList').DataTable({
      "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
      "columnDefs": [{ "orderable": false, "targets": [2] }],
      "order": [[ 0, "asc" ]]
    });
  });

  function removeFeatImg(divid, me){

    me.parent().parent().remove();
    // $('.feat-img[data-id="'+ divid +'"]').remove();
  }

  function populateBanner(banner_id){
      var hidBanners = $('#hidBanners').val();
      if(hidBanners < 2){
        hidBanners++;
        $('#hidBanners').val(hidBanners);
        $.ajax({
          type : 'post',
          url  : '<?php echo base_url(); ?>admin/pages/bannerData',
          data : {
            'bid' : banner_id
          },
          dataType : 'json',
          beforeSend : function(){

          },
          success : function(res){
            console.log(res);
            var len = res.result.length;
            console.log('length:'+ len);
            var html_ = '\
              <div class="spacer10"></div>\
              <div class="row feat-img" data-id="'+ res.result[0].id +'" id="divfeatImg_'+ hidBanners +'">\
                <div class="col-md-12">\
                  <button type="button" class="btn btn-danger" style="position: absolute; right: 10px; top: 10px; z-index: 2;" onclick="removeFeatImg('+ res.result[0].id +', $(this))"><i class="fa fa-trash"></i></button>\
                  <img src="<?php echo base_url(); ?>/assets/uploads/files/'+ res.result[0].image +'" class="img-rounded" style="width: 100%; z-index: 0;" >\
                  <select class="form-control">\
                    <option value="top">TOP</option>\
                    <option value="bottom">BOTTOM</option>\
                  </select>\
                </div>\
              </div>\
            ';

            $('#divPageBannerSelection').append(html_);
          }
        })
      }else{
        swal({
          title: "Error!",
          text: "You can only have 2 featured image or banner!",
          icon: "error",
          dangerMode: true,
        })
      }
  }

  $('#btnBanner').click(function(){
    table.clear().draw();
    table.destroy();
    $.ajax({
      type : 'post',
      url  : '<?php echo base_url(); ?>admin/pages/bannerlist',
      data : {
      },
      dataType: 'json',
      beforeSend : function(){
        table = $('#tblBannerList').DataTable({
          "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
          "columnDefs": [{ "orderable": false, "targets": [2] }],
          "order": [[ 0, "asc" ]]
        });        
      },
      success : function(result){
        // alert(result);
        console.log(result['result'].length);
        console.log(result);
        var len = result['result'].length;
        if(len > 0){
          for(var i = 0; i < len; i++){
            table.row.add( [
                '<img src="<?php echo base_url(); ?>/assets/uploads/files/'+ result['result'][i].image + '" class="img-rounded" style="width: 100px;">' ,
                result['result'][i].contentType,
                '<button type="button" class="btn btn-primary" data-id="'+ result['result'][i].id +'" onclick="javascript:populateBanner('+ result['result'][i].id +');"><i class="fa fa-download"></i></button>'
            ] ).draw( false );            
          }
        }
        // $('#dispMessages').html(result)
        $('#bannerPanel').modal('show');
      }
    });
  });

</script>

<style>
/*.cf:after { visibility: hidden; display: block; font-size: 0; content: " "; clear: both; height: 0; }
* html .cf { zoom: 1; }
*:first-child+html .cf { zoom: 1; }

html { margin: 0; padding: 0; }
body { font-size: 100%; margin: 0; padding: 1.75em; font-family: 'Helvetica Neue', Arial, sans-serif; }

h1 { font-size: 1.75em; margin: 0 0 0.6em 0; }

a { color: #2996cc; }
a:hover { text-decoration: none; }

p { line-height: 1.5em; }
.small { color: #666; font-size: 0.875em; }
.large { font-size: 1.25em; }*/

/**
 * Nestable
 */

.dd { position: relative; display: block; margin: 0; padding: 0; max-width: 600px; list-style: none; font-size: 13px; line-height: 20px; }

.dd-list { display: block; position: relative; margin: 0; padding: 0; list-style: none; }
.dd-list .dd-list { padding-left: 30px; }
.dd-collapsed .dd-list { display: none; }

.dd-item,
.dd-empty,
.dd-placeholder { display: block; position: relative; margin: 0; padding: 0; min-height: 20px; font-size: 13px; line-height: 20px; }

.dd-handle { display: block; height: 30px; margin: 5px 0; padding: 5px 10px; color: #333; text-decoration: none; font-weight: bold; border: 1px solid #ccc;
    background: #fafafa;
    background: -webkit-linear-gradient(top, #fafafa 0%, #eee 100%);
    background:    -moz-linear-gradient(top, #fafafa 0%, #eee 100%);
    background:         linear-gradient(top, #fafafa 0%, #eee 100%);
    -webkit-border-radius: 3px;
            border-radius: 3px;
    box-sizing: border-box; -moz-box-sizing: border-box;
}
.dd-handle:hover { color: #2ea8e5; background: #fff; }

.dd-item > button { display: block; position: relative; cursor: pointer; float: left; width: 25px; height: 20px; margin: 5px 0; padding: 0; text-indent: 100%; white-space: nowrap; overflow: hidden; border: 0; background: transparent; font-size: 12px; line-height: 1; text-align: center; font-weight: bold; }
.dd-item > button:before { content: '+'; display: block; position: absolute; width: 100%; text-align: center; text-indent: 0; }
.dd-item > button[data-action="collapse"]:before { content: '-'; }

.dd-placeholder,
.dd-empty { margin: 5px 0; padding: 0; min-height: 30px; background: #f2fbff; border: 1px dashed #b6bcbf; box-sizing: border-box; -moz-box-sizing: border-box; }
.dd-empty { border: 1px dashed #bbb; min-height: 100px; background-color: #e5e5e5;
    background-image: -webkit-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
                      -webkit-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
    background-image:    -moz-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
                         -moz-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
    background-image:         linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
                              linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
    background-size: 60px 60px;
    background-position: 0 0, 30px 30px;
}

.dd-dragel { position: absolute; pointer-events: none; z-index: 9999; }
.dd-dragel > .dd-item .dd-handle { margin-top: 0; }
.dd-dragel .dd-handle {
    -webkit-box-shadow: 2px 4px 6px 0 rgba(0,0,0,.1);
            box-shadow: 2px 4px 6px 0 rgba(0,0,0,.1);
}

/**
 * Nestable Extras
 */

.nestable-lists { display: block; clear: both; padding: 30px 0; width: 100%; border: 0; border-top: 2px solid #ddd; border-bottom: 2px solid #ddd; }

#nestable-menu { padding: 0; margin: 20px 0; }

#nestable-output,
#nestable2-output { width: 100%; height: 7em; font-size: 0.75em; line-height: 1.333333em; font-family: Consolas, monospace; padding: 5px; box-sizing: border-box; -moz-box-sizing: border-box; }

#nestable2 .dd-handle {
    color: #fff;
    border: 1px solid #999;
    background: #bbb;
    background: -webkit-linear-gradient(top, #bbb 0%, #999 100%);
    background:    -moz-linear-gradient(top, #bbb 0%, #999 100%);
    background:         linear-gradient(top, #bbb 0%, #999 100%);
}
#nestable2 .dd-handle:hover { background: #bbb; }
#nestable2 .dd-item > button:before { color: #fff; }

@media only screen and (min-width: 700px) {

    .dd { float: left; width: 48%; }
    .dd + .dd { margin-left: 2%; }

}

.dd-hover > .dd-handle { background: #2ea8e5 !important; }

/**
 * Nestable Draggable Handles
 */

.dd3-content { display: block; height: 30px; margin: 5px 0; padding: 5px 10px 5px 40px; color: #333; text-decoration: none; font-weight: bold; border: 1px solid #ccc;
    background: #fafafa;
    background: -webkit-linear-gradient(top, #fafafa 0%, #eee 100%);
    background:    -moz-linear-gradient(top, #fafafa 0%, #eee 100%);
    background:         linear-gradient(top, #fafafa 0%, #eee 100%);
    -webkit-border-radius: 3px;
            border-radius: 3px;
    box-sizing: border-box; -moz-box-sizing: border-box;
}
.dd3-content:hover { color: #2ea8e5; background: #fff; }

.dd-dragel > .dd3-item > .dd3-content { margin: 0; }

.dd3-item > button { margin-left: 30px; }

.dd3-handle { position: absolute; margin: 0; left: 0; top: 0; cursor: pointer; width: 30px; text-indent: 100%; white-space: nowrap; overflow: hidden;
    border: 1px solid #aaa;
    background: #ddd;
    background: -webkit-linear-gradient(top, #ddd 0%, #bbb 100%);
    background:    -moz-linear-gradient(top, #ddd 0%, #bbb 100%);
    background:         linear-gradient(top, #ddd 0%, #bbb 100%);
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
}
.dd3-handle:before { content: '≡'; display: block; position: absolute; left: 0; top: 3px; width: 100%; text-align: center; text-indent: 0; color: #fff; font-size: 20px; font-weight: normal; }
.dd3-handle:hover { background: #ddd; }
</style>